﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using static MCS.App_Code._Config;
using WordToPDF;
using static MCS.App_Code.Enums;

namespace MCS.NewCredit
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsCustomerController.CustomerController wsCController = new wsCustomerController.CustomerController();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();
        MCS.wsLoanController.LoanController wsLController = new MCS.wsLoanController.LoanController();
        MCS.wsUserController.UserController wsUController = new wsUserController.UserController();
        MCS.wsAccountController.AccountController wsAController = new MCS.wsAccountController.AccountController();

        _Config _config = new _Config();
        wsCustomerController.GetCustomerRequest reqCustomer = new wsCustomerController.GetCustomerRequest();
        wsLoanController.AddLoanRequestDTO addLoanRequest = new wsLoanController.AddLoanRequestDTO();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                FillData();
                lnkTabLoanPayments_Click(null, null); // sub tabs active

                //add loan id for new loan
                addLoanRequest.CreatedID = UserSession.UserId;
                wsLoanController.AddLoanResponse response = wsLController.AddLoan(addLoanRequest);
                hdnFieldLoanId.Value = "";
                if (!response.isSuccess)
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi : " + response.errorCode);
                    return;
                }
                hdnFieldLoanId.Value = Convert.ToString(response.loanId);
                isLoanActive(Convert.ToInt32(hdnFieldLoanId.Value));
                fillLoanCollateral();
                fillLoanCard();
                fillLoanDocuments();
                fillLoanNotes();
                fillLoanGuarantor();
                fillLoanLender_Verifycator();

                txtInterest.Text = _config.GetAppSetting("creditInterestValue");
            }

            //default input values
            txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
            if (true) // if input txtStartDate is admin , shert deyishcek
            {
                txtStartDate.Attributes.Add("disabled", "disabled");
            }
            txtPaymentDate.Text = DateTime.ParseExact(txtStartDate.Text, "dd.MM.yyyy", CultureInfo.InvariantCulture).AddMonths(1).ToString("dd.MM.yyyy");
            txtPaymentDate.Attributes.Add("disabled", "disabled");
            txtEndDate.Attributes.Add("disabled", "disabled");
            isLoanAccepted(Convert.ToInt32(hdnFieldLoanId.Value));
            drlLoanStatus.Attributes.Add("disabled", "disabled");


            #region Begin Permission
            CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.NEW_CREDIT));
            btnCollateralBarcodeOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRINT_BARCODE));
            btnCollateralReturnProcessOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT_DATE));
            btnInsertCollateralOutOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT));
            btnLoanAnnuitetOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRINT_SCHEDULE));
            btnLoanCollateralAktPrint.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT_ACT));
            btnLoanContractPrint.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRIN_CONTRACT));
            btnLoanDeleteOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_DELETE));
            btnLoanIltizamPrint.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRINT_ILTIZAM));
            btnSaveLender.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_ADDITIONAL_SAVE));
            btnSaveLoan.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_SAVE_LOAN));
            btnSaveLoanCollateralGeneralData.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_HEADER_DATA));
            lnkAddLoanDocument.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_DOCUMENTS_SAVE));
            lnkAddLoanGuarantor.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_GUARANTOR_ADD_NEW));
            lnkAddLoanNote.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_NOTES_ADD_NEW));
            if (grdLoanCollaterals.Rows.Count > 0)
            {
                grdLoanCollaterals.Columns[7].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_SEND_OUT));
                grdLoanCollaterals.Columns[8].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_EDIT));
                grdLoanCollaterals.Columns[9].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_DELETE));
            }
            if (grdLoanCards.Rows.Count > 0)
            {
                grdLoanCards.Columns[4].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_CARD_EDIT));
                grdLoanCards.Columns[5].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_CARD_DELETE));
            }
            if (grdLoanDocuments.Rows.Count > 0)
            {
                grdLoanDocuments.Columns[3].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_DOCUMENTS_DELETE));
            }
            if (grdLoanGuarantor.Rows.Count > 0)
            {
                grdLoanGuarantor.Columns[3].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_GUARANTOR_DELETE));
            }
            if (grdLoanNotes.Rows.Count > 0)
            {
                grdLoanNotes.Columns[3].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_NOTES_DELETE));
            }
            lnkNewLoanCard.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_CARD_ADD_NEW));
            lnkNewLoanCollateral.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_ADD_NEW));
            btnAcceptLoan.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_ACCEPT));

           

            bool _isLoanApprove = wsLController.LoanIsApproved(Convert.ToInt32(hdnFieldLoanId.Value));

            if (UserSession.UserPermission.Equals("STANDART_USER"))
            {
                if (!_isLoanApprove && isOperationToDay)
                {
                        btnCollateralBarcodeOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRINT_BARCODE));
                        btnCollateralReturnProcessOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT_DATE));
                        btnInsertCollateralOutOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT));
                        btnLoanAnnuitetOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRINT_SCHEDULE));
                        btnLoanCollateralAktPrint.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT_ACT));
                        btnLoanContractPrint.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRIN_CONTRACT));
                        btnLoanDeleteOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_DELETE));
                        btnLoanIltizamPrint.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRINT_ILTIZAM));
                        btnSaveLender.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_ADDITIONAL_SAVE));
                        btnSaveLoan.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_SAVE_LOAN));
                        btnSaveLoanCollateralGeneralData.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_HEADER_DATA));
                        lnkAddLoanDocument.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_DOCUMENTS_SAVE));
                        lnkAddLoanGuarantor.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_GUARANTOR_ADD_NEW));
                       // lnkAddLoanNote.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_NOTES_ADD_NEW));
                        if (grdLoanCollaterals.Rows.Count > 0)
                        {
                            grdLoanCollaterals.Columns[7].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_SEND_OUT));
                            grdLoanCollaterals.Columns[8].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_EDIT));
                            grdLoanCollaterals.Columns[9].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_DELETE));
                        }
                        if (grdLoanCards.Rows.Count > 0)
                        {
                        grdLoanCards.Columns[4].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_CARD_EDIT));
                        grdLoanCards.Columns[5].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_CARD_DELETE));
                        }
                        if (grdLoanDocuments.Rows.Count > 0)
                        {
                            grdLoanDocuments.Columns[3].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_DOCUMENTS_DELETE));
                        }
                        if (grdLoanGuarantor.Rows.Count > 0)
                        {
                            grdLoanGuarantor.Columns[3].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_GUARANTOR_DELETE));
                        }
                        if (grdLoanNotes.Rows.Count > 0)
                        {
                            grdLoanNotes.Columns[3].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_NOTES_DELETE));
                        }
                        lnkNewLoanCard.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_CARD_ADD_NEW));
                        lnkNewLoanCollateral.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_ADD_NEW));
                        btnAcceptLoan.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_ACCEPT));
                }
                if (_isLoanApprove)
                {
                    btnCollateralBarcodeOpenModal.Visible = false;
                    btnCollateralReturnProcessOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT_DATE)); ;
                    btnInsertCollateralOutOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT));
                    btnLoanAnnuitetOpenModal.Visible = false;
                    btnLoanCollateralAktPrint.Visible = false;
                    btnLoanContractPrint.Visible = false;
                    btnLoanDeleteOpenModal.Visible = false;
                    btnLoanIltizamPrint.Visible = false;
                    btnSaveLender.Visible = false;
                    btnSaveLoan.Visible = false;
                    btnSaveLoanCollateralGeneralData.Visible = false;
                    lnkAddLoanDocument.Visible = false;
                    lnkAddLoanGuarantor.Visible = false;
                   // lnkAddLoanNote.Visible = false;
                    if (grdLoanCollaterals.Rows.Count > 0)
                    {
                        grdLoanCollaterals.Columns[7].Visible = false;
                        grdLoanCollaterals.Columns[8].Visible = false;
                        grdLoanCollaterals.Columns[9].Visible = false;
                    }
                    if (grdLoanCards.Rows.Count > 0)
                    {
                        grdLoanCards.Columns[4].Visible = false;
                        grdLoanCards.Columns[5].Visible = false;
                    }
                    if (grdLoanDocuments.Rows.Count > 0)
                    {
                        grdLoanDocuments.Columns[3].Visible = false;
                    }
                    if (grdLoanGuarantor.Rows.Count > 0)
                    {
                        grdLoanGuarantor.Columns[3].Visible = false;
                    }
                    if (grdLoanNotes.Rows.Count > 0)
                    {
                        grdLoanNotes.Columns[3].Visible = false;
                    }
                    lnkNewLoanCard.Visible = false;
                    lnkNewLoanCollateral.Visible = false;
                    btnAcceptLoan.Visible = false;
                }
            }

            if (UserSession.UserPermission.Equals("ADMIN"))
            {
                if (isOperationToDay)
                {
                    btnCollateralBarcodeOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRINT_BARCODE));
                    btnCollateralReturnProcessOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT_DATE));
                    btnInsertCollateralOutOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT));
                    btnLoanAnnuitetOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRINT_SCHEDULE));
                    btnLoanCollateralAktPrint.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT_ACT));
                    btnLoanContractPrint.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRIN_CONTRACT));
                    btnLoanDeleteOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_DELETE));
                    btnLoanIltizamPrint.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRINT_ILTIZAM));
                    btnSaveLender.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_ADDITIONAL_SAVE));
                    btnSaveLoan.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_SAVE_LOAN));
                    btnSaveLoanCollateralGeneralData.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_HEADER_DATA));
                    lnkAddLoanDocument.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_DOCUMENTS_SAVE));
                    lnkAddLoanGuarantor.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_GUARANTOR_ADD_NEW));
                    //lnkAddLoanNote.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_NOTES_ADD_NEW));
                    if (grdLoanCollaterals.Rows.Count > 0)
                    {
                        grdLoanCollaterals.Columns[7].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_SEND_OUT));
                        grdLoanCollaterals.Columns[8].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_EDIT));
                        grdLoanCollaterals.Columns[9].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_DELETE));
                    }
                    if (grdLoanCards.Rows.Count > 0)
                    {
                        grdLoanCards.Columns[4].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_CARD_EDIT));
                        grdLoanCards.Columns[5].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_CARD_DELETE));
                    }
                    if (grdLoanDocuments.Rows.Count > 0)
                    {
                        grdLoanDocuments.Columns[3].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_DOCUMENTS_DELETE));
                    }
                    if (grdLoanGuarantor.Rows.Count > 0)
                    {
                        grdLoanGuarantor.Columns[3].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_GUARANTOR_DELETE));
                    }
                    if (grdLoanNotes.Rows.Count > 0)
                    {
                        grdLoanNotes.Columns[3].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_NOTES_DELETE));
                    }
                    lnkNewLoanCard.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_CARD_ADD_NEW));
                    lnkNewLoanCollateral.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_ADD_NEW));
                    btnAcceptLoan.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_ACCEPT));
                }
                else
                {
                    btnCollateralBarcodeOpenModal.Visible = false;
                    btnCollateralReturnProcessOpenModal.Visible = false;
                    btnInsertCollateralOutOpenModal.Visible = false;
                    btnLoanAnnuitetOpenModal.Visible = false;
                    btnLoanCollateralAktPrint.Visible = false;
                    btnLoanContractPrint.Visible = false;
                    btnLoanDeleteOpenModal.Visible = false;
                    btnLoanIltizamPrint.Visible = false;
                    btnSaveLender.Visible = false;
                    btnSaveLoan.Visible = false;
                    btnSaveLoanCollateralGeneralData.Visible = false;
                    lnkAddLoanDocument.Visible = false;
                    lnkAddLoanGuarantor.Visible = false;
                    //lnkAddLoanNote.Visible = false;
                    if (grdLoanCollaterals.Rows.Count > 0)
                    {
                        grdLoanCollaterals.Columns[7].Visible = false;
                        grdLoanCollaterals.Columns[8].Visible = false;
                        grdLoanCollaterals.Columns[9].Visible = false;
                    }
                    if (grdLoanCards.Rows.Count > 0)
                    {
                        grdLoanCards.Columns[4].Visible = false;
                        grdLoanCards.Columns[5].Visible = false;
                    }
                    if (grdLoanDocuments.Rows.Count > 0)
                    {
                        grdLoanDocuments.Columns[3].Visible = false;
                    }
                    if (grdLoanGuarantor.Rows.Count > 0)
                    {
                        grdLoanGuarantor.Columns[3].Visible = false;
                    }
                    if (grdLoanNotes.Rows.Count > 0)
                    {
                        grdLoanNotes.Columns[3].Visible = false;
                    }
                    lnkNewLoanCard.Visible = false;
                    lnkNewLoanCollateral.Visible = false;
                    btnAcceptLoan.Visible = false;
                }
            }

            if (UserSession.UserPermission.Equals("SUPER_ADMIN"))
            {
                btnCollateralBarcodeOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRINT_BARCODE));
                btnCollateralReturnProcessOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT_DATE));
                btnInsertCollateralOutOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT));
                btnLoanAnnuitetOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRINT_SCHEDULE));
                btnLoanCollateralAktPrint.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_COLLATERAL_OUT_ACT));
                btnLoanContractPrint.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRIN_CONTRACT));
                btnLoanDeleteOpenModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_DELETE));
                btnLoanIltizamPrint.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PRINT_ILTIZAM));
                btnSaveLender.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_ADDITIONAL_SAVE));
                btnSaveLoan.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_SAVE_LOAN));
                btnSaveLoanCollateralGeneralData.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_HEADER_DATA));
                lnkAddLoanDocument.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_DOCUMENTS_SAVE));
                lnkAddLoanGuarantor.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_GUARANTOR_ADD_NEW));
                //lnkAddLoanNote.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_NOTES_ADD_NEW));
                if (grdLoanCollaterals.Rows.Count > 0)
                {
                    grdLoanCollaterals.Columns[7].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_SEND_OUT));
                    grdLoanCollaterals.Columns[8].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_EDIT));
                    grdLoanCollaterals.Columns[9].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_DELETE));
                }
                if (grdLoanCards.Rows.Count > 0)
                {
                    grdLoanCards.Columns[4].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_CARD_EDIT));
                    grdLoanCards.Columns[5].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_CARD_DELETE));
                }
                if (grdLoanDocuments.Rows.Count > 0)
                {
                    grdLoanDocuments.Columns[3].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_DOCUMENTS_DELETE));
                }
                if (grdLoanGuarantor.Rows.Count > 0)
                {
                    grdLoanGuarantor.Columns[3].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_GUARANTOR_DELETE));
                }
                if (grdLoanNotes.Rows.Count > 0)
                {
                    grdLoanNotes.Columns[3].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_NOTES_DELETE));
                }
                lnkNewLoanCard.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_CARD_ADD_NEW));
                lnkNewLoanCollateral.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_PART_COLLAT_ADD_NEW));
                btnAcceptLoan.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.N_CREDIT_ACCEPT));
            }

            #endregion


        }

        bool isOperationToDay
        {
            get
            {
                string loanDate = txtStartDate.Text;
                string toDate = _Config.HostingTime.ToString("dd.MM.yyyy");
                return (loanDate == toDate);
            }
        }
        void isLoanActive(int loanId)
        {
            string _status = wsLController.GetLoanStatus(loanId);
            bool isVisible = _status == "ACTIVE";
            btnLoanAnnuitetOpenModal.Visible = isVisible;
            btnLoanContractPrint.Visible = isVisible;
            btnLoanIltizamPrint.Visible = isVisible;
            btnLoanCollateralAktPrint.Visible = isVisible;
            btnInsertCollateralOutOpenModal.Visible = isVisible;
            btnCollateralReturnProcessOpenModal.Visible = isVisible;
            btnCollateralBarcodeOpenModal.Visible = isVisible;
            btnLoanDeleteOpenModal.Visible = isVisible;
        }

        void isLoanAccepted(int loanId)
        {
            bool _isApprove = wsLController.LoanIsApproved(loanId);
            if (_isApprove)
            {
                btnAcceptLoan.Visible = false;
            }
        }

        void FillData()
        {
            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial seçin", ""));
            }

            DataTable dtLoanStatus = wsLController.LoanStatusList();
            if (dtLoanStatus.Rows.Count > 0)
            {
                drlLoanStatus.Items.Clear();
                drlLoanStatus.DataTextField = "LoanStatus";
                drlLoanStatus.DataValueField = "LoanStatusID";
                drlLoanStatus.DataSource = dtLoanStatus;
                drlLoanStatus.DataBind();

                drlLoanStatus.SelectedValue = "1";
            }


            DataTable dtCurrency = wsGController.CurrencyList();
            if (dtCurrency.Rows.Count > 0)
            {
                drlCurrency.Items.Clear();
                drlCurrency.DataTextField = "CurrencyName";
                drlCurrency.DataValueField = "CurrencyID";
                drlCurrency.DataSource = dtCurrency;
                drlCurrency.DataBind();

                drlCurrency.SelectedValue = "1";
            }


            DataTable dtQrafik = wsLController.LoanMethodList();
            if (dtQrafik.Rows.Count > 0)
            {
                drlQrafikType.Items.Clear();
                drlQrafikType.DataTextField = "MethodName";
                drlQrafikType.DataValueField = "MethodID";
                drlQrafikType.DataSource = dtQrafik;
                drlQrafikType.DataBind();
                drlQrafikType.Items.Insert(0, new ListItem("Qrafik seçin", ""));

            }



            DataTable dtLoanTypeList = wsLController.LoanTypeList();
            if (dtLoanTypeList.Rows.Count > 0)
            {
                drlLoanTypeList.Items.Clear();
                drlLoanTypeList.DataTextField = "LoanTypeName";
                drlLoanTypeList.DataValueField = "LoanTypeID";
                drlLoanTypeList.DataSource = dtLoanTypeList;
                drlLoanTypeList.DataBind();
                drlLoanTypeList.Items.Insert(0, new ListItem("Təyinat seçin", ""));
            }

            DataTable dtAccountTypesList = new DataTable("dtAccountTypesList");
            dtAccountTypesList = wsAController.GetAccountTypeList();
            if (dtAccountTypesList.Rows.Count > 0)
            {
                drlAccountTypesList.Items.Clear();
                drlAccountTypesList.DataTextField = "AccountName";
                drlAccountTypesList.DataValueField = "AccountID";
                drlAccountTypesList.DataSource = dtAccountTypesList;
                drlAccountTypesList.DataBind();
                drlAccountTypesList.Items.Insert(0, new ListItem("Seçin", ""));
            }

        }


        protected void linkCustomerSearchModalOpen_Click(object sender, EventArgs e)
        {
            hdnCustomerModalType.Value = "CUSTOMER";
            grdCustomers.Visible = false;
            txtSearchCustomerName.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openSearchCustomerModal();", true);
            drlCustomer.Items.Clear();
        }

        protected void btnSearchCustomer_Click(object sender, EventArgs e)
        {
            FillCustomer();
        }

        void FillCustomer()
        {
            reqCustomer.pageIndex = 1;
            reqCustomer.pageSize = Convert.ToInt32(_config.GetAppSetting("GridPageSize"));
            reqCustomer.Fullname = txtSearchCustomerName.Text.Trim() == "" ? "xxxxx" : txtSearchCustomerName.Text.Trim();
            reqCustomer.Pin = "";
            reqCustomer.IdCardNumber = "";
            reqCustomer.userID = UserSession.UserId;
            int recordSize;
            DataTable dtCustomer = wsCController.GetCustomers(reqCustomer, out recordSize);
            DataTable dtCustomerClone = new DataTable("dtCustomerClone");
            if (dtCustomer != null)
            {
                if (dtCustomer.Rows.Count > 0)
                {
                     dtCustomerClone = dtCustomer.AsEnumerable().Take(10).CopyToDataTable(); // for 10 rows
                }
            }
           
            grdCustomers.DataSource = dtCustomerClone; 
            grdCustomers.DataBind();
            grdCustomers.Visible = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openSearchCustomerModal();", true);
        }

        protected void grdCustomers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lb = e.Row.FindControl("lnkSelectCustomerData") as LinkButton;
                ScriptManager.GetCurrent(this).RegisterPostBackControl(lb);
            }
        }

        protected void lnkSelectCustomerData_Click(object sender, EventArgs e)
        {
            LinkButton lnkSelectCustomerData = (LinkButton)sender;
            string customerId = lnkSelectCustomerData.CommandArgument;

            if (hdnCustomerModalType.Value.Equals("CUSTOMER"))
            {
                int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
                string fullname = Convert.ToString(grdCustomers.DataKeys[rowIndex].Values["Fullname"]);

                drlCustomer.Items.Clear();
                drlCustomer.Items.Add(new ListItem(fullname, customerId));
                drlCustomer.SelectedIndex = 0;
            }
            if (hdnCustomerModalType.Value.Equals("GUARANTOR"))
            {
                // Guarantor
                wsLoanController.AddGuarantorRequest req = new wsLoanController.AddGuarantorRequest();
                req.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);
                req.CusomterID = int.Parse(customerId);
                req.CreatedID = UserSession.UserId;
                wsLoanController.AddGuarantorResponse res = wsLController.AddGuarantor(req);

                if (!res.isSuccess)
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi : " + res.errorCode);
                    return;
                }
                fillLoanGuarantor();
            }

        }


        // Loan tabs
        protected void lnkTabLoanPayments_Click(object sender, EventArgs e)
        {
            tabLoanPayments.Attributes.Add("class", "active");
            tabLoanColleteral.Attributes.Remove("class");
            tabLoanCards.Attributes.Remove("class");
            tabLoanDocuments.Attributes.Remove("class");
            tabLoanGuarantors.Attributes.Remove("class");
            tabLoanNotes.Attributes.Remove("class");
            tabLoanAdditionals.Attributes.Remove("class");
            MultiviewLoans.ActiveViewIndex = 0;
        }

        protected void lnkTabLoanColleteral_Click(object sender, EventArgs e)
        {
            tabLoanColleteral.Attributes.Add("class", "active");
            tabLoanPayments.Attributes.Remove("class");
            tabLoanCards.Attributes.Remove("class");
            tabLoanDocuments.Attributes.Remove("class");
            tabLoanGuarantors.Attributes.Remove("class");
            tabLoanNotes.Attributes.Remove("class");
            tabLoanAdditionals.Attributes.Remove("class");
            MultiviewLoans.ActiveViewIndex = 1;
            fillLoanCollateral();
        }

        protected void lnkTabLoanCards_Click(object sender, EventArgs e)
        {
            tabLoanCards.Attributes.Add("class", "active");
            tabLoanPayments.Attributes.Remove("class");
            tabLoanColleteral.Attributes.Remove("class");
            tabLoanDocuments.Attributes.Remove("class");
            tabLoanGuarantors.Attributes.Remove("class");
            tabLoanNotes.Attributes.Remove("class");
            tabLoanAdditionals.Attributes.Remove("class");
            MultiviewLoans.ActiveViewIndex = 2;
            fillLoanCard();
        }

        protected void lnkTabLoanDocuments_Click(object sender, EventArgs e)
        {
            tabLoanDocuments.Attributes.Add("class", "active");
            tabLoanGuarantors.Attributes.Remove("class");
            tabLoanPayments.Attributes.Remove("class");
            tabLoanColleteral.Attributes.Remove("class");
            tabLoanCards.Attributes.Remove("class");
            tabLoanNotes.Attributes.Remove("class");
            tabLoanAdditionals.Attributes.Remove("class");
            MultiviewLoans.ActiveViewIndex = 3;
            fillLoanDocuments();
        }

        protected void lnkTabLoanNotes_Click(object sender, EventArgs e)
        {
            tabLoanNotes.Attributes.Add("class", "active");
            tabLoanPayments.Attributes.Remove("class");
            tabLoanColleteral.Attributes.Remove("class");
            tabLoanCards.Attributes.Remove("class");
            tabLoanDocuments.Attributes.Remove("class");
            tabLoanGuarantors.Attributes.Remove("class");
            tabLoanAdditionals.Attributes.Remove("class");
            MultiviewLoans.ActiveViewIndex = 5;
            fillLoanNotes();
        }

        protected void lnkTabLoanGuarantor_Click(object sender, EventArgs e)
        {
            tabLoanGuarantors.Attributes.Add("class", "active");
            tabLoanPayments.Attributes.Remove("class");
            tabLoanColleteral.Attributes.Remove("class");
            tabLoanCards.Attributes.Remove("class");
            tabLoanDocuments.Attributes.Remove("class");
            tabLoanNotes.Attributes.Remove("class");
            tabLoanAdditionals.Attributes.Remove("class");
            MultiviewLoans.ActiveViewIndex = 4;
            fillLoanGuarantor();
        }

        protected void lnkTabLoanAdditionals_Click(object sender, EventArgs e)
        {
            tabLoanAdditionals.Attributes.Add("class", "active");
            tabLoanPayments.Attributes.Remove("class");
            tabLoanColleteral.Attributes.Remove("class");
            tabLoanCards.Attributes.Remove("class");
            tabLoanDocuments.Attributes.Remove("class");
            tabLoanNotes.Attributes.Remove("class");
            tabLoanGuarantors.Attributes.Remove("class");
            MultiviewLoans.ActiveViewIndex = 6;
            fillLoanLender_Verifycator();
        }


        void fillLoanCard()
        {
            if (hdnFieldLoanId.Value != "")
            {
                wsLoanController.GetCardRequest req = new wsLoanController.GetCardRequest();
                req.LoanId = Convert.ToInt32(hdnFieldLoanId.Value);
                grdLoanCards.DataSource = wsLController.GetCardrs(req);
                grdLoanCards.DataBind();
            }
        }

        void fillLoanCollateral()
        {
            //fill collateral type list
            DataTable dtColleteral = wsLController.LoanCollateralTypeList();
            if (dtColleteral.Rows.Count > 0)
            {
                drlLoanColleteralType.Items.Clear();
                drlLoanColleteralType.DataSource = dtColleteral;
                drlLoanColleteralType.DataTextField = "CollateralType";
                drlLoanColleteralType.DataValueField = "CollateralTypeID";
                drlLoanColleteralType.SelectedValue = null;
                drlLoanColleteralType.DataBind();
                drlLoanColleteralType.Items.Insert(0, new ListItem("Girov seçin", ""));
            }

            // fill GoldCarat
            DataTable dtGoldCarat = wsLController.GetGoldCaratList();
            if (dtGoldCarat.Rows.Count > 0)
            {
                drlLoanCollateralGoldCarat.DataSource = dtGoldCarat;
                drlLoanCollateralGoldCarat.Items.Clear();
                drlLoanCollateralGoldCarat.DataTextField = "GoldCarat";
                drlLoanCollateralGoldCarat.DataValueField = "CaratID";
                drlLoanCollateralGoldCarat.SelectedValue = null;
                drlLoanCollateralGoldCarat.DataBind();
                drlLoanCollateralGoldCarat.Items.Insert(0, new ListItem("Əyar", "0")); // if collateral not  gold then goldCaratId = 0
            }

            if (hdnFieldLoanId.Value != "")
            {
                // fill list collateral by loan id
                int loanId = Convert.ToInt32(hdnFieldLoanId.Value); 
                DataTable dtCollateralByLoanIDList = wsLController.GetCollateralByLoanIDList(loanId);
                grdLoanCollaterals.DataSource = dtCollateralByLoanIDList;
                grdLoanCollaterals.DataBind();

                if (grdLoanCollaterals.Rows.Count > 0)
                {
                    GridViewRow drr = grdLoanCollaterals.FooterRow;
                    Label lblGridLoanCollateralBruttoWeightFooter = (Label)drr.FindControl("lblGridLoanCollateralBruttoWeightFooter");
                    Label lblGridLoanCollateralWeightFooter = (Label)drr.FindControl("lblGridLoanCollateralWeightFooter");
                    Label lblGridLoanCollateralNettoWeightFooter = (Label)drr.FindControl("lblGridLoanCollateralNettoWeightFooter");
                    Label lblGridLoanCollateralCountFooter = (Label)drr.FindControl("lblGridLoanCollateralCountFooter");
                    Label lblGridLoanCollateralValueFooter = (Label)drr.FindControl("lblGridLoanCollateralValueFooter");


                    var BruttoSum = dtCollateralByLoanIDList.AsEnumerable()
                        //.Where(r => r.Field<string>("Transaction") == "IN")
                        .Sum(x => x.Field<double>("BruttoWeight"));

                    var WeightSum = dtCollateralByLoanIDList.AsEnumerable()
                        // .Where(r => r.Field<string>("Transaction") == "IN")
                        .Sum(x => x.Field<double>("Weight"));

                    var NettoWeightSum = dtCollateralByLoanIDList.AsEnumerable()
                       // .Where(r => r.Field<string>("Transaction") == "IN")
                       .Sum(x => x.Field<double>("NettoWeight"));

                    var CountSum = dtCollateralByLoanIDList.AsEnumerable()
                       // .Where(r => r.Field<string>("Transaction") == "IN")
                       .Sum(x => x.Field<int>("Count"));

                    var CollateralValueSum = dtCollateralByLoanIDList.AsEnumerable()
                       // .Where(r => r.Field<string>("Transaction") == "IN")
                       .Sum(x => x.Field<double>("CollateralValue"));

                    lblGridLoanCollateralBruttoWeightFooter.Text = Convert.ToString(BruttoSum);
                    lblGridLoanCollateralWeightFooter.Text = Convert.ToString(WeightSum);
                    lblGridLoanCollateralNettoWeightFooter.Text = NettoWeightSum.ToString();
                    lblGridLoanCollateralCountFooter.Text = "Say : " + CountSum.ToString();
                    lblGridLoanCollateralValueFooter.Text = Convert.ToString(CollateralValueSum);
                }

                DataTable dtLoanFullData = wsLController.GetLoanFullDatatByLoanID(loanId);
                if (dtLoanFullData == null)
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                    return;
                }
                if (dtLoanFullData.Rows.Count > 0)
                {
                    txtLoanCollateralDepositBox.Text = Convert.ToString(dtLoanFullData.Rows[0]["DepositBox"]);
                    txtLoanCollateralJewelerCost.Text = Convert.ToString(dtLoanFullData.Rows[0]["JewelerCost"]);
                    txtLoanCollateralJewelerName.Text = Convert.ToString(dtLoanFullData.Rows[0]["JewelerName"]);
                    txtLoanCollateralReturnDate.Text = Convert.ToString(dtLoanFullData.Rows[0]["CollateralReturnDate"]);
                }
            }
        }


        void fillLoanNotes()
        {
            if (hdnFieldLoanId.Value != "")
            {
                int loanId = Convert.ToInt32(hdnFieldLoanId.Value);
                grdLoanNotes.DataSource = wsLController.GetNotesByLoanID(loanId);
                grdLoanNotes.DataBind();
            }
        }


        void fillLoanGuarantor()
        {
            if (hdnFieldLoanId.Value != "")
            {
                grdLoanGuarantor.DataSource = wsLController.GetGuarantorsByLoanID(Convert.ToInt32(hdnFieldLoanId.Value));
                grdLoanGuarantor.DataBind();
            }
        }

        void fillLoanDocuments()
        {
            if (hdnFieldLoanId.Value != "")
            {
                int loanId = Convert.ToInt32(hdnFieldLoanId.Value);
                grdLoanDocuments.DataSource = wsLController.GetDocumentsByLoanID(loanId);
                grdLoanDocuments.DataBind();
            }

            drlAddLoanDocumentNote.Items.Clear();
            drlAddLoanDocumentNote.DataTextField = "DocumentName";
            drlAddLoanDocumentNote.DataValueField = "DocumentID";
            drlAddLoanDocumentNote.DataSource = wsLController.GetDocumentList();
            drlAddLoanDocumentNote.DataBind();
            if (drlAddLoanDocumentNote.Items.Count > 1)
            {
                drlAddLoanDocumentNote.Items.Insert(0, new ListItem("Seçin", ""));
            }
        }


        void fillLoanLender_Verifycator()
        {
            if (hdnFieldLoanId.Value != "")
            {
                int loanId = Convert.ToInt32(hdnFieldLoanId.Value);

                drlLender.Items.Clear();
                drlLender.DataTextField = "FullName";
                drlLender.DataValueField = "ID";
                drlLender.DataSource = wsUController.dtUser();
                drlLender.DataBind();
                if (drlLender.Items.Count > 1)
                {
                    drlLender.Items.Insert(0, new ListItem("Seçin", ""));
                }

                // selected value
                try
                {
                    DataTable dtLender = wsLController.GetLenderByLoanID(loanId);
                    drlLender.SelectedValue = Convert.ToString(dtLender.Rows[0]["LenderID"]);
                }
                catch
                {
                    drlLender.SelectedValue = "";
                }


                drlVerifycator.Items.Clear();
                drlVerifycator.DataTextField = "FullName";
                drlVerifycator.DataValueField = "ID";
                drlVerifycator.DataSource = wsUController.dtUser();
                drlVerifycator.DataBind();
                if (drlVerifycator.Items.Count > 1)
                {
                    drlVerifycator.Items.Insert(0, new ListItem("---", ""));
                }


                // selected value
                try
                {
                    DataTable dtVerifier = wsLController.GetVerifierByLoanID(loanId);
                    drlVerifycator.SelectedValue = Convert.ToString(dtVerifier.Rows[0]["VerifierID"]);
                }
                catch
                {
                    drlVerifycator.SelectedValue = "";
                }
            }
        }


        protected void lnkNewLoanCard_Click(object sender, EventArgs e)
        {
            if (hdnFieldLoanId.Value != "")
            {
                wsLoanController.AddCardRequest req = new wsLoanController.AddCardRequest();
                req.CreatedID = UserSession.UserId;
                req.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);
                req.Pan = "";
                req.DateOfExpiry = "";
                req.SecretWord = "";
                req.CardAmount = 0;

                wsLoanController.AddCardResponse res = wsLController.AddCard(req);
                if (!res.isSuccess)
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi : " + res.errorCode);
                    return;
                }
                fillLoanCard();
            }
        }

        protected void grdLoanCards_RowEditing(object sender, GridViewEditEventArgs e)
        {
            grdLoanCards.EditIndex = e.NewEditIndex;
            fillLoanCard();
        }

        protected void grdLoanCards_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grdLoanCards.EditIndex = -1;
            fillLoanCard();
        }

        protected void grdLoanCards_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int cardID = Convert.ToInt32(grdLoanCards.DataKeys[e.RowIndex].Values["CardID"]);

            TextBox txtGridLoanCard_Pan = grdLoanCards.Rows[e.RowIndex].FindControl("txtGridLoanCard_Pan") as TextBox;
            TextBox txtGridLoanCard_DateOfExpiry = grdLoanCards.Rows[e.RowIndex].FindControl("txtGridLoanCard_DateOfExpiry") as TextBox;
            TextBox txtGridLoanCard_SecretWord = grdLoanCards.Rows[e.RowIndex].FindControl("txtGridLoanCard_SecretWord") as TextBox;
            TextBox txtGridLoanCard_CardAmount = grdLoanCards.Rows[e.RowIndex].FindControl("txtGridLoanCard_CardAmount") as TextBox;


            if (txtGridLoanCard_Pan.Text.Length != 19)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Kart nömrəsini düzgün daxil edin!");
                return;
            }

            if (!_config.isDateCardExpiry(txtGridLoanCard_DateOfExpiry.Text))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Tarix formatını düzgün daxil edin!");
                return;
            }

            if (txtGridLoanCard_SecretWord.Text.Length < 1)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Açar sözü daxil edin!");
                return;
            }


            float cardAmount = 0;
            try
            {
                cardAmount = _config.ToFloat(txtGridLoanCard_CardAmount.Text);
            }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Məbləği düzgün daxil edin!");
                return;
            }


            wsLoanController.ModifyCardRequest req = new wsLoanController.ModifyCardRequest();
            req.CardID = cardID;

            req.Pan = txtGridLoanCard_Pan.Text.Trim();
            req.DateOfExpiry = txtGridLoanCard_DateOfExpiry.Text.Trim();
            req.CardAmount = cardAmount;
            req.SecretWord = txtGridLoanCard_SecretWord.Text;
            req.ModifiedID = UserSession.UserId;
            req.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);

            wsLoanController.ModifyCardResponse res = wsLController.ModifyCard(req);
            if (!res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi : " + res.errorCode);
                return;
            }

            grdLoanCards.EditIndex = -1;
            fillLoanCard();

        }

        protected void lnkDeleteCustomerData_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            hdnFieldDeleteLoanCardId.Value = lnk.CommandArgument;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCardDeleteAlertModal();", true);
        }

        protected void btnDeleteLoanCard_Click(object sender, EventArgs e)
        {
            wsLoanController.DeleteCardRequest req = new wsLoanController.DeleteCardRequest();
            req.CardID = Convert.ToInt32(hdnFieldDeleteLoanCardId.Value);
            wsLoanController.DeleteCardResponse res = wsLController.DeleteCard(req);
            if (!res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi : " + res.errorCode);
                return;
            }
            else
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Kart silindi.");
            }
            fillLoanCard();
        }



        protected void lnkNewCustomer_Click(object sender, EventArgs e)
        {
            _Config.Rd("/customer_new");

        }


        protected void btnSaveLoan_Click(object sender, EventArgs e)
        {
            if (drlCustomer.Items.Count == 0 || drlCustomer.SelectedIndex != 0)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Müştəri seçin!");
                return;
            }

            if (txtColleteralNo.Text.Trim().Length == 0)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Girov № daxil edin!");
                return;
            }

            if (txtACNo.Text.Trim().Length == 0)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Ac № daxil edin!");
                return;
            }

            if (!_config.isDate(txtStartDate.Text))
            {
                _config.AlertMessage(this, MessageType.ERROR, "İlk tarix daxil edin!");
                return;
            }
            if (!_config.isDate(txtPaymentDate.Text))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Ödəmə tarixi daxil edin!");
                return;
            }


            if (txtContractNo.Text.Trim().Length == 0)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Müqavilə № daxil edin!");
                return;
            }

            float _amount = 0;
            try { _amount = _config.ToFloat(txtAmount.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Məbləği düzgün daxil edin!");
                return;
            }


            float _interest = 0;
            try { _interest = _config.ToFloat(txtInterest.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Faizi(%) düzgün daxil edin!");
                return;
            }


            int _discountMonth = 0;
            try { _discountMonth = int.Parse(txtDiscountMonth.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Güzəşt (ay) düzgün daxil edin!");
                return;
            }

            int _loanMonth = 0;
            try { _loanMonth = int.Parse(txtLoanMonths.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Müddət (ay) düzgün daxil edin!");
                return;
            }

            if (drlAccountTypesList.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Hesab növü seçin!");
                return;
            }

            if (drlLCollateralTypes.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Girov növü seçin!");
                return;
            }


            if (drlBranch.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Filial seçin!");
                return;
            }

            if (drlLoanTypeList.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Təyinat seçin!");
                return;
            }

            float _feeAmount = 0;
            try { _feeAmount = _config.ToFloat(txtFeeAmount.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Komissiya düzgün daxil edin!");
                return;
            }

            if (drlQrafikType.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Qrafik seçin!");
                return;
            }


            int _day = 0;
            try { _day = int.Parse(txtDay.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Günü düzgün daxil edin!");
                return;
            }

            int _month = 0;
            try { _month = int.Parse(txtMonth.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Ayı düzgün daxil edin!");
                return;
            }


            int _dayOfDelay = 0;
            try { _dayOfDelay = int.Parse(txtDayOfDelay.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Gecikmə gününü düzgün daxil edin!");
                return;
            }

            float _monthInterest = 0;
            try { _monthInterest = _config.ToFloat(txtMonthInterest.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Aylıq faizi düzgün daxil edin!");
                return;
            }


            float _monthPayment = 0;
            try { _monthPayment = _config.ToFloat(txtMonthPayment.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Aylıq ödəməni düzgün daxil edin!");
                return;
            }


            float _restAmount = 0;
            try { _restAmount = _config.ToFloat(txtRestAmount.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Qalıq məbləği düzgün daxil edin!");
                return;
            }

            float _penya = 0;
            try { _penya = _config.ToFloat(txtPenya.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Penyanı düzgün daxil edin!");
                return;
            }

            float _currentDebt = 0;
            try { _currentDebt = _config.ToFloat(txtCurrentDebt.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Cari borcu düzgün daxil edin!");
                return;
            }


            float _interestAmount = 0;
            try { _interestAmount = _config.ToFloat(txtInterestAmount.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Faiz məbləğini düzgün daxil edin!");
                return;
            }


            float _debtAvans = 0;
            try { _debtAvans = _config.ToFloat(txtDebtAvans.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Borc / Avans düzgün daxil edin!");
                return;
            }

            float _sumAmount = 0;
            try { _sumAmount = _config.ToFloat(txtSumAmount.Text); }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Сəm məbləğ düzgün daxil edin!");
                return;
            }


            /* HtmlMeta meta = new HtmlMeta();
             meta.HttpEquiv = "Refresh";
             meta.Content = "2;url=/generalcredits";
             this.Page.Controls.Add(meta);*/

            wsLoanController.ModifyLoanRequestDTO request = new wsLoanController.ModifyLoanRequestDTO();
            request.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);
            request.BranchID = Convert.ToInt32(drlBranch.SelectedValue);
            request.CustomerID = Convert.ToInt32(drlCustomer.SelectedValue);
            request.LoanTypeID = Convert.ToInt32(drlLoanTypeList.SelectedValue);
            request.StartDate = txtStartDate.Text;
            request.PaymentDate = txtPaymentDate.Text;
            request.DateOfExpiry = DateTime.ParseExact(txtStartDate.Text, "dd.MM.yyyy", CultureInfo.InvariantCulture).AddMonths(Convert.ToInt32(txtLoanMonths.Text)).ToString("dd.MM.yyyy");
            request.CalculationDate = txtStartDate.Text;
            request.CollateralNumber = txtColleteralNo.Text;
            request.AgreementNumber = txtContractNo.Text;
            request.AcNumber = txtACNo.Text;
            request.LoanAmount = _amount;
            request.LoanInterest = _interest;
            request.LoanPeriod = Convert.ToInt32(txtLoanMonths.Text);
            request.DiscountPeriod = Convert.ToInt32(txtDiscountMonth.Text);
            request.CommissionFee = _feeAmount;
            request.LoanStatusID = Convert.ToInt32(drlLoanStatus.SelectedValue);
            request.CurrencyID = Convert.ToInt32(drlCurrency.SelectedValue);
            request.LoanMethodID = Convert.ToInt32(drlQrafikType.SelectedValue);
            request.DepositBox = "";
            request.CollateralReturnDate = "";
            request.JewelerCost = 0;
            request.AccountTypeID = Convert.ToInt32(drlAccountTypesList.SelectedValue);
            request.CollateralType = drlLCollateralTypes.SelectedValue;
            request.LenderID = 0;
            request.VerifierID = 0;
            request.ModifiedID = UserSession.UserId;


            wsLoanController.ModifyLoanResponse response = wsLController.ModifyLoan(request);
            if (!response.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + response.errorCode);
                return;
            }
            _config.AlertMessage(this, MessageType.SUCCESS, "Yadda saxlanıldı...");
            isLoanActive(Convert.ToInt32(hdnFieldLoanId.Value));

        }

        protected void lnkAddLoanNote_Click(object sender, EventArgs e)
        {
            if (txtAddLoanNote.Text.Trim() == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Qeydi daxil edin");
                return;
            }
            wsLoanController.AddLoanNoteRequest req = new wsLoanController.AddLoanNoteRequest();
            req.CreatedID = UserSession.UserId;
            req.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);
            req.Note = txtAddLoanNote.Text.Trim();

            wsLoanController.AddLoanNoteResponse res = wsLController.AddLoanNote(req);
            if (res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Yadda saxlanıldı");
                txtAddLoanNote.Text = "";
                fillLoanNotes();
                return;
            }
            _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
        }


        // delete loan note modal
        protected void lnkDeleteLoanNote_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            hdnFieldDeleteLoanNoteId.Value = lnk.CommandArgument;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openNoteDeleteAlertModal();", true);
        }

        // delete loan note
        protected void btnDeleteLoanNote_Click(object sender, EventArgs e)
        {
            int loanNoteId = Convert.ToInt32(hdnFieldDeleteLoanNoteId.Value);
            wsLoanController.DeleteLoanNoteRequest req = new wsLoanController.DeleteLoanNoteRequest();
            req.NoteID = loanNoteId;
            req.ModifiedId = UserSession.UserId;
            wsLoanController.DeleteLoanNoteResponse res = wsLController.DeleteLoanNote(req);
            if (res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Qeyd silindi");
                fillLoanNotes();
                return;
            }
            _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
        }


        protected void lnkAddLoanGuarantor_Click(object sender, EventArgs e)
        {
            hdnCustomerModalType.Value = "GUARANTOR";
            txtSearchCustomerName.Text = "";
            grdCustomers.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openSearchCustomerModal();", true);
        }


        // delete loan guarantor modal

        protected void lnkDeleteLoanGuarantor_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            hdnFieldDeleteLoanGuarantorId.Value = lnk.CommandArgument;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openGuarantorDeleteAlertModal();", true);
        }

        // delete loan guarantor modal
        protected void btnDeleteLoanGuarantor_Click(object sender, EventArgs e)
        {
            int loanGuarantorId = Convert.ToInt32(hdnFieldDeleteLoanGuarantorId.Value);
            wsLoanController.DeleteGuarantorRequest req = new wsLoanController.DeleteGuarantorRequest();
            req.GuarantorID = loanGuarantorId;
            req.ModifiedID = UserSession.UserId;

            wsLoanController.DeleteGuarantorResponse res = wsLController.DeleteGuarantor(req);
            if (res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Zamin silindi");
                fillLoanGuarantor();
                return;
            }
            _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
        }

        // add loan document
        protected void lnkAddLoanDocument_Click(object sender, EventArgs e)
        {
            string fileName = "";
            if (!flUploadDocument.HasFile)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Faylı seçin!");
                return;
            }
            string AllowTypesImage = "-jpg-jpeg-png-pdf-doc-docx-";
            string ImageType = System.IO.Path.GetExtension(flUploadDocument.PostedFile.FileName).Trim('.').ToLower();
            if (AllowTypesImage.IndexOf("-" + ImageType + "-") < 0)
            {
                _config.AlertMessage(this, MessageType.ERROR, "*.jpg , *.jpeg , *.png , *.pdf, *.doc, *.docx  tiplərindən birini seçin!");
                return;
            }

            if (flUploadDocument.PostedFile.ContentLength / 1024 > 10240)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Maksimum 10 mb fayl yükləməyə icazə verilir.");
                return;
            }

            Guid g_id = Guid.NewGuid();
            string ID = g_id.ToString();
            fileName = Server.MapPath("~/UploadDocuments/LoanDocument/") + ID.ToString().Trim() + "_" + System.IO.Path.GetFileName(flUploadDocument.FileName.Trim());

            flUploadDocument.SaveAs(fileName);

            wsLoanController.AddLoanDocumentRequest req = new wsLoanController.AddLoanDocumentRequest();
            req.CreatedID = UserSession.UserId;
            req.FileName = ID.ToString().Trim() + "_" + System.IO.Path.GetFileName(flUploadDocument.FileName.Trim());
            req.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);
            req.Note = drlAddLoanDocumentNote.SelectedItem.Text;
            wsLoanController.AddLoanDocumentResponse res = wsLController.AddLoanDocument(req);
            if (res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Sənəd əlavə edildi");
                fillLoanDocuments();
                return;
            }
            _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);

        }

        // download loan document
        protected void lnkDownloadLoanDocument_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            string filePath = Server.MapPath("~/UploadDocuments/LoanDocument/") + lnk.CommandArgument;
            if (File.Exists(filePath))
            {
                Response.Clear();
                Response.ContentType = "application/octet-stream";
                Response.AppendHeader("content-disposition", "filename=" + Path.GetFileName(filePath));
                Response.WriteFile(filePath);
                Response.Flush();
                Response.End();
            }
        }

        // delete loan document link
        protected void lnkDeleteLoanDocument_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            hdnFieldDeleteLoanDocumentId.Value = lnk.CommandArgument;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDocumentDeleteAlertModal();", true);
        }


        // delete loan document
        protected void btnDeleteLoanDocument_Click(object sender, EventArgs e)
        {
            int loanDocumentId = Convert.ToInt32(hdnFieldDeleteLoanDocumentId.Value);
            wsLoanController.DeleteLoanDocumentRequest req = new wsLoanController.DeleteLoanDocumentRequest();
            req.LoanDocumentID = loanDocumentId;
            req.ModifiedID = UserSession.UserId;

            wsLoanController.DeleteLoanDocumentResponse res = wsLController.DeleteLoanDocument(req);
            if (res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Sənəd silindi");
                fillLoanDocuments();
                return;
            }
            _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
        }

        protected void btnSaveLender_Click(object sender, EventArgs e)
        {
            if (drlLender.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Kredit verən şəxsi seçin!");
                return;
            }
            wsLoanController.setLoanLenderRequest req = new wsLoanController.setLoanLenderRequest();
            req.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);
            req.LenderID = Convert.ToInt32(drlLender.SelectedValue);
            req.ModifiedID = UserSession.UserId;
            wsLoanController.SetLoanLenderResponse res = wsLController.SetLoanLender(req);
            if (res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Yadda saxlanıldı.");
                fillLoanLender_Verifycator();
                return;
            }
            _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
        }

        /*
        protected void btnSaveVerifycator_Click(object sender, EventArgs e)
        {
            if (drlVerifycator.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Krediti təsdiq edən şəxsi seçin!");
                return;
            }

            wsLoanController.setLoanVerifierRequest req = new wsLoanController.setLoanVerifierRequest();
            req.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);
            req.VerifierID = Convert.ToInt32(drlVerifycator.SelectedValue);
            req.ModifiedID = UserSession.UserId;
            wsLoanController.SetLoanVerifierResponse res = wsLController.SetLoanVerifier(req);
            if (res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Yadda saxlanıldı.");
                fillLoanLender_Verifycator();
                return;
            }
            _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
        }
        */

        // collateral save general data
        protected void btnSaveLoanCollateralGeneralData_Click(object sender, EventArgs e)
        {
            wsLoanController.ModifyLoanCollateralRequest req = new wsLoanController.ModifyLoanCollateralRequest();

            try { req.JewelerCost = _config.ToFloat(txtLoanCollateralJewelerCost.Text); }
            catch { req.JewelerCost = 0; }

            req.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);
            req.DepositBox = txtLoanCollateralDepositBox.Text;
            req.ModifiedID = UserSession.UserId;
            req.JewelerName = txtLoanCollateralJewelerName.Text;
            wsLoanController.ModifyLoanCollateralResponse res = wsLController.ModifyLoanCollateral(req);
            if (res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Yadda saxlanıldı.");
                return;
            }
            _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
        }


        //open collateral modal
        protected void lnkNewLoanCollateral_Click(object sender, EventArgs e)
        {
            hdnCollateralOperType.Value = "INSERT";
            hdnCollateralTransactionType.Value = "IN";
            ltrModalCollateralInOutHeaderLabel.Text = "Yeni girov";
            ClearCollateralModal();
            drlLoanColleteralType.SelectedValue = "1"; // selected gold by default
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
        }

        // working on the collateral
        protected void btnAcceptCollateral_Click(object sender, EventArgs e)
        {
            //INSERT or UPDATE operation
            string operType = hdnCollateralOperType.Value;

            string tranType = hdnCollateralTransactionType.Value;

            if (drlLoanColleteralType.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Girov tipini seçin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
                return;
            }
            if (txtLoanCollateralName.Text.Trim().Length < 3)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Girov adını daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
                return;
            }
            if (drlLoanCollateralUnitOfMeasurement.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Ölçü vahidini seçin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
                return;
            }
            if (drlLoanColleteralType.SelectedValue == "1") // if gold
            {
                if (drlLoanCollateralGoldCarat.SelectedValue == "0")
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Əyar seçin!");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
                    return;
                }
            }
            float _price = 0;
            try
            {
                _price = _config.ToFloat(txtCollateralPrice.Text);
            }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Qiymət daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
                return;
            }

            int _count = 0;
            try
            {
                _count = Convert.ToInt32(txtCollateralCount.Text);
            }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Say daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
                return;
            }

            float _txtCollateralBruttoWeight = 0;
            try
            {
                _txtCollateralBruttoWeight = _config.ToFloat(txtCollateralBruttoWeight.Text);
            }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Ümumi çəkini daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
                return;
            }

            float _txtCollateralWeight = 0;
            try
            {
                _txtCollateralWeight = _config.ToFloat(txtCollateralWeight.Text);
            }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Daş çəkini daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
                return;
            }

            if (_txtCollateralBruttoWeight - _txtCollateralWeight <= 0)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Çəkiləri daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
                return;
            }

            if (_txtCollateralBruttoWeight <= 0)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Ümumi çəkini düzgün daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
                return;
            }

            if (_txtCollateralWeight < 0)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Daş çəkini düzgün daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
                return;
            }


            if (operType == "INSERT")
            {
                wsLoanController.AddCollateralRequest req = new wsLoanController.AddCollateralRequest();
                req.CollateralTypeID = Convert.ToInt32(drlLoanColleteralType.SelectedValue);
                req.CollateralName = txtLoanCollateralName.Text;
                req.UnitOfMeasurement = drlLoanCollateralUnitOfMeasurement.Text;
                req.CaratID = Convert.ToInt32(drlLoanCollateralGoldCarat.SelectedValue);
                req.Price = _price;
                req.Count = _count;
                req.BruttoWeight = _txtCollateralBruttoWeight;
                req.Weight = _txtCollateralWeight;
                req.Transaction = tranType;
                req.Note = txtCollateralNote.Text;
                req.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);
                req.CreatedID = UserSession.UserId;
                wsLoanController.AddCollateralResponse res = wsLController.AddCollateral(req);
                if (res.isSuccess)
                {
                    _config.AlertMessage(this, MessageType.SUCCESS, "Yadda saxlanıldı.");
                    ClearCollateralModal();
                }
                else
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
                    return;
                }
            }

            if (operType == "UPDATE")
            {
                wsLoanController.ModifyCollateralRequest req = new wsLoanController.ModifyCollateralRequest();
                req.CollateralID = Convert.ToInt32(hdnCollateralID.Value);
                req.CollateralTypeID = Convert.ToInt32(drlLoanColleteralType.SelectedValue);
                req.CollateralName = txtLoanCollateralName.Text;
                req.UnitOfMeasurement = drlLoanCollateralUnitOfMeasurement.Text;
                req.CaratID = Convert.ToInt32(drlLoanCollateralGoldCarat.SelectedValue);
                req.Price = _price;
                req.Count = _count;
                req.BruttoWeight = _txtCollateralBruttoWeight;
                req.Weight = _txtCollateralWeight;
                req.Note = txtCollateralNote.Text;
                req.ModifiedID = UserSession.UserId;
                wsLoanController.ModifyCollateralResponse res = wsLController.ModifyCollateral(req);
                if (res.isSuccess)
                {
                    _config.AlertMessage(this, MessageType.SUCCESS, "Yadda saxlanıldı.");
                    ClearCollateralModal();
                }
                else
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
                    return;
                }
            }



            fillLoanCollateral();
        }



        protected void drlLoanCollateralGoldCarat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drlLoanColleteralType.SelectedValue == "1") // if gold
            {
                DataTable dtGoldCarat = wsLController.GetGoldCaratList();

                var query = from r in dtGoldCarat.AsEnumerable()
                            where r.Field<int>("CaratID") == Convert.ToInt32(drlLoanCollateralGoldCarat.SelectedValue)
                            let objectArray = new object[]
                            {
                             r.Field<double>("CaratPrice")
                            }
                            select objectArray;

                DataTable newDataTable = new DataTable("CaratPrice");
                newDataTable.Columns.Add("CaratPrice", typeof(double));
                foreach (var array in query)
                {
                    newDataTable.Rows.Add(array);
                }
                txtCollateralPrice.Text = Convert.ToString(newDataTable.Rows[0]["CaratPrice"]);
            }
            else
            {
                txtCollateralPrice.Text = "";
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
        }


        void ClearCollateralModal()
        {
            drlLoanColleteralType.SelectedValue = "";
            txtLoanCollateralName.Text = "";
            drlLoanCollateralUnitOfMeasurement.SelectedValue = "";
            drlLoanCollateralGoldCarat.SelectedValue = "0";
            txtCollateralPrice.Text = "";
            txtCollateralCount.Text = "";
            txtCollateralBruttoWeight.Text = "1";
            txtCollateralWeight.Text = "0";
            txtCollateralNote.Text = "";
        }

        // open collateral modal
        protected void lnkEditLoanClollateral_Click(object sender, EventArgs e)
        {
            hdnCollateralOperType.Value = "UPDATE";
            ltrModalCollateralInOutHeaderLabel.Text = "Girovun dəyişdirilməsi";


            LinkButton lnk = (LinkButton)sender;
            int collateralId = Convert.ToInt32(lnk.CommandArgument);
            hdnCollateralID.Value = lnk.CommandArgument;

            DataTable dtCollateralRow = wsLController.GetCollateralByID(collateralId);
            if (dtCollateralRow != null)
            {
                if (dtCollateralRow.Rows.Count > 0)
                {
                    DataRow drCollateralRow = dtCollateralRow.Rows[0];

                    drlLoanColleteralType.SelectedValue = Convert.ToString(drCollateralRow["CollateralTypeID"]);
                    txtLoanCollateralName.Text = Convert.ToString(drCollateralRow["CollateralName"]);
                    drlLoanCollateralUnitOfMeasurement.SelectedValue = Convert.ToString(drCollateralRow["UnitOfMeasurement"]);
                    drlLoanCollateralGoldCarat.SelectedValue = Convert.ToString(drCollateralRow["CaratID"]); // 0
                    txtCollateralPrice.Text = Convert.ToString(drCollateralRow["Price"]);
                    txtCollateralCount.Text = Convert.ToString(drCollateralRow["Count"]);
                    txtCollateralBruttoWeight.Text = Convert.ToString(drCollateralRow["BruttoWeight"]);
                    txtCollateralWeight.Text = Convert.ToString(drCollateralRow["Weight"]);
                    txtCollateralNote.Text = Convert.ToString(drCollateralRow["Note"]);
                }
                else
                {
                    ClearCollateralModal();
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);
        }


        //out collateral modal
        protected void btnLoanCollateralSendOutOpenModal_Click(object sender, EventArgs e)
        {
            hdnCollateralTransactionType.Value = "OUT";
            hdnCollateralOperType.Value = "INSERT";
            ltrModalCollateralInOutHeaderLabel.Text = "Girovun təhvil verilməsi";

            int rowIndex = ((sender as Button).NamingContainer as GridViewRow).RowIndex;
            int collateralId = Convert.ToInt32(grdLoanCollaterals.DataKeys[rowIndex].Values["CollateralID"]);
            DataTable dtCollateralRow = wsLController.GetCollateralByID(collateralId);
            if (dtCollateralRow != null)
            {
                if (dtCollateralRow.Rows.Count > 0)
                {
                    DataRow drCollateralRow = dtCollateralRow.Rows[0];

                    drlLoanColleteralType.SelectedValue = Convert.ToString(drCollateralRow["CollateralTypeID"]);
                    txtLoanCollateralName.Text = Convert.ToString(drCollateralRow["CollateralName"]);
                    drlLoanCollateralUnitOfMeasurement.SelectedValue = Convert.ToString(drCollateralRow["UnitOfMeasurement"]);
                    drlLoanCollateralGoldCarat.SelectedValue = Convert.ToString(drCollateralRow["CaratID"]); // 0
                    txtCollateralPrice.Text = Convert.ToString(drCollateralRow["Price"]);
                    txtCollateralCount.Text = Convert.ToString(drCollateralRow["Count"]);
                    txtCollateralBruttoWeight.Text = Convert.ToString(drCollateralRow["BruttoWeight"]);
                    txtCollateralWeight.Text = Convert.ToString(drCollateralRow["Weight"]);
                    txtCollateralNote.Text = "";
                }
                else
                {
                    ClearCollateralModal();
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralInOutModal();", true);

        }


        //open modal delete collateral
        protected void lnkDeleteLoanClollateral_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            int collateralId = Convert.ToInt32(lnk.CommandArgument);
            hdnDeleteCollateralID.Value = lnk.CommandArgument;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralDeleteModal();", true);
        }

        //delete loan collateral by loan id
        protected void btnDeleteLoanCollateral_Click(object sender, EventArgs e)
        {
            wsLoanController.DeleteCollateralRequest req = new wsLoanController.DeleteCollateralRequest();
            req.CollateralID = Convert.ToInt32(hdnDeleteCollateralID.Value);
            req.ModifiedID = UserSession.UserId;
            wsLoanController.DeleteCollateralResponse res = wsLController.DeleteCollateral(req);
            if (res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Yadda saxlanıldı.");
                fillLoanCollateral();
                return;
            }
            _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
        }





        //open loan annuitet modal
        protected void btnLoanAnnuitetOpenModal_Click(object sender, EventArgs e)
        {
            DataTable dtAnnuitet = wsLController.GetPaymentScheduleByLoanID(Convert.ToInt32(hdnFieldLoanId.Value));
            grdLoanAnnuitet.DataSource = dtAnnuitet;
            grdLoanAnnuitet.DataBind();
            lnkLoanAnnuitetPrintTemplate.Visible = grdLoanAnnuitet.Rows.Count > 0;

            if (grdLoanAnnuitet.Rows.Count > 0)
            {
                GridViewRow drr = grdLoanAnnuitet.FooterRow;
                Label lblGridLoanAnnuitetPPMT_f = (Label)drr.FindControl("lblGridLoanAnnuitetPPMT_f");
                Label lblGridLoanAnnuitetIPMT_f = (Label)drr.FindControl("lblGridLoanAnnuitetIPMT_f");

                var PPMT_sum = dtAnnuitet.AsEnumerable()
                       .Sum(x => x.Field<decimal>("PPMT"));

                var IPMT_sum = dtAnnuitet.AsEnumerable()
                      .Sum(x => x.Field<decimal>("IPMT"));

                lblGridLoanAnnuitetPPMT_f.Text = PPMT_sum.ToString();
                lblGridLoanAnnuitetIPMT_f.Text = IPMT_sum.ToString();


            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openLoanAnnuitetModal();", true);
        }

        //open loan annuitet print
        protected void lnkLoanAnnuitetPrintTemplate_Click(object sender, EventArgs e)
        {
            string f_t_name = Server.MapPath(@"~/DocumentTemplates/templates/PaymentSchedule.docx");
            string f_o_name = Server.MapPath("~/DocumentTemplates/" + "PaymentSchedule_" + Guid.NewGuid().ToString() + ".docx");

            // copy file from template
            System.IO.File.Copy(f_t_name, f_o_name);

            bookMarks bookmarks = new bookMarks();

            DataTable dtLoanFullData = wsLController.GetLoanFullDatatByLoanID(Convert.ToInt32(hdnFieldLoanId.Value));
            if (dtLoanFullData == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }

            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(f_o_name);

            if (dtLoanFullData.Rows.Count > 0)
            {
                DataRow drLoanFullData = dtLoanFullData.Rows[0];

                //CompanyName
                bookmarks.key = "LombardNo";
                bookmarks.value = Convert.ToString(drLoanFullData["CompanyName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT,doc);

                //CustomerName
                bookmarks.key = "CustomerName";
                bookmarks.value = Convert.ToString(drLoanFullData["CustomerName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //PrintDate
                bookmarks.key = "PrintDate";
                bookmarks.value = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //LoanAmount
                bookmarks.key = "LoanAmount";
                bookmarks.value = Convert.ToString(drLoanFullData["LoanAmount"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //LoanAmountLetter
                bookmarks.key = "LoanAmountLetter";
                bookmarks.value = _config.ConvertMoneyToWord(Convert.ToString(drLoanFullData["LoanAmount"]));
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //LoanPeriod
                bookmarks.key = "LoanPeriod";
                bookmarks.value = Convert.ToString(drLoanFullData["LoanPeriod"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //LoanPeriodLetter
                bookmarks.key = "LoanPeriodLetter";
                bookmarks.value = _config.ConvertMonthToWord(Convert.ToString(drLoanFullData["LoanPeriod"])); 
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


                //StartDate
                bookmarks.key = "StartDate";
                bookmarks.value = Convert.ToString(drLoanFullData["StartDate"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchName
                bookmarks.key = "BranchName";
                bookmarks.value = Convert.ToString(drLoanFullData["BranchName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //DirectorName
                bookmarks.key = "DirectorName";
                bookmarks.value = Convert.ToString(drLoanFullData["DirectorName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchTaxID
                bookmarks.key = "BranchTaxID";
                bookmarks.value = Convert.ToString(drLoanFullData["BranchTaxID"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CustomerName
                bookmarks.key = "CustomerName1";
                bookmarks.value = Convert.ToString(drLoanFullData["CustomerName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


                //IdCardSeries
                bookmarks.key = "IdCardSeries";
                bookmarks.value = Convert.ToString(drLoanFullData["IdCardSeries"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //IdCardNumber
                bookmarks.key = "IdCardNumber";
                bookmarks.value = Convert.ToString(drLoanFullData["IdCardNumber"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //DateOfIssue
                bookmarks.key = "DateOfIssue";
                bookmarks.value = Convert.ToString(drLoanFullData["DateOfIssue"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT,doc);

                //Authority
                bookmarks.key = "Authority";
                bookmarks.value = Convert.ToString(drLoanFullData["Authority"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //vCurrency
                bookmarks.key = "vCurrency";
                bookmarks.value = Convert.ToString(drLoanFullData["CurrencyName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


                DataTable dtAnnuitet = wsLController.GetPaymentScheduleByLoanID(Convert.ToInt32(hdnFieldLoanId.Value)); 
                string paymentHtml = File.ReadAllText(Server.MapPath(@"~/DocumentTemplates/templates/PaymentScheduleTable.txt"));
                string paymentBodyTable = "";
                foreach (DataRow dr in dtAnnuitet.Rows)
                {
                    paymentBodyTable += $@"<tr><td>{Convert.ToString(dr["Period"])}</td><td>{Convert.ToString(dr["PaymentDate"])}</td><td>{Convert.ToString(dr["BeginingBalans"])}</td><td>{Convert.ToString(dr["PPMT"])}</td><td>{Convert.ToString(dr["IPMT"])}</td><td>{Convert.ToString(dr["MonthlyPayment"])}</td><td>{Convert.ToString(dr["EndingBalans"])}</td></tr>";
                }

                if (paymentBodyTable != "") //fill footer
                {
                    var PPMT_sum = dtAnnuitet.AsEnumerable()
                       .Sum(x => x.Field<decimal>("PPMT"));

                    var IPMT_sum = dtAnnuitet.AsEnumerable()
                          .Sum(x => x.Field<decimal>("IPMT"));

                    paymentBodyTable += $@"<tr><td>YEKUN</td><td></td><td></td><td>{PPMT_sum}</td><td>{IPMT_sum}</td><td></td><td></td></tr>";
                }
                string _paymentHtml = paymentHtml.Replace("{0}", paymentBodyTable);
                   

                //paymentTable
                bookmarks.key = "paymentTable";
                bookmarks.value = _paymentHtml;
                fillTemplateWord(bookmarks, WordBookmarkType.HTML, doc);

            }

            ((Microsoft.Office.Interop.Word._Document)doc).Close();
            ((Microsoft.Office.Interop.Word._Application)app).Quit();


            string pdf_file_name = f_o_name.Replace(".docx", ".pdf");

            Word2Pdf objWordPDF = new Word2Pdf();
            objWordPDF.InputLocation = f_o_name;
            objWordPDF.OutputLocation = pdf_file_name;
            string aa =  objWordPDF.Word2PdfCOnversion();


            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            response.AddHeader("Content-Disposition", "attachment;filename=" + System.IO.Path.GetFileName(pdf_file_name));
            response.WriteFile(pdf_file_name);
            response.Flush();
            System.IO.File.Delete(f_o_name);
            System.IO.File.Delete(pdf_file_name);
            response.End();
        }


        protected void btnLoanContractPrint_Click(object sender, EventArgs e)
        {
            string f_t_name = Server.MapPath(@"~/DocumentTemplates/templates/LoanContract.docx");
            string f_o_name = Server.MapPath("~/DocumentTemplates/" + "LoanContract_" + Guid.NewGuid().ToString() + ".docx");

            // copy file from template
            System.IO.File.Copy(f_t_name, f_o_name);

            bookMarks bookmarks = new bookMarks();

            DataTable dtLoanFullData = wsLController.GetLoanFullDatatByLoanID(Convert.ToInt32(hdnFieldLoanId.Value));
            if (dtLoanFullData == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }

            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(f_o_name);

            if (dtLoanFullData.Rows.Count > 0)
            {
                DataRow drLoanFullData = dtLoanFullData.Rows[0];

                //ContractNumber
                bookmarks.key = "ContractNumber";
                bookmarks.value = Convert.ToString(drLoanFullData["AgreementNumber"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //PrintDate
                bookmarks.key = "PrintDate";
                bookmarks.value = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //StartDate
                bookmarks.key = "StartDate";
                bookmarks.value = Convert.ToString(drLoanFullData["StartDate"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Owner
                bookmarks.key = "Owner";
                bookmarks.value = Convert.ToString(drLoanFullData["Owner"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


                //BranchTaxID
                bookmarks.key = "BranchTaxID";
                bookmarks.value = Convert.ToString(drLoanFullData["BranchTaxID"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //DirectorName
                bookmarks.key = "DirectorName";
                bookmarks.value = Convert.ToString(drLoanFullData["DirectorName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CustomerName
                bookmarks.key = "CustomerName";
                bookmarks.value = Convert.ToString(drLoanFullData["CustomerName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //IdCardSeries
                bookmarks.key = "IdCardSeries";
                bookmarks.value = Convert.ToString(drLoanFullData["IdCardSeries"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //IdCardNumber
                bookmarks.key = "IdCardNumber";
                bookmarks.value = Convert.ToString(drLoanFullData["IdCardNumber"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //LegalAddress
                bookmarks.key = "LegalAddress";
                bookmarks.value = Convert.ToString(drLoanFullData["LegalAddress"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Amount
                bookmarks.key = "Amount";
                bookmarks.value = _config.ToDecimal(Convert.ToString(drLoanFullData["LoanAmount"])).ToString("0.00");
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AmountText
                bookmarks.key = "AmountText";
                bookmarks.value = _config.ConvertMoneyToWord(Convert.ToString(drLoanFullData["LoanAmount"]));
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Currency
                bookmarks.key = "Currency";
                bookmarks.value = Convert.ToString(drLoanFullData["CurrencyName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Rate
                bookmarks.key = "Rate";
                bookmarks.value = Convert.ToString(drLoanFullData["LoanInterest"]).Trim(); 
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Amount1
                bookmarks.key = "Amount1";
                bookmarks.value = _config.ToDecimal(Convert.ToString(drLoanFullData["LoanAmount"])).ToString("0.00");
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AmountText
                bookmarks.key = "AmountText1";
                bookmarks.value = _config.ConvertMoneyToWord(Convert.ToString(drLoanFullData["LoanAmount"]));
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Currency1
                bookmarks.key = "Currency1";
                bookmarks.value = Convert.ToString(drLoanFullData["CurrencyName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //LoanPeriod
                bookmarks.key = "LoanPeriod";
                bookmarks.value = Convert.ToString(drLoanFullData["LoanPeriod"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //LoanPeriodText
                bookmarks.key = "LoanPeriodText";
                bookmarks.value = _config.ConvertMonthToWord(Convert.ToString(drLoanFullData["LoanPeriod"]));
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //DiscountPeriod
                bookmarks.key = "DiscountPeriod";
                bookmarks.value = Convert.ToString(drLoanFullData["DiscountPeriod"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


                //LoanTypeName
                bookmarks.key = "LoanTypeName";
                bookmarks.value = Convert.ToString(drLoanFullData["LoanTypeName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //DayStart
                bookmarks.key = "DayStart";
                bookmarks.value = Convert.ToString(drLoanFullData["StartDay"]); 
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //StartDate1
                bookmarks.key = "StartDate1";
                bookmarks.value = Convert.ToString(drLoanFullData["StartDate"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AcNumber
                bookmarks.key = "AcNumber";
                bookmarks.value = Convert.ToString(drLoanFullData["AcNumber"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AmountToUsd
                bookmarks.key = "AmountToUsd";
                bookmarks.value = (_config.ToDecimal(Convert.ToString(drLoanFullData["LoanAmount"])) / wsGController.getDailyCurrency("USD")).ToString("0.00");
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CustomerName1
                bookmarks.key = "CustomerName1";
                bookmarks.value = Convert.ToString(drLoanFullData["CustomerName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //DirectoName1
                bookmarks.key = "DirectoName1";
                bookmarks.value = Convert.ToString(drLoanFullData["DirectorName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CustomerName2
                bookmarks.key = "CustomerName2";
                bookmarks.value = Convert.ToString(drLoanFullData["CustomerName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchTaxID1
                bookmarks.key = "BranchTaxID1";
                bookmarks.value = Convert.ToString(drLoanFullData["BranchTaxID"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //IdCardSeries1
                bookmarks.key = "IdCardSeries1";
                bookmarks.value = Convert.ToString(drLoanFullData["IdCardSeries"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //IdCardNumber1
                bookmarks.key = "IdCardNumber1";
                bookmarks.value = Convert.ToString(drLoanFullData["IdCardNumber"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchAddress
                bookmarks.key = "BranchAddress";
                bookmarks.value = Convert.ToString(drLoanFullData["Adres"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //LegalAddress1
                bookmarks.key = "LegalAddress1";
                bookmarks.value = Convert.ToString(drLoanFullData["LegalAddress"]).Trim(); 
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //DateOfIssue
                bookmarks.key = "DateOfIssue";
                bookmarks.value = Convert.ToString(drLoanFullData["DateOfIssue"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            }

            ((Microsoft.Office.Interop.Word._Document)doc).Close(); 
            ((Microsoft.Office.Interop.Word._Application)app).Quit();


            string pdf_file_name = f_o_name.Replace(".docx", ".pdf");

            Word2Pdf objWordPDF = new Word2Pdf();
            objWordPDF.InputLocation = f_o_name;
            objWordPDF.OutputLocation = pdf_file_name;
            objWordPDF.Word2PdfCOnversion();

     
           System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
           response.Clear();
           response.Buffer = true;
           response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
           response.AddHeader("Content-Disposition", "attachment;filename=" + System.IO.Path.GetFileName(pdf_file_name));
           response.WriteFile(pdf_file_name);
           response.Flush();
           System.IO.File.Delete(f_o_name);
           System.IO.File.Delete(pdf_file_name);
           response.End();
      
            
           
        }




        protected void btnLoanIltizamPrint_Click(object sender, EventArgs e)
        {
            string f_t_name = Server.MapPath(@"~/DocumentTemplates/templates/ILTIZAM.docx");
            string f_o_name = Server.MapPath("~/DocumentTemplates/" + "ILTIZAM_" + Guid.NewGuid().ToString() + ".docx");

            // copy file from template
            System.IO.File.Copy(f_t_name, f_o_name);

            bookMarks bookmarks = new bookMarks();

            DataTable dtLoanFullData = wsLController.GetLoanFullDatatByLoanID(Convert.ToInt32(hdnFieldLoanId.Value));
            if (dtLoanFullData == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }

            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(f_o_name);

            if (dtLoanFullData.Rows.Count > 0)
            {
                DataRow drLoanFullData = dtLoanFullData.Rows[0];


                //CompanyName1
                bookmarks.key = "CompanyName1";
                bookmarks.value = Convert.ToString(drLoanFullData["CompanyName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //PrintDate
                bookmarks.key = "PrintDate";
                bookmarks.value = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchName
                bookmarks.key = "BranchName";
                bookmarks.value = Convert.ToString(drLoanFullData["BranchName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //LegalAddress
                bookmarks.key = "LegalAddress";
                bookmarks.value = Convert.ToString(drLoanFullData["LegalAddress"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CustomerName
                bookmarks.key = "CustomerName";
                bookmarks.value = Convert.ToString(drLoanFullData["CustomerName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //IdCardSeries
                bookmarks.key = "IdCardSeries";
                bookmarks.value = Convert.ToString(drLoanFullData["IdCardSeries"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //IdCardNumber
                bookmarks.key = "IdCardNumber";
                bookmarks.value = Convert.ToString(drLoanFullData["IdCardNumber"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


                //CustomerName1
                bookmarks.key = "CustomerName1";
                bookmarks.value = Convert.ToString(drLoanFullData["CustomerName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Owner
                bookmarks.key = "Owner";
                bookmarks.value = Convert.ToString(drLoanFullData["Owner"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


                //BranchTaxId
                bookmarks.key = "BranchTaxId";
                bookmarks.value = Convert.ToString(drLoanFullData["BranchTaxID"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CompanyName2
                bookmarks.key = "CompanyName2";
                bookmarks.value = Convert.ToString(drLoanFullData["CompanyName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


                //StartDate
                bookmarks.key = "StartDate";
                bookmarks.value = Convert.ToString(drLoanFullData["StartDate"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AgreementNumber
                bookmarks.key = "AgreementNumber";
                bookmarks.value = Convert.ToString(drLoanFullData["AgreementNumber"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CollateralNumber
                bookmarks.key = "CollateralNumber";
                bookmarks.value = Convert.ToString(drLoanFullData["CollateralNumber"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //LoanAmount
                bookmarks.key = "LoanAmount";
                bookmarks.value = _config.ToDecimal(Convert.ToString(drLoanFullData["LoanAmount"])).ToString("0.00");
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AmountLetter
                bookmarks.key = "AmountLetter";
                bookmarks.value = _config.ConvertMoneyToWord(Convert.ToString(drLoanFullData["LoanAmount"]));
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Currency
                bookmarks.key = "Currency";
                bookmarks.value = Convert.ToString(drLoanFullData["CurrencyName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //StartDate1
                bookmarks.key = "StartDate1";
                bookmarks.value = Convert.ToString(drLoanFullData["StartDate"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AcNumber
                bookmarks.key = "AcNumber";
                bookmarks.value = Convert.ToString(drLoanFullData["AcNumber"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


                decimal _amountToUsd = _config.ToDecimal(drLoanFullData["LoanAmount"]) / wsGController.getDailyCurrency("USD");
                //AmountToDollar
                bookmarks.key = "AmountToDollar";
                bookmarks.value = _amountToUsd.ToString("0.00");
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AmountToDollarLetter
                bookmarks.key = "AmountToDollarLetter";
                bookmarks.value = _config.ConvertMoneyToWord(_amountToUsd.ToString("0.00"));
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //StartDate2
                bookmarks.key = "StartDate2";
                bookmarks.value = Convert.ToString(drLoanFullData["StartDate"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //DirectorName
                bookmarks.key = "DirectorName";
                bookmarks.value = Convert.ToString(drLoanFullData["DirectorName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);
            }

            ((Microsoft.Office.Interop.Word._Document)doc).Close();
            ((Microsoft.Office.Interop.Word._Application)app).Quit();

            string pdf_file_name = f_o_name.Replace(".docx", ".pdf");

            Word2Pdf objWordPDF = new Word2Pdf();
            objWordPDF.InputLocation = f_o_name;
            objWordPDF.OutputLocation = pdf_file_name;
            objWordPDF.Word2PdfCOnversion();


            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            response.AddHeader("Content-Disposition", "attachment;filename=" + System.IO.Path.GetFileName(pdf_file_name));
            response.WriteFile(pdf_file_name);
            response.Flush();
            System.IO.File.Delete(pdf_file_name);
            System.IO.File.Delete(f_o_name);
            response.End();
        }


        // Girov akti
        protected void btnLoanCollateralAktPrint_Click(object sender, EventArgs e)
        {
            int loanId = Convert.ToInt32(hdnFieldLoanId.Value);
            DataTable dtCollateralList = wsLController.GetCollateralByLoanIDList(loanId);
            if (dtCollateralList.Rows.Count == 0)
            {
                _config.AlertMessage(this, MessageType.WARNING, "Girov daxil ediliməyib!");
                return;
            }

            string f_t_name = Server.MapPath(@"~/DocumentTemplates/templates/Girov_Akti.docx");
            string f_o_name = Server.MapPath("~/DocumentTemplates/" + "Girov_Akti_" + Guid.NewGuid().ToString() + ".docx");

            // copy file from template
            System.IO.File.Copy(f_t_name, f_o_name);

            bookMarks bookmarks = new bookMarks();

            DataTable dtLoanFullData = wsLController.GetLoanFullDatatByLoanID(Convert.ToInt32(hdnFieldLoanId.Value));
            if (dtLoanFullData == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }

            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(f_o_name);

            if (dtLoanFullData.Rows.Count > 0)
            {
                DataRow drLoanFullData = dtLoanFullData.Rows[0];


                //CompanyName
                bookmarks.key = "CompanyName";
                bookmarks.value = Convert.ToString(drLoanFullData["CompanyName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //PrintDate
                bookmarks.key = "PrintDate";
                bookmarks.value = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss");
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchName
                bookmarks.key = "BranchName";
                bookmarks.value = Convert.ToString(drLoanFullData["BranchName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CustomerName
                bookmarks.key = "CustomerName";
                bookmarks.value = Convert.ToString(drLoanFullData["CustomerName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AcNumber
                bookmarks.key = "AcNumber";
                bookmarks.value = Convert.ToString(drLoanFullData["AcNumber"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //DirectorName
                bookmarks.key = "DirectorName";
                bookmarks.value = Convert.ToString(drLoanFullData["DirectorName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //JewelerName
                bookmarks.key = "JewelerName";
                bookmarks.value = Convert.ToString(drLoanFullData["JewelerName"]).Trim();
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CustomerName1
                bookmarks.key = "CustomerName1";
                bookmarks.value = Convert.ToString(drLoanFullData["CustomerName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CustomerName2
                bookmarks.key = "CustomerName2";
                bookmarks.value = Convert.ToString(drLoanFullData["CustomerName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


                string collateralHtml = File.ReadAllText(Server.MapPath(@"~/DocumentTemplates/templates/CollateralAktTable.txt"));
                string collateralBodyTable = "";
                int pCount = 0;
                foreach (DataRow dr in dtCollateralList.Rows)
                {
                    pCount++;
                   // if (Convert.ToString(dr["Transaction"]) == "IN")
                  //  {
                        collateralBodyTable += $@"<tr><td>{Convert.ToString(pCount)}</td><td>{Convert.ToString(dr["CollateralName"])}</td><td>{Convert.ToString(dr["UnitOfMeasurement"])}</td><td>{Convert.ToString(dr["GoldCarat"])}</td><td>{Convert.ToString(dr["Price"])}</td><td>{Convert.ToString(dr["Count"])}</td><td>{Convert.ToString(dr["BruttoWeight"])}</td><td>{Convert.ToString(dr["Weight"])}</td><td>{Convert.ToString(dr["NettoWeight"])}</td><td>{Convert.ToString(dr["CollateralValue"])}</td></tr>";
                   // }
                }

                if (collateralBodyTable != "") //fill footer
                {
                    var Count_sum = dtCollateralList.AsEnumerable()
                      // .Where(r => r.Field<string>("Transaction") == "IN")
                       .Sum(x => x.Field<int>("Count"));

                    var brutto_sum = dtCollateralList.AsEnumerable()
                     // .Where(r => r.Field<string>("Transaction") == "IN")
                      .Sum(x => x.Field<double>("BruttoWeight"));

                    var weight_sum = dtCollateralList.AsEnumerable()
                    // .Where(r => r.Field<string>("Transaction") == "IN")
                     .Sum(x => x.Field<double>("Weight"));

                    var nettoweight_sum = dtCollateralList.AsEnumerable()
                    // .Where(r => r.Field<string>("Transaction") == "IN")
                     .Sum(x => x.Field<double>("NettoWeight"));

                    var CollateralValue_sum = dtCollateralList.AsEnumerable()
                   // .Where(r => r.Field<string>("Transaction") == "IN")
                    .Sum(x => x.Field<double>("CollateralValue"));

                    collateralBodyTable += $@"<tr><td>YEKUN</td><td></td><td></td><td></td><td></td><td>{Count_sum}</td><td>{brutto_sum}</td><td>{weight_sum}</td><td>{nettoweight_sum}</td><td>{CollateralValue_sum}</td></tr>";
                }
                string _collateralHtml = collateralHtml.Replace("{0}", collateralBodyTable);

                //collateral
                bookmarks.key = "collateralTable";
                bookmarks.value = _collateralHtml;
                fillTemplateWord(bookmarks, WordBookmarkType.HTML, doc);

            }

            ((Microsoft.Office.Interop.Word._Document)doc).Close();
            ((Microsoft.Office.Interop.Word._Application)app).Quit();

            string pdf_file_name = f_o_name.Replace(".docx", ".pdf");

            Word2Pdf objWordPDF = new Word2Pdf();
            objWordPDF.InputLocation = f_o_name;
            objWordPDF.OutputLocation = pdf_file_name;
            objWordPDF.Word2PdfCOnversion();


            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            response.AddHeader("Content-Disposition", "attachment;filename=" + System.IO.Path.GetFileName(pdf_file_name));
            response.WriteFile(pdf_file_name);
            response.Flush();
            System.IO.File.Delete(pdf_file_name);
            System.IO.File.Delete(f_o_name);
            response.End();
        }

        
        // Accept loan contract
        protected void btnAcceptLoan_Click(object sender, EventArgs e)
        {
            wsLoanController.acceptLoanRequest req = new wsLoanController.acceptLoanRequest();
            req.CreatedID = UserSession.UserId;
            req.Description = "Accept loan contract";
            req.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);
            wsLoanController.acceptLoanResponse res = wsLController.AcceptLoanContract(req);

            if (res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Təsdiq edildi .");
                isLoanAccepted(Convert.ToInt32(hdnFieldLoanId.Value));
                return;
            }
            _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
        }

        //open model out collateral 
        protected void btnInsertCollateralOutOpenModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openInsertCollateralOutModal();", true);
        }

        protected void btnInsertCollateralOut_Click(object sender, EventArgs e)
        {
            wsLoanController.insertCollateralOutRequest req = new wsLoanController.insertCollateralOutRequest();
            req.CreatedID = UserSession.UserId;
            req.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);
            wsLoanController.SetResponse res = wsLController.InsertCollateralOut(req);

            if (res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Çıxarış göndərildi .");
                isLoanAccepted(Convert.ToInt32(hdnFieldLoanId.Value));
                return;
            }
            _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
        }


        //open model return collateral  process
        protected void btnCollateralReturnProcessOpenModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openReturnCollateralProcessModal();", true);
        }

        protected void btnCollateralReturnProcess_Click(object sender, EventArgs e)
        {
            wsLoanController.CollateralReturnRequest req = new wsLoanController.CollateralReturnRequest();
            req.ModifiedID = UserSession.UserId;
            req.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);
            wsLoanController.SetResponse res = wsLController.CollateralRetrunProcess(req);

            if (res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Təhvil verildi.");
                isLoanAccepted(Convert.ToInt32(hdnFieldLoanId.Value));
                return;
            }
            _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
        }


        //open model collateral barcode  process
        protected void btnCollateralBarcodeOpenModal_Click(object sender, EventArgs e)
        {
            string _template = File.ReadAllText(Server.MapPath(@"~/_templates/CollateralBarCodeTemplate.html"));

            string tableRow = @"<tr style=""height: 20px;"">
                <td style=""width: 112px;border: 1px solid black;"" colspan=""2"">
                    <p>{0}</p>
                </td>
                <td style=""width: 52px;border: 1px solid black;text-align:center"">
                    <p>{1}</p>
                </td>
                <td style=""width: 36px;border: 1px solid black;text-align:center"">
                    <p>{2}</p>
                </td>
                <td style=""width: 45px;border: 1px solid black;text-align:center"">
                    <p>{3}</p>
                </td>
                <td style=""width: 363px;border: 1px solid black;text-align:center"" colspan=""2"">
                    <p>{4}</p>
                </td>
                <td style=""width: 52px;border: 1px solid black;text-align:center"">
                    <p>{5}</p>
                </td>
            </tr>";
            DataTable dtCollateralList = wsLController.BarCodeCollateralListByLoanID(Convert.ToInt32(hdnFieldLoanId.Value));
            if (dtCollateralList == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }
            if (dtCollateralList.Rows.Count == 0)
            {
                _config.AlertMessage(this, MessageType.WARNING, "Kreditə aid girov tapılmadı.");
                return;
            }

            string tableContent = "";
            int _count = 0;
            double _bruttoWeight = 0;
            double _weight = 0;
            double _collateralValue = 0;
            foreach (DataRow dr in dtCollateralList.Rows)
            {
                _count += Convert.ToInt32(dr["Count"]);
                _bruttoWeight += _config.ToDouble(Convert.ToString(dr["BruttoWeight"]));
                _weight += _config.ToDouble(Convert.ToString(dr["Weight"]));
                _collateralValue += _config.ToDouble(Convert.ToString(dr["CollateralValue"]));
                tableContent += string.Format(tableRow, dr["CollateralName"], dr["GoldCarat"], dr["Count"], dr["BruttoWeight"], dr["Weight"], dr["CollateralValue"]);
            }

            DataRow _drMainData = dtCollateralList.Rows[0];

            string tableFooter = @"<tr style=""height: 20px;"">
                    <td style=""width: 112px;border: 1px solid black;"" colspan=""2"">
                        <p>Yekun Cəm:</p>
                    </td>
                    <td style=""width: 52px;border: 1px solid black;text-align:center"">
                        <p><strong>&nbsp;</strong></p>
                    </td>
                    <td style=""width: 36px;border: 1px solid black;text-align:center"">
                        <p><strong>{0}</strong></p>
                    </td>
                    <td style=""width: 45px;border: 1px solid black;text-align:center"">
                        <p><strong>{1}</strong></p>
                    </td>
                    <td style=""width: 363px;border: 1px solid black;text-align:center"" colspan=""2"">
                        <p><strong>{2}</strong></p>
                    </td>
                    <td style=""width: 52px;border: 1px solid black;text-align:center"">
                        <p><strong>{3}</strong></p>
                    </td>
                </tr>";
            string tableFooterContent = string.Format(tableFooter, _count, _bruttoWeight, _weight, _collateralValue);

            string _Content = string.Format(_template,_drMainData["BranchName1"],_drMainData["StartDate"],_drMainData["FullName"],_drMainData["CollateralNumber"], _drMainData["CollateralNumber"], _drMainData["LoanAmount"], tableContent, tableFooterContent);
            _Content = @"<style>
                @font-face {
                    font-family: 'CustomBarCodeFont';
                    src: url('../_templates/barcode_fonts/Code39r.ttf') format('truetype');
                }
                @media print {
                    #Header, #Footer { display: none !important; }
                }
            </style>" + _Content;

            ltrCollateralListForBarcode.Text = _Content;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralBarcodeModal();", true);
        }


        //print barcode
        protected void lnkPrintCollateralBarcode_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "PrintBarcodeElem();", true);
        }

        //open model delete loan
        protected void btnLoanDeleteOpenModal_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDeleteLoanModal();", true);
        }

        //delete loan process
        protected void btnDeleteLoan_Click(object sender, EventArgs e)
        {
            if (txtDeleteLoanDescription.Text.Trim().Length < 2)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Silinmə səbəbini qeyd edin");
                return;
            }
            wsLoanController.DeleteLoanByIDRequest req = new wsLoanController.DeleteLoanByIDRequest();
            req.ModifiedID = UserSession.UserId;
            req.LoanID = Convert.ToInt32(hdnFieldLoanId.Value);
            req.Description = txtDeleteLoanDescription.Text.Trim(); ;
            wsLoanController.SetResponse res = wsLController.DeleteLoanByID(req);

            if (res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Kredit silindi.");
                isLoanAccepted(Convert.ToInt32(hdnFieldLoanId.Value));
                return;
            }
            _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
        }
















        // Begin word template

        public static string SaveToTemporaryFile(string html)
        {
            string htmlTempFilePath = Path.Combine(Path.GetTempPath(), string.Format("{0}.html", Path.GetRandomFileName()));
            using (StreamWriter writer = File.CreateText(htmlTempFilePath))
            {
                html = string.Format("<html>{0}</html>", html);

                writer.WriteLine(html);
            }

            return htmlTempFilePath;
        }

        enum WordBookmarkType
        {
            HTML,
            TXT
        }


        private void ReplaceBookmarkText(Microsoft.Office.Interop.Word.Document doc, string bookmarkName, string text, WordBookmarkType type)
        {
            try
            {
                if (doc.Bookmarks.Exists(bookmarkName))
                {

                    Object name = bookmarkName;
                    Microsoft.Office.Interop.Word.Range range =
                        doc.Bookmarks.get_Item(ref name).Range;

                    range.Text = range.Text.Replace(range.Text, text);
                    object newRange = range;
                    if (type == WordBookmarkType.HTML)
                    {
                        range.InsertFile(SaveToTemporaryFile(text), Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                    }
                    doc.Bookmarks.Add(bookmarkName, ref newRange);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void fillTemplateWord(bookMarks bookmarks, WordBookmarkType wordBookmarkType, Microsoft.Office.Interop.Word.Document doc)
        {
            try
            {
                ReplaceBookmarkText(doc, bookmarks.key.ToString(), bookmarks.value.ToString(), wordBookmarkType);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

      

        // End word template





    }

    public class bookMarks
    {
        public object key { get; set; }
        public object value { get; set; }
    }
}