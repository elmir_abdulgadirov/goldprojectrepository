﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code._Config;
using static MCS.App_Code.Enums;

namespace MCS.Cust_curr_acnt
{
    public partial class Default :  Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsCustomerController.CustomerController wsCController = new wsCustomerController.CustomerController();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();
        MCS.wsLoanController.LoanController wsLController = new MCS.wsLoanController.LoanController();
        MCS.wsUserController.UserController wsUController = new wsUserController.UserController();
        MCS.wsAccountController.AccountController wsAController = new MCS.wsAccountController.AccountController();

        _Config _config = new _Config();
        int pageIndex = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                fillData();

                hdnGridCustAccountsPageIndex.Value = "0";
                pageIndex = Convert.ToInt32(hdnGridCustAccountsPageIndex.Value);
                FillAccounts(pageIndex, ""); 
            }

            #region Begin Permission
            CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.CUSTOMER_CURR_ACNT_LIST));
            lnkNewAccount.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.CREATE_NEW_CURR_ACNT));
            if (grdCurrAcnts.Rows.Count > 0)
            {
                grdCurrAcnts.Columns[4].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.DELETE_CURR_ACNT));
            }
            #endregion
        }


        void fillData()
        {
            DataTable dtCurrency = wsGController.CurrencyList();
            if (dtCurrency.Rows.Count > 0)
            {
                drlCurrency.Items.Clear();
                drlCurrency.DataTextField = "CurrencyName";
                drlCurrency.DataValueField = "CurrencyID";
                drlCurrency.DataSource = dtCurrency;
                drlCurrency.DataBind();

                drlCurrency.SelectedValue = "1";
            }
        }

        protected void linkCustomerSearchModalOpen_Click(object sender, EventArgs e)
        {
            grdCustomers.Visible = false;
            txtSearchCustomerName.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openSearchCustomerModal();", true);
            drlCustomer.Items.Clear();
        }

        protected void lnkNewCustomer_Click(object sender, EventArgs e)
        {
            _Config.Rd("/customer_new");
        }

        void FillCustomer()
        {
            wsCustomerController.GetCustomerRequest reqCustomer = new wsCustomerController.GetCustomerRequest();
            reqCustomer.pageIndex = 1;
            reqCustomer.pageSize = Convert.ToInt32(_config.GetAppSetting("GridPageSize"));
            reqCustomer.Fullname = txtSearchCustomerName.Text.Trim() == "" ? "xxxxx" : txtSearchCustomerName.Text.Trim();
            reqCustomer.Pin = "";
            reqCustomer.IdCardNumber = "";
            reqCustomer.userID = UserSession.UserId;
            int recordSize;
            DataTable dtCustomer = wsCController.GetCustomers(reqCustomer, out recordSize);
            grdCustomers.DataSource = dtCustomer;
            grdCustomers.DataBind();
            grdCustomers.Visible = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openSearchCustomerModal();", true);
        }

        protected void btnSearchCustomer_Click(object sender, EventArgs e)
        {
            FillCustomer();
        }

        protected void lnkSelectCustomerData_Click(object sender, EventArgs e)
        {
            LinkButton lnkSelectCustomerData = (LinkButton)sender;
            string customerId = lnkSelectCustomerData.CommandArgument;
            int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
            string fullname = Convert.ToString(grdCustomers.DataKeys[rowIndex].Values["Fullname"]);

            drlCustomer.Items.Clear();
            drlCustomer.Items.Add(new ListItem(fullname, customerId));
            drlCustomer.SelectedIndex = 0;
        }

        protected void grdCustomers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lb = e.Row.FindControl("lnkSelectCustomerData") as LinkButton;
                ScriptManager.GetCurrent(this).RegisterPostBackControl(lb);
            }
        }

        protected void lnkNewAccount_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 1;
        }

        protected void btnAddCurrAccount_Click(object sender, EventArgs e)
        {
            if (drlCustomer.Items.Count == 0 || drlCustomer.SelectedValue == "" || drlCustomer.SelectedValue == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Müştəri seçin");
                return;
            }
            wsAccountController.AddAccountRequest req = new wsAccountController.AddAccountRequest();
            req.AcntCurrencyID = Convert.ToInt32(drlCurrency.SelectedValue);
            req.AcntName = drlCustomer.SelectedItem.Text + " (Cari hesab)";
            req.CustomerID = Convert.ToInt32(drlCustomer.SelectedValue);
            req.CreatedID = UserSession.UserId;


            wsAccountController.AddAccountResponse res = wsAController.AddCustomerCurrentAccount(req);

            if (!res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi : " + res.errorCode);
                return;
            }
            _config.AlertMessage(this, MessageType.SUCCESS, "Yadda saxlanıldı");
            MultiView1.ActiveViewIndex = 0;
            FillAccounts(0, "");
            drlCustomer.Items.Clear();
            drlCurrency.SelectedValue = "1";
        }

        protected void btnAddCurrAccountCancel_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void grdCurrAcnts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            hdnGridCustAccountsPageIndex.Value =Convert.ToString(e.NewPageIndex);
            string accnt_name = ""; // search companent

            FillAccounts(Convert.ToInt32(hdnGridCustAccountsPageIndex.Value), accnt_name);
        }

        void FillAccounts(int pageIndex, string acntName)
        {
            wsAccountController.GetCustAcntsRequest req = new wsAccountController.GetCustAcntsRequest();
            req.pageIndex = (pageIndex + 1);
            req.pageSize = Convert.ToInt32(_config.GetAppSetting("GridPageSize"));
            req.AcntName = acntName;
            req.UserId = UserSession.UserId;
            int RecordSize = 0;
            DataTable dtAccounts = wsAController.GetCustomerCurrAcnts(req,out RecordSize);
            if (dtAccounts == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }

            grdCurrAcnts.VirtualItemCount = RecordSize;
            grdCurrAcnts.PageIndex = pageIndex;
            grdCurrAcnts.PageSize = Convert.ToInt32(_config.GetAppSetting("GridPageSize"));
            grdCurrAcnts.DataSource = dtAccounts;
            grdCurrAcnts.DataBind();
        }

        protected void lnkSearchCustAcnt_Click(object sender, EventArgs e)
        {
            txtSearchAcntCustomerName.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openSearchModal();", true);
        }

        protected void btnSearchCustomerAcnt_Click(object sender, EventArgs e)
        {
            string _searchname = txtSearchAcntCustomerName.Text;
           
            pageIndex = 0;
            hdnGridCustAccountsPageIndex.Value = "0";
            FillAccounts(pageIndex, _searchname);
        }

        protected void lnkDeleteCustomerCurrAcnt_Click(object sender, EventArgs e)
        {
            ltrAlertModalHeaderLabel.Text = "<i class=\"fa fa-trash\"></i>&nbspHesabın silinməsi</h4>";
            ltrAlertModalBodyLabel.Text = "Hesab silinsin?";
            LinkButton lnk = (LinkButton)sender;
            hdnCustCurrAcntID.Value = lnk.CommandArgument;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }

        protected void btnDeleteCustCurrAcnt_Click(object sender, EventArgs e)
        {
            wsAccountController.DeleteAccountRequest  req = new wsAccountController.DeleteAccountRequest();
            req.AcntID = Convert.ToInt32(hdnCustCurrAcntID.Value);
            req.ModifiedID = UserSession.UserId;
            wsAccountController.DeleteAccountResponse res = wsAController.DeletCurrentAccount(req);
            if (!res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
                return;
            }
            _config.AlertMessage(this, MessageType.SUCCESS, "Hesab silindi.");

            FillAccounts(Convert.ToInt32(hdnGridCustAccountsPageIndex.Value), "");
        }
    }
}