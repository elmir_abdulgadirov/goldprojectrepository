﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Cust_curr_acnt.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">CARİ HESABLAR</h3>
        </div>
        <div class="panel-body">
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <ul class="list-inline">
                        <li>
                            <asp:LinkButton ID="lnkNewAccount" runat="server" OnClick="lnkNewAccount_Click">
                               <i class="fa fa-plus-square-o"></i><span> Yeni hesab</span>
                            </asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="lnkSearchCustAcnt" runat="server" OnClick="lnkSearchCustAcnt_Click">
                                <i class="fa fa-search"></i><span> Axtarış</span>
                            </asp:LinkButton>
                        </li>
                    </ul>

                    <div class="mb40"></div>
                    <div class="mb40"></div>
                    <div class="table-responsive">
                        <asp:GridView class="table table-hover table-striped nomargin table_custom_border_top_gray" ID="grdCurrAcnts" runat="server"
                            DataKeyNames="ACNT_ID,CUSTOMER_ID"
                            AutoGenerateColumns="False" GridLines="None" AllowPaging="True" AllowCustomPaging="True"
                            OnPageIndexChanging="grdCurrAcnts_PageIndexChanging">
                            <Columns>
                                <asp:BoundField DataField="ACNT_CODE" HeaderText="HESAB NÖMRƏSİ" />
                                <asp:BoundField DataField="ACNT_NAME" HeaderText="ADI" />
                                <asp:BoundField DataField="CURRENT_BALANCE" HeaderText="BALANS" />
                                <asp:TemplateField HeaderText="VALYUTA">
                                    <ItemTemplate>
                                        <%#Config.getValute(Convert.ToInt32(Eval("ACNT_CURRENCY"))) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <ul class="table-options">
                                            <li>
                                                <asp:LinkButton ID="lnkDeleteCustomerCurrAcnt" OnClick="lnkDeleteCustomerCurrAcnt_Click" CommandArgument='<%# Eval("ACNT_ID") %>' title="Sil" runat="server"><i class="fa fa-trash"></i></asp:LinkButton>
                                            </li>
                                        </ul>
                                    </ItemTemplate>
                                    <ItemStyle Width="32px" HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="bg-dark" />
                            <PagerStyle CssClass="pagination-ys" />
                        </asp:GridView>
                    </div>
                    <asp:HiddenField ID="hdnGridCustAccountsPageIndex" runat="server" />
                    <asp:HiddenField ID="hdnCustCurrAcntID" runat="server" />


                </asp:View>
                <asp:View ID="View2" runat="server">
                    <div class="row">
                        <div class="col-sm-5">
                            Müştəri <span class="text-danger"></span>
                            <div class="input-group">
                                <asp:DropDownList ID="drlCustomer" class="form-control" runat="server">
                                </asp:DropDownList>
                                <span class="input-group-addon">
                                    <asp:LinkButton ID="linkCustomerSearchModalOpen" OnClick="linkCustomerSearchModalOpen_Click" runat="server" ToolTip="Müştəri axtarışı">
                                    <i class="glyphicon glyphicon-search"></i>
                                    </asp:LinkButton>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                Valyuta<span class="text-danger"></span>
                                <asp:DropDownList ID="drlCurrency" class="form-control" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="mb20"></div>
                    <div class="row">
                        <div class="btn-demo col-sm-3">
                            <asp:Button ID="btnAddCurrAccount" OnClick="btnAddCurrAccount_Click" runat="server" CssClass="btn btn-primary" Text="Yadda saxla" />
                            <asp:Button ID="btnAddCurrAccountCancel" OnClick="btnAddCurrAccountCancel_Click" runat="server" CssClass="btn btn-default" Text="İmtina et" />
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>

        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal bounceIn" id="modalCustomerSearch" data-backdrop="static">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalSearchCustomerLabel">
                                Müştəri axtarışı
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <asp:TextBox ID="txtSearchCustomerName" class="form-control" placeHolder="Müştəri adı..." runat="server"></asp:TextBox>
                                            <div class="input-group-btn">
                                                <img id="search_customer_loading" style="display: none" src="../img/loader1.gif" />
                                                <asp:Button ID="btnSearchCustomer" class="btn btn-primary" runat="server" Text="Axtarış"
                                                    OnClick="btnSearchCustomer_Click"
                                                    OnClientClick="this.style.display = 'none';
                                                  document.getElementById('search_customer_loading').style.display = '';" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="mb20"></div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <asp:GridView class="table table-bordered table-primary nomargin" ID="grdCustomers" runat="server" OnRowDataBound="grdCustomers_RowDataBound"
                                                DataKeyNames="CustomerID,DocumentNumber,Fullname"
                                                AutoGenerateColumns="False" GridLines="None">
                                                <Columns>
                                                    <asp:BoundField DataField="FullName" HeaderText="S.A.A" />
                                                    <asp:BoundField DataField="BranchName" HeaderText="FİLİAL" />
                                                    <asp:TemplateField HeaderText="SƏNƏD NÖMRƏSİ">
                                                        <HeaderTemplate>
                                                            SƏNƏD NÖMRƏSİ
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <%# Eval("IdCardSeries") + " " +  Eval("IdCardNumber") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkSelectCustomerData" OnClick="lnkSelectCustomerData_Click" CommandArgument='<%# Eval("CustomerID") %>' title="Seç" runat="server">Seç</asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="32px" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <table class="table nomargin">
                                                        <tr>
                                                            <td>Müştəri tapılmadı</td>
                                                            <td>
                                                                <asp:LinkButton ID="lnkNewCustomer" OnClick="lnkNewCustomer_Click" runat="server"><i class="fa fa-user-plus"></i><span> Yeni müştəri</span></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EmptyDataTemplate>

                                                <HeaderStyle CssClass="bg-blue" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSearchCustomer" />
            </Triggers>
        </asp:UpdatePanel>



        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal bounceIn" id="modalSearch" data-backdrop="static">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalSearchLabel">
                                Hesab axtarışı
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <asp:TextBox ID="txtSearchAcntCustomerName" class="form-control" placeHolder="Müştəri adı" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <img id="search_customer_acc_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnSearchCustomerAcnt" class="btn btn-primary" runat="server" Text="Axtarış"
                                    OnClick="btnSearchCustomerAcnt_Click"
                                    OnClientClick="this.style.display = 'none';
                                                  document.getElementById('search_customer_acc_loading').style.display = '';" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSearchCustomerAcnt" />
            </Triggers>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalAlert" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalAlertLabel">
                                    <asp:Literal ID="ltrAlertModalHeaderLabel" runat="server"></asp:Literal>
                            </div>
                            <div class="modal-body">
                                <p>
                                    <asp:Literal ID="ltrAlertModalBodyLabel" runat="server"></asp:Literal>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="delete_cust_curr_acnt_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnDeleteCustCurrAcnt" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClick="btnDeleteCustCurrAcnt_Click"
                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('delete_cust_curr_acnt_loading').style.display = '';
                                                           document.getElementById('btnDeleteCustCurrAcntCancel').style.display = 'none';" />
                                <button id="btnDeleteCustCurrAcntCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDeleteCustCurrAcnt" />
            </Triggers>
        </asp:UpdatePanel>




    </div>

    <script>
        function openSearchCustomerModal() {
            $('#modalCustomerSearch').modal({ show: true });
        }
        function openSearchModal() {
            $('#modalSearch').modal({ show: true });
        }
        function openAlertModal() {
            $('#modalAlert').modal({ show: true });
        }
    </script>

</asp:Content>
