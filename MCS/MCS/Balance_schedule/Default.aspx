﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Balance_schedule.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary-full" style="margin-bottom: 0;">
        <div class="panel-body" style="padding: 5px; min-height: 38px">
            <div style="float: left; width: 90%">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtEndDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList ID="drlBranch" class="form-control input-sm" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div style="float: right; text-align: right">
                <div class="row">
                    <div class="col-sm-12">
                        <img id="search_tran_data" style="display: none" src="../img/loader1.gif" />
                        <asp:Button ID="btnSearchTran" CssClass="btn btn-default btn-quirk btn-sm" Style="margin: 0" runat="server" Text="YENİLƏ"
                            OnClick="btnSearchTran_Click"
                            OnClientClick="this.style.display = 'none';
                                document.getElementById('search_tran_data').style.display = '';" />
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <ul class="list-inline" style="padding-left: 5px">
                    <li style="float: left">
                        <span class="label label-black">Məlumat sayı:
                            <asp:Literal ID="ltrDataCount" runat="server"></asp:Literal>
                        </span>
                    </li>
                    <li style="float: right">
                        <asp:LinkButton ID="lnkDownloadResult" runat="server">
                        <i class="glyphicon glyphicon-print"></i><span> Çap</span>
                        </asp:LinkButton>
                    </li>
                    <li style="clear: both"></li>
                </ul>
                <div class="mb40"></div>
                <div class="mb40"></div>
                <div class="table-responsive">
                    <asp:GridView class="table table-striped nomargin table_custom_border_top_gray"
                        ID="grdMainData" runat="server" Style="background-color: #ffffff !important;"
                        DataKeyNames="ACNT_CODE,BranchID"
                        AutoGenerateColumns="False" GridLines="None" ShowFooter="True">
                        <Columns>
                            <asp:TemplateField HeaderText="HESAB">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdAccountName" runat="server" Text='<%#Eval("ACNT_CODE") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="TƏSVİR">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdDescription" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DEBET">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdDebet" runat="server" Text='<%#Eval("DtBalance") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdDebetSum" runat="server" Text=""></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="110px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="CREDİT">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdCredit" runat="server" Text='<%#Eval("CtBalance") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdCreditSum" runat="server" Text=""></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="110px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDetails" CommandArgument='<%# Eval("BranchID") %>'
                                        OnClick="lnkDetails_Click"
                                        title="Ətraflı" runat="server"><span class="glyphicon glyphicon-chevron-right"></span></asp:LinkButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="45px" />
                            </asp:TemplateField>

                        </Columns>
                        <EmptyDataTemplate>
                            <i>Məlumat yoxdur...</i>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#D1D1D1" BorderColor="#520000" Font-Bold="True" ForeColor="Black" />
                        <HeaderStyle CssClass="bg-dark" />
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="modal bounceIn" id="modalDetails" data-backdrop="static">
                <div class="modal-dialog modal-lg" style="width: 1200px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4></h4>
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="list-inline" style="padding-left: 5px; padding-top: 10px">
                                        <li style="float: left">
                                            <span class="label label-black">Məlumat sayı:
                                              <asp:Literal ID="ltrDataCount_1" runat="server"></asp:Literal>
                                            </span>
                                        </li>
                                        <li style="float: right">
                                            <asp:LinkButton ID="lnkDownloadResult_1" runat="server">
                                             <i class="glyphicon glyphicon-print"></i><span> Çap</span>
                                            </asp:LinkButton>
                                        </li>
                                        <li style="clear: both"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-primary">
                                        <thead>
                                            <tr>
                                                <th>HESAB</th>
                                                <th>TƏSVİR</th>
                                                <th>
                                                    <div style="width: 200px; text-align: center">
                                                        <div style="width: 100%">Dövrün əvvəlinə</div>
                                                        <div style="width: 100%">
                                                            <div style="width: 50%; float: left">DEBET</div>
                                                            <div style="width: 50%; float: left">KREDİT</div>
                                                            <div style="clear: both"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th>
                                                    <div style="width: 200px; text-align: center">
                                                        <div style="width: 100%">Dövr ərzində</div>
                                                        <div style="width: 100%">
                                                            <div style="width: 50%; float: left">DEBET</div>
                                                            <div style="width: 50%; float: left">KREDİT</div>
                                                            <div style="clear: both"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th>
                                                    <div style="width: 200px; text-align: center">
                                                        <div style="width: 100%">Dövrün sonunda</div>
                                                        <div style="width: 100%">
                                                            <div style="width: 50%; float: left">DEBET</div>
                                                            <div style="width: 50%; float: left">KREDİT</div>
                                                            <div style="clear: both"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Literal ID="ltrHtmlData" runat="server"></asp:Literal>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lnkDownloadResult_1" />
        </Triggers>
    </asp:UpdatePanel>

    <script>
        function openModalDetails() {
            $('#modalDetails').modal({ show: true });
        }
    </script>

    <style>
        .input-sm {
            height: 26px !important;
            line-height: 26px !important;
        }


        .input-group-addon {
            padding: 5px 7px !important;
        }

        .form-group {
            margin-bottom: 10px;
        }

        .no_visible_font {
            color: #ffffff;
        }

        .label-black {
            background-color: #343a40;
        }

        .label {
            display: inline;
            padding: 0.6em 1em 0.6em;
            font-size: 80%;
            font-weight: bold !important;
            line-height: 1;
            color: #ffffff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25em;
        }
    </style>
</asp:Content>
