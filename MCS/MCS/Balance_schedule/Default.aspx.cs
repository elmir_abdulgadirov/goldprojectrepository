﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code._Config;

namespace MCS.Balance_schedule
{
    public partial class Default : System.Web.UI.Page
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();
        MCS.wsTransactionController.TransactionController wsTController = new wsTransactionController.TransactionController();
        MCS.wsAccountController.AccountController wsAController = new wsAccountController.AccountController();
        MCS.wsTemplateController.TemplateController wsTeController = new wsTemplateController.TemplateController();
        MCS.wsUserController.UserController wsUController = new wsUserController.UserController();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                fillData();
                txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                txtEndDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");

                if (drlBranch.SelectedValue != "0")
                {
                    wsAccountController.GetTotalBalanceRequest Req = new wsAccountController.GetTotalBalanceRequest();
                    Req.DateFrom = txtStartDate.Text;
                    Req.DateTo = txtEndDate.Text;
                    Req.BranchID = int.Parse(drlBranch.SelectedValue);
                    Req.UserID = UserSession.UserId;
                    ltrDataCount.Text = "0";
                    fillGrid(Req);
                    ViewState["DateFrom"] = null;
                    ViewState["DateTo"] = null;
                    ViewState["BranchID"] = null;
                }

            }
        }

        void fillData()
        {

            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial", "0"));
            }
        }


        void fillGrid(wsAccountController.GetTotalBalanceRequest Req)
        {
            if (!_config.isDate(Req.DateFrom))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Başlanğıc tarixi düzgün daxil edin!");
                return;
            }
            if (!_config.isDate(Req.DateTo))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Son tarixi düzgün daxil edin!");
                return;
            }
            if (drlBranch.SelectedValue == "0")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Filialı daxil edin!");
                return;
            }

            DataTable dtMainData = wsAController.GetAcntTotalBalanceByBranch(Req);

            if (dtMainData == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!");
                return;
            }

            grdMainData.DataSource = dtMainData;
            grdMainData.DataBind();

            if (grdMainData.Rows.Count > 0)
            {
                GridViewRow drr = grdMainData.FooterRow;
                Label lblGrdDebetSum = (Label)drr.FindControl("lblGrdDebetSum");
                Label lblGrdCreditSum = (Label)drr.FindControl("lblGrdCreditSum");

                double DebetSum = 0;
                double CreditSum = 0;

                for (int i = 0; i < grdMainData.Rows.Count; i++)
                {
                    DebetSum += Convert.ToDouble(((Label)grdMainData.Rows[i].FindControl("lblGrdDebet")).Text);
                    CreditSum += Convert.ToDouble(((Label)grdMainData.Rows[i].FindControl("lblGrdCredit")).Text);
                }
                lblGrdDebetSum.Text = DebetSum.ToString("0.00");
                lblGrdCreditSum.Text = CreditSum.ToString("0.00");
                ltrDataCount.Text = grdMainData.Rows.Count.ToString();
            }
        }

        protected void btnSearchTran_Click(object sender, EventArgs e)
        {
            wsAccountController.GetTotalBalanceRequest Req = new wsAccountController.GetTotalBalanceRequest();
            Req.DateFrom = txtStartDate.Text;
            Req.DateTo = txtEndDate.Text;
            Req.BranchID = int.Parse(drlBranch.SelectedValue);
            Req.UserID = UserSession.UserId;
            fillGrid(Req);
            ViewState["DateFrom"] = Req.DateFrom;
            ViewState["DateTo"] = Req.DateTo;
            ViewState["BranchID"] = Req.BranchID;
        }


        //open modal
        protected void lnkDetails_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            wsAccountController.GetAcntBalanceBySubSubCodeRequest req = new wsAccountController.GetAcntBalanceBySubSubCodeRequest();

            int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
            string ACNT_CODE = Convert.ToString(grdMainData.DataKeys[rowIndex].Values["ACNT_CODE"]);

            req.DateFrom = Convert.ToString(ViewState["DateFrom"]);
            req.DateTo = Convert.ToString(ViewState["DateTo"]);
            req.BranchID = Convert.ToInt32(ViewState["BranchID"]);
            req.AcntSubSubGroupCode = ACNT_CODE;
            req.UserID = Convert.ToInt32(ViewState["UserID"]);
            DataTable dtSub = wsAController.GetAcntBalanceBySubSubCode(req);
            if (dtSub == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }
            if (dtSub.Rows.Count == 0)
            {
                _config.AlertMessage(this, MessageType.WARNING, "Məlumat tapılmadı.");
                return;
            }



            ltrDataCount_1.Text = dtSub.ToString();
            string _htmlRows = "";
            double DtToPeriod = 0, CtToPeriod = 0, DtInPeriod = 0, CtInPeriod = 0, DtOutPeriod = 0, CtOutPeriod = 0;
            foreach (DataRow dr in dtSub.Rows)
            {
                DtToPeriod += _config.ToDouble(dr["DtToPeriod"]);
                CtToPeriod += _config.ToDouble(dr["CtToPeriod"]);
                DtInPeriod += _config.ToDouble(dr["DtInPeriod"]);
                CtInPeriod += _config.ToDouble(dr["CtInPeriod"]);
                DtOutPeriod += _config.ToDouble(dr["DtOutPeriod"]);
                CtOutPeriod += _config.ToDouble(dr["CtOutPeriod"]);

                _htmlRows += $@"<tr>
                    <td>" +  $@"{Convert.ToString(dr["Code"])}" +  $@"</td>
                    <td>" +  $@"{Convert.ToString(dr["Description"])}" +  $@"</td>
                    <td>
                          <div style=""width:200px;text-align:center"">
                            <div style=""width:100%"">
                                    <div style=""width:50%;float:left"">" +  dr["DtToPeriod"] +  $@"</div>
                                    <div style=""width:50%;float:left"">" +  dr["CtToPeriod"] +  $@"</div>
                                    <div style=""clear:both""></div>
                            </div>
                          </div>
                     </td>
                    <td>
                      <div style=""width:200px;text-align:center"">
                            <div style=""width:100%"">
                                    <div style=""width:50%;float:left"">" +  dr["DtInPeriod"] +  $@"</div>
                                    <div style=""width:50%;float:left"">" +  dr["CtInPeriod"] +  $@"</div>
                                    <div style=""clear:both""></div>
                            </div>
                       </div>
                    </td>
                    <td>
                       <div style=""width: 200px; text-align:center"">
                            <div style = ""width:100%"">
                                    <div style=""width:50%;float:left"">" +  dr["DtOutPeriod"] +  $@"</div>
                                    <div style=""width:50%;float:left"">" +  dr["CtOutPeriod"] +  $@"</div>
                                    <div style=""clear:both""></div>
                            </div>
                       </div>
                    </td>
                    </tr>";
            }

            _htmlRows += $@"<tr>
                    <td></td>
                    <td></td>
                    <td>
                          <div style=""width:200px;text-align:center"">
                            <div style=""width:100%"">
                                    <div style=""width:50%;float:left""><b>" + DtToPeriod.ToString("0.00") + $@"</b></div>
                                    <div style=""width:50%;float:left""><b>" + CtToPeriod.ToString("0.00") + $@"</b></div>
                                    <div style=""clear:both""></div>
                            </div>
                          </div>
                     </td>
                    <td>
                      <div style=""width:200px;text-align:center"">
                            <div style=""width:100%"">
                                    <div style=""width:50%;float:left""><b>" + DtInPeriod.ToString("0.00") + $@"</b></div>
                                    <div style=""width:50%;float:left""><b>" +CtInPeriod.ToString("0.00") + $@"</b></div>
                                    <div style=""clear:both""></div>
                            </div>
                       </div>
                    </td>
                    <td>
                       <div style=""width: 200px; text-align:center"">
                            <div style = ""width:100%"">
                                    <div style=""width:50%;float:left""><b>" + DtOutPeriod.ToString("0.00") + $@"</b></div>
                                    <div style=""width:50%;float:left""><b>" + CtOutPeriod.ToString("0.00") + $@"</b></div>
                                    <div style=""clear:both""></div>
                            </div>
                       </div>
                    </td>
                    </tr>";
            ltrHtmlData.Text = _htmlRows;
            ltrDataCount_1.Text = dtSub.Rows.Count.ToString();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalDetails();", true);
        }
    }
}