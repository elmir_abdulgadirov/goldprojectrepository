﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Collateral_general_list.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary-full" style="margin-bottom: 0;">
        <div class="panel-body" style="padding: 5px; min-height: 38px">
            <div style="float: left; width: 90%">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="input-group">
                            <asp:TextBox ID="txtStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <asp:TextBox ID="txtEndDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList ID="drlBranch" class="form-control input-sm" runat="server">
                        </asp:DropDownList>
                    </div>

                    <div class="col-sm-3">
                        <asp:DropDownList ID="drlCurrency" AutoPostBack="true" class="form-control input-sm" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div style="float: right; text-align: right">
                <div class="row">
                    <div class="col-sm-12">
                        <img id="search_collateral_data" style="display: none" src="../img/loader1.gif" />
                        <asp:Button ID="btnSearchCollateral" CssClass="btn btn-default btn-quirk btn-sm"
                            Style="margin: 0" runat="server" Text="YENİLƏ"
                            OnClick="btnSearchCollateral_Click"
                            OnClientClick="this.style.display = 'none';
                                document.getElementById('search_collateral_data').style.display = '';" />
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <ul class="list-inline" style="padding-left: 5px">
                    <li>
                        <asp:LinkButton ID="lnkDownloadResultModal" runat="server"
                            OnClick="lnkDownloadResultModal_Click">
                        <i class="glyphicon glyphicon-print"></i><span> Çap</span>
                        </asp:LinkButton>
                    </li>
                </ul>
                <div class="mb20"></div>
                <div class="table-responsive">
                    <asp:GridView class="table table-striped nomargin table_custom_border_top_gray"
                        ID="grdCollateralList" runat="server" Style="background-color: #ffffff !important;"
                        AutoGenerateColumns="False" GridLines="None" ShowFooter="True">
                        <Columns>
                            <asp:TemplateField HeaderText="FİLİAL">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdBranch" runat="server" Text='<%#Eval("BranchName") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="S.A.A">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdFullName" runat="server" Text='<%#Eval("FullName") + " - " + Eval("StartDate") + " - " + Eval("LoanAmount") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="QUTU №">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdDepositBox" runat="server" Text='<%#Eval("DepositBox") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="GİROV №">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdCollateralNumber" runat="server" Text='<%#Eval("CollateralNumber") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="GİROV DƏYƏRİ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdCollateralValue" runat="server" Text='<%#Eval("CollateralValue") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdCollateralValueSum" runat="server"></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="VALYUTA DƏYƏRİ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdCurrencyValue" runat="server" Text='<%#Eval("CurrencyValue") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdCurrencyValueSum" runat="server"></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ÜMÜMİ ÇƏKİ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdBruttoWeight" runat="server" Text='<%#Eval("BruttoWeight") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdBruttoWeightSum" runat="server"></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ÇIXILAN ÇƏKİ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdWeight" runat="server" Text='<%#Eval("Weight") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdWeightSum" runat="server"></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="XALİS ÇƏKİ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdNettoWeight" runat="server" Text='<%#Eval("NettoWeight") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdNettoWeightSum" runat="server"></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <i>Məlumat yoxdur...</i>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#D1D1D1" BorderColor="#520000" Font-Bold="True" ForeColor="Black" />
                        <HeaderStyle CssClass="bg-dark" />
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>


    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="modal bounceIn" id="modalCollateralSearch" data-backdrop="static">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalCollateralSearchLabel">
                                <i class="glyphicon glyphicon-print"></i>&nbspGirovların çapı
                            </h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="drlSearchBranch" class="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="drlSearchCurrency" class="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txtSearchDepositBox" autocomplete="off" class="form-control" placeHolder="Qutu №" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:LinkButton ID="lnkPrintSearchResult" runat="server"
                                OnClick ="lnkPrintSearchResult_Click"
                                CssClass ="btn btn-primary btn-quirk btn-stroke">
                             <i class="glyphicon glyphicon-print"></i><span> Çap et</span>
                            </asp:LinkButton>

                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lnkPrintSearchResult" />
        </Triggers>
    </asp:UpdatePanel>

    <style>
        .input-sm {
            height: 26px !important;
            line-height: 26px !important;
        }


        .input-group-addon {
            padding: 5px 7px !important;
        }

        .form-group {
            margin-bottom: 10px;
        }

        .no_visible_font {
            color: #ffffff;
        }
    </style>

    <script>
        function openCollateralSearchModal() {
            $('#modalCollateralSearch').modal({ show: true });
        }
    </script>

</asp:Content>
