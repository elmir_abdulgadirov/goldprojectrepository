﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code._Config;
using MCS.App_Code;
using System.Data;
using System.IO;
using WordToPDF;
using static MCS.App_Code.Enums;

namespace MCS.Collateral_general_list
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();
        MCS.wsLoanController.LoanController wsLController = new wsLoanController.LoanController();
        MCS.wsUserController.UserController wsUController = new wsUserController.UserController();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                fillHeaderData();
                wsLoanController.GetCollateralListRequest ReqGrid = new wsLoanController.GetCollateralListRequest();
                ReqGrid.UserID = UserSession.UserId;
                ReqGrid.StartDate = txtStartDate.Text;
                ReqGrid.EndDate = txtEndDate.Text;
                ReqGrid.CurrencyID = Convert.ToInt32(drlCurrency.SelectedValue);
                ReqGrid.BranchID = Convert.ToInt32(drlBranch.SelectedValue);
                fillGrid(ReqGrid);
            }

            #region Begin Permission
             CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.COLLATERAL_GENERAL_LIST));
             lnkDownloadResultModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.PRINT_COLLATERAL_LIST));
            #endregion

        }

        void fillHeaderData()
        {
            txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
            txtEndDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial", "0"));
            }

            drlSearchBranch.Items.Clear();
            drlSearchBranch.DataTextField = "BranchName";
            drlSearchBranch.DataValueField = "BranchID";
            drlSearchBranch.DataSource = dtBranch;
            drlSearchBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlSearchBranch.Items.Insert(0, new ListItem("Filial", "0"));
            }

            DataTable dtCurrency = wsGController.CurrencyList();
            if (dtCurrency.Rows.Count > 0)
            {
                drlCurrency.Items.Clear();
                drlCurrency.DataTextField = "CurrencyName";
                drlCurrency.DataValueField = "CurrencyID";
                drlCurrency.DataSource = dtCurrency;
                drlCurrency.DataBind();
                drlCurrency.SelectedValue = "2";

                drlSearchCurrency.Items.Clear();
                drlSearchCurrency.DataTextField = "CurrencyName";
                drlSearchCurrency.DataValueField = "CurrencyID";
                drlSearchCurrency.DataSource = dtCurrency;
                drlSearchCurrency.DataBind();
                drlSearchCurrency.SelectedValue = "2";
            }
        }

        void fillGrid(wsLoanController.GetCollateralListRequest Req)
        {
            if (!_config.isDate(Req.StartDate))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Başlanğıc tarixi düzgün daxil edin!");
                return;
            }
            if (!_config.isDate(Req.EndDate))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Son tarixi düzgün daxil edin!");
                return;
            }

            DataTable dtCollateralList = wsLController.GetCollateralList(Req);

            if (dtCollateralList == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!");
                return;
            }

            grdCollateralList.DataSource = dtCollateralList;
            grdCollateralList.DataBind();

            if (grdCollateralList.Rows.Count > 0)
            {
                GridViewRow drr = grdCollateralList.FooterRow;
                Label lblGrdCollateralValueSum = (Label)drr.FindControl("lblGrdCollateralValueSum");
                Label lblGrdCurrencyValueSum = (Label)drr.FindControl("lblGrdCurrencyValueSum");
                Label lblGrdBruttoWeightSum = (Label)drr.FindControl("lblGrdBruttoWeightSum");
                Label lblGrdWeightSum = (Label)drr.FindControl("lblGrdWeightSum");
                Label lblGrdNettoWeightSum = (Label)drr.FindControl("lblGrdNettoWeightSum");

                decimal CollateralValue = 0, CurrencyValue = 0, BruttoWeight = 0, Weight = 0, NettoWeight = 0;

                for (int i = 0; i < grdCollateralList.Rows.Count; i++)
                {
                    CollateralValue += _config.ToDecimal(((Label)grdCollateralList.Rows[i].FindControl("lblGrdCollateralValue")).Text);
                    CurrencyValue += _config.ToDecimal(((Label)grdCollateralList.Rows[i].FindControl("lblGrdCurrencyValue")).Text);
                    BruttoWeight += _config.ToDecimal(((Label)grdCollateralList.Rows[i].FindControl("lblGrdBruttoWeight")).Text);
                    Weight += _config.ToDecimal(((Label)grdCollateralList.Rows[i].FindControl("lblGrdWeight")).Text);
                    NettoWeight += _config.ToDecimal(((Label)grdCollateralList.Rows[i].FindControl("lblGrdNettoWeight")).Text);
                }
                lblGrdCollateralValueSum.Text = CollateralValue.ToString();
                lblGrdCurrencyValueSum.Text = CurrencyValue.ToString();
                lblGrdBruttoWeightSum.Text = BruttoWeight.ToString();
                lblGrdWeightSum.Text = Weight.ToString();
                lblGrdNettoWeightSum.Text = NettoWeight.ToString();
            }
        }

        protected void btnSearchCollateral_Click(object sender, EventArgs e)
        {
            wsLoanController.GetCollateralListRequest ReqGrid = new wsLoanController.GetCollateralListRequest();
            ReqGrid.UserID = UserSession.UserId;
            ReqGrid.StartDate = txtStartDate.Text;
            ReqGrid.EndDate = txtEndDate.Text;
            ReqGrid.CurrencyID = Convert.ToInt32(drlCurrency.SelectedValue);
            ReqGrid.BranchID = Convert.ToInt32(drlBranch.SelectedValue);
            fillGrid(ReqGrid);
        }

        //open print modal
        protected void lnkDownloadResultModal_Click(object sender, EventArgs e)
        {
            txtSearchDepositBox.Text = "";
            fillHeaderData();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralSearchModal();", true);
        }

        // print data
        protected void lnkPrintSearchResult_Click(object sender, EventArgs e)
        {
            if (drlSearchBranch.SelectedValue == "0")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Filial seçin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralSearchModal();", true);
                return;
            }
            if (txtSearchDepositBox.Text.Trim().Length < 2)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Qutu № daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralSearchModal();", true);
                return;
            }
            wsLoanController.GetTempCollateralListRequest reqPrint = new wsLoanController.GetTempCollateralListRequest();
            reqPrint.BranchID = Convert.ToInt32(drlSearchBranch.SelectedValue);
            reqPrint.CurrencyID = Convert.ToInt32(drlSearchCurrency.SelectedValue);
            reqPrint.DepositBox = txtSearchDepositBox.Text.Trim();
            DataTable dtTempCollateral = wsLController.GetTempCollateralList(reqPrint);
            if (dtTempCollateral == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralSearchModal();", true);
                return;
            }
            if (dtTempCollateral.Rows.Count == 0)
            {
                _config.AlertMessage(this, MessageType.WARNING, "Məlumat tapılmadı.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCollateralSearchModal();", true);
                return;
            }

            string f_t_name = Server.MapPath(@"~/DocumentTemplates/templates/GetTempCollateralList.docx");
            string f_o_name = Server.MapPath("~/DocumentTemplates/" + "GetTempCollateralList" + Guid.NewGuid().ToString() + ".docx");

            // copy file from template
            System.IO.File.Copy(f_t_name, f_o_name);

            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(f_o_name);

            bookMarks bookmarks = new bookMarks();

            //StartDate
            bookmarks.key = "Date";
            bookmarks.value = _Config.HostingTime.ToString("dd.MM.yyyy HH:mm");
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //DepositBox
            bookmarks.key = "DepositBox";
            bookmarks.value = reqPrint.DepositBox;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //table html to bookmark
            string GetTempCollateralListDataTableHtml = File.ReadAllText(Server.MapPath(@"~/DocumentTemplates/templates/GetTempCollateralListDataTable.txt"));
            string GetTempCollateralListDataTableHtmlBody = "";
            int _rowCount = 0;
            decimal C_333 = 0, C_375 = 0, C_417 = 0, C_583 = 0, C_585 = 0, C_625 = 0, C_750 = 0, C_834 = 0, C_916 = 0, C_999 = 0;
            decimal TotalBruttoWeight = 0, CollateralValue = 0, CurrencyValue = 0;

            foreach (DataRow dr in dtTempCollateral.Rows)
            {
                _rowCount++;
                GetTempCollateralListDataTableHtmlBody += $@"<tr>
                    <td>{_rowCount}</td>
                    <td>{Convert.ToString(dr["CollateralNumber"])}</td>
                    <td>{Convert.ToString(dr["C_333"])}</td>
                    <td>{Convert.ToString(dr["C_375"])}</td>
                    <td>{Convert.ToString(dr["C_417"])}</td>
                    <td>{Convert.ToString(dr["C_583"])}</td>
                    <td>{Convert.ToString(dr["C_585"])}</td>
                    <td>{Convert.ToString(dr["C_625"])}</td>
                    <td>{Convert.ToString(dr["C_750"])}</td>
                    <td>{Convert.ToString(dr["C_834"])}</td>
                    <td>{Convert.ToString(dr["C_916"])}</td>
                    <td>{Convert.ToString(dr["C_999"])}</td>
                    <td>{Convert.ToString(dr["TotalBruttoWeight"])}</td>
                    <td>{Convert.ToString(dr["CollateralValue"])}</td>
                    <td>{Convert.ToString(dr["CurrencyValue"])}</td>
                    <td>{Convert.ToString(dr["TehvilVerdi"])}</td>
                    <td>{Convert.ToString(dr["TehvilAldi"])}</td>
                    <td>{Convert.ToString(dr["Tarix"])}</td>
                    </tr>";

                C_333 += Convert.ToString(dr["C_333"]) == "" ? 0 : _config.ToDecimal(Convert.ToString(dr["C_333"]));
                C_375 += Convert.ToString(dr["C_375"]) == "" ? 0 : _config.ToDecimal(Convert.ToString(dr["C_375"]));
                C_417 += Convert.ToString(dr["C_417"]) == "" ? 0 : _config.ToDecimal(Convert.ToString(dr["C_417"]));
                C_583 += Convert.ToString(dr["C_583"]) == "" ? 0 : _config.ToDecimal(Convert.ToString(dr["C_583"]));
                C_585 += Convert.ToString(dr["C_585"]) == "" ? 0 : _config.ToDecimal(Convert.ToString(dr["C_585"]));
                C_625 += Convert.ToString(dr["C_625"]) == "" ? 0 : _config.ToDecimal(Convert.ToString(dr["C_625"]));
                C_750 += Convert.ToString(dr["C_750"]) == "" ? 0 : _config.ToDecimal(Convert.ToString(dr["C_750"]));
                C_834 += Convert.ToString(dr["C_834"]) == "" ? 0 : _config.ToDecimal(Convert.ToString(dr["C_834"]));
                C_916 += Convert.ToString(dr["C_916"]) == "" ? 0 : _config.ToDecimal(Convert.ToString(dr["C_916"]));
                C_999 += Convert.ToString(dr["C_999"]) == "" ? 0 : _config.ToDecimal(Convert.ToString(dr["C_999"]));
                TotalBruttoWeight += Convert.ToString(dr["TotalBruttoWeight"]) == "" ? 0 : _config.ToDecimal(Convert.ToString(dr["TotalBruttoWeight"]));
                CollateralValue += Convert.ToString(dr["CollateralValue"]) == "" ? 0 : _config.ToDecimal(Convert.ToString(dr["CollateralValue"]));
                CurrencyValue += Convert.ToString(dr["CurrencyValue"]) == "" ? 0 : _config.ToDecimal(Convert.ToString(dr["CurrencyValue"]));
            }

            if (GetTempCollateralListDataTableHtmlBody != "") //fill footer
            {
                GetTempCollateralListDataTableHtmlBody += $@"<tr>
                    <td></td>
                    <td></td>
                    <td>{C_333}</td>
                    <td>{C_375}</td>
                    <td>{C_417}</td>
                    <td>{C_583}</td>
                    <td>{C_585}</td>
                    <td>{C_625}</td>
                    <td>{C_750}</td>
                    <td>{C_834}</td>
                    <td>{C_916}</td>
                    <td>{C_999}</td>
                    <td>{TotalBruttoWeight}</td>
                    <td>{CollateralValue}</td>
                    <td>{CurrencyValue}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>";
            }

            string _GetTempCollateralListDataTableHtml = GetTempCollateralListDataTableHtml
                                                .Replace("{0}", GetTempCollateralListDataTableHtmlBody);

            string _htmlGeneral = @"<head><meta charset=""UTF-8""><style>table, td, th {  border: 1px solid black; text-align: left;font-size:14px;}table {border-collapse: collapse;} </style></head><body>{0}</body>";

            _htmlGeneral = _htmlGeneral.Replace("{0}", _GetTempCollateralListDataTableHtml);

            bookmarks.key = "pDatatTable";
            bookmarks.value = _htmlGeneral;
            fillTemplateWord(bookmarks, WordBookmarkType.HTML, doc);


            ((Microsoft.Office.Interop.Word._Document)doc).Close();
            ((Microsoft.Office.Interop.Word._Application)app).Quit();

            string pdf_file_name = f_o_name.Replace(".docx", ".pdf");

            Word2Pdf objWordPDF = new Word2Pdf();
            objWordPDF.InputLocation = f_o_name;
            objWordPDF.OutputLocation = pdf_file_name;
            objWordPDF.Word2PdfCOnversion();

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            response.AddHeader("Content-Disposition", "attachment;filename=" + System.IO.Path.GetFileName(pdf_file_name));
            response.WriteFile(pdf_file_name);
            response.Flush();
            System.IO.File.Delete(f_o_name);
            System.IO.File.Delete(pdf_file_name);
            response.End();
        }


        // Begin word template

        public static string SaveToTemporaryFile(string html)
        {
            string htmlTempFilePath = Path.Combine(Path.GetTempPath(), string.Format("{0}.html", Path.GetRandomFileName()));
            using (StreamWriter writer = File.CreateText(htmlTempFilePath))
            {
                html = string.Format("<html>{0}</html>", html);
                writer.WriteLine(html);
            }

            return htmlTempFilePath;
        }

        enum WordBookmarkType
        {
            HTML,
            TXT
        }


        private void ReplaceBookmarkText(Microsoft.Office.Interop.Word.Document doc, string bookmarkName, string text, WordBookmarkType type)
        {
            try
            {
                if (doc.Bookmarks.Exists(bookmarkName))
                {
                    Object name = bookmarkName;
                    Microsoft.Office.Interop.Word.Range range = doc.Bookmarks.get_Item(ref name).Range;
                    range.Text = range.Text.Replace(range.Text, text);
                    object newRange = range;
                    if (type == WordBookmarkType.HTML)
                    {
                        range.InsertFile(SaveToTemporaryFile(text), Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    }
                    doc.Bookmarks.Add(bookmarkName, ref newRange);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }



        private void fillTemplateWord(bookMarks bookmarks, WordBookmarkType wordBookmarkType, Microsoft.Office.Interop.Word.Document doc)
        {
            try
            {
                ReplaceBookmarkText(doc, bookmarks.key.ToString(), bookmarks.value.ToString(), wordBookmarkType);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }




        // End word template


    }

    public class bookMarks
    {
        public object key { get; set; }
        public object value { get; set; }
    }
}