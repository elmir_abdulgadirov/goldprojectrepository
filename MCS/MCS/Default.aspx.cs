﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code._Config;

namespace MCS
{
    public partial class Default : System.Web.UI.Page
    {
        _Config _config = new _Config();
        wsUserController.UserController wsUserController = new MCS.wsUserController.UserController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserSession"] != null) _Config.Rd("/index");
                ImgSecurity.ImageUrl = "~/Captcha/?" + new Random().Next().ToString();
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtLogin.Text.Length < 3)
            {
                _config.AlertMessage(this, MessageType.ERROR, "İstifadəçi adını daxil edin!");
                txtLogin.Focus();
                return;
            }

            if (txtPassword.Text.Length < 5)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Şifrəni daxil edin!");
                txtPassword.Focus();
                return;
            }

            string security_code = txtSecurity.Text;
            txtSecurity.Text = "";
            ImgSecurity.ImageUrl = "~/Captcha/?" + new Random().Next().ToString();

            string Er = "";
            if (security_code.Length < 1) Er = "*";
            if (_config.Cs(Session["SecurityKod"]).Length < 1) Er = "*";
            if (_config.Cs(Session["SecurityKod"]) != security_code) Er = "*";

            if (Er == "*")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Təhlükəsizlik kodu düzgün deyil.");
                return;
            }
            MCS.wsUserController.UserLoginRequest LoginRequest = new wsUserController.UserLoginRequest();
            LoginRequest.UserLogin = txtLogin.Text;
            LoginRequest.UserPassword = txtPassword.Text;

            MCS.wsUserController.UserLoginResponse response = wsUserController.UserLogin(LoginRequest);
            if (!response.IsLogin)
            {
                if (response.ErrorCode.Equals("OPERATION_DATE_NOT_ACTIVE"))
                {
                    if (response.UserPermission == "SUPER_ADMIN")
                    {
                        Session["UserSession"] = response;
                        _Config.Rd("/index");
                    }
                    else
                    {
                        _config.AlertMessage(this, MessageType.WARNING, "Əməliyyat günü aktiv deyil!");
                        txtLogin.Focus();
                        return;
                    }
                    
                }
                _config.AlertMessage(this, MessageType.ERROR, "İstifadəçi adı və ya şifrə düzgün deyil!");
                txtLogin.Focus();
                return;
            }
            Session["UserSession"] = response;
            _Config.Rd("/index");




        }
    }
}