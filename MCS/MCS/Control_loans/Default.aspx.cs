﻿using MCS.App_Code;
using MCS.wsLoanController;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WordToPDF;
using static MCS.App_Code._Config;
using static MCS.App_Code.Enums;

namespace MCS.Control_loans
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();
        MCS.wsLoanController.LoanController wsLController = new wsLoanController.LoanController();
        MCS.wsUserController.UserController wsUController = new wsUserController.UserController();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                txtEndDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                fillHeaderData();
                wsLoanController.GetControlLoansListRequest Req = new wsLoanController.GetControlLoansListRequest();
                Req.BranchID = int.Parse(drlBranch.SelectedValue);
                Req.CollateralNumber = txtCollateralNumber.Text;
                Req.Fullname = txtFullname.Text;
                Req.CurrencyID = int.Parse(drlCurrency.SelectedValue);
                Req.DateStart = txtStartDate.Text;
                Req.DateEnd = txtEndDate.Text;
                Req.UserID = UserSession.UserId;
                fillGrid(Req, false);
            }

            #region Begin Permission
            CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.CREDIT_CONTROL));
            if (grdLoanData.Rows.Count > 0)
            {
                grdLoanData.Columns[9].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.CHANGE_TO_ACTIVE_STATUS));
            }
            lnkDownloadResult.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.PRINT_CONTROL_LIST));
            #endregion
        }

        void fillHeaderData()
        {
            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial", "0"));
            }

            DataTable dtCurrency = wsGController.CurrencyList();
            if (dtCurrency.Rows.Count > 0)
            {
                drlCurrency.Items.Clear();
                drlCurrency.DataTextField = "CurrencyName";
                drlCurrency.DataValueField = "CurrencyID";
                drlCurrency.DataSource = dtCurrency;
                drlCurrency.DataBind();
                drlCurrency.SelectedValue = "1";
            }

            txtFullname.Text = "";
            txtCollateralNumber.Text = "";
        }

        void fillGrid(wsLoanController.GetControlLoansListRequest Req, bool isPrint)
        {
            if (!_config.isDate(Req.DateStart))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Başlanğıc tarixi düzgün daxil edin!");
                return;
            }
            if (!_config.isDate(Req.DateEnd))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Son tarixi düzgün daxil edin!");
                return;
            }
            DataTable dtLoanList = wsLController.GetControlLoansList(Req);

            if (dtLoanList == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!");
                return;
            }

            grdLoanData.DataSource = dtLoanList;
            grdLoanData.DataBind();

            ltrDataCount.Text = dtLoanList.Rows.Count.ToString();

            if (grdLoanData.Rows.Count > 0)
            {
                GridViewRow drr = grdLoanData.FooterRow;
                Label lblGrdTotalPrincipalDebtSum = (Label)drr.FindControl("lblGrdTotalPrincipalDebtSum");
                Label lblGrdTotalInterestDebtSum = (Label)drr.FindControl("lblGrdTotalInterestDebtSum");
                Label lblGrdPenaltyDebtSum = (Label)drr.FindControl("lblGrdPenaltyDebtSum");
                Label lblGrdTotalDebtSum = (Label)drr.FindControl("lblGrdTotalDebtSum");

                double TotalPrincipalDebtSum = 0;
                double TotalInterestDebtSum = 0;
                double PenaltyDebtSum = 0;
                double TotalDebtSum = 0;

                for (int i = 0; i < grdLoanData.Rows.Count; i++)
                {
                    TotalPrincipalDebtSum += Convert.ToDouble(((Label)grdLoanData.Rows[i].FindControl("lblGrdTotalPrincipalDebt")).Text);
                    TotalInterestDebtSum += Convert.ToDouble(((Label)grdLoanData.Rows[i].FindControl("lblGrdTotalInterestDebt")).Text);
                    PenaltyDebtSum += Convert.ToDouble(((Label)grdLoanData.Rows[i].FindControl("lblGrdPenaltyDebt")).Text);
                    TotalDebtSum += Convert.ToDouble(((Label)grdLoanData.Rows[i].FindControl("lblGrdTotalDebt")).Text);
                }

                lblGrdTotalPrincipalDebtSum.Text = TotalPrincipalDebtSum.ToString("0.00");
                lblGrdTotalInterestDebtSum.Text = TotalInterestDebtSum.ToString("0.00");
                lblGrdPenaltyDebtSum.Text = PenaltyDebtSum.ToString("0.00");
                lblGrdTotalDebtSum.Text = TotalDebtSum.ToString("0.00");


                if (isPrint)
                {
                    PrintData(dtLoanList);
                }
            }
        }

        void PrintData(DataTable dtLoanList)
        {
            // print datatable
            string f_t_name = Server.MapPath(@"~/DocumentTemplates/templates/GetControlLoansList.docx");
            string f_o_name = Server.MapPath("~/DocumentTemplates/" + "GetControlLoansList" + Guid.NewGuid().ToString() + ".docx");

            // copy file from template
            System.IO.File.Copy(f_t_name, f_o_name);

            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(f_o_name);

            bookMarks bookmarks = new bookMarks();

            if (drlBranch.SelectedValue == "0")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Filialı seçin");
                return;
            }

            //BranchName
            bookmarks.key = "BranchName";
            bookmarks.value = drlBranch.SelectedItem.Text;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //PrintDate
            bookmarks.key = "PrintDate";
            bookmarks.value = _Config.HostingTime.ToString("dd.MM.yyyy HH:mm");
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            string GetControlLoansListDataTableHtml = File.ReadAllText(Server.MapPath(@"~/DocumentTemplates/templates/GetControlLoansListDataTable.txt"));
            string GetControlLoansListDataTableHtmlBody = "";

            decimal TotalPrincipalDebt = 0, TotalInterestDebt = 0, PenaltyDebt = 0, TotalDebt = 0;

            foreach (DataRow dr in dtLoanList.Rows)
            {
                GetControlLoansListDataTableHtmlBody += $@"<tr>
                    <td>{Convert.ToString(dr["LoanCount"])}</td>
                    <td>{Convert.ToString(dr["BranchName"])}</td>
                    <td>{Convert.ToString(dr["Description"])}</td>
                    <td>{Convert.ToString(dr["CollateralNumber"])}</td>
                    <td>{Convert.ToString(dr["TotalPrincipalDebt"])}</td>
                    <td>{Convert.ToString(dr["TotalInterestDebt"])}</td>
                    <td>{Convert.ToString(dr["PenaltyDebt"])}</td>
                    <td>{Convert.ToString(dr["TotalDebt"])}</td>
                    <td>{Convert.ToString(dr["PaymentDate"])}</td>
                    <td>{Convert.ToString(dr["DaysOfRespite"])}</td>
                    <td>{Convert.ToString(dr["MobilePhone1"]) + " " + Convert.ToString(dr["MobilePhone2"])}</td>
                    </tr>";

                TotalPrincipalDebt += _config.ToDecimal(dr["TotalPrincipalDebt"]);
                TotalInterestDebt += _config.ToDecimal(dr["TotalInterestDebt"]);
                PenaltyDebt += _config.ToDecimal(dr["PenaltyDebt"]);
                TotalDebt += _config.ToDecimal(dr["TotalDebt"]);
            }
            if (GetControlLoansListDataTableHtmlBody != "") //fill footer
            {
                GetControlLoansListDataTableHtmlBody += $@"<tr>
                                                      <td>Say : " + dtLoanList.Rows.Count + $@"</td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td>{TotalPrincipalDebt}</td>
                                                      <td>{TotalInterestDebt}</td>
                                                      <td>{PenaltyDebt}</td>
                                                      <td>{TotalDebt}</td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      </tr>";
            }
            string _GetControlLoansListDataTableHtml = GetControlLoansListDataTableHtml
                                             .Replace("{0}", GetControlLoansListDataTableHtmlBody);

            string _htmlGeneral = "<head><style>table, td, th {  border: 1px solid black; text-align: left;font-size:14px;}table {border-collapse: collapse;} </style></head><body>{0}</body>";

            _htmlGeneral = _htmlGeneral.Replace("{0}", _GetControlLoansListDataTableHtml);

            bookmarks.key = "DataTable";
            bookmarks.value = _htmlGeneral;
            fillTemplateWord(bookmarks, WordBookmarkType.HTML, doc);


            ((Microsoft.Office.Interop.Word._Document)doc).Close();
            ((Microsoft.Office.Interop.Word._Application)app).Quit();

            string pdf_file_name = f_o_name.Replace(".docx", ".pdf");

            Word2Pdf objWordPDF = new Word2Pdf();
            objWordPDF.InputLocation = f_o_name;
            objWordPDF.OutputLocation = pdf_file_name;
            objWordPDF.Word2PdfCOnversion();

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            response.AddHeader("Content-Disposition", "attachment;filename=" + System.IO.Path.GetFileName(pdf_file_name));
            response.WriteFile(pdf_file_name);
            response.Flush();
            response.End();
            try
            {
                System.IO.File.Delete(f_o_name);
                System.IO.File.Delete(pdf_file_name);
            }
            catch
            { }
        }

        protected void btnSearchLoan_Click(object sender, EventArgs e)
        {
            wsLoanController.GetControlLoansListRequest Req = new wsLoanController.GetControlLoansListRequest();
            Req.BranchID = int.Parse(drlBranch.SelectedValue);
            Req.CollateralNumber = txtCollateralNumber.Text;
            Req.Fullname = txtFullname.Text;
            Req.CurrencyID = int.Parse(drlCurrency.SelectedValue);
            Req.DateStart = txtStartDate.Text;
            Req.DateEnd = txtEndDate.Text;
            Req.UserID = UserSession.UserId;
            fillGrid(Req, false);
        }

        //change loan status open modal
        protected void lnkLoanAktivModal_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            hdnSendLoanID.Value = lnk.CommandArgument;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCheckLoanSendModal();", true);
        }

        //change loan status
        protected void btnSendCheckLoan_Click(object sender, EventArgs e)
        {
            wsLoanController.ChangeLoanStatusRequest req = new ChangeLoanStatusRequest();
            req.LoanID = Convert.ToInt32(hdnSendLoanID.Value);
            req.LoanStatusID = 1;
            req.ModifiedID = UserSession.UserId;
            wsLoanController.SetResponse res = wsLController.ChangeLoanStatusByID(req);
            if (!res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta -" + res.errorCode);
                return;
            }
            _config.AlertMessage(this, MessageType.SUCCESS, "Aktivləşdirildi.");
           
            wsLoanController.GetControlLoansListRequest Req = new wsLoanController.GetControlLoansListRequest();
            Req.BranchID = int.Parse(drlBranch.SelectedValue);
            Req.CollateralNumber = txtCollateralNumber.Text;
            Req.Fullname = txtFullname.Text;
            Req.CurrencyID = int.Parse(drlCurrency.SelectedValue);
            Req.DateStart = txtStartDate.Text;
            Req.DateEnd = txtEndDate.Text;
            Req.UserID = UserSession.UserId;
            fillGrid(Req, false);
        }

        //print data
        protected void lnkDownloadResult_Click(object sender, EventArgs e)
        {
            wsLoanController.GetControlLoansListRequest Req = new wsLoanController.GetControlLoansListRequest();
            Req.BranchID = int.Parse(drlBranch.SelectedValue);
            Req.CollateralNumber = txtCollateralNumber.Text;
            Req.Fullname = txtFullname.Text;
            Req.CurrencyID = int.Parse(drlCurrency.SelectedValue);
            Req.DateStart = txtStartDate.Text;
            Req.DateEnd = txtEndDate.Text;
            Req.UserID = UserSession.UserId;
            fillGrid(Req, true);
        }







        // Begin word template

        public static string SaveToTemporaryFile(string html)
        {
            string htmlTempFilePath = Path.Combine(Path.GetTempPath(), string.Format("{0}.html", Path.GetRandomFileName()));
            using (StreamWriter writer = File.CreateText(htmlTempFilePath))
            {
                html = string.Format("<html>{0}</html>", html);
                writer.WriteLine(html);
            }

            return htmlTempFilePath;
        }

        enum WordBookmarkType
        {
            HTML,
            TXT
        }


        private void ReplaceBookmarkText(Microsoft.Office.Interop.Word.Document doc, string bookmarkName, string text, WordBookmarkType type)
        {
            try
            {
                if (doc.Bookmarks.Exists(bookmarkName))
                {
                    Object name = bookmarkName;
                    Microsoft.Office.Interop.Word.Range range = doc.Bookmarks.get_Item(ref name).Range;
                    range.Text = range.Text.Replace(range.Text, text);
                    object newRange = range;
                    if (type == WordBookmarkType.HTML)
                    {
                        range.InsertFile(SaveToTemporaryFile(text), Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    }
                    doc.Bookmarks.Add(bookmarkName, ref newRange);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }



        private void fillTemplateWord(bookMarks bookmarks, WordBookmarkType wordBookmarkType, Microsoft.Office.Interop.Word.Document doc)
        {
            try
            {
                ReplaceBookmarkText(doc, bookmarks.key.ToString(), bookmarks.value.ToString(), wordBookmarkType);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        // End word template

    }


    public class bookMarks
    {
        public object key { get; set; }
        public object value { get; set; }
    }
}