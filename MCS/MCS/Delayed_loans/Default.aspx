﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Delayed_loans.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary-full" style="margin-bottom: 0;">
        <div class="panel-body" style="padding: 5px; min-height: 38px">
            <div style="float: left; width: 90%">
                <div class="row">
                    <div class="col-sm-3">
                        <asp:DropDownList ID="drlBranch" AutoPostBack="true" class="form-control input-sm" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <asp:TextBox ID="txtFullname" autocomplete="off" class="form-control input-sm" placeHolder="S.A.A" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-2">
                        <asp:TextBox ID="txtCollateralNumber" autocomplete="off" class="form-control input-sm" placeHolder="Girov nömrəsi" runat="server"></asp:TextBox>
                    </div>

                    <div class="col-sm-2">
                        <asp:DropDownList ID="drlCurrency" AutoPostBack="true" class="form-control input-sm" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-2">
                        <asp:TextBox ID="txtDayOfLoan" autocomplete="off" class="form-control input-sm" placeHolder="Günü" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div style="float: right; text-align: right">
                <div class="row">
                    <div class="col-sm-12">
                        <img id="search_loan_data" style="display: none" src="../img/loader1.gif" />
                        <asp:Button ID="btnSearchLoan" CssClass="btn btn-default btn-quirk btn-sm"
                            OnClick="btnSearchLoan_Click"
                            Style="margin: 0" runat="server" Text="YENİLƏ"
                            OnClientClick="this.style.display = 'none';
                            document.getElementById('search_loan_data').style.display = '';" />
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <ul class="list-inline" style="padding-left: 5px">
                    <li style="float: left">
                        <span class="label label-black">Məlumat sayı:
                            <asp:Literal ID="ltrDataCount" runat="server"></asp:Literal>
                        </span>
                    </li>

                    <li style="float: left">
                        <asp:LinkButton ID="lnkYellow" runat="server" OnClick="lnkYellow_Click">
                        <span class="label label-yellow">0</span>
                        </asp:LinkButton>
                    </li>
                    <li style="float: left">
                        <asp:LinkButton ID="lnkRed" runat="server" OnClick="lnkRed_Click">
                        <span class="label label-red">1-31</span>
                        </asp:LinkButton>
                    </li>
                    <li style="float: left">
                        <asp:LinkButton ID="lnkDarkRed" runat="server" OnClick="lnkDarkRed_Click">
                        <span class="label label-darkred">> 31</span>
                        </asp:LinkButton>
                    </li>
                    <li style="float: right">
                        <asp:LinkButton ID="lnkDownloadResult" runat="server"
                            OnClick="lnkDownloadResult_Click">
                        <i class="glyphicon glyphicon-print"></i><span> Çap</span>
                        </asp:LinkButton>
                    </li>
                    <li style="clear: both"></li>
                </ul>
                <div class="mb20"></div>
                <div class="table-responsive">
                    <asp:GridView class="table table-striped nomargin table_custom_border_top_gray"
                        ID="grdLoanData" runat="server" Style="background-color: #ffffff !important; font-family: Tahoma;"
                        DataKeyNames="LoanID"
                        AutoGenerateColumns="False" GridLines="None" ShowFooter="True">
                        <Columns>
                            <asp:TemplateField HeaderText="KS">
                                <ItemTemplate>
                                    <%#Eval("LoanCount")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="50px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FİLİAL">
                                <ItemTemplate>
                                    <%#Eval("BranchName")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="120px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="TƏSVİR">
                                <ItemTemplate>
                                    <%#Eval("Description")%><br />
                                   <label style="font-weight: bold;font-family: 'Open Sans';"> <%#Eval("MobilePhone1")%><br />
                                    <%#Eval("MobilePhone2")%></label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="250px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="SƏNƏD №">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hrlCollateralNumber"
                                        Target="_blank"
                                        NavigateUrl='<%# string.Format("~/CreditDetails/?LoanID={0}", Eval("LoanID")) %>'
                                        runat="server"><%# Eval("CollateralNumber") %></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="KREDİT<br/>QALIĞI">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdTotalBalance" runat="server" Text='<%#Eval("TotalBalance") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdTotalBalanceSum" runat="server" Text=""></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>



                             <asp:TemplateField HeaderText="CARİ HESAB">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdCurAcntAmountBalance" runat="server" Text='<%#Eval("CurAcntAmount") %>'></asp:Label><br />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdCurAcntAmountBalanceSum" runat="server" Text=""></asp:Label><br />
                                    </b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>



                            <asp:TemplateField HeaderText="ƏSAS<br/>FAİZ<br/>DƏBBƏ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdExpiredPrincipalDebt" runat="server" Text='<%#Eval("ExpiredPrincipalDebt") %>'></asp:Label><br />
                                    <asp:Label ID="lblGrdExpiredInterestDebt" runat="server" Text='<%#Eval("ExpiredInterestDebt") %>'></asp:Label><br />
                                    <asp:Label ID="lblGrdPenaltyDebt" runat="server" Text='<%#Eval("PenaltyDebt")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdExpiredPrincipalDebtSum" runat="server" Text=""></asp:Label><br />
                                        <asp:Label ID="lblGrdExpiredInterestDebtSum" runat="server" Text=""></asp:Label><br />
                                        <asp:Label ID="lblGrdPenaltyDebtSum" runat="server" Text=""></asp:Label>
                                    </b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ÖDƏMƏ<br/>TARİXİ">
                                <ItemTemplate>
                                    <%#Eval("PaymentDate")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="GECİKMƏ<br/>GÜNÜ">
                                <ItemTemplate>
                                    <%#Eval("DaysOfRespite")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            

                            <asp:TemplateField HeaderText="QEYD">
                                <ItemTemplate>
                                    <%#Eval("LoanNote")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <ul class="list-inline">
                                        <li>
                                            <i class="glyphicon glyphicon-check"></i>
                                            <asp:LinkButton ID="lnkLoanCheckModal" CommandArgument='<%# Eval("LoanID") %>'
                                                OnClick="lnkLoanCheckModal_Click"
                                                title="Yoxlama" runat="server">
                                        Yoxlama</asp:LinkButton>
                                        </li>
                                    </ul>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="90px" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <i>Məlumat yoxdur...</i>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#D1D1D1" BorderColor="#520000" Font-Bold="True" ForeColor="Black" />
                        <HeaderStyle CssClass="bg-dark" />
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
    <div>

        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalCheckLoanSendModal" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalTitleCheckLoanSend">
                                    <i class="glyphicon glyphicon-check"></i>
                                &nbspYoxlamaya göndərilmə
                            </div>
                            <div class="modal-body">
                                <p>
                                    Yoxlamaya göndərilsin?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="send_check_loan_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnSendCheckLoan" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClick="btnSendCheckLoan_Click"
                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('send_check_loan_loading').style.display = '';
                                                           document.getElementById('btnSendCheckLoanCancel').style.display = 'none';" />
                                <button id="btnSendCheckLoanCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSendCheckLoan" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hdnSendLoanID" runat="server" />

        <script>
            function openCheckLoanSendModal() {
                $('#modalCheckLoanSendModal').modal({ show: true });
            }
        </script>

        <style>
            .input-sm {
                height: 26px !important;
                line-height: 26px !important;
            }


            .input-group-addon {
                padding: 5px 7px !important;
            }

            .form-group {
                margin-bottom: 10px;
            }

            .no_visible_font {
                color: #ffffff;
            }

            .label-yellow {
                background-color: #eaa84b;
            }

            .label-red {
                background-color: #f71b1b;
            }

            .label-darkred {
                background-color: #920f0f;
            }

            .label-black {
                background-color: #343a40;
            }

            .label {
                display: inline;
                padding: 0.6em 1em 0.6em;
                font-size: 80%;
                font-weight: bold !important;
                line-height: 1;
                color: #ffffff;
                text-align: center;
                white-space: nowrap;
                vertical-align: baseline;
                border-radius: .25em;
            }
        </style>
    </div>

</asp:Content>
