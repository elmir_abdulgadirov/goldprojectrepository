﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Tran_CurrAccounts.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">CARİ HESAB ƏMƏLİYYATLARI</h3>
        </div>
        <div class="panel-body">
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <ul class="list-inline">
                        <li>
                            <asp:LinkButton ID="lnkOpenModalSearchcURRaCNTn" runat="server" OnClick="lnkOpenModalSearchcURRaCNTn_Click">
                                <i class="fa fa-search"></i><span> Axtarış</span>
                            </asp:LinkButton>
                        </li>
                    </ul>
                    <div class="mb10"></div>
                    
                    <div class="table-responsive">
                        <asp:GridView ID="grdCustCurrAcnt" class="table table-bordered table-primary table-striped nomargin" runat="server"
                            AutoGenerateColumns="False" DataKeyNames="ACNT_ID,BRANCH_ID,ACNT_CURRENCY,CurrencyName,ACNT_CODE,ACNT_NAME" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="BranchName" HeaderText="FİLİAL" />
                                <asp:BoundField DataField="ACNT_CODE" HeaderText="HESAB №" />
                                <asp:BoundField DataField="CurrencyName" HeaderText="VALYUTA" />
                                <asp:BoundField DataField="ACNT_NAME" HeaderText="HESAB ADI" />
                                <asp:BoundField DataField="CURRENT_BALANCE" HeaderText="BALANS" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPlusAccountModal" runat="server" OnClick="lnkPlusAccountModal_Click">
                                            <i class="fa fa-plus text-success"></i> <p class="text-success">Hesaba mədaxil</p>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="140px" />
                                    <HeaderStyle />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkMinusAccountModal" runat="server" OnClick="lnkMinusAccountModal_Click">
                                            <i class="fa fa-minus text-danger"></i><p class="text-danger">Hesabdan məxaric</p> 
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="150px" />
                                    <HeaderStyle />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>Hesab taılmadı...</EmptyDataTemplate>
                            <HeaderStyle CssClass="bg-blue" />
                        </asp:GridView>
                        <asp:HiddenField ID="hdnSelectedCurrencyId" runat="server" />
                        <asp:HiddenField ID="hdnSelectedCurrencyName" runat="server" />
                        <asp:HiddenField ID="hdnSelectedAccountName" runat="server" />
                    </div>
                </asp:View>
                <asp:View ID="ViewMedaxil" runat="server">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs nav-line">
                                <li style="background-color: #2599ab; height: 27px; padding-top: 5px">
                                    <asp:LinkButton ID="lnkBackToMainView" ToolTip="Geri" runat="server" OnClick="lnkBackToMainView_Click"><img src="../img/back2.png"/></asp:LinkButton></li>
                                <li class="active"><a><strong>CARİ HESABA MƏDAXİL</strong></a></li>
                            </ul>
                            <div class="tab-content mb20">
                                <div class="tab-pane active">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <div class="mb40"></div>
                                                    <div class="form-group">
                                                        Debet hesab:
                                                          <asp:DropDownList ID="drl_Plus_DebetAccount" class="form-control" runat="server"></asp:DropDownList>
                                                    </div>
                                                    <div class="form-group">
                                                        Kredit hesab:
                                                          <asp:DropDownList ID="drl_Plus_CreditAccount" disabled  class="form-control" runat="server"></asp:DropDownList>
                                                    </div>

                                                    <div class="form-group">
                                                        Kredit sub hesab:
                                                          <asp:DropDownList ID="drl_Plus_CreditSubAccount"  class="form-control" runat="server"></asp:DropDownList>
                                                    </div>

                                                    <div class="form-group">
                                                        Məbləğ:
                                                        <asp:TextBox ID="txt_Plus_Amount" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        Valyuta:
                                                        <asp:TextBox ID="txt_Plus_Valuta" disabled type="text" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        Ödənişin təyinatı:
                                                        <asp:TextBox ID="txt_Plus_Description" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group" style="text-align: right">
                                                        <img id="create_transaction_loading" style="display: none" src="../img/loader1.gif" />
                                                        <asp:Button ID="btnCreateMedaxil" class="btn btn-primary" runat="server" Text="Təsdiq et"
                                                            OnClick="btnCreateMedaxil_Click"
                                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('create_transaction_loading').style.display = '';" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="ViewMexaric" runat="server">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs nav-line nav-line-danger">
                                <li style="background-color: #d9534f; height: 27px; padding-top: 5px">
                                    <asp:LinkButton ID="lnkBackToMainView_1" ToolTip="Geri" runat="server" OnClick="lnkBackToMainView_Click"><img src="../img/back2.png"/></asp:LinkButton></li>
                                <li class="active"><a><strong>CARİ HESABDAN MƏXARİC</strong></a></li>
                            </ul>
                            <div class="tab-content mb20">
                                <div class="tab-pane active">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <div class="mb40"></div>
                                                    <div class="form-group">
                                                        Debet hesab:
                                                          <asp:DropDownList ID="drl_Minus_DebetAccount" disabled class="form-control" runat="server"></asp:DropDownList>
                                                    </div>

                                                    <div class="form-group">
                                                        Debet sub hesab:
                                                          <asp:DropDownList ID="drl_Minus_DebetSubAccount"  class="form-control" runat="server"></asp:DropDownList>
                                                    </div>

                                                    <div class="form-group">
                                                        Kredit hesab:
                                                          <asp:DropDownList ID="drl_Minus_CreditAccount" class="form-control" runat="server"></asp:DropDownList>
                                                    </div>
                                                    <div class="form-group">
                                                        Məbləğ:
                                                        <asp:TextBox ID="txt_Minus_Amount" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        Valyuta:
                                                        <asp:TextBox ID="txt_Minus_Valuta" disabled type="text" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        Ödənişin təyinatı:
                                                        <asp:TextBox ID="txt_Minus_Description" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group" style="text-align: right">
                                                        <img id="create_transaction_mexaric_loading" style="display: none" src="../img/loader1.gif" />
                                                        <asp:Button ID="btnCreateMexaric" class="btn btn-primary" runat="server" Text="Təsdiq et"
                                                            OnClick="btnCreateMexaric_Click"
                                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('create_transaction_mexaric_loading').style.display = '';" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal bounceIn" id="modalAccountSearch" data-backdrop="static">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalAccountSearchLabel">
                                Hesab axtarışı
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtSearchAcntCustomerName" autocomplete="off" class="form-control" placeHolder="Müştəri adı" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtSearchCollateralNumber" autocomplete="off" class="form-control" placeHolder="Girov bileti №" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <img id="search_acc_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnSearchCustomerAcnt" class="btn btn-primary" runat="server" Text="Axtarış"
                                    OnClick="btnSearchCustomerAcnt_Click"
                                    OnClientClick="this.style.display = 'none';
                                                  document.getElementById('search_acc_loading').style.display = '';" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSearchCustomerAcnt" />
            </Triggers>
        </asp:UpdatePanel>

    </div>


    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h3 class="panel-title">ƏMƏLİYYATLARIN SİYAHISI</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-2">
                    <asp:DropDownList ID="drlBranch" class="form-control input-sm" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="col-sm-1">
                    <asp:DropDownList ID="drlCurrency" class="form-control input-sm" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <div class="input-group">
                        <asp:TextBox ID="txtStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="input-group">
                        <asp:TextBox ID="txtEndDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <asp:TextBox ID="txtAcntName" autocomplete="off" class="form-control input-sm" placeHolder="Hesabın adı" runat="server"></asp:TextBox>
                </div>
                <div class="col-sm-2">
                    <img id="search_tran_data" style="display: none" src="../img/loader1.gif" />
                    <asp:Button ID="btnSearchTran" CssClass="btn btn-default btn-quirk btn-sm"
                        OnClick="btnSearchTran_Click"
                        Style="margin: 0; width: 100%" runat="server" Text="AXTAR"
                        OnClientClick="this.style.display = 'none';
                                            document.getElementById('search_tran_data').style.display = '';" />
                </div>
            </div>
            <div class="mb20"></div>
            <div class="row">
                <div class="table-responsive">
                    <asp:GridView class="table table-striped nomargin table_custom_border_top_gray"
                        ID="grdTransactionList" runat="server" Style="background-color: #ffffff !important;"
                        DataKeyNames="MsgID"
                        AutoGenerateColumns="False" GridLines="None" ShowFooter="True">
                        <Columns>
                            <asp:TemplateField HeaderText="FİLİAL">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdBranchName" runat="server" Text='<%#Eval("BranchName") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="TARİX">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdTransactionDate" runat="server" Text='<%#Eval("TranDate") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="HESABIN TƏSVİRİ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdTransactionACNT_NAME" runat="server" Text='<%#Eval("ACNT_NAME") %>'></asp:Label>
                                    <br />
                                    <asp:Label ID="lblGrdTransactionACNT_CODE" runat="server" Text='<%#Eval("ACNT_CODE") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdTransactionCountFooter" runat="server" ></asp:Label>
                                    </b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="VALYUTA">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdTransactionCurrencyName" runat="server" Text='<%#Eval("CurrencyName") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DEBET">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdTransactionDTAmount" runat="server" Text='<%#Eval("DTAmount") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdTransactionDTAmountSum" runat="server" Text=""></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" ForeColor="Black" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="CREDIT">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdTransactionCTAmount" runat="server" Text='<%#Eval("CTAmount") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdTransactionCTAmountSum" runat="server" Text=""></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" ForeColor="Black" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="BALANS">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdTransactionCURRENT_BALANCE" runat="server" Text='<%#Eval("CURRENT_BALANCE") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdTransactionCURRENT_BALANCESum" runat="server" Text=""></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" ForeColor="Black" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>


                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkGrdTransactionDelete" 
                                        CommandArgument='<%# Eval("MsgID") %>'
                                        OnClick ="lnkGrdTransactionDelete_Click"
                                        title="Sil" runat="server">
                                                               <i class="glyphicon glyphicon-trash"></i>
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="45px" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <i>Əməliyyat yoxdur...</i>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#D1D1D1" BorderColor="#520000" Font-Bold="True" ForeColor="Black" />
                        <HeaderStyle CssClass="bg-dark" />
                    </asp:GridView>
                    <asp:HiddenField ID="hdnSelectedTranMsgID" runat="server" />
                </div>


            </div>
        </div>



         <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalDeleteTranAlert" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalDeleteTranAlertLabel">
                                    <i class="fa fa-trash"></i>&nbsp;Əməliyyatın silinməsi
                                </h4>
                            </div>
                            <div class="modal-body">
                                <p>
                                    Əməliyyat silinsin?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="delete_transaction_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnDeleteTransaction" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClick ="btnDeleteTransaction_Click"
                                    OnClientClick="this.style.display = 'none';
                                                   document.getElementById('delete_transaction_loading').style.display = '';
                                                   document.getElementById('btnDeleteTransactionCancel').style.display = 'none';" />
                                <button id="btnDeleteTransactionCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDeleteTransaction" />
            </Triggers>
        </asp:UpdatePanel>



    </div>


    <style>
        .input-sm {
            height: 26px !important;
            line-height: 26px !important;
        }

        .input-group-addon {
            padding: 5px 7px !important;
        }

        .form-group {
            margin-bottom: 10px;
        }

        .no_visible_font {
            color: #ffffff;
        }

        .nav-line-danger > li.active > a,
        .nav-line-danger > li.active > a:hover,
        .nav-line-danger > li.active > a:focus {
            color: #d9534f !important;
            background-color: transparent;
            -webkit-box-shadow: 0 1px 0 #d9534f !important;
            box-shadow: 0 1px 0 #d9534f !important;
        }
    </style>

    <script>
        function openAccountSearchModal() {
            $('#modalAccountSearch').modal({ show: true });
        }

        function openDeleteTransactionModal() {
            $('#modalDeleteTranAlert').modal({ show: true });
        }

        $(document).ready(function () {

            'use strict';

            $('#ContentPlaceHolder1_grdTransactionList').DataTable();


        });

    </script>

</asp:Content>
