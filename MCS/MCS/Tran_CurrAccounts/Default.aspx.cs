﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code._Config;

namespace MCS.Tran_CurrAccounts
{
    public partial class Default : System.Web.UI.Page
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsLoanController.LoanController wsLController = new MCS.wsLoanController.LoanController();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();
        MCS.wsAccountController.AccountController wsAController = new MCS.wsAccountController.AccountController();
        MCS.wsTransactionController.TransactionController wsTController = new wsTransactionController.TransactionController();
        

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                txtEndDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                FillSearchData();
                fillGrdTransaction();
            }    
        }

        void fillGrdTransaction()
        {
            wsAccountController.GetCurAcntTranLIstRequest req = new wsAccountController.GetCurAcntTranLIstRequest();
            req.DateFrom = !_config.isDate(txtStartDate.Text) ? _Config.HostingTime.ToString("dd.MM.yyyy") : txtStartDate.Text;
            req.DateTo = !_config.isDate(txtEndDate.Text) ? _Config.HostingTime.ToString("dd.MM.yyyy") : txtEndDate.Text;
            req.AcntName = txtAcntName.Text.Trim();
            req.CurrencyID = Convert.ToInt32(drlCurrency.SelectedValue);
            req.UserID = UserSession.UserId;
            req.BranchID = Convert.ToInt32(drlBranch.SelectedValue);
           
           
           
            DataTable dt = wsAController.GetCurAcntTranLIst(req);

            int cnt = dt.Rows.Count;

            if (dt == null)
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Xəta baş verdi");
                return;
            }
            grdTransactionList.DataSource = dt;
            grdTransactionList.DataBind();


            if (grdTransactionList.Rows.Count > 0)
            {
                GridViewRow drr = grdTransactionList.FooterRow;
                Label lblGrdTransactionDTAmountSum = (Label)drr.FindControl("lblGrdTransactionDTAmountSum");
                Label lblGrdTransactionCTAmountSum = (Label)drr.FindControl("lblGrdTransactionCTAmountSum");
                Label lblGrdTransactionCURRENT_BALANCESum = (Label)drr.FindControl("lblGrdTransactionCURRENT_BALANCESum");
                Label lblGrdTransactionCountFooter = (Label)drr.FindControl("lblGrdTransactionCountFooter");

                double DTAmountSum = 0;
                double CTAmountSum = 0;
                double CURRENT_BALANCESum = 0;

                for (int i = 0; i < grdTransactionList.Rows.Count; i++)
                {
                    DTAmountSum += Convert.ToDouble(((Label)grdTransactionList.Rows[i].FindControl("lblGrdTransactionDTAmount")).Text);
                    CTAmountSum += Convert.ToDouble(((Label)grdTransactionList.Rows[i].FindControl("lblGrdTransactionCTAmount")).Text);
                    CURRENT_BALANCESum += Convert.ToDouble(((Label)grdTransactionList.Rows[i].FindControl("lblGrdTransactionCURRENT_BALANCE")).Text);
                }
                lblGrdTransactionDTAmountSum.Text = DTAmountSum.ToString("0.00");
                lblGrdTransactionCTAmountSum.Text = CTAmountSum.ToString("0.00");
                lblGrdTransactionCURRENT_BALANCESum.Text = CURRENT_BALANCESum.ToString("0.00");
                lblGrdTransactionCountFooter.Text = "ƏMƏLİYYAT SAYI :    " + grdTransactionList.Rows.Count.ToString();

            }

        }
        void FillSearchData()
        {
            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial", "0"));
            }


            //Currency
            DataTable dtCurrency = wsGController.CurrencyList();
            if (dtCurrency.Rows.Count > 0)
            {
                drlCurrency.Items.Clear();
                drlCurrency.DataTextField = "CurrencyName";
                drlCurrency.DataValueField = "CurrencyID";
                drlCurrency.DataSource = dtCurrency;
                drlCurrency.DataBind();
                drlCurrency.SelectedValue = "1";
            }
        }


        //open search modal
        protected void lnkOpenModalSearchcURRaCNTn_Click(object sender, EventArgs e)
        {
            txtSearchAcntCustomerName.Text = "";
            txtSearchCollateralNumber.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAccountSearchModal();", true);
        }

        //search account
        protected void btnSearchCustomerAcnt_Click(object sender, EventArgs e)
        {
            string _searchname = txtSearchAcntCustomerName.Text;
            string _search_collateral = txtSearchCollateralNumber.Text;

            wsAccountController.SearchCustCurrAcntsRequest Req = new wsAccountController.SearchCustCurrAcntsRequest();
            Req.UserId = UserSession.UserId;
            Req.AcntName = _searchname;
            Req.CollaeralNumber = _search_collateral;
            Req.AcntID = 0;

            FillAccounts(Req);
        }

        void FillAccounts(wsAccountController.SearchCustCurrAcntsRequest Request)
        {
            DataTable dtAccounts = wsAController.SearchCustCurrAcnts(Request);
            grdCustCurrAcnt.DataSource = dtAccounts;
            grdCustCurrAcnt.DataBind();
        }


        //Cari hesaba medaxil view
        protected void lnkPlusAccountModal_Click(object sender, EventArgs e)
        {        
            int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
            
            int ACNT_ID = Convert.ToInt32(grdCustCurrAcnt.DataKeys[rowIndex].Values["ACNT_ID"]);
            int BRANCH_ID = Convert.ToInt32(grdCustCurrAcnt.DataKeys[rowIndex].Values["BRANCH_ID"]);
            int ACNT_CURRENCY = Convert.ToInt32(grdCustCurrAcnt.DataKeys[rowIndex].Values["ACNT_CURRENCY"]);
            string CurrencyName = Convert.ToString(grdCustCurrAcnt.DataKeys[rowIndex].Values["CurrencyName"]);
            string ACNT_CODE = Convert.ToString(grdCustCurrAcnt.DataKeys[rowIndex].Values["ACNT_CODE"]);
            string ACNT_NAME = Convert.ToString(grdCustCurrAcnt.DataKeys[rowIndex].Values["ACNT_NAME"]);
            MultiView1.ActiveViewIndex = 1;


            drl_Plus_CreditAccount.Items.Clear();
            drl_Plus_CreditAccount.Items.Add(new ListItem(ACNT_CODE, ACNT_ID.ToString()));
            drl_Plus_CreditAccount.SelectedIndex = 0;

            wsAccountController.GetSubAcntListRequest reqSubAcntD = new wsAccountController.GetSubAcntListRequest();
            reqSubAcntD.MainAcntID = ACNT_ID;
            DataTable dtCreditSubAccount = wsAController.GetSubAcntList(reqSubAcntD);
            drl_Plus_CreditSubAccount.Items.Clear();
            drl_Plus_CreditSubAccount.DataTextField = "SUB_ACNT_BALANCE";
            drl_Plus_CreditSubAccount.DataValueField = "SUB_ACNT_ID";
            drl_Plus_CreditSubAccount.DataSource = dtCreditSubAccount;
            drl_Plus_CreditSubAccount.DataBind();
            drl_Plus_CreditSubAccount.Items.Insert(0, new ListItem("Kredit sub hesabı seçin", ""));
            drl_Plus_CreditSubAccount.SelectedIndex = 0;

            txt_Plus_Valuta.Text = CurrencyName;


            

            wsAccountController.GetDebetAcntsByCurrAcntRequest reqDebetAcnts = new wsAccountController.GetDebetAcntsByCurrAcntRequest();
            reqDebetAcnts.BranchID = BRANCH_ID;
            reqDebetAcnts.CurrencyID = ACNT_CURRENCY;
            hdnSelectedCurrencyId.Value = ACNT_CURRENCY.ToString();
            hdnSelectedCurrencyName.Value = CurrencyName;
            hdnSelectedAccountName.Value = ACNT_NAME;

            DataTable dtDebetAcnts = wsAController.GetDebetAcntsByCurrAcnt(reqDebetAcnts);
           
            drl_Plus_DebetAccount.Items.Clear();
            drl_Plus_DebetAccount.DataTextField = "ACNT_NAME";
            drl_Plus_DebetAccount.DataValueField = "ACNT_ID";
            drl_Plus_DebetAccount.DataSource = dtDebetAcnts;
            drl_Plus_DebetAccount.DataBind();
            drl_Plus_DebetAccount.Items.Insert(0, new ListItem("Debet hesabı seçin", ""));

            txt_Plus_Amount.Text = "";
            txt_Plus_Description.Text = "";


        }

        //back to main view
        protected void lnkBackToMainView_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;//
        }

        //CreateTransactions Medaxil
        protected void btnCreateMedaxil_Click(object sender, EventArgs e)
        {
            txt_Plus_Valuta.Text = hdnSelectedCurrencyName.Value;
            if (drl_Plus_DebetAccount.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Debet hesabı seçilməyib.");
                return;
            }
            if (drl_Plus_CreditAccount.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Kredit hesabı seçilməyib.");
                return;
            }

            float _amount = 0;
            try
            {
                _amount = _config.ToFloat(txt_Plus_Amount.Text);
            }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Məbləğ düzgün daxil edilməyib.");
                return;
            }

            if (txt_Plus_Description.Text.Trim().Length < 2)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Ödənişin təyinatı daxil edilməyib.");
                return;
            }

            wsTransactionController.CreateCashBankTransactionRequest treq = new wsTransactionController.CreateCashBankTransactionRequest();
            treq.ValueDate = _Config.HostingTime.ToString("dd.MM.yyyy");
            treq.ResponsibilityPerson = txt_Plus_Description.Text;
            treq.Assignment = "T_INCREASE_CURRENT_ACNT";
            treq.Amount = Convert.ToDecimal(_amount);
            treq.Note = txt_Plus_Description.Text;
            treq.TranType = "D";
            treq.OperationType = "CASH";
            treq.Corresp_AcntID = Convert.ToInt32(drl_Plus_CreditAccount.SelectedValue);
            treq.CashBankAcntID = Convert.ToInt32(drl_Plus_DebetAccount.SelectedValue);
            treq.CreatedID = UserSession.UserId;

            wsTransactionController.SetResponse tres = wsTController.CreateCashBankTransaction(treq);

            if (!tres.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi : " + tres.errorCode);
                return;
            }
            _config.AlertMessage(this, MessageType.SUCCESS, "Əməliyyat uğurla yaradıldı.");

            wsAccountController.SearchCustCurrAcntsRequest ReqAccount = new wsAccountController.SearchCustCurrAcntsRequest();
            ReqAccount.UserId = UserSession.UserId;
            ReqAccount.AcntName = "";
            ReqAccount.CollaeralNumber = "";
            ReqAccount.AcntID = Convert.ToInt32(drl_Plus_CreditAccount.SelectedValue);

            FillAccounts(ReqAccount);
            MultiView1.ActiveViewIndex = 0; //back to main view
        }


        //cari hesabdan mexaric view
        protected void lnkMinusAccountModal_Click(object sender, EventArgs e)
        {
            int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;

            int ACNT_ID = Convert.ToInt32(grdCustCurrAcnt.DataKeys[rowIndex].Values["ACNT_ID"]);
            int BRANCH_ID = Convert.ToInt32(grdCustCurrAcnt.DataKeys[rowIndex].Values["BRANCH_ID"]);
            int ACNT_CURRENCY = Convert.ToInt32(grdCustCurrAcnt.DataKeys[rowIndex].Values["ACNT_CURRENCY"]);
            string CurrencyName = Convert.ToString(grdCustCurrAcnt.DataKeys[rowIndex].Values["CurrencyName"]);
            string ACNT_CODE = Convert.ToString(grdCustCurrAcnt.DataKeys[rowIndex].Values["ACNT_CODE"]);
            string ACNT_NAME = Convert.ToString(grdCustCurrAcnt.DataKeys[rowIndex].Values["ACNT_NAME"]);
            MultiView1.ActiveViewIndex = 2;

            drl_Minus_DebetAccount.Items.Clear();
            drl_Minus_DebetAccount.Items.Add(new ListItem(ACNT_CODE, ACNT_ID.ToString()));
            drl_Minus_DebetAccount.SelectedIndex = 0;


            wsAccountController.GetSubAcntListRequest reqSubAcntD = new wsAccountController.GetSubAcntListRequest();
            reqSubAcntD.MainAcntID = ACNT_ID;
            DataTable dtDebetSubAccount = wsAController.GetSubAcntList(reqSubAcntD);
            drl_Minus_DebetSubAccount.Items.Clear();
            drl_Minus_DebetSubAccount.DataTextField = "SUB_ACNT_BALANCE";
            drl_Minus_DebetSubAccount.DataValueField = "SUB_ACNT_ID";
            drl_Minus_DebetSubAccount.DataSource = dtDebetSubAccount;
            drl_Minus_DebetSubAccount.DataBind();
            drl_Minus_DebetSubAccount.Items.Insert(0, new ListItem("Debet sub hesabı seçin", ""));
            drl_Minus_DebetSubAccount.SelectedIndex = 0;


            txt_Minus_Valuta.Text = CurrencyName;


            wsAccountController.GetDebetAcntsByCurrAcntRequest reqDebetAcnts = new wsAccountController.GetDebetAcntsByCurrAcntRequest();
            reqDebetAcnts.BranchID = BRANCH_ID;
            reqDebetAcnts.CurrencyID = ACNT_CURRENCY;
            hdnSelectedCurrencyId.Value = ACNT_CURRENCY.ToString();
            hdnSelectedCurrencyName.Value = CurrencyName;
            hdnSelectedAccountName.Value = ACNT_NAME;

            DataTable dtDebetAcnts = wsAController.GetDebetAcntsByCurrAcnt(reqDebetAcnts);

            drl_Minus_CreditAccount.Items.Clear();
            drl_Minus_CreditAccount.DataTextField = "ACNT_NAME";
            drl_Minus_CreditAccount.DataValueField = "ACNT_ID";
            drl_Minus_CreditAccount.DataSource = dtDebetAcnts;
            drl_Minus_CreditAccount.DataBind();
            drl_Minus_CreditAccount.Items.Insert(0, new ListItem("Kredit hesabı seçin", ""));

            txt_Minus_Amount.Text = "";
            txt_Minus_Description.Text = "";
        }

        //CreateTransactions Mexaric
        protected void btnCreateMexaric_Click(object sender, EventArgs e)
        {
            txt_Minus_Valuta.Text = hdnSelectedCurrencyName.Value;
            if (drl_Minus_DebetAccount.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Debet hesabı seçilməyib.");
                return;
            }
            if (drl_Minus_CreditAccount.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Kredit hesabı seçilməyib.");
                return;
            }

            float _amount = 0;
            try
            {
                _amount = _config.ToFloat(txt_Minus_Amount.Text);
            }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Məbləğ düzgün daxil edilməyib.");
                return;
            }

            if (txt_Minus_Description.Text.Trim().Length < 2)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Ödənişin təyinatı daxil edilməyib.");
                return;
            }

            wsTransactionController.CreateCashBankTransactionRequest treq = new wsTransactionController.CreateCashBankTransactionRequest();
            treq.ValueDate = _Config.HostingTime.ToString("dd.MM.yyyy");
            treq.ResponsibilityPerson = txt_Minus_Description.Text;
            treq.Assignment = "T_TRANS_CURR_ACNT_TO_CREDIT";
            treq.Amount = Convert.ToDecimal(_amount);
            treq.Note = txt_Minus_Description.Text;
            treq.TranType = "C";
            treq.OperationType = "CASH";
            treq.Corresp_AcntID = Convert.ToInt32(drl_Minus_DebetAccount.SelectedValue);
            treq.CashBankAcntID = Convert.ToInt32(drl_Minus_CreditAccount.SelectedValue);
            treq.CreatedID = UserSession.UserId;

            wsTransactionController.SetResponse tres = wsTController.CreateCashBankTransaction(treq);

            if (!tres.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi : " + tres.errorCode);
                return;
            }

            _config.AlertMessage(this, MessageType.SUCCESS, "Əməliyyat uğurla yaradıldı.");

            wsAccountController.SearchCustCurrAcntsRequest ReqAccount = new wsAccountController.SearchCustCurrAcntsRequest();
            ReqAccount.UserId = UserSession.UserId;
            ReqAccount.AcntName = "";
            ReqAccount.CollaeralNumber = "";
            ReqAccount.AcntID = Convert.ToInt32(drl_Minus_DebetAccount.SelectedValue);

            FillAccounts(ReqAccount);
            MultiView1.ActiveViewIndex = 0; //back to main view
        }

        //search tran
        protected void btnSearchTran_Click(object sender, EventArgs e)
        {
            fillGrdTransaction();
        }

        //open delete modal
        protected void lnkGrdTransactionDelete_Click(object sender, EventArgs e)
        {
            string msgID = (sender as LinkButton).CommandArgument;
            hdnSelectedTranMsgID.Value = msgID;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDeleteTransactionModal();", true);
        }

        //delete transactions
        protected void btnDeleteTransaction_Click(object sender, EventArgs e)
        {
            string msgID = hdnSelectedTranMsgID.Value;
            wsTransactionController.DeleteCashBankTransactionRequest req = new wsTransactionController.DeleteCashBankTransactionRequest();
            req.Description = "Səhv əməliyyat";
            req.ModifiedID = UserSession.UserId;
            req.MsgID = msgID;
            wsTransactionController.SetResponse res = wsTController.DeleteCashBankTransactionByID(req);
            if (!res.isSuccess)
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Xəta :" + res.errorCode);
                return;
            }
            _config.AlertMessage(this, _Config.MessageType.SUCCESS, "Əməliyyat silindi");
            fillGrdTransaction();
        }
    }
}