﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code.Enums;

namespace MCS.Specialtransaction
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsTransactionController.TransactionController wsTController = new wsTransactionController.TransactionController();
        MCS.wsAccountController.AccountController wsAController = new wsAccountController.AccountController();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                txtEndDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                FillAndClearForm();
                fillGrid();
            }
            txtOperationDate.Attributes.Add("disabled", "disabled");
            txtOperationDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");

            #region Begin Permission
              CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.SPECIAL_TRANSACTION));
              if (grdTransactions.Rows.Count > 0)
              {
                  grdTransactions.Columns[7].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.SPECIAL_TRAN_DELETE));
              }
              lnkNewTransactions.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.SPECIAL_TRAN_ADD_NEW));
            #endregion

        }

        void fillGrid()
        {
            if (!_config.isDate(txtStartDate.Text))
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Başlanğıc tarix düzgün deyil");
                return;
            }

            if (!_config.isDate(txtEndDate.Text))
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Son tarix düzgün deyil");
                return;
            }

            wsTransactionController.GetSpecialTranListRequest req = new wsTransactionController.GetSpecialTranListRequest();
            req.BranchID =Convert.ToInt32(drlBranch.SelectedValue); 
            req.DateFrom = txtStartDate.Text;
            req.DateTo = txtEndDate.Text;
            req.UserID = UserSession.UserId;
            DataTable dt = wsTController.GetSpecialTranList(req);
            if (dt == null)
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Xəta baş verdi.");
                return;
            }
            grdTransactions.DataSource = dt;
            grdTransactions.DataBind();
        }

        void FillAndClearForm()
        {
            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial", "0"));
            }
            
            DataTable dtAccountGroup = wsAController.GetAccountGroupList();

            drlDTAccountGroup.DataTextField = "Name";
            drlDTAccountGroup.DataValueField = "Code";
            drlDTAccountGroup.DataSource = dtAccountGroup;
            drlDTAccountGroup.DataBind();
            drlDTAccountGroup.Items.Insert(0, new ListItem("Seçin", "-1"));

            drlKTAccountGroup.DataTextField = "Name";
            drlKTAccountGroup.DataValueField = "Code";
            drlKTAccountGroup.DataSource = dtAccountGroup;
            drlKTAccountGroup.DataBind();
            drlKTAccountGroup.Items.Insert(0, new ListItem("Seçin", "-1"));

            drlDTAccountSubGroup.Items.Clear();
            drlDTAccountSubSubGroup.Items.Clear();
            drlDTAccount.Items.Clear();

            drlKTAccountSubGroup.Items.Clear();
            drlKTAccountSubSubGroup.Items.Clear();
            drlKTAccount.Items.Clear();

            txtAmount.Text = "";
            txtNote.Text = "";

            //SourceMaster
            DataTable dtSourseMaster = wsTController.GetTransactionSourceMasterList();
            drlNarrative.Items.Clear();
            drlNarrative.DataTextField = "T_DESCRIPTION";
            drlNarrative.DataValueField = "T_KEY";
            drlNarrative.DataSource = dtSourseMaster;
            drlNarrative.DataBind();
            drlNarrative.Items.Insert(0, new ListItem("Təyinat", ""));
        }

        // operation view
        protected void lnkNewTransactions_Click(object sender, EventArgs e)
        {
            FillAndClearForm();
            MultiView1.ActiveViewIndex = 1;
        }

        protected void lnkBackToMainView_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void drlDTAccountGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string code = drlDTAccountGroup.SelectedValue;
            if (code != "-1")
            {
                drlDTAccountSubGroup.DataTextField = "Name";
                drlDTAccountSubGroup.DataValueField = "Code";
                drlDTAccountSubGroup.DataSource = wsAController.GetAccountSubGroupList(code);
                drlDTAccountSubGroup.DataBind();
                drlDTAccountSubGroup.Items.Insert(0, new ListItem("Seçin", "-1"));
            }
            if (code == "-1")
                drlDTAccountSubGroup.Items.Clear();
            drlDTAccountSubSubGroup.Items.Clear();
            drlDTAccount.Items.Clear();
        }

        protected void drlKTAccountGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string code = drlKTAccountGroup.SelectedValue;
            if (code != "-1")
            {
                drlKTAccountSubGroup.DataTextField = "Name";
                drlKTAccountSubGroup.DataValueField = "Code";
                drlKTAccountSubGroup.DataSource = wsAController.GetAccountSubGroupList(code);
                drlKTAccountSubGroup.DataBind();
                drlKTAccountSubGroup.Items.Insert(0, new ListItem("Seçin", "-1"));
            }
            if (code == "-1")
                drlKTAccountSubGroup.Items.Clear();
            drlKTAccountSubSubGroup.Items.Clear();
            drlKTAccount.Items.Clear();
        }

        protected void drlDTAccountSubGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string code = drlDTAccountSubGroup.SelectedValue;
            if (code != "-1")
            {
                drlDTAccountSubSubGroup.DataTextField = "Description";
                drlDTAccountSubSubGroup.DataValueField = "CodeAndCurrencyID";
                drlDTAccountSubSubGroup.DataSource = wsAController.GetAccountSubSubGroupList(code);
                drlDTAccountSubSubGroup.DataBind();
                drlDTAccountSubSubGroup.Items.Insert(0, new ListItem("Seçin", "-1"));
            }
            if (code == "-1")
                drlDTAccountSubSubGroup.Items.Clear();

            drlDTAccount.Items.Clear();
        }

        protected void drlKTAccountSubGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string code = drlKTAccountSubGroup.SelectedValue;
            if (code != "-1")
            {
                drlKTAccountSubSubGroup.DataTextField = "Description";
                drlKTAccountSubSubGroup.DataValueField = "CodeAndCurrencyID";
                drlKTAccountSubSubGroup.DataSource = wsAController.GetAccountSubSubGroupList(code);
                drlKTAccountSubSubGroup.DataBind();
                drlKTAccountSubSubGroup.Items.Insert(0, new ListItem("Seçin", "-1"));
            }
            if (code == "-1")
                drlKTAccountSubSubGroup.Items.Clear();

            drlKTAccount.Items.Clear();
        }

        protected void drlDTAccountSubSubGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CodeAndCurrencyID = drlDTAccountSubSubGroup.SelectedValue;
            if (CodeAndCurrencyID != "-1")
            {
                drlDTAccount.DataTextField = "ACNT_NAME1";
                drlDTAccount.DataValueField = "ACNT_ID";
                drlDTAccount.DataSource = wsAController.GetAcntDataByAcntCode(CodeAndCurrencyID.Split('_')[0], UserSession.UserId, int.Parse(CodeAndCurrencyID.Split('_')[1]));
                drlDTAccount.DataBind();
                drlDTAccount.Items.Insert(0, new ListItem("Seçin", "-1"));
            }
            if (CodeAndCurrencyID == "-1")
                drlDTAccount.Items.Clear();
        }

        protected void drlKTAccountSubSubGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CodeAndCurrencyID = drlKTAccountSubSubGroup.SelectedValue;
            if (CodeAndCurrencyID != "-1")
            {
                drlKTAccount.DataTextField = "ACNT_NAME1";
                drlKTAccount.DataValueField = "ACNT_ID";
                drlKTAccount.DataSource = wsAController.GetAcntDataByAcntCode(CodeAndCurrencyID.Split('_')[0], UserSession.UserId, int.Parse(CodeAndCurrencyID.Split('_')[1]));
                drlKTAccount.DataBind();
                drlKTAccount.Items.Insert(0, new ListItem("Seçin", "-1"));
            }
            if (CodeAndCurrencyID == "-1")
                drlKTAccount.Items.Clear();
        }

        protected void btnCreateTransactions_Click(object sender, EventArgs e)
        {
            if (!_config.isDate(txtOperationDate.Text))
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Tarix formatı düzgün deyil");
                return;
            }
            if (drlDTAccountGroup.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "DT Hesab qrupunu seçin!");
                return;
            }
            if (drlDTAccountSubGroup.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "DT Hesab alt qrupunu seçin!");
                return;
            }
            if (drlDTAccountSubSubGroup.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "DT Hesab adını seçin!");
                return;
            }
            if (drlDTAccount.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "DT Hesabı  seçin!");
                return;
            }

            decimal amount = 0;
            try
            {
                amount = _config.ToDecimal(txtAmount.Text);
            }
            catch
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Məbləği düzgün daxil edin!");
                return;
            }
            if (drlNarrative.SelectedValue == "")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Təyinatı daxil edin!");
                return;
            }

            if (drlKTAccountGroup.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "KT Hesab qrupunu seçin!");
                return;
            }
            if (drlKTAccountSubGroup.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "KT Hesab alt qrupunu seçin!");
                return;
            }
            if (drlKTAccountSubSubGroup.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "KT Hesab adını seçin!");
                return;
            }
            if (drlKTAccount.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "KT Hesabı  seçin!");
                return;
            }

            wsTransactionController.CreateSpecialTransactionRequest req = new wsTransactionController.CreateSpecialTransactionRequest();
            req.Amount = amount;
            req.CreatedID = UserSession.UserId;
            req.CTAccountID = Convert.ToInt32(drlKTAccount.SelectedValue);
            req.DTAccountID = Convert.ToInt32(drlDTAccount.SelectedValue);
            req.Note = txtNote.Text;
            req.ValueDate = txtOperationDate.Text;
            req.SourceKey = drlNarrative.SelectedValue;
            wsTransactionController.SetResponse res = wsTController.CreateSpecialTransaction(req);
            if (!res.isSuccess)
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Xəta : " + res.errorCode);
                return;
            }
            _config.AlertMessage(this, _Config.MessageType.SUCCESS, "Əməliyyat yaradıldı.");
            fillGrid();
            MultiView1.ActiveViewIndex = 0;
        }

        //search tran
        protected void btnSearchTran_Click(object sender, EventArgs e)
        {
            fillGrid();
        }

        //open delete modal
        protected void lnkDeleteTranModal_Click(object sender, EventArgs e)
        {
            string msgID = (sender as LinkButton).CommandArgument;
            hdnSelectedTranMsgID.Value = msgID;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDeleteTransactionModal();", true);
        }

        //delete (reverse) transactions
        protected void btnDeleteTransaction_Click(object sender, EventArgs e)
        {
            string msgID = hdnSelectedTranMsgID.Value;
            wsTransactionController.reverseTranRequest req = new wsTransactionController.reverseTranRequest();
            req.MsgID = msgID;
            req.ModifiedID = UserSession.UserId;
            req.Description = "Reverse transaction process";
            wsTransactionController.reverseTranResponse res =  wsTController.ReverseTransaction(req);
            if (!res.isSuccess)
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Xəta : " + res.errorCode);
                return;
            }
            _config.AlertMessage(this, _Config.MessageType.SUCCESS, "Əməliyyat silindi.");
            fillGrid();
        }
    }
}