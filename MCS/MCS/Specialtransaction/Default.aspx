﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Specialtransaction.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <style>
        .dropdown-menu {
            min-width: 120%;
            margin: 0.125rem 0 0;
            list-style: none;
            transform: translate3d(0px, 0px, 0px)!important;
            max-height: 250px;
        }

        .list-group-item {
            position: relative;
            display: block;
            padding: 5px 10px;
            background-color: #fff;
            border: 0px;
        }


            .list-group-item :hover {
                background-color: #dadada;
                padding: 4px 7px;
            }

         .dropdown-menu {
             max-width:100%;
             min-width:auto !important;
         }
    </style>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">DİGƏR ƏMƏLİYYATLAR</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="viewListOperation" runat="server">
                        <!--List operation-->
                        <div class="row">
                            <div class="col-sm-12">
                                <div style="float: left; width: 50%; text-align: left">
                                    <ul class="list-inline">
                                        <li>
                                            <asp:LinkButton ID="lnkNewTransactions" OnClick="lnkNewTransactions_Click" runat="server">
                                          <i class="glyphicon glyphicon-transfer"></i><span> Yeni əməliyyat</span>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                                <div style="float: right; text-align: right; width: 50%">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="drlBranch" class="form-control input-sm" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group">
                                                <asp:TextBox ID="txtStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="input-group">
                                                <asp:TextBox ID="txtEndDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <img id="search_tran_data" style="display: none" src="../img/loader1.gif" />
                                            <asp:Button ID="btnSearchTran" CssClass="btn btn-default btn-quirk btn-sm"
                                                Style="margin: 0; width: 100%" runat="server" Text="YENİLƏ"
                                                OnClick="btnSearchTran_Click"
                                                OnClientClick="this.style.display = 'none';
                                            document.getElementById('search_tran_data').style.display = '';" />
                                        </div>
                                    </div>
                                </div>
                                <div style="clear: both"></div>
                            </div>

                            <div class="mb40"></div>
                            <div class="mb40"></div>
                            <div class="table-responsive">
                                <asp:GridView ID="grdTransactions" class="table table-bordered table-primary table-striped nomargin" runat="server"
                                    AutoGenerateColumns="False" GridLines="None">
                                    <Columns>
                                        <asp:TemplateField HeaderText="TARİX/FİLİAL">
                                            <ItemTemplate>
                                                <%#Eval("TranDate")%>
                                                <br />
                                                <%#Eval("BranchName")%>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="DTAccount" ItemStyle-Width="120" HeaderText="DEBET" />
                                        <asp:BoundField DataField="DTSubAccount"  HeaderText="SUB HESAB" />
                                        <asp:BoundField DataField="CTAccount" ItemStyle-Width="120" HeaderText="KREDİT" />
                                        <asp:BoundField DataField="CTSubAccount" HeaderText="SUB HESAB" />
                                        <asp:BoundField DataField="CurrencyName" ItemStyle-Width="100" HeaderText="VALYUTA" />
                                        <asp:BoundField DataField="Amount" ItemStyle-Width="100" HeaderText="MƏBLƏĞ" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDeleteTranModal"
                                                    OnClick="lnkDeleteTranModal_Click"
                                                    CommandArgument='<%#Eval("MsgID")%>'
                                                    ToolTip="Sil" runat="server">
                                                    <span class ="glyphicon glyphicon-trash"></span>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" Width="70px" />
                                            <HeaderStyle />
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>Əməliyyat tapılmadı...</EmptyDataTemplate>
                                    <HeaderStyle CssClass="bg-blue" />
                                </asp:GridView>
                                <asp:HiddenField ID="hdnSelectedTranMsgID" runat="server" />
                            </div>


                        </div>
                    </asp:View>
                    <asp:View ID="viewOperation" runat="server">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs nav-line">
                                    <li style="background-color: #2599ab; height: 27px; padding-top: 5px">
                                        <asp:LinkButton ID="lnkBackToMainView" OnClick="lnkBackToMainView_Click"
                                            ToolTip="Geri" runat="server">
                                            <img src="../img/back2.png"/></asp:LinkButton></li>
                                    <li class="active"></li>
                                </ul>
                                <div class="tab-content mb20">
                                    <div class="tab-pane active">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            Əməliyyat tarixi:
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtOperationDate" autocomplete="off" class="form-control datepicker" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            DT Hesab qrupu:
                                                           <asp:DropDownList ID="drlDTAccountGroup"
                                                               OnSelectedIndexChanged="drlDTAccountGroup_SelectedIndexChanged"
                                                               AutoPostBack="true" class="form-control" runat="server">
                                                           </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            DT Hesab alt qrupu:
                                                            <asp:DropDownList ID="drlDTAccountSubGroup"
                                                                OnSelectedIndexChanged="drlDTAccountSubGroup_SelectedIndexChanged"
                                                                class="form-control" AutoPostBack="true" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            DT Hesab adı:
                                                            <asp:DropDownList ID="drlDTAccountSubSubGroup"
                                                                AutoPostBack="true"
                                                                OnSelectedIndexChanged="drlDTAccountSubSubGroup_SelectedIndexChanged"
                                                                class="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            DT Hesab:
                                                            <asp:DropDownList ID="drlDTAccount" 
                                                                 class="selectpicker form-control"
                                                                 data-live-search="true"
                                                                 data-hide-disabled="true"
                                                                runat="server"></asp:DropDownList>
                                                        </div>

                                                        <div class="form-group">
                                                            Məbləğ:
                                                        <asp:TextBox ID="txtAmount" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            Təyinat:
                                                            <asp:DropDownList ID="drlNarrative" class="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            KT Hesab qrupu:
                                                            <asp:DropDownList ID="drlKTAccountGroup"
                                                                OnSelectedIndexChanged="drlKTAccountGroup_SelectedIndexChanged"
                                                                AutoPostBack="true" class="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            KT Hesab alt qrupu:
                                                             <asp:DropDownList ID="drlKTAccountSubGroup"
                                                                 OnSelectedIndexChanged="drlKTAccountSubGroup_SelectedIndexChanged"
                                                                 class="form-control" AutoPostBack="true" runat="server">
                                                             </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            KT Hesab adı:
                                                         <asp:DropDownList ID="drlKTAccountSubSubGroup"
                                                             OnSelectedIndexChanged="drlKTAccountSubSubGroup_SelectedIndexChanged"
                                                             AutoPostBack="true"
                                                             class="form-control" runat="server">
                                                         </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            KT Hesab:
                                                            <asp:DropDownList ID="drlKTAccount" 
                                                                 class="selectpicker form-control"
                                                                 data-live-search="true"
                                                                 data-hide-disabled="true"
                                                                runat="server"></asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            Qeyd:
                                                            <asp:TextBox ID="txtNote" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                        </div>

                                                        <div class="form-group" style="text-align: right">
                                                            <img id="create_transaction_loading" style="display: none" src="../img/loader1.gif" />
                                                            <asp:Button ID="btnCreateTransactions"
                                                                OnClick="btnCreateTransactions_Click"
                                                                class="btn btn-primary" runat="server" Text="Təsdiq et"
                                                                OnClientClick="this.style.display = 'none';
                                                                document.getElementById('create_transaction_loading').style.display = '';" />
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>
    </div>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  Alert-->
            <div class="modal bounceIn" id="modalDeleteTranAlert" data-backdrop="static">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalDeleteTranAlertLabel">
                                <i class="fa fa-trash"></i>&nbsp;Əməliyyatın silinməsi
                            </h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                Əməliyyat silinsin?
                            </p>
                        </div>
                        <div class="modal-footer">
                            <img id="delete_transaction_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnDeleteTransaction" class="btn btn-primary" runat="server" Text="Bəli"
                                OnClick ="btnDeleteTransaction_Click"
                                OnClientClick="this.style.display = 'none';
                                                   document.getElementById('delete_transaction_loading').style.display = '';
                                                   document.getElementById('btnDeleteTransactionCancel').style.display = 'none';" />
                            <button id="btnDeleteTransactionCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDeleteTransaction" />
        </Triggers>
    </asp:UpdatePanel>

    <script>

        function openDeleteTransactionModal() {
            $('#modalDeleteTranAlert').modal({ show: true });
        }

    </script>

    <style>
        .input-sm {
            height: 26px !important;
            line-height: 26px !important;
        }

        .input-group-addon {
            padding: 5px 7px !important;
        }

        .form-group {
            margin-bottom: 10px;
        }

        .no_visible_font {
            color: #ffffff;
        }

        .nav-line-danger > li.active > a,
        .nav-line-danger > li.active > a:hover,
        .nav-line-danger > li.active > a:focus {
            color: #d9534f !important;
            background-color: transparent;
            -webkit-box-shadow: 0 1px 0 #d9534f !important;
            box-shadow: 0 1px 0 #d9534f !important;
        }
    </style>


</asp:Content>
