﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code._Config;
using static MCS.App_Code.Enums;

namespace MCS.Customer_list
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsCustomerController.CustomerController wsCController = new wsCustomerController.CustomerController();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();
        _Config _config = new _Config();

        DataTable dtCustomers = new DataTable();
        int pageIndex = 0;
        int recordSize = 0;
        wsCustomerController.GetCustomerRequest reqCustomer = new wsCustomerController.GetCustomerRequest();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];

            if (!IsPostBack)
            {
                reqCustomer.pageIndex = 1;
                reqCustomer.pageSize = Convert.ToInt32(_config.GetAppSetting("GridPageSize"));
                reqCustomer.Fullname = "";
                reqCustomer.Pin = "";
                reqCustomer.IdCardNumber = "";

                Session["GridCustomersPageIndex"] = 0;
                pageIndex = Convert.ToInt32(Session["GridCustomersPageIndex"]);

                FillCustomer(pageIndex,"","","");
            }

            #region Begin Permission
            CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.CUSTOMER_LIST));
            if (grdCustomers.Rows.Count > 0)
            {
                grdCustomers.Columns[5].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.CUSTOMER_EDIT));
                grdCustomers.Columns[6].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.CUSTOMER_DELETE));
            }
            #endregion

        }

        void FillCustomer(int pageIndex,string fullname,string pin , string idCardNumber)
        {
            reqCustomer.pageIndex = (pageIndex + 1);
            reqCustomer.pageSize = Convert.ToInt32(_config.GetAppSetting("GridPageSize"));
            reqCustomer.Fullname = fullname;
            reqCustomer.Pin = pin;
            reqCustomer.IdCardNumber = idCardNumber;
            reqCustomer.userID = UserSession.UserId;
            dtCustomers = wsCController.GetCustomers(reqCustomer, out recordSize);

            grdCustomers.VirtualItemCount = recordSize;
            grdCustomers.PageIndex = pageIndex;
            grdCustomers.PageSize = Convert.ToInt32(_config.GetAppSetting("GridPageSize"));
            grdCustomers.DataSource = dtCustomers;
            grdCustomers.DataBind();
        }

        void FillEditData(int customerId)
        {
            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial seçin", ""));
            }

            //country
            DataTable dtCountry = wsGController.CountryList();
            drlCountry.DataTextField = "CountryName";
            drlCountry.DataValueField = "CountryID";
            drlCountry.DataSource = dtCountry;
            drlCountry.DataBind();
            if (dtCountry.Rows.Count > 1)
            {
                drlCountry.Items.Insert(0, new ListItem("Ölkə seçin", "0"));
            }


            //country
            DataTable dtCity = wsGController.CityList();
            drlCity.DataTextField = "CityName";
            drlCity.DataValueField = "CityID";
            drlCity.DataSource = dtCity;
            drlCity.DataBind();
            if (dtCity.Rows.Count > 1)
            {
                drlCity.Items.Insert(0, new ListItem("Şəhər seçin", "0"));
            }


            DataTable dtUserData =  wsCController.GetCustomerByID(customerId);

            if (dtUserData.Rows.Count > 0)
            {
                drlBranch.SelectedValue = Convert.ToString(dtUserData.Rows[0]["BranchID"]);
                txtSurname.Text = Convert.ToString(dtUserData.Rows[0]["Surname"]);
                txtName.Text = Convert.ToString(dtUserData.Rows[0]["Name"]);
                txtPatronymic.Text = Convert.ToString(dtUserData.Rows[0]["Patronymic"]);
                txtCardNumber.Text = Convert.ToString(dtUserData.Rows[0]["IdCardNumber"]);
                if (Convert.ToString(dtUserData.Rows[0]["IdCardSeries"]).Trim() != "")
                {
                    drlCardSerie.SelectedValue = Convert.ToString(dtUserData.Rows[0]["IdCardSeries"]).Trim();
                }
                txtPin.Text = Convert.ToString(dtUserData.Rows[0]["Pin"]);
                txtDateOfIssue.Text = Convert.ToString(dtUserData.Rows[0]["DateOfIssue"]);
                txtDateOfExpiry.Text = Convert.ToString(dtUserData.Rows[0]["DateOfExpiry"]);
                txtDateOfBirth.Text = Convert.ToString(dtUserData.Rows[0]["DateOfBirth"]);
                txtAuthority.Text = Convert.ToString(dtUserData.Rows[0]["Authority"]);
                drlCustomerGender.SelectedValue = Convert.ToString(dtUserData.Rows[0]["Gender"]);
                drlCountry.SelectedValue = Convert.ToString(dtUserData.Rows[0]["CountryID"]);
                drlCity.SelectedValue = Convert.ToString(dtUserData.Rows[0]["CityID"]);
                txtLegalAddress.Text = Convert.ToString(dtUserData.Rows[0]["LegalAddress"]);
                txtRegistrationAddress.Text = Convert.ToString(dtUserData.Rows[0]["RegistrationAddress"]);
                txtMobilePhone1.Text = Convert.ToString(dtUserData.Rows[0]["MobilePhone1"]);
                txtMobilePhone2.Text = Convert.ToString(dtUserData.Rows[0]["MobilePhone2"]);
                txtMobilePhone3.Text = Convert.ToString(dtUserData.Rows[0]["MobilePhone3"]);
                txtHomePhone.Text = Convert.ToString(dtUserData.Rows[0]["HomePhone"]);
                txtNote.Text = Convert.ToString(dtUserData.Rows[0]["Note"]);
                txtEmail.Text = Convert.ToString(dtUserData.Rows[0]["Email"]);
            }
        }

        protected void grdCustomers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Session["GridCustomersPageIndex"] = e.NewPageIndex;
            string _fullname = txtSearchFullname.Text;
            string _idCardNumber = txtSearchIdCardNumber.Text;
            string _pin = txtSearchPin.Text;
            FillCustomer(Convert.ToInt32(Session["GridCustomersPageIndex"]), _fullname, _pin, _idCardNumber);
        }

        protected void lnkNewCustomer_Click(object sender, EventArgs e)
        {
            _Config.Rd("/customer_new");

        }

        protected void lnkEditCustomerData_Click(object sender, EventArgs e)
        {
            LinkButton lnkDocumentsDataEdit = (LinkButton)sender;
            hdnCustomerID.Value = lnkDocumentsDataEdit.CommandArgument;
            MultiView1.ActiveViewIndex = 1;
            FillEditData(Convert.ToInt32(hdnCustomerID.Value));
        }

        protected void btnUpdateCustomerCancel_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
        }

        protected void btnUpdateCustomer_Click(object sender, EventArgs e)
        {
            int customerId = Convert.ToInt32(hdnCustomerID.Value);

            if (drlBranch.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Filial seçin!");
                return;
            }
            if (txtSurname.Text.Trim() == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Soyad daxil edin.");
                return;
            }
            if (txtName.Text.Trim() == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Ad daxil edin.");
                return;
            }
            if (txtPatronymic.Text.Trim() == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Ata adı daxil edin!");
                return;
            }
            if (txtCardNumber.Text.Trim() == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Sənəd nömrəsini daxil edin!");
                return;
            }
            if (txtPin.Text.Trim() == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "FİN kodu daxil edin!");
                return;
            }
            if (txtDateOfIssue.Text.Trim().Length != 10)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Sənədin verilmə tarixini daxil edin!");
                return;
            }

            if (txtDateOfExpiry.Text.Trim().Length != 10)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Sənədin bitmə tarixini daxil edin!");
                return;
            }
            if (txtMobilePhone1.Text.Trim().Length < 7)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Mobil nömrəni daxil edin!");
                return;
            }

            wsCustomerController.ModifyCustomerRequest request = new wsCustomerController.ModifyCustomerRequest();

            request.CustomerID = Convert.ToInt32(hdnCustomerID.Value);
            request.BranchID = Convert.ToInt32(drlBranch.SelectedValue);
            request.Surname = txtSurname.Text;
            request.Name = txtName.Text;
            request.Patronymic = txtPatronymic.Text;
            request.Pin = txtPin.Text;
            request.IdCardSeries = drlCardSerie.SelectedValue;
            request.IdCardNumber = txtCardNumber.Text;
            request.DateOfIssue = txtDateOfIssue.Text;
            request.DateOfExpiry = txtDateOfExpiry.Text;
            request.DateOfBirth = txtDateOfBirth.Text;
            request.Authority = txtAuthority.Text;
            request.Gender = drlCustomerGender.SelectedValue;
            request.CountryID = drlCountry.SelectedValue == "0" ? 0 : Convert.ToInt32(drlCountry.SelectedValue);
            request.CityID = drlCity.SelectedValue == "0" ? 0 : Convert.ToInt32(drlCity.SelectedValue);
            request.LegalAddress = txtLegalAddress.Text;
            request.RegistrationAddress = txtRegistrationAddress.Text;
            request.MobilePhone1 = txtMobilePhone1.Text;
            request.MobilePhone2 = txtMobilePhone2.Text;
            request.MobilePhone3 = txtMobilePhone3.Text;
            request.HomePhone = txtHomePhone.Text;
            request.Note = txtNote.Text;
            request.Email = txtEmail.Text;
            request.ModifiedID = UserSession.UserId;

            wsCustomerController.ModifyCustomerResponse response = wsCController.ModifyCustomer(request);
            if (!response.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + response.errorCode);
                return;
            }

            _config.AlertMessage(this, MessageType.SUCCESS, "Müştəri məlumatları yeniləndi.");
            pageIndex = Convert.ToInt32(Session["GridCustomersPageIndex"]);

            string _fullname = txtSearchFullname.Text;
            string _idCardNumber = txtSearchIdCardNumber.Text;
            string _pin = txtSearchPin.Text;
            FillCustomer(pageIndex,_fullname,_pin,_idCardNumber);
            MultiView1.ActiveViewIndex = 0;


        }

        protected void lnkDeleteCustomerData_Click(object sender, EventArgs e)
        {
            ltrAlertModalHeaderLabel.Text = "<i class=\"fa fa-trash\"></i>&nbspMüştərinin silinməsi</h4>";
            ltrAlertModalBodyLabel.Text = "Müştəri silinsin?";
            LinkButton lnk = (LinkButton)sender;
            hdnCustomerID.Value =  lnk.CommandArgument;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }

        protected void btnDeleteCustomer_Click(object sender, EventArgs e)
        {
            wsCustomerController.DeleteCustomerRequest req = new wsCustomerController.DeleteCustomerRequest();
            req.CustomerID = Convert.ToInt32(hdnCustomerID.Value);
            wsCustomerController.DeleteCustomerResponse res = wsCController.DeleteCustomer(req);
            if (!res.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + res.errorCode);
                return;
            }
            _config.AlertMessage(this, MessageType.SUCCESS, "Müştəri silindi.");

            pageIndex = Convert.ToInt32(Session["GridCustomersPageIndex"]);

            string _fullname = "";
            string _idCardNumber = "";
            string _pin = "";
            FillCustomer(pageIndex, _fullname, _pin, _idCardNumber);
        }

        protected void lnkSearchCustomer_Click(object sender, EventArgs e)
        {
            txtSearchFullname.Text = "";
            txtSearchIdCardNumber.Text = "";
            txtSearchPin.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openSearchModal();", true);
        }

        protected void btnSearchCustomer_Click(object sender, EventArgs e)
        {
            string _fullname = txtSearchFullname.Text;
            string _idCardNumber = txtSearchIdCardNumber.Text;
            string _pin = txtSearchPin.Text;
            pageIndex = 0;
            Session["GridCustomersPageIndex"] = "0";
            FillCustomer(pageIndex, _fullname, _pin, _idCardNumber);
        
        }
    }
}