﻿using MCS.App_Code;
using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using WordToPDF;
using static MCS.App_Code._Config;
using static MCS.App_Code.Enums;

namespace MCS.Portfel_list
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();
        MCS.wsTransactionController.TransactionController wsTController = new wsTransactionController.TransactionController();
        MCS.wsAccountController.AccountController wsAController = new wsAccountController.AccountController();
        MCS.wsTemplateController.TemplateController wsTeController = new wsTemplateController.TemplateController();
        MCS.wsUserController.UserController wsUController = new wsUserController.UserController();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                fillData();
                txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                txtEndDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");

                wsAccountController.GetPortfolioListRequest Req = new wsAccountController.GetPortfolioListRequest();
                Req.DateFrom = txtStartDate.Text;
                Req.DateTo = txtEndDate.Text;
                Req.CurrencyID = Convert.ToInt32(drlCurrency.SelectedValue);
                Req.BranchID = Convert.ToInt32(drlBranch.SelectedValue);
                Req.UserID = UserSession.UserId;
                fillGrid(Req);
            }

            #region Begin Permission
            CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.PORTFOLIO_LIST));
            #endregion

        }

        void fillData()
        {
            DataTable dtCurrency = wsGController.CurrencyList();
            if (dtCurrency.Rows.Count > 0)
            {
                drlCurrency.Items.Clear();
                drlCurrency.DataTextField = "CurrencyName";
                drlCurrency.DataValueField = "CurrencyID";
                drlCurrency.DataSource = dtCurrency;
                drlCurrency.DataBind();
                drlCurrency.SelectedValue = "1";
            }

            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial", "0"));
            }
        }


        void fillGrid(wsAccountController.GetPortfolioListRequest Req)
        {
            if (!_config.isDate(Req.DateFrom))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Başlanğıc tarixi düzgün daxil edin!");
                return;
            }
            if (!_config.isDate(Req.DateTo))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Son tarixi düzgün daxil edin!");
                return;
            }

            DataTable dtPortfolioBalance = new DataTable();
            DataTable dt = wsAController.GetPortfolioList(Req, out dtPortfolioBalance);

            if (dt == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!");
                return;
            }

            if (dtPortfolioBalance == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.!");
                return;
            }

            grdPortfolioBalance.DataSource = dtPortfolioBalance;
            grdPortfolioBalance.DataBind();

            if (dtPortfolioBalance.Rows.Count > 0)
            {
                GridViewRow drr = grdPortfolioBalance.FooterRow;
                Label lblGrdBeginBalanceSum = (Label)drr.FindControl("lblGrdBeginBalanceSum");
                Label lblGrdEndBalanceSum = (Label)drr.FindControl("lblGrdEndBalanceSum");

                double BeginBalanceSum = 0;
                double EndBalanceSum = 0;

                for (int i = 0; i < grdPortfolioBalance.Rows.Count; i++)
                {
                    BeginBalanceSum += Convert.ToDouble(((Label)grdPortfolioBalance.Rows[i].FindControl("lblGrdBeginBalance")).Text);
                    EndBalanceSum += Convert.ToDouble(((Label)grdPortfolioBalance.Rows[i].FindControl("lblGrdEndBalance")).Text);
                }
                lblGrdBeginBalanceSum.Text = BeginBalanceSum.ToString("0.00");
                lblGrdEndBalanceSum.Text = EndBalanceSum.ToString("0.00");
            }

            grdTransactions.DataSource = dt;
            grdTransactions.DataBind();
            if (grdTransactions.Rows.Count > 0)
            {
                GridViewRow drr = grdTransactions.FooterRow;
                Label lblGrdTransactionsDT_AmountSum = (Label)drr.FindControl("lblGrdTransactionsDT_AmountSum");
                Label lblGrdTransactionsCT_AmountSum = (Label)drr.FindControl("lblGrdTransactionsCT_AmountSum");

                Label lblGrdTransactionsCurrencySum = (Label)drr.FindControl("lblGrdTransactionsCurrencySum");

                double DT_AmountSum = 0;
                double CT_AmountSum = 0;
                string ccy = "";

                for (int i = 0; i < grdTransactions.Rows.Count; i++)
                {
                    DT_AmountSum += Convert.ToDouble(((Label)grdTransactions.Rows[i].FindControl("lblGrdTransactionsDT_Amount")).Text);
                    CT_AmountSum += Convert.ToDouble(((Label)grdTransactions.Rows[i].FindControl("lblGrdTransactionsCT_Amount")).Text);
                    ccy = Convert.ToString(grdTransactions.DataKeys[0].Values["CurrencyName"]);
                }
                lblGrdTransactionsDT_AmountSum.Text = DT_AmountSum.ToString("0.00");
                lblGrdTransactionsCT_AmountSum.Text = CT_AmountSum.ToString("0.00");

                lblGrdTransactionsCurrencySum.Text = ccy;
            }

        }

        protected void btnSearchTran_Click(object sender, EventArgs e)
        {
            wsAccountController.GetPortfolioListRequest Req = new wsAccountController.GetPortfolioListRequest();
            Req.DateFrom = txtStartDate.Text;
            Req.DateTo = txtEndDate.Text;
            Req.CurrencyID = Convert.ToInt32(drlCurrency.SelectedValue);
            Req.BranchID = Convert.ToInt32(drlBranch.SelectedValue);
            Req.UserID = UserSession.UserId;
            fillGrid(Req);
        }
    }
}