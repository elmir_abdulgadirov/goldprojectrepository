﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WordToPDF;
using static MCS.App_Code._Config;
using static MCS.App_Code.Enums;

namespace MCS.Balance_ByAccount
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsTransactionController.TransactionController wsTController = new wsTransactionController.TransactionController();
        MCS.wsAccountController.AccountController wsAController = new wsAccountController.AccountController();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                ViewState["GetAcntGroupedBalance"] = null;
                ViewState["DateFrom"] = null;
                ViewState["DateTo"] = null;
                ViewState["BranchID"] = null;
                ViewState["CurrencyID"] = null;
                ViewState["UserID"] = null;
                txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                txtEndDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                FillAndClearForm();
            }

            #region Begin Permission
             CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.BALANCE_BY_ACCOUNT));
             lnkDownloadResult.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.PRINT_BY_ACCOUNT_BALANCE));
            #endregion
        }

        void FillAndClearForm()
        {
            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial", "0"));
            }


            // Account group
            DataTable dtAccountGroup = wsAController.GetAccountGroupList();
            drlDTAccountGroup.DataTextField = "Name";
            drlDTAccountGroup.DataValueField = "Code";
            drlDTAccountGroup.DataSource = dtAccountGroup;
            drlDTAccountGroup.DataBind();
            drlDTAccountGroup.Items.Insert(0, new ListItem("Hesab seçin", "-1"));


            //Currency
            DataTable dtCurrency = wsGController.CurrencyList();
            if (dtCurrency.Rows.Count > 0)
            {
                drlCurrency.Items.Clear();
                drlCurrency.DataTextField = "CurrencyName";
                drlCurrency.DataValueField = "CurrencyID";
                drlCurrency.DataSource = dtCurrency;
                drlCurrency.DataBind();
                drlCurrency.SelectedValue = "1";
            }
        }

        void fillGrid(wsAccountController.GetAcntGroupedBalanceRequest req)
        {
            if (req.BranchID == 0)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Filial seçin");
                return;
            }
            if (req.AcntGroupCode.Trim() == "-1")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Hesab seçin");
                return;
            }
            if (!_config.isDate(req.DateFrom))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Başlanğıc tarixi düzgün daxil edin!");
                return;
            }
            if (!_config.isDate(req.DateTo))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Son tarixi düzgün daxil edin!");
                return;
            }
            
           
            DataTable dt = wsAController.GetAcntGroupedBalance(req);
            if (dt == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }

            DataTable dt_clone = new DataTable();
            dt_clone.TableName = "CloneTable";
            dt_clone = dt.Clone();
            dt_clone.Columns.Add("SortOrder", typeof(int));
            decimal DtToPeriod = 0, CtToPeriod = 0, DtInPeriod = 0, CtInPeriod = 0, DtOutPeriod = 0, CtOutPeriod = 0;
            foreach (DataRow dr in dt.Rows)
            {
                dt_clone.Rows.Add(
                    Convert.ToInt32(dr["BranchID"]),
                    Convert.ToString(dr["BranchName"]),
                    Convert.ToString(dr["AcntSubGroupCode"]),
                    Convert.ToString(dr["Description"]),
                    _config.ToDecimal(dr["DtToPeriod"]),
                    _config.ToDecimal(dr["CtToPeriod"]),
                    _config.ToDecimal(dr["DtInPeriod"]),
                    _config.ToDecimal(dr["CtInPeriod"]),
                    _config.ToDecimal(dr["DtOutPeriod"]),
                    _config.ToDecimal(dr["CtOutPeriod"]),
                    2
                    );
                DtToPeriod += _config.ToDecimal(dr["DtToPeriod"]);
                CtToPeriod += _config.ToDecimal(dr["CtToPeriod"]);
                DtInPeriod += _config.ToDecimal(dr["DtInPeriod"]);
                CtInPeriod += _config.ToDecimal(dr["CtInPeriod"]);
                DtOutPeriod += _config.ToDecimal(dr["DtOutPeriod"]);
                CtOutPeriod += _config.ToDecimal(dr["CtOutPeriod"]);
            }
            if (dt_clone.Rows.Count > 0)
            {
                dt_clone.Rows.Add(
                       0,
                       "",
                       "",
                       "",
                       DtToPeriod,
                       CtToPeriod,
                       DtInPeriod,
                       CtInPeriod,
                       DtOutPeriod,
                       CtOutPeriod,
                       1
                       );
            }

            DataView dv = dt_clone.DefaultView;
            dv.Sort = "SortOrder asc";
            DataTable sortedDT = dv.ToTable();

            grdTransactions.DataSource = sortedDT;
            grdTransactions.DataBind();
            ltrDataCount.Text = dt.Rows.Count.ToString();
            if (grdTransactions.Rows.Count > 0)
            {
                grdTransactions.Rows[0].Cells[2].Style.Add("font-weight", "bold");
                grdTransactions.Rows[0].Cells[3].Style.Add("font-weight", "bold");
                grdTransactions.Rows[0].Cells[4].Style.Add("font-weight", "bold");
                grdTransactions.Rows[0].Cells[5].Visible = false;
            }
            ViewState["GetAcntGroupedBalance"] = dt;
        }

        //search
        protected void btnSearchTran_Click(object sender, EventArgs e)
        {
            wsAccountController.GetAcntGroupedBalanceRequest req = new wsAccountController.GetAcntGroupedBalanceRequest();
            req.DateFrom = txtStartDate.Text;
            req.DateTo = txtEndDate.Text;
            req.AcntGroupCode = drlDTAccountGroup.SelectedValue;
            req.BranchID = int.Parse(drlBranch.SelectedValue);
            req.CurrencyID = int.Parse(drlCurrency.SelectedValue);
            req.AcntSubGroupCode = txtSubAccount.Text.Trim();
            req.UserID = UserSession.UserId;
            ViewState["DateFrom"] = req.DateFrom;
            ViewState["DateTo"] = req.DateTo;
            ViewState["BranchID"] = req.BranchID;
            ViewState["CurrencyID"] = req.CurrencyID;
            ViewState["UserID"] = req.UserID;
            fillGrid(req);
        }

        //print main grid
        protected void lnkDownloadResult_Click(object sender, EventArgs e)
        {
            if (ViewState["GetAcntGroupedBalance"] == null)
            {
                _config.AlertMessage(this, MessageType.WARNING, "Axtarışı yeniləyin.");
                return;
            }
            DataTable dt = (DataTable)ViewState["GetAcntGroupedBalance"];
            if (dt.Rows.Count > 0)
            {
                PrintData(dt);
            }
        }

        void PrintData(DataTable dt)
        {
            // print datatable
            string f_t_name = Server.MapPath(@"~/DocumentTemplates/templates/GetAcntGroupedBalance.docx");
            string f_o_name = Server.MapPath("~/DocumentTemplates/" + "GetAcntGroupedBalance" + Guid.NewGuid().ToString() + ".docx");

            // copy file from template
            System.IO.File.Copy(f_t_name, f_o_name);

            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(f_o_name);

            bookMarks bookmarks = new bookMarks();

            if (drlBranch.SelectedValue == "0")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Filialı seçin");
                return;
            }

            //BranchName
            bookmarks.key = "BranchName";
            bookmarks.value = drlBranch.SelectedItem.Text;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //PrintDate
            bookmarks.key = "PrintDate";
            bookmarks.value = _Config.HostingTime.ToString("dd.MM.yyyy HH:mm");
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            string GetAcntGroupedBalanceDataTableHtml = File.ReadAllText(Server.MapPath(@"~/DocumentTemplates/templates/GetAcntGroupedBalanceDataTable.txt"));
            string GetAcntGroupedBalanceDataTableHtmlBody = "";

            decimal DtToPeriod = 0, CtToPeriod = 0, DtInPeriod = 0, CtInPeriod = 0, DtOutPeriod = 0, CtOutPeriod = 0;

            foreach (DataRow dr in dt.Rows)
            {
                GetAcntGroupedBalanceDataTableHtmlBody += $@"<tr>
                    <td>{Convert.ToString(dr["AcntSubGroupCode"])}</td>
                    <td>{Convert.ToString(dr["Description"])}</td>
                    <td>
                          <div style=""width:220px;text-align:center"">
                            <div style=""width:100%"">
                                    <div style=""width:50%;float:left"">"+dr["DtToPeriod"]+ $@"</div>
                                    <div style=""width:50%;float:left"">"+dr["CtToPeriod"]+ $@"</div>
                                    <div style=""clear:both""></div>
                            </div>
                          </div>
                     </td>
                    <td>
                      <div style=""width:220px;text-align:center"">
                            <div style=""width:100%"">
                                    <div style=""width:50%;float:left"">" + dr["DtInPeriod"] + $@"</div>
                                    <div style=""width:50%;float:left"">" + dr["CtInPeriod"] + $@"</div>
                                    <div style=""clear:both""></div>
                            </div>
                       </div>
                    </td>
                    <td>
                       <div style=""width: 220px; text-align:center"">
                            <div style = ""width:100%"">
                                    <div style=""width:50%;float:left"">" + dr["DtOutPeriod"] + $@"</div>
                                    <div style=""width:50%;float:left"">" + dr["CtOutPeriod"] + $@"</div>
                                    <div style=""clear:both""></div>
                            </div>
                       </div>
                    </td>
                    </tr>";

                DtToPeriod += _config.ToDecimal(dr["DtToPeriod"]);
                CtToPeriod += _config.ToDecimal(dr["CtToPeriod"]);
                DtInPeriod += _config.ToDecimal(dr["DtInPeriod"]);
                CtInPeriod += _config.ToDecimal(dr["CtInPeriod"]);
                DtOutPeriod += _config.ToDecimal(dr["DtOutPeriod"]);
                CtOutPeriod += _config.ToDecimal(dr["CtOutPeriod"]);
            }
            if (GetAcntGroupedBalanceDataTableHtmlBody != "") //fill footer
            {
                GetAcntGroupedBalanceDataTableHtmlBody += $@"<tr>
                                                      <td><b>Say : " + dt.Rows.Count + $@"<b></td>
                                                      <td></td>
                                                      <td>
                                                          <div style=""width:220px;text-align:center"">
                                                            <div style=""width: 100%"">
                                                                    <div style=""width:50%;float:left""><b>{DtToPeriod}</b></div>
                                                                    <div style=""width:50%;float:left""><b>{CtToPeriod}</b></div>
                                                                    <div style=""clear:both""></div>
                                                            </div>
                                                          </div>
                                                      </td>
                                                      <td>
                                                          <div style=""width: 220px;text-align:center"">
                                                            <div style = ""width:100%"">
                                                                    <div style=""width:50%;float:left""><b>{DtInPeriod}</b></div>
                                                                    <div style=""width:50%;float:left""><b>{CtInPeriod}</b></div>
                                                                    <div style=""clear:both""></div>
                                                            </div>
                                                          </div>
                                                      </td>
                                                      <td>
                                                          <div style=""width:220px;text-align:center"">
                                                            <div style=""width:100%"">
                                                                    <div style = ""width:50%;float:left""><b>{DtOutPeriod}</b></div>
                                                                    <div style = ""width:50%;float:left""><b>{CtOutPeriod}</b></div>
                                                                    <div style = ""clear:both""></div>
                                                            </div>
                                                          </div>
                                                      </td>
                                                      </tr>";
            }
            string _GetAcntGroupedBalanceDataTableHtml = GetAcntGroupedBalanceDataTableHtml
                                             .Replace("{0}", GetAcntGroupedBalanceDataTableHtmlBody);

            string _htmlGeneral = @"<head><meta charset=""UTF-8""><style>table, td, th {  border: 1px solid black; text-align: left;font-size:14px;}table {border-collapse: collapse;} </style></head><body>{0}</body>";

            _htmlGeneral = _htmlGeneral.Replace("{0}", _GetAcntGroupedBalanceDataTableHtml);

            bookmarks.key = "DataTable";
            bookmarks.value = _htmlGeneral;
            fillTemplateWord(bookmarks, WordBookmarkType.HTML, doc);


            ((Microsoft.Office.Interop.Word._Document)doc).Close();
            ((Microsoft.Office.Interop.Word._Application)app).Quit();

            string pdf_file_name = f_o_name.Replace(".docx", ".pdf");

            Word2Pdf objWordPDF = new Word2Pdf();
            objWordPDF.InputLocation = f_o_name;
            objWordPDF.OutputLocation = pdf_file_name;
            objWordPDF.Word2PdfCOnversion();

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            response.AddHeader("Content-Disposition", "attachment;filename=" + System.IO.Path.GetFileName(pdf_file_name));
            response.WriteFile(pdf_file_name);
            response.Flush();
            response.End();
            try
            {
                System.IO.File.Delete(f_o_name);
                System.IO.File.Delete(pdf_file_name);
            }
            catch
            { }
        }



        //modal details
        protected void lnkDetailTranModal_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            wsAccountController.GetAcntBalanceBySubGroupRequest req = new wsAccountController.GetAcntBalanceBySubGroupRequest();
            req.DateFrom =Convert.ToString(ViewState["DateFrom"]);
            req.DateTo = Convert.ToString(ViewState["DateTo"]);
            req.BranchID = Convert.ToInt32(ViewState["BranchID"]);
            req.CurrencyID = Convert.ToInt32(ViewState["CurrencyID"]);
            req.UserID = Convert.ToInt32(ViewState["UserID"]);
            req.AcntSubGroupCode = lnk.CommandArgument;
            DataTable dtSub = wsAController.GetAcntBalanceBySubGroup(req);
            if (dtSub == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }
            if (dtSub.Rows.Count == 0)
            {
                _config.AlertMessage(this, MessageType.WARNING, "Məlumat tapılmadı.");
                return;
            }

            DataRow mainRow = ((DataTable)ViewState["GetAcntGroupedBalance"]).Select("AcntSubGroupCode = " + req.AcntSubGroupCode)[0];
           

            DataTable dtNew = new DataTable();
            dtNew.Columns.Add("AcntSubGroupCode", typeof(string));
            dtNew.Columns.Add("Description", typeof(string));
            dtNew.Columns.Add("DtToPeriod", typeof(decimal));
            dtNew.Columns.Add("CtToPeriod", typeof(decimal));
            dtNew.Columns.Add("DtInPeriod", typeof(decimal));
            dtNew.Columns.Add("CtInPeriod", typeof(decimal));
            dtNew.Columns.Add("DtOutPeriod", typeof(decimal));
            dtNew.Columns.Add("CtOutPeriod", typeof(decimal));
            dtNew.Columns.Add("OrderSort", typeof(int));
            
            dtNew.Rows.Add(Convert.ToString(mainRow["AcntSubGroupCode"]),
                           Convert.ToString(mainRow["Description"]),
                           _config.ToDecimal(mainRow["DtToPeriod"]),
                           _config.ToDecimal(mainRow["CtToPeriod"]),
                           _config.ToDecimal(mainRow["DtInPeriod"]),
                           _config.ToDecimal(mainRow["CtInPeriod"]),
                           _config.ToDecimal(mainRow["DtOutPeriod"]),
                           _config.ToDecimal(mainRow["CtOutPeriod"]),
                           1);


            foreach (DataRow item in dtSub.Rows)
            {
                dtNew.Rows.Add(Convert.ToString(item["Code"]),
                           Convert.ToString(item["Description"]),
                           _config.ToDecimal(item["DtToPeriod"]),
                           _config.ToDecimal(item["CtToPeriod"]),
                           _config.ToDecimal(item["DtInPeriod"]),
                           _config.ToDecimal(item["CtInPeriod"]),
                           _config.ToDecimal(item["DtOutPeriod"]),
                           _config.ToDecimal(item["CtOutPeriod"]),
                           2);
            }

            DataView dv = dtNew.DefaultView;
            dv.Sort = "OrderSort asc";
            DataTable sortedDT = dv.ToTable();

            ltrDataCount_1.Text = (sortedDT.Rows.Count-1).ToString();
            string _htmlRows = "";
            int ii = 0;
            string _beginBold = "", _endBold = "", _bcColor = ""; ;
            foreach (DataRow dr in sortedDT.Rows)
            {
                if (ii == 0)
                {
                    _beginBold = "<b>";
                    _endBold = "</b>";
                    _bcColor = @" style = ""background-color: #9db5c5;""";
                }
                else
                {
                    _beginBold = "";
                    _endBold = "";
                    _bcColor = "";
                }
                _htmlRows += $@"<tr "+ _bcColor + $@">
                    <td>"+ _beginBold + $@"{Convert.ToString(dr["AcntSubGroupCode"])}" + _endBold+ $@"</td>
                    <td>" + _beginBold + $@"{Convert.ToString(dr["Description"])}" + _endBold + $@"</td>
                    <td>
                          <div style=""width:200px;text-align:center"">
                            <div style=""width:100%"">
                                    <div style=""width:50%;float:left"">" + _beginBold + dr["DtToPeriod"] + _endBold+ $@"</div>
                                    <div style=""width:50%;float:left"">" + _beginBold + dr["CtToPeriod"] + _endBold + $@"</div>
                                    <div style=""clear:both""></div>
                            </div>
                          </div>
                     </td>
                    <td>
                      <div style=""width:200px;text-align:center"">
                            <div style=""width:100%"">
                                    <div style=""width:50%;float:left"">" + _beginBold + dr["DtInPeriod"] + _endBold+ $@"</div>
                                    <div style=""width:50%;float:left"">" + _beginBold + dr["CtInPeriod"] + _endBold + $@"</div>
                                    <div style=""clear:both""></div>
                            </div>
                       </div>
                    </td>
                    <td>
                       <div style=""width: 200px; text-align:center"">
                            <div style = ""width:100%"">
                                    <div style=""width:50%;float:left"">" + _beginBold + dr["DtOutPeriod"] + _endBold+ $@"</div>
                                    <div style=""width:50%;float:left"">" + _beginBold + dr["CtOutPeriod"] + _endBold + $@"</div>
                                    <div style=""clear:both""></div>
                            </div>
                       </div>
                    </td>
                    </tr>";
                ii++;
            }
            ltrHtmlData.Text = _htmlRows;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalDetails();", true);
        }






        // Begin word template

        public static string SaveToTemporaryFile(string html)
        {
            string htmlTempFilePath = Path.Combine(Path.GetTempPath(), string.Format("{0}.html", Path.GetRandomFileName()));
            using (StreamWriter writer = File.CreateText(htmlTempFilePath))
            {
                html = string.Format("<html>{0}</html>", html);
                writer.WriteLine(html);
            }

            return htmlTempFilePath;
        }

        enum WordBookmarkType
        {
            HTML,
            TXT
        }


        private void ReplaceBookmarkText(Microsoft.Office.Interop.Word.Document doc, string bookmarkName, string text, WordBookmarkType type)
        {
            try
            {
                if (doc.Bookmarks.Exists(bookmarkName))
                {
                    Object name = bookmarkName;
                    Microsoft.Office.Interop.Word.Range range = doc.Bookmarks.get_Item(ref name).Range;
                    range.Text = range.Text.Replace(range.Text, text);
                    object newRange = range;
                    if (type == WordBookmarkType.HTML)
                    {
                        range.InsertFile(SaveToTemporaryFile(text), Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    }
                    doc.Bookmarks.Add(bookmarkName, ref newRange);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }



        private void fillTemplateWord(bookMarks bookmarks, WordBookmarkType wordBookmarkType, Microsoft.Office.Interop.Word.Document doc)
        {
            try
            {
                ReplaceBookmarkText(doc, bookmarks.key.ToString(), bookmarks.value.ToString(), wordBookmarkType);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

      

        // End word template

    }


    public class bookMarks
    {
        public object key { get; set; }
        public object value { get; set; }
    }
}