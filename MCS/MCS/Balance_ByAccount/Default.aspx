﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Balance_ByAccount.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--List operation-->
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-2">
                    <asp:DropDownList ID="drlBranch" class="form-control input-sm" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <asp:DropDownList ID="drlDTAccountGroup" class="form-control input-sm" runat="server"></asp:DropDownList>
                </div>
                <div class="col-sm-1">
                    <asp:TextBox ID="txtSubAccount" autocomplete="off" class="form-control input-sm" placeHolder="Alt hesab" runat="server"></asp:TextBox>
                </div>
                <div class="col-sm-1">
                    <asp:DropDownList ID="drlCurrency" class="form-control input-sm" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <div class="input-group">
                        <asp:TextBox ID="txtStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="input-group">
                        <asp:TextBox ID="txtEndDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <img id="search_tran_data" style="display: none" src="../img/loader1.gif" />
                    <asp:Button ID="btnSearchTran" CssClass="btn btn-default btn-quirk btn-sm"
                        Style="margin: 0; width: 100%" runat="server" Text="Axtar"
                        OnClick="btnSearchTran_Click"
                        OnClientClick="this.style.display = 'none';
                                            document.getElementById('search_tran_data').style.display = '';" />
                </div>
            </div>
            <div class="mb20"></div>
            <ul class="list-inline" style="padding-left: 5px">
                <li style="float: left">
                    <span class="label label-black">Məlumat sayı:
                            <asp:Literal ID="ltrDataCount" runat="server"></asp:Literal>
                    </span>
                </li>
                <li style="float: right">
                    <asp:LinkButton ID="lnkDownloadResult" runat="server" OnClick="lnkDownloadResult_Click">
                        <i class="glyphicon glyphicon-print"></i><span> Çap</span>
                    </asp:LinkButton>
                </li>
                <li style="clear: both"></li>
            </ul>
            <div class="mb40"></div>
            <div class="mb40"></div>
            <div class="table-responsive">
                <asp:GridView ID="grdTransactions" class="table table-bordered table-primary table-striped nomargin" runat="server"
                    AutoGenerateColumns="False" GridLines="None">
                    <Columns>
                        <asp:TemplateField HeaderText="HESAB">
                            <ItemTemplate>
                                <%#Eval("AcntSubGroupCode")%>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="80px" />
                            <HeaderStyle />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Description" HeaderText="TƏSVİR" />

                        <asp:TemplateField>
                            <HeaderTemplate>
                                <div style="width: 220px; text-align: center">
                                    <div style="width: 100%">Dövrün əvvəlinə</div>
                                    <div style="width: 100%">
                                        <div style="width: 50%; float: left">DEBET</div>
                                        <div style="width: 50%; float: left">KREDİT</div>
                                        <div style="clear: both"></div>
                                    </div>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div style="width: 220px; text-align: center">
                                    <div style="width: 100%">
                                        <div style="width: 50%; float: left"><%#Eval("DtToPeriod")%></div>
                                        <div style="width: 50%; float: left"><%#Eval("CtToPeriod")%></div>
                                        <div style="clear: both"></div>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="220px" />
                            <HeaderStyle HorizontalAlign="Center" Width="220px" />
                            <HeaderStyle />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                <div style="width: 220px; text-align: center">
                                    <div style="width: 100%">Dövr ərzində</div>
                                    <div style="width: 100%">
                                        <div style="width: 50%; float: left">DEBET</div>
                                        <div style="width: 50%; float: left">KREDİT</div>
                                        <div style="clear: both"></div>
                                    </div>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div style="width: 220px; text-align: center">
                                    <div style="width: 100%">
                                        <div style="width: 50%; float: left"><%#Eval("DtInPeriod")%></div>
                                        <div style="width: 50%; float: left"><%#Eval("CtInPeriod")%></div>
                                        <div style="clear: both"></div>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="220px" />
                            <HeaderStyle HorizontalAlign="Center" Width="220px" />
                            <HeaderStyle />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                <div style="width: 220px; text-align: center">
                                    <div style="width: 100%">Dövrün sonunda</div>
                                    <div style="width: 100%">
                                        <div style="width: 50%; float: left">DEBET</div>
                                        <div style="width: 50%; float: left">KREDİT</div>
                                        <div style="clear: both"></div>
                                    </div>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div style="width: 220px; text-align: center">
                                    <div style="width: 100%">
                                        <div style="width: 50%; float: left"><%#Eval("DtOutPeriod")%></div>
                                        <div style="width: 50%; float: left"><%#Eval("CtOutPeriod")%></div>
                                        <div style="clear: both"></div>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="220px" />
                            <HeaderStyle HorizontalAlign="Center" Width="220px" />
                            <HeaderStyle />
                        </asp:TemplateField>


                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDetailTranModal"
                                    CommandArgument='<%#Eval("AcntSubGroupCode")%>'
                                    ToolTip="Ətraflı" runat="server" OnClick="lnkDetailTranModal_Click">
                                                    <span class ="glyphicon glyphicon-chevron-right"></span>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="50px" />
                            <HeaderStyle />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>Əməliyyat tapılmadı...</EmptyDataTemplate>
                    <HeaderStyle CssClass="bg-blue" />
                </asp:GridView>
            </div>
        </div>

    </div>


    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="modal bounceIn" id="modalDetails" data-backdrop="static">
                <div class="modal-dialog modal-lg" style ="width:1200px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4></h4>
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="list-inline" style="padding-left: 5px; padding-top: 10px">
                                        <li style="float: left">
                                            <span class="label label-black">Məlumat sayı:
                                              <asp:Literal ID="ltrDataCount_1" runat="server"></asp:Literal>
                                            </span>
                                        </li>
                                        <li style="float: right">
                                            <asp:LinkButton ID="lnkDownloadResult_1" runat="server">
                                             <i class="glyphicon glyphicon-print"></i><span> Çap</span>
                                            </asp:LinkButton>
                                        </li>
                                        <li style="clear: both"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-primary">
                                        <thead>
                                            <tr>
                                                <th>HESAB</th>
                                                <th>TƏSVİR</th>
                                                <th>
                                                    <div style="width: 200px; text-align: center">
                                                        <div style="width: 100%">Dövrün əvvəlinə</div>
                                                        <div style="width: 100%">
                                                            <div style="width: 50%; float: left">DEBET</div>
                                                            <div style="width: 50%; float: left">KREDİT</div>
                                                            <div style="clear: both"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th>
                                                    <div style="width: 200px; text-align: center">
                                                        <div style="width: 100%">Dövr ərzində</div>
                                                        <div style="width: 100%">
                                                            <div style="width: 50%; float: left">DEBET</div>
                                                            <div style="width: 50%; float: left">KREDİT</div>
                                                            <div style="clear: both"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th>
                                                    <div style="width: 200px; text-align: center">
                                                        <div style="width: 100%">Dövrün sonunda</div>
                                                        <div style="width: 100%">
                                                            <div style="width: 50%; float: left">DEBET</div>
                                                            <div style="width: 50%; float: left">KREDİT</div>
                                                            <div style="clear: both"></div>
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <asp:Literal ID="ltrHtmlData" runat="server"></asp:Literal>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID ="lnkDownloadResult_1" />
        </Triggers>
    </asp:UpdatePanel>

    <script>
        function openModalDetails() {
            $('#modalDetails').modal({ show: true });
        }
    </script>

    <style>
        .input-sm {
            height: 26px !important;
            line-height: 26px !important;
        }

        .input-group-addon {
            padding: 5px 7px !important;
        }

        .form-group {
            margin-bottom: 10px;
        }

        .label-black {
            background-color: #343a40;
        }

        .label {
            display: inline;
            padding: 0.6em 1em 0.6em;
            font-size: 80%;
            font-weight: bold !important;
            line-height: 1;
            color: #ffffff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: .25em;
        }
    </style>
</asp:Content>
