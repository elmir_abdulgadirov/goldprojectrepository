﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code._Config;
using MCS.App_Code;
using System.Data;
using System.IO;
using WordToPDF;
using static MCS.App_Code.Enums;

namespace MCS.Collateral_out_list
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();
        MCS.wsLoanController.LoanController wsLController = new wsLoanController.LoanController();
        MCS.wsUserController.UserController wsUController = new wsUserController.UserController();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                fillHeaderData();
                wsLoanController.GetCollateralOutListRequest ReqGrid = new wsLoanController.GetCollateralOutListRequest();
                ReqGrid.StartDate = txtStartDate.Text;
                ReqGrid.EndDate = txtEndDate.Text;
                ReqGrid.UserID = UserSession.UserId;
                ReqGrid.BranchID = int.Parse(drlBranch.SelectedValue);
                ReqGrid.DepositBox = txtDepositBox.Text;
                ReqGrid.CollateralNumber = txtCollateralNumber.Text;
                fillGrid(ReqGrid,false,false);
            }


            #region Begin Permission
              CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.COLLATERAL_OUT_LIST));
              if (grdCollateralOutData.Rows.Count > 0)
              { 
                grdCollateralOutData.Columns[6].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.DELETE_COLLATERAL_OUT_LIST));
            }
              lnkDownloadResult.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.PRINT_COLLATERAL_OUT_LIST));
              lnkSendMailResult.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.EMAIL_COLLATERAL_OUT_LIST));
            #endregion
        }

        void fillHeaderData()
        {
            txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
            txtEndDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial", "0"));
            }
        }


        void fillGrid(wsLoanController.GetCollateralOutListRequest Req,bool isPrint,bool isSendMail)
        {
            if (!_config.isDate(Req.StartDate))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Başlanğıc tarixi düzgün daxil edin!");
                return;
            }
            if (!_config.isDate(Req.EndDate))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Son tarixi düzgün daxil edin!");
                return;
            }

            DataTable dtCollateralOutList = wsLController.GetCollateralOutList(Req);

            if (dtCollateralOutList == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!");
                return;
            }

            grdCollateralOutData.DataSource = dtCollateralOutList;
            grdCollateralOutData.DataBind();

            if (grdCollateralOutData.Rows.Count > 0)
            {
                GridViewRow drr = grdCollateralOutData.FooterRow;
                Label lblGrdOutDepositBoxFooterCount = (Label)drr.FindControl("lblGrdOutDepositBoxFooterCount");
                Label lblGrdOutCollateralNumberFooterCount = (Label)drr.FindControl("lblGrdOutCollateralNumberFooterCount");

                lblGrdOutDepositBoxFooterCount.Text = lblGrdOutCollateralNumberFooterCount.Text = grdCollateralOutData.Rows.Count.ToString();
                if (isPrint)
                {
                    PrintAndSendMail(dtCollateralOutList, Req, isSendMail);
                }
                if (isSendMail)
                {
                    PrintAndSendMail(dtCollateralOutList, Req, isSendMail);
                }
            }

            
        }

        protected void btnSearchCollateral_Click(object sender, EventArgs e)
        {
            wsLoanController.GetCollateralOutListRequest ReqGrid = new wsLoanController.GetCollateralOutListRequest();
            ReqGrid.StartDate = txtStartDate.Text;
            ReqGrid.EndDate = txtEndDate.Text;
            ReqGrid.UserID = UserSession.UserId;
            ReqGrid.BranchID = int.Parse(drlBranch.SelectedValue);
            ReqGrid.DepositBox = txtDepositBox.Text;
            ReqGrid.CollateralNumber = txtCollateralNumber.Text;
            fillGrid(ReqGrid, false, false);
        }

        //open delete collateral out modal
        protected void lnkDeleteCollateralOutModal_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            hdnCollateralOutID.Value = lnk.CommandArgument;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDeleteCollateralOutModal();", true);
        }

        // delete collateral out
        protected void btnDeleteCollateralOut_Click(object sender, EventArgs e)
        {
            int outID = int.Parse(hdnCollateralOutID.Value);
            wsLoanController.DeleteCollateralByOutIDRequest reqDelete = new wsLoanController.DeleteCollateralByOutIDRequest();
            reqDelete.OutID = outID;
            reqDelete.Description = "";
            reqDelete.ModifiedID = UserSession.UserId;
            wsLoanController.SetResponse resDelete =  wsLController.DeleteCollateralOutByID(reqDelete);
            if (!resDelete.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + resDelete.errorCode);
                return;
            }
            _config.AlertMessage(this, MessageType.SUCCESS, "Çıxarış silindi.");

            //after delete fill grid

            wsLoanController.GetCollateralOutListRequest ReqGrid = new wsLoanController.GetCollateralOutListRequest();
            ReqGrid.StartDate = txtStartDate.Text;
            ReqGrid.EndDate = txtEndDate.Text;
            ReqGrid.UserID = UserSession.UserId;
            ReqGrid.BranchID = int.Parse(drlBranch.SelectedValue);
            ReqGrid.DepositBox = txtDepositBox.Text;
            ReqGrid.CollateralNumber = txtCollateralNumber.Text;
            fillGrid(ReqGrid, false, false);
        }

        void PrintAndSendMail(DataTable dtCollateralOutList, wsLoanController.GetCollateralOutListRequest ReqGrid , bool sendMail)
        {
            DataView dv = new DataView(dtCollateralOutList);
            DataTable dtBranchs = dv.ToTable(true, "BranchID", "BranchName");

            string f_t_name = Server.MapPath(@"~/DocumentTemplates/templates/CollateralOutListTemp.docx");
            string f_o_name = Server.MapPath("~/DocumentTemplates/" + "CollateralOutListTemp" + Guid.NewGuid().ToString() + ".docx");

            // copy file from template
            System.IO.File.Copy(f_t_name, f_o_name);

            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(f_o_name);

            bookMarks bookmarks = new bookMarks();

            //StartDate
            bookmarks.key = "StartDate";
            bookmarks.value = ReqGrid.StartDate;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //PrintDate
            bookmarks.key = "PrintDate";
            bookmarks.value = _Config.HostingTime.ToString("dd.MM.yyyy HH:mm"); 
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //EndDate
            bookmarks.key = "EndDate";
            bookmarks.value = ReqGrid.EndDate;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);
            
            string _htmll_table = "";
            for (int i = 0; i < dtBranchs.Rows.Count; i++)
            {
                int branchID = Convert.ToInt32(dtBranchs.Rows[i]["BranchID"]);
                string branchName = Convert.ToString(dtBranchs.Rows[i]["BranchName"]);

                var dataRow = from lls in dtCollateralOutList.AsEnumerable()
                              where lls.Field<int>("BranchID") == branchID
                              select lls;
               
                DataTable dtListDetail = dataRow.CopyToDataTable<DataRow>();

                //table html to bookmark
                string CollateralOutListTempDataTableHtml = File.ReadAllText(Server.MapPath(@"~/DocumentTemplates/templates/CollateralOutListTempDataTable.txt"));
                string CollateralOutListTempDataTableHtmlBody = "";
                int _rowCount = 0;

                foreach (DataRow dr in dtListDetail.Rows)
                {
                    CollateralOutListTempDataTableHtmlBody += $@"<tr>
                    <td>{Convert.ToString(dr["OutDate"])}</td>
                    <td>{Convert.ToString(dr["BranchName"])}</td>
                    <td>{Convert.ToString(dr["FullName"])}</td>
                    <td>{Convert.ToString(dr["DepositBox"])}</td>
                    <td>{Convert.ToString(dr["CollateralNumber"])}</td>
                    <td>{Convert.ToString(dr["Note"])}</td>
                    </tr>";
                    _rowCount++;
                }

                if (CollateralOutListTempDataTableHtmlBody != "") //fill footer
                {


                    CollateralOutListTempDataTableHtmlBody += $@"<tr>
                                                      <td>Say</td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td></td>
                                                      <td>{_rowCount}</td>
                                                      </tr>";
                }
                string _CollateralOutListTempDataTableHtml = CollateralOutListTempDataTableHtml
                                                 .Replace("{0}", branchName)
                                                 .Replace("{1}", CollateralOutListTempDataTableHtmlBody);

                _htmll_table += _CollateralOutListTempDataTableHtml;
            }

            string _htmlGeneral = "<head><style>table, td, th {  border: 1px solid black; text-align: left;font-size:14px;}table {border-collapse: collapse;} </style></head><body>{0}</body>";

            _htmlGeneral = _htmlGeneral.Replace("{0}", _htmll_table);

            bookmarks.key = "pDatatTable";
            bookmarks.value = _htmlGeneral;
            fillTemplateWord(bookmarks, WordBookmarkType.HTML, doc);


            ((Microsoft.Office.Interop.Word._Document)doc).Close();
            ((Microsoft.Office.Interop.Word._Application)app).Quit();

            string pdf_file_name = f_o_name.Replace(".docx", ".pdf");

            Word2Pdf objWordPDF = new Word2Pdf();
            objWordPDF.InputLocation = f_o_name;
            objWordPDF.OutputLocation = pdf_file_name;
            objWordPDF.Word2PdfCOnversion();

            if (sendMail)
            {
                int branchID = Convert.ToInt32(drlBranch.SelectedValue);
                if (branchID == 0)
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Mailin göndəriləcəyi filialı seçin!");
                    return;
                }
                wsUserController.GetBranchFullDataByIDRequest branchReq = new wsUserController.GetBranchFullDataByIDRequest();
                branchReq.BranchID = branchID;
                wsUserController.GetBranchFullDataByIDResponse resBranch = wsUController.GetBranchFullDataByID(branchReq);

                string mail_result = _config.SendMailWithAttachment(resBranch.Branch_Mail, _config.GetAppSetting("destinationMails_1"), resBranch.Branch_Mail_Password, "Girov Çıxarışı", "", new string[] { pdf_file_name });
                if (!mail_result.Equals("OK"))
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi. Mail: " + mail_result);
                    return;
                }
                else
                {
                    _config.AlertMessage(this, MessageType.SUCCESS, "Mail göndərildi");
                }
            }

            if (!sendMail) // if only pdf download (print)
            {
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.Buffer = true;
                response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                response.AddHeader("Content-Disposition", "attachment;filename=" + System.IO.Path.GetFileName(pdf_file_name));
                response.WriteFile(pdf_file_name);
                response.Flush();
                response.End();
            }
            try
            {
                System.IO.File.Delete(f_o_name);
                System.IO.File.Delete(pdf_file_name);
            }
            catch
            { }



        }

        //print data
        protected void lnkDownloadResult_Click(object sender, EventArgs e)
        {
            if (!_config.isDate(txtStartDate.Text))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Başlanğıc tarixi düzgün daxil edin!");
                return;
            }
            if (!_config.isDate(txtEndDate.Text))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Son tarixi düzgün daxil edin!");
                return;
            }
            wsLoanController.GetCollateralOutListRequest ReqGrid = new wsLoanController.GetCollateralOutListRequest();
            ReqGrid.StartDate = txtStartDate.Text;
            ReqGrid.EndDate = txtEndDate.Text;
            ReqGrid.UserID = UserSession.UserId;
            ReqGrid.BranchID = int.Parse(drlBranch.SelectedValue);
            ReqGrid.DepositBox = txtDepositBox.Text;
            ReqGrid.CollateralNumber = txtCollateralNumber.Text;

            fillGrid(ReqGrid, true, false);
        }

        protected void lnkSendMailResult_Click(object sender, EventArgs e)
        {
            if (!_config.isDate(txtStartDate.Text))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Başlanğıc tarixi düzgün daxil edin!");
                return;
            }
            if (!_config.isDate(txtEndDate.Text))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Son tarixi düzgün daxil edin!");
                return;
            }
            wsLoanController.GetCollateralOutListRequest ReqGrid = new wsLoanController.GetCollateralOutListRequest();
            ReqGrid.StartDate = txtStartDate.Text;
            ReqGrid.EndDate = txtEndDate.Text;
            ReqGrid.UserID = UserSession.UserId;
            ReqGrid.BranchID = int.Parse(drlBranch.SelectedValue);
            ReqGrid.DepositBox = txtDepositBox.Text;
            ReqGrid.CollateralNumber = txtCollateralNumber.Text;

            fillGrid(ReqGrid, false, true);
        }


        protected void drlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!_config.isDate(txtStartDate.Text))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Başlanğıc tarixi düzgün daxil edin!");
                return;
            }
            if (!_config.isDate(txtEndDate.Text))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Son tarixi düzgün daxil edin!");
                return;
            }
            wsLoanController.GetCollateralOutListRequest ReqGrid = new wsLoanController.GetCollateralOutListRequest();
            ReqGrid.StartDate = txtStartDate.Text;
            ReqGrid.EndDate = txtEndDate.Text;
            ReqGrid.UserID = UserSession.UserId;
            ReqGrid.BranchID = int.Parse(drlBranch.SelectedValue);
            ReqGrid.DepositBox = txtDepositBox.Text;
            ReqGrid.CollateralNumber = txtCollateralNumber.Text;

            fillGrid(ReqGrid, false, false);
        }



        // Begin word template

        public static string SaveToTemporaryFile(string html)
        {
            string htmlTempFilePath = Path.Combine(Path.GetTempPath(), string.Format("{0}.html", Path.GetRandomFileName()));
            using (StreamWriter writer = File.CreateText(htmlTempFilePath))
            {
                html = string.Format("<html>{0}</html>", html);
                writer.WriteLine(html);
            }

            return htmlTempFilePath;
        }

        enum WordBookmarkType
        {
            HTML,
            TXT
        }


        private void ReplaceBookmarkText(Microsoft.Office.Interop.Word.Document doc, string bookmarkName, string text, WordBookmarkType type)
        {
            try
            {
                if (doc.Bookmarks.Exists(bookmarkName))
                {
                    Object name = bookmarkName;
                    Microsoft.Office.Interop.Word.Range range = doc.Bookmarks.get_Item(ref name).Range;
                    range.Text = range.Text.Replace(range.Text, text);
                    object newRange = range;
                    if (type == WordBookmarkType.HTML)
                    {
                        range.InsertFile(SaveToTemporaryFile(text), Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    }
                    doc.Bookmarks.Add(bookmarkName, ref newRange);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }



        private void fillTemplateWord(bookMarks bookmarks, WordBookmarkType wordBookmarkType, Microsoft.Office.Interop.Word.Document doc)
        {
            try
            {
                ReplaceBookmarkText(doc, bookmarks.key.ToString(), bookmarks.value.ToString(), wordBookmarkType);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

      


        // End word template


    }

    public class bookMarks
    {
        public object key { get; set; }
        public object value { get; set; }
    }
}