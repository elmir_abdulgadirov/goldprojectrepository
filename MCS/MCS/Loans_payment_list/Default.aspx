﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Loans_payment_list.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="panel panel-primary-full" style="margin-bottom: 0;">
        <div class="panel-body" style="padding: 5px; min-height: 38px">
            <div style="float: left; width: 94%">
                <div class="row">
                    <div class="col-sm-2">
                        <asp:DropDownList ID="drlBranch" class="form-control input-sm" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <asp:TextBox ID="txtFullname" autocomplete="off" class="form-control input-sm" placeHolder="S.A.A" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-2">
                        <asp:TextBox ID="txtCollateralNumber" autocomplete="off" class="form-control input-sm" placeHolder="Girov nömrəsi" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-1">
                        <asp:DropDownList ID="drlCurrency" AutoPostBack="true" class="form-control input-sm" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtEndDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>

                </div>
            </div>
            <div style="float: right; text-align: right">
                <div class="row">
                    <div class="col-sm-12">
                        <img id="search_loan_data" style="display: none" src="../img/loader1.gif" />
                        <asp:Button ID="btnSearchLoan" CssClass="btn btn-default btn-quirk btn-sm"
                            OnClick="btnSearchLoan_Click"
                            Style="margin: 0" runat="server" Text="YENİLƏ"
                            OnClientClick="this.style.display = 'none';
                            document.getElementById('search_loan_data').style.display = '';" />
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <ul class="list-inline" style="padding-left: 5px">
                    <li style="float: left">
                        <span class="label label-black">Məlumat sayı:
                            <asp:Literal ID="ltrDataCount" runat="server"></asp:Literal>
                        </span>
                    </li>
                    <li style="float: right">
                        <asp:LinkButton ID="lnkDownloadResult" runat="server"
                            OnClick ="lnkDownloadResult_Click">
                        <i class="glyphicon glyphicon-print"></i><span> Çap</span>
                        </asp:LinkButton>
                    </li>
                    <li style="clear: both"></li>
                </ul>
                <div class="mb20"></div>
                <div class="table-responsive">
                    <asp:GridView class="table table-striped nomargin table_custom_border_top_gray"
                        ID="grdLoanData" runat="server" Style="background-color: #ffffff !important;"
                        AutoGenerateColumns="False" GridLines="None" ShowFooter="True">
                        <Columns>
                            <asp:TemplateField HeaderText="FİLİAL">
                                <ItemTemplate>
                                    <%#Eval("BranchName")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="140px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="TƏSVİR">
                                <ItemTemplate>
                                    <%#Eval("Description")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="SƏNƏD №">
                                <ItemTemplate>
                                    <%# Eval("CollateralNumber") %>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="110px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="TARİX">
                                <ItemTemplate>
                                    <%# Eval("PaymentDate") %>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="110px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ƏSAS<br/>FAİZ<br/>DƏBBƏ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdPayedPrincipalAmount" runat="server" Text='<%#Eval("PayedPrincipalAmount") %>'></asp:Label><br />
                                    <asp:Label ID="lblGrdPayedRateAmount" runat="server" Text='<%#Eval("PayedRateAmount") %>'></asp:Label><br />
                                    <asp:Label ID="lblGrdPayedPenaltyAmount" runat="server" Text='<%#Eval("PayedPenalytAmount") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdPayedPrincipalAmountSum" runat="server" Text=""></asp:Label><br />
                                        <asp:Label ID="lblGrdPayedRateAmountSum" runat="server" Text=""></asp:Label><br />
                                        <asp:Label ID="lblGrdPayedPenaltyAmountSum" runat="server" Text=""></asp:Label>
                                    </b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="120px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="CƏM">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdTotalPaymentAmount" runat="server" Text='<%#Eval("TotalPayment") %>'></asp:Label><br />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdTotalPaymentAmountSum" runat="server" Text=""></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="110px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <i>Məlumat yoxdur...</i>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#D1D1D1" BorderColor="#520000" Font-Bold="True" ForeColor="Black" />
                        <HeaderStyle CssClass="bg-dark" />
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>


    <div>
        <style>
            .input-sm {
                height: 26px !important;
                line-height: 26px !important;
            }

            .input-group-addon {
                padding: 5px 7px !important;
            }

            .form-group {
                margin-bottom: 10px;
            }

            .label-black {
                background-color: #343a40;
            }

            .label {
                display: inline;
                padding: 0.6em 1em 0.6em;
                font-size: 80%;
                font-weight: bold !important;
                line-height: 1;
                color: #ffffff;
                text-align: center;
                white-space: nowrap;
                vertical-align: baseline;
                border-radius: .25em;
            }
        </style>
    </div>
</asp:Content>
