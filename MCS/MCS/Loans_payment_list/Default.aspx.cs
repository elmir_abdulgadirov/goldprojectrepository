﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WordToPDF;
using static MCS.App_Code._Config;
using static MCS.App_Code.Enums;

namespace MCS.Loans_payment_list
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();
        MCS.wsLoanController.LoanController wsLController = new wsLoanController.LoanController();
        MCS.wsUserController.UserController wsUController = new wsUserController.UserController();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                txtEndDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                fillHeaderData();
                wsLoanController.GetLoanPaymentListRequest Req = new wsLoanController.GetLoanPaymentListRequest();
                Req.BranchID = int.Parse(drlBranch.SelectedValue);
                Req.CollateralNumber = txtCollateralNumber.Text;
                Req.Fullname = txtFullname.Text;
                Req.CurrencyID = int.Parse(drlCurrency.SelectedValue);
                Req.DateStart = txtStartDate.Text;
                Req.DateEnd = txtEndDate.Text;
                Req.UserID = UserSession.UserId;
                fillGrid(Req, false);
            }

            #region Begin Permission
             CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.LOANS_PAYMENT));
             lnkDownloadResult.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.PRINT_LOAN_PAYMENT_LIST));
            #endregion
        }

        void fillHeaderData()
        {
            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial", "0"));
            }

            DataTable dtCurrency = wsGController.CurrencyList();
            if (dtCurrency.Rows.Count > 0)
            {
                drlCurrency.Items.Clear();
                drlCurrency.DataTextField = "CurrencyName";
                drlCurrency.DataValueField = "CurrencyID";
                drlCurrency.DataSource = dtCurrency;
                drlCurrency.DataBind();
                drlCurrency.SelectedValue = "1";
            }

            txtFullname.Text = "";
            txtCollateralNumber.Text = "";
        }


        void fillGrid(wsLoanController.GetLoanPaymentListRequest Req, bool isPrint)
        {
            if (!_config.isDate(Req.DateStart))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Başlanğıc tarixi düzgün daxil edin!");
                return;
            }
            if (!_config.isDate(Req.DateEnd))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Son tarixi düzgün daxil edin!");
                return;
            }
            DataTable dtLoanList = wsLController.GetPaymentList(Req);

            if (dtLoanList == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!");
                return;
            }

            grdLoanData.DataSource = dtLoanList;
            grdLoanData.DataBind();

            ltrDataCount.Text = dtLoanList.Rows.Count.ToString();

            if (grdLoanData.Rows.Count > 0)
            {
                GridViewRow drr = grdLoanData.FooterRow;
                Label lblGrdPayedPrincipalAmountSum = (Label)drr.FindControl("lblGrdPayedPrincipalAmountSum");
                Label lblGrdPayedRateAmountSum = (Label)drr.FindControl("lblGrdPayedRateAmountSum");
                Label lblGrdPayedPenaltyAmountSum = (Label)drr.FindControl("lblGrdPayedPenaltyAmountSum");
                Label lblGrdTotalPaymentAmountSum = (Label)drr.FindControl("lblGrdTotalPaymentAmountSum");

                double PayedPrincipalAmount = 0, PayedRateAmount = 0 , PayedPenalytAmount = 0, TotalPayment = 0;

                for (int i = 0; i < grdLoanData.Rows.Count; i++)
                {
                    PayedPrincipalAmount += Convert.ToDouble(((Label)grdLoanData.Rows[i].FindControl("lblGrdPayedPrincipalAmount")).Text);
                    PayedRateAmount += Convert.ToDouble(((Label)grdLoanData.Rows[i].FindControl("lblGrdPayedRateAmount")).Text);
                    PayedPenalytAmount += Convert.ToDouble(((Label)grdLoanData.Rows[i].FindControl("lblGrdPayedPenaltyAmount")).Text);
                    TotalPayment += Convert.ToDouble(((Label)grdLoanData.Rows[i].FindControl("lblGrdTotalPaymentAmount")).Text);
                }

                lblGrdPayedPrincipalAmountSum.Text = PayedPrincipalAmount.ToString("0.00");
                lblGrdPayedRateAmountSum.Text = PayedRateAmount.ToString("0.00");
                lblGrdPayedPenaltyAmountSum.Text = PayedPenalytAmount.ToString("0.00");
                lblGrdTotalPaymentAmountSum.Text = TotalPayment.ToString("0.00");

                if (isPrint)
                {
                    PrintData(dtLoanList);
                }
            }
        }

        protected void btnSearchLoan_Click(object sender, EventArgs e)
        {
            wsLoanController.GetLoanPaymentListRequest Req = new wsLoanController.GetLoanPaymentListRequest();
            Req.BranchID = int.Parse(drlBranch.SelectedValue);
            Req.CollateralNumber = txtCollateralNumber.Text;
            Req.Fullname = txtFullname.Text;
            Req.CurrencyID = int.Parse(drlCurrency.SelectedValue);
            Req.DateStart = txtStartDate.Text;
            Req.DateEnd = txtEndDate.Text;
            Req.UserID = UserSession.UserId;
            fillGrid(Req, false);
        }


        protected void lnkDownloadResult_Click(object sender, EventArgs e)
        {
            wsLoanController.GetLoanPaymentListRequest Req = new wsLoanController.GetLoanPaymentListRequest();
            Req.BranchID = int.Parse(drlBranch.SelectedValue);
            Req.CollateralNumber = txtCollateralNumber.Text;
            Req.Fullname = txtFullname.Text;
            Req.CurrencyID = int.Parse(drlCurrency.SelectedValue);
            Req.DateStart = txtStartDate.Text;
            Req.DateEnd = txtEndDate.Text;
            Req.UserID = UserSession.UserId;
            fillGrid(Req, true);
        }

        void PrintData(DataTable dtLoanList)
        {
            // print datatable
            string f_t_name = Server.MapPath(@"~/DocumentTemplates/templates/GetPaymentList.docx");
            string f_o_name = Server.MapPath("~/DocumentTemplates/" + "GetPaymentList" + Guid.NewGuid().ToString() + ".docx");

            // copy file from template
            System.IO.File.Copy(f_t_name, f_o_name);

            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(f_o_name);

            bookMarks bookmarks = new bookMarks();

            if (drlBranch.SelectedValue == "0")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Filialı seçin");
                return;
            }

            //BranchName
            bookmarks.key = "BranchName";
            bookmarks.value = drlBranch.SelectedItem.Text;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //PrintDate
            bookmarks.key = "PrintDate";
            bookmarks.value = _Config.HostingTime.ToString("dd.MM.yyyy HH:mm");
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            string GetPaymentListDataTableHtml = File.ReadAllText(Server.MapPath(@"~/DocumentTemplates/templates/GetPaymentListDataTable.txt"));
            string GetPaymentListDataTableHtmlBody = "";

            decimal PayedPrincipalAmount = 0, PayedRateAmount = 0, PayedPenalytAmount = 0, TotalPayment = 0;

            foreach (DataRow dr in dtLoanList.Rows)
            {
                GetPaymentListDataTableHtmlBody += $@"<tr>
                    <td>{Convert.ToString(dr["BranchName"])}</td>
                    <td>{Convert.ToString(dr["Description"])}</td>
                    <td>{Convert.ToString(dr["CollateralNumber"])}</td>
                    <td>{Convert.ToString(dr["PayedPrincipalAmount"])}</td>
                    <td>{Convert.ToString(dr["PayedRateAmount"])}</td>
                    <td>{Convert.ToString(dr["PayedPenalytAmount"])}</td>
                    <td>{Convert.ToString(dr["TotalPayment"])}</td>
                    </tr>";

                PayedPrincipalAmount += _config.ToDecimal(dr["PayedPrincipalAmount"]);
                PayedRateAmount += _config.ToDecimal(dr["PayedRateAmount"]);
                PayedPenalytAmount += _config.ToDecimal(dr["PayedPenalytAmount"]);
                TotalPayment += _config.ToDecimal(dr["TotalPayment"]);
            }
            if (GetPaymentListDataTableHtmlBody != "") //fill footer
            {
                GetPaymentListDataTableHtmlBody += $@"<tr>
                                                      <td>Say : " + dtLoanList.Rows.Count + $@"</td>
                                                      <td></td>
                                                      <td></td>
                                                      <td>{PayedPrincipalAmount}</td>
                                                      <td>{PayedRateAmount}</td>
                                                      <td>{PayedPenalytAmount}</td>
                                                      <td>{TotalPayment}</td>
                                                      </tr>";
            }
            string _GetPaymentListDataTableHtml = GetPaymentListDataTableHtml
                                             .Replace("{0}", GetPaymentListDataTableHtmlBody);

            string _htmlGeneral = "<head><style>table, td, th {  border: 1px solid black; text-align: left;font-size:14px;}table {border-collapse: collapse;} </style></head><body>{0}</body>";

            _htmlGeneral = _htmlGeneral.Replace("{0}", _GetPaymentListDataTableHtml);

            bookmarks.key = "DataTable";
            bookmarks.value = _htmlGeneral;
            fillTemplateWord(bookmarks, WordBookmarkType.HTML, doc);


            ((Microsoft.Office.Interop.Word._Document)doc).Close();
            ((Microsoft.Office.Interop.Word._Application)app).Quit();

            string pdf_file_name = f_o_name.Replace(".docx", ".pdf");

            Word2Pdf objWordPDF = new Word2Pdf();
            objWordPDF.InputLocation = f_o_name;
            objWordPDF.OutputLocation = pdf_file_name;
            objWordPDF.Word2PdfCOnversion();

            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            response.AddHeader("Content-Disposition", "attachment;filename=" + System.IO.Path.GetFileName(pdf_file_name));
            response.WriteFile(pdf_file_name);
            response.Flush();
            response.End();
            try
            {
                System.IO.File.Delete(f_o_name);
                System.IO.File.Delete(pdf_file_name);
            }
            catch
            { }
        }


        // Begin word template

        public static string SaveToTemporaryFile(string html)
        {
            string htmlTempFilePath = Path.Combine(Path.GetTempPath(), string.Format("{0}.html", Path.GetRandomFileName()));
            using (StreamWriter writer = File.CreateText(htmlTempFilePath))
            {
                html = string.Format("<html>{0}</html>", html);
                writer.WriteLine(html);
            }

            return htmlTempFilePath;
        }

        enum WordBookmarkType
        {
            HTML,
            TXT
        }


        private void ReplaceBookmarkText(Microsoft.Office.Interop.Word.Document doc, string bookmarkName, string text, WordBookmarkType type)
        {
            try
            {
                if (doc.Bookmarks.Exists(bookmarkName))
                {
                    Object name = bookmarkName;
                    Microsoft.Office.Interop.Word.Range range = doc.Bookmarks.get_Item(ref name).Range;
                    range.Text = range.Text.Replace(range.Text, text);
                    object newRange = range;
                    if (type == WordBookmarkType.HTML)
                    {
                        range.InsertFile(SaveToTemporaryFile(text), Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                    }
                    doc.Bookmarks.Add(bookmarkName, ref newRange);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }



        private void fillTemplateWord(bookMarks bookmarks, WordBookmarkType wordBookmarkType, Microsoft.Office.Interop.Word.Document doc)
        {
            try
            {
                ReplaceBookmarkText(doc, bookmarks.key.ToString(), bookmarks.value.ToString(), wordBookmarkType);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }


        // End word template


    }

    public class bookMarks
    {
        public object key { get; set; }
        public object value { get; set; }
    }
}