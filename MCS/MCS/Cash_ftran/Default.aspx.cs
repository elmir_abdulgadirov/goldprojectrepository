﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code.Enums;

namespace MCS.Cash_ftran
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsTransactionController.TransactionController wsTController = new wsTransactionController.TransactionController();
        MCS.wsAccountController.AccountController wsAController = new wsAccountController.AccountController();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                txtEndDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                FillAndClearForm();
            }
            txtMedaxilOperationDate.Attributes.Add("disabled", "disabled");
            txtMedaxilOperationDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
            txtMexaricOperationDate.Attributes.Add("disabled", "disabled");
            txtMexaricOperationDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");


            #region Begin Permission
             CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.CASH_TRAN));
             if (grdTransactions.Rows.Count > 0)
             {
                 grdTransactions.Columns[7].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.CASH_TRAN_DELETE));
             }
             lnkOpenModalSelectOperationType.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.CASH_TRAN_ADD_NEW));
            #endregion


        }

        protected void lnkOpenModalSelectOperationType_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openOperationTypeModal();", true);
        }

        //open medaxil view
        protected void lnkMedaxilForm_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 1;
        }

        //open mexaric view
        protected void lnkMexaricForm_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 2;
        }

        // go to main view from medaxil view
        protected void lnkBackToMainView_Click(object sender, EventArgs e)
        {
            FillAndClearForm();
            MultiView1.ActiveViewIndex = 0;
        }

        // go to main view from medaxil view
        protected void lnkBackToMainView_1_Click(object sender, EventArgs e)
        {
            FillAndClearForm();
            MultiView1.ActiveViewIndex = 0;
        }


        void FillAndClearForm()
        {
            DataTable dtAccountGroup = wsAController.GetAccountGroupList();

            drlMedaxilAccountGroup.DataTextField = "Name";
            drlMedaxilAccountGroup.DataValueField = "Code";
            drlMedaxilAccountGroup.DataSource = dtAccountGroup;
            drlMedaxilAccountGroup.DataBind();
            drlMedaxilAccountGroup.Items.Insert(0, new ListItem("Seçin", "-1"));

            drlMexaricAccountGroup.DataTextField = "Name";
            drlMexaricAccountGroup.DataValueField = "Code";
            drlMexaricAccountGroup.DataSource = dtAccountGroup;
            drlMexaricAccountGroup.DataBind();
            drlMexaricAccountGroup.Items.Insert(0, new ListItem("Seçin", "-1"));

            drlMedaxilAccountSubGroup.Items.Clear();
            drlMexaricAccountSubGroup.Items.Clear();

            drlMedaxilAccountSubGroup.Items.Clear();
            drlMexaricAccountSubGroup.Items.Clear();

            drlMedaxilAccountSubSubGroup.Items.Clear();
            drlMexaricAccountSubSubGroup.Items.Clear();

            drlMedaxilAccount.Items.Clear();
            drlMexaricAccount.Items.Clear();


            //SourceMaster
            DataTable dtSourseMaster = wsTController.GetTransactionSourceMasterList();
            drlMedaxilNarrative.Items.Clear();
            drlMedaxilNarrative.DataTextField = "T_DESCRIPTION";
            drlMedaxilNarrative.DataValueField = "T_KEY";
            drlMedaxilNarrative.DataSource = dtSourseMaster;
            drlMedaxilNarrative.DataBind();
            drlMedaxilNarrative.Items.Insert(0, new ListItem("Təyinat", ""));

            drlMexaricNarrative.Items.Clear();
            drlMexaricNarrative.DataTextField = "T_DESCRIPTION";
            drlMexaricNarrative.DataValueField = "T_KEY";
            drlMexaricNarrative.DataSource = dtSourseMaster;
            drlMexaricNarrative.DataBind();
            drlMexaricNarrative.Items.Insert(0, new ListItem("Təyinat", ""));


            txtMexaricResponsibilityPerson.Text = txtMexaricAmount.Text = txtMexaricNote.Text = "";
            txtMedaxilResponsibilityPerson.Text = txtMedaxilAmount.Text = txtMedaxilNote.Text = "";

            DataTable dtXezine = wsAController.GetCashBankAccountList(UserSession.UserId, "CASH_ACNT");
            drlXezineMedaxilAccount.DataTextField = "AcntData";
            drlXezineMedaxilAccount.DataValueField = "ACNT_ID";
            drlXezineMedaxilAccount.DataSource = dtXezine;
            drlXezineMedaxilAccount.DataBind();
            drlXezineMedaxilAccount.Items.Insert(0, new ListItem("Seçin", ""));

            drlXezineMexaricAccount.DataTextField = "AcntData";
            drlXezineMexaricAccount.DataValueField = "ACNT_ID";
            drlXezineMexaricAccount.DataSource = dtXezine;
            drlXezineMexaricAccount.DataBind();
            drlXezineMexaricAccount.Items.Insert(0, new ListItem("Seçin", ""));


            wsAccountController.GetCashBankRequest reqMainList = new wsAccountController.GetCashBankRequest();
            reqMainList.OPER_TYPE = "CASH";
            reqMainList.UserID = UserSession.UserId;
            reqMainList.DateFrom = !_config.isDate(txtStartDate.Text) ? _Config.HostingTime.ToString("dd.MM.yyyy") : txtStartDate.Text;
            reqMainList.DateTo = !_config.isDate(txtEndDate.Text) ? _Config.HostingTime.ToString("dd.MM.yyyy") : txtEndDate.Text;
            DataTable dtMainList = wsAController.GetCashBankDataList(reqMainList);
            if (dtMainList == null)
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Xəta baş verdi");
                return;
            }
            grdTransactions.DataSource = dtMainList;
            grdTransactions.DataBind();
        }

        protected void drlMedaxilAccountGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string code = drlMedaxilAccountGroup.SelectedValue;
            if (code != "-1")
            {
                drlMedaxilAccountSubGroup.DataTextField = "Name";
                drlMedaxilAccountSubGroup.DataValueField = "Code";
                drlMedaxilAccountSubGroup.DataSource = wsAController.GetAccountSubGroupList(code);
                drlMedaxilAccountSubGroup.DataBind();
                drlMedaxilAccountSubGroup.Items.Insert(0, new ListItem("Seçin", "-1"));
            }
            if (code == "-1")
                drlMedaxilAccountSubGroup.Items.Clear();

            drlMedaxilAccountSubSubGroup.Items.Clear();
            drlMedaxilAccount.Items.Clear();
        }

        protected void drlMexaricAccountGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string code = drlMexaricAccountGroup.SelectedValue;
            if (code != "-1")
            {
                drlMexaricAccountSubGroup.DataTextField = "Name";
                drlMexaricAccountSubGroup.DataValueField = "Code";
                drlMexaricAccountSubGroup.DataSource = wsAController.GetAccountSubGroupList(code);
                drlMexaricAccountSubGroup.DataBind();
                drlMexaricAccountSubGroup.Items.Insert(0, new ListItem("Seçin", "-1"));
            }
            if (code == "-1")
                drlMexaricAccountSubGroup.Items.Clear();

            drlMexaricAccountSubSubGroup.Items.Clear();
            drlMexaricAccount.Items.Clear();
        }

        protected void drlMedaxilAccountSubGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string code = drlMedaxilAccountSubGroup.SelectedValue;
            if (code != "-1")
            {
                drlMedaxilAccountSubSubGroup.DataTextField = "Description";
                drlMedaxilAccountSubSubGroup.DataValueField = "CodeAndCurrencyID";
                drlMedaxilAccountSubSubGroup.DataSource = wsAController.GetAccountSubSubGroupList(code);
                drlMedaxilAccountSubSubGroup.DataBind();
                drlMedaxilAccountSubSubGroup.Items.Insert(0, new ListItem("Seçin", "-1"));
            }
            if (code == "-1")
                drlMedaxilAccountSubSubGroup.Items.Clear();

            drlMedaxilAccount.Items.Clear();
        }

        protected void drlMexaricAccountSubGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string code = drlMexaricAccountSubGroup.SelectedValue;
            if (code != "-1")
            {
                drlMexaricAccountSubSubGroup.DataTextField = "Description";
                drlMexaricAccountSubSubGroup.DataValueField = "CodeAndCurrencyID";
                drlMexaricAccountSubSubGroup.DataSource = wsAController.GetAccountSubSubGroupList(code);
                drlMexaricAccountSubSubGroup.DataBind();
                drlMexaricAccountSubSubGroup.Items.Insert(0, new ListItem("Seçin", "-1"));
            }
            if (code == "-1")
                drlMexaricAccountSubSubGroup.Items.Clear();

            drlMexaricAccount.Items.Clear();
        }

        protected void drlMedaxilAccountSubSubGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CodeAndCurrencyID = drlMedaxilAccountSubSubGroup.SelectedValue;
            if (CodeAndCurrencyID != "-1")
            {
                drlMedaxilAccount.DataTextField = "ACNT_NAME1";
                drlMedaxilAccount.DataValueField = "ACNT_ID";
                drlMedaxilAccount.DataSource = wsAController.GetAcntDataByAcntCode(CodeAndCurrencyID.Split('_')[0], UserSession.UserId, int.Parse(CodeAndCurrencyID.Split('_')[1]));
                drlMedaxilAccount.DataBind();
                drlMedaxilAccount.Items.Insert(0, new ListItem("Seçin", "-1"));
            }
            if (CodeAndCurrencyID == "-1")
                drlMedaxilAccount.Items.Clear();
        }

        protected void drlMexaricAccountSubSubGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CodeAndCurrencyID = drlMexaricAccountSubSubGroup.SelectedValue;
            if (CodeAndCurrencyID != "-1")
            {
                drlMexaricAccount.DataTextField = "ACNT_NAME1";
                drlMexaricAccount.DataValueField = "ACNT_ID";
                drlMexaricAccount.DataSource = wsAController.GetAcntDataByAcntCode(CodeAndCurrencyID.Split('_')[0], UserSession.UserId, int.Parse(CodeAndCurrencyID.Split('_')[1]));
                drlMexaricAccount.DataBind();
                drlMexaricAccount.Items.Insert(0, new ListItem("Seçin", "-1"));
            }
            if (CodeAndCurrencyID == "-1")
                drlMexaricAccount.Items.Clear();


        }


        //Medaxil
        protected void btnCreateMedaxil_Click(object sender, EventArgs e)
        {
            if (drlXezineMedaxilAccount.SelectedValue == "")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Xəzinə hesabını seçin!");
                return;
            }
            if (!_config.isDate(txtMedaxilOperationDate.Text))
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Tarix formatı düzgün deyil");
                return;
            }
            if (drlMedaxilAccountGroup.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Hesab qrupunu seçin!");
                return;
            }
            if (drlMedaxilAccountSubGroup.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Hesab alt qrupunu seçin!");
                return;
            }
            if (drlMedaxilAccountSubSubGroup.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Hesab adını seçin!");
                return;
            }
            if (drlMedaxilAccount.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Hesabı  seçin!");
                return;
            }

            if (txtMedaxilResponsibilityPerson.Text.Length < 3)
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Təhvil verəni daxil edin!");
                return;
            }
            if (drlMedaxilNarrative.SelectedValue == "")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Təyinatı daxil edin!");
                return;
            }
            decimal amount = 0;
            try
            {
                amount = _config.ToDecimal(txtMedaxilAmount.Text);
            }
            catch
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Məbləği düzgün daxil edin!");
                return;
            }

            wsTransactionController.CreateCashBankTransactionRequest req = new wsTransactionController.CreateCashBankTransactionRequest();
            req.Amount = amount;
            req.Assignment = drlMedaxilNarrative.SelectedValue;
            req.Corresp_AcntID = int.Parse(drlMedaxilAccount.SelectedValue);
            req.CreatedID = UserSession.UserId;
            req.Note = txtMedaxilNote.Text;
            req.OperationType = "CASH";
            req.ResponsibilityPerson = txtMedaxilResponsibilityPerson.Text;
            req.TranType = "D";
            req.ValueDate = txtMedaxilOperationDate.Text;
            req.CashBankAcntID = int.Parse(drlXezineMedaxilAccount.SelectedValue);
            wsTransactionController.SetResponse res = wsTController.CreateCashBankTransaction(req);
            if (!res.isSuccess)
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Xəta :" + res.errorCode);
                return;
            }

            _config.AlertMessage(this, _Config.MessageType.SUCCESS, "Əməliyyat yaradıldı.");
            FillAndClearForm();
            MultiView1.ActiveViewIndex = 0;

        }

        // Mexaric
        protected void btnCreateMexaric_Click(object sender, EventArgs e)
        {
            if (drlXezineMexaricAccount.SelectedValue == "")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Xəzinə hesabını seçin!");
                return;
            }
            if (!_config.isDate(txtMexaricOperationDate.Text))
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Tarix formatı düzgün deyil");
                return;
            }
            if (drlMexaricAccountGroup.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Hesab qrupunu seçin!");
                return;
            }
            if (drlMexaricAccountSubGroup.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Hesab alt qrupunu seçin!");
                return;
            }
            if (drlMexaricAccountSubSubGroup.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Hesab adını seçin!");
                return;
            }
            if (drlMexaricAccount.SelectedValue == "-1")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Hesabı  seçin!");
                return;
            }

            if (txtMexaricResponsibilityPerson.Text.Length < 3)
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Təhvil verəni daxil edin!");
                return;
            }
            if (drlMexaricNarrative.SelectedValue == "")
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Təyinatı daxil edin!");
                return;
            }
            decimal amount = 0;
            try
            {
                amount = _config.ToDecimal(txtMexaricAmount.Text);
            }
            catch
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Məbləği düzgün daxil edin!");
                return;
            }

            wsTransactionController.CreateCashBankTransactionRequest req = new wsTransactionController.CreateCashBankTransactionRequest();
            req.Amount = amount;
            req.Assignment = drlMexaricNarrative.SelectedValue;
            req.Corresp_AcntID = int.Parse(drlMexaricAccount.SelectedValue);
            req.CreatedID = UserSession.UserId;
            req.Note = txtMexaricNote.Text;
            req.OperationType = "CASH";
            req.ResponsibilityPerson = txtMexaricResponsibilityPerson.Text;
            req.TranType = "C";
            req.ValueDate = txtMexaricOperationDate.Text;
            req.CashBankAcntID = int.Parse(drlXezineMexaricAccount.SelectedValue);
            wsTransactionController.SetResponse res = wsTController.CreateCashBankTransaction(req);
            if (!res.isSuccess)
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Xəta :" + res.errorCode);
                return;
            }

            _config.AlertMessage(this, _Config.MessageType.SUCCESS, "Əməliyyat yaradıldı.");
            FillAndClearForm();
            MultiView1.ActiveViewIndex = 0;


        }

        //search by dates
        protected void btnSearchTran_Click(object sender, EventArgs e)
        {
            FillAndClearForm();
        }

        //open delete modal
        protected void lnkDeleteTranModal_Click(object sender, EventArgs e)
        {
            string msgID = (sender as LinkButton).CommandArgument;
            hdnSelectedTranMsgID.Value = msgID;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDeleteTransactionModal();", true);
        }

        //delete transactions
        protected void btnDeleteTransaction_Click(object sender, EventArgs e)
        {
            string msgID = hdnSelectedTranMsgID.Value;
            wsTransactionController.DeleteCashBankTransactionRequest req = new wsTransactionController.DeleteCashBankTransactionRequest();
            req.Description = "Səhv əməliyyat";
            req.ModifiedID = UserSession.UserId;
            req.MsgID = msgID;
            wsTransactionController.SetResponse res = wsTController.DeleteCashBankTransactionByID(req);
            if (!res.isSuccess)
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Xəta :" + res.errorCode);
                return;
            }
            _config.AlertMessage(this, _Config.MessageType.SUCCESS, "Əməliyyat silindi");
            FillAndClearForm();
        }
    }
}