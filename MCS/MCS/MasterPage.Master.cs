﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MCS
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        MCS.wsUserController.UserController wsUController = new wsUserController.UserController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string  req_url = "";
                try { req_url = Request.Url.Segments[1].ToLower().Replace("/",""); }
                catch { req_url = ""; }

                if (Session["UserSession"] == null) _Config.Rd("/exit");
                MCS.wsUserController.UserLoginResponse UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];

                if (UserSession.Picture.Trim() != "")
                {
                    ltrUserHeaderImage.Text = string.Format(@"<img src=""{0}"" alt="""" />", UserSession.Picture.Trim());
                    ltrUserHeaderImage_1.Text = string.Format(@" <img src=""{0}"" alt=""""  class=""media-object img-circle"">", UserSession.Picture.Trim());
                }
                ltrUserHeaderName.Text = UserSession.FullName;
                ltrUserHeaderName_1.Text = UserSession.FullName;
                ltrUserPosition.Text = UserSession.Position;

                DataTable dtMenu = wsUController.dtMenu(UserSession.UserId);   
                if (dtMenu == null || dtMenu.Rows.Count == 0) return;


                IEnumerable<DataRow> queryMainMenu =
                from mainMenu in dtMenu.AsEnumerable()
                where mainMenu.Field<int>("Parent_ID") == 0
                orderby mainMenu.Field<int>("Position")
                select mainMenu;

                DataTable dtMainMenu = queryMainMenu.CopyToDataTable();

                if (dtMainMenu == null || dtMainMenu.Rows.Count == 0) return;
                ltrLeftMenu.Text = "";

                string activeMain = "", activeSub = "";
                string activeMainNameHeader = "", activeSubNameHeader = "";
                string iconCell = "";

                foreach (DataRow drMainMenu in dtMainMenu.Rows)
                {
                    string nav_parent = "";
                    activeMain = "";

                    DataTable dtSubMenu = new DataTable("dtSubMenu");
                    IEnumerable<DataRow> querySubMenu =
                       from subMenu in dtMenu.AsEnumerable()
                       where subMenu.Field<int>("Parent_ID") == Convert.ToInt32(drMainMenu["ID"])
                       orderby subMenu.Field<int>("Position")
                       select subMenu;
                    if (querySubMenu.Any())
                    {
                        dtSubMenu = querySubMenu.CopyToDataTable<DataRow>();
                        nav_parent = dtSubMenu.Rows.Count > 0 ? "nav-parent" : "";
                    }

                    /*for active menu*/
                    if (req_url != "")
                    {
                        foreach (DataRow drSubMenu1 in dtSubMenu.Rows)
                        {
                            string url_sub = Convert.ToString(drSubMenu1["URL"]).ToLower().Replace("/","");
                            if (url_sub == req_url)
                            {
                                activeMain = " active";
                                activeMainNameHeader = Convert.ToString(drMainMenu["NAME"]);
                            }
                        }

                        string url_main = Convert.ToString(drMainMenu["URL"]).ToLower().Replace("/","");
                        if (url_main == req_url)
                        {
                            activeMain = " active";
                            activeMainNameHeader = Convert.ToString(drMainMenu["NAME"]);
                        }

                    }



                    ltrLeftMenu.Text +=
                    @"<li class="""+ nav_parent + "" + activeMain + "\">";

                    iconCell = "";
                    if (Convert.ToString(drMainMenu["ICON"]) != "")  iconCell = "<i class = \""+ Convert.ToString(drMainMenu["ICON"]) + "\"></i>"  ;  

                    ltrLeftMenu.Text +=
                    "<a href=\"" + Convert.ToString(drMainMenu["URL"])  + "\">" + iconCell + "<span>" + Convert.ToString(drMainMenu["NAME"]) + "</span></a>";
                    if (dtSubMenu.Rows.Count > 0)
                    {
                        activeSub = "";
                        ltrLeftMenu.Text +=
                        "<ul class=\"children\">";
                        foreach (DataRow drSubMenu in dtSubMenu.Rows)
                        {
                            string url_sub = Convert.ToString(drSubMenu["URL"]).ToLower().Replace("/", "");
                            if (url_sub == req_url)
                            {
                                activeSub = " class =  \"active\"";
                                activeSubNameHeader = Convert.ToString(drSubMenu["NAME"]);
                            }
                            else activeSub = "";
                            ltrLeftMenu.Text += "<li"+ activeSub + "><a href=\"" + Convert.ToString(drSubMenu["URL"]) + "\">" + Convert.ToString(drSubMenu["NAME"]) + "</a></li>";
                        }
                        ltrLeftMenu.Text += "</ul>";
                    }
                    ltrLeftMenu.Text += "</li>";

                }
  
                if (activeSubNameHeader == "")
                {
                    ltrContentHeaderMenu1.Text = string.Format("<li class=\"active\">{0}</li>", activeMainNameHeader);
                    ltrContentHeaderMenu2.Text = "";
                }
                else
                {
                    ltrContentHeaderMenu1.Text = string.Format("<li>{0}</li>", activeMainNameHeader);
                    ltrContentHeaderMenu2.Text = string.Format("<li class=\"active\">{0}</li>", activeSubNameHeader); ;
                }

                if (req_url == "index")
                {
                    ltrContentHeaderMenu1.Text = ltrContentHeaderMenu2.Text = "";
                    ltrContentHeaderMenuMain.Text = "<li class = \"active\"><a href=\"/index\"><i class=\"fa fa-home mr5\"></i>FRM</a></li>";
                }
                else
                {
                    ltrContentHeaderMenuMain.Text = "<li><a href=\"/index\"><i class=\"fa fa-home mr5\"></i>FRM</a></li>";
                }

                if (req_url == "changepassword")
                {
                    ltrContentHeaderMenu1.Text = string.Format("<li class=\"active\">{0}</li>", "Şifrəni dəyiş");
                    ltrContentHeaderMenu2.Text = "";
                }
                if (req_url == "creditdetails")
                {
                    ltrContentHeaderMenu1.Text = string.Format("<li class=\"active\">{0}</li>", "Kredit məlumatları");
                    ltrContentHeaderMenu2.Text = "";
                }
                DeleteTempFiles();
            }


            if (Convert.ToString(ViewState["isCollapsed"]) == "1")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "toggleMenu(220,0);", true);
            }
            if (Convert.ToString(ViewState["isCollapsed"]) == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "toggleMenu(-220,0);", true);
            }

        }


        void DeleteTempFiles()
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/DocumentTemplates/"));
            foreach (FileInfo file in di.GetFiles())
            {
                try{file.Delete();}
                catch{}
            }    
        }

      
    }
}