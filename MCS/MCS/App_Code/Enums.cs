﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MCS.App_Code
{
    
    public class Enums
    {
         public enum EnumFunctions
         {
            USERS_VIEW_MAIN_PAGE,
            EDITUSER,
            NEWUSER,

            NEW_CUSTOMER,
            CREATE_NEW_CUSTOMER,

            CUSTOMER_LIST,
            CUSTOMER_EDIT,
            CUSTOMER_DELETE,

            CUSTOMER_CURR_ACNT_LIST,
            CREATE_NEW_CURR_ACNT,
            DELETE_CURR_ACNT,

            NEW_CREDIT,
            N_CREDIT_PRINT_BARCODE,
            N_CREDIT_COLLATERAL_OUT_DATE,
            N_CREDIT_COLLATERAL_OUT,
            N_CREDIT_PRINT_SCHEDULE,
            N_CREDIT_COLLATERAL_OUT_ACT,
            N_CREDIT_PART_COLLAT_SEND_OUT,
            N_CREDIT_PRIN_CONTRACT,
            N_CREDIT_DELETE,
            N_CREDIT_PRINT_ILTIZAM,
            N_CREDIT_PART_ADDITIONAL_SAVE,
            N_CREDIT_SAVE_LOAN,
            N_CREDIT_PART_COLLAT_HEADER_DATA,
            N_CREDIT_PART_DOCUMENTS_SAVE,
            N_CREDIT_PART_GUARANTOR_ADD_NEW,
            N_CREDIT_PART_NOTES_ADD_NEW,
            N_CREDIT_PART_CARD_DELETE,
            N_CREDIT_PART_CARD_EDIT,
            N_CREDIT_PART_COLLAT_DELETE,
            N_CREDIT_PART_DOCUMENTS_DELETE,
            N_CREDIT_PART_GUARANTOR_DELETE,
            N_CREDIT_PART_NOTES_DELETE,
            N_CREDIT_PART_COLLAT_EDIT,
            N_CREDIT_PART_CARD_ADD_NEW,
            N_CREDIT_PART_COLLAT_ADD_NEW,
            N_CREDIT_ACCEPT,



            GENERAL_CREDITS,
            E_CREDIT_ACCEPT,
            E_CREDIT_PRINT_BARCODE,
            E_CREDIT_COLLATERAL_OUT_DATE,
            E_CREDIT_COLLATERAL_OUT,
            E_CREDIT_PRINT_SCHEDULE,
            E_CREDIT_COLLATERAL_OUT_ACT,
            E_CREDIT_PART_COLLAT_SEND_OUT,
            E_CREDIT_PRIN_CONTRACT,
            E_CREDIT_DELETE,
            E_CREDIT_PRINT_ILTIZAM,
            E_CREDIT_PART_ADDITIONAL_SAVE,
            E_CREDIT_SAVE_LOAN,
            E_CREDIT_PART_COLLAT_HEADER_DATA,
            E_CREDIT_PART_DOCUMENTS_SAVE,
            E_CREDIT_PART_GUARANTOR_ADD_NEW,
            E_CREDIT_PART_NOTES_ADD_NEW,
            E_CREDIT_PART_CARD_DELETE,
            E_CREDIT_PART_CARD_EDIT,
            E_CREDIT_PART_COLLAT_DELETE,
            E_CREDIT_PART_DOCUMENTS_DELETE,
            E_CREDIT_PART_GUARANTOR_DELETE,
            E_CREDIT_PART_NOTES_DELETE,
            E_CREDIT_PAYMENT_DELETE,
            E_CREDIT_PAYMENT_GRID_EDIT,
            E_CREDIT_PART_COLLAT_EDIT,
            EDIT_LOAN,
            CREDIT_ADD_NEW,
            E_CREDIT_PART_CARD_ADD_NEW,
            E_CREDIT_PART_COLLAT_ADD_NEW,
            SEARCH_FORM,
            E_CREDIT_PAYMENT_BTN,


            CREDIT_DELAYED,
            CHANGE_TO_CONTROL_STATUS,
            PRINT_DELAYED_LIST,

            CREDIT_CONTROL,
            CHANGE_TO_ACTIVE_STATUS,
            PRINT_CONTROL_LIST,

            CREDIT_CLOSED,
            PRINT_CLOSED_LOAN_LIST,

            CREDIT_BY_PAYMENT_DATE,
            PRINT_BYDATE_LOAN_LIST,

            LOANS_PAYMENT,
            PRINT_LOAN_PAYMENT_LIST,


            COLLATERAL_OUT_LIST,
            DELETE_COLLATERAL_OUT_LIST,
            EMAIL_COLLATERAL_OUT_LIST,
            PRINT_COLLATERAL_OUT_LIST,

            COLLATERAL_GENERAL_LIST,
            PRINT_COLLATERAL_LIST,

            CASH_TRAN,
            CASH_TRAN_ADD_NEW,
            CASH_TRAN_DELETE,

            BANK_TRAN,
            BANK_TRAN_ADD_NEW,
            BANK_TRAN_DELETE,

            SPECIAL_TRANSACTION,
            SPECIAL_TRAN_DELETE,
            SPECIAL_TRAN_ADD_NEW,

            CASH_ACCOUNT,
            PRINT_CASH_TRAN,
            PRINT_CASH_DOCUMENT,
            EMAIL_CASH_DOCUMENT,

            BANK_ACCOUNT,
            PRINT_BANK_TRAN,
            PRINT_BANK_DOCUMENT,
            EMAIL_BANK_DOCUMENT,

            GENERAL_TRANSACTION,
            DELETE_GENERAL_TRAN,

            BALANCE_BY_ACCOUNT,
            PRINT_BY_ACCOUNT_BALANCE,

            BALANCE_SCHEDULE,
            PRINT_ACCOUNT_BALANCE,

            PORTFOLIO_LIST,


            OPEN_DAY_MAIN_PAGE


        }
    }
}