﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace MCS.App_Code
{
    public class Settings : System.Web.UI.Page
    {
        MCS.wsUserController.UserController wsUController = new wsUserController.UserController();

        public void CheckFunctionPermission(int LogonUserId, string functionId)
        {
            wsUserController.CheckFunctionPermissionRequest req = new wsUserController.CheckFunctionPermissionRequest();
            req.UserID = LogonUserId;
            req.FunctionID = functionId;

            wsUserController.CheckFunctionPermissionResponse res = wsUController.CheckUserPermission(req);
            if (res.Permisssion != "YES")
            {
                _Config.Rd("/notaccess");
                return;
            }
        }

        public bool CheckFieldView(int LogonUserId, string functionId)
        {
            wsUserController.CheckFunctionPermissionRequest req = new wsUserController.CheckFunctionPermissionRequest();
            req.UserID = LogonUserId;
            req.FunctionID = functionId;

            wsUserController.CheckFunctionPermissionResponse res = wsUController.CheckUserPermission(req);
            if (res.Permisssion != "YES")
            {
                return false;
            }
            return true;
        }
    }
}