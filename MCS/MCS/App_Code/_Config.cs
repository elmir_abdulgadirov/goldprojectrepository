﻿using System;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;

namespace MCS.App_Code
{
    public class _Config
    {
       

        public string SendMailWithAttachment(string fromMail , string toMail,string fromPassword,string subject,string body,string [] _attachmentsFileNames)
        {
            string result = "";
            var fromAddress = new MailAddress(fromMail, fromMail);

            var smtp = new SmtpClient
            {
                Host = GetAppSetting("smtpHost"),
                Port = Convert.ToInt32(GetAppSetting("smtpPort")),
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };

            string[] to_address = toMail.Split(';');
            MailMessage message = new MailMessage();
            message.From = fromAddress;
            message.Body = body;
            message.Subject = subject;
            foreach (string _mailAddress in to_address)
            {
                message.To.Add(_mailAddress);
            }



            /*   using (var message = new MailMessage(fromAddress, toAddress)
               {
                   Subject = subject,
                   Body = body
               })*/


            {
                foreach (string fileName in _attachmentsFileNames)
                {
                    message.Attachments.Add(new Attachment(fileName));
                }
                try
                {
                    smtp.Send(message);
                    result = "OK";
                }
                catch(Exception ex) 
                {
                    result = ex.Message;
                }
               
            }
            return result;

        }

        public decimal ToDecimal(object value)
        {
            try
            {
                string _value = Convert.ToString(value);
                decimal _a = 5m / 4m;
                string _splitter = "";
                if (_a.ToString().IndexOf(".") > -1) _splitter = ".";
                else if (_a.ToString().IndexOf(",") > -1) _splitter = ",";
                string value2 = _value;
                value2 = _value.Replace(".", _splitter);
                value2 = _value.Replace(",", _splitter);
                return Convert.ToDecimal(value2);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        public double ToDouble(object value)
        {
            try
            {
                string _value = Convert.ToString(value);
                decimal _a = 5m / 4m;
                string _splitter = "";
                if (_a.ToString().IndexOf(".") > -1) _splitter = ".";
                else if (_a.ToString().IndexOf(",") > -1) _splitter = ",";
                string value2 = _value;
                value2 = _value.Replace(".", _splitter);
                value2 = _value.Replace(",", _splitter);
                return Convert.ToDouble(value2);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public  float ToFloat(object Value)
        {
            string _value = Convert.ToString(Value);
            try
            {
                return float.Parse(_value);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        

      

        public static bool CheckObjectCode(string ObjectFullCode)
        {
            int count = 0;
            for (int i = 0; i < ObjectFullCode.Length; i++)
            {
                if (ObjectFullCode[i] == '-')
                {
                    count++;
                }
            }
            if (ObjectFullCode.Length < 7) return false;
            if (count == 3) return true;
            return false;
        }

        // for hosting datetime - deyishir asili olaraq
        public static DateTime HostingTime
        {
            get
            {
                return DateTime.Now.AddHours(0);
            }
        }
        //Bunun lazım olan bütün səhifələrin ilk Page_Load -na qoyuruq. 
        //Lazım olan bütün setting əmməliyyatlarını bura əlavə et.
        public static void PageSettings()
        {
            HttpContext.Current.Response.Cache.SetNoStore(); //Templəri bağlayırıq...
            HttpContext.Current.Session.Timeout = 10080;
            HttpContext.Current.Server.ScriptTimeout = 9999; //Əgər yüklənmə gecikərsə maksimum gözləmə saniyəsi.
            System.Threading.Thread.Sleep(0);
        }

        //Get WebConfig.config App Key
        public  string GetAppSetting(string KeyName)
        {
            return Cs(ConfigurationManager.AppSettings[KeyName]);
        }

        //Fileupload-da gələn şəkilin ölçüsünü kəsir (100x??px).
        public static Unit PicturesSizeSplit(string s)
        {
            try
            {
                int i = Convert.ToInt16(s.Substring(0, 3).Trim().Trim('x'));
                if (i < 220)
                    return Unit.Pixel(i);
                else return Unit.Pixel(220);
            }
            catch { return Unit.Pixel(220); }
        }

       

        //Set title to url (clear latin and special simvols)
        public static string ClrTitle(string Title)
        {
            Title = Title.ToLower().Trim().Trim('-').Trim('.').Trim();
            Title = Title.Replace("   ", " ");
            Title = Title.Replace("  ", " ");
            Title = Title.Replace(" ", "-");
            Title = Title.Replace(",", "-");
            Title = Title.Replace("\"", "");
            Title = Title.Replace("“", "");
            Title = Title.Replace("”", "");

            Title = Title.Replace("---", "-");
            Title = Title.Replace("--", "-");
            Title = Title.Replace("#", "");
            Title = Title.Replace("&", "");

            //No Latin
            Title = Title.Replace("ü", "u");
            Title = Title.Replace("ı", "i");
            Title = Title.Replace("ö", "o");
            Title = Title.Replace("ğ", "g");
            Title = Title.Replace("ə", "e");
            Title = Title.Replace("ç", "ch");
            Title = Title.Replace("ş", "sh");

            return Title;
        }


        public static string MonthToText(string Mont)
        {
            if (Mont == "01") return "Yanvar";
            if (Mont == "02") return "Fevral";
            if (Mont == "03") return "Mart";
            if (Mont == "04") return "Aprel";
            if (Mont == "05") return "May";
            if (Mont == "06") return "İyun";
            if (Mont == "07") return "İyul";
            if (Mont == "08") return "Avqust";
            if (Mont == "09") return "Sentyabr";
            if (Mont == "10") return "Oktyabr";
            if (Mont == "11") return "Noyabr";
            if (Mont == "12") return "Dekabr";

            return "--";
        }

        public static string DateTimeFormat(DateTime Dt)
        {
            return Dt.ToString("dd") + " " + MonthToText(Dt.ToString("MM")) + " " + Dt.ToString("yyyy");
        }

        public static string ClearInjection(string x)
        {
            x = x.Replace("<", "&lt;");
            x = x.Replace(">", "&gt;");
            x = x.Replace("`", "");
            x = x.Replace("'", "");
            x = x.Replace("\"", "");
            x = x.Replace("?", "");
            x = x.Replace("!", "");
            x = x.Replace("/", "");
            x = x.Replace("\\", "");
            x = x.Replace("&", "");
            x = x.Replace("#", "");
            x = x.Replace("~", "");
            x = x.Replace("%", "");
            x = x.Trim('.');
            x = x.Trim();
            return x;
        }

        //Cümlə çox uzun olanda üç nöqtə qoyaq.
        public static string SizeLimit(string Text, int Length)
        {
            if (Text.Length > Length) Text = Text.Substring(0, Length) + " ...";
            return Text;
        }

        //ConvertString.
        public  string Cs(object s)
        {
            if (s == null) s = "";
            return Convert.ToString(s);
        }

        //Numaric testi.
        public static bool IsNumeric(string s)
        {
            if (s == null) return false;
            if (s.Length < 1) return false;
            for (int i = 0; i < s.Length; i++)
            {
                if ("0123456789".IndexOf(s.Substring(i, 1)) < 0) { return false; }
            }
            return true;
        }

        //Messages Box.
        public static void MsgBox(string MsgTxt, Page P)
        {
            P.ClientScript.RegisterClientScriptBlock(P.GetType(), "PopupScript", "window.focus(); alert('" + MsgTxt + "');", true);
        }

        //Sha1  - özəl
        public static string Sha1(string Password)
        {
            byte[] result;
            System.Security.Cryptography.SHA1 ShaEncrp = new System.Security.Cryptography.SHA1CryptoServiceProvider();
            Password = String.Format("{0}{1}{0}", "CSAASADM", Password);
            byte[] buffer = new byte[Password.Length];
            buffer = System.Text.Encoding.UTF8.GetBytes(Password);
            result = ShaEncrp.ComputeHash(buffer);
            return Convert.ToBase64String(result);
        }

        //Səhifəni yönləndirək:
        public static void Rd(string GetUrl)
        {
            HttpContext.Current.Response.Redirect(GetUrl, true);
            HttpContext.Current.Response.End();
        }

        //Tarix yaradaq istifadəçilər üçün:
        public static string Dt(string Format)
        {
            //Əgər boş gələrsə hər ikisidə return edilir.
            string DateStr = "dd.MM.yyyy HH:mm";
            if (Format.Length > 1) DateStr = Format;
            if (Format == "t") DateStr = "dd.MM.yyyy";
            if (Format == "z") DateStr = "HH:mm:ss";
            DateStr = DateTime.Now.AddHours(0).ToString(DateStr);
            return DateStr;
        }


        public static bool IsEmail(string inputEmail)
        {
            const string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            return (new Regex(strRegex)).IsMatch(inputEmail);
        }

        public  bool SendAdminMail(string to, string messTxt)
        {
            //Mail gondermek lazimd olduqda gonderilsin:
            System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(GetAppSetting("MailLogin"), to, "LearnInformatics", messTxt);
            System.Net.Mail.SmtpClient SmtpMail = new System.Net.Mail.SmtpClient(GetAppSetting("MailServer"));
            mm.IsBodyHtml = true;
            SmtpMail.Credentials = new System.Net.NetworkCredential(GetAppSetting("MailLogin"), GetAppSetting("MailPassword"));
            mm.BodyEncoding = System.Text.Encoding.UTF8;
            mm.Priority = System.Net.Mail.MailPriority.Normal;
            mm.IsBodyHtml = true;
            try
            {
                SmtpMail.Send(mm);
                return true;
            }
            catch { return false; }
        }

        // alternativ sendMail
        public  bool SendEmail(string to, string messTxt)
        {
            using (MailMessage mm = new MailMessage(GetAppSetting("MailLogin"), to))
            {
                mm.Subject = "LearnInformatics";
                mm.Body = messTxt;

                mm.IsBodyHtml = false;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = GetAppSetting("MailServer");
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential(GetAppSetting("MailLogin"), GetAppSetting("MailPassword"));
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;

                try
                {
                    smtp.Send(mm);
                    return true;
                }
                catch
                {
                    return false;
                }

            }
        }

        public  bool isDate(string dateFormat)
        {
            string tarih = dateFormat;
            DateTime date;
            string pattern = "dd.MM.yyyy";
            bool rihtDate = DateTime.TryParseExact(tarih, pattern, CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
            return rihtDate;
        }

        public bool isDateCardExpiry(string dateFormat)
        {
            string _date = dateFormat;
            DateTime date;
            string pattern = "MM/yyyy";
            bool rihtDate = DateTime.TryParseExact(_date, pattern, CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
            return rihtDate;
        }


        public static bool isEnglish(String key)
        {
            string bind = "0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
            for (int i = 0; i < key.Length; i++)
            {
                if (bind.IndexOf(key[i]) == -1) return false;
            }
            return true;
        }

        public static string getKeyFromSplit(string str, char delimiter, int keyIndex)
        {
            try
            {
                return str.Split(delimiter)[keyIndex];
            }
            catch
            {
                return "";
            }
        }

        public static DateTime ToDate(string date)
        {
            DateTime dt;
            string pattern = "dd.MM.yyyy";
            DateTime.TryParseExact(date, pattern, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
            return dt;
        }

        public  void AlertMessage(Page control, MessageType _messageType, string message)
        {
            string mType = "", title = ""; ;
            if (_messageType == MessageType.SUCCESS)
            {
                mType = "success";
                title = "İNFORMASİYA";
            }
            if (_messageType == MessageType.ERROR)
            {
                mType = "danger";
                title = "XƏTA";
            }
            if (_messageType == MessageType.WARNING)
            {
                mType = "warning";
                title = "XƏBƏRDARLIQ";
            }
            control.ClientScript.RegisterStartupScript(control.GetType(), "jsAlert", "jsAlert('" + title + "','" + message + "','"+ mType +"');", true);
        }

        public  enum MessageType
        {
            SUCCESS,
            ERROR,
            WARNING
        }



        public string ConvertMoneyToWord(string money)
        {
            string result = "";
            decimal _money = Convert.ToDecimal(money);
            string toWord = ConvertToWord(_money);
            string[] split = Regex.Split(toWord, @"(?<!^)(?=[A-Z])");
            result = String.Join(" ", split);
            result = result.Replace("Wyirmi", "İyirmi").Replace("Alli", "Əlli").Replace("Uç", "Üç").Replace("Iki", "İki");
            if (result.Trim().Length > 1)
            {
                result = result.ToLower();
            }

            result = result.Replace("manat", result.IndexOf("qəpik") > -1 ? ",":"").Trim();
            result = result.Replace("qəpik", "").Trim();
            return result;
        }


        public string ConvertMonthToWord(string month)
        {
            string toWord = ConvertMoneyToWord(month); ;
            toWord = toWord.Replace("manat", "").Trim();
            toWord = toWord.Replace("qəpik", "").Trim();
            return toWord;
        }

        private string ConvertToWord(decimal tutar)
        {
            string sTutar = tutar.ToString("F2").Replace('.', ','); // Replace('.',',') ondalık ayracının . olma durumu için            
            string lira = sTutar.Substring(0, sTutar.IndexOf(',')); //tutarın tam kısmı
            string kurus = sTutar.Substring(sTutar.IndexOf(',') + 1, 2);
            string yazi = "";

            string[] birler = { "", "Bir", "Iki", "Uç", "Dörd", "Beş", "Altı", "Yeddi", "Səkkiz", "Doqquz" };
            string[] onlar = { "", "On", "Wyirmi", "Otuz", "Qırx", "Alli", "Altmış", "Yetmiş", "Səksən", "Doxsan" };
            string[] binler = { "Kvadrilyon", "Trilyon", "Milyard", "Milyon", "Min", "" }; //KATRİLYON'un önüne ekleme yapılarak artırabilir.

            int grupSayisi = 6; //sayıdaki 3'lü grup sayısı. katrilyon içi 6. (1.234,00 daki grup sayısı 2'dir.)
                                //KATRİLYON'un başına ekleyeceğiniz her değer için grup sayısını artırınız.

            lira = lira.PadLeft(grupSayisi * 3, '0'); //sayının soluna '0' eklenerek sayı 'grup sayısı x 3' basakmaklı yapılıyor.            

            string grupDegeri;

            for (int i = 0; i < grupSayisi * 3; i += 3) //sayı 3'erli gruplar halinde ele alınıyor.
            {
                grupDegeri = "";

                if (lira.Substring(i, 1) != "0")
                    grupDegeri += birler[Convert.ToInt32(lira.Substring(i, 1))] + "Yüz"; //yüzler                

                if (grupDegeri.Trim() == "BirYüz") //biryüz düzeltiliyor.
                    grupDegeri = "Yüz";

                grupDegeri += onlar[Convert.ToInt32(lira.Substring(i + 1, 1))]; //onlar

                grupDegeri += birler[Convert.ToInt32(lira.Substring(i + 2, 1))]; //birler                

                if (grupDegeri.Trim() != "") //binler
                    grupDegeri += binler[i / 3];

                if (grupDegeri.Trim() == "BirMin") //birbin düzeltiliyor.
                    grupDegeri = "Min";

                yazi += grupDegeri;
            }

            if (yazi != "")
                yazi += " Manat ";

            int yaziUzunlugu = yazi.Length;

            if (kurus.Substring(0, 1) != "0") //kuruş onlar
                yazi += onlar[Convert.ToInt32(kurus.Substring(0, 1))];

            if (kurus.Substring(1, 1) != "0") //kuruş birler
                yazi += birler[Convert.ToInt32(kurus.Substring(1, 1))];

            if (yazi.Length > yaziUzunlugu)
                yazi += "Qəpik";
            else
                yazi += "";

            return yazi.Trim();
        }

        public string transactionNumber
        {
            get
            {
                try
                {
                    int _digits = getSequence;
                    if (_digits == -1) return "";
                    string last_digits = _digits.ToString().PadLeft(6,'0');
                    return "EQ" + DateTime.Now.Year.ToString().Substring(3, 1) + DateTime.Now.DayOfYear.ToString("000") + last_digits;
                }
                catch { return ""; }
            }
        }

        public string transactionGuid
        {
            get
            {
                try
                {
                    string last_digits = Guid.NewGuid().ToString();
                    return DateTime.Now.Year.ToString().Substring(3, 1) + DateTime.Now.DayOfYear.ToString("000") + "-" + last_digits;
                }
                catch { return ""; }
            }
        }



         int getSequence
        {
            get
            {
                wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();
                int _digits = wsGController.GetSequenceNextVal();
                return _digits;
            }

        }

    }
}