﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code._Config;
using static MCS.App_Code.Enums;

namespace MCS.Customer
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsCustomerController.CustomerController wsCController = new wsCustomerController.CustomerController();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();
        _Config _config = new _Config();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                FillData();
            }

            #region Begin Permission
            CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.NEW_CUSTOMER));
            btnAddCustomer.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.CREATE_NEW_CUSTOMER));
            #endregion
        }

        void FillData()
        {
            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial seçin", ""));
            }

            //country
            DataTable dtCountry = wsGController.CountryList();
            drlCountry.DataTextField = "CountryName";
            drlCountry.DataValueField = "CountryID";
            drlCountry.DataSource = dtCountry;
            drlCountry.DataBind();
            if (dtCountry.Rows.Count > 1)
            {
                drlCountry.Items.Insert(0, new ListItem("Ölkə seçin", "0"));
            }


            //country
            DataTable dtCity = wsGController.CityList();
            drlCity.DataTextField = "CityName";
            drlCity.DataValueField = "CityID";
            drlCity.DataSource = dtCity;
            drlCity.DataBind();
            if (dtCity.Rows.Count > 1)
            {
                drlCity.Items.Insert(0, new ListItem("Şəhər seçin", "0"));
            }



            //inputs clear
            txtSurname.Text = "";
            txtName.Text = "";
            txtPatronymic.Text = "";
            txtCardNumber.Text = "";
            txtPin.Text = "";
            drlCardSerie.SelectedValue = "AZE";
            txtCardNumber.Text = "";
            txtDateOfIssue.Text = txtDateOfExpiry.Text = txtDateOfBirth.Text = "";
            txtAuthority.Text = "";
            drlCustomerGender.SelectedValue = "";
            drlCountry.SelectedValue = "0";
            drlCity.SelectedValue = "0";
            txtLegalAddress.Text = "";
            txtRegistrationAddress.Text = "";
            txtMobilePhone1.Text = txtMobilePhone2.Text = txtMobilePhone3.Text = txtHomePhone.Text = "";
            txtNote.Text = "";
            txtEmail.Text = "";
        }

        protected void btnAddCustomer_Click(object sender, EventArgs e)
        {
            if (drlBranch.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Filial seçin!");
                return;
            }
            if (txtSurname.Text.Trim() == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Soyad daxil edin.");
                return;
            }
            if (txtName.Text.Trim() == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Ad daxil edin.");
                return;
            }
            if (txtPatronymic.Text.Trim() == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Ata adı daxil edin!");
                return;
            }
            if (txtCardNumber.Text.Trim() == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Sənəd nömrəsini daxil edin!");
                return;
            }
            if (txtPin.Text.Trim() == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "FİN kodu daxil edin!");
                return;
            }
            if (txtDateOfIssue.Text.Trim().Length != 10)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Sənədin verilmə tarixini daxil edin!");
                return;
            }

            if (txtDateOfExpiry.Text.Trim().Length != 10)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Sənədin bitmə tarixini daxil edin!");
                return;
            }
            if (txtMobilePhone1.Text.Trim().Length < 7)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Mobil nömrəni daxil edin!");
                return;
            }
            wsCustomerController.AddCustomerRequest request = new wsCustomerController.AddCustomerRequest();

            request.BranchID = Convert.ToInt32(drlBranch.SelectedValue);
            request.Surname = txtSurname.Text;
            request.Name = txtName.Text;
            request.Patronymic = txtPatronymic.Text;
            request.Pin = txtPin.Text;
            request.IdCardSeries = drlCardSerie.SelectedValue;
            request.IdCardNumber = txtCardNumber.Text;
            request.DateOfIssue = txtDateOfIssue.Text;
            request.DateOfExpiry = txtDateOfExpiry.Text;
            request.DateOfBirth = txtDateOfBirth.Text;
            request.Authority = txtAuthority.Text;
            request.Gender = drlCustomerGender.SelectedValue;
            request.CountryID = drlCountry.SelectedValue == "0" ? 0 : Convert.ToInt32(drlCountry.SelectedValue);
            request.CityID = drlCity.SelectedValue == "0" ? 0 : Convert.ToInt32(drlCity.SelectedValue);
            request.LegalAddress = txtLegalAddress.Text;
            request.RegistrationAddress = txtRegistrationAddress.Text;
            request.MobilePhone1 = txtMobilePhone1.Text;
            request.MobilePhone2 = txtMobilePhone2.Text;
            request.MobilePhone3 = txtMobilePhone3.Text;
            request.HomePhone = txtHomePhone.Text;
            request.Note = txtNote.Text;
            request.Email = txtEmail.Text;
            request.CreatedID = UserSession.UserId;

            wsCustomerController.AddCustomerResponse response = wsCController.AddCustomer(request);
            if (!response.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta : " + response.errorCode);
                return;
            }
            FillData();
            _config.AlertMessage(this, MessageType.SUCCESS, "Müştəri yaradıldı.");

        }

    }
}