﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Customer.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">MÜŞTƏRİ MƏLUMATLARI</h3>
        </div>
        <div class="panel-body">
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex ="0">
                <asp:View ID="View1" runat="server">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                Filial:
                               <asp:DropDownList ID="drlBranch" class="form-control input-sm" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                Soyad:
                                <asp:TextBox ID="txtSurname" class="form-control input-sm" placeHolder="Soyad" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                Ad:
                               <asp:TextBox ID="txtName" class="form-control input-sm" placeHolder="Ad" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                Ata adı:
                               <asp:TextBox ID="txtPatronymic" class="form-control input-sm" placeHolder="Ata adı" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                FİN:
                               <asp:TextBox ID="txtPin" class="form-control input-sm" placeHolder="FİN" runat="server"></asp:TextBox>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                          Sənəd seriya:
                                          <asp:DropDownList ID="drlCardSerie" class="form-control input-sm" runat="server">
                                              <asp:ListItem Value="AZE" Selected="True">AZE</asp:ListItem>
                                              <asp:ListItem Value="AD">AD</asp:ListItem>
                                              <asp:ListItem Value="NYI">MYI</asp:ListItem>
                                              <asp:ListItem Value="DQ">DQ</asp:ListItem>
                                              <asp:ListItem Value="DYI">DYI</asp:ListItem>
                                              <asp:ListItem Value="AA">AA</asp:ListItem>
                                          </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        Sənəd nömrə:
                                        <asp:TextBox ID="txtCardNumber" class="form-control input-sm" placeHolder="Sənəd nömrə" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Sənədin verilmə tarixi:
                                    <div class="input-group">
                                        <asp:TextBox ID="txtDateOfIssue" autocomplete="off"  class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Sənədin bitmə tarixi:
                                            <div class="input-group">
                                                <asp:TextBox ID="txtDateOfExpiry" autocomplete="off"  class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                 Doğum tarixi:
                                 <div class="input-group">
                                     <asp:TextBox ID="txtDateOfBirth" autocomplete="off"  class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                     <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                 </div>
                            </div>
                            <div class="form-group">
                                Vəsiqəni verən orqan:
                               <asp:TextBox ID="txtAuthority" class="form-control input-sm" placeHolder="Vəsiqəni verən orqan" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                Cinsi:
                              <asp:DropDownList ID="drlCustomerGender" class="form-control input-sm" runat="server">
                                  <asp:ListItem Value="" Selected="True">Cinsi</asp:ListItem>
                                  <asp:ListItem Value="M">Kişi</asp:ListItem>
                                  <asp:ListItem Value="F">Qadın</asp:ListItem>
                              </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                Ölkə:
                                <asp:DropDownList ID="drlCountry" class="form-control input-sm" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                Şəhər:
                                <asp:DropDownList ID="drlCity" class="form-control input-sm" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                Yaşadığı ünvan:
                               <asp:TextBox ID="txtLegalAddress" class="form-control input-sm" placeHolder="Yaşadığı ünvan" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                Qeydiyyat ünvanı:
                               <asp:TextBox ID="txtRegistrationAddress" class="form-control input-sm" placeHolder="Qeydiyyat ünvanı" runat="server"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Mobil nömrə 1:
                                           <asp:TextBox ID="txtMobilePhone1" class="form-control input-sm" placeHolder="Mobil nömrə 1" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Mobil nömrə 2:
                                            <asp:TextBox ID="txtMobilePhone2" class="form-control input-sm" placeHolder="Mobil nömrə 2" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Mobil nömrə 3:
                                           <asp:TextBox ID="txtMobilePhone3" class="form-control input-sm" placeHolder="Mobil nömrə 3" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Ev telefonu:
                                           <asp:TextBox ID="txtHomePhone" class="form-control input-sm" placeHolder="Ev telefonu" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                Email:
                               <asp:TextBox ID="txtEmail" class="form-control input-sm" placeHolder="Email" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                Qeyd:
                               <asp:TextBox ID="txtNote" class="form-control input-sm" placeHolder="Qeyd" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="mb30"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" style="text-align: right">
                                <img id="add_customer_data" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnAddCustomer" class="btn btn-primary" runat="server" Text="Müştəri yarat"
                                    OnClick="btnAddCustomer_Click"
                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_customer_data').style.display = '';" />
                            </div>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="View2" runat="server">
                   
                </asp:View>
            </asp:MultiView>
        </div>

      
            <style>
        .input-group-addon {
            padding: 8px 10px !important;
        }

       .form-group  {
            margin-bottom: 10px;
        }  
        </style>
    </div>
</asp:Content>
