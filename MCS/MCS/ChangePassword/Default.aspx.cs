﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code._Config;

namespace MCS.ChangePassword
{
    public partial class Default : System.Web.UI.Page
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsUserController.UserController wsUController = new wsUserController.UserController();
        _Config _config = new _Config();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
               
            }
        }

        protected void btnChangePassword_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text.Trim().Length < 6)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Şifrəni 6 simvoldan az olmayaraq daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
                txtPassword.Focus();
                return;
            }

            if (txtPassword.Text.Trim() != txtPasswordConfirm.Text.Trim())
            {
                _config.AlertMessage(this, MessageType.ERROR, "Şifrələr eyni deyil.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
                txtPasswordConfirm.Focus();
                return;
            }
            wsUserController.ChangePasswordRequest request = new wsUserController.ChangePasswordRequest();
            request.UserID =  Convert.ToInt32(UserSession.UserId);
            request.NewPassword = txtPassword.Text.Trim();
            request.ModifyId = Convert.ToInt32(UserSession.UserId);

            wsUserController.ChangePasswordResponse response = wsUController.ChangePassword(request);

            if (response.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Şifrə dəyişdirildi!");
            }
            else
            {
                _config.AlertMessage(this, MessageType.ERROR, response.errorCode.Replace("'", ""));
                return;
            }
        }
    }
}