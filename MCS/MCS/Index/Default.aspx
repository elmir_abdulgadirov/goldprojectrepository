﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Index.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">   
    <script src="/lib/_dataTable/jquery-3.3.1.slim.min.js"></script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">KREDİT KALKULYATORU</h3>
        </div>
        <div class="panel-body">
            <div style="float: left; width: 90%">
                <div class="row">
                    <div style="float: left">
                        <div class="col-sm-3">
                            <div class="input-group">
                                <asp:TextBox ID="txtStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <asp:TextBox ID="txtLoanAmount"  autocomplete="off" class="form-control input-sm" placeHolder="Kredit məbləği" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtMonthlyRate"  autocomplete="off" class="form-control input-sm" placeHolder="Faiz (Aylıq)" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtMonthPeriod"   autocomplete="off" class="form-control input-sm" placeHolder="Müddət (Ay)" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-sm-2">
                            <asp:TextBox ID="txtDiscountPeriod"   autocomplete="off" class="form-control input-sm" placeHolder="Güzəşt (Ay)" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div style="float: right">
                    </div>

                </div>
            </div>
            <div style="float: right; text-align: right">
                <div class="row">
                    <div class="col-sm-12">
                        <img id="search_loan_calc" style="display: none" src="../img/loader1.gif" />
                        <asp:Button ID="btnCalculateLoan" CssClass="btn btn-default btn-quirk btn-sm" runat="server" Text="Hesabla"
                            OnClick="btnCalculateLoan_Click"
                            OnClientClick="this.style.display = 'none';
                                document.getElementById('search_loan_calc').style.display = '';" />
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>

        </div>
    </div>


    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="modal bounceIn" id="modalLoanPaymentList" data-backdrop="static">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalLoanPaymentListLabel">
                            Ödəniş cədvəli
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <a id ="lnkDownloadResult" style ="cursor:pointer"> <i class="glyphicon glyphicon-print"></i><span> Çap</span></a>
                            </div>
                            <div class="mb20"></div>
                            <div class="row">
                                <div class="col-sm-12" style ="overflow: scroll;height: 500px;">
                                    <div class="table-responsive">
                                        <asp:Literal ID="ltrLoanPaymentList" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
       
    </asp:UpdatePanel>


     <script>
         function openLoanPaymentListModal() {
             $('#modalLoanPaymentList').modal({ show: true });
         }
       </script>

    <style>

        .input-sm {
            height: 26px !important;
            line-height: 26px !important;
        }


        .input-group-addon {
            padding: 5px 7px !important;
        }

        .form-group {
            margin-bottom: 10px;
        }

        .no_visible_font {
            color: #ffffff;
        }


        .loanPayment > thead > tr > th,
        .loanPayment > tbody > tr > th,
        .loanPayment > tfoot > tr > th,
        .loanPayment > thead > tr > td,
        .loanPayment > tbody > tr > td,
        .loanPayment > tfoot > tr > td {
          padding: 5px 10px;
          line-height: 1.42857143;
          vertical-align: top;
          border-top: 1px solid #ffffff;
        }

    </style>
     <script src="../lib/_dataTable/divjs.js"></script>
    <script>
        $('#lnkDownloadResult').click(function () {
            $('.loanPayment').printElement({
            });
        })
    </script>

   

</asp:Content>
