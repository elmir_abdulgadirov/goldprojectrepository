﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code._Config;

namespace MCS.Index
{
    public partial class Default : System.Web.UI.Page
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsGeneralController.GeneralController GController = new wsGeneralController.GeneralController();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
        }

        protected void btnCalculateLoan_Click(object sender, EventArgs e)
        {
            ltrLoanPaymentList.Text = "";
            MCS.wsGeneralController.loanAmortizaiotnRequest req = new wsGeneralController.loanAmortizaiotnRequest();
            try
            {
                req.startDate = txtStartDate.Text;
                req.loanAmount = _config.ToDouble(txtLoanAmount.Text.Trim());
                req.monthlyRate = _config.ToDouble(txtMonthlyRate.Text.Trim());
                req.monthPeriod = Convert.ToInt32(txtMonthPeriod.Text.Trim());
                req.discountPeriod = Convert.ToInt32(txtDiscountPeriod.Text.Trim());
            }
            catch
            {
                _config.AlertMessage(this, MessageType.ERROR, "Məlumatlar düzgün daxil edilməyib.");
                return;
            }
            System.Data.DataTable dt = GController.LoanAmortization(req);
            if (dt == null || dt.Rows.Count == 0)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }

            string _rows = "";
            double  PrincipalAmount = 0, InterestAmount = 0, PMT = 0;
            foreach (System.Data.DataRow item in dt.Rows)
            {
                _rows += $@"
                 <tr>
                     <th scope=""row"">{item["N"]}</th>
                     <td>{item["PaymentDate"]}</td>
                     <td>{item["BeginingBalance"]}</td>
                     <td>{item["PrincipalAmount"]}</td>
                     <td>{item["InterestAmount"]}</td>
                     <td>{item["PMT"]}</td>
                     <td>{item["EndingBalance"]}</td>
                </tr> 
             ";
                PrincipalAmount += _config.ToDouble(item["PrincipalAmount"]);
                InterestAmount += _config.ToDouble(item["InterestAmount"]);
                PMT += _config.ToDouble(item["PMT"]);
            }
            _rows += $@"
                 <tr>
                     <th scope=""row""></th>
                     <td><b></td>
                     <td><b></b></td>
                     <td><b>{PrincipalAmount}</b></td>
                     <td><b>{InterestAmount}</b></td>
                     <td><b>{PMT}</b></td>
                     <td><b></b></td>
                </tr>"; 

            string _table = $@" <table class=""table table_custom_border_top_gray loanPayment"" id=""loanPayment"">
                                  <thead>
                                    <tr>
                                      <th scope=""col"">#</th>
                                      <th scope=""col"">ÖDƏMƏ TARİXİ</th>
                                      <th scope=""col"">İLKİN BALANS</th>
                                      <th scope=""col"">ƏSAS MƏBLƏĞ</th>
                                      <th scope=""col"">FAİZ MƏBLƏĞ</th>
                                      <th scope=""col"">AYLIQ ÖDƏNİŞ</th>
                                      <th scope=""col"">SON BALANS</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    {_rows}  
                                  </tbody>
                                </table>";
            ltrLoanPaymentList.Text = _table;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openLoanPaymentListModal();", true);

            txtLoanAmount.Text = "";
            txtMonthlyRate.Text = "";
            txtMonthPeriod.Text = "";
            txtDiscountPeriod.Text = "";
        }
    }
}