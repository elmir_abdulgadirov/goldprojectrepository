﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Users.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <asp:LinkButton ID="lnkAddNewUserModal" CssClass="btn btn-primary" OnClick="lnkAddNewUserModal_Click" runat="server">
            <i class ="fa fa-user-plus"></i> Yeni istifadəçi</asp:LinkButton>
            <div class="mb40"></div>
            <div class="mb40"></div>
            <div class="table-responsive">
                <asp:GridView ID="grdUsers" class="table table-bordered table-hover table-primary table-striped nomargin" runat="server" AutoGenerateColumns="False" DataKeyNames="ID,STATUS" GridLines="None">
                    <Columns>
                        <asp:BoundField DataField="Login" HeaderText="İSTİFADƏÇİ ADI" />
                        <asp:BoundField DataField="FullName" HeaderText="S.A.A" />
                        <asp:BoundField DataField="ContactNumber" HeaderText="ƏLAQƏ NÖMRƏSİ" />
                        <asp:BoundField DataField="Position" HeaderText="VƏZİFƏSİ" />
                        <asp:TemplateField HeaderText="STATUS">
                            <ItemTemplate>
                                <img src='<%#Convert.ToString(Eval("STATUS")) == "ACTIVE" ? "/img/ACCEPT24.png" : "/img/CANCEL24.png" %>' title='<%#Convert.ToString(Eval("STATUS")) == "ACTIVE" ? "Aktiv" : "Blok" %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            <HeaderStyle />
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <ItemTemplate>
                                <ul class="table-options">
                                    <li>
                                        <asp:LinkButton ID="lnkEditUser"
                                            title="Redaktə et" runat="server" OnClick="lnkEditUser_Click"
                                            class="fa fa-pencil" CommandArgument='<%#Eval("ID") %>'>
                                        </asp:LinkButton>
                                    </li>
                                    <li>
                                        <asp:LinkButton ID="lnkBlockUser" OnClick="lnkBlockUser_Click"
                                            title='<%#Convert.ToString(Eval("STATUS")) == "ACTIVE" ? "Blok et" : "Blokdan çıxar"%>' runat="server"
                                            class='<%#Convert.ToString(Eval("STATUS")) == "ACTIVE" ? "fa fa-unlock" : "fa fa-lock"%>' CommandArgument='<%#Eval("ID") %>'>
                                        </asp:LinkButton>
                                    </li>
                                    <li>
                                        <asp:LinkButton ID="lnkDeleteUser" OnClick="lnkDeleteUser_Click"
                                            title="Sil" runat="server"
                                            class="fa fa-trash" CommandArgument='<%#Eval("ID") %>'>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" Width="100px" />
                        </asp:TemplateField>
                    </Columns>

                </asp:GridView>
                <asp:HiddenField ID="hdnDeleteUser" runat="server" />
                <asp:HiddenField ID="hdnBlockUser" runat="server" />
                <asp:HiddenField ID="hdnUserStatus" runat="server" />
                <asp:HiddenField ID="hdnEditUser" runat="server" />
            </div>

            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <!-- Modal  AddUser-->
                    <div class="modal bounceIn" id="modalAddUser" data-backdrop="static">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="modalAddUserLabel"><i class="icon ion-person-add"></i>&nbsp&nbspYeni istifadəçi</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                Soyad , ad:
                                                <asp:TextBox ID="txtUserFullName" type="text" placeholder="Soyad, ad" runat="server" class="form-control" MaxLength="60"></asp:TextBox>
                                            </div>

                                            <div class="form-group">
                                                İstifadəçi adı:
                                                <asp:TextBox ID="txtLogin" type="text" placeholder="İstifadəçi adı" runat="server" class="form-control" MaxLength="30"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                Şifrə:
                                                <asp:TextBox ID="txtPassword" type="text" placeholder="Şifrə" runat="server" TextMode="Password" class="form-control" MaxLength="40"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                Şifrə təkrar:
                                                <asp:TextBox ID="txtPasswordConfirm" type="text" placeholder="Şifrə təkrar" runat="server" TextMode="Password" class="form-control" MaxLength="40"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                Əlaqə nömrəsi:
                                                <asp:TextBox ID="txtContactNumber" type="text" placeholder="Əlaqə nömrəsi" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                            </div>
                                            <div class="form-group">
                                                Cinsi:
                                                <asp:DropDownList ID="drlUserSex" class="form-control" runat="server">
                                                    <asp:ListItem Value="" Selected="True">Cinsi</asp:ListItem>
                                                    <asp:ListItem Value="M">Kişi</asp:ListItem>
                                                    <asp:ListItem Value="F">Qadın</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group">
                                                Vəzifə:
                                                <asp:TextBox ID="txtPosition" type="text" placeholder="Vəzifə" runat="server" class="form-control" MaxLength="80"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <img id="add_user_loading" style="display: none" src="../img/loader1.gif" />
                                    <asp:Button ID="btnUserAdd" class="btn btn-primary" runat="server" Text="Əlavə et" OnClick="btnUserAdd_Click"
                                        OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_user_loading').style.display = '';
                                                           document.getElementById('btnUserAddCancel').style.display = 'none';" />
                                    <button id="btnUserAddCancel" type="button" class="btn btn-default" data-dismiss="modal">İmtina et</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUserAdd" />
                </Triggers>
            </asp:UpdatePanel>


            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <!-- Modal  Alert-->
                    <div class="modal bounceIn" id="modalAlert" data-backdrop="static">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="modalAlertLabel">
                                        <asp:Literal ID="ltrAlertModalHeaderLabel" runat="server"></asp:Literal>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <asp:Literal ID="ltrAlertModalBodyLabel" runat="server"></asp:Literal>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <img id="change_userstatus_loading" style="display: none" src="../img/loader1.gif" />
                                    <asp:Button ID="btnUserChangeStatus" class="btn btn-primary" runat="server" Text="Bəli" OnClick="btnUserChangeStatus_Click"
                                        OnClientClick="this.style.display = 'none';
                                                           document.getElementById('change_userstatus_loading').style.display = '';
                                                           document.getElementById('btnUserChangeStatusCancel').style.display = 'none';" />
                                    <button id="btnUserChangeStatusCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUserChangeStatus" />
                </Triggers>
            </asp:UpdatePanel>


        </asp:View>
        <asp:View ID="View2" runat="server">
            <ul class="nav nav-tabs nav-primary">
                <li>
                    <asp:LinkButton ID="lnkBackToMain" title="Geri" runat="server" OnClick="lnkBackToMain_Click">
                        <img src ="../img/back2.png" />
                    </asp:LinkButton></li>
                <li id="tabGeneral" runat="server">
                    <asp:LinkButton ID="lnkGeneral" runat="server" CommandArgument="tab1" OnClick="lnktabs_Click"><strong>ÜMUMİ MƏLUMAT</strong></asp:LinkButton>
                </li>
                <li id="tabBranch" runat="server">
                    <asp:LinkButton ID="lnkBranch" runat="server" CommandArgument="tab2" OnClick="lnktabs_Click"><strong>FİLİAL HÜQUQLARI</strong></asp:LinkButton>
                </li>
                <li id="tabFunction" runat="server">
                    <asp:LinkButton ID="lnkFunction" runat="server" CommandArgument="tab3" OnClick="lnktabs_Click"><strong>MODUL HÜQUQLARI</strong></asp:LinkButton>
                </li>
            </ul>
            <asp:MultiView ID="MultiViewTabs" runat="server" ActiveViewIndex="0">
                <asp:View ID="View3" runat="server">
                    <div class="tab-pane">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="mb40"></div>
                                <div class="form-group">
                                    İstifadəçi adı:
                                    <asp:TextBox ID="txtEditLogin" type="text" placeholder="İstifadəçi adı" ReadOnly runat="server" class="form-control" MaxLength="30"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    Soyad , ad:
                                    <asp:TextBox ID="txtEditUserFullName" type="text" placeholder="Soyad, ad" runat="server" class="form-control" MaxLength="60"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    Əlaqə nömrəsi:
                                    <asp:TextBox ID="txtEditContactNumber" type="text" placeholder="Əlaqə nömrəsi" runat="server" class="form-control" MaxLength="20"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    Cinsi:
                                    <asp:DropDownList ID="drlEditUserSex" class="form-control" runat="server">
                                        <asp:ListItem Value="" Selected="True">Cinsi</asp:ListItem>
                                        <asp:ListItem Value="M">Kişi</asp:ListItem>
                                        <asp:ListItem Value="F">Qadın</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    Vəzifə:
                                     <asp:TextBox ID="txtEditPosition" type="text" placeholder="Vəzifə" runat="server" class="form-control" MaxLength="80"></asp:TextBox>
                                </div>

                                <div class="form-group" style="text-align: right">
                                    <img id="edit_user_data" style="display: none" src="../img/loader1.gif" />
                                    <asp:Button ID="btnEditUserData" class="btn btn-primary" runat="server" Text="Təsdiq et" OnClick="btnEditUserData_Click"
                                        OnClientClick="this.style.display = 'none';
                                                           document.getElementById('edit_user_data').style.display = '';" />
                                </div>

                            </div>
                        </div>
                        <asp:HiddenField ID="hdnEditUserDataUserId" runat="server" />
                    </div>
                </asp:View>
                <asp:View ID="View4" runat="server">
                    <div class="tab-pane">

                        <div class="mb40"></div>
                        <div class="table-responsive">
                            <asp:GridView ID="GrdUserBranchs" class="table table-bordered table-inverse nomargin" runat="server" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
                                <Columns>
                                    <asp:BoundField DataField="BranchCode" HeaderText="FİLİAL KODU" />
                                    <asp:BoundField DataField="BranchName" HeaderText="FİLİAL ADI" />
                                    <asp:BoundField DataField="Adres" HeaderText="ÜNVAN" />
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <label class="ckbox">
                                                <input type="checkbox" id="chkUserBranchHeader" runat="server" onchange="checkAllBranch(this)" />
                                                <span></span>
                                            </label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <label class="ckbox">
                                                <asp:CheckBox ID="chkUserBranch" Checked='<%#Convert.ToInt32(Eval("UserID")) == 0 ? false : true %>' runat="server"></asp:CheckBox>
                                                <span></span>
                                            </label>
                                        </ItemTemplate>
                                        <HeaderStyle Width="50px" />
                                        <ItemStyle Width="50px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <p class="mg-b-20 mg-sm-b-20"></p>
                        <div class="form-group" style="text-align: right">
                            <img id="user_edit_branch_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnUserEditBranchs" class="btn btn-primary" runat="server" Text="Təsdiqlə" OnClick="btnUserEditBranchs_Click"
                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('user_edit_branch_loading').style.display = '';" />
                        </div>

                    </div>
                </asp:View>
                <asp:View ID="View5" runat="server">
                    <div class="tab-pane">
                        <div class="mb40"></div>
                        <div class="table-responsive">
                            <asp:GridView ID="GrdUserModulesList" class="table table-inverse nomargin" runat="server" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
                                <Columns>
                                    <asp:BoundField DataField="FullName" HeaderText="Modul adı" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnUsersModuleDetails" OnClick ="btnUsersModuleDetails_Click"
                                                CssClass="btn btn-default"
                                                runat="server" Text="Funksiyalar"
                                                CommandArgument='<%#Eval("ID")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="150px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>


                        <!-- Modal  User fUNCTİONS-->
                        <div class="modal bounceIn" id="modalUsersMenuRole" data-backdrop="static">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="modalUsersMenuRoleLabel"><i class="fa fa-gear"></i>&nbsp&nbspİstifadəçinin funksiya hüquqları</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="table-responsive" style ="height:500px">
                                            <asp:GridView ID="GrdUsersFunctions" class="table mg-b-0" runat="server" DataKeyNames="Function_ID,FieldName,Menu_ID" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
                                                <Columns>
                                                    <asp:BoundField DataField="Description" HeaderText="FUNKSİYA ADI" HtmlEncode="False" />
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <label class="ckbox">
                                                                <input type="checkbox" id="chkUserRoleHeader" runat="server" onchange="checkAllFunctions(this)" />
                                                                <span></span>
                                                            </label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <label class="ckbox">
                                                                <asp:CheckBox ID="chkUserRoleFunctions" Checked='<%#Convert.ToInt32(Eval("isRole")) == 0 ? false : true %>' runat="server"></asp:CheckBox>
                                                                <span></span>
                                                            </label>
                                                        </ItemTemplate>
                                                        <HeaderStyle  Width="50px" />
                                                        <ItemStyle  Width="50px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    SEÇDİYİNİZ MODUL DAXİLİNDƏ FUNKSİYA TAPILMADI...
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <img id="menurole_loading" style="display: none" src="../img/loader1.gif" />
                                        <asp:Button ID="btnUserMenuRoleAccept" class="btn btn-primary" runat="server" Text="Təsdiqlə" OnClick ="btnUserMenuRoleAccept_Click"
                                            OnClientClick="this.style.display = 'none';
                                           document.getElementById('menurole_loading').style.display = '';
                                           document.getElementById('btnUserMenuRoleAcceptCancel').style.display = 'none';" />
                                        <button id="btnUserMenuRoleAcceptCancel" type="button" class="btn btn-default" data-dismiss="modal">Bağla</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </asp:View>
            </asp:MultiView>
        </asp:View>
    </asp:MultiView>


    <script type="text/javascript">

        function openAddUserModal() {
            $('#modalAddUser').modal({ show: true });
        }

        function openAlertModal() {
            $('#modalAlert').modal({ show: true });
        }

        function openUsermenuRoleModal() {
            $('#modalUsersMenuRole').modal({ show: true });
        }




        function checkAllBranch(ele) {
            var checkboxes = document.getElementsByTagName('input');
            if (ele.checked) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        if (checkboxes[i].id.indexOf('chkUserBranch') != -1) {
                            checkboxes[i].checked = true;
                        }
                    }
                }
            } else {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        if (checkboxes[i].id.indexOf('chkUserBranch') != -1) {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }
        }



        function checkAllFunctions(ele) {
            var checkboxes = document.getElementsByTagName('input');
            if (ele.checked) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        if (checkboxes[i].id.indexOf('chkUserRoleFunctions') != -1) {
                            checkboxes[i].checked = true;
                        }
                    }
                }
            } else {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        if (checkboxes[i].id.indexOf('chkUserRoleFunctions') != -1) {
                            checkboxes[i].checked = false;
                        }
                    }
                }
            }
        }

    </script>
</asp:Content>
