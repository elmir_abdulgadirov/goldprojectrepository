﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code._Config;
using static MCS.App_Code.Enums;




namespace MCS.Users
{
    public partial class Default :  Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsUserController.UserController wsUController = new wsUserController.UserController();
       _Config _config = new _Config();

        void FillUsers()
        {
            grdUsers.DataSource = wsUController.dtUser();
            grdUsers.DataBind();
            grdUsers.UseAccessibleHeader = true;
            grdUsers.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        void FillUserModulesList()
        {
            GrdUserModulesList.DataSource = wsUController.dtUserModulesList();
            GrdUserModulesList.DataBind();
            GrdUserModulesList.UseAccessibleHeader = true;
            GrdUserModulesList.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];

            if (!IsPostBack)
            {
                FillUsers();
                FillUserModulesList();
            }
            if (grdUsers.Rows.Count > 0) grdUsers.HeaderRow.TableSection = TableRowSection.TableHeader;
            if (GrdUserBranchs.Rows.Count > 0) GrdUserBranchs.HeaderRow.TableSection = TableRowSection.TableHeader;
            if (GrdUserModulesList.Rows.Count > 0) GrdUserModulesList.HeaderRow.TableSection = TableRowSection.TableHeader;
            if (GrdUsersFunctions.Rows.Count > 0) GrdUsersFunctions.HeaderRow.TableSection = TableRowSection.TableHeader;

            #region Begin Permission

            CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.USERS_VIEW_MAIN_PAGE));
            if (grdUsers.Rows.Count > 0)
            {
                grdUsers.Columns[5].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.EDITUSER));
            }
            lnkAddNewUserModal.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.NEWUSER));

            #endregion



        }

        protected void lnkAddNewUserModal_Click(object sender, EventArgs e)
        {
            txtUserFullName.Text = "";
            txtLogin.Text = "";
            drlUserSex.SelectedValue = "";
            txtPosition.Text = "";
            txtContactNumber.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
        }

        protected void btnUserAdd_Click(object sender, EventArgs e)
        {
            if (txtUserFullName.Text.Trim().Length < 5)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Ad,soyadı daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
                txtUserFullName.Focus();
                return;
            }

            if (txtLogin.Text.Trim().Length < 3)
            {
                _config.AlertMessage(this, MessageType.ERROR, "İstifadəçi daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
                txtLogin.Focus();
                return;
            }

            if (txtPassword.Text.Trim().Length < 6)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Şifrəni 6 simvoldan az olmayaraq daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
                txtPassword.Focus();
                return;
            }

            if (txtPassword.Text.Trim() !=  txtPasswordConfirm.Text.Trim())
            {
                _config.AlertMessage(this, MessageType.ERROR, "Şifrələr eyni deyil.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
                txtPasswordConfirm.Focus();
                return;
            }

            if (drlUserSex.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR, "Cinsi seçin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
                drlUserSex.Focus();
                return;
            }
            string image_path = drlUserSex.SelectedValue == "M" ? "/img/man.png" : "/img/woman.png";

            wsUserController.AddUserRequest request = new wsUserController.AddUserRequest();
            request.FullName = txtUserFullName.Text.Trim();
            request.ContactNumber = txtContactNumber.Text.Trim();
            request.Sex = drlUserSex.SelectedValue.Trim();
            request.Position = txtPosition.Text.Trim();
            request.Login = txtLogin.Text.Trim();
            request.Password = txtPassword.Text.Trim();
            request.Picture = image_path;
            request.Status = "ACTIVE";
            request.CreatedId =UserSession.UserId;
            wsUserController.AddUserResponse response = wsUController.AddUser(request);
            if (response.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "İstifadəçi sistemə əlavə olundu!");
            }
            else
            {
                _config.AlertMessage(this, MessageType.ERROR, response.errorCode.Replace("'",""));
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
                return;
            }
            FillUsers();
        }


        protected void lnkBlockUser_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            int userId = Convert.ToInt32(lnk.CommandArgument);

            int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
            string currstatus = Convert.ToString(grdUsers.DataKeys[rowIndex].Values["STATUS"]);
            if (currstatus == "ACTIVE")
            {
                ltrAlertModalHeaderLabel.Text = "<i class=\"glyphicon glyphicon-lock\"></i>&nbspBlock User</h4>";
                ltrAlertModalBodyLabel.Text = "İstifadəçi blok olunsun?";
                hdnUserStatus.Value = "DEACTIVE";
            }

            if (currstatus == "DEACTIVE")
            {
                ltrAlertModalHeaderLabel.Text = "<i class=\"fa fa-unlock\"></i>&nbspUnblock user</h4>";
                ltrAlertModalBodyLabel.Text = "İstifadəçi blokdan çıxarılsın?";
                hdnUserStatus.Value = "ACTIVE";
            }
            hdnBlockUser.Value = userId.ToString();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        
        }

        protected void btnUserChangeStatus_Click(object sender, EventArgs e)
        {
            int userId = 0;
            string successStatusMessage = "";
            if (hdnUserStatus.Value == "ACTIVE")
            {
                userId = Convert.ToInt32(hdnBlockUser.Value);
                successStatusMessage = "İstifaəçi blokdan çıxarıldı";
            }
            if (hdnUserStatus.Value == "DEACTIVE")
            {
                userId = Convert.ToInt32(hdnBlockUser.Value);
                successStatusMessage = "İstifadəçi blok edildi";
            }
            if (hdnUserStatus.Value == "DELETED")
            {
                userId = Convert.ToInt32(hdnDeleteUser.Value);
                successStatusMessage = "İstifadəçi silindi";
            }

            wsUserController.ChangeUserStatusRequest request = new wsUserController.ChangeUserStatusRequest();
            request.ModifyId = UserSession.UserId;
            request.UserId = userId;
            request.UserStatus = hdnUserStatus.Value;
            
          wsUserController.ChangeUserStatusResponse response = wsUController.ChangeUserStatus(request);
          if (response.isSuccess)
          {
              _config.AlertMessage(this, MessageType.SUCCESS, successStatusMessage);
          }
          else
          {
              _config.AlertMessage(this, MessageType.ERROR, response.errorCode.Replace("'", ""));
          }
          FillUsers();
        }

        protected void lnkDeleteUser_Click(object sender, EventArgs e)
        {
            ltrAlertModalHeaderLabel.Text = "<i class=\"fa fa-trash\"></i>&nbspİstifadəçinin silinməsi</h4>";
            ltrAlertModalBodyLabel.Text = "İstifadəçi silinsin?";
            LinkButton lnk = (LinkButton)sender;
            hdnDeleteUser.Value = lnk.CommandArgument;
            hdnUserStatus.Value = "DELETED";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }

        protected void lnkEditUser_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            int userId = Convert.ToInt32(lnk.CommandArgument);

            int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
            string currstatus = Convert.ToString(grdUsers.DataKeys[rowIndex].Values["STATUS"]);
            if (currstatus != "ACTIVE")
            {
                _config.AlertMessage(this, MessageType.WARNING,"Ancaq aktiv istifadəçinin məlumatlarını redaktə etməyə icazə verilir");
                return;
            }
            DataTable dtUser =  wsUController.dtGetUserByID(userId);
            if (dtUser != null)
            {
                if (dtUser.Rows.Count > 0)
                {
                    tabGeneral.Attributes.Add("class", "active");
                    tabBranch.Attributes.Remove("class");
                    tabFunction.Attributes.Remove("class");
                    DataRow drUser = dtUser.Rows[0];
                    txtEditLogin.Text = Convert.ToString(drUser["Login"]);
                    txtEditUserFullName.Text = Convert.ToString(drUser["FullName"]);
                    txtEditContactNumber.Text = Convert.ToString(drUser["ContactNumber"]);
                    txtEditPosition.Text = Convert.ToString(drUser["Position"]);
                    drlEditUserSex.SelectedValue = Convert.ToString(drUser["Sex"]);
                    hdnEditUserDataUserId.Value = userId.ToString();
                    FillUserBranchs(userId);
                    MultiView1.ActiveViewIndex = 1;
                    MultiViewTabs.ActiveViewIndex = 0;
                }              
            }          
        }


        void FillUserBranchs(int userId)
        {
            GrdUserBranchs.DataSource = wsUController.dtUserBranchList(userId);
            GrdUserBranchs.DataBind();
            GrdUserBranchs.UseAccessibleHeader = true;
            GrdUserBranchs.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void lnktabs_Click(object sender, EventArgs e)
        {
            LinkButton lnk = (LinkButton)sender;
            string tab = Convert.ToString(lnk.CommandArgument);

            if (tab == "tab1")
            {
                tabGeneral.Attributes.Add("class", "active");
                tabBranch.Attributes.Remove("class");
                tabFunction.Attributes.Remove("class");
                MultiViewTabs.ActiveViewIndex = 0;
            }
            if (tab == "tab2")
            {
                tabBranch.Attributes.Add("class", "active");
                tabGeneral.Attributes.Remove("class");
                tabFunction.Attributes.Remove("class");
                MultiViewTabs.ActiveViewIndex = 1;
            }
            if (tab == "tab3")
            {
                tabFunction.Attributes.Add("class", "active");
                tabBranch.Attributes.Remove("class");
                tabGeneral.Attributes.Remove("class");
                MultiViewTabs.ActiveViewIndex = 2;
            }
        }

        protected void lnkBackToMain_Click(object sender, EventArgs e)
        {
            FillUsers();
            MultiView1.ActiveViewIndex = 0;
        }

        protected void btnEditUserData_Click(object sender, EventArgs e)
        {
            if (drlEditUserSex.SelectedValue == "")
            {
                _config.AlertMessage(this, MessageType.ERROR,"İstifadəçinin cinsini keçin!");
                return;
            }
            int userId = Convert.ToInt32(hdnEditUserDataUserId.Value);
            wsUserController.ModifyUserRequest request = new wsUserController.ModifyUserRequest();
            request.FullName = txtEditUserFullName.Text;
            request.ContactNumber = txtEditContactNumber.Text;
            request.Sex = drlEditUserSex.SelectedValue;
            request.Position = txtEditPosition.Text;
            request.UserId = userId;
            request.ModifydId = UserSession.UserId;

            string _image = "/img/man.png";
            if (drlEditUserSex.SelectedValue == "M") _image = "/img/man.png";
            if (drlEditUserSex.SelectedValue == "F") _image = "/img/woman.png";

            request.Picture = _image;

            wsUserController.ModifyUserResponse response =  wsUController.ModifyUser(request);

            if (response.isSuccess)
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Ümumi məlumatlar təsdiq edildi");
            }
            else
            {
                _config.AlertMessage(this, MessageType.ERROR, response.errorCode.Replace("'", ""));
            }
        }


        protected void btnUserEditBranchs_Click(object sender, EventArgs e)
        {
            int userId = Convert.ToInt32(hdnEditUserDataUserId.Value);
            wsUserController.DeleteUserBranchRequest requestDelete = new wsUserController.DeleteUserBranchRequest();
            requestDelete.UserID = userId;
            wsUserController.DeleteUserBranchResponse responseDelete = wsUController.DeleteUserBranch(requestDelete);
            if (!responseDelete.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta: " + responseDelete.errorCode);
                return;
            }

            for (int i = 0; i < GrdUserBranchs.Rows.Count; i++)
            {
                CheckBox chk = (CheckBox)GrdUserBranchs.Rows[i].Cells[3].FindControl("chkUserBranch");
                if (chk.Checked)
                {
                    wsUserController.AddUserBranchRequest addBranchRequest = new wsUserController.AddUserBranchRequest();
                    addBranchRequest.BranchCode = Convert.ToString(GrdUserBranchs.Rows[i].Cells[0].Text);
                    addBranchRequest.UserID = userId;
                    addBranchRequest.CreatedID = UserSession.UserId;
                    wsUserController.AddUserBranchResponse response = wsUController.AddUserBranch(addBranchRequest);
                    if (!response.isSuccess)
                    {
                        _config.AlertMessage(this, MessageType.ERROR, "Xəta.: " + response.errorCode);
                        return;
                    }
                }
            }
            FillUserBranchs(userId);
            _config.AlertMessage(this, MessageType.SUCCESS, "Filial hüquqları təsdiqləndi");

        }

        void FillUserRoleFunctions(int userID, int menuID)
        {
            DataTable dt = wsUController.dtUserModuleFunctionsList(userID, menuID);
            if (dt != null)
            {
                GrdUsersFunctions.DataSource = dt;
                GrdUsersFunctions.DataBind();
                GrdUsersFunctions.UseAccessibleHeader = true;
                if (dt.Rows.Count > 0) GrdUsersFunctions.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void btnUsersModuleDetails_Click(object sender, EventArgs e)
        {
            int userId = Convert.ToInt32(hdnEditUserDataUserId.Value);
            Button btnThis = (Button)sender;
            int menuID = Convert.ToInt32(btnThis.CommandArgument);

            FillUserRoleFunctions(userId, menuID);
            //Dashboard
            if (menuID == 1)
            {
                btnUserMenuRoleAccept.Attributes["disabled"] = "disabled";
            }
            else
            {
                if (GrdUsersFunctions.Rows.Count == 0)
                {
                    btnUserMenuRoleAccept.Attributes["disabled"] = "disabled";
                }
                else
                {
                    btnUserMenuRoleAccept.Attributes.Remove("disabled");
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openUsermenuRoleModal();", true);
        }

      
        protected void btnUserMenuRoleAccept_Click(object sender, EventArgs e)
        {
            int userId = Convert.ToInt32(hdnEditUserDataUserId.Value);
           
            //first delete all function permission
            if (GrdUsersFunctions.Rows.Count == 0) return;
            int Menu_ID = (int)this.GrdUsersFunctions.DataKeys[0]["Menu_ID"];
            wsUserController.DeleteUserFunctionRequest req_d = new wsUserController.DeleteUserFunctionRequest();
            req_d.UserID = userId;
            req_d.MenuID = Menu_ID;
            wsUserController.DeleteUserFunctionRespnse res_d = wsUController.DeleteUserFunction(req_d);
            if (!res_d.isSuccess)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta: " + res_d.errorCode);
                return;
            }

            //second add function permission
            DataTable dtTemp = new DataTable("UserFunctionParams");
            dtTemp.Columns.Add("pMenuId", typeof(int));
            dtTemp.Columns.Add("pUserId", typeof(int));
            dtTemp.Columns.Add("pFunctionId", typeof(string));


            string Function_ID = "", FieldName = "";
            string main_page_function_id = "";
            bool chkFunction, selectedIdEmpty = false; 
            for (int i = 0; i < GrdUsersFunctions.Rows.Count; i++)
            {
                Function_ID = (string)this.GrdUsersFunctions.DataKeys[i]["Function_ID"];
                FieldName = (string)this.GrdUsersFunctions.DataKeys[i]["FieldName"];

                if (FieldName == "_MAIN_PAGE") main_page_function_id = Function_ID;
                chkFunction = ((CheckBox)GrdUsersFunctions.Rows[i].Cells[1].FindControl("chkUserRoleFunctions")).Checked;

                if (chkFunction)
                {
                    selectedIdEmpty = true;
                    if (FieldName != "_MAIN_PAGE")
                    {
                        dtTemp.Rows.Add(Menu_ID, userId, Function_ID);
                    }
                }
            }

            if (selectedIdEmpty)
            {
                if (main_page_function_id != "") dtTemp.Rows.Add(Menu_ID, userId, main_page_function_id);
            }
            else
            {
                return;
            }

            // add
            for (int i = 0; i < dtTemp.Rows.Count; i++)
            {
                wsUserController.AddUserFunctionRequest req_a = new wsUserController.AddUserFunctionRequest();
                req_a.UserID = Convert.ToInt32(dtTemp.Rows[i]["pUserId"]);
                req_a.MenuID = Convert.ToInt32(dtTemp.Rows[i]["pMenuId"]);
                req_a.FunctonID = Convert.ToString(dtTemp.Rows[i]["pFunctionId"]);
                req_a.CreatedID = UserSession.UserId;

                wsUserController.AddUserFunctionRespnse res_a = wsUController.AddUserFunction(req_a);
                if (!res_a.isSuccess)
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Xəta: " + res_d.errorCode);
                    return;
                }
            }
            _config.AlertMessage(this, MessageType.SUCCESS, "Funksiya hüquqları təsdiqləndi");


        }
    }
}