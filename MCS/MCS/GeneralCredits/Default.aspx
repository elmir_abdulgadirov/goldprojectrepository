﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.GeneralCredits.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-body">
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <ul class="list-inline">
                        <li>
                            <asp:LinkButton ID="lnkNewLoan" runat="server">
                               <i class="fa fa-plus-square-o"></i><span> Yeni kredit</span>
                            </asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="lnkOpenModalSearchLoan" runat="server" OnClick="lnkOpenModalSearchLoan_Click">
                                <i class="fa fa-search"></i><span> Axtarış</span>
                            </asp:LinkButton>
                        </li>

                         <li>
                            <asp:LinkButton ID="lnkLoanListRefresh" runat="server" OnClick ="lnkLoanListRefresh_Click" >
                                <i class="glyphicon glyphicon-refresh"></i><span> </span>
                            </asp:LinkButton>
                        </li>

                    </ul>
                    <div class="mb40"></div>
                    <div class="mb40"></div>
                    <div class="table-responsive">
                        <asp:GridView class="table table-hover table-striped nomargin table_custom_border_top_gray" ID="grdLoans" runat="server"
                            DataKeyNames="LoanID,isApprove"
                            AutoGenerateColumns="False" GridLines="None" AllowPaging="True" AllowCustomPaging="True"
                            OnPageIndexChanging="grdLoans_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="S.A.A & FİLİAL">
                                    <ItemTemplate>
                                        <%#Convert.ToString(Eval("CustomerName")) + "<br/>" + 
                                          "<b><i>Filial</i></b> - " +Convert.ToString(Eval("BranchName"))  %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="SƏNƏD №">
                                    <ItemTemplate>
                                        <%#"<b><i>Girov №</i></b> - " + Convert.ToString(Eval("CollateralNumber")) + "<br/>" + 
                                           "<b><i>Müqavilə №</i></b> - " + Convert.ToString(Eval("AgreementNumber"))  %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="TARİX">
                                    <ItemTemplate>
                                        <%#"<b><i>Verilmə tarixi</i></b> - " + Convert.ToString(Eval("StartDate")) + "<br/>" + 
                                           "<b><i>Ödəmə tarixi</i></b> - " + Convert.ToString(Eval("PaymentDate")) + "<br/>" +
                                           "<b><i>Bitmə tarixi</i></b> - " + Convert.ToString(Eval("ActualExpiryDate"))%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="MƏBLƏĞ & FAİZ">
                                    <ItemTemplate>
                                        <%#"<b><i>Kredit məbləğ</i></b> - " + Convert.ToString(Eval("LAmount")) + "<br/>" + 
                                           "<b><i>Ödəmə məbləğ</i></b> - " + Convert.ToString(Eval("PAmount")) + "<br/>" +
                                           "<b><i>Kredit qalığı</i></b> - " + Convert.ToString(Eval("LoanBalance")) + "<br/>" +
                                           "<b><i>Kredit faizi</i></b> - " + Convert.ToString(Eval("LoanInterest"))%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="DİGƏR MƏLUMATLAR">
                                    <ItemTemplate>
                                        <%#"<b><i>Ödəmə metodu</i></b> - " + Convert.ToString(Eval("MethodName")) + "<br/>" + 
                                           "<b><i>Valyuta</i></b> - " + Convert.ToString(Eval("CurrencyName")) + "<br/>" +
                                           "<b><i>Kredit statusu</i></b> - " + Convert.ToString(Eval("LoanStatus")) + "<br/>" +
                                           "<b><i>Ödəmə günü</i></b> : "  %>
                                        <asp:Label ID="lblCountOfDayPaymentRed" runat="server"
                                            Text='<%#Convert.ToInt32(Eval("CountOfDayPayment")) < 0 ?  Convert.ToString(Eval("CountOfDayPayment")) : "" %>'
                                            ForeColor="#CC0000" Font-Bold="True"></asp:Label>
                                        <asp:Label ID="lblCountOfDayPaymentBlack" runat="server" Font-Bold="True"
                                            Text='<%#Convert.ToInt32(Eval("CountOfDayPayment")) >= 0 ?  Convert.ToString(Eval("CountOfDayPayment")) : "" %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <ul class="table-options">
                                            <li>
                                                <asp:LinkButton ID="lnkEditLoanData" CommandArgument='<%# Eval("LoanID") %>' title="Ətraflı" 
                                                   OnClick ="lnkEditLoanData_Click" runat="server"><span class="glyphicon glyphicon-chevron-right"></span></asp:LinkButton>
                                            </li>
                                        </ul>
                                    </ItemTemplate>
                                    <ItemStyle Width="32px" HorizontalAlign="Right" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                Məlumat yoxdur...
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="bg-dark" />
                            <PagerStyle CssClass="pagination-ys" />
                        </asp:GridView>
                        <asp:HiddenField ID="hdnGridLoansPageIndex" runat="server" />
                    </div>



                    <!--Begin modal Search Loan-->
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="modal fade" id="modalSearchLoan" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="modalSearchLoanLabel">
                                            Kredit axtarışı
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        Müştəri adı<span class="text-danger"></span>
                                                        <asp:TextBox ID="txtSearchCustomerName" runat="server"
                                                            autocomplete="off" class="form-control input-sm" placeholder="Müştəri adı"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        Girov №<span class="text-danger"></span>
                                                        <asp:TextBox ID="txtSearchCollateralNumber" runat="server"
                                                            autocomplete="off" class="form-control input-sm" placeholder="Girov №"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb10"></div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        Qrafik<span class="text-danger"></span>
                                                        <asp:DropDownList ID="drlSearchQrafikType" class="form-control input-sm" runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        Girov növü<span class="text-danger"></span>
                                                        <asp:DropDownList ID="drlSearchCollateralTypes" class="form-control input-sm" runat="server">
                                                            <asp:ListItem Value="" Selected="True">Seçin</asp:ListItem>
                                                            <asp:ListItem>Girovlu</asp:ListItem>
                                                            <asp:ListItem>Girovsuz</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="mb10"></div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        Mobil nömrə 1<span class="text-danger"></span>
                                                        <asp:TextBox ID="txtSearchMobilePhone1" runat="server"
                                                            autocomplete="off" class="form-control input-sm" placeholder="Mobil nömrə 1"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        Mobil nömrə 2<span class="text-danger"></span>
                                                        <asp:TextBox ID="txtSearchMobilePhone2" runat="server"
                                                            autocomplete="off" class="form-control input-sm" placeholder="Mobil nömrə 2"></asp:TextBox>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="mb10"></div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        İlk tarix<span class="text-danger"></span>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtSearchStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        Valyuta<span class="text-danger"></span>
                                                        <asp:DropDownList ID="drlSearchCurrency" class="form-control input-sm" runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb10"></div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        Kredit məbləği<span class="text-danger"></span>
                                                        <asp:TextBox ID="txtSearchAmount" autocomplete="off" class="form-control input-sm" placeHolder="Kredit məbləği" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        Status<span class="text-danger"></span>
                                                        <asp:DropDownList ID="drlSearchLoanStatus" class="form-control input-sm" runat="server">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <img id="search_loan_loading" style="display: none" src="../img/loader1.gif" />
                                            <asp:Button ID="btnSearchLoan" class="btn btn-primary" runat="server" Text="Axtar" OnClick ="btnSearchLoan_Click"
                                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('search_loan_loading').style.display = '';" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnSearchLoan" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <!--End modal Search Loan-->


                </asp:View>
            </asp:MultiView>
        </div>

        <script>
            function openSearchLoanModal() {
                $('#modalSearchLoan').modal({ show: true });
            }
        </script>

        <style>
            .input-sm {
                height: 26px !important;
                line-height: 26px !important;
            }


            .input-group-addon {
                padding: 5px 7px !important;
            }

            .form-group {
                margin-bottom: 10px;
            }

            .no_visible_font {
                color: #ffffff;
            }



            .input-group-btn_btngroup_custom .btn:not(.btn-lg),
            .input-group-btn_btngroup_custom .btn:not(.btn-sm),
            .input-group-btn_btngroup_custom .btn:not(.btn-xs) {
                min-height: 26px !important;
            }

            .collateral > a.collapsed {
                padding: 10px 15px !important;
                color: #505b72;
            }
        </style>

        
          
         <style>
            .ui-datepicker {
                z-index: 9999 !important; /* has to be larger than 1050 */
            }

        </style>

     

    </div>
</asp:Content>
