﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code._Config;
using static MCS.App_Code.Enums;

namespace MCS.GeneralCredits
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsLoanController.LoanController wsLController = new MCS.wsLoanController.LoanController();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();

        _Config _config = new _Config();
        int pageIndex = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["SearchLoanRequestBack"]) == "1")
                {
                    if (Session["SearchLoanRequest"] != null)
                    {
                        wsLoanController.GetLoanListRequest req = (wsLoanController.GetLoanListRequest)Session["SearchLoanRequest"];
                        FillLoans(req);
                    }
                    Session["SearchLoanRequestBack"] = "";
                    Session.Remove("SearchLoanRequestBack");
                }
                else
                {
                    hdnGridLoansPageIndex.Value = "0";
                    pageIndex = Convert.ToInt32(hdnGridLoansPageIndex.Value);
                    FillLoans(pageIndex, "", "", 0, "", "", "", "", 0, 0, 0);
                }
            }
            fillGridLoansBackColor();


            #region Begin Permission
            CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.GENERAL_CREDITS));
            if (grdLoans.Rows.Count > 0)
            {
                grdLoans.Columns[5].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.EDIT_LOAN));
            }
            lnkNewLoan.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.CREDIT_ADD_NEW));
            lnkOpenModalSearchLoan.Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.SEARCH_FORM));
            #endregion
        }

        void FillLoans(int pageIndex, string CustomerName, string CollateralNumber, int LoanMethodID, string MobilePhone1, string MobilePhone2, string StartDate, string CollateralType, int CurrencyID, float LoanAmount, int LoanSTatusID)
        {
            wsLoanController.GetLoanListRequest req = new wsLoanController.GetLoanListRequest();
            req.pageIndex = (pageIndex + 1);
            req.pageSize = Convert.ToInt32(_config.GetAppSetting("GridPageSize"));
            req.CustomerName = CustomerName;
            req.CollateralNumber = CollateralNumber;
            req.LoanMethodID = LoanMethodID;
            req.MobilePhone1 = MobilePhone1;
            req.MobilePhone2 = MobilePhone2;
            req.StartDate = StartDate;
            req.CollateralType = CollateralType;
            req.CurrencyID = CurrencyID;
            req.LoanAmount = LoanAmount;
            req.LoanSTatusID = LoanSTatusID;

            req.UserId = UserSession.UserId;
            int RecordSize = 0;
            DataTable dtLoans = wsLController.GetLoanList(req, out RecordSize);
            if (dtLoans == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }

            grdLoans.VirtualItemCount = RecordSize;
            grdLoans.PageIndex = pageIndex;
            grdLoans.PageSize = Convert.ToInt32(_config.GetAppSetting("GridPageSize"));
            grdLoans.DataSource = dtLoans;
            grdLoans.DataBind();
            fillGridLoansBackColor();
            Session["SearchLoanRequest"] = req;
        }


        void FillLoans(wsLoanController.GetLoanListRequest req)
        {
            int RecordSize = 0;
            DataTable dtLoans = wsLController.GetLoanList(req, out RecordSize);
            if (dtLoans == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi.");
                return;
            }

            grdLoans.VirtualItemCount = RecordSize;
            grdLoans.PageIndex = req.pageIndex -1;
            grdLoans.PageSize = Convert.ToInt32(_config.GetAppSetting("GridPageSize"));
            grdLoans.DataSource = dtLoans;
            grdLoans.DataBind();
            fillGridLoansBackColor();
        }


        void fillGridLoansBackColor()
        {
            for (int i = 0; i < grdLoans.Rows.Count; i++)
            {
                bool isApprove = Convert.ToBoolean(grdLoans.DataKeys[i].Values["isApprove"]);
                if (!isApprove)
                {
                    grdLoans.Rows[i].BackColor = System.Drawing.Color.FromName("#eaad07");
                }
            }
        }


        protected void grdLoans_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            hdnGridLoansPageIndex.Value = Convert.ToString(e.NewPageIndex);
            string CustomerName = txtSearchCustomerName.Text;
            string CollateralNumber = txtSearchCollateralNumber.Text;
            int LoanMethodID = drlSearchQrafikType.SelectedValue != "" ? Convert.ToInt32(drlSearchQrafikType.SelectedValue) : 0;
            string MobilePhone1 = txtSearchMobilePhone1.Text;
            string MobilePhone2 = txtSearchMobilePhone2.Text;
            string StartDate = txtSearchStartDate.Text;
            string CollateralType = drlSearchCollateralTypes.SelectedValue;
            int CurrencyID = drlSearchCurrency.SelectedValue != "" ? Convert.ToInt32(drlSearchCurrency.SelectedValue) : 0;
            float LoanAmount = txtSearchAmount.Text.Trim() != "" ? float.Parse(txtSearchAmount.Text.Trim()) : 0;
            int LoanSTatusID = drlSearchLoanStatus.SelectedValue != "" ? Convert.ToInt32(drlSearchLoanStatus.SelectedValue) : 0;

            FillLoans(Convert.ToInt32(hdnGridLoansPageIndex.Value), CustomerName, CollateralNumber, LoanMethodID, MobilePhone1, MobilePhone2, StartDate, CollateralType, CurrencyID, LoanAmount, LoanSTatusID);
        }


        //open search modal
        protected void lnkOpenModalSearchLoan_Click(object sender, EventArgs e)
        {
            fillSearchComponents();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openSearchLoanModal();", true);
        }


        void fillSearchComponents()
        {
            DataTable dtQrafik = wsLController.LoanMethodList();
            if (dtQrafik.Rows.Count > 0)
            {
                drlSearchQrafikType.Items.Clear();
                drlSearchQrafikType.DataTextField = "MethodName";
                drlSearchQrafikType.DataValueField = "MethodID";
                drlSearchQrafikType.DataSource = dtQrafik;
                drlSearchQrafikType.DataBind();
                drlSearchQrafikType.Items.Insert(0, new ListItem("Qrafik seçin", ""));

            }


            DataTable dtLoanStatus = wsLController.LoanStatusList();
            if (dtLoanStatus.Rows.Count > 0)
            {
                drlSearchLoanStatus.Items.Clear();
                drlSearchLoanStatus.DataTextField = "LoanStatus";
                drlSearchLoanStatus.DataValueField = "LoanStatusID";
                drlSearchLoanStatus.DataSource = dtLoanStatus;
                drlSearchLoanStatus.DataBind();
                drlSearchCurrency.Items.Insert(0, new ListItem("Status seçin", ""));
            }

            DataTable dtCurrency = wsGController.CurrencyList();
            if (dtCurrency.Rows.Count > 0)
            {
                drlSearchCurrency.Items.Clear();
                drlSearchCurrency.DataTextField = "CurrencyName";
                drlSearchCurrency.DataValueField = "CurrencyID";
                drlSearchCurrency.DataSource = dtCurrency;
                drlSearchCurrency.DataBind();
                drlSearchCurrency.Items.Insert(0, new ListItem("Valyuta seçin", ""));
            }


        }

        //refresh loan list
        protected void lnkLoanListRefresh_Click(object sender, EventArgs e)
        {
            _Config.Rd("/generalcredits");
        }

        //search loan
        protected void btnSearchLoan_Click(object sender, EventArgs e)
        {
            if (txtSearchStartDate.Text != "")
            {
                if (!_config.isDate(txtSearchStartDate.Text))
                {
                    _config.AlertMessage(this, MessageType.ERROR, "İlk tarix daxil edin!");
                    return;
                }
            }

            float _amount = 0;
            if (txtSearchAmount.Text.Trim() != "")
            {
                try { _amount = float.Parse(txtSearchAmount.Text); }
                catch
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Məbləği düzgün daxil edin!");
                    return;
                }
            }

            string CustomerName = txtSearchCustomerName.Text;
            string CollateralNumber = txtSearchCollateralNumber.Text;
            int LoanMethodID = drlSearchQrafikType.SelectedValue != "" ? Convert.ToInt32(drlSearchQrafikType.SelectedValue) : 0;
            string MobilePhone1 = txtSearchMobilePhone1.Text;
            string MobilePhone2 = txtSearchMobilePhone2.Text;
            string StartDate = txtSearchStartDate.Text;
            string CollateralType = drlSearchCollateralTypes.SelectedValue;
            int CurrencyID = drlSearchCurrency.SelectedValue != "" ? Convert.ToInt32(drlSearchCurrency.SelectedValue) : 0;
            float LoanAmount = _amount;
            int LoanSTatusID = drlSearchLoanStatus.SelectedValue != "" ? Convert.ToInt32(drlSearchLoanStatus.SelectedValue) : 0;

            FillLoans(0, CustomerName, CollateralNumber, LoanMethodID, MobilePhone1, MobilePhone2, StartDate, CollateralType, CurrencyID, LoanAmount, LoanSTatusID);
        }

        //redirect to details page
        protected void lnkEditLoanData_Click(object sender, EventArgs e)
        {
            LinkButton lnkEditLoanData = (LinkButton)sender;
            string LoanID = lnkEditLoanData.CommandArgument;
            _Config.Rd("/CreditDetails/?LoanID=" + LoanID);

        }
    }
}