﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static MCS.App_Code.Enums;

namespace MCS.Generaltrans
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsTransactionController.TransactionController wsTController = new wsTransactionController.TransactionController();
        MCS.wsAccountController.AccountController wsAController = new wsAccountController.AccountController();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                txtEndDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                FillAndClearForm();
                fillGrid();
            }

            #region Begin Permission
            CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.GENERAL_TRANSACTION));
            if (grdTransactions.Rows.Count > 0)
            {
                grdTransactions.Columns[7].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.DELETE_GENERAL_TRAN));
            }
            #endregion
        }

        void fillGrid()
        {
            if (!_config.isDate(txtStartDate.Text))
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Başlanğıc tarix düzgün deyil");
                return;
            }

            if (!_config.isDate(txtEndDate.Text))
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Son tarix düzgün deyil");
                return;
            }
            wsTransactionController.GetGeneralTranListRequest req = new wsTransactionController.GetGeneralTranListRequest();
            req.BranchID = Convert.ToInt32(drlBranch.SelectedValue);
            req.DateFrom = txtStartDate.Text;
            req.DateTo = txtEndDate.Text;
            req.UserID = UserSession.UserId;
            DataTable dt = wsTController.GetGeneralTranList(req);
            if (dt == null)
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Xəta baş verdi.");
                return;
            }
            grdTransactions.DataSource = dt;
            grdTransactions.DataBind();
        }


        void FillAndClearForm()
        {
            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial", "0"));
            }
        }

        protected void lnkDeleteTranModal_Click(object sender, EventArgs e)
        {
            string msgID = (sender as LinkButton).CommandArgument;
            hdnSelectedTranMsgID.Value = msgID;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openDeleteTransactionModal();", true);
        }

        protected void btnSearchTran_Click(object sender, EventArgs e)
        {
            fillGrid();
        }

        //delete (reverse) transactions
        protected void btnDeleteTransaction_Click(object sender, EventArgs e)
        {
            string msgID = hdnSelectedTranMsgID.Value;
            wsTransactionController.reverseTranRequest req = new wsTransactionController.reverseTranRequest();
            req.MsgID = msgID;
            req.ModifiedID = UserSession.UserId;
            req.Description = "Reverse transaction process";
            wsTransactionController.reverseTranResponse res = wsTController.ReverseTransaction(req);
            if (!res.isSuccess)
            {
                _config.AlertMessage(this, _Config.MessageType.ERROR, "Xəta : " + res.errorCode);
                return;
            }
            _config.AlertMessage(this, _Config.MessageType.SUCCESS, "Əməliyyat silindi.");
            fillGrid();
        }
    }
}