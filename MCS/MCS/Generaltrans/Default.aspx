﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Generaltrans.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--List operation-->
    <div class="row">
        <div class="col-sm-12">
            <div style="float: left; width: 50%; text-align: left">
                <ul class="list-inline">
                    <li></li>
                </ul>
            </div>
            <div style="float: right; text-align: right; width: 50%">
                <div class="row">
                    <div class="col-sm-4">
                        <asp:DropDownList ID="drlBranch" class="form-control input-sm" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <asp:TextBox ID="txtStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                            <asp:TextBox ID="txtEndDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <img id="search_tran_data" style="display: none" src="../img/loader1.gif" />
                        <asp:Button ID="btnSearchTran" CssClass="btn btn-default btn-quirk btn-sm"
                            OnClick ="btnSearchTran_Click"
                            Style="margin: 0; width: 100%" runat="server" Text="YENİLƏ"
                            OnClientClick="this.style.display = 'none';
                                            document.getElementById('search_tran_data').style.display = '';" />
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
        <div class="mb40"></div>
        <div class="mb40"></div>
        <div class="table-responsive">
            <asp:GridView ID="grdTransactions" class="table table-bordered table-primary table-striped nomargin" runat="server"
                AutoGenerateColumns="False" GridLines="None">
                <Columns>
                    <asp:TemplateField HeaderText="TARİX/FİLİAL">
                        <ItemTemplate>
                            <%#Eval("TranDate")%>
                            <br />
                            <%#Eval("BranchName")%>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                        <HeaderStyle />
                    </asp:TemplateField>
                    <asp:BoundField DataField="DTAccount" ItemStyle-Width="120" HeaderText="DEBET" />
                    <asp:BoundField DataField="DTSubAccount" HeaderText="SUB HESAB" />
                    <asp:BoundField DataField="CTAccount" ItemStyle-Width="120" HeaderText="KREDİT" />
                    <asp:BoundField DataField="CTSubAccount" HeaderText="SUB HESAB" />
                    <asp:BoundField DataField="CurrencyName" ItemStyle-Width="100" HeaderText="VALYUTA" />
                    <asp:BoundField DataField="Amount" ItemStyle-Width="100" HeaderText="MƏBLƏĞ" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDeleteTranModal"
                                OnClick ="lnkDeleteTranModal_Click"
                                CommandArgument='<%#Eval("MsgID")%>'
                                ToolTip="Sil" runat="server">
                                                    <span class ="glyphicon glyphicon-trash"></span>
                            </asp:LinkButton>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="70px" />
                        <HeaderStyle />
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>Əməliyyat tapılmadı...</EmptyDataTemplate>
                <HeaderStyle CssClass="bg-blue" />
            </asp:GridView>
            <asp:HiddenField ID="hdnSelectedTranMsgID" runat="server" />
        </div>
    </div>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Modal  Alert-->
            <div class="modal bounceIn" id="modalDeleteTranAlert" data-backdrop="static">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="modalDeleteTranAlertLabel">
                                <i class="fa fa-trash"></i>&nbsp;Əməliyyatın silinməsi
                            </h4>
                        </div>
                        <div class="modal-body">
                            <p>
                                Əməliyyat silinsin?
                            </p>
                        </div>
                        <div class="modal-footer">
                            <img id="delete_transaction_loading" style="display: none" src="../img/loader1.gif" />
                            <asp:Button ID="btnDeleteTransaction" class="btn btn-primary" runat="server" Text="Bəli"
                                OnClick ="btnDeleteTransaction_Click"
                                OnClientClick="this.style.display = 'none';
                                                   document.getElementById('delete_transaction_loading').style.display = '';
                                                   document.getElementById('btnDeleteTransactionCancel').style.display = 'none';" />
                            <button id="btnDeleteTransactionCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDeleteTransaction" />
        </Triggers>
    </asp:UpdatePanel>

    <script>

        function openDeleteTransactionModal() {
            $('#modalDeleteTranAlert').modal({ show: true });
        }

    </script>

    <style>
        .input-sm {
            height: 26px !important;
            line-height: 26px !important;
        }

        .input-group-addon {
            padding: 5px 7px !important;
        }

        .form-group {
            margin-bottom: 10px;
        }
    </style>

</asp:Content>
