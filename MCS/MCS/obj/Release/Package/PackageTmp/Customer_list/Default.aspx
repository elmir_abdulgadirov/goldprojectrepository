﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Customer_list.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">MÜŞTƏRİ SİYAHISI</h3>
        </div>
        <div class="panel-body">
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <div class="panel-body">
                        <ul class="list-inline">
                            <li>
                                <asp:LinkButton ID="lnkNewCustomer" OnClick="lnkNewCustomer_Click" runat="server">
                        <i class="fa fa-user-plus"></i><span> Yeni müştəri</span>
                                </asp:LinkButton>
                            </li>
                            <li>
                                <asp:LinkButton ID="lnkSearchCustomer" runat="server" OnClick ="lnkSearchCustomer_Click">
                        <i class="fa fa-search"></i><span> Axtarış</span>
                                </asp:LinkButton>
                            </li>
                        </ul>


                        <div class="mb40"></div>
                        <div class="mb40"></div>
                        <div class="table-responsive">
                            <asp:GridView class="table table-hover table-striped nomargin table_custom_border_top_gray" ID="grdCustomers" runat="server"
                                DataKeyNames="CustomerID,DocumentNumber"
                                AutoGenerateColumns="False" GridLines="None" AllowPaging="True" AllowCustomPaging="True"
                                OnPageIndexChanging="grdCustomers_PageIndexChanging">
                                <Columns>
                                    <asp:BoundField DataField="FullName" HeaderText="S.A.A" />
                                    <asp:BoundField DataField="DocumentNumber" HeaderText="MÜŞTƏRİ KODU" />
                                    <asp:BoundField DataField="BranchName" HeaderText="FİLİAL" />
                                    <asp:TemplateField HeaderText="SƏNƏD NÖMRƏSİ">
                                        <HeaderTemplate>
                                            SƏNƏD NÖMRƏSİ
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%#(Convert.ToString(Eval("Pin")).Trim().Length > 0 ?  "<b>FİN</b> : " + Eval("Pin") + "<br/>" : "") +  Eval("IdCardSeries") + " " +  Eval("IdCardNumber") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ÜNVAN">
                                        <HeaderTemplate>
                                            ÜNVAN
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Eval("LegalAddress") + "<br/>" +  Eval("RegistrationAddress") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <ul class="table-options">
                                                <li>
                                                    <asp:LinkButton ID="lnkEditCustomerData" CommandArgument='<%# Eval("CustomerID") %>' OnClick="lnkEditCustomerData_Click" title="Dəyişdir" runat="server"><i class="fa fa-pencil"></i></asp:LinkButton>
                                                </li>
                                            </ul>
                                        </ItemTemplate>
                                        <ItemStyle Width="32px" HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <ul class="table-options">
                                                <li>
                                                    <asp:LinkButton ID="lnkDeleteCustomerData" CommandArgument='<%# Eval("CustomerID") %>' OnClick="lnkDeleteCustomerData_Click" title="Sil" runat="server"><i class="fa fa-trash"></i></asp:LinkButton>
                                                </li>
                                            </ul>
                                        </ItemTemplate>
                                        <ItemStyle Width="32px" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="bg-dark" />
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <asp:HiddenField ID="hdnCustomerID" runat="server" />
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                Filial:
								<asp:DropDownList ID="drlBranch" class="form-control input-sm" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                Soyad:
                               <asp:TextBox ID="txtSurname" class="form-control input-sm" placeHolder="Soyad" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                Ad:
                               <asp:TextBox ID="txtName" class="form-control input-sm" placeHolder="Ad" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                Ata adı:
                                <asp:TextBox ID="txtPatronymic" class="form-control input-sm" placeHolder="Ata adı" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                FİN:
                                <asp:TextBox ID="txtPin" class="form-control input-sm" placeHolder="FİN" runat="server"></asp:TextBox>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        Sənəd seriya:
										<asp:DropDownList ID="drlCardSerie" class="form-control input-sm" runat="server">
                                            <asp:ListItem Value="AZE" Selected="True">AZE</asp:ListItem>
                                            <asp:ListItem Value="AD">AD</asp:ListItem>
                                            <asp:ListItem Value="NYI">MYI</asp:ListItem>
                                            <asp:ListItem Value="DQ">DQ</asp:ListItem>
                                            <asp:ListItem Value="DYI">DYI</asp:ListItem>
                                            <asp:ListItem Value="AA">AA</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        Sənəd nömrə:
                                        <asp:TextBox ID="txtCardNumber" class="form-control input-sm" placeHolder="Sənəd nömrə" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Sənədin verilmə tarixi:
											<div class="input-group">
                                                <asp:TextBox ID="txtDateOfIssue" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Sənədin bitmə tarixi:
											<div class="input-group">
                                                <asp:TextBox ID="txtDateOfExpiry" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                Doğum tarixi:
								<div class="input-group">
                                    <asp:TextBox ID="txtDateOfBirth" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                Vəsiqəni verən orqan:
                                <asp:TextBox ID="txtAuthority" class="form-control input-sm" placeHolder="Vəsiqəni verən orqan" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                Cinsi:
							    <asp:DropDownList ID="drlCustomerGender" class="form-control input-sm" runat="server">
                                    <asp:ListItem Value="" Selected="True">Cinsi</asp:ListItem>
                                    <asp:ListItem Value="M">Kişi</asp:ListItem>
                                    <asp:ListItem Value="F">Qadın</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                Ölkə:
								<asp:DropDownList ID="drlCountry" class="form-control input-sm" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="form-group">
                                Şəhər:
								<asp:DropDownList ID="drlCity" class="form-control input-sm" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                Yaşadığı ünvan:
                                <asp:TextBox ID="txtLegalAddress" class="form-control input-sm" placeHolder="Yaşadığı ünvan" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                Qeydiyyat ünvanı:
                                <asp:TextBox ID="txtRegistrationAddress" class="form-control input-sm" placeHolder="Qeydiyyat ünvanı" runat="server"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Mobil nömrə 1:
                                           <asp:TextBox ID="txtMobilePhone1" class="form-control input-sm" placeHolder="Mobil nömrə 1" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Mobil nömrə 2:
                                            <asp:TextBox ID="txtMobilePhone2" class="form-control input-sm" placeHolder="Mobil nömrə 2" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Mobil nömrə 3:
                                            <asp:TextBox ID="txtMobilePhone3" class="form-control input-sm" placeHolder="Mobil nömrə 3" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Ev telefonu:
                                           <asp:TextBox ID="txtHomePhone" class="form-control input-sm" placeHolder="Ev telefonu" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                Email:
                               <asp:TextBox ID="txtEmail" class="form-control input-sm" placeHolder="Email" runat="server"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                Qeyd:
                               <asp:TextBox ID="txtNote" class="form-control input-sm" placeHolder="Qeyd" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="mb30"></div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" style="text-align: right">
                                <img id="update_customer_data" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnUpdateCustomer" class="btn btn-primary" runat="server" Text="Təsdiq et" OnClick="btnUpdateCustomer_Click"
                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('update_customer_data').style.display = '';" />
                                <asp:Button ID="btnUpdateCustomerCancel" class="btn btn-default" runat="server" Text="İmtina" OnClick="btnUpdateCustomerCancel_Click" />
                            </div>
                        </div>
                    </div>

                </asp:View>
            </asp:MultiView>
        </div>


        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalAlert" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalAlertLabel">
                                    <asp:Literal ID="ltrAlertModalHeaderLabel" runat="server"></asp:Literal>
                            </div>
                            <div class="modal-body">
                                <p>
                                    <asp:Literal ID="ltrAlertModalBodyLabel" runat="server"></asp:Literal>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="delete_customer_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnDeleteCustomer" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClick="btnDeleteCustomer_Click"
                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('delete_customer_loading').style.display = '';
                                                           document.getElementById('btnDeleteCustomerCancel').style.display = 'none';" />
                                <button id="btnDeleteCustomerCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDeleteCustomer" />
            </Triggers>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal bounceIn" id="modalSearch" data-backdrop="static">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalSearchLabel">
                                Müştəri axtarışı
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtSearchFullname" class="form-control" placeHolder="Ad" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtSearchIdCardNumber" class="form-control" placeHolder="Sənəd nömrəsi" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtSearchPin" class="form-control" placeHolder="Fin" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <img id="search_customer_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnSearchCustomer" class="btn btn-primary" runat="server" Text="Axtarış"
                                    OnClick ="btnSearchCustomer_Click"
                                    OnClientClick="this.style.display = 'none';
                                                  document.getElementById('search_customer_loading').style.display = '';" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSearchCustomer" />
            </Triggers>
        </asp:UpdatePanel>


    </div>

    <script>
        function openAlertModal() {
            $('#modalAlert').modal({ show: true });
        }

        function openSearchModal() {
            $('#modalSearch').modal({ show: true });
        }
    </script>

           <style>
        .input-group-addon {
            padding: 8px 10px !important;
        }

       .form-group  {
            margin-bottom: 10px;
        }  
        </style>

</asp:Content>
