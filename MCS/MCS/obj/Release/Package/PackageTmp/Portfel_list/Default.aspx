﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Portfel_list.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary-full" style="margin-bottom: 0;">
        <div class="panel-body" style="padding: 5px; min-height: 38px">
            <div style="float: left; width: 90%">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtEndDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList ID="drlCurrency" class="form-control input-sm" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList ID="drlBranch" class="form-control input-sm" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div style="float: right; text-align: right">
                <div class="row">
                    <div class="col-sm-12">
                        <img id="search_tran_data" style="display: none" src="../img/loader1.gif" />
                        <asp:Button ID="btnSearchTran" CssClass="btn btn-default btn-quirk btn-sm" Style="margin: 0" runat="server" Text="YENİLƏ"
                            OnClick ="btnSearchTran_Click"
                            OnClientClick="this.style.display = 'none';
                                document.getElementById('search_tran_data').style.display = '';" />
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>


    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">

                <div class="mb20"></div>
                <div class="table-responsive">
                    <asp:GridView class="table table-striped nomargin table_custom_border_top_gray"
                        ID="grdPortfolioBalance" runat="server" Style="background-color: #ffffff !important;"
                        DataKeyNames="BranchID"
                        AutoGenerateColumns="False" GridLines="None" ShowFooter="True">
                        <Columns>
                            <asp:TemplateField HeaderText="FİLİAL">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdBranchName" runat="server" Text='<%#Eval("BranchName") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>CƏMİ</b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="VALYUTA">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdCurrencyName" runat="server" Text='<%#Eval("CurrencyName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="200px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="GÜNÜN ƏVVƏLİNƏ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdBeginBalance" runat="server" Text='<%#Eval("BegBalance") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdBeginBalanceSum" runat="server" Text=""></asp:Label></b>
                                </FooterTemplate>

                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="220px" ForeColor="Black" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="GÜNÜN SONUNA">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdEndBalance" runat="server" Text='<%#Eval("EndBalance") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdEndBalanceSum" runat="server" Text=""></asp:Label></b>
                                </FooterTemplate>

                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="220px" ForeColor="Black" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <i>Məlumat yoxdur...</i>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#D1D1D1" BorderColor="#520000" Font-Bold="True" ForeColor="Black" />
                        <HeaderStyle CssClass="bg-dark" />
                    </asp:GridView>
                </div>


                <div class="mb40"></div>
                <div class="table-responsive">
                    <asp:GridView ID="grdTransactions" class="table table-bordered table-hover table-primary table-striped nomargin"
                        runat="server" AutoGenerateColumns="False" DataKeyNames="CurrencyName" GridLines="None" ShowFooter="True">
                        <Columns>
                            <asp:TemplateField HeaderText="TARİX/FİLİAL">
                                <ItemTemplate>
                                     <%#Eval("TranDate") %><br/>
                                    <%#Eval("BranchName") %>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>CƏMİ </b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="200px" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="TƏSVİR">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdTransactionsAddlText" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="VALYUTA">
                                <ItemTemplate>
                                  <%#Eval("CurrencyName") %>
                                </ItemTemplate>
                                 <FooterTemplate>
                                      <b> <asp:Label ID="lblGrdTransactionsCurrencySum" runat="server"></asp:Label></b>
                                     </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="200px" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="MƏDAXİL">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdTransactionsDT_Amount" runat="server" Text='<%#Eval("DT") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdTransactionsDT_AmountSum" runat="server"></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="200px" />
                                <HeaderStyle />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="MƏXARİC">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdTransactionsCT_Amount" runat="server" Text='<%#Eval("CT") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b><asp:Label ID="lblGrdTransactionsCT_AmountSum" runat="server"></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="200px" />
                                <HeaderStyle />
                            </asp:TemplateField>                         
                        </Columns>
                        <FooterStyle BackColor="#89B7D8" ForeColor="#070712" />
                        <HeaderStyle CssClass="bg-blue" />
                    </asp:GridView>

                </div>

            </div>
        </div>
        <style>
            .input-sm {
                height: 26px !important;
                line-height: 26px !important;
            }


            .input-group-addon {
                padding: 5px 7px !important;
            }

            .form-group {
                margin-bottom: 10px;
            }

            .no_visible_font {
                color: #ffffff;
            }
        </style>
    </div>

</asp:Content>
