﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Control_loans.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary-full" style="margin-bottom: 0;">
        <div class="panel-body" style="padding: 5px; min-height: 38px">
            <div style="float: left; width: 90%">
                <div class="row">
                    <div class="col-sm-2">
                        <asp:DropDownList ID="drlBranch" AutoPostBack="true" class="form-control input-sm" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <asp:TextBox ID="txtFullname" autocomplete="off" class="form-control input-sm" placeHolder="S.A.A" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-2">
                        <asp:TextBox ID="txtCollateralNumber" autocomplete="off" class="form-control input-sm" placeHolder="Girov nömrəsi" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-1">
                        <asp:DropDownList ID="drlCurrency" AutoPostBack="true" class="form-control input-sm" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtEndDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>

                </div>
            </div>
            <div style="float: right; text-align: right">
                <div class="row">
                    <div class="col-sm-12">
                        <img id="search_loan_data" style="display: none" src="../img/loader1.gif" />
                        <asp:Button ID="btnSearchLoan" CssClass="btn btn-default btn-quirk btn-sm"
                            OnClick ="btnSearchLoan_Click"
                            Style="margin: 0" runat="server" Text="YENİLƏ"
                            OnClientClick="this.style.display = 'none';
                            document.getElementById('search_loan_data').style.display = '';" />
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>


    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <ul class="list-inline" style="padding-left: 5px">
                    <li style="float: left">
                        <span class="label label-black">Məlumat sayı:
                            <asp:Literal ID="ltrDataCount" runat="server"></asp:Literal>
                        </span>
                    </li>
                    <li style="float: right">
                        <asp:LinkButton ID="lnkDownloadResult" runat="server" OnClick ="lnkDownloadResult_Click">
                        <i class="glyphicon glyphicon-print"></i><span> Çap</span>
                        </asp:LinkButton>
                    </li>
                    <li style="clear: both"></li>
                </ul>
                <div class="mb20"></div>
                <div class="table-responsive">
                    <asp:GridView class="table table-striped nomargin table_custom_border_top_gray"
                        ID="grdLoanData" runat="server" Style="background-color: #ffffff !important;"
                        DataKeyNames="LoanID"
                        AutoGenerateColumns="False" GridLines="None" ShowFooter="True">
                        <Columns>
                            <asp:TemplateField HeaderText="KS">
                                <ItemTemplate>
                                    <%#Eval("LoanCount")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="50px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FİLİAL">
                                <ItemTemplate>
                                    <%#Eval("BranchName")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="120px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="TƏSVİR">
                                <ItemTemplate>
                                    <%#Eval("Description")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="250px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="SƏNƏD №">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hrlCollateralNumber"
                                        Target="_blank"
                                        NavigateUrl='<%# string.Format("~/CreditDetails/?LoanID={0}", Eval("LoanID")) %>'
                                        runat="server"><%# Eval("CollateralNumber") %></asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ƏSAS<br/>FAİZ<br/>DƏBBƏ">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdTotalPrincipalDebt" runat="server" Text='<%#Eval("TotalPrincipalDebt") %>'></asp:Label><br />
                                    <asp:Label ID="lblGrdTotalInterestDebt" runat="server" Text='<%#Eval("TotalInterestDebt") %>'></asp:Label><br />
                                    <asp:Label ID="lblGrdPenaltyDebt" runat="server" Text='<%#Eval("PenaltyDebt")%>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdTotalPrincipalDebtSum" runat="server" Text=""></asp:Label><br />
                                        <asp:Label ID="lblGrdTotalInterestDebtSum" runat="server" Text=""></asp:Label><br />
                                        <asp:Label ID="lblGrdPenaltyDebtSum" runat="server" Text=""></asp:Label>
                                    </b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="CƏM BORC">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdTotalDebt" runat="server" Text='<%#Eval("TotalDebt") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdTotalDebtSum" runat="server" Text=""></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ÖDƏMƏ TARİXİ">
                                <ItemTemplate>
                                    <%#Eval("PaymentDate")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="GECİKMƏ GÜNÜ">
                                <ItemTemplate>
                                    <%#Eval("DaysOfRespite")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="MOBİL">
                                <ItemTemplate>
                                    <%#Eval("MobilePhone1")%><br />
                                    <%#Eval("MobilePhone2")%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <ul class="list-inline">
                                        <li>
                                            <i class="glyphicon glyphicon-check"></i>
                                            <asp:LinkButton ID="lnkLoanAktivModal" CommandArgument='<%# Eval("LoanID") %>'
                                                OnClick ="lnkLoanAktivModal_Click"
                                                title="Aktiv" runat="server">
                                        Aktiv</asp:LinkButton>
                                        </li>
                                    </ul>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="90px" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <i>Məlumat yoxdur...</i>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#D1D1D1" BorderColor="#520000" Font-Bold="True" ForeColor="Black" />
                        <HeaderStyle CssClass="bg-dark" />
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>


    <div>

        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalCheckLoanSendModal" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalTitleCheckLoanSend">
                                    <i class="glyphicon glyphicon-check"></i>
                                &nbspAktivləşdirmə
                            </div>
                            <div class="modal-body">
                                <p>
                                    Aktivləşdirilsin?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="send_check_loan_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnSendCheckLoan" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClick ="btnSendCheckLoan_Click"
                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('send_check_loan_loading').style.display = '';
                                                           document.getElementById('btnSendCheckLoanCancel').style.display = 'none';" />
                                <button id="btnSendCheckLoanCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSendCheckLoan" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hdnSendLoanID" runat="server" />

        <script>
            function openCheckLoanSendModal() {
                $('#modalCheckLoanSendModal').modal({ show: true });
            }
        </script>

        <style>
            .input-sm {
                height: 26px !important;
                line-height: 26px !important;
            }

            .input-group-addon {
                padding: 5px 7px !important;
            }

            .form-group {
                margin-bottom: 10px;
            }

            .label-black {
                background-color: #343a40;
            }

            .label {
                display: inline;
                padding: 0.6em 1em 0.6em;
                font-size: 80%;
                font-weight: bold !important;
                line-height: 1;
                color: #ffffff;
                text-align: center;
                white-space: nowrap;
                vertical-align: baseline;
                border-radius: .25em;
            }
        </style>
    </div>

</asp:Content>
