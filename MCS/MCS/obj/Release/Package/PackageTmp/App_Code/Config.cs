﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Config
{
    //Açar yaradaq.
    public static string Key(int say)
    {
        Random ran = new Random();
        string Bind = "aqwertyuipasdfghjkzxcvbnmQAZWSXEDCRFVTGBYHNUJMKP23456789";
        string Key = "";
        for (int i = 1; i <= say; i++)
        {
            Key += Bind.Substring(ran.Next(Bind.Length - 1), 1);
        }
        return Key.Trim();
    }

    public static string getValute(int currencyCode)
    {
        string result = "" ;
        switch (currencyCode)
        {
            case 1:
                result = "AZN";
                break;
            case 2:
                result = "USD";
                break;

            default:
                result = "";
                break;
        }
        return result;

    }
}
