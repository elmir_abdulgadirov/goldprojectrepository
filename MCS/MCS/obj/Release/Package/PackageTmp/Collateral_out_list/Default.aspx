﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Collateral_out_list.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary-full" style="margin-bottom: 0;">
        <div class="panel-body" style="padding: 5px; min-height: 38px">
            <div style="float: left; width: 90%">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="input-group">
                            <asp:TextBox ID="txtEndDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <asp:DropDownList ID="drlBranch" AutoPostBack ="true" OnSelectedIndexChanged ="drlBranch_SelectedIndexChanged"  class="form-control input-sm" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-2">
                        <asp:TextBox ID="txtDepositBox" autocomplete="off" class="form-control input-sm" placeHolder="Depozit qutusu" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-2">
                        <asp:TextBox ID="txtCollateralNumber" autocomplete="off" class="form-control input-sm" placeHolder="Girov nömrəsi" runat="server"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div style="float: right; text-align: right">
                <div class="row">
                    <div class="col-sm-12">
                        <img id="search_collateral_data" style="display: none" src="../img/loader1.gif" />
                        <asp:Button ID="btnSearchCollateral" CssClass="btn btn-default btn-quirk btn-sm" Style="margin: 0" runat="server" Text="YENİLƏ"
                            OnClick="btnSearchCollateral_Click"
                            OnClientClick="this.style.display = 'none';
                                document.getElementById('search_collateral_data').style.display = '';" />
                    </div>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <ul class="list-inline" style="padding-left: 5px">
                    <li>
                        <asp:LinkButton ID="lnkDownloadResult" runat="server"
                            OnClick="lnkDownloadResult_Click">
                        <i class="glyphicon glyphicon-print"></i><span> Çap et</span>
                        </asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="lnkSendMailResult" runat="server"
                            OnClick ="lnkSendMailResult_Click">
                        <i class="fa fa-envelope-o"></i><span> Mail göndər</span>
                        </asp:LinkButton>
                    </li>
                </ul>


                <div class="mb20"></div>
                <div class="table-responsive">
                    <asp:GridView class="table table-striped nomargin table_custom_border_top_gray"
                        ID="grdCollateralOutData" runat="server" Style="background-color: #ffffff !important;"
                        DataKeyNames="OutID"
                        AutoGenerateColumns="False" GridLines="None" ShowFooter="True">
                        <Columns>
                            <asp:TemplateField HeaderText="TARİX">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdOutDate" runat="server" Text='<%#Eval("OutDate") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>SAY</b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FİLİAL">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdOutBranchName" runat="server" Text='<%#Eval("BranchName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="S.A.A">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdOutFullName" runat="server" Text='<%#Eval("FullName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="QUTU №">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdOutDepositBox" runat="server" Text='<%#Eval("DepositBox") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdOutDepositBoxFooterCount" runat="server" Text=""></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="GİROV №">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdOutCollateralNumber" runat="server" Text='<%#Eval("CollateralNumber") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblGrdOutCollateralNumberFooterCount" runat="server" Text=""></asp:Label></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="QEYD">
                                <ItemTemplate>
                                    <asp:Label ID="lblGrdOutNote" runat="server" Text='<%#Eval("Note") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            </asp:TemplateField>



                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDeleteCollateralOutModal" CommandArgument='<%# Eval("OutID") %>'
                                        OnClick="lnkDeleteCollateralOutModal_Click"
                                        title="Sil" runat="server">
                                         <i class="fa fa-trash"></i></asp:LinkButton>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <b></b>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="45px" />
                            </asp:TemplateField>



                        </Columns>
                        <EmptyDataTemplate>
                            <i>Məlumat yoxdur...</i>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#D1D1D1" BorderColor="#520000" Font-Bold="True" ForeColor="Black" />
                        <HeaderStyle CssClass="bg-dark" />
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <div>

        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalDeleteCollateralOut" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalTitleDeleteCollateralOut">
                                    <i class="fa fa-trash"></i>
                                &nbspÇıxarışın silinməsi
                            </div>
                            <div class="modal-body">
                                <p>
                                    Çıxarış silinsin?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="delete_collateral_out_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnDeleteCollateralOut" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClick="btnDeleteCollateralOut_Click"
                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('delete_collateral_out_loading').style.display = '';
                                                           document.getElementById('btnDeleteCollateralOutCancel').style.display = 'none';" />
                                <button id="btnDeleteCollateralOutCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDeleteCollateralOut" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hdnCollateralOutID" runat="server" />

        <script>
            function openDeleteCollateralOutModal() {
                $('#modalDeleteCollateralOut').modal({ show: true });
            }
        </script>

        <style>
            .input-sm {
                height: 26px !important;
                line-height: 26px !important;
            }


            .input-group-addon {
                padding: 5px 7px !important;
            }

            .form-group {
                margin-bottom: 10px;
            }

            .no_visible_font {
                color: #ffffff;
            }
        </style>
    </div>
</asp:Content>
