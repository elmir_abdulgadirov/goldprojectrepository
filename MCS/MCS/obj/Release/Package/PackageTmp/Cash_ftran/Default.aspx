﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Cash_ftran.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">XƏZİNƏ ƏMƏLİYYATLARI</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="viewListOperation" runat="server">
                        <!--List operation-->
                        <div class="row">
                            <div class="col-sm-12">
                                <div style="float: left; width: 60%; text-align: left">
                                    <ul class="list-inline">
                                        <li>
                                            <asp:LinkButton ID="lnkOpenModalSelectOperationType" runat="server" OnClick="lnkOpenModalSelectOperationType_Click">
                                          <i class="glyphicon glyphicon-transfer"></i><span> Yeni əməliyyat</span>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                                <div style="float: right; text-align: right; width: 40%">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <asp:TextBox ID="txtStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <asp:TextBox ID="txtEndDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <img id="search_tran_data" style="display: none" src="../img/loader1.gif" />
                                            <asp:Button ID="btnSearchTran" CssClass="btn btn-default btn-quirk btn-sm"
                                                Style="margin: 0; width: 100%" runat="server" Text="YENİLƏ"
                                                OnClientClick="this.style.display = 'none';
                                            document.getElementById('search_tran_data').style.display = '';"
                                                OnClick="btnSearchTran_Click" />
                                        </div>
                                    </div>
                                </div>
                                <div style="clear: both"></div>
                            </div>
                        </div>

                        <div class="mb40"></div>
                        <div class="mb40"></div>
                        <div class="table-responsive">
                            <asp:GridView ID="grdTransactions" class="table table-bordered table-primary table-striped nomargin" runat="server"
                                AutoGenerateColumns="False" GridLines="None">
                                <Columns>
                                    <asp:BoundField DataField="valueDate" ItemStyle-Width="100" HeaderText="TARİX" />
                                    <asp:BoundField DataField="BranchName" ItemStyle-Width="150" HeaderText="FİLİAL" />
                                    <asp:BoundField DataField="TranType" HeaderText="TİP" ItemStyle-Width="100" />
                                    <asp:TemplateField HeaderText="TƏSVİR">
                                        <ItemTemplate>
                                            <%#Eval("Addl_Text")%>
                                            <br />
                                              <%#Eval("Account")%>
                                            <br />
                                            <b><%#Eval("Assignment")%></b>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CurrencyName" ItemStyle-Width="100" HeaderText="VALYUTA" />
                                    <asp:BoundField DataField="DT_Amount" ItemStyle-Width="100" HeaderText="MƏDAXİL" />
                                    <asp:BoundField DataField="CT_Amount" ItemStyle-Width="100" HeaderText="MƏXARİC" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDeleteTranModal"
                                                CommandArgument='<%#Eval("MsgID")%>'
                                                OnClick="lnkDeleteTranModal_Click"
                                                ToolTip="Sil" runat="server">
                                              <span class ="glyphicon glyphicon-trash"></span>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="70px" />
                                        <HeaderStyle />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>Əməliyyat tapılmadı...</EmptyDataTemplate>
                                <HeaderStyle CssClass="bg-blue" />
                            </asp:GridView>
                            <asp:HiddenField ID="hdnSelectedTranMsgID" runat="server" />
                        </div>




                    </asp:View>
                    <asp:View ID="viewMedaxil" runat="server">
                        <!-- medaxil-->
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs nav-line">
                                    <li style="background-color: #2599ab; height: 27px; padding-top: 5px">
                                        <asp:LinkButton ID="lnkBackToMainView" ToolTip="Geri" runat="server"
                                            OnClick="lnkBackToMainView_Click"><img src="../img/back2.png"/></asp:LinkButton></li>
                                    <li class="active"><a><strong>MƏDAXİL</strong></a></li>
                                </ul>
                                <div class="tab-content mb20">
                                    <div class="tab-pane active">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            Xəzinə hesabı:
                                                           <asp:DropDownList ID="drlXezineMedaxilAccount"
                                                               AutoPostBack="true"
                                                               class="form-control" runat="server">
                                                           </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            Hesab qrupu:
                                                           <asp:DropDownList ID="drlMedaxilAccountGroup"
                                                               AutoPostBack="true" OnSelectedIndexChanged="drlMedaxilAccountGroup_SelectedIndexChanged"
                                                               class="form-control" runat="server">
                                                           </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            Hesab alt qrupu:
                                                            <asp:DropDownList ID="drlMedaxilAccountSubGroup"
                                                                class="form-control"
                                                                AutoPostBack="true" OnSelectedIndexChanged="drlMedaxilAccountSubGroup_SelectedIndexChanged"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            Hesab adı:
                                                            <asp:DropDownList ID="drlMedaxilAccountSubSubGroup"
                                                                AutoPostBack="true"
                                                                OnSelectedIndexChanged="drlMedaxilAccountSubSubGroup_SelectedIndexChanged"
                                                                class="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            Hesab:
                                                            <asp:DropDownList ID="drlMedaxilAccount" class="form-control" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            Əməliyyat tarixi:
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtMedaxilOperationDate" autocomplete="off" class="form-control datepicker" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            Təhvil verən:
                                                            <asp:TextBox ID="txtMedaxilResponsibilityPerson" type="text" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="form-group">
                                                            Təyinat:
                                                            <asp:DropDownList ID="drlMedaxilNarrative" class="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            Məbləğ:
                                                        <asp:TextBox ID="txtMedaxilAmount" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="form-group">
                                                            Qeyd:
                                                          <asp:TextBox ID="txtMedaxilNote" Rows="2" TextMode="MultiLine" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="form-group" style="text-align: right">
                                                            <img id="create_medaxil_transaction_loading" style="display: none" src="../img/loader1.gif" />
                                                            <asp:Button ID="btnCreateMedaxil"
                                                                OnClick="btnCreateMedaxil_Click"
                                                                class="btn btn-primary" runat="server" Text="Təsdiq et"
                                                                OnClientClick="this.style.display = 'none';
                                                                document.getElementById('create_medaxil_transaction_loading').style.display = '';" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </asp:View>
                    <asp:View ID="viewMexaric" runat="server">
                        <!-- mexaric-->
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs nav-line nav-line-danger">
                                    <li style="background-color: #d9534f; height: 27px; padding-top: 5px">
                                        <asp:LinkButton ID="lnkBackToMainView_1" ToolTip="Geri" runat="server"
                                            OnClick="lnkBackToMainView_1_Click"><img src="../img/back2.png"/></asp:LinkButton></li>
                                    <li class="active"><a><strong>MƏXARİC</strong></a></li>
                                </ul>
                                <div class="tab-content mb20">
                                    <div class="tab-pane active">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            Xəzinə hesabı:
                                                           <asp:DropDownList ID="drlXezineMexaricAccount"
                                                               AutoPostBack="true"
                                                               class="form-control" runat="server">
                                                           </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            Hesab qrupu:
                                                           <asp:DropDownList ID="drlMexaricAccountGroup"
                                                               AutoPostBack="true"
                                                               OnSelectedIndexChanged="drlMexaricAccountGroup_SelectedIndexChanged"
                                                               class="form-control" runat="server">
                                                           </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            Hesab alt qrupu:
                                                            <asp:DropDownList ID="drlMexaricAccountSubGroup"
                                                                AutoPostBack="true"
                                                                OnSelectedIndexChanged="drlMexaricAccountSubGroup_SelectedIndexChanged"
                                                                class="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            Hesab adı:
                                                            <asp:DropDownList ID="drlMexaricAccountSubSubGroup" class="form-control"
                                                                AutoPostBack="true"
                                                                OnSelectedIndexChanged="drlMexaricAccountSubSubGroup_SelectedIndexChanged"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            Hesab:
                                                            <asp:DropDownList ID="drlMexaricAccount" class="form-control" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            Əməliyyat tarixi:
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtMexaricOperationDate" autocomplete="off" class="form-control datepicker" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            Təhvil alan:
                                                            <asp:TextBox ID="txtMexaricResponsibilityPerson" type="text" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="form-group">
                                                            Təyinat:
                                                            <asp:DropDownList ID="drlMexaricNarrative" class="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group">
                                                            Məbləğ:
                                                        <asp:TextBox ID="txtMexaricAmount" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="form-group">
                                                            Qeyd:
                                                          <asp:TextBox ID="txtMexaricNote" TextMode="MultiLine" Rows="2" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="form-group" style="text-align: right">
                                                            <img id="create_mexaric_transaction_loading" style="display: none" src="../img/loader1.gif" />
                                                            <asp:Button ID="btnCreateMexaric"
                                                                OnClick="btnCreateMexaric_Click"
                                                                class="btn btn-primary" runat="server" Text="Təsdiq et"
                                                                OnClientClick="this.style.display = 'none';
                                                                document.getElementById('create_mexaric_transaction_loading').style.display = '';" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>



        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  select oper-->
                <div class="modal bounceIn animated in" id="modalOperationType" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">
                            </div>
                            <div class="modal-body" style="height: 70px">
                                <div class="rows">
                                    <div class="col-sm-6">
                                        <asp:LinkButton ID="lnkMedaxilForm" CssClass="btn btn-success btn-quirk btn-stroke" runat="server"
                                            OnClick="lnkMedaxilForm_Click">
                                             <span class="wi wi-up"></span>&nbsp MƏDAXİL
                                        </asp:LinkButton>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:LinkButton ID="lnkMexaricForm" CssClass="btn btn-danger btn-quirk btn-stroke" runat="server"
                                            OnClick="lnkMexaricForm_Click">
                                            <span class="wi wi-down"></span>&nbsp MƏXARİC
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lnkMedaxilForm" />
                <asp:PostBackTrigger ControlID="lnkMexaricForm" />
            </Triggers>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalDeleteTranAlert" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalDeleteTranAlertLabel">
                                    <i class="fa fa-trash"></i>&nbsp;Əməliyyatın silinməsi
                                </h4>
                            </div>
                            <div class="modal-body">
                                <p>
                                    Əməliyyat silinsin?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="delete_transaction_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnDeleteTransaction" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClick="btnDeleteTransaction_Click"
                                    OnClientClick="this.style.display = 'none';
                                                   document.getElementById('delete_transaction_loading').style.display = '';
                                                   document.getElementById('btnDeleteTransactionCancel').style.display = 'none';" />
                                <button id="btnDeleteTransactionCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDeleteTransaction" />
            </Triggers>
        </asp:UpdatePanel>
    </div>


    <script>
        function openOperationTypeModal() {
            $('#modalOperationType').modal({ show: true });
        }

        function openDeleteTransactionModal() {
            $('#modalDeleteTranAlert').modal({ show: true });
        }

    </script>


    <style>
        .input-sm {
            height: 26px !important;
            line-height: 26px !important;
        }

        .input-group-addon {
            padding: 5px 7px !important;
        }

        .form-group {
            margin-bottom: 10px;
        }

        .no_visible_font {
            color: #ffffff;
        }

        .nav-line-danger > li.active > a,
        .nav-line-danger > li.active > a:hover,
        .nav-line-danger > li.active > a:focus {
            color: #d9534f !important;
            background-color: transparent;
            -webkit-box-shadow: 0 1px 0 #d9534f !important;
            box-shadow: 0 1px 0 #d9534f !important;
        }
    </style>

</asp:Content>
