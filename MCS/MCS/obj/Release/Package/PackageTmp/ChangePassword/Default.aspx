﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.ChangePassword.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                Şifrə:
                <asp:TextBox ID="txtPassword" type="text" placeholder="Şifrə" runat="server" TextMode="Password" class="form-control" MaxLength="40"></asp:TextBox>
            </div>
            <div class="form-group">
                Şifrə təkrar:
                <asp:TextBox ID="txtPasswordConfirm" type="text" placeholder="Şifrə təkrar" runat="server" TextMode="Password" class="form-control" MaxLength="40"></asp:TextBox>
            </div>
            <div class ="form-group">
                  <img id="change_password_loading" style="display: none" src="../img/loader1.gif" />
                  <asp:Button ID="btnChangePassword" class="btn btn-primary" runat="server" Text="Şifrəni dəyiş" OnClick ="btnChangePassword_Click"
                                        OnClientClick="this.style.display = 'none';
                                                       document.getElementById('change_password_loading').style.display = '';"/>
            </div>
        </div>
    </div>
</asp:Content>
