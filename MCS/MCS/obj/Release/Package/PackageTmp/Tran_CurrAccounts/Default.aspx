﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.Tran_CurrAccounts.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">CARİ HESAB ƏMƏLİYYATLARI</h3>
        </div>
        <div class="panel-body">
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <ul class="list-inline">
                        <li>
                            <asp:LinkButton ID="lnkOpenModalSearchcURRaCNTn" runat="server" OnClick="lnkOpenModalSearchcURRaCNTn_Click">
                                <i class="fa fa-search"></i><span> Axtarış</span>
                            </asp:LinkButton>
                        </li>
                    </ul>
                    <div class="mb40"></div>
                    <div class="mb40"></div>
                    <div class="table-responsive">
                        <asp:GridView ID="grdCustCurrAcnt" class="table table-bordered table-primary table-striped nomargin" runat="server"
                            AutoGenerateColumns="False" DataKeyNames="ACNT_ID,BRANCH_ID,ACNT_CURRENCY,CurrencyName,ACNT_CODE,ACNT_NAME" GridLines="None">
                            <Columns>
                                <asp:BoundField DataField="BranchName" HeaderText="FİLİAL" />
                                <asp:BoundField DataField="ACNT_CODE" HeaderText="HESAB №" />
                                <asp:BoundField DataField="CurrencyName" HeaderText="VALYUTA" />
                                <asp:BoundField DataField="ACNT_NAME" HeaderText="HESAB ADI" />
                                <asp:BoundField DataField="CURRENT_BALANCE" HeaderText="BALANS" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPlusAccountModal" runat="server" OnClick="lnkPlusAccountModal_Click">
                                            <i class="fa fa-plus text-success"></i> <p class="text-success">Hesaba mədaxil</p>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="140px" />
                                    <HeaderStyle />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkMinusAccountModal" runat="server" OnClick ="lnkMinusAccountModal_Click">
                                            <i class="fa fa-minus text-danger"></i><p class="text-danger">Hesabdan məxaric</p> 
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="150px" />
                                    <HeaderStyle />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>Hesab taılmadı...</EmptyDataTemplate>
                            <HeaderStyle CssClass="bg-blue" />
                        </asp:GridView>
                        <asp:HiddenField ID="hdnSelectedCurrencyId" runat="server" />
                        <asp:HiddenField ID="hdnSelectedCurrencyName" runat="server" />
                        <asp:HiddenField ID="hdnSelectedAccountName" runat="server" />
                    </div>
                </asp:View>
                <asp:View ID="ViewMedaxil" runat="server">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs nav-line">
                                <li style="background-color: #2599ab; height: 27px; padding-top: 5px">
                                    <asp:LinkButton ID="lnkBackToMainView" ToolTip="Geri" runat="server" OnClick="lnkBackToMainView_Click"><img src="../img/back2.png"/></asp:LinkButton></li>
                                <li class="active"><a><strong>CARİ HESABA MƏDAXİL</strong></a></li>
                            </ul>
                            <div class="tab-content mb20">
                                <div class="tab-pane active">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <div class="mb40"></div>
                                                    <div class="form-group">
                                                        Debet hesab:
                                                          <asp:DropDownList ID="drl_Plus_DebetAccount" class="form-control" runat="server"></asp:DropDownList>
                                                    </div>
                                                    <div class="form-group">
                                                        Kredit hesab:
                                                          <asp:DropDownList ID="drl_Plus_CreditAccount" disabled class="form-control" runat="server"></asp:DropDownList>
                                                    </div>
                                                    <div class="form-group">
                                                        Məbləğ:
                                                        <asp:TextBox ID="txt_Plus_Amount" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        Valyuta:
                                                        <asp:TextBox ID="txt_Plus_Valuta" disabled type="text" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        Ödənişin təyinatı:
                                                        <asp:TextBox ID="txt_Plus_Description" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group" style="text-align: right">
                                                        <img id="create_transaction_loading" style="display: none" src="../img/loader1.gif" />
                                                        <asp:Button ID="btnCreateMedaxil" class="btn btn-primary" runat="server" Text="Təsdiq et"
                                                            OnClick="btnCreateMedaxil_Click"
                                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('create_transaction_loading').style.display = '';" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="ViewMexaric" runat="server">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs nav-line nav-line-danger">
                                <li style="background-color: #d9534f; height: 27px; padding-top: 5px">
                                    <asp:LinkButton ID="lnkBackToMainView_1" ToolTip="Geri" runat="server" OnClick="lnkBackToMainView_Click"><img src="../img/back2.png"/></asp:LinkButton></li>
                                <li class="active"><a><strong>CARİ HESABDAN MƏXARİC</strong></a></li>
                            </ul>
                            <div class="tab-content mb20">
                                <div class="tab-pane active">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <div class="mb40"></div>
                                                    <div class="form-group">
                                                        Debet hesab:
                                                          <asp:DropDownList ID="drl_Minus_DebetAccount" disabled class="form-control" runat="server"></asp:DropDownList>
                                                    </div>
                                                    <div class="form-group">
                                                        Kredit hesab:
                                                          <asp:DropDownList ID="drl_Minus_CreditAccount" class="form-control" runat="server"></asp:DropDownList>
                                                    </div>
                                                    <div class="form-group">
                                                        Məbləğ:
                                                        <asp:TextBox ID="txt_Minus_Amount" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        Valyuta:
                                                        <asp:TextBox ID="txt_Minus_Valuta" disabled type="text" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group">
                                                        Ödənişin təyinatı:
                                                        <asp:TextBox ID="txt_Minus_Description" autocomplete="off" type="text" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                    <div class="form-group" style="text-align: right">
                                                        <img id="create_transaction_mexaric_loading" style="display: none" src="../img/loader1.gif" />
                                                        <asp:Button ID="btnCreateMexaric" class="btn btn-primary" runat="server" Text="Təsdiq et"
                                                            OnClick ="btnCreateMexaric_Click"
                                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('create_transaction_mexaric_loading').style.display = '';" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal bounceIn" id="modalAccountSearch" data-backdrop="static">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalAccountSearchLabel">
                                Hesab axtarışı
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtSearchAcntCustomerName" autocomplete="off" class="form-control" placeHolder="Müştəri adı" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtSearchCollateralNumber" autocomplete="off" class="form-control" placeHolder="Girov bileti №" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <img id="search_acc_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnSearchCustomerAcnt" class="btn btn-primary" runat="server" Text="Axtarış"
                                    OnClick="btnSearchCustomerAcnt_Click"
                                    OnClientClick="this.style.display = 'none';
                                                  document.getElementById('search_acc_loading').style.display = '';" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSearchCustomerAcnt" />
            </Triggers>
        </asp:UpdatePanel>



    </div>
    <style>
        .nav-line-danger > li.active > a,
        .nav-line-danger > li.active > a:hover,
        .nav-line-danger > li.active > a:focus {
          color: #d9534f !important;
          background-color: transparent;
          -webkit-box-shadow: 0 1px 0 #d9534f !important;;
          box-shadow: 0 1px 0 #d9534f !important;;
        }
    </style>

    <script>
        function openAccountSearchModal() {
            $('#modalAccountSearch').modal({ show: true });
        }
    </script>

</asp:Content>
