﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" ClientIDMode="AutoID" CodeBehind="Default.aspx.cs" Inherits="MCS.NewCredit.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="panel panel-primary-full" style="margin-bottom: 0;">
        <div class="panel-body" style="padding: 5px; text-align: right; min-height: 38px">
            <img id="accept_loan_data" style="display: none" src="../img/loader1.gif" />
            <asp:Button ID="btnAcceptLoan" CssClass="btn btn-default btn-quirk btn-sm" Style="margin: 0" runat="server" Text="TƏSDİQ ET"
                OnClick="btnAcceptLoan_Click"
                OnClientClick="this.style.display = 'none';
                                document.getElementById('accept_loan_data').style.display = '';" />
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">

                    <div class="row">
                        <div class="col-sm-12">
                            Müştəri <span class="text-danger"></span>
                            <div class="input-group">
                                <asp:DropDownList ID="drlCustomer" class="form-control input-sm" runat="server">
                                </asp:DropDownList>
                                <span class="input-group-addon">
                                    <asp:LinkButton ID="linkCustomerSearchModalOpen" OnClick="linkCustomerSearchModalOpen_Click" runat="server" ToolTip="Müştəri axtarışı">
                                    <i class="glyphicon glyphicon-search"></i>
                                    </asp:LinkButton>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="mb5"></div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                Girov №<span class="text-danger"></span>
                                <asp:TextBox ID="txtColleteralNo" autocomplete="off" class="form-control input-sm" oninput="txtCAC_Copy()" placeHolder="Girov №" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                Müqavilə №<span class="text-danger"></span>
                                <asp:TextBox ID="txtContractNo" autocomplete="off" class="form-control input-sm" placeHolder="Müqavilə №" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="mb5"></div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                AC №<span class="text-danger"></span>
                                <asp:TextBox ID="txtACNo" autocomplete="off" class="form-control input-sm" placeHolder="Ac №" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        Məbləğ<span class="text-danger"></span>
                                        <asp:TextBox ID="txtAmount" oninput="calculateFeeAmount()" autocomplete="off" class="form-control input-sm" placeHolder="Məbləğ" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        Faiz(%)<span class="text-danger"></span>
                                        <asp:TextBox ID="txtInterest" autocomplete="off" class="form-control input-sm" placeHolder="Faiz(%)" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="mb5"></div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                İlk tarix
                                    <div class="input-group">
                                        <asp:TextBox ID="txtStartDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        Güzəşt (ay)<span class="text-danger"></span>
                                        <asp:TextBox ID="txtDiscountMonth" Text="0" autocomplete="off" class="form-control input-sm" placeHolder="Güzəşt (ay)" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        Müddət (ay)<span class="text-danger"></span>
                                        <asp:TextBox ID="txtLoanMonths" Text="0" autocomplete="off" class="form-control input-sm" placeHolder="Müddət (ay)" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="mb5"></div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                Ödəmə tarixi
                                    <div class="input-group">
                                        <asp:TextBox ID="txtPaymentDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                Hesab növü<span class="text-danger"></span>
                                <asp:DropDownList ID="drlAccountTypesList" class="form-control input-sm" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="mb5"></div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                Son tarix
                                    <div class="input-group">
                                        <asp:TextBox ID="txtEndDate" autocomplete="off" class="form-control datepicker input-sm" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                Girov növü<span class="text-danger"></span>
                                <asp:DropDownList ID="drlLCollateralTypes" class="form-control input-sm" runat="server">
                                    <asp:ListItem Value="" Selected="True">Seçin</asp:ListItem>
                                    <asp:ListItem>Girovlu</asp:ListItem>
                                    <asp:ListItem>Girovsuz</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="mb5"></div>
                </div>

                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                Filial<span class="text-danger"></span>
                                <asp:DropDownList ID="drlBranch" class="form-control input-sm" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>


                        <div class="col-sm-4">
                            <div class="form-group">
                                Ay<span class="text-danger"></span>
                                <asp:TextBox ID="txtMonth" ReadOnly Text="0" autocomplete="off" class="form-control input-sm" placeHolder="Ay" runat="server"></asp:TextBox>
                            </div>
                        </div>


                    </div>
                    <div class="mb5"></div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                Təyinat<span class="text-danger"></span>
                                <asp:DropDownList ID="drlLoanTypeList" class="form-control input-sm" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                Gün<span class="text-danger"></span>
                                <asp:TextBox ID="txtDay" ReadOnly Text="0" autocomplete="off" class="form-control input-sm" placeHolder="Gün" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="mb5"></div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                Komissiya<span class="text-danger"></span>
                                <asp:TextBox ID="txtFeeAmount" autocomplete="off" class="form-control input-sm" placeHolder="Komissiya" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                Gecikmə<span class="text-danger"></span>
                                <asp:TextBox ID="txtDayOfDelay" ReadOnly Text="0" class="form-control input-sm" placeHolder="Gecikmə" runat="server"></asp:TextBox>
                            </div>
                        </div>

                    </div>
                    <div class="mb5"></div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                Status<span class="text-danger"></span>
                                <asp:DropDownList ID="drlLoanStatus" class="form-control input-sm" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                Aylıq faiz<span class="text-danger"></span>
                                <asp:TextBox ID="txtMonthInterest" ReadOnly Text="0" autocomplete="off" class="form-control input-sm" placeHolder="Aylıq faiz" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="mb5"></div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                Valyuta<span class="text-danger"></span>
                                <asp:DropDownList ID="drlCurrency" class="form-control input-sm" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                Aylıq ödəmə<span class="text-danger"></span>
                                <asp:TextBox ID="txtMonthPayment" ReadOnly Text="0" autocomplete="off" class="form-control input-sm" placeHolder="Aylıq ödəmə" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="mb5"></div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                Qrafik<span class="text-danger"></span>
                                <asp:DropDownList ID="drlQrafikType" class="form-control input-sm" runat="server">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-4">
                        </div>
                    </div>
                    <div class="mb5"></div>
                </div>
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                Qalıq məbləğ<span class="text-danger"></span>
                                <asp:TextBox ID="txtRestAmount" ReadOnly Text="0" autocomplete="off" class="form-control input-sm" placeHolder="Qalıq məbləğ" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                Faiz məbləğ<span class="text-danger"></span>
                                <asp:TextBox ID="txtInterestAmount" ReadOnly Text="0" autocomplete="off" class="form-control input-sm" placeHolder="Faiz məbləğ" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="mb5"></div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                Penya<span class="text-danger"></span>
                                <asp:TextBox ID="txtPenya" ReadOnly Text="0" autocomplete="off" class="form-control input-sm" placeHolder="Penya" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                Borc / Avans<span class="text-danger"></span>
                                <asp:TextBox ReadOnly ID="txtDebtAvans" Text="0" autocomplete="off" class="form-control input-sm" placeHolder="Borc / Avans" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="mb5"></div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                Cari borc<span class="text-danger"></span>
                                <asp:TextBox ID="txtCurrentDebt" ReadOnly Text="0" autocomplete="off" class="form-control input-sm" placeHolder="Cari borc" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                Сəm məbləğ<span class="text-danger"></span>
                                <asp:TextBox ID="txtSumAmount" ReadOnly Text="0" autocomplete="off" class="form-control input-sm" placeHolder="Сəm məbləğ" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-sm-2">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <span class="no_visible_font">Yadda saxla</span>
                                <div class="input-group-btn input-group-btn_btngroup_custom">
                                    <img id="save_loan_loading" style="display: none" src="../img/loader1.gif" />
                                    <asp:Button ID="btnSaveLoan" runat="server" Text="Yadda saxla" Height="26px" CssClass="btn btn-success btn-sm btn-block form-control"
                                        OnClick="btnSaveLoan_Click"
                                        OnClientClick="this.style.display = 'none';
                                                       document.getElementById('save_loan_loading').style.display = '';" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <span class="no_visible_font">Annuitet</span>
                                <div class="input-group-btn input-group-btn_btngroup_custom">
                                    <asp:Button ID="btnLoanAnnuitetOpenModal" runat="server" Text="Annuitet" OnClick="btnLoanAnnuitetOpenModal_Click"
                                        Height="26px" Style="margin-top: 5px" CssClass="btn btn-primary btn-sm form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <span class="no_visible_font">Çıxarış</span>
                                <div class="input-group-btn input-group-btn_btngroup_custom">
                                    <asp:Button ID="btnInsertCollateralOutOpenModal" OnClick="btnInsertCollateralOutOpenModal_Click" runat="server" Text="Çıxarış"
                                        Height="26px" Style="margin-top: 5px" CssClass="btn btn-primary btn-sm form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <span class="no_visible_font">Müqavilə</span>
                                <div class="input-group-btn input-group-btn_btngroup_custom">
                                    <asp:Button ID="btnLoanContractPrint" runat="server"
                                        Text="Müqavilə" OnClick="btnLoanContractPrint_Click"
                                        Height="26px" Style="margin-top: 5px" CssClass="btn btn-primary btn-sm form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <span class="no_visible_font">Təhvil</span>
                                <div class="input-group-btn input-group-btn_btngroup_custom">
                                    <asp:Button ID="btnCollateralReturnProcessOpenModal" runat="server"
                                        OnClick ="btnCollateralReturnProcessOpenModal_Click"
                                        Text="Təhvil"
                                        Height="26px" Style="margin-top: 5px" CssClass="btn btn-primary btn-sm form-control" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <span class="no_visible_font">İltizam</span>
                                <div class="input-group-btn input-group-btn_btngroup_custom">
                                    <asp:Button ID="btnLoanIltizamPrint" runat="server" Text="İltizam" OnClick="btnLoanIltizamPrint_Click"
                                        Height="26px" Style="margin-top: 5px" CssClass="btn btn-primary btn-sm form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                      <span class="no_visible_font">Barcode</span>
                                <div class="input-group-btn input-group-btn_btngroup_custom">
                                    <asp:Button ID="btnCollateralBarcodeOpenModal" runat="server" Text="Barcode" 
                                        OnClick ="btnCollateralBarcodeOpenModal_Click"
                                        Height="26px" Style="margin-top: 5px" CssClass="btn btn-primary btn-sm form-control" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <span class="no_visible_font">Girov Akti</span>
                                <div class="input-group-btn input-group-btn_btngroup_custom">
                                    <asp:Button ID="btnLoanCollateralAktPrint" runat="server" Text="Girov Akti" OnClick="btnLoanCollateralAktPrint_Click"
                                        Height="26px" Style="margin-top: 5px" CssClass="btn btn-primary btn-sm form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                 <span class="no_visible_font">Sil</span>
                                <div class="input-group-btn input-group-btn_btngroup_custom">
                                    <asp:Button ID="btnLoanDeleteOpenModal" runat="server" Text="Sil" OnClick ="btnLoanDeleteOpenModal_Click"
                                        Height="26px" Style="margin-top: 5px" CssClass="btn btn-danger btn-sm form-control" />
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="mb20"></div>
        <div class="row">
            <div class="col-sm-12">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-line">
                    <li id="tabLoanPayments" runat="server">
                        <asp:LinkButton ID="lnkTabLoanPayments" OnClick="lnkTabLoanPayments_Click" runat="server"><strong>ÖDƏMƏLƏR</strong></asp:LinkButton>
                    </li>
                    <li id="tabLoanColleteral" runat="server">
                        <asp:LinkButton ID="lnkTabLoanColleteral" OnClick="lnkTabLoanColleteral_Click" runat="server"><strong>GİROVLAR</strong></asp:LinkButton>
                    </li>
                    <li id="tabLoanCards" runat="server">
                        <asp:LinkButton ID="lnkTabLoanCards" OnClick="lnkTabLoanCards_Click" runat="server"><strong>KARTLAR</strong></asp:LinkButton>
                    </li>
                    <li id="tabLoanDocuments" runat="server">
                        <asp:LinkButton ID="lnkTabLoanDocuments" OnClick="lnkTabLoanDocuments_Click" runat="server"><strong>SƏNƏDLƏR</strong></asp:LinkButton>
                    </li>
                    <li id="tabLoanGuarantors" runat="server">
                        <asp:LinkButton ID="lnkTabLoanGuarantor" OnClick="lnkTabLoanGuarantor_Click" runat="server"><strong>ZAMİNLƏR</strong></asp:LinkButton>
                    </li>
                    <li id="tabLoanNotes" runat="server">
                        <asp:LinkButton ID="lnkTabLoanNotes" OnClick="lnkTabLoanNotes_Click" runat="server"><strong>QEYDLƏR</strong></asp:LinkButton>
                    </li>
                    <li id="tabLoanAdditionals" runat="server">
                        <asp:LinkButton ID="lnkTabLoanAdditionals" OnClick="lnkTabLoanAdditionals_Click" runat="server"><strong>ƏLAVƏLƏR</strong></asp:LinkButton>
                    </li>

                </ul>

                <!-- Tab panes -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="tab-content" style="overflow-y: scroll; height: 400px;">
                            <asp:MultiView ID="MultiviewLoans" runat="server" ActiveViewIndex="0">
                                <asp:View ID="ViewTabLoanPayments" runat="server">
                                    <div class="tab-pane active">
                                        <!--Loan Payments-->

                                    </div>
                                </asp:View>
                                <asp:View ID="ViewTabLoanColleteral" runat="server">
                                    <div class="tab-pane active">
                                        <!--Loan Colleteral-->


                                        <div class="panel-body" style="padding: 10px !important">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Depozit qutusu<span class="text-danger"></span></label>
                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="txtLoanCollateralDepositBox" autocomplete="off" CssClass="form-control input-sm" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <label class="col-sm-6 control-label">Zərgər haqqı<span class="text-danger"></span></label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtLoanCollateralJewelerCost" autocomplete="off" CssClass="form-control input-sm" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">Zərgər adı<span class="text-danger"></span></label>
                                                        <div class="col-sm-10">
                                                            <asp:TextBox ID="txtLoanCollateralJewelerName" autocomplete="off" class="form-control input-sm" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 control-label">Təhvil tarixi<span class="text-danger"></span></label>
                                                        <div class="col-sm-7">
                                                            <asp:TextBox ID="txtLoanCollateralReturnDate" ReadOnly autocomplete="off" class="form-control input-sm" runat="server"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <div class="input-group-btn input-group-btn_btngroup_custom">
                                                            <img id="save_loancollateralgeneralinfo_loading_loading" style="display: none" src="../img/loader1.gif" />
                                                            <asp:Button ID="btnSaveLoanCollateralGeneralData" runat="server" Text="Yadda saxla" Height="26px" CssClass="btn btn-success btn-sm btn-block form-control"
                                                                OnClick="btnSaveLoanCollateralGeneralData_Click"
                                                                OnClientClick="this.style.display = 'none';
                                                                            document.getElementById('save_loancollateralgeneralinfo_loading_loading').style.display = '';" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>




                                        <ul class="list-inline">
                                            <li>
                                                <asp:LinkButton ID="lnkNewLoanCollateral" runat="server" OnClick="lnkNewLoanCollateral_Click">
                                                   <i class="fa fa-plus"></i><span> Yeni girov</span>
                                                </asp:LinkButton>
                                            </li>
                                        </ul>
                                        <div class="mb20"></div>


                                        <div class="table-responsive">
                                            <asp:GridView class="table table-condensed table-bordered table-primary nomargin" ID="grdLoanCollaterals" runat="server"
                                                DataKeyNames="CollateralID,Transaction,LoanID"
                                                AutoGenerateColumns="False" GridLines="None"
                                                ShowFooter="true">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Tipi / Adı">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGridLoanCollateralType" runat="server" Style="font-weight: bold" Text='<%#Eval("CollateralType") %>'></asp:Label><br />
                                                            <asp:Label ID="lblGridLoanCollateralName" runat="server" Text='<%#Eval("CollateralName") %>'></asp:Label><br />
                                                            <asp:Label ID="lblGridLoanCollateralData" runat="server" Text='<%#Eval("Cdate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="200px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Vahidi / Əyarı">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGridLoanCollateralUnitOfMeasurement" runat="server" Text='<%#Eval("UnitOfMeasurement") %>'></asp:Label><b> / </b>
                                                            <asp:Label ID="lblGridLoanCollateralGoldCarat" runat="server" Text='<%#Eval("GoldCarat") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Qiymət / Say">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGridLoanCollateralPrice" runat="server" Text='<%#Eval("Price") %>'></asp:Label>
                                                            <b>/ </b>
                                                            <asp:Label ID="lblGridLoanCollateralCount" runat="server" Text='<%#Eval("Count") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblGridLoanCollateralCountFooter" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Ümumi / Daş çəki">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGridLoanCollateralBruttoWeight" runat="server" Text='<%#Eval("BruttoWeight") %>'></asp:Label><b> / </b>
                                                            <asp:Label ID="lblGridLoanCollateralWeight" runat="server" Text='<%#Eval("Weight") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblGridLoanCollateralBruttoWeightFooter" runat="server"></asp:Label>
                                                            <b>/ </b>
                                                            <asp:Label ID="lblGridLoanCollateralWeightFooter" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="130px" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Çəki">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGridLoanCollateralNettoWeight" runat="server" Text='<%#Eval("NettoWeight") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblGridLoanCollateralNettoWeightFooter" runat="server"></asp:Label><br />
                                                        </FooterTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="80px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Dəyəri">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGridLoanCollateralValue" runat="server" Text='<%#Eval("CollateralValue") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblGridLoanCollateralValueFooter" runat="server"></asp:Label><br />
                                                        </FooterTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="70px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Qeyd">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGridLoanCollateralNote" runat="server" Text='<%#Eval("Note") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnLoanCollateralSendOutOpenModal" Visible='<%# Convert.ToString(Eval("Transaction")) == "IN"%>' CssClass="btn btn-default btn-sm" runat="server" Text="Təhvil ver" OnClick="btnLoanCollateralSendOutOpenModal_Click" />
                                                            <%# Convert.ToString(Eval("Transaction")) == "OUT" ?  "<span class=\"btn  btn-sm label-warning\" style =\"cursor:default\">Təhvil verilib</span>" : "" %>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="80px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEditLoanClollateral" OnClick="lnkEditLoanClollateral_Click"
                                                                Visible='<%# Convert.ToString(Eval("Transaction")) == "IN"%>'
                                                                CommandArgument='<%# Eval("CollateralID") %>' title="Redaktə" runat="server">
                                                               <i class="fa fa-pencil"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="45px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDeleteLoanClollateral" OnClick="lnkDeleteLoanClollateral_Click" CommandArgument='<%# Eval("CollateralID") %>' title="Sil" runat="server">
                                                               <i class="fa fa-trash"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="45px" />
                                                    </asp:TemplateField>

                                                </Columns>

                                                <FooterStyle BackColor="#8BC9D8" Font-Bold="True" ForeColor="White" Height="20px" />
                                                <HeaderStyle CssClass="bg-sucess" />
                                            </asp:GridView>
                                        </div>


                                    </div>
                                </asp:View>
                                <asp:View ID="ViewTabLoanCards" runat="server">
                                    <div class="tab-pane active">

                                        <!--Loan Cards-->
                                        <ul class="list-inline">
                                            <li>
                                                <asp:LinkButton ID="lnkNewLoanCard" runat="server" OnClick="lnkNewLoanCard_Click">
                                           <i class="fa fa-plus"></i><span> Yeni kart</span>
                                                </asp:LinkButton>
                                            </li>
                                        </ul>
                                        <div class="table-responsive">
                                            <asp:GridView class="table table-bordered table-primary nomargin" ID="grdLoanCards" runat="server"
                                                OnRowEditing="grdLoanCards_RowEditing"
                                                OnRowCancelingEdit="grdLoanCards_RowCancelingEdit"
                                                OnRowUpdating="grdLoanCards_RowUpdating"
                                                DataKeyNames="CardID,LoanID"
                                                AutoGenerateColumns="False" GridLines="None">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="KART NÖMRƏSİ">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGridLoanCard_Pan" runat="server" Text='<%#Eval("Pan") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtGridLoanCard_Pan" CssClass="form-control input-sm panMask" Placeholder="0000-0000-0000-0000" runat="server" Text='<%#Eval("Pan") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="BİTMƏ TARİXİ">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGridLoanCard_DateOfExpiry" runat="server" Text='<%#Eval("DateOfExpiry") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtGridLoanCard_DateOfExpiry" CssClass="form-control datepicker_mmyy input-sm" autocomplete="off" Placeholder="mm/yyyy" runat="server" Text='<%#Eval("DateOfExpiry") %>'></asp:TextBox>
                                                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                            </div>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="MƏBLƏĞ">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGridLoanCard_CardAmount" runat="server" Text='<%#Eval("CardAmount") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtGridLoanCard_CardAmount" CssClass="form-control input-sm" Placeholder="0.00" runat="server" Text='<%#Eval("CardAmount") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="AÇAR SÖZ">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGridLoanCard_SecretWord" runat="server" Text='<%#Eval("SecretWord") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtGridLoanCard_SecretWord" CssClass="form-control input-sm" Placeholder="Açar söz" runat="server" Text='<%#Eval("SecretWord") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnLoanCardEdit" Width="100%" runat="server" CssClass="btn btn-primary btn-sm" Text="Düzəliş et" CommandName="Edit" />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Button ID="btnUpdateLoanCard" Width="48%" runat="server" CssClass="btn btn-success btn-sm" Text="Yenilə" CommandName="Update" />
                                                            <asp:Button ID="btnCancelLoanCard" Width="48%" runat="server" CssClass="btn btn-danger btn-sm" Text="İmtina" CommandName="Cancel" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDeleteCustomerData" CommandArgument='<%# Eval("CardId") %>' title="Sil" runat="server" CssClass="btn-sm" OnClick="lnkDeleteCustomerData_Click">
                                                                    <i class="fa fa-trash btn btn-danger btn-sm"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="60px" />
                                                    </asp:TemplateField>

                                                </Columns>
                                                <HeaderStyle CssClass="bg-sucess" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </asp:View>
                                <asp:View ID="ViewTabLoanDocuments" runat="server">
                                    <div class="tab-pane active">
                                        <!--Loan Documents-->
                                        <div class="row">
                                            <%--  <div class="col-sm-6">
                                                <asp:TextBox ID="txtAddLoanDocumentNote" autocomplete="off" class="form-control" MaxLength="255" placeHolder="Qeyd" runat="server"></asp:TextBox>
                                            </div>--%>
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="drlAddLoanDocumentNote" class="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:FileUpload ID="flUploadDocument" class="form-control" runat="server" />
                                            </div>
                                            <div class="col-sm-2">
                                                <asp:LinkButton ID="lnkAddLoanDocument" OnClick="lnkAddLoanDocument_Click" runat="server" CssClass="button-for-form-control-input">
                                                 <i class="fa fa-plus"></i><span> Yadda saxla</span>
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="mb20"></div>

                                        <div class="table-responsive">
                                            <asp:GridView class="table table-bordered table-primary nomargin" ID="grdLoanDocuments" runat="server"
                                                DataKeyNames="DocumentID,LoanID"
                                                AutoGenerateColumns="False" GridLines="None">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SƏNƏD NÖVÜ">
                                                        <ItemTemplate>
                                                            <%#Eval("Note")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="SƏNƏD ADI">
                                                        <ItemTemplate>
                                                            <%# Convert.ToString(Eval("FileName")).Split('_')[1]%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDownloadLoanDocument" OnClick="lnkDownloadLoanDocument_Click" CommandArgument='<%# Eval("FileName") %>' title="Yüklə" runat="server" CssClass="btn-sm">
                                                            <i class="fa fa-download"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="60px" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDeleteLoanDocument" OnClick="lnkDeleteLoanDocument_Click" CommandArgument='<%# Eval("DocumentID") %>' title="Sil" runat="server" CssClass="btn-sm">
                                                            <i class="fa fa-trash btn btn-danger btn-sm"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="60px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="bg-sucess" />
                                            </asp:GridView>
                                        </div>



                                    </div>
                                </asp:View>
                                <asp:View ID="ViewTabLoanGuarantors" runat="server">
                                    <div class="tab-pane active">
                                        <!--Loan Guarantors-->

                                        <ul class="list-inline">
                                            <li>
                                                <asp:LinkButton ID="lnkAddLoanGuarantor" runat="server" OnClick="lnkAddLoanGuarantor_Click">
                                                 <i class="fa fa-plus"></i><span> Yeni zamin</span>
                                                </asp:LinkButton>
                                            </li>
                                        </ul>

                                        <div class="table-responsive">
                                            <asp:GridView class="table table-bordered table-primary nomargin" ID="grdLoanGuarantor" runat="server"
                                                DataKeyNames="GuarantorID,CustomerID,LoanID"
                                                AutoGenerateColumns="False" GridLines="None">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ADI">
                                                        <ItemTemplate>
                                                            <%#Eval("GuarantorName") + " (" + Eval("Document") + ")"%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="TELEFON 1">
                                                        <ItemTemplate>
                                                            <%# Eval("MobilePhone1")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="TELEFON 2">
                                                        <ItemTemplate>
                                                            <%# Eval("MobilePhone2")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDeleteLoanGuarantor" OnClick="lnkDeleteLoanGuarantor_Click" CommandArgument='<%# Eval("GuarantorID") %>' title="Sil" runat="server" CssClass="btn-sm">
                                                                    <i class="fa fa-trash btn btn-danger btn-sm"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="60px" />
                                                    </asp:TemplateField>

                                                </Columns>
                                                <HeaderStyle CssClass="bg-sucess" />
                                            </asp:GridView>
                                        </div>


                                    </div>
                                </asp:View>
                                <asp:View ID="ViewTabLoanNotes" runat="server">
                                    <div class="tab-pane active">
                                        <!--Loan Notes-->
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtAddLoanNote" autocomplete="off" class="form-control" MaxLength="255" placeHolder="Qeyd" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:LinkButton ID="lnkAddLoanNote" runat="server" Width="90px" CssClass="button-for-form-control-input" OnClick="lnkAddLoanNote_Click">
                                                 <i class="fa fa-plus"></i><span> Əlavə et</span>
                                                </asp:LinkButton>
                                            </div>
                                        </div>

                                        <div class="mb20"></div>

                                        <div class="table-responsive">
                                            <asp:GridView class="table table-bordered table-primary nomargin" ID="grdLoanNotes" runat="server"
                                                DataKeyNames="LoanNoteID,LoanID"
                                                AutoGenerateColumns="False" GridLines="None">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Qeyd">
                                                        <ItemTemplate>
                                                            <%#Eval("Note")%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="70%" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Avtor">
                                                        <ItemTemplate>
                                                            <%# Eval("CreatedName")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="TARİX">
                                                        <ItemTemplate>
                                                            <%# Eval("CreatedDate")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDeleteLoanNote" CommandArgument='<%# Eval("LoanNoteID") %>' OnClick="lnkDeleteLoanNote_Click" title="Sil" runat="server" CssClass="btn-sm">
                                                                    <i class="fa fa-trash btn btn-danger btn-sm"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="60px" />
                                                    </asp:TemplateField>

                                                </Columns>
                                                <HeaderStyle CssClass="bg-sucess" />
                                            </asp:GridView>
                                        </div>


                                    </div>
                                </asp:View>
                                <asp:View ID="ViewLoanAdditionals" runat="server">
                                    <div class="tab-pane active">
                                        <!--Loan Additionals-->

                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    Kredit verən şəxs<span class="text-danger"></span>
                                                    <asp:DropDownList ID="drlLender" class="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <span class="no_visible_font">Yadda saxla</span>
                                                    <div class="input-group-btn input-group-btn_btngroup_custom">
                                                        <img id="save_lender_loading" style="display: none" src="../img/loader1.gif" />
                                                        <asp:Button ID="btnSaveLender" runat="server" Text="Yadda saxla" CssClass="btn btn-success form-control"
                                                            OnClick="btnSaveLender_Click"
                                                            Width="100px"
                                                            OnClientClick="this.style.display = 'none';
                                                            document.getElementById('save_lender_loading').style.display = '';" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb10"></div>

                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="form-group">
                                                    Krediti təsdiq edən şəxs<span class="text-danger"></span>
                                                    <asp:DropDownList ID="drlVerifycator" disabled class="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <%--  
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <span class="no_visible_font">Yadda saxla</span>
                                                    <div class="input-group-btn input-group-btn_btngroup_custom">
                                                        <img id="save_verifycator_loading" style="display: none" src="../img/loader1.gif" />
                                                        <asp:Button ID="btnSaveVerifycator" runat="server" Text="Yadda saxla" CssClass="btn btn-success form-control"
                                                            OnClick="btnSaveVerifycator_Click"
                                                            Width="100px"
                                                            OnClientClick="this.style.display = 'none';
                                                            document.getElementById('save_verifycator_loading').style.display = '';" />
                                                    </div>
                                                </div>
                                            </div>--%>
                                        </div>


                                    </div>
                                </asp:View>
                            </asp:MultiView>
                        </div>


                    </div>
                </div>


            </div>
        </div>





        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal bounceIn" id="modalCustomerSearch" data-backdrop="static">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalSearchCustomerLabel">
                                Müştəri axtarışı
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <asp:TextBox ID="txtSearchCustomerName" class="form-control" placeHolder="Müştəri adı..." runat="server"></asp:TextBox>
                                            <div class="input-group-btn">
                                                <img id="search_customer_loading" style="display: none" src="../img/loader1.gif" />
                                                <asp:Button ID="btnSearchCustomer" OnClick="btnSearchCustomer_Click" class="btn btn-primary" runat="server" Text="Axtarış"
                                                    OnClientClick="this.style.display = 'none';
                                                  document.getElementById('search_customer_loading').style.display = '';" />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="mb20"></div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <asp:GridView class="table table-bordered table-primary nomargin" ID="grdCustomers" runat="server" OnRowDataBound="grdCustomers_RowDataBound"
                                                DataKeyNames="CustomerID,DocumentNumber,Fullname"
                                                AutoGenerateColumns="False" GridLines="None">
                                                <Columns>
                                                    <asp:BoundField DataField="FullName" HeaderText="S.A.A" />
                                                    <asp:BoundField DataField="BranchName" HeaderText="FİLİAL" />
                                                    <asp:TemplateField HeaderText="SƏNƏD NÖMRƏSİ">
                                                        <HeaderTemplate>
                                                            SƏNƏD NÖMRƏSİ
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <%# Eval("IdCardSeries") + " " +  Eval("IdCardNumber") %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkSelectCustomerData" OnClick="lnkSelectCustomerData_Click" CommandArgument='<%# Eval("CustomerID") %>' title="Seç" runat="server">Seç</asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="32px" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <table class="table nomargin">
                                                        <tr>
                                                            <td>Müştəri tapılmadı</td>
                                                            <td>
                                                                <asp:LinkButton ID="lnkNewCustomer" OnClick="lnkNewCustomer_Click" runat="server"><i class="fa fa-user-plus"></i><span> Yeni müştəri</span></asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EmptyDataTemplate>

                                                <HeaderStyle CssClass="bg-blue" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hdnCustomerModalType" runat="server" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSearchCustomer" />
            </Triggers>
        </asp:UpdatePanel>



        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalCardDeleteAlert" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalAlertLabel">
                                Kartın silinməsi
                            </div>
                            <div class="modal-body">
                                <p>
                                    Kart silinsin?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="delete_Loan_card_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnDeleteLoanCard" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClick="btnDeleteLoanCard_Click"
                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('delete_Loan_card_loading').style.display = '';
                                                           document.getElementById('btnDeleteLoanCreditCancel').style.display = 'none';" />
                                <button id="btnDeleteLoanCreditCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDeleteLoanCard" />
            </Triggers>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalLoanNoteDeleteAlert" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalNoteAlertLabel">
                                Qeydin silinməsi
                            </div>
                            <div class="modal-body">
                                <p>
                                    Qeyd silinsin?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="delete_Loan_note_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnDeleteLoanNote" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClick="btnDeleteLoanNote_Click"
                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('delete_Loan_note_loading').style.display = '';
                                                           document.getElementById('btnDeleteLoanNoteCancel').style.display = 'none';" />
                                <button id="btnDeleteLoanNoteCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDeleteLoanNote" />
            </Triggers>
        </asp:UpdatePanel>



        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalLoanGuarantorDeleteAlert" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalGuarantorAlertLabel">
                                Zaminin silinməsi
                            </div>
                            <div class="modal-body">
                                <p>
                                    Zamin silinsin?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="delete_Loan_guarantor_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnDeleteLoanGuarantor" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClick="btnDeleteLoanGuarantor_Click"
                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('delete_Loan_guarantor_loading').style.display = '';
                                                           document.getElementById('btnDeleteLoanGuarantorCancel').style.display = 'none';" />
                                <button id="btnDeleteLoanGuarantorCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDeleteLoanGuarantor" />
            </Triggers>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalLoanDocumentDeleteAlert" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalDocumentAlertLabel">
                                Sənədin silinməsi
                            </div>
                            <div class="modal-body">
                                <p>
                                    Sənəd silinsin?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="delete_Loan_document_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnDeleteLoanDocument" OnClick="btnDeleteLoanDocument_Click" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('delete_Loan_document_loading').style.display = '';
                                                           document.getElementById('btnDeleteLoanDocumentCancel').style.display = 'none';" />
                                <button id="btnDeleteLoanDocumentCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDeleteLoanDocument" />
            </Triggers>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal bounceIn" id="modalCollateralInOut" data-backdrop="static">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalCollateralInOutLabel">
                                    <asp:Literal ID="ltrModalCollateralInOutHeaderLabel" runat="server"></asp:Literal>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            Girov tipi<span class="text-danger"></span>
                                            <asp:DropDownList ID="drlLoanColleteralType" AppendDataBoundItems="true" class="form-control input-sm" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb10"></div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            Girov adı<span class="text-danger"></span>
                                            <asp:TextBox ID="txtLoanCollateralName" runat="server"
                                                autocomplete="off" class="form-control input-sm" placeholder="Girov adı"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="mb10"></div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Ölçü vahidi<span class="text-danger"></span>
                                            <asp:DropDownList ID="drlLoanCollateralUnitOfMeasurement" class="form-control input-sm" runat="server">
                                                <asp:ListItem Value="" Selected="True">Seçin</asp:ListItem>
                                                <asp:ListItem Value="Qram">Qram</asp:ListItem>
                                                <asp:ListItem Value="Ədəd">Ədəd</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Əyar<span class="text-danger"></span>
                                            <asp:DropDownList ID="drlLoanCollateralGoldCarat" OnSelectedIndexChanged="drlLoanCollateralGoldCarat_SelectedIndexChanged" AutoPostBack="true" class="form-control input-sm" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb10"></div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Qiymət<span class="text-danger"></span>
                                            <asp:TextBox ID="txtCollateralPrice" runat="server"
                                                autocomplete="off" class="form-control input-sm" placeholder="Qiymət"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Say<span class="text-danger"></span>
                                            <asp:TextBox ID="txtCollateralCount" runat="server"
                                                autocomplete="off" class="form-control input-sm" placeholder="Say"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb10"></div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Ümumi çəki<span class="text-danger"></span>
                                            <asp:TextBox ID="txtCollateralBruttoWeight" Text="1" runat="server"
                                                autocomplete="off" class="form-control input-sm" placeholder="Ümumi çəki"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            Daş çəki<span class="text-danger"></span>
                                            <asp:TextBox ID="txtCollateralWeight" Text="0" runat="server"
                                                autocomplete="off" class="form-control input-sm" placeholder="Daş çəki"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb10"></div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            Qeyd<span class="text-danger"></span>
                                            <asp:TextBox ID="txtCollateralNote" runat="server"
                                                autocomplete="off" class="form-control input-sm" placeholder="Qeyd"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <img id="add_modify_loan_collateral_accept_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnAcceptCollateral" OnClick="btnAcceptCollateral_Click" class="btn btn-primary" runat="server" Text="Təsdiq et"
                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_modify_loan_collateral_accept_loading').style.display = '';
                                                           document.getElementById('btnAcceptCollateralCancel').style.display = 'none';" />
                                <button id="btnAcceptCollateralCancel" type="button" class="btn btn-default" data-dismiss="modal">İmtina et</button>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hdnCollateralOperType" runat="server" />
                <!--insert / update-->

                <asp:HiddenField ID="hdnCollateralTransactionType" runat="server" />
                <!--in / out-->

                <asp:HiddenField ID="hdnCollateralID" runat="server" />
                <asp:HiddenField ID="hdnDeleteCollateralID" runat="server" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAcceptCollateral" />
                <asp:PostBackTrigger ControlID="drlLoanCollateralGoldCarat" />
            </Triggers>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalCollateralDeleteAlert" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalAlertLabell">
                                Girovun silinməsi
                            </div>
                            <div class="modal-body">
                                <p>
                                    Girov silinsin?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="delete_Loan_Collateral_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnDeleteLoanCollateral" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClick ="btnDeleteLoanCollateral_Click"
                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('delete_Loan_Collateral_loading').style.display = '';
                                                           document.getElementById('btnDeleteLoanCollateralCancel').style.display = 'none';" />
                                <button id="btnDeleteLoanCollateralCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDeleteLoanCollateral" />
            </Triggers>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal bounceIn" id="modalLoanAnnuitet" data-backdrop="static">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalmodalLoanAnnuitetLabel">Ödəmə cədvəli
                                </h4>
                            </div>
                            <div class="modal-body" style="overflow-x: auto; max-height: 550px">
                                <div class="row">
                                    <div class="col-sm-12" style="text-align: right">
                                        <asp:LinkButton ID="lnkLoanAnnuitetPrintTemplate" runat="server"
                                            OnClick="lnkLoanAnnuitetPrintTemplate_Click">
                                                <span class="glyphicon glyphicon-print"></span>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                                <div class="mb20"></div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="table-responsive">
                                            <asp:GridView class="table table-bordered table-primary nomargin" ID="grdLoanAnnuitet" runat="server"
                                                DataKeyNames="ID"
                                                AutoGenerateColumns="False" GridLines="None"
                                                ShowFooter="true">
                                                <Columns>
                                                    <asp:BoundField DataField="Period" HeaderText="N" />
                                                    <asp:BoundField DataField="PaymentDate" HeaderText="ÖDƏMƏ TARİXİ" />
                                                    <asp:BoundField DataField="BeginingBalans" HeaderText="İLKİN BALANS" />
                                                    <asp:TemplateField HeaderText="ƏSAS MƏBLƏĞ">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGridLoanAnnuitetPPMT" runat="server" Text='<%#Eval("PPMT") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblGridLoanAnnuitetPPMT_f" runat="server" Style="font-weight: bold"></asp:Label>
                                                        </FooterTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="FAİZ">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGridLoanAnnuitetIPMT" runat="server" Text='<%#Eval("IPMT") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblGridLoanAnnuitetIPMT_f" runat="server" Style="font-weight: bold"></asp:Label>
                                                        </FooterTemplate>
                                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="MonthlyPayment" HeaderText="AYLIQ ÖDƏNİŞ" />
                                                    <asp:BoundField DataField="EndingBalans" HeaderText="Son Balans" />

                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <table class="table nomargin">
                                                        <tr>
                                                            <td>Kreditə aid qrafik təyin edilməyib</td>
                                                        </tr>
                                                    </table>
                                                </EmptyDataTemplate>
                                                <FooterStyle BackColor="#8BC9D8" Font-Bold="True" ForeColor="White" Height="20px" />
                                                <HeaderStyle CssClass="bg-blue" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lnkLoanAnnuitetPrintTemplate" />
            </Triggers>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalInsertCollateralOutAlert" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalInsertCollateralOutAlertLabel">
                                Çıxarışın göndərilməsi
                            </div>
                            <div class="modal-body">
                                <p>
                                    Çıxarış göndərilsin?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="insert_collateral_out_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnInsertCollateralOut" OnClick="btnInsertCollateralOut_Click" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClientClick="this.style.display = 'none';
                                    document.getElementById('insert_collateral_out_loading').style.display = '';
                                    document.getElementById('btnInsertCollateralOutCancel').style.display = 'none';" />
                                <button id="btnInsertCollateralOutCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnInsertCollateralOut" />
            </Triggers>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalReturnCollateralProcessAlert" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalReturnCollateralProcessAlertLabel">
                                Girov təhvili
                            </div>
                            <div class="modal-body">
                                <p>
                                    Girov təhvil verilsin?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <img id="return_collateral_process_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnCollateralReturnProcess" class="btn btn-primary" runat="server" Text="Bəli"
                                    OnClick ="btnCollateralReturnProcess_Click"
                                    OnClientClick="this.style.display = 'none';
                                    document.getElementById('return_collateral_process_loading').style.display = '';
                                    document.getElementById('btnCollateralReturnProcessCancel').style.display = 'none';" />
                                <button id="btnCollateralReturnProcessCancel" type="button" class="btn btn-default" data-dismiss="modal">Xeyr</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnCollateralReturnProcess" />
            </Triggers>
        </asp:UpdatePanel>


        <asp:UpdatePanel ID="UpdatePanel11" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalCollateralBarcodeAlert" data-backdrop="static">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								 <h4 class="modal-title" id="modalCollateralBarcodeAlertLabel">
                                     <asp:LinkButton ID="lnkPrintCollateralBarcode" runat="server" ToolTip ="Çap et" OnClick ="lnkPrintCollateralBarcode_Click">
                                         <span class="glyphicon glyphicon-print"></span>
                                     </asp:LinkButton>
                            </div>
                            <div class="modal-body" id ="barcode_print">
                                <asp:Literal ID="ltrCollateralListForBarcode" runat="server"></asp:Literal>
                            </div>
                            <div class="modal-footer">
                                <button id="btnCollateralBarcodeCancel" type="button" class="btn btn-default" data-dismiss="modal">Bağla</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lnkPrintCollateralBarcode" />
            </Triggers>
        </asp:UpdatePanel>


          <asp:UpdatePanel ID="UpdatePanel12" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <!-- Modal  Alert-->
                <div class="modal bounceIn" id="modalDeleteLoanAlert" data-backdrop="static">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="modalDeleteLoanAlertLabel">
                                Kreditin silinməsi
                            </div>
                            <div class="modal-body">
                                <div class ="row">
                                     <div class="col-sm-12">
                                        <div class="form-group">
                                            Silinmə səbəbi<span class="text-danger"></span>
                                            <asp:TextBox ID="txtDeleteLoanDescription" Text="" runat="server"
                                                autocomplete="off" class="form-control input-sm" placeholder="Silinmə səbəbi"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <img id="delete_loan_loading" style="display: none" src="../img/loader1.gif" />
                                <asp:Button ID="btnDeleteLoan" class="btn btn-danger" runat="server" Text="Sil"
                                    OnClick ="btnDeleteLoan_Click"
                                    OnClientClick="this.style.display = 'none';
                                    document.getElementById('delete_loan_loading').style.display = '';
                                    document.getElementById('btnDeleteLoanCancel').style.display = 'none';" />
                                <button id="btnDeleteLoanCancel" type="button" class="btn btn-default" data-dismiss="modal">Imtina</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnDeleteLoan" />
            </Triggers>
        </asp:UpdatePanel>





        <script>
            function openSearchCustomerModal() {
                $('#modalCustomerSearch').modal({ show: true });
            }

            function openCardDeleteAlertModal() {
                $('#modalCardDeleteAlert').modal({ show: true });
            }

            function openNoteDeleteAlertModal() {
                $('#modalLoanNoteDeleteAlert').modal({ show: true });
            }

            function openGuarantorDeleteAlertModal() {
                $('#modalLoanGuarantorDeleteAlert').modal({ show: true });
            }

            function openDocumentDeleteAlertModal() {
                $('#modalLoanDocumentDeleteAlert').modal({ show: true });
            }

            function openCollateralInOutModal() {
                $('#modalCollateralInOut').modal({ show: true });
            }

            function openCollateralDeleteModal() {
                $('#modalCollateralDeleteAlert').modal({ show: true });
            }

            function openLoanAnnuitetModal() {
                $('#modalLoanAnnuitet').modal({ show: true });
            }

            function openInsertCollateralOutModal() {
                $('#modalInsertCollateralOutAlert').modal({ show: true });
            } 

            function openReturnCollateralProcessModal() {
                $('#modalReturnCollateralProcessAlert').modal({ show: true });
            }

            function openCollateralBarcodeModal() {
                $('#modalCollateralBarcodeAlert').modal({ show: true });
            }

            function openDeleteLoanModal() {
                $('#modalDeleteLoanAlert').modal({ show: true });
            }

            

        </script>
        <style>
            .input-sm {
                height: 26px !important;
                line-height: 26px !important;
            }


            .input-group-addon {
                padding: 5px 7px !important;
            }

            .form-group {
                margin-bottom: 10px;
            }

            .no_visible_font {
                color: #ffffff;
            }



            .input-group-btn_btngroup_custom .btn:not(.btn-lg),
            .input-group-btn_btngroup_custom .btn:not(.btn-sm),
            .input-group-btn_btngroup_custom .btn:not(.btn-xs) {
                min-height: 26px !important;
            }

            .collateral > a.collapsed {
                padding: 10px 15px !important;
                color: #505b72;
            }
        </style>




        <script>
            $(function () {
                // Input Masks
                $('.panMask').mask("9999-9999-9999-9999");
            });
        </script>

        <script>


            function txtCAC_Copy() {
                var x = document.getElementById('<%=txtColleteralNo.ClientID%>').value;
                document.getElementById('<%=txtACNo.ClientID%>').value = x;
                document.getElementById('<%=txtContractNo.ClientID%>').value = x;
            }

            function calculateFeeAmount() {
                var feePercent =  <%=ConfigurationManager.AppSettings["feePercent"].ToString() %>;
                var loanAmount = document.getElementById('<%=txtAmount.ClientID%>').value;
                var feeAmount = (feePercent * loanAmount) / 100.00;
                document.getElementById('<%=txtFeeAmount.ClientID%>').value = feeAmount;
                // document.getElementById('<%=txtRestAmount.ClientID%>').value = loanAmount;
            }

           

            function PrintBarcodeElem() {
                var mywindow = window.open('', 'PRINT');

                mywindow.document.write('<html><head>');
                mywindow.document.write('<link rel="stylesheet" href="../lib/jquery-ui/jquery-ui.css"/>');
                mywindow.document.write('<link rel="stylesheet" href="../lib/select2/select2.css"/>');
                mywindow.document.write('<link rel="stylesheet" href="../lib/dropzone/dropzone.css"/>');
                mywindow.document.write('<link rel="stylesheet" href="../lib/Hover/hover.css" />');
                mywindow.document.write('<link rel="stylesheet" href="../lib/fontawesome/css/font-awesome.css" />');
                mywindow.document.write('<link rel="stylesheet" href="../lib/weather-icons/css/weather-icons.css" />');
                mywindow.document.write('<link rel="stylesheet" href="../lib/ionicons/css/ionicons.css" />');
                mywindow.document.write('<link rel="stylesheet" href="../lib/jquery-toggles/toggles-full.css" />');
                mywindow.document.write('<link rel="stylesheet" href="../lib/jquery.gritter/jquery.gritter.css" />');
                mywindow.document.write('<link rel="stylesheet" href="../lib/animate.css/animate.css" />');
                mywindow.document.write('<link rel="stylesheet" href="../lib/timepicker/jquery.timepicker.css"/>');
                mywindow.document.write('<link rel="stylesheet" href="../lib/bootstrapcolorpicker/css/bootstrap-colorpicker.css">');
                mywindow.document.write('<link rel="stylesheet" href="../css/quirk.css"/>');

                mywindow.document.write('</head><body>');
                mywindow.document.write(document.getElementById('barcode_print').innerHTML);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10*/

                mywindow.print();
                mywindow.close();
                document.getElementById('barcode_print').style.display = 'none';
                return true;
            }


        </script>

        <asp:HiddenField ID="hdnFieldLoanId" runat="server" />
        <asp:HiddenField ID="hdnFieldDeleteLoanCardId" runat="server" />
        <asp:HiddenField ID="hdnFieldDeleteLoanNoteId" runat="server" />
        <asp:HiddenField ID="hdnFieldDeleteLoanGuarantorId" runat="server" />
        <asp:HiddenField ID="hdnFieldDeleteLoanDocumentId" runat="server" />
    </div>
</asp:Content>
