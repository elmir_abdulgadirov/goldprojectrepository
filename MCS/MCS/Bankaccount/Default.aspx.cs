﻿using MCS.App_Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WordToPDF;
using static MCS.App_Code._Config;
using static MCS.App_Code.Enums;

namespace MCS.Bankaccount
{
    public partial class Default : Settings
    {
        MCS.wsUserController.UserLoginResponse UserSession = new wsUserController.UserLoginResponse();
        MCS.wsGeneralController.GeneralController wsGController = new wsGeneralController.GeneralController();
        MCS.wsTransactionController.TransactionController wsTController = new wsTransactionController.TransactionController();
        MCS.wsAccountController.AccountController wsAController = new wsAccountController.AccountController();
        MCS.wsTemplateController.TemplateController wsTeController = new wsTemplateController.TemplateController();
        MCS.wsUserController.UserController wsUController = new wsUserController.UserController();

        _Config _config = new _Config();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserSession"] == null) _Config.Rd("/exit");
            UserSession = (MCS.wsUserController.UserLoginResponse)Session["UserSession"];
            if (!IsPostBack)
            {
                fillData();
                txtStartDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");
                txtEndDate.Text = _Config.HostingTime.ToString("dd.MM.yyyy");

                wsAccountController.GetCashTransactiontRequest Req = new wsAccountController.GetCashTransactiontRequest();
                Req.DateFrom = txtStartDate.Text;
                Req.DateTo = txtEndDate.Text;
                Req.ACNT_TYPE = "BANK_ACNT";
                Req.CurrencyID = Convert.ToInt32(drlCurrency.SelectedValue);
                Req.BranchID = Convert.ToInt32(drlBranch.SelectedValue);
                Req.Source = drlSourseMaster.SelectedValue;
                Req.UserID = UserSession.UserId;
                fillGrid(Req);
            }

            #region Begin Permission
              CheckFunctionPermission(UserSession.UserId, Convert.ToString(EnumFunctions.BANK_ACCOUNT));
              if (grdBranchGroupData.Rows.Count > 0)
              {
                  grdBranchGroupData.Columns[4].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.PRINT_BANK_DOCUMENT));
                  grdBranchGroupData.Columns[5].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.EMAIL_BANK_DOCUMENT));
              }
              if (grdTransactions.Rows.Count > 0)
              {
                  grdTransactions.Columns[6].Visible = CheckFieldView(UserSession.UserId, Convert.ToString(EnumFunctions.PRINT_BANK_TRAN));
              }
            #endregion
        }

        void fillGrid(wsAccountController.GetCashTransactiontRequest Req)
        {
            if (!_config.isDate(Req.DateFrom))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Başlanğıc tarixi düzgün daxil edin!");
                return;
            }
            if (!_config.isDate(Req.DateTo))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Son tarixi düzgün daxil edin!");
                return;
            }

            DataTable dtBranchGroupData = new DataTable();
            DataTable dt = wsAController.GetCashTransactionList(Req, out dtBranchGroupData);

            if (dt == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!");
                return;
            }

            if (dtBranchGroupData == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!");
                return;
            }

            grdBranchGroupData.DataSource = dtBranchGroupData;
            grdBranchGroupData.DataBind();

            if (grdBranchGroupData.Rows.Count > 0)
            {
                GridViewRow drr = grdBranchGroupData.FooterRow;
                Label lblGrdBeginBalanceSum = (Label)drr.FindControl("lblGrdBeginBalanceSum");
                Label lblGrdEndBalanceSum = (Label)drr.FindControl("lblGrdEndBalanceSum");

                double BeginBalanceSum = 0;
                double EndBalanceSum = 0;

                for (int i = 0; i < grdBranchGroupData.Rows.Count; i++)
                {
                    BeginBalanceSum += Convert.ToDouble(((Label)grdBranchGroupData.Rows[i].FindControl("lblGrdBeginBalance")).Text);
                    EndBalanceSum += Convert.ToDouble(((Label)grdBranchGroupData.Rows[i].FindControl("lblGrdEndBalance")).Text);
                }
                lblGrdBeginBalanceSum.Text = BeginBalanceSum.ToString("0.00");
                lblGrdEndBalanceSum.Text = EndBalanceSum.ToString("0.00");
            }

            grdTransactions.DataSource = dt;
            grdTransactions.DataBind();
            if (grdTransactions.Rows.Count > 0)
            {
                GridViewRow drr = grdTransactions.FooterRow;
                Label lblGrdTransactionsDT_AmountSum = (Label)drr.FindControl("lblGrdTransactionsDT_AmountSum");
                Label lblGrdTransactionsCT_AmountSum = (Label)drr.FindControl("lblGrdTransactionsCT_AmountSum");

                Label lblGrdTransactionsDT_CurrencySum = (Label)drr.FindControl("lblGrdTransactionsDT_CurrencySum");
                Label lblGrdTransactionsCT_CurrencySum = (Label)drr.FindControl("lblGrdTransactionsCT_CurrencySum");

                double DT_AmountSum = 0;
                double CT_AmountSum = 0;
                string ccy = "";

                for (int i = 0; i < grdTransactions.Rows.Count; i++)
                {
                    DT_AmountSum += Convert.ToDouble(((Label)grdTransactions.Rows[i].FindControl("lblGrdTransactionsDT_Amount")).Text);
                    CT_AmountSum += Convert.ToDouble(((Label)grdTransactions.Rows[i].FindControl("lblGrdTransactionsCT_Amount")).Text);
                    ccy = Convert.ToString(grdTransactions.DataKeys[0].Values["CurrencyName"]);
                }
                lblGrdTransactionsDT_AmountSum.Text = DT_AmountSum.ToString("0.00");
                lblGrdTransactionsCT_AmountSum.Text = CT_AmountSum.ToString("0.00");

                lblGrdTransactionsDT_CurrencySum.Text = ccy;
                lblGrdTransactionsCT_CurrencySum.Text = ccy;
            }

        }

        void fillData()
        {
            DataTable dtCurrency = wsGController.CurrencyList();
            if (dtCurrency.Rows.Count > 0)
            {
                drlCurrency.Items.Clear();
                drlCurrency.DataTextField = "CurrencyName";
                drlCurrency.DataValueField = "CurrencyID";
                drlCurrency.DataSource = dtCurrency;
                drlCurrency.DataBind();
                drlCurrency.SelectedValue = "1";
            }



            //Branchs
            DataTable dtBranch = wsGController.GetUserBranchs(UserSession.UserId);
            drlBranch.Items.Clear();
            drlBranch.DataTextField = "BranchName";
            drlBranch.DataValueField = "BranchID";
            drlBranch.DataSource = dtBranch;
            drlBranch.DataBind();
            if (dtBranch.Rows.Count > 1)
            {
                drlBranch.Items.Insert(0, new ListItem("Filial", "0"));
            }

            //SourceMaster
            DataTable dtSourseMaster = wsTController.GetTransactionSourceMasterList();
            drlSourseMaster.Items.Clear();
            drlSourseMaster.DataTextField = "T_DESCRIPTION";
            drlSourseMaster.DataValueField = "T_KEY";
            drlSourseMaster.DataSource = dtSourseMaster;
            drlSourseMaster.DataBind();
            drlSourseMaster.Items.Insert(0, new ListItem("Təyinat", ""));

        }

        protected void btnSearchTran_Click(object sender, EventArgs e)
        {
            wsAccountController.GetCashTransactiontRequest Req = new wsAccountController.GetCashTransactiontRequest();
            Req.DateFrom = txtStartDate.Text;
            Req.DateTo = txtEndDate.Text;
            Req.ACNT_TYPE = "BANK_ACNT";
            Req.CurrencyID = Convert.ToInt32(drlCurrency.SelectedValue);
            Req.BranchID = Convert.ToInt32(drlBranch.SelectedValue);
            Req.Source = drlSourseMaster.SelectedValue;
            Req.UserID = UserSession.UserId;
            fillGrid(Req);
        }


        protected void lnkGrdTransactionsPrint_Click(object sender, EventArgs e)
        {
            LinkButton lnkGrdTransactionsPrint = (LinkButton)sender;
            int tranID = Convert.ToInt32(lnkGrdTransactionsPrint.CommandArgument);

            int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
            string source = Convert.ToString(grdTransactions.DataKeys[rowIndex].Values["Source"]);

            if (source.Equals("T_INCREASE_CURRENT_ACNT") || source.Equals("T_CASH_FLOW_IN"))
            {
                wsTemplateController.TempLoanPaymentRequest req = new wsTemplateController.TempLoanPaymentRequest();
                req.TransactionID = tranID;

                DataTable dtPaymentTempData = wsTeController.GetPaymentTempDataByID(req);
                if (dtPaymentTempData == null)
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!");
                    return;
                }
                if (dtPaymentTempData.Rows.Count == 0)
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!");
                    return;
                }
                DataRow dr = dtPaymentTempData.Rows[0];

                string f_t_name = Server.MapPath(@"~/DocumentTemplates/templates/GetPaymentTempDataByID.docx");
                string f_o_name = Server.MapPath("~/DocumentTemplates/" + "GetPaymentTempDataByID_" + Guid.NewGuid().ToString() + ".docx");

                // copy file from template
                System.IO.File.Copy(f_t_name, f_o_name);

                bookMarks bookmarks = new bookMarks();

                Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
                Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(f_o_name);

                //BranchName
                bookmarks.key = "BranchName1";
                bookmarks.value = Convert.ToString(dr["BranchName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


                //TranID1
                bookmarks.key = "TranID1";
                bookmarks.value = Convert.ToString(dr["TranID"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


                //BranchTaxID1
                bookmarks.key = "BranchTaxID1";
                bookmarks.value = Convert.ToString(dr["BranchTaxID"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CustomerName1
                bookmarks.key = "CustomerName1";
                bookmarks.value = Convert.ToString(dr["CustomerName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //TranDate1
                bookmarks.key = "TranDate1";
                bookmarks.value = Convert.ToString(dr["TranDate"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Amount1
                bookmarks.key = "Amount1";
                bookmarks.value = Convert.ToString(dr["Amount"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AmountToText1
                bookmarks.key = "AmountToText1";
                bookmarks.value = _config.ConvertMoneyToWord(Convert.ToString(dr["Amount"]));
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Description1
                bookmarks.key = "Description1";
                bookmarks.value = Convert.ToString(dr["T_DESCRIPTION"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //PayedPerson1
                bookmarks.key = "PayedPerson1";
                bookmarks.value = Convert.ToString(dr["PayedPerson"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CreatedUser1
                bookmarks.key = "CreatedUser1";
                bookmarks.value = Convert.ToString(dr["CreatedUser"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchName2
                bookmarks.key = "BranchName2";
                bookmarks.value = Convert.ToString(dr["BranchName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //TranID2
                bookmarks.key = "TranID2";
                bookmarks.value = Convert.ToString(dr["TranID"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchTaxID2
                bookmarks.key = "BranchTaxID2";
                bookmarks.value = Convert.ToString(dr["BranchTaxID"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CustomerName2
                bookmarks.key = "CustomerName2";
                bookmarks.value = Convert.ToString(dr["CustomerName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //TranDate2
                bookmarks.key = "TranDate2";
                bookmarks.value = Convert.ToString(dr["TranDate"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Amount2
                bookmarks.key = "Amount2";
                bookmarks.value = Convert.ToString(dr["Amount"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AmountToText2
                bookmarks.key = "AmountToText2";
                bookmarks.value = _config.ConvertMoneyToWord(Convert.ToString(dr["Amount"]));
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Description2
                bookmarks.key = "Description2";
                bookmarks.value = Convert.ToString(dr["T_DESCRIPTION"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //PayedPerson2
                bookmarks.key = "PayedPerson2";
                bookmarks.value = Convert.ToString(dr["PayedPerson"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CreatedUser2
                bookmarks.key = "CreatedUser2";
                bookmarks.value = Convert.ToString(dr["CreatedUser"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchName3
                bookmarks.key = "BranchName3";
                bookmarks.value = Convert.ToString(dr["BranchName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //TranID3
                bookmarks.key = "TranID3";
                bookmarks.value = Convert.ToString(dr["TranID"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchTaxID3
                bookmarks.key = "BranchTaxID3";
                bookmarks.value = Convert.ToString(dr["BranchTaxID"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CustomerName3
                bookmarks.key = "CustomerName3";
                bookmarks.value = Convert.ToString(dr["CustomerName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CustomerName3
                bookmarks.key = "CustomerName3";
                bookmarks.value = Convert.ToString(dr["CustomerName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //TranDate3
                bookmarks.key = "TranDate3";
                bookmarks.value = Convert.ToString(dr["TranDate"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Amount3
                bookmarks.key = "Amount3";
                bookmarks.value = Convert.ToString(dr["Amount"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AmountToText3
                bookmarks.key = "AmountToText3";
                bookmarks.value = _config.ConvertMoneyToWord(Convert.ToString(dr["Amount"]));
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Description3
                bookmarks.key = "Description3";
                bookmarks.value = Convert.ToString(dr["T_DESCRIPTION"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //PayedPerson3
                bookmarks.key = "PayedPerson3";
                bookmarks.value = Convert.ToString(dr["PayedPerson"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CreatedUser3
                bookmarks.key = "CreatedUser3";
                bookmarks.value = Convert.ToString(dr["CreatedUser"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);



                ((Microsoft.Office.Interop.Word._Document)doc).Close();
                ((Microsoft.Office.Interop.Word._Application)app).Quit();


                string pdf_file_name = f_o_name.Replace(".docx", ".pdf");

                Word2Pdf objWordPDF = new Word2Pdf();
                objWordPDF.InputLocation = f_o_name;
                objWordPDF.OutputLocation = pdf_file_name;
                objWordPDF.Word2PdfCOnversion();


                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.Buffer = true;
                response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                response.AddHeader("Content-Disposition", "attachment;filename=" + System.IO.Path.GetFileName(pdf_file_name));
                response.WriteFile(pdf_file_name);
                response.Flush();
                System.IO.File.Delete(f_o_name);
                System.IO.File.Delete(pdf_file_name);
                response.End();

            }

            if (source.Equals("T_COST") || source.Equals("T_CASH_FLOW_OUT") || source.Equals("T_CREDIT"))
            {
                DataTable dtCostTempData = wsTeController.GetCostTempDataByID(tranID);
                if (dtCostTempData == null)
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!");
                    return;
                }
                if (dtCostTempData.Rows.Count == 0)
                {
                    _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!!");
                    return;
                }
                DataRow dr = dtCostTempData.Rows[0];

                string f_t_name = Server.MapPath(@"~/DocumentTemplates/templates/GetCostTempDataById.docx");
                string f_o_name = Server.MapPath("~/DocumentTemplates/" + "GetCostTempDataById_" + Guid.NewGuid().ToString() + ".docx");

                // copy file from template
                System.IO.File.Copy(f_t_name, f_o_name);

                bookMarks bookmarks = new bookMarks();

                Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
                Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(f_o_name);

                //BranchName1
                bookmarks.key = "BranchName1";
                bookmarks.value = Convert.ToString(dr["BranchName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //TranID1
                bookmarks.key = "TranID1";
                bookmarks.value = Convert.ToString(dr["TranID"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchTaxID1
                bookmarks.key = "BranchTaxID1";
                bookmarks.value = Convert.ToString(dr["BranchTaxID"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //ResponsibilityPerson1
                bookmarks.key = "ResponsibilityPerson1";
                bookmarks.value = Convert.ToString(dr["ResponsibilityPerson"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //TranDate1
                bookmarks.key = "TranDate1";
                bookmarks.value = Convert.ToString(dr["TranDate"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Amount1
                bookmarks.key = "Amount1";
                bookmarks.value = Convert.ToString(dr["Amount"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AmountToText1
                bookmarks.key = "AmountToText1";
                bookmarks.value = _config.ConvertMoneyToWord(Convert.ToString(dr["Amount"]));
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Description1
                bookmarks.key = "Description1";
                bookmarks.value = Convert.ToString(dr["Description"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CreatedUser1
                bookmarks.key = "CreatedUser1";
                bookmarks.value = Convert.ToString(dr["CreatedUser"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


                //BranchName2
                bookmarks.key = "BranchName2";
                bookmarks.value = Convert.ToString(dr["BranchName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //TranID2
                bookmarks.key = "TranID2";
                bookmarks.value = Convert.ToString(dr["TranID"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchTaxID2
                bookmarks.key = "BranchTaxID2";
                bookmarks.value = Convert.ToString(dr["BranchTaxID"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //ResponsibilityPerson2
                bookmarks.key = "ResponsibilityPerson2";
                bookmarks.value = Convert.ToString(dr["ResponsibilityPerson"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //TranDate2
                bookmarks.key = "TranDate2";
                bookmarks.value = Convert.ToString(dr["TranDate"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Amount2
                bookmarks.key = "Amount2";
                bookmarks.value = Convert.ToString(dr["Amount"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AmountToText2
                bookmarks.key = "AmountToText2";
                bookmarks.value = _config.ConvertMoneyToWord(Convert.ToString(dr["Amount"]));
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Description2
                bookmarks.key = "Description2";
                bookmarks.value = Convert.ToString(dr["Description"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CreatedUser2
                bookmarks.key = "CreatedUser2";
                bookmarks.value = Convert.ToString(dr["CreatedUser"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchName3
                bookmarks.key = "BranchName3";
                bookmarks.value = Convert.ToString(dr["BranchName"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //TranID3
                bookmarks.key = "TranID3";
                bookmarks.value = Convert.ToString(dr["TranID"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //BranchTaxID3
                bookmarks.key = "BranchTaxID3";
                bookmarks.value = Convert.ToString(dr["BranchTaxID"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //ResponsibilityPerson3
                bookmarks.key = "ResponsibilityPerson3";
                bookmarks.value = Convert.ToString(dr["ResponsibilityPerson"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //TranDate3
                bookmarks.key = "TranDate3";
                bookmarks.value = Convert.ToString(dr["TranDate"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Amount3
                bookmarks.key = "Amount3";
                bookmarks.value = Convert.ToString(dr["Amount"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //AmountToText3
                bookmarks.key = "AmountToText3";
                bookmarks.value = _config.ConvertMoneyToWord(Convert.ToString(dr["Amount"]));
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //Description3
                bookmarks.key = "Description3";
                bookmarks.value = Convert.ToString(dr["Description"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                //CreatedUser3
                bookmarks.key = "CreatedUser3";
                bookmarks.value = Convert.ToString(dr["CreatedUser"]);
                fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

                ((Microsoft.Office.Interop.Word._Document)doc).Close();
                ((Microsoft.Office.Interop.Word._Application)app).Quit();


                string pdf_file_name = f_o_name.Replace(".docx", ".pdf");

                Word2Pdf objWordPDF = new Word2Pdf();
                objWordPDF.InputLocation = f_o_name;
                objWordPDF.OutputLocation = pdf_file_name;
                objWordPDF.Word2PdfCOnversion();


                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.Clear();
                response.Buffer = true;
                response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                response.AddHeader("Content-Disposition", "attachment;filename=" + System.IO.Path.GetFileName(pdf_file_name));
                response.WriteFile(pdf_file_name);
                response.Flush();
                System.IO.File.Delete(f_o_name);
                System.IO.File.Delete(pdf_file_name);
                response.End();
            }


        }


        //branch group transactions print
        protected void lnkPrintBranchGroupData_Click(object sender, EventArgs e)
        {//
            int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
            int branchID = Convert.ToInt32(grdBranchGroupData.DataKeys[rowIndex].Values["BranchID"]);
            int currencyId = Convert.ToInt32(grdBranchGroupData.DataKeys[rowIndex].Values["CurrencyId"]);

            int ACNT_ID = Convert.ToInt32(grdBranchGroupData.DataKeys[rowIndex].Values["ACNT_ID"]);

            decimal BeginBalance = _config.ToDecimal(grdBranchGroupData.DataKeys[rowIndex].Values["BeginBalance"]);
            decimal EndBalance = _config.ToDecimal(grdBranchGroupData.DataKeys[rowIndex].Values["EndBalance"]);
            string BranchName = Convert.ToString(grdBranchGroupData.DataKeys[rowIndex].Values["BranchName"]);



            wsTemplateController.TempCashJournalRequest req = new wsTemplateController.TempCashJournalRequest();
            req.ACNT_ID = ACNT_ID;
            req.BranchID = branchID;
            req.CurrencyID = currencyId;
            req.DateFrom = txtStartDate.Text;
            req.DateTo = txtEndDate.Text;
            req.UserID = UserSession.UserId;
            req.Source = "";
            DataTable dtList = wsTeController.GetCashJournalTempData(req);

            if (dtList == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!");
                return;
            }

            if (dtList.Rows.Count == 0)
            {
                _config.AlertMessage(this, MessageType.WARNING, "Seçilmiş kriteriyalara görə əməliyyat tapılmadı");
                return;
            }

            DataView dv = new DataView(dtList);
            DataTable dtSources = dv.ToTable(true, "Source", "T_DESCRIPTION");

            string f_t_name = Server.MapPath(@"~/DocumentTemplates/templates/GetCashJournalTempData.docx");
            string f_o_name = Server.MapPath("~/DocumentTemplates/" + "GetCashJournalTempData" + Guid.NewGuid().ToString() + ".docx");

            // copy file from template
            System.IO.File.Copy(f_t_name, f_o_name);


            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(f_o_name);

            bookMarks bookmarks = new bookMarks();

            //PrintDate
            bookmarks.key = "PrintDate";
            bookmarks.value = _Config.HostingTime.ToString("dd.MM.yyyy HH:mm");
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //BranchName
            bookmarks.key = "BranchName";
            bookmarks.value = BranchName;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //StartDate
            bookmarks.key = "StartDate";
            bookmarks.value = txtStartDate.Text;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //BegBalance
            bookmarks.key = "BegBalance";
            bookmarks.value = BeginBalance;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //EndDate
            bookmarks.key = "EndDate";
            bookmarks.value = txtEndDate.Text;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //EndBalance
            bookmarks.key = "EndBalance";
            bookmarks.value = EndBalance;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


            string _htmll_table = "";
            for (int i = 0; i < dtSources.Rows.Count; i++)
            {
                string _source = Convert.ToString(dtSources.Rows[i]["Source"]);
                string _description = Convert.ToString(dtSources.Rows[i]["T_DESCRIPTION"]);

                var dataRow = from lls in dtList.AsEnumerable()
                              where lls.Field<string>("Source") == _source
                              select lls;
                DataTable dtListDetail = dataRow.CopyToDataTable<DataRow>();

                //table html to bookmark
                string cashJournalTempDataHtml = File.ReadAllText(Server.MapPath(@"~/DocumentTemplates/templates/GetCashJournalTempDataTable.txt"));
                string cashJournalTempDataBodyTable = "";
                foreach (DataRow dr in dtListDetail.Rows)
                {
                    cashJournalTempDataBodyTable += $@"<tr>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["TranDate"])}</td>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["Addl_Text"])}</td>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["T_DESCRIPTION"])}</td>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["TranID"])}</td>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["DT_Amount"])}</td>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["CT_Amount"])}</td>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["Description"])}</td>
                    </tr>";
                }

                if (cashJournalTempDataBodyTable != "") //fill footer
                {
                    var DT_Amount_Sum = dtListDetail.AsEnumerable()
                       .Sum(x => x.Field<decimal>("DT_Amount"));

                    var CT_Amount_Sum = dtListDetail.AsEnumerable()
                       .Sum(x => x.Field<decimal>("CT_Amount"));

                    cashJournalTempDataBodyTable += $@"<tr>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">YEKUN</td>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in""></td>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in""></td>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in""></td>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{DT_Amount_Sum}</td>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{CT_Amount_Sum}</td>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in""></td>
                                                      </tr>";
                }
                string _cashJournalTempDataHtml = cashJournalTempDataHtml
                                                 .Replace("{0}", _description)
                                                 .Replace("{1}", cashJournalTempDataBodyTable);

                _htmll_table += _cashJournalTempDataHtml;
            }

            string _htmlGeneral = "<head><style>table, td, th {  border: 1px solid black; text-align: left;font-size:14px;}table {border-collapse: collapse;width: 100%;} </style></head><body>{0}</body>";

            _htmlGeneral = _htmlGeneral.Replace("{0}", _htmll_table);

            bookmarks.key = "cashTracsactionTabel";
            bookmarks.value = _htmlGeneral;
            fillTemplateWord(bookmarks, WordBookmarkType.HTML, doc);



            ((Microsoft.Office.Interop.Word._Document)doc).Close();
            ((Microsoft.Office.Interop.Word._Application)app).Quit();

            string pdf_file_name = f_o_name.Replace(".docx", ".pdf");

            Word2Pdf objWordPDF = new Word2Pdf();
            objWordPDF.InputLocation = f_o_name;
            objWordPDF.OutputLocation = pdf_file_name;
            objWordPDF.Word2PdfCOnversion();


            System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            response.Clear();
            response.Buffer = true;
            response.ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            response.AddHeader("Content-Disposition", "attachment;filename=" + System.IO.Path.GetFileName(pdf_file_name));
            response.WriteFile(pdf_file_name);
            response.Flush();
            System.IO.File.Delete(f_o_name);
            System.IO.File.Delete(pdf_file_name);
            response.End();

        }


        protected void lnkSendMailData_Click(object sender, EventArgs e)
        {
            int rowIndex = ((sender as LinkButton).NamingContainer as GridViewRow).RowIndex;
            int branchID = Convert.ToInt32(grdBranchGroupData.DataKeys[rowIndex].Values["BranchID"]);
            int currencyId = Convert.ToInt32(grdBranchGroupData.DataKeys[rowIndex].Values["CurrencyId"]);

            int ACNT_ID = Convert.ToInt32(grdBranchGroupData.DataKeys[rowIndex].Values["ACNT_ID"]);

            decimal BeginBalance = _config.ToDecimal(grdBranchGroupData.DataKeys[rowIndex].Values["BeginBalance"]);
            decimal EndBalance = _config.ToDecimal(grdBranchGroupData.DataKeys[rowIndex].Values["EndBalance"]);
            string BranchName = Convert.ToString(grdBranchGroupData.DataKeys[rowIndex].Values["BranchName"]);



            wsTemplateController.TempCashJournalRequest req = new wsTemplateController.TempCashJournalRequest();
            req.ACNT_ID = ACNT_ID;
            req.BranchID = branchID;
            req.CurrencyID = currencyId;
            req.DateFrom = txtStartDate.Text;
            req.DateTo = txtEndDate.Text;
            req.UserID = UserSession.UserId;
            req.Source = "";
            DataTable dtList = wsTeController.GetCashJournalTempData(req);

            if (dtList == null)
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi!");
                return;
            }

            if (dtList.Rows.Count == 0)
            {
                _config.AlertMessage(this, MessageType.WARNING, "Seçilmiş kriteriyalara görə əməliyyat tapılmadı");
                return;
            }

            DataView dv = new DataView(dtList);
            DataTable dtSources = dv.ToTable(true, "Source", "T_DESCRIPTION");

            string f_t_name = Server.MapPath(@"~/DocumentTemplates/templates/GetCashJournalTempData.docx");
            string f_o_name = Server.MapPath("~/DocumentTemplates/" + "GetCashJournalTempData" + Guid.NewGuid().ToString() + ".docx");

            // copy file from template
            System.IO.File.Copy(f_t_name, f_o_name);


            Microsoft.Office.Interop.Word.Application app = new Microsoft.Office.Interop.Word.Application();
            Microsoft.Office.Interop.Word.Document doc = app.Documents.Open(f_o_name);

            bookMarks bookmarks = new bookMarks();

            //PrintDate
            bookmarks.key = "PrintDate";
            bookmarks.value = _Config.HostingTime.ToString("dd.MM.yyyy HH:mm");
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //BranchName
            bookmarks.key = "BranchName";
            bookmarks.value = BranchName;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //StartDate
            bookmarks.key = "StartDate";
            bookmarks.value = txtStartDate.Text;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //BegBalance
            bookmarks.key = "BegBalance";
            bookmarks.value = BeginBalance;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //EndDate
            bookmarks.key = "EndDate";
            bookmarks.value = txtEndDate.Text;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);

            //EndBalance
            bookmarks.key = "EndBalance";
            bookmarks.value = EndBalance;
            fillTemplateWord(bookmarks, WordBookmarkType.TXT, doc);


            string _htmll_table = "";
            for (int i = 0; i < dtSources.Rows.Count; i++)
            {
                string _source = Convert.ToString(dtSources.Rows[i]["Source"]);
                string _description = Convert.ToString(dtSources.Rows[i]["T_DESCRIPTION"]);

                var dataRow = from lls in dtList.AsEnumerable()
                              where lls.Field<string>("Source") == _source
                              select lls;
                DataTable dtListDetail = dataRow.CopyToDataTable<DataRow>();

                //table html to bookmark
                string cashJournalTempDataHtml = File.ReadAllText(Server.MapPath(@"~/DocumentTemplates/templates/GetCashJournalTempDataTable.txt"));
                string cashJournalTempDataBodyTable = "";
                foreach (DataRow dr in dtListDetail.Rows)
                {
                    cashJournalTempDataBodyTable += $@"<tr>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["TranDate"])}</td>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["Addl_Text"])}</td>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["T_DESCRIPTION"])}</td>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["TranID"])}</td>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["DT_Amount"])}</td>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["CT_Amount"])}</td>
                    <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{Convert.ToString(dr["Description"])}</td>
                    </tr>";
                }

                if (cashJournalTempDataBodyTable != "") //fill footer
                {
                    var DT_Amount_Sum = dtListDetail.AsEnumerable()
                       .Sum(x => x.Field<decimal>("DT_Amount"));

                    var CT_Amount_Sum = dtListDetail.AsEnumerable()
                       .Sum(x => x.Field<decimal>("CT_Amount"));

                    cashJournalTempDataBodyTable += $@"<tr>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">YEKUN</td>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in""></td>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in""></td>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in""></td>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{DT_Amount_Sum}</td>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in"">{CT_Amount_Sum}</td>
                                                      <td STYLE=""border: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0.08in""></td>
                                                      </tr>";
                }
                string _cashJournalTempDataHtml = cashJournalTempDataHtml
                                                 .Replace("{0}", _description)
                                                 .Replace("{1}", cashJournalTempDataBodyTable);

                _htmll_table += _cashJournalTempDataHtml;
            }

            string _htmlGeneral = "<head><style>table, td, th {  border: 1px solid black; text-align: left;font-size:14px;}table {border-collapse: collapse;width: 100%;} </style></head><body>{0}</body>";

            _htmlGeneral = _htmlGeneral.Replace("{0}", _htmll_table);

            bookmarks.key = "cashTracsactionTabel";
            bookmarks.value = _htmlGeneral;
            fillTemplateWord(bookmarks, WordBookmarkType.HTML, doc);



            ((Microsoft.Office.Interop.Word._Document)doc).Close();
            ((Microsoft.Office.Interop.Word._Application)app).Quit();

            string pdf_file_name = f_o_name.Replace(".docx", ".pdf");

            Word2Pdf objWordPDF = new Word2Pdf();
            objWordPDF.InputLocation = f_o_name;
            objWordPDF.OutputLocation = pdf_file_name;
            objWordPDF.Word2PdfCOnversion();

            /*send mail*/
            wsUserController.GetBranchFullDataByIDRequest branchReq = new wsUserController.GetBranchFullDataByIDRequest();
            branchReq.BranchID = branchID;
            wsUserController.GetBranchFullDataByIDResponse resBranch = wsUController.GetBranchFullDataByID(branchReq);

            string mail_result = _config.SendMailWithAttachment(resBranch.Branch_Mail, _config.GetAppSetting("destinationMails_1"), resBranch.Branch_Mail_Password, "Bank hesabı", "", new string[] { pdf_file_name });
            if (!mail_result.Equals("OK"))
            {
                _config.AlertMessage(this, MessageType.ERROR, "Xəta baş verdi. Mail: " + mail_result);
                return;
            }
            else
            {
                _config.AlertMessage(this, MessageType.SUCCESS, "Mail göndərildi");
            }

            /*--end send mail*/
            try
            {
                System.IO.File.Delete(f_o_name);
                System.IO.File.Delete(pdf_file_name);
            }
            catch
            {
                
            }

        }










        // Begin word template

        public static string SaveToTemporaryFile(string html)
        {
            string htmlTempFilePath = Path.Combine(Path.GetTempPath(), string.Format("{0}.html", Path.GetRandomFileName()));
            using (StreamWriter writer = File.CreateText(htmlTempFilePath))
            {
                html = string.Format("<html>{0}</html>", html);

                writer.WriteLine(html);
            }

            return htmlTempFilePath;
        }

        enum WordBookmarkType
        {
            HTML,
            TXT
        }


        private void ReplaceBookmarkText(Microsoft.Office.Interop.Word.Document doc, string bookmarkName, string text, WordBookmarkType type)
        {
            try
            {
                if (doc.Bookmarks.Exists(bookmarkName))
                {

                    Object name = bookmarkName;
                    Microsoft.Office.Interop.Word.Range range =
                        doc.Bookmarks.get_Item(ref name).Range;

                    range.Text = range.Text.Replace(range.Text, text);
                    object newRange = range;
                    if (type == WordBookmarkType.HTML)
                    {
                        range.InsertFile(SaveToTemporaryFile(text), Type.Missing, Type.Missing, Type.Missing, Type.Missing);

                    }
                    doc.Bookmarks.Add(bookmarkName, ref newRange);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        private void fillTemplateWord(bookMarks bookmarks, WordBookmarkType wordBookmarkType, Microsoft.Office.Interop.Word.Document doc)
        {
            try
            {
                ReplaceBookmarkText(doc, bookmarks.key.ToString(), bookmarks.value.ToString(), wordBookmarkType);
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
        }

        // End word template
    }

    public class bookMarks
    {
        public object key { get; set; }
        public object value { get; set; }
    }
}