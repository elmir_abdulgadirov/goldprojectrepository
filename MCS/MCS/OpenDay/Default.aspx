﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MCS.OpenDay.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">OPEN DAY</h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        Filial:
                        <asp:DropDownList ID="drlBranch" class="form-control" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        Tarix:
                        <div class="input-group">
                            <asp:TextBox ID="txtDate" autocomplete="off" class="form-control datepicker" placeHolder="dd.mm.yyyy" runat="server"></asp:TextBox>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group" style="text-align: right">
                        <br />
                        <img id="post_open_day" style="display: none" src="../img/loader1.gif" />
                        <asp:Button ID="btnOpenDay" class="btn btn-primary" runat="server" Text="Start"
                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('post_open_day').style.display = '';" />
                    </div>
                </div>

            </div>
        </div>
    </div>
</asp:Content>
