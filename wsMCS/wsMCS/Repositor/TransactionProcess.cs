﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using wsMCS.App_Code;
using apiMCS.App_Code;


namespace wsMCS.Repositor
{
    public class TransactionProcess
    {
        public Model.ResponseDTO.createTranResponse createTran(Model.RequestDTO.createTranRequest tranRequest)
        {
            Model.ResponseDTO.createTranResponse tranResponse = new Model.ResponseDTO.createTranResponse();
            tranResponse = CreateTransactions(tranRequest);
            return tranResponse;
        }
        wsMCS.Model.ResponseDTO.createTranResponse CreateTransactions(Model.RequestDTO.createTranRequest tranRequest)
        {
            Model.ResponseDTO.createTranResponse tranResponse = new Model.ResponseDTO.createTranResponse();
            using (SqlConnection connection = Utils.sqlConn)
            {
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("CreateTransactions");

                cmd.Connection = connection;
                cmd.Transaction = transaction;
                cmd.CommandText = "spCreateTransaction";
                try
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //INPUT
                    cmd.Parameters.AddWithValue("@pMsgID", tranRequest.MsgID);
                    cmd.Parameters.AddWithValue("@pDebet_Acnt_Id", tranRequest.Debet_Acnt_ID);
                    cmd.Parameters.AddWithValue("@pDebet_Amount", tranRequest.Debet_Amount);
                    cmd.Parameters.AddWithValue("@pDebet_CurrencyId", tranRequest.Debet_Acnt_CurrencyID);
                    cmd.Parameters.AddWithValue("@pCredit_Acnt_Id", tranRequest.Credit_Acnt_ID);
                    cmd.Parameters.AddWithValue("@pCredit_Amount", tranRequest.Credit_Amount);
                    cmd.Parameters.AddWithValue("@pCredit_CurrencyId", tranRequest.Credit_Acnt_CurrencyID);
                    cmd.Parameters.AddWithValue("@pTranDate", Config.HostingTime);
                    cmd.Parameters.AddWithValue("@pSource", tranRequest.Source);
                    cmd.Parameters.AddWithValue("@pDescription", tranRequest.Description);
                    cmd.Parameters.AddWithValue("@pAddlText", tranRequest.AddlText);
                    cmd.Parameters.AddWithValue("@pCreatedID", tranRequest.CreatedID);
                    cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;

                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    cmd.ExecuteNonQuery();
                    pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);

                    if (pResult == "OK")
                    {
                        tranResponse.isSuccess = true;
                        tranResponse.errorCode = "";
                        transaction.Commit();
                    }
                    else
                    {
                        tranResponse.isSuccess = false;
                        tranResponse.errorCode = pResult;
                        transaction.Rollback();
                    }
                }
                catch (Exception ex)
                {
                    tranResponse.isSuccess = false;
                    tranResponse.errorCode = ex.Message;
                    transaction.Rollback();
                }
            }
            return tranResponse;
        }
        public Model.ResponseDTO.reverseTranResponse reverseTran(Model.RequestDTO.reverseTranRequest reverseRequest)
        {
            Model.ResponseDTO.reverseTranResponse reverseResponse = new Model.ResponseDTO.reverseTranResponse();
            reverseResponse = dtreverseTran(reverseRequest);
            return reverseResponse;
        }
        wsMCS.Model.ResponseDTO.reverseTranResponse dtreverseTran(Model.RequestDTO.reverseTranRequest reverseRequest)
        {
            Model.ResponseDTO.reverseTranResponse reverseResponse = new Model.ResponseDTO.reverseTranResponse();
            SqlCommand cmd = new SqlCommand("spReverseTransaction", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pMsgID", reverseRequest.MsgID);
            cmd.Parameters.AddWithValue("@pDescription", reverseRequest.Description);
            cmd.Parameters.AddWithValue("@pModifiedID", reverseRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                reverseResponse.isSuccess = true;
                reverseResponse.errorCode = "";
            }
            else
            {
                reverseResponse.isSuccess = false;
                reverseResponse.errorCode = pResult;
            }
            return reverseResponse;
        }    
        public Model.ResponseDTO.SetResponse dbCreatePaymentByLoanID(Model.RequestDTO.CreatePaymentByLoanIDRequest paymentRequest)
        {
            Model.ResponseDTO.SetResponse paymentResponse = new Model.ResponseDTO.SetResponse();
            paymentResponse = dbCreatePayment(paymentRequest);
            return paymentResponse;
        }
        wsMCS.Model.ResponseDTO.SetResponse dbCreatePayment(Model.RequestDTO.CreatePaymentByLoanIDRequest paymentRequest)
        {
            wsMCS.Model.ResponseDTO.SetResponse paymentResponse = new Model.ResponseDTO.SetResponse();
            SqlCommand cmd = new SqlCommand("spCreateLoanPayment", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pLoanID", paymentRequest.LoanID);
            cmd.Parameters.AddWithValue("@pTranDate", Config.HostingTime);
            cmd.Parameters.AddWithValue("@pMonthCount", paymentRequest.MonthCount);
            cmd.Parameters.AddWithValue("@pPaymentType", paymentRequest.PaymentType);
            cmd.Parameters.AddWithValue("@pPayedPrincipalAmount", paymentRequest.PayedPrincipalAmount);
            cmd.Parameters.AddWithValue("@pPayedExpPrincipalAmount", paymentRequest.PayedExpPrincipalAmount);
            cmd.Parameters.AddWithValue("@pPayedRateAmount", paymentRequest.PayedRateAmount);
            cmd.Parameters.AddWithValue("@pPayedExpRateAmount", paymentRequest.PayedExpRateAmount);
            cmd.Parameters.AddWithValue("@pPayedPenaltyAmount", paymentRequest.PayedPenaltyAmount);
            cmd.Parameters.AddWithValue("@pNote", paymentRequest.Note);
            cmd.Parameters.AddWithValue("@pCreatedID", paymentRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }
            if (pResult == "OK")
            {
                paymentResponse.isSuccess = true;
                paymentResponse.errorCode = "";
            }
            else
            {
                paymentResponse.isSuccess = false;
                paymentResponse.errorCode = pResult;
            }
            return paymentResponse;
        }
        public DataTable dtTranSourceMaster()
        {
            DataTable dt = new DataTable("TransactionSourseMaster");
            SqlDataAdapter da = new SqlDataAdapter(@"Select * from transactionSourceMaster WHERE Oper1_IsFilter = 'YES' 
                                                              ORDER BY T_DESCRIPTION ", Utils.sqlConn);
            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                dt = null;
            }
            return dt;
        }
        public Model.ResponseDTO.SetResponse CreatCashBankTran(Model.RequestDTO.CreateCashBankTransactionRequest tranRequest)
        {
            Model.ResponseDTO.SetResponse cashBankResponse = new Model.ResponseDTO.SetResponse();
            cashBankResponse = dbCashBankTran(tranRequest);
            return cashBankResponse;
        }
        wsMCS.Model.ResponseDTO.SetResponse dbCashBankTran(Model.RequestDTO.CreateCashBankTransactionRequest tranRequest)
        {

            wsMCS.Model.ResponseDTO.SetResponse cashBankResponse = new Model.ResponseDTO.SetResponse();

            SqlCommand cmd = new SqlCommand("spCreateCashBankTransaction", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pValueDate", tranRequest.ValueDate);
            cmd.Parameters.AddWithValue("@pResponsibilityPerson", tranRequest.ResponsibilityPerson);
            cmd.Parameters.AddWithValue("@pAssignment", tranRequest.Assignment);
            cmd.Parameters.AddWithValue("@pAmount", tranRequest.Amount);
            cmd.Parameters.AddWithValue("@pNote", tranRequest.Note);
            cmd.Parameters.AddWithValue("@pTranType", tranRequest.TranType);
            cmd.Parameters.AddWithValue("@pOperationType", tranRequest.OperationType);
            cmd.Parameters.AddWithValue("@pCashBankAcntID", tranRequest.CashBankAcntID);
            cmd.Parameters.AddWithValue("@pCorresp_AcntID", tranRequest.Corresp_AcntID);
            cmd.Parameters.AddWithValue("@pCreatedID", tranRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                cashBankResponse.isSuccess = true;
                cashBankResponse.errorCode = "";
            }
            else
            {
                cashBankResponse.isSuccess = false;
                cashBankResponse.errorCode = pResult;
            }

            return cashBankResponse;
        }
        public Model.ResponseDTO.SetResponse DeleteCashBankTranByID(Model.RequestDTO.DeleteCashBankTransactionRequest dataRequest)
        {
            Model.ResponseDTO.SetResponse deleteResponse = new Model.ResponseDTO.SetResponse();
            deleteResponse = dtDeleteCashBankTranByID(dataRequest);
            return deleteResponse;
        }
        wsMCS.Model.ResponseDTO.SetResponse dtDeleteCashBankTranByID(Model.RequestDTO.DeleteCashBankTransactionRequest dataRequest)
        {
            Model.ResponseDTO.SetResponse deleteResponse = new Model.ResponseDTO.SetResponse();
            SqlCommand cmd = new SqlCommand("spDeleteCashBankTrancsaction", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pMsgID", dataRequest.MsgID);
            cmd.Parameters.AddWithValue("@pDescription", dataRequest.Description);
            cmd.Parameters.AddWithValue("@pModifiedID", dataRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar, 100);
            paramResult.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(paramResult);
            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }
            if (pResult == "OK")
            {
                deleteResponse.isSuccess = true;
                deleteResponse.errorCode = "";
            }
            else
            {
                deleteResponse.isSuccess = false;
                deleteResponse.errorCode = pResult;
            }
            return deleteResponse;
        }
        public Model.ResponseDTO.SetResponse CreateSpecialTran(Model.RequestDTO.CreateSpecialTransactionRequest tranRequest)
        {
            Model.ResponseDTO.SetResponse specialTranResponse = new Model.ResponseDTO.SetResponse();
            specialTranResponse = dbSpecialTran(tranRequest);
            return specialTranResponse;
        }
        wsMCS.Model.ResponseDTO.SetResponse dbSpecialTran(Model.RequestDTO.CreateSpecialTransactionRequest tranRequest)
        {

            wsMCS.Model.ResponseDTO.SetResponse specialTransactionResponse = new Model.ResponseDTO.SetResponse();

            SqlCommand cmd = new SqlCommand("spCreateSpecialTransaction", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pValueDate", tranRequest.ValueDate);
            cmd.Parameters.AddWithValue("@pDTAccountID", tranRequest.DTAccountID);
            cmd.Parameters.AddWithValue("@pCTAccountID", tranRequest.CTAccountID);
            cmd.Parameters.AddWithValue("@pSourceKey", tranRequest.SourceKey);
            cmd.Parameters.AddWithValue("@pAmount", tranRequest.Amount);
            cmd.Parameters.AddWithValue("@pNote", tranRequest.Note);
            cmd.Parameters.AddWithValue("@pCreatedID", tranRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                specialTransactionResponse.isSuccess = true;
                specialTransactionResponse.errorCode = "";
            }
            else
            {
                specialTransactionResponse.isSuccess = false;
                specialTransactionResponse.errorCode = pResult;
            }

            return specialTransactionResponse;
        }
        public DataTable dtGetSpecialTranList(Model.RequestDTO.GetSpecialTranListRequest dataRequest)
        {
            DataTable dtSpecialTranList = new DataTable("SpecialTranList");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetSpecialTranList", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pDateFrom", dataRequest.DateFrom);
                    cmd.Parameters.AddWithValue("@pDateTo", dataRequest.DateTo);
                    cmd.Parameters.AddWithValue("@pUserID", dataRequest.UserID);
                    cmd.Parameters.AddWithValue("@pBranchID", dataRequest.BranchID);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtSpecialTranList.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        pResult = error;
                    }
                    return dtSpecialTranList;
                }
            }
        }
        public DataTable dtGetGeneralTranList(Model.RequestDTO.GetGeneralTranListRequest dataRequest)
        {
            DataTable dtGeneralTranList = new DataTable("GeneralTranList");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetGeneralTranList", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pDateFrom", dataRequest.DateFrom);
                    cmd.Parameters.AddWithValue("@pDateTo", dataRequest.DateTo);
                    cmd.Parameters.AddWithValue("@pUserID", dataRequest.UserID);
                    cmd.Parameters.AddWithValue("@pBranchID", dataRequest.BranchID);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtGeneralTranList.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        pResult = error;
                    }
                    return dtGeneralTranList;
                }
            }
        }
    }
}