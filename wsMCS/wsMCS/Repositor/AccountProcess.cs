﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using wsMCS.App_Code;
using apiMCS.App_Code;

namespace wsMCS.Repositor
{
    public class AccountProcess
    {
        public DataTable dtAccountList()
        {
            DataTable dt = new DataTable("AccountList");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM  AccountTypesList ORDER BY AccountName", Utils.sqlConn);
            
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }

        public Model.ResponseDTO.AddAccountResponse AddAccount(Model.RequestDTO.AddAccountRequest AccountRequest)
        {
            Model.ResponseDTO.AddAccountResponse AccountResponse = new Model.ResponseDTO.AddAccountResponse();
            AccountResponse = dbAddAccount(AccountRequest);
            return AccountResponse;
        }
        wsMCS.Model.ResponseDTO.AddAccountResponse dbAddAccount(Model.RequestDTO.AddAccountRequest AccountRequest)
        {

            wsMCS.Model.ResponseDTO.AddAccountResponse AccountResponse = new Model.ResponseDTO.AddAccountResponse();

            SqlCommand cmd = new SqlCommand("spAddCustomerCurrentAccount", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pCustomerID", AccountRequest.CustomerID);
            cmd.Parameters.AddWithValue("@pAcntCurrencyID", AccountRequest.AcntCurrencyID);
            cmd.Parameters.AddWithValue("@pAcntName", AccountRequest.AcntName);
            cmd.Parameters.AddWithValue("@pCreatedID", AccountRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                AccountResponse.isSuccess = true;
                AccountResponse.errorCode = "";
            }
            else
            {
                AccountResponse.isSuccess = false;
                AccountResponse.errorCode = pResult;
            }

            return AccountResponse;
        }
        public Model.ResponseDTO.DeleteAccountResponse DeleteAccount(Model.RequestDTO.DeleteAccountRequest AccountRequest)
        {
            Model.ResponseDTO.DeleteAccountResponse AccountResponse = new Model.ResponseDTO.DeleteAccountResponse();
            AccountResponse = dtDeleteAccount(AccountRequest);
            return AccountResponse;
        }
        wsMCS.Model.ResponseDTO.DeleteAccountResponse dtDeleteAccount(Model.RequestDTO.DeleteAccountRequest AccountRequest)
        {
            Model.ResponseDTO.DeleteAccountResponse AccountResponse = new Model.ResponseDTO.DeleteAccountResponse();
            SqlCommand cmd = new SqlCommand("spDeleteAccount", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pACNT_ID", AccountRequest.AcntID);
            cmd.Parameters.AddWithValue("@pModifiedID", AccountRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate",Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                AccountResponse.isSuccess = true;
                AccountResponse.errorCode = "";
            }
            else
            {
                AccountResponse.isSuccess = false;
                AccountResponse.errorCode = pResult;
            }

            return AccountResponse;
        }

        public DataTable GetCustomerCurrAcnts(Model.RequestDTO.GetCustAcntsRequest getCustAcntsRequest,out int RecorCount)
        {
            RecorCount = 0;
            string sql_query = @"select tab.* from (
                                SELECT   ROW_NUMBER() OVER (ORDER BY ACNT_ID DESC) AS RowNumber, A.*,B.BranchName
                                FROM     Accounts A left join Customer C on A.customer_id = c.CustomerID
                                                    left join Branch B on B.BranchID = C.BranchID
                                WHERE    
                                A.ACNT_TYPE = 'CUST_CURRENT_ACNT' AND A.Status = 'ACTIVE' AND
                                B.BranchCode in (select BranchCode from UserBranch where UserID = @UserId) 
                                {0}
                                ) tab
                                where  (tab.RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)";

            string sql_sub_query = " ";
            if (getCustAcntsRequest.AcntName != "")
            {
                sql_sub_query += " AND upper(replace(A.ACNT_NAME,N'i',N'İ')) like  @ACNT_NAME";
            }

           string _sql = string.Format(sql_query, sql_sub_query).Trim();

            SqlDataAdapter da = new SqlDataAdapter(_sql, Utils.sqlConn);

            da.SelectCommand.Parameters.AddWithValue("@UserId", getCustAcntsRequest.UserId);
            if (getCustAcntsRequest.AcntName != "")
            {
                da.SelectCommand.Parameters.Add("@ACNT_NAME", SqlDbType.NVarChar, 32767).Value = "%" + getCustAcntsRequest.AcntName.Replace("i", "İ").ToUpper() + "%";
            }
            da.SelectCommand.Parameters.AddWithValue("@PageIndex", getCustAcntsRequest.pageIndex);
            da.SelectCommand.Parameters.AddWithValue("@PageSize", getCustAcntsRequest.pageSize);
            DataTable dtAccount = new DataTable("GetCustomerCurrAcnts");
            try
            {
                da.Fill(dtAccount);
            }
            catch
            {
                return null;
            }
            RecorCount = CountCustomerCurrAcnts(getCustAcntsRequest);


            return dtAccount;
        }

        private int CountCustomerCurrAcnts(Model.RequestDTO.GetCustAcntsRequest getCustAcntsRequest)
        {
            string sql_query = @"select Count(1) as Cntt from (
                                SELECT   ROW_NUMBER() OVER (ORDER BY ACNT_ID DESC) AS RowNumber, A.*,B.BranchName
                                FROM     Accounts A left join Customer C on A.customer_id = c.CustomerID
                                                    left join Branch B on B.BranchID = C.BranchID
                                WHERE    
                                A.ACNT_TYPE = 'CUST_CURRENT_ACNT' AND A.Status = 'ACTIVE' AND
                                B.BranchCode in (select BranchCode from UserBranch where UserID = @UserId) 
                                {0}
                                ) tab";

            string sql_sub_query = " ";
            if (getCustAcntsRequest.AcntName != "")
            {
                sql_sub_query += " AND upper(replace(A.ACNT_NAME,N'i',N'İ')) like  @ACNT_NAME";
            }

            string _sql = string.Format(sql_query, sql_sub_query).Trim();
            SqlDataAdapter da = new SqlDataAdapter(_sql, Utils.sqlConn);

            da.SelectCommand.Parameters.AddWithValue("@UserId", getCustAcntsRequest.UserId);
            if (getCustAcntsRequest.AcntName != "")
            {
                da.SelectCommand.Parameters.Add("@ACNT_NAME", SqlDbType.NVarChar, 32767).Value = "%" + getCustAcntsRequest.AcntName.Replace("i", "İ").ToUpper() + "%";
            }
            DataTable dtAccount = new DataTable("GetCustomerCurrAcnts");
            try
            {
                da.Fill(dtAccount);
            }
            catch
            {
                return 0;
            }
            return Convert.ToInt32(dtAccount.Rows[0]["Cntt"]);
        }

        public DataTable SearchCustCurrAcnts(Model.RequestDTO.SearchCustCurrAcntsRequest searchCustCurrAcntsRequest)
        {
            DataTable dtAccount = new DataTable("SearchCustCurrAcnts");

            if (searchCustCurrAcntsRequest.AcntName.Trim() == "" && searchCustCurrAcntsRequest.CollaeralNumber.Trim() == "" && searchCustCurrAcntsRequest.AcntID == 0)
            {
                return null;
            }

            string sql_query = @"select distinct top 5  A.* , B.BranchName , Cy.CurrencyName from Accounts A inner join Customer C on A.CUSTOMER_ID = C.CustomerID
                                           inner join Loan L on L.CustomerID = C.CustomerID
										   inner join Branch B on B.BranchID = C.BranchID
										   inner join Currency Cy on A.ACNT_CURRENCY = Cy.CurrencyID
            where A.STATUS = 'ACTIVE' and  A.acnt_type = 'CUST_CURRENT_ACNT' And
            B.BranchCode in (select BranchCode from UserBranch where UserID = @UserId) 
            and upper(replace(A.ACNT_NAME,N'i',N'İ')) like  @ACNT_NAME
            and L.CollateralNumber like @CollateralNumber";

            string sql_sub_query = "";

            if (searchCustCurrAcntsRequest.AcntID != 0)
            {
                sql_sub_query += " and A.ACNT_ID = @AcntID";
            }

            sql_query = sql_query + sql_sub_query;

            SqlDataAdapter da = new SqlDataAdapter(sql_query, Utils.sqlConn);
            if (searchCustCurrAcntsRequest.AcntID != 0)
            {
                da.SelectCommand.Parameters.Add("@AcntID", SqlDbType.Int).Value = searchCustCurrAcntsRequest.AcntID;
            }
            da.SelectCommand.Parameters.AddWithValue("@UserId", searchCustCurrAcntsRequest.UserId);
            da.SelectCommand.Parameters.Add("@ACNT_NAME", SqlDbType.NVarChar, 32767).Value = "%" + searchCustCurrAcntsRequest.AcntName.Replace("i", "İ").ToUpper() + "%";
            da.SelectCommand.Parameters.Add("@CollateralNumber", SqlDbType.VarChar, 32767).Value = "%" + searchCustCurrAcntsRequest.CollaeralNumber + "%";
            try
            {
                da.Fill(dtAccount);
            }
            catch(Exception ex)
            {
                return null;
            }
            return dtAccount;
        }
        public DataTable getDebetAcnt(Model.RequestDTO.GetDebetAcntsByCurrAcntRequest acntRequest)
        {
            DataTable dtAccount = new DataTable("DebetAcnts");
            string sql_query = @"select A.ACNT_ID , A.ACNT_NAME + '(' + A.ACNT_CODE + ')' as ACNT_NAME  from Accounts A  
                               where A.ACNT_TYPE in ('CASH_ACNT','CARD_ACNT') and A.STATUS = 'ACTIVE'
                               and branch_id = @BranchID and ACNT_CURRENCY = @CurrencyID ";

            SqlDataAdapter da = new SqlDataAdapter(sql_query, Utils.sqlConn);
           
            da.SelectCommand.Parameters.Add("@BranchID", acntRequest.BranchID);
            da.SelectCommand.Parameters.Add("@CurrencyID", acntRequest.CurrencyID);
            try
            {
                da.Fill(dtAccount);
            }
            catch (Exception ex)
            {
                return null;
            }
            return dtAccount;
        }
        public DataTable GetCashList(Model.RequestDTO.GetCashTransactiontRequest getCashRequest, out DataTable accountBalance)
        {
            accountBalance = null;
            string sql_query = @"select BranchID, BranchName, BranchTaxID, ID, ACNT_ID, TranDate1, TranDate, Addl_Text, Source, T_DESCRIPTION,
                                 CurrencyId, AcntDescription, CurrencyName, DT_Amount, CT_Amount, Description, CreatedUser, AcntDescription AS Account 
                                from vTransactions where ACNT_ID in (select ACNT_ID from Accounts A 
                                 where A.ACNT_TYPE = @pACNT_TYPE AND A.ACNT_CURRENCY = @pCurrencyID
                                 AND A.BRANCH_ID in (select BranchID from v_UserBranch where UserID = @pUserID)) 
                                 and cast(trandate as date) between TRY_PARSE(@pDateFrom as date using 'en-gb') 
                                 and TRY_PARSE(@pDateTo as date using 'en-gb') {0} ORDER BY ID DESC";

            string sql_sub_query = " ";

            if (getCashRequest.BranchID != 0)
            {
                sql_sub_query += " AND BranchID =  @BranchID";
            }
            if (getCashRequest.Source != "")
            {
                sql_sub_query += " AND Source =  @Source";
            }

            string _sql = string.Format(sql_query, sql_sub_query).Trim();

            SqlDataAdapter da = new SqlDataAdapter(_sql, Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@pUserID", getCashRequest.UserID);
            da.SelectCommand.Parameters.AddWithValue("@pACNT_TYPE", getCashRequest.ACNT_TYPE);
            da.SelectCommand.Parameters.AddWithValue("@pCurrencyID", getCashRequest.CurrencyID);
            da.SelectCommand.Parameters.AddWithValue("@pDateFrom", getCashRequest.DateFrom);
            da.SelectCommand.Parameters.AddWithValue("@pDateTo", getCashRequest.DateTo);
            if (getCashRequest.BranchID != 0)
            {
                da.SelectCommand.Parameters.Add("@BranchID", SqlDbType.Int).Value = getCashRequest.BranchID;
            }

            if (getCashRequest.Source != "")
            {
                da.SelectCommand.Parameters.Add("@Source", SqlDbType.VarChar, 32767).Value = getCashRequest.Source;
            }

            DataTable dtAccount = new DataTable("GetCashList");
            try
            {
                da.Fill(dtAccount);
            }
            catch (Exception ex)
            {
                return null;
            }
            accountBalance = CashBalanceByBranch(getCashRequest);
            return dtAccount;
        }
        public DataTable CashBalanceByBranch(Model.RequestDTO.GetCashTransactiontRequest CashRequest)
        {
            DataTable dtCash = new DataTable("CashBalance");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetCashBalance", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pUserID", CashRequest.UserID);
                    cmd.Parameters.AddWithValue("@pBranchID", CashRequest.BranchID);
                    cmd.Parameters.AddWithValue("@pACNT_TYPE", CashRequest.ACNT_TYPE);
                    cmd.Parameters.AddWithValue("@pCurrencyID", CashRequest.CurrencyID);
                    cmd.Parameters.AddWithValue("@pDateFrom", CashRequest.DateFrom);
                    cmd.Parameters.AddWithValue("@pDateTo", CashRequest.DateTo);


                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtCash.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                    }
                }
            }
            return dtCash;
        }
        public DataTable dtAccountGroupList()
        {
            DataTable dt = new DataTable("AccountGroupList");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT Code, Code +' - '+ Name AS Name FROM AccountGroup ORDER BY Code", Utils.sqlConn);

            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable dtAccountSubGroupList(string AcntGroupCode)
        {
            DataTable dt = new DataTable("AccountSubGroupList");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT Code, Code +' - '+ Name AS Name FROM AccountSUBGroup 
                                                            WHERE AcntGroupCode = @AcntGroupCode", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@AcntGroupCode", AcntGroupCode);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable dtAccountSubSubGroupList(string AcntSubGroupCode)
        {
            DataTable dt = new DataTable("AccountSubSubGroupList");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT t1.AccountSUBGroupCode, t1.Code, t1.Code + '_' + CAST(t3.CurrencyID AS varchar) AS CodeAndCurrencyID,
                                                     t2.Valuta, t2.ACNT_TYPE, Code + ' - ' + t2.Description AS Description from  AccountSubSubGroup t1
                                                   INNER JOIN AccountMaster t2 ON t1.Code = t2.ACNT_CODE
												   INNER JOIN Currency t3 ON t2.VALUTA = t3.CurrencyName
                                                   WHERE AccountSubGroupCode = @AcntSubGroupCode ", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@AcntSubGroupCode", AcntSubGroupCode);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable dtAcntDataList(string AcntCode, int UserID, int CurrencyID)
        {
            DataTable dt = new DataTable("AcntDataListByAcntCode");
            SqlDataAdapter da = new SqlDataAdapter(@" SELECT A.ACNT_ID, A.ACNT_NAME, A.ACNT_NAME + ' - ' + A.ACNT_CODE + ' - '+ CAST(A.CURRENT_BALANCE AS varchar) +' - '+ C.CurrencyName AS ACNT_NAME1,
                   A.ACNT_TYPE, A.ACNT_CODE, A.ACNT_CURRENCY AS CurrencyID, A.CURRENT_BALANCE from Accounts A INNER JOIN Currency C ON A.ACNT_CURRENCY = C.CurrencyID
                    where ACNT_TYPE = (SELECT ACNT_TYPE FROM AccountMaster WHERE ACNT_CODE = @AcntCode) 
						AND BRANCH_ID in (select BranchID from v_UserBranch where UserId = @UserID) 
						AND A.ACNT_CURRENCY = @CurrencyID AND A.Status = 'ACTIVE'
                        ORDER BY ACNT_NAME ", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@AcntCode", AcntCode);
            da.SelectCommand.Parameters.AddWithValue("@UserID", UserID);
            da.SelectCommand.Parameters.AddWithValue("@CurrencyID", CurrencyID);
         
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable CashBankDataList(Model.RequestDTO.GetCashBankRequest dataRequest)
        {
            DataTable dtCash = new DataTable("CashBankDataList");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetCashBankDataList", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pUserID", dataRequest.UserID);
                    cmd.Parameters.AddWithValue("@OperType", dataRequest.OPER_TYPE);
                    cmd.Parameters.AddWithValue("@pDateFrom", dataRequest.DateFrom);
                    cmd.Parameters.AddWithValue("@pDateTo", dataRequest.DateTo);


                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtCash.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        pResult = error;
                    }
                }
            }
            return dtCash;
        }
        public DataTable dtCashBankAcntList(int UserID, string AcntType)
        {
            DataTable dt = new DataTable("CashBankAccountList");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT ACNT_ID, ACNT_NAME + ' - ' + ACNT_CODE + ' - ' + Cast(CURRENT_BALANCE as varchar) AS AcntData FROM Accounts 
                                                    WHERE ACNT_TYPE = @Acnt_Type AND STATUS = 'ACTIVE'
                                                          AND BRANCH_ID IN (SELECT v.BranchID FROM v_UserBranch v WHERE v.UserID = @UserID)", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@Acnt_Type", AcntType);
            da.SelectCommand.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable PortfolioList(Model.RequestDTO.GetPortfolioListRequest portfolioRequest, out DataTable portfoliotBalance)
        {
            portfoliotBalance = null;
            DataTable dtPortfolioList = new DataTable("PortfolioList");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetPortfolioList", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pUserID", portfolioRequest.UserID);
                    cmd.Parameters.AddWithValue("@pBranchID", portfolioRequest.BranchID);                 
                    cmd.Parameters.AddWithValue("@pCurrencyID", portfolioRequest.CurrencyID);
                    cmd.Parameters.AddWithValue("@pDateFrom", portfolioRequest.DateFrom);
                    cmd.Parameters.AddWithValue("@pDateTo", portfolioRequest.DateTo);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtPortfolioList.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                    }
                }
            }
            portfoliotBalance = PortfolioBalanceByBranch(portfolioRequest);
            return dtPortfolioList;
        }
        public DataTable PortfolioBalanceByBranch(Model.RequestDTO.GetPortfolioListRequest portfolioRequest)
        {
            DataTable dtPortfolioBalance = new DataTable("PortfolioBalance");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetPortfoliBalance", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pUserID", portfolioRequest.UserID);
                    cmd.Parameters.AddWithValue("@pBranchID", portfolioRequest.BranchID);
                    cmd.Parameters.AddWithValue("@pCurrencyID", portfolioRequest.CurrencyID);
                    cmd.Parameters.AddWithValue("@pDateFrom", portfolioRequest.DateFrom);
                    cmd.Parameters.AddWithValue("@pDateTo", portfolioRequest.DateTo);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtPortfolioBalance.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                    }
                }
            }
            return dtPortfolioBalance;
        }
        public DataTable GetAcntGroupedBalanceList(Model.RequestDTO.GetAcntGroupedBalanceRequest dataRequest)
        {
            DataTable dtAcntGroupedBalance = new DataTable("AcntGroupedBalance");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetAcntGroupedBalance", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pStartDate", dataRequest.DateFrom);
                    cmd.Parameters.AddWithValue("@pEndDate", dataRequest.DateTo);
                    cmd.Parameters.AddWithValue("@pAcntGroupCode", dataRequest.AcntGroupCode);
                    cmd.Parameters.AddWithValue("@pCurrenyID", dataRequest.CurrencyID);
                    cmd.Parameters.AddWithValue("@pBranchID", dataRequest.BranchID);
                    cmd.Parameters.AddWithValue("@pUserID", dataRequest.UserID);
                    cmd.Parameters.AddWithValue("@pAcntSubGroupCode", dataRequest.AcntSubGroupCode);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandTimeout = 9999999;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtAcntGroupedBalance.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        pResult = error;
                    }
                }
            }
            return dtAcntGroupedBalance;
        }
        public DataTable GetAcntBalanceBySubGroupList(Model.RequestDTO.GetAcntBalanceBySubGroupRequest dataRequest)
        {
            DataTable dtGetAcntBalanceBySubGroup = new DataTable("GetAcntBalanceBySubGroup");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetAcntBalanceBySubGroup", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pStartDate", dataRequest.DateFrom);
                    cmd.Parameters.AddWithValue("@pEndDate", dataRequest.DateTo);
                    cmd.Parameters.AddWithValue("@pAcntSubGroupCode", dataRequest.AcntSubGroupCode);
                    cmd.Parameters.AddWithValue("@pCurrenyID", dataRequest.CurrencyID);
                    cmd.Parameters.AddWithValue("@pBranchID", dataRequest.BranchID);
                    cmd.Parameters.AddWithValue("@pUserID", dataRequest.UserID);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandTimeout = 9999999;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtGetAcntBalanceBySubGroup.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        pResult = error;
                    }
                }
            }
            return dtGetAcntBalanceBySubGroup;
        }
        public DataTable GetAcntTotalBalanceList(Model.RequestDTO.GetTotalBalanceRequest dataRequest)
        {
            DataTable dtGetAcntTotalBalanceList = new DataTable("GetAcntTotalBalanceList");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetAcntTotalBalanceByBranch", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pBranchID", dataRequest.BranchID);
                    cmd.Parameters.AddWithValue("@pUserID", dataRequest.UserID);
                    cmd.Parameters.AddWithValue("@pStartDate", dataRequest.DateFrom);
                    cmd.Parameters.AddWithValue("@pEndDate", dataRequest.DateTo);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandTimeout = 9999999;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtGetAcntTotalBalanceList.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        pResult = error;
                    }
                }
            }
            return dtGetAcntTotalBalanceList;
        }
        public DataTable AcntBalanceBySubSubCodeList(Model.RequestDTO.GetAcntBalanceBySubSubCodeRequest dataRequest)
        {
            DataTable dtAcntBalanceBySubSubCodeList = new DataTable("AcntBalanceBySubSubCodeList");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetAcntBalanceBySubSubCode", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pStartDate", dataRequest.DateFrom);
                    cmd.Parameters.AddWithValue("@pEndDate", dataRequest.DateTo);
                    cmd.Parameters.AddWithValue("@pAcntSubSubGroupCode", dataRequest.AcntSubSubGroupCode);
                    cmd.Parameters.AddWithValue("@pBranchID", dataRequest.BranchID);
                    cmd.Parameters.AddWithValue("@pUserID", dataRequest.UserID);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandTimeout = 9999999;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtAcntBalanceBySubSubCodeList.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        pResult = error;
                    }
                }
            }
            return dtAcntBalanceBySubSubCodeList;
        }
        public DataTable CurAcntTranList(Model.RequestDTO.GetCurAcntTranLIstRequest dataRequest)
        {
            DataTable dtCurAcntTranList = new DataTable("CurAcntTranList");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetCurAcntTranLIst", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pDateFrom", dataRequest.DateFrom);
                    cmd.Parameters.AddWithValue("@pDateTo", dataRequest.DateTo);
                    cmd.Parameters.AddWithValue("@pAcntName", dataRequest.AcntName);
                    cmd.Parameters.AddWithValue("@pBranchID", dataRequest.BranchID);
                    cmd.Parameters.AddWithValue("@pCurrencyID", dataRequest.CurrencyID);
                    cmd.Parameters.AddWithValue("@pUserID", dataRequest.UserID);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtCurAcntTranList.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        pResult = error;
                    }
                }
            }
            return dtCurAcntTranList;
        }
        public Model.ResponseDTO.GetAllAcntBalanceByLoanIDResponse GetBalanceByLoanID(Model.RequestDTO.GetAllAcntBalanceByLoanIDRequest debtRequest)
        {
            Model.ResponseDTO.GetAllAcntBalanceByLoanIDResponse balanceResponse = new Model.ResponseDTO.GetAllAcntBalanceByLoanIDResponse();
            balanceResponse = dbGetAllAcntBalanceByLoanID(debtRequest);
            return balanceResponse;
        }
        wsMCS.Model.ResponseDTO.GetAllAcntBalanceByLoanIDResponse dbGetAllAcntBalanceByLoanID(Model.RequestDTO.GetAllAcntBalanceByLoanIDRequest debtRequest)
        {
            string pCustCurrent_Balance = "0";
            string psSuda_Balance = "0";
            string pExpSsuda_Balance = "0";
            string pRate_Balance = "0";
            string pExpRate_Balance = "0";
            string pPenalty_Balance = "0";
            string pExpDebt_Balance = "0";
            string pCloseDebt_Balance = "0";

            wsMCS.Model.ResponseDTO.GetAllAcntBalanceByLoanIDResponse debtResponse = new Model.ResponseDTO.GetAllAcntBalanceByLoanIDResponse();

            SqlCommand cmd = new SqlCommand("spGetAllAcntBalanceByLoanID", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pLoanID", debtRequest.LOAN_ID);

            SqlParameter custCurrent_Balance = new SqlParameter("@pC_CURRENT_BALANCE", SqlDbType.VarChar);
            custCurrent_Balance.Direction = ParameterDirection.Output;
            custCurrent_Balance.Size = 50;
            cmd.Parameters.Add(custCurrent_Balance);

            SqlParameter sSuda_Balance = new SqlParameter("@pL_SSUDA_BALANCE", SqlDbType.VarChar);
            sSuda_Balance.Direction = ParameterDirection.Output;
            sSuda_Balance.Size = 50;
            cmd.Parameters.Add(sSuda_Balance);

            SqlParameter expSsuda_Balance = new SqlParameter("@pL_EXP_SSUDA_BALANCE", SqlDbType.VarChar);
            expSsuda_Balance.Direction = ParameterDirection.Output;
            expSsuda_Balance.Size = 50;
            cmd.Parameters.Add(expSsuda_Balance);

            SqlParameter rate_Balance = new SqlParameter("@pL_RATE_BALANCE", SqlDbType.VarChar);
            rate_Balance.Direction = ParameterDirection.Output;
            rate_Balance.Size = 50;
            cmd.Parameters.Add(rate_Balance);

            SqlParameter expRate_Balance = new SqlParameter("@pL_EXP_RATE_BALANCE", SqlDbType.VarChar);
            expRate_Balance.Direction = ParameterDirection.Output;
            expRate_Balance.Size = 50;
            cmd.Parameters.Add(expRate_Balance);

            SqlParameter penalty_Balance = new SqlParameter("@pL_PENALTY_BALANCE", SqlDbType.VarChar);
            penalty_Balance.Direction = ParameterDirection.Output;
            penalty_Balance.Size = 50;
            cmd.Parameters.Add(penalty_Balance);

            SqlParameter expDebt_Balance = new SqlParameter("@pL_TOTAL_EXP_DEBT", SqlDbType.VarChar);
            expDebt_Balance.Direction = ParameterDirection.Output;
            expDebt_Balance.Size = 50;
            cmd.Parameters.Add(expDebt_Balance);

            SqlParameter closeDebt_Balance = new SqlParameter("@pL_TOTAL_CLOSE_DEBT", SqlDbType.VarChar);
            closeDebt_Balance.Direction = ParameterDirection.Output;
            closeDebt_Balance.Size = 50;
            cmd.Parameters.Add(closeDebt_Balance);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    pResult = ex.Message;
                }

                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                if (pResult == "OK")
                {
                    pCustCurrent_Balance = Convert.ToString(cmd.Parameters["@pC_CURRENT_BALANCE"].Value);
                    psSuda_Balance = Convert.ToString(cmd.Parameters["@pL_SSUDA_BALANCE"].Value);
                    pExpSsuda_Balance = Convert.ToString(cmd.Parameters["@pL_EXP_SSUDA_BALANCE"].Value);
                    pRate_Balance = Convert.ToString(cmd.Parameters["@pL_RATE_BALANCE"].Value);
                    pExpRate_Balance = Convert.ToString(cmd.Parameters["@pL_EXP_RATE_BALANCE"].Value);
                    pPenalty_Balance = Convert.ToString(cmd.Parameters["@pL_PENALTY_BALANCE"].Value);
                    pExpDebt_Balance = Convert.ToString(cmd.Parameters["@pL_TOTAL_EXP_DEBT"].Value);
                    pCloseDebt_Balance = Convert.ToString(cmd.Parameters["@pL_TOTAL_CLOSE_DEBT"].Value);

                    debtResponse.isSuccess = true;
                    debtResponse.errorCode = "";

                    debtResponse.custCurrent_Balance = pCustCurrent_Balance;
                    debtResponse.sSuda_Balance = psSuda_Balance;
                    debtResponse.expSsuda_Balance = pExpSsuda_Balance;
                    debtResponse.rate_Balance = pRate_Balance;
                    debtResponse.expRate_Balance = pExpRate_Balance;
                    debtResponse.penalty_Balance = pPenalty_Balance;
                    debtResponse.expDebt_Balance = pExpDebt_Balance;
                    debtResponse.closeDebt_Balance = pCloseDebt_Balance;
                }
                else
                {
                    debtResponse.isSuccess = false;
                    debtResponse.errorCode = pResult;
                    debtResponse.custCurrent_Balance = "0";
                    debtResponse.sSuda_Balance = "0";
                    debtResponse.expSsuda_Balance = "0";
                    debtResponse.rate_Balance = "0";
                    debtResponse.expRate_Balance = "0";
                    debtResponse.penalty_Balance = "0";
                    debtResponse.expDebt_Balance = "0";
                    debtResponse.closeDebt_Balance = "0";
                }
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return debtResponse;
        }
        public DataTable dbGetSubAcntListByMainAcntID(Model.RequestDTO.GetSubAcntListRequest dataRequest)
        {
            DataTable dtSubAcntList = new DataTable("SubAcntList");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetSubAcntListByMainAcntID", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pMainAcntID", dataRequest.MainAcntID);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtSubAcntList.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        pResult = error;
                    }
                }
            }
            return dtSubAcntList;
        }
    }
}