﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wsMCS.Model;
using System.Data.SqlClient;
using System.Data;
using wsMCS.App_Code;
using apiMCS.App_Code;

namespace wsMCS.Repositor
{
    public class CustomerProcess
    {
        public Model.ResponseDTO.AddCustomerResponse AddCustomer(Model.RequestDTO.AddCustomerRequest request)
        {
            Model.ResponseDTO.AddCustomerResponse CustomerResponse = new Model.ResponseDTO.AddCustomerResponse();
            CustomerResponse = dbAddCustomer(request);
            return CustomerResponse;
        }
        wsMCS.Model.ResponseDTO.AddCustomerResponse dbAddCustomer(Model.RequestDTO.AddCustomerRequest AddCustomerRequest)
        {
            wsMCS.Model.ResponseDTO.AddCustomerResponse AddCustomerresponse = new Model.ResponseDTO.AddCustomerResponse();

            SqlCommand cmd = new SqlCommand("spAddCustomer", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pBranchID", AddCustomerRequest.BranchID);
            cmd.Parameters.AddWithValue("@pSurname", AddCustomerRequest.Surname);
            cmd.Parameters.AddWithValue("@pName", AddCustomerRequest.Name);
            cmd.Parameters.AddWithValue("@pPatronymic", AddCustomerRequest.Patronymic);
            cmd.Parameters.AddWithValue("@pFullName", AddCustomerRequest.Surname + " " + AddCustomerRequest.Name + " " + AddCustomerRequest.Patronymic);
            cmd.Parameters.AddWithValue("@pPin", AddCustomerRequest.Pin);
            cmd.Parameters.AddWithValue("@pIdCardSeries", AddCustomerRequest.IdCardSeries);
            cmd.Parameters.AddWithValue("@pIdCardNumber", AddCustomerRequest.IdCardNumber);
            cmd.Parameters.AddWithValue("@pDateOfIssue", AddCustomerRequest.DateOfIssue);
            cmd.Parameters.AddWithValue("@pDateOfExpiry", AddCustomerRequest.DateOfExpiry);
            cmd.Parameters.AddWithValue("@pDateOfBirth", AddCustomerRequest.DateOfBirth);
            cmd.Parameters.AddWithValue("@pAuthority", AddCustomerRequest.Authority);
            cmd.Parameters.AddWithValue("@pGender", AddCustomerRequest.Gender);
            cmd.Parameters.AddWithValue("@pCountryID", AddCustomerRequest.CountryID);
            cmd.Parameters.AddWithValue("@pCityID", AddCustomerRequest.CityID);
            cmd.Parameters.AddWithValue("@pLegalAddress", AddCustomerRequest.LegalAddress);
            cmd.Parameters.AddWithValue("@pRegistrationAddress", AddCustomerRequest.RegistrationAddress);
            cmd.Parameters.AddWithValue("@pMobilePhone1", AddCustomerRequest.MobilePhone1);
            cmd.Parameters.AddWithValue("@pMobilePhone2", AddCustomerRequest.MobilePhone2);
            cmd.Parameters.AddWithValue("@pMobilePhone3", AddCustomerRequest.MobilePhone3);
            cmd.Parameters.AddWithValue("@pHomePhone", AddCustomerRequest.HomePhone);
            cmd.Parameters.AddWithValue("@pNote", AddCustomerRequest.Note);
            cmd.Parameters.AddWithValue("@pEmail", AddCustomerRequest.Email);
            cmd.Parameters.AddWithValue("@pCreatedID", AddCustomerRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);


            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);


            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                AddCustomerresponse.isSuccess = true;
                AddCustomerresponse.errorCode = "";
            }
            else
            {
                AddCustomerresponse.isSuccess = false;
                AddCustomerresponse.errorCode = pResult;
            }

            return AddCustomerresponse;
        }
        public Model.ResponseDTO.ModifyCustomerResponse ModifyCustomer(Model.RequestDTO.ModifyCustomerRequest ModifyCustomerRequest)
        {
            Model.ResponseDTO.ModifyCustomerResponse CustomerResponse = new Model.ResponseDTO.ModifyCustomerResponse();
            CustomerResponse = dbModifyCustomer(ModifyCustomerRequest);
            return CustomerResponse;
        }
        wsMCS.Model.ResponseDTO.ModifyCustomerResponse dbModifyCustomer(Model.RequestDTO.ModifyCustomerRequest ModifyCustomerRequest)
        {
            wsMCS.Model.ResponseDTO.ModifyCustomerResponse ModifyCustomerresponse = new Model.ResponseDTO.ModifyCustomerResponse();

            SqlCommand cmd = new SqlCommand("spModifyCustomer", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pCustomerID", ModifyCustomerRequest.CustomerID);
            cmd.Parameters.AddWithValue("@pBranchID", ModifyCustomerRequest.BranchID);
            cmd.Parameters.AddWithValue("@pSurname", ModifyCustomerRequest.Surname);
            cmd.Parameters.AddWithValue("@pName", ModifyCustomerRequest.Name);
            cmd.Parameters.AddWithValue("@pPatronymic", ModifyCustomerRequest.Patronymic);
            cmd.Parameters.AddWithValue("@pFullName", ModifyCustomerRequest.Surname + " " + ModifyCustomerRequest.Name + " " + ModifyCustomerRequest.Patronymic);
            cmd.Parameters.AddWithValue("@pPin", ModifyCustomerRequest.Pin);
            cmd.Parameters.AddWithValue("@pIdCardSeries", ModifyCustomerRequest.IdCardSeries);
            cmd.Parameters.AddWithValue("@pIdCardNumber", ModifyCustomerRequest.IdCardNumber);
            cmd.Parameters.AddWithValue("@pDateOfIssue", ModifyCustomerRequest.DateOfIssue);
            cmd.Parameters.AddWithValue("@pDateOfExpiry", ModifyCustomerRequest.DateOfExpiry);
            cmd.Parameters.AddWithValue("@pDateOfBirth", ModifyCustomerRequest.DateOfBirth);
            cmd.Parameters.AddWithValue("@pAuthority", ModifyCustomerRequest.Authority);
            cmd.Parameters.AddWithValue("@pGender", ModifyCustomerRequest.Gender);
            cmd.Parameters.AddWithValue("@pCountryID", ModifyCustomerRequest.CountryID);
            cmd.Parameters.AddWithValue("@pCityID", ModifyCustomerRequest.CityID);
            cmd.Parameters.AddWithValue("@pLegalAddress", ModifyCustomerRequest.LegalAddress);
            cmd.Parameters.AddWithValue("@pRegistrationAddress", ModifyCustomerRequest.RegistrationAddress);
            cmd.Parameters.AddWithValue("@pMobilePhone1", ModifyCustomerRequest.MobilePhone1);
            cmd.Parameters.AddWithValue("@pMobilePhone2", ModifyCustomerRequest.MobilePhone2);
            cmd.Parameters.AddWithValue("@pMobilePhone3", ModifyCustomerRequest.MobilePhone3);
            cmd.Parameters.AddWithValue("@pHomePhone", ModifyCustomerRequest.HomePhone);
            cmd.Parameters.AddWithValue("@pNote", ModifyCustomerRequest.Note);
            cmd.Parameters.AddWithValue("@pEmail", ModifyCustomerRequest.Email);
            cmd.Parameters.AddWithValue("@pModifiedID", ModifyCustomerRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);


            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);


            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                ModifyCustomerresponse.isSuccess = true;
                ModifyCustomerresponse.errorCode = "";
            }
            else
            {
                ModifyCustomerresponse.isSuccess = false;
                ModifyCustomerresponse.errorCode = pResult;
            }

            return ModifyCustomerresponse;
        }
        public Model.ResponseDTO.DeleteCustomerResponse DeleteCustomer(Model.RequestDTO.DeleteCustomerRequest CustomerRequest)
        {
            Model.ResponseDTO.DeleteCustomerResponse CustomerResponse = new Model.ResponseDTO.DeleteCustomerResponse();
            CustomerResponse = dtDeleteCustomer(CustomerRequest);
            return CustomerResponse;
        }
        wsMCS.Model.ResponseDTO.DeleteCustomerResponse dtDeleteCustomer(Model.RequestDTO.DeleteCustomerRequest CustomerRequest)
        {
            Model.ResponseDTO.DeleteCustomerResponse DeleteCustomerResponse = new Model.ResponseDTO.DeleteCustomerResponse();
            SqlCommand cmd = new SqlCommand("spDeleteCustomer", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pCustomerID", CustomerRequest.CustomerID);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                DeleteCustomerResponse.isSuccess = true;
                DeleteCustomerResponse.errorCode = "";
            }
            else
            {
                DeleteCustomerResponse.isSuccess = false;
                DeleteCustomerResponse.errorCode = pResult;
            }

            return DeleteCustomerResponse;
        }
        public DataTable GetCustomers(Model.RequestDTO.GetCustomerRequest CustomerRequest, out int recordCount)
        {
            DataTable dtCustomer = new DataTable("Customer");
            recordCount = 0;
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetCustomerList", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PageIndex", CustomerRequest.pageIndex);
                    cmd.Parameters.AddWithValue("@PageSize", CustomerRequest.pageSize);
                    cmd.Parameters.AddWithValue("@pUserID", CustomerRequest.userID);
                    cmd.Parameters.AddWithValue("@pFullname", CustomerRequest.Fullname);
                    cmd.Parameters.AddWithValue("@pPin", CustomerRequest.Pin);
                    cmd.Parameters.AddWithValue("@pIdCardNumber", CustomerRequest.IdCardNumber);
                    cmd.Parameters.Add("@RecordCount", SqlDbType.Int, 4);
                    cmd.Parameters["@RecordCount"].Direction = ParameterDirection.Output;
                    con.Open();
                    IDataReader idr = cmd.ExecuteReader();
                    dtCustomer.Load(idr);
                    idr.Close();
                    con.Close();
                    recordCount = Convert.ToInt32(cmd.Parameters["@RecordCount"].Value);
                }
            }
            return dtCustomer;
        }
        public DataTable dtCustomerByID(int CustomerID)
        {
            DataTable dt = new DataTable("SelectedCustomer");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT c.* FROM Customer c WHERE c.CustomerID = @CustomerID ", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@CustomerID", CustomerID);

            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
    }
}