﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wsMCS.Model;
using System.Data.SqlClient;
using System.Data;
using wsMCS.App_Code;
using apiMCS.App_Code;
using System.Globalization;

namespace wsMCS.Repositor
{
    public class GeneralProcess
    {   
        public DataTable dtBranch(int UserID)
        {
            DataTable dt = new DataTable("Country");
            SqlDataAdapter da = new SqlDataAdapter(@"select b.* from Branch b where  b.BranchCode in
                                                    (select u.BranchCode from UserBranch u where u.UserID = @UserID) ", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@UserID", UserID);

            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable dtCountry()
        {
            DataTable dt = new DataTable("Country");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM Country
            ORDER BY CountryName ", Utils.sqlConn);

            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable dtCity()
        {
            DataTable dt = new DataTable("City");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM City
            ORDER BY CityName ", Utils.sqlConn);

            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }  
        public DataTable dtCurrency()
        {
            DataTable dt = new DataTable("Currency");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM Currency
            ORDER BY CurrencyName ", Utils.sqlConn);

            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }

        public decimal getDailyCurrency(string currencyName)
        {
            decimal result = 0;
            DataTable dt = new DataTable("DailyCurrency");
            SqlDataAdapter da = new SqlDataAdapter(@"select * from DailyCurrency where ID = (select max(id) from DailyCurrency) and NAME = @NAME", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@NAME", currencyName);
            try
            {
                da.Fill(dt);
                result = Convert.ToDecimal(dt.Rows[0]["Value"]);
            }
            catch
            {
                result = -1;
            }
            return result;
        }
        public int SequenceNextVal()
        {
            int Sequence = 0;
            SqlCommand cmd = new SqlCommand("_spGetSequenceNextVal", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter SequenceValue = new SqlParameter("@pSeqValue", SqlDbType.Int);
            SequenceValue.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(SequenceValue);

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                Sequence = Convert.ToInt32(cmd.Parameters["@pSeqValue"].Value);
            }
            catch (Exception ex)
            {
                Sequence = -1;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return Sequence;
        }
        public DataTable dtLoanAmortization(Model.RequestDTO.loanAmortizaiotnRequest dataRequest)
        {
            DateTime startDate;
            string formatString = "dd.MM.yyyy";
            CultureInfo enUS = new CultureInfo("en-US"); // is up to you
            DateTime.TryParseExact(dataRequest.startDate, formatString, enUS,
                                           DateTimeStyles.None, out startDate) ;

              string paymentDate;
            double monthlyRate = dataRequest.monthlyRate;
            double principalAmount = 0, interestAmount = 0, endingBalance = 0, monthlyPayment = 0;
            double PMT = GetPMT(dataRequest);
            double beginingBalance = dataRequest.loanAmount;
            DataTable dt = new DataTable("PaymentSchedule");

            dt.Columns.Add("N", typeof(int));
            dt.Columns.Add("PaymentDate", typeof(string));
            dt.Columns.Add("BeginingBalance", typeof(double));
            dt.Columns.Add("PrincipalAmount", typeof(double));
            dt.Columns.Add("InterestAmount", typeof(double));
            dt.Columns.Add("PMT", typeof(double));
            dt.Columns.Add("EndingBalance", typeof(double));
            dt.DefaultView.Sort = "PaymentDate ASC";

            DataRow dr = dt.NewRow();
            for (int i = 1; i <= dataRequest.monthPeriod; i++)
            {
                paymentDate = startDate.AddMonths(i).ToString("dd.MM.yyyy");
                beginingBalance = (i == 1) ? beginingBalance : endingBalance;
                interestAmount = Math.Round(beginingBalance * monthlyRate / 100, 1);

                if (i > dataRequest.discountPeriod)
                {
                    principalAmount = PMT - interestAmount;
                    endingBalance = Math.Round(beginingBalance - principalAmount, 1);
                    principalAmount = (i == dataRequest.monthPeriod) ? Math.Round(principalAmount + endingBalance, 1) : Math.Round(principalAmount, 1);
                    monthlyPayment = (i == dataRequest.monthPeriod) ? Math.Round(PMT,1) + endingBalance : Math.Round(PMT, 1);
                }
                else
                {
                    monthlyPayment = interestAmount;
                    principalAmount = 0;
                    endingBalance = beginingBalance;
                }
                if (i == dataRequest.monthPeriod)
                {
                    endingBalance = 0;
                }

                try
                {
                    dt.Rows.Add(i, paymentDate, beginingBalance, principalAmount, interestAmount, monthlyPayment, endingBalance);

                }
                catch
                {
                    dt = null;
                }
            }
            return dt;
        }
        public Double GetPMT(Model.RequestDTO.loanAmortizaiotnRequest dataRequest)
        {
            Double pmtValue = 0;
            pmtValue = Math.Round(Convert.ToDouble(dataRequest.monthlyRate / 100)
             / (Math.Pow(Convert.ToDouble((1 + dataRequest.monthlyRate / 100)), dataRequest.monthPeriod - dataRequest.discountPeriod) - 1)
             * -(-dataRequest.loanAmount * Math.Pow(Convert.ToDouble((1 + dataRequest.monthlyRate / 100)), dataRequest.monthPeriod - dataRequest.discountPeriod)
             + 0), 1);
            return pmtValue;
        }
    }
}