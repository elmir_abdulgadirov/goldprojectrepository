﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using wsMCS.App_Code;
using apiMCS.App_Code;

namespace wsMCS.Repositor
{
    public class LoanProcess
    {
        public Model.ResponseDTO.AddLoanResponse AddLoan(Model.RequestDTO.AddLoanRequestDTO LoanRequest)
        {
            Model.ResponseDTO.AddLoanResponse LoanResponse = new Model.ResponseDTO.AddLoanResponse();
            LoanResponse = dbAddLoan(LoanRequest);
            return LoanResponse;
        }
        wsMCS.Model.ResponseDTO.AddLoanResponse dbAddLoan(Model.RequestDTO.AddLoanRequestDTO AddLoanRequest)
        {
            int pLoanId = -2;
            wsMCS.Model.ResponseDTO.AddLoanResponse AddLoanResponse = new Model.ResponseDTO.AddLoanResponse();

            SqlCommand cmd = new SqlCommand("spAddLoan", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT

            cmd.Parameters.AddWithValue("@pCreatedID", AddLoanRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);


            SqlParameter paramLoanId = new SqlParameter("@pLoanId", SqlDbType.Int);
            paramLoanId.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(paramLoanId);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                pLoanId = Convert.ToInt32(cmd.Parameters["@pLoanId"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
                pLoanId = -2;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                AddLoanResponse.isSuccess = true;
                AddLoanResponse.errorCode = "";
                AddLoanResponse.loanId = pLoanId;
            }
            else
            {
                AddLoanResponse.isSuccess = false;
                AddLoanResponse.errorCode = pResult;
                AddLoanResponse.loanId = pLoanId;
            }

            return AddLoanResponse;
        }
        public Model.ResponseDTO.ModifyLoanResponse ModifyLoan(Model.RequestDTO.ModifyLoanRequestDTO LoanRequest)
        {
            Model.ResponseDTO.ModifyLoanResponse LoanResponse = new Model.ResponseDTO.ModifyLoanResponse();
            LoanResponse = dbModifyLoan(LoanRequest);
            return LoanResponse;
        }
        wsMCS.Model.ResponseDTO.ModifyLoanResponse dbModifyLoan(Model.RequestDTO.ModifyLoanRequestDTO LoanRequest)
        {
            wsMCS.Model.ResponseDTO.ModifyLoanResponse ModifyLoanResponse = new Model.ResponseDTO.ModifyLoanResponse();

            SqlCommand cmd = new SqlCommand("spModifyLoan", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pLoanID", LoanRequest.LoanID);
            cmd.Parameters.AddWithValue("@pBranchID", LoanRequest.BranchID);
            cmd.Parameters.AddWithValue("@pCustomerID", LoanRequest.CustomerID);
            cmd.Parameters.AddWithValue("@pLoanTypeID", LoanRequest.LoanTypeID);
            cmd.Parameters.AddWithValue("@pStartDate", LoanRequest.StartDate);
            cmd.Parameters.AddWithValue("@pDateOfExpiry", LoanRequest.DateOfExpiry);
            cmd.Parameters.AddWithValue("@pPaymentDate", LoanRequest.PaymentDate);
            cmd.Parameters.AddWithValue("@pCalculationDate", LoanRequest.CalculationDate);
            cmd.Parameters.AddWithValue("@pCollateralNumber", LoanRequest.CollateralNumber);
            cmd.Parameters.AddWithValue("@pAgreementNumber", LoanRequest.AgreementNumber);
            cmd.Parameters.AddWithValue("@pAcNumber", LoanRequest.AcNumber);
            cmd.Parameters.AddWithValue("@pLoanAmount", LoanRequest.LoanAmount);
            cmd.Parameters.AddWithValue("@pLoanInterest", LoanRequest.LoanInterest);
            cmd.Parameters.AddWithValue("@pLoanPeriod", LoanRequest.LoanPeriod);
            cmd.Parameters.AddWithValue("@pDiscountPeriod", LoanRequest.DiscountPeriod);
            cmd.Parameters.AddWithValue("@pCommissionFee", LoanRequest.CommissionFee);
            cmd.Parameters.AddWithValue("@pLoanStatusID", LoanRequest.LoanStatusID);
            cmd.Parameters.AddWithValue("@pCurrencyID", LoanRequest.CurrencyID);
            cmd.Parameters.AddWithValue("@pLoanMethodID", LoanRequest.LoanMethodID);
            cmd.Parameters.AddWithValue("@pDepositBox", LoanRequest.DepositBox);
            cmd.Parameters.AddWithValue("@pCollateralReturnDate", LoanRequest.CollateralReturnDate);
            cmd.Parameters.AddWithValue("@pJewelerCost", LoanRequest.JewelerCost);
            cmd.Parameters.AddWithValue("@pAccountTypeID", LoanRequest.AccountTypeID);
            cmd.Parameters.AddWithValue("@pCollateralType", LoanRequest.CollateralType);
            cmd.Parameters.AddWithValue("@pLenderID", LoanRequest.LenderID);
            cmd.Parameters.AddWithValue("@pVerifierID", LoanRequest.VerifierID);
            cmd.Parameters.AddWithValue("@pModifiedID", LoanRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                ModifyLoanResponse.isSuccess = true;
                ModifyLoanResponse.errorCode = "";
            }
            else
            {
                ModifyLoanResponse.isSuccess = false;
                ModifyLoanResponse.errorCode = pResult;
            }

            return ModifyLoanResponse;
        }
        public Model.ResponseDTO.ModifyLoanCollateralResponse ModifyLoanCollateral(Model.RequestDTO.ModifyLoanCollateralRequest LoanRequest)
        {
            Model.ResponseDTO.ModifyLoanCollateralResponse LoanResponse = new Model.ResponseDTO.ModifyLoanCollateralResponse();
            LoanResponse = dbModifyLoanCollateral(LoanRequest);
            return LoanResponse;
        }
        wsMCS.Model.ResponseDTO.ModifyLoanCollateralResponse dbModifyLoanCollateral(Model.RequestDTO.ModifyLoanCollateralRequest LoanRequest)
        {
            wsMCS.Model.ResponseDTO.ModifyLoanCollateralResponse ModifyLoanResponse = new Model.ResponseDTO.ModifyLoanCollateralResponse();

            SqlCommand cmd = new SqlCommand("spModifyLoanCollateral2", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pLoanID", LoanRequest.LoanID);
            cmd.Parameters.AddWithValue("@pDepositBox", LoanRequest.DepositBox);
            cmd.Parameters.AddWithValue("@pJewelerName", LoanRequest.JewelerName);
            cmd.Parameters.AddWithValue("@pJewelerCost", LoanRequest.JewelerCost);
            cmd.Parameters.AddWithValue("@pModifiedID", LoanRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                ModifyLoanResponse.isSuccess = true;
                ModifyLoanResponse.errorCode = "";
            }
            else
            {
                ModifyLoanResponse.isSuccess = false;
                ModifyLoanResponse.errorCode = pResult;
            }

            return ModifyLoanResponse;
        }
        /* public Model.ResponseDTO.DeleteLoanResponse DeleteLoan(Model.RequestDTO.DeleteLoanByIDRequest LoanRequest)
         {
             Model.ResponseDTO.DeleteLoanResponse LoanResponse = new Model.ResponseDTO.DeleteLoanResponse();
             LoanResponse = dtDeleteLoan(LoanRequest);
             return LoanResponse;
         }
         wsMCS.Model.ResponseDTO.DeleteLoanResponse dtDeleteLoan(Model.RequestDTO.DeleteLoanRequest LoanRequest)
         {
             Model.ResponseDTO.DeleteLoanResponse DeleteLoanResponse = new Model.ResponseDTO.DeleteLoanResponse();
             SqlCommand cmd = new SqlCommand("spDeleteLoan", Utils.sqlConn);
             cmd.CommandType = CommandType.StoredProcedure;
             //INPUT
             cmd.Parameters.AddWithValue("@pCustomerID", LoanRequest.LoanID);

             SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
             ParamResult.Direction = ParameterDirection.Output;
             ParamResult.Size = 100;

             cmd.Parameters.Add(ParamResult);
             string pResult = "";

             try
             {
                 cmd.Connection.Open();
                 cmd.ExecuteNonQuery();
                 pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
             }
             catch (Exception EX)
             {
                 pResult = EX.Message;
             }
             finally
             {
                 cmd.Connection.Close();
             }

             if (pResult == "OK")
             {
                 DeleteLoanResponse.isSuccess = true;
                 DeleteLoanResponse.errorCode = "";
             }
             else
             {
                 DeleteLoanResponse.isSuccess = false;
                 DeleteLoanResponse.errorCode = pResult;
             }

             return DeleteLoanResponse;
         }*/
        public DataTable GetLoanList(Model.RequestDTO.GetLoanListRequest getLoanRequest, out int RecorCount)
        {
            RecorCount = 0;
            string sql_query = @"SELECT tab.* FROM (SELECT ROW_NUMBER() OVER (ORDER BY LoanID DESC) AS RowNumber, 
                        v.BranchID, v.BranchName, v.LoanID,	v.CustomerID, v.CustomerName, v.LoanMethodID, v.MethodName, v.CollateralNumber, 
						v.AcNumber, v.AgreementNumber, v.StartDate, v.CollateralType, v.CurrencyID, v.CurrencyName, v.LoanAmount AS LAmount, v.IsApprove,
						dbo.TotalPrinsipalPaymentByLoanID(v.LoanID) AS PAmount,
						v.LoanAmount - isnull(dbo.TotalPrinsipalPaymentByLoanID(v.LoanID),0) AS LoanBalance,
						v.LoanInterest, v.LoanStatusID, v.LoanStatus, v.ActualExpiryDate,v.PaymentDate,
						dbo.CountOfDayPayment(v.PaymentDate) as  CountOfDayPayment, v.MobilePhone1, v.MobilePhone2
	               FROM v_LoanFullData v									      
				   WHERE v.BranchCode in (select BranchCode from UserBranch where UserID = @UserId) AND v.status = 'ACTIVE'
                {0}) tab
                   WHERE (tab.RowNumber BETWEEN(@PageIndex -1) * @PageSize + 1 AND(((@PageIndex -1) * @PageSize + 1) + @PageSize) - 1)";

            string sql_sub_query = " ";
            if (getLoanRequest.CustomerName != "")
            {
                sql_sub_query += " AND v.CustomerName like  @CUSTOMER_NAME";
            }
            if (getLoanRequest.CollateralNumber != "")
            {
                sql_sub_query += " AND v.CollateralNumber like  @COLLATERAL_NUMBER";
            }
            if (getLoanRequest.LoanMethodID != 0)
            {
                sql_sub_query += " AND v.loanMethodID =  @LOAN_METHOD_ID";
            }
            if (getLoanRequest.MobilePhone1 != "")
            {
                sql_sub_query += " AND v.MobilePhone1 like  @MOBILE_PHONE1";
            }
            if (getLoanRequest.MobilePhone2 != "")
            {
                sql_sub_query += " AND v.MobilePhone2 like  @MOBILE_PHONE2";
            }
            if (getLoanRequest.StartDate != "")
            {
                sql_sub_query += " AND v.StartDate =  @START_DATE";
            }
            if (getLoanRequest.CollateralType != "")
            {
                sql_sub_query += " AND v.CollateralType =  @COLLATERAL_TYPE";
            }
            if (getLoanRequest.CurrencyID != 0)
            {
                sql_sub_query += " AND v.CurrencyID =  @CURRENCY_ID";
            }
            if (getLoanRequest.LoanAmount != 0)
            {
                sql_sub_query += " AND v.LoanAmount =  @LOAN_AMOUNT";
            }
            if (getLoanRequest.LoanSTatusID != 0)
            {
                sql_sub_query += " AND v.LoanStatusID =  @LOAN_STATUS_ID";
            }

            string _sql = string.Format(sql_query, sql_sub_query).Trim();

            SqlDataAdapter da = new SqlDataAdapter(_sql, Utils.sqlConn);

            da.SelectCommand.Parameters.AddWithValue("@UserId", getLoanRequest.UserId);
            if (getLoanRequest.CustomerName != "")
            {
                da.SelectCommand.Parameters.Add("@CUSTOMER_NAME", SqlDbType.NVarChar, 32767).Value = "%" + getLoanRequest.CustomerName + "%";
            }
            if (getLoanRequest.CollateralNumber != "")
            {
                da.SelectCommand.Parameters.Add("@COLLATERAL_NUMBER", SqlDbType.NVarChar, 32767).Value = "%" + getLoanRequest.CollateralNumber + "%";
            }
            if (getLoanRequest.LoanMethodID != 0)
            {
                da.SelectCommand.Parameters.Add("@LOAN_METHOD_ID", SqlDbType.Int).Value = getLoanRequest.LoanMethodID;
            }
            if (getLoanRequest.MobilePhone1 != "")
            {
                da.SelectCommand.Parameters.Add("@MOBILE_PHONE1", SqlDbType.NVarChar, 32767).Value = "%" + getLoanRequest.MobilePhone1 + "%";
            }
            if (getLoanRequest.MobilePhone2 != "")
            {
                da.SelectCommand.Parameters.Add("@MOBILE_PHONE2", SqlDbType.NVarChar, 32767).Value = "%" + getLoanRequest.MobilePhone2 + "%";
            }
            if (getLoanRequest.StartDate != "")
            {
                da.SelectCommand.Parameters.Add("@START_DATE", SqlDbType.NVarChar, 32767).Value = getLoanRequest.StartDate;
            }
            if (getLoanRequest.CollateralType != "")
            {
                da.SelectCommand.Parameters.Add("@COLLATERAL_TYPE", SqlDbType.NVarChar, 32767).Value = getLoanRequest.CollateralType;
            }
            if (getLoanRequest.CurrencyID != 0)
            {
                da.SelectCommand.Parameters.Add("@CURRENCY_ID", SqlDbType.Int).Value = getLoanRequest.CurrencyID;
            }
            if (getLoanRequest.LoanAmount != 0)
            {
                da.SelectCommand.Parameters.Add("@LOAN_AMOUNT", SqlDbType.Float).Value = getLoanRequest.LoanAmount;
            }
            if (getLoanRequest.LoanSTatusID != 0)
            {
                da.SelectCommand.Parameters.Add("@LOAN_STATUS_ID", SqlDbType.Int).Value = getLoanRequest.LoanSTatusID;
            }

            da.SelectCommand.Parameters.AddWithValue("@PageIndex", getLoanRequest.pageIndex);
            da.SelectCommand.Parameters.AddWithValue("@PageSize", getLoanRequest.pageSize);



            DataTable dtAccount = new DataTable("GetLoanList");
            try
            {
                da.Fill(dtAccount);
            }
            catch (Exception ex)
            {
                return null;
            }
            RecorCount = CountLoan(getLoanRequest);


            return dtAccount;
        }

        private int CountLoan(Model.RequestDTO.GetLoanListRequest getLoanRequest)
        {
            string sql_query = @"select Count(1) as Cntt from (
                                SELECT ROW_NUMBER() OVER (ORDER BY LoanID DESC) AS RowNumber, 
                        v.BranchID, v.BranchName, v.LoanID,	v.CustomerID, v.CustomerName, v.LoanMethodID, v.MethodName, v.CollateralNumber, 
						v.AcNumber, v.AgreementNumber, v.StartDate, v.CollateralType, v.CurrencyID, v.CurrencyName, v.LoanAmount AS LAmount, v.IsApprove,
						dbo.TotalPrinsipalByLoanID(v.LoanID) AS PAmount,
						v.LoanAmount - isnull(dbo.TotalPrinsipalByLoanID(v.LoanID),0) AS LoanBalance,
						v.LoanInterest, v.LoanStatusID, v.LoanStatus, v.ActualExpiryDate,v.PaymentDate,
						dbo.CountOfDayPayment(v.PaymentDate) as  CountOfDayPayment, v.MobilePhone1, v.MobilePhone2
	               FROM v_LoanFullData v 
				                WHERE v.BranchCode in (select BranchCode from UserBranch where UserID = @UserId) AND v.status = 'ACTIVE'
                                {0} ) tab";

            string sql_sub_query = " ";
            if (getLoanRequest.CustomerName != "")
            {
                sql_sub_query += " AND v.CustomerName like  @CUSTOMER_NAME";
            }
            if (getLoanRequest.CollateralNumber != "")
            {
                sql_sub_query += " AND v.CollateralNumber like  @COLLATERAL_NUMBER";
            }
            if (getLoanRequest.LoanMethodID != 0)
            {
                sql_sub_query += " AND v.loanMethodID =  @LOAN_METHOD_ID";
            }
            if (getLoanRequest.MobilePhone1 != "")
            {
                sql_sub_query += " AND v.MobilePhone1 like  @MOBILE_PHONE1";
            }
            if (getLoanRequest.MobilePhone2 != "")
            {
                sql_sub_query += " AND v.MobilePhone2 like  @MOBILE_PHONE2";
            }
            if (getLoanRequest.StartDate != "")
            {
                sql_sub_query += " AND v.StartDate =  @START_DATE";
            }
            if (getLoanRequest.CollateralType != "")
            {
                sql_sub_query += " AND v.CollateralType =  @COLLATERAL_TYPE";
            }
            if (getLoanRequest.CurrencyID != 0)
            {
                sql_sub_query += " AND v.CurrencyID =  @CURRENCY_ID";
            }
            if (getLoanRequest.LoanAmount != 0)
            {
                sql_sub_query += " AND v.LoanAmount =  @LOAN_AMOUNT";
            }
            if (getLoanRequest.LoanSTatusID != 0)
            {
                sql_sub_query += " AND v.LoanStatusID =  @LOAN_STATUS_ID";
            }

            string _sql = string.Format(sql_query, sql_sub_query).Trim();
            SqlDataAdapter da = new SqlDataAdapter(_sql, Utils.sqlConn);

            da.SelectCommand.Parameters.AddWithValue("@UserId", getLoanRequest.UserId);
            if (getLoanRequest.CustomerName != "")
            {
                da.SelectCommand.Parameters.Add("@CUSTOMER_NAME", SqlDbType.NVarChar, 32767).Value = "%" + getLoanRequest.CustomerName + "%";
            }
            if (getLoanRequest.CollateralNumber != "")
            {
                da.SelectCommand.Parameters.Add("@COLLATERAL_NUMBER", SqlDbType.NVarChar, 32767).Value = "%" + getLoanRequest.CollateralNumber + "%";
            }
            if (getLoanRequest.LoanMethodID != 0)
            {
                da.SelectCommand.Parameters.Add("@LOAN_METHOD_ID", SqlDbType.Int).Value = getLoanRequest.LoanMethodID;
            }
            if (getLoanRequest.MobilePhone1 != "")
            {
                da.SelectCommand.Parameters.Add("@MOBILE_PHONE1", SqlDbType.NVarChar, 32767).Value = "%" + getLoanRequest.MobilePhone1 + "%";
            }
            if (getLoanRequest.MobilePhone2 != "")
            {
                da.SelectCommand.Parameters.Add("@MOBILE_PHONE2", SqlDbType.NVarChar, 32767).Value = "%" + getLoanRequest.MobilePhone2 + "%";
            }
            if (getLoanRequest.StartDate != "")
            {
                da.SelectCommand.Parameters.Add("@START_DATE", SqlDbType.NVarChar, 32767).Value = getLoanRequest.StartDate;
            }
            if (getLoanRequest.CollateralType != "")
            {
                da.SelectCommand.Parameters.Add("@COLLATERAL_TYPE", SqlDbType.NVarChar, 32767).Value = getLoanRequest.CollateralType;
            }
            if (getLoanRequest.CurrencyID != 0)
            {
                da.SelectCommand.Parameters.Add("@CURRENCY_ID", SqlDbType.Int).Value = getLoanRequest.CurrencyID;
            }
            if (getLoanRequest.LoanAmount != 0)
            {
                da.SelectCommand.Parameters.Add("@LOAN_AMOUNT", SqlDbType.Float).Value = getLoanRequest.LoanAmount;
            }
            if (getLoanRequest.LoanSTatusID != 0)
            {
                da.SelectCommand.Parameters.Add("@LOAN_STATUS_ID", SqlDbType.Int).Value = getLoanRequest.LoanSTatusID;
            }

            DataTable dtAccount = new DataTable("GetCustomerCurrAcnts");
            try
            {
                da.Fill(dtAccount);
            }
            catch (Exception ex)
            {
                return 0;
            }
            return Convert.ToInt32(dtAccount.Rows[0]["Cntt"]);

        }

        public DataTable dtLoanType()
        {
            DataTable dt = new DataTable("LoanType");
            SqlDataAdapter da = new SqlDataAdapter(@"select * from LoanType Order By LoanTypeName", Utils.sqlConn);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable dtLoanStatus()
        {
            DataTable dt = new DataTable("LoanStatus");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM LoanStatus ORDER BY LoanStatus", Utils.sqlConn);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable dtLoanMethod()
        {
            DataTable dt = new DataTable("LoanMethod");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM LoanMethod ORDER BY MethodName", Utils.sqlConn);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable dtLoanCollateralType()
        {
            DataTable dt = new DataTable("LoanCollateralType");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM LoanCollateralType ORDER BY CollateralType", Utils.sqlConn);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public Model.ResponseDTO.AddCardResponse AddCard(Model.RequestDTO.AddCardRequest request)
        {
            Model.ResponseDTO.AddCardResponse CardResponse = new Model.ResponseDTO.AddCardResponse();
            CardResponse = dbAddCard(request);
            return CardResponse;
        }
        wsMCS.Model.ResponseDTO.AddCardResponse dbAddCard(Model.RequestDTO.AddCardRequest AddCardRequest)
        {
            wsMCS.Model.ResponseDTO.AddCardResponse AddCardResponse = new Model.ResponseDTO.AddCardResponse();

            SqlCommand cmd = new SqlCommand("spAddCard", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pPan", AddCardRequest.Pan);
            cmd.Parameters.AddWithValue("@pCardAmount", AddCardRequest.CardAmount);
            cmd.Parameters.AddWithValue("@pDateOfExpiry", AddCardRequest.DateOfExpiry);
            cmd.Parameters.AddWithValue("@pSecretWord", AddCardRequest.SecretWord);
            cmd.Parameters.AddWithValue("@pLoanID", AddCardRequest.LoanID);
            cmd.Parameters.AddWithValue("@pCreatedID", AddCardRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                AddCardResponse.isSuccess = true;
                AddCardResponse.errorCode = "";
            }
            else
            {
                AddCardResponse.isSuccess = false;
                AddCardResponse.errorCode = pResult;
            }

            return AddCardResponse;
        }
        public Model.ResponseDTO.ModifyCardResponse ModifyCard(Model.RequestDTO.ModifyCardRequest ModifyRequest)
        {
            Model.ResponseDTO.ModifyCardResponse CardResponse = new Model.ResponseDTO.ModifyCardResponse();
            CardResponse = dbModifyCard(ModifyRequest);
            return CardResponse;
        }
        wsMCS.Model.ResponseDTO.ModifyCardResponse dbModifyCard(Model.RequestDTO.ModifyCardRequest ModifyRequest)
        {
            wsMCS.Model.ResponseDTO.ModifyCardResponse ModifyCardResponse = new Model.ResponseDTO.ModifyCardResponse();

            SqlCommand cmd = new SqlCommand("spModifyCard", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pCardID", ModifyRequest.CardID);
            cmd.Parameters.AddWithValue("@pPan", ModifyRequest.Pan);
            cmd.Parameters.AddWithValue("@pCardAmount", ModifyRequest.CardAmount);
            cmd.Parameters.AddWithValue("@pDateOfExpiry", ModifyRequest.DateOfExpiry);
            cmd.Parameters.AddWithValue("@pSecretWord", ModifyRequest.SecretWord);
            cmd.Parameters.AddWithValue("@pModifiedID", ModifyRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pLoanId", ModifyRequest.LoanID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);


            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                ModifyCardResponse.isSuccess = true;
                ModifyCardResponse.errorCode = "";
            }
            else
            {
                ModifyCardResponse.isSuccess = false;
                ModifyCardResponse.errorCode = pResult;
            }

            return ModifyCardResponse;
        }
        public Model.ResponseDTO.AddLoanNoteResponse AddLoanNote(Model.RequestDTO.AddLoanNoteRequest noteRequest)
        {
            Model.ResponseDTO.AddLoanNoteResponse NoteResponse = new Model.ResponseDTO.AddLoanNoteResponse();
            NoteResponse = dbAddNote(noteRequest);
            return NoteResponse;
        }
        wsMCS.Model.ResponseDTO.AddLoanNoteResponse dbAddNote(Model.RequestDTO.AddLoanNoteRequest noteRequest)
        {
            wsMCS.Model.ResponseDTO.AddLoanNoteResponse NoteResponse = new Model.ResponseDTO.AddLoanNoteResponse();

            SqlCommand cmd = new SqlCommand("spAddLoanNote", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pNote", noteRequest.Note);
            cmd.Parameters.AddWithValue("@pLoanID", noteRequest.LoanID);
            cmd.Parameters.AddWithValue("@pCreatedID", noteRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                NoteResponse.isSuccess = true;
                NoteResponse.errorCode = "";
            }

            else
            {
                NoteResponse.isSuccess = false;
                NoteResponse.errorCode = pResult;
            }

            return NoteResponse;
        }
        public Model.ResponseDTO.ModifyLoanNoteResponse ModifyLoanNote(Model.RequestDTO.ModifyLoanNoteRequest noteRequest)
        {
            Model.ResponseDTO.ModifyLoanNoteResponse NoteResponse = new Model.ResponseDTO.ModifyLoanNoteResponse();
            NoteResponse = dbModifyNote(noteRequest);
            return NoteResponse;
        }
        wsMCS.Model.ResponseDTO.ModifyLoanNoteResponse dbModifyNote(Model.RequestDTO.ModifyLoanNoteRequest noteRequest)
        {
            wsMCS.Model.ResponseDTO.ModifyLoanNoteResponse NoteResponse = new Model.ResponseDTO.ModifyLoanNoteResponse();

            SqlCommand cmd = new SqlCommand("spModifyLoanNote", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pNoteID", noteRequest.NoteID);
            cmd.Parameters.AddWithValue("@pNote", noteRequest.Note);
            cmd.Parameters.AddWithValue("@pModifiedID", noteRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                NoteResponse.isSuccess = true;
                NoteResponse.errorCode = "";
            }

            else
            {
                NoteResponse.isSuccess = false;
                NoteResponse.errorCode = pResult;
            }

            return NoteResponse;
        }
        public Model.ResponseDTO.DeleteLoanNoteResponse DeleteLoanNote(Model.RequestDTO.DeleteLoanNoteRequest NoteRequest)
        {
            Model.ResponseDTO.DeleteLoanNoteResponse NoteResponse = new Model.ResponseDTO.DeleteLoanNoteResponse();
            NoteResponse = dtDeleteNote(NoteRequest);
            return NoteResponse;
        }
        wsMCS.Model.ResponseDTO.DeleteLoanNoteResponse dtDeleteNote(Model.RequestDTO.DeleteLoanNoteRequest NoteRequest)
        {
            Model.ResponseDTO.DeleteLoanNoteResponse NoteResponse = new Model.ResponseDTO.DeleteLoanNoteResponse();
            SqlCommand cmd = new SqlCommand("spDeleteLoanNote", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pNoteID", NoteRequest.NoteID);
            cmd.Parameters.AddWithValue("@pModifiedId", NoteRequest.ModifiedId);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                NoteResponse.isSuccess = true;
                NoteResponse.errorCode = "";
            }
            else
            {
                NoteResponse.isSuccess = false;
                NoteResponse.errorCode = pResult;
            }

            return NoteResponse;
        }
        public DataTable dtNoteByLoanID(int LoanID)
        {
            DataTable dt = new DataTable("SelectedLoanNotes");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT U.FullName AS CreatedName , L.LoanNoteID, L.Note, L.LoanID, L.Status, L.CreatedID, 
                                                            Format(L.CreatedDate,'dd.MM.yyyy HH:MM') AS CreatedDate, L.ModifiedID, l.ModifiedDate 
                                                    FROM LoanNotes L iNNER JOIN USERS U ON L.CreatedID = U.ID  
                                                    WHERE L.Status = 'ACTIVE' AND L.LoanID = @LoanID
                                                    ORDER BY L.LoanNoteID DESC ", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@LoanID", LoanID);

            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public Model.ResponseDTO.DeleteCardResponse DeleteCard(Model.RequestDTO.DeleteCardRequest CardRequest)
        {
            Model.ResponseDTO.DeleteCardResponse CardResponse = new Model.ResponseDTO.DeleteCardResponse();
            CardResponse = dtDeleteCard(CardRequest);
            return CardResponse;
        }
        wsMCS.Model.ResponseDTO.DeleteCardResponse dtDeleteCard(Model.RequestDTO.DeleteCardRequest CardRequest)
        {
            Model.ResponseDTO.DeleteCardResponse DeleteCardrResponse = new Model.ResponseDTO.DeleteCardResponse();
            SqlCommand cmd = new SqlCommand("spDeleteCard", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pCardID", CardRequest.CardID);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                DeleteCardrResponse.isSuccess = true;
                DeleteCardrResponse.errorCode = "";
            }
            else
            {
                DeleteCardrResponse.isSuccess = false;
                DeleteCardrResponse.errorCode = pResult;
            }

            return DeleteCardrResponse;
        }
        public DataTable GetCards(Model.RequestDTO.GetCardRequest CardRequest)
        {
            DataTable dtCard = new DataTable("Card");
            SqlDataAdapter da = new SqlDataAdapter("select * from Card where Status = 'ACTIVE' and LoanId = @LoanId order by CardId desc", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@LoanId", CardRequest.LoanId);
            da.Fill(dtCard);
            return dtCard;
        }
        public DataTable dtCardrByID(int CardID)
        {
            DataTable dt = new DataTable("SelectedCard");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT Loan.DocumentNumber, Loan.BranchName, Loan.Fullname, Loan.CollateralNumber,
                                                            Loan.AgreementNumber, Loan.LoanAmount, t.* FROM Card t      
                                                     Left join (Select B.BranchName , C.FullName, L.* from Loan L
	                                                            LEFT Join Customer C ON L.CustomerID = C.CustomerID
		                                                        LEFT JOIN Branch B ON L.BranchID = B.BranchID) 
                                                     AS Loan On Loan.LoanID= t.LoanID
                                                     WHERE t.CardID = @CardID", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@CardID", CardID);

            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public Model.ResponseDTO.AddGuarantorResponse AddGuarantor(Model.RequestDTO.AddGuarantorRequest guarantorRequest)
        {
            Model.ResponseDTO.AddGuarantorResponse guarantorResponse = new Model.ResponseDTO.AddGuarantorResponse();
            guarantorResponse = dbAddGuarantor(guarantorRequest);
            return guarantorResponse;
        }
        wsMCS.Model.ResponseDTO.AddGuarantorResponse dbAddGuarantor(Model.RequestDTO.AddGuarantorRequest addGuarantorRequest)
        {
            wsMCS.Model.ResponseDTO.AddGuarantorResponse addGuarantorResponse = new Model.ResponseDTO.AddGuarantorResponse();

            SqlCommand cmd = new SqlCommand("spAddGuarantor", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pCustomerID", addGuarantorRequest.CusomterID);
            cmd.Parameters.AddWithValue("@pLoanID", addGuarantorRequest.LoanID);
            cmd.Parameters.AddWithValue("@pCreatedID", addGuarantorRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                addGuarantorResponse.isSuccess = true;
                addGuarantorResponse.errorCode = "";
            }
            else
            {
                addGuarantorResponse.isSuccess = false;
                addGuarantorResponse.errorCode = pResult;
            }

            return addGuarantorResponse;
        }
        public Model.ResponseDTO.DeleteGuarantorResponse DeleteGuarantor(Model.RequestDTO.DeleteGuarantorRequest guarantorRequest)
        {
            Model.ResponseDTO.DeleteGuarantorResponse guarantorResponse = new Model.ResponseDTO.DeleteGuarantorResponse();
            guarantorResponse = dtDeleteGuarantor(guarantorRequest);
            return guarantorResponse;
        }
        wsMCS.Model.ResponseDTO.DeleteGuarantorResponse dtDeleteGuarantor(Model.RequestDTO.DeleteGuarantorRequest guarantorRequest)
        {
            Model.ResponseDTO.DeleteGuarantorResponse DeleteGuarantorResponse = new Model.ResponseDTO.DeleteGuarantorResponse();
            SqlCommand cmd = new SqlCommand("spDeleteGuarantor", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pGuarantorID", guarantorRequest.GuarantorID);
            cmd.Parameters.AddWithValue("@pModifiedID", guarantorRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                DeleteGuarantorResponse.isSuccess = true;
                DeleteGuarantorResponse.errorCode = "";
            }
            else
            {
                DeleteGuarantorResponse.isSuccess = false;
                DeleteGuarantorResponse.errorCode = pResult;
            }

            return DeleteGuarantorResponse;
        }
        public DataTable GetGuarantor(int LoanID)
        {
            DataTable dt = new DataTable("GuarantorByLoanID");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT C.Fullname AS GuarantorName, C.IdCardSeries + ' ' + C.idCardNumber AS Document, C.MobilePhone1, C.MobilePhone2, G.* 
                                                     FROM Guarantor G INNER JOIN Customer C ON G.CustomerID = C.CustomerID  
                                                     WHERE G.Status = 'ACTIVE' and LoanID = @LoanID", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@LoanID", LoanID);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public Model.ResponseDTO.AddLoanDocumentResponse AddLoanDocument(Model.RequestDTO.AddLoanDocumentRequest documentRequest)
        {
            Model.ResponseDTO.AddLoanDocumentResponse documentResponse = new Model.ResponseDTO.AddLoanDocumentResponse();
            documentResponse = dbAddDocument(documentRequest);
            return documentResponse;
        }
        wsMCS.Model.ResponseDTO.AddLoanDocumentResponse dbAddDocument(Model.RequestDTO.AddLoanDocumentRequest documentRequest)
        {
            wsMCS.Model.ResponseDTO.AddLoanDocumentResponse documnentResponse = new Model.ResponseDTO.AddLoanDocumentResponse();

            SqlCommand cmd = new SqlCommand("spAddLoanDocument", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pLoanID", documentRequest.LoanID);
            cmd.Parameters.AddWithValue("@pFileName", documentRequest.FileName);
            cmd.Parameters.AddWithValue("@pNote", documentRequest.Note);
            cmd.Parameters.AddWithValue("@pCreatedID", documentRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                documnentResponse.isSuccess = true;
                documnentResponse.errorCode = "";
            }
            else
            {
                documnentResponse.isSuccess = false;
                documnentResponse.errorCode = pResult;
            }

            return documnentResponse;
        }
        public Model.ResponseDTO.DeleteLoanDocumentResponse DeleteDocument(Model.RequestDTO.DeleteLoanDocumentRequest documentRequest)
        {
            Model.ResponseDTO.DeleteLoanDocumentResponse documentResponse = new Model.ResponseDTO.DeleteLoanDocumentResponse();
            documentResponse = dtDeleteDocument(documentRequest);
            return documentResponse;
        }
        wsMCS.Model.ResponseDTO.DeleteLoanDocumentResponse dtDeleteDocument(Model.RequestDTO.DeleteLoanDocumentRequest documentRequest)
        {
            Model.ResponseDTO.DeleteLoanDocumentResponse DeleteDocumentResponse = new Model.ResponseDTO.DeleteLoanDocumentResponse();
            SqlCommand cmd = new SqlCommand("spDeleteLoanDocument", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pDocumentID", documentRequest.LoanDocumentID);
            cmd.Parameters.AddWithValue("@pModifiedID", documentRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                DeleteDocumentResponse.isSuccess = true;
                DeleteDocumentResponse.errorCode = "";
            }
            else
            {
                DeleteDocumentResponse.isSuccess = false;
                DeleteDocumentResponse.errorCode = pResult;
            }

            return DeleteDocumentResponse;
        }
        public DataTable GetDocuments(int LoanID)
        {
            DataTable dt = new DataTable("DocumentsByLoanID");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM LoanDocuments 
                                                     WHERE status = 'ACTIVE' AND LoanID = @LoanID", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@LoanID", LoanID);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public Model.ResponseDTO.AddCollateralResponse AddCollateral(Model.RequestDTO.AddCollateralRequest collateralRequest)
        {
            Model.ResponseDTO.AddCollateralResponse collateralResponse = new Model.ResponseDTO.AddCollateralResponse();
            collateralResponse = dbAddCollateral(collateralRequest);
            return collateralResponse;
        }
        wsMCS.Model.ResponseDTO.AddCollateralResponse dbAddCollateral(Model.RequestDTO.AddCollateralRequest collateralRequest)
        {
            wsMCS.Model.ResponseDTO.AddCollateralResponse collateralResponse = new Model.ResponseDTO.AddCollateralResponse();

            SqlCommand cmd = new SqlCommand("spAddLoanCollateral", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pCollateralTypeID", collateralRequest.CollateralTypeID);
            cmd.Parameters.AddWithValue("@pCollateralName", collateralRequest.CollateralName);
            cmd.Parameters.AddWithValue("@pUnitOfMeasurement", collateralRequest.UnitOfMeasurement);
            cmd.Parameters.AddWithValue("@pCaratID", collateralRequest.CaratID);
            cmd.Parameters.AddWithValue("@pPrice", collateralRequest.Price);
            cmd.Parameters.AddWithValue("@pCount", collateralRequest.Count);
            cmd.Parameters.AddWithValue("@pBruttoWeight", collateralRequest.BruttoWeight);
            cmd.Parameters.AddWithValue("@pWeight", collateralRequest.Weight);
            cmd.Parameters.AddWithValue("@pTransaction", collateralRequest.Transaction);
            cmd.Parameters.AddWithValue("@pNote", collateralRequest.Note);
            cmd.Parameters.AddWithValue("@pLoanID", collateralRequest.LoanID);
            cmd.Parameters.AddWithValue("@pCreatedID", collateralRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                collateralResponse.isSuccess = true;
                collateralResponse.errorCode = "";
            }
            else
            {
                collateralResponse.isSuccess = false;
                collateralResponse.errorCode = pResult;
            }

            return collateralResponse;
        }
        public Model.ResponseDTO.ModifyCollateralResponse ModifyCollateral(Model.RequestDTO.ModifyCollateralRequest collateralRequest)
        {
            Model.ResponseDTO.ModifyCollateralResponse collateralResponse = new Model.ResponseDTO.ModifyCollateralResponse();
            collateralResponse = dbModifyCollateral(collateralRequest);
            return collateralResponse;
        }
        wsMCS.Model.ResponseDTO.ModifyCollateralResponse dbModifyCollateral(Model.RequestDTO.ModifyCollateralRequest collateralRequest)
        {
            wsMCS.Model.ResponseDTO.ModifyCollateralResponse collateralResponse = new Model.ResponseDTO.ModifyCollateralResponse();

            SqlCommand cmd = new SqlCommand("spModifyLoanCollateral", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pCollateralID", collateralRequest.CollateralID);
            cmd.Parameters.AddWithValue("@pCollateralTypeID", collateralRequest.CollateralTypeID);
            cmd.Parameters.AddWithValue("@pCollateralName", collateralRequest.CollateralName);
            cmd.Parameters.AddWithValue("@pUnitOfMeasurement", collateralRequest.UnitOfMeasurement);
            cmd.Parameters.AddWithValue("@pCaratID", collateralRequest.CaratID);
            cmd.Parameters.AddWithValue("@pPrice", collateralRequest.Price);
            cmd.Parameters.AddWithValue("@pCount", collateralRequest.Count);
            cmd.Parameters.AddWithValue("@pBruttoWeight", collateralRequest.BruttoWeight);
            cmd.Parameters.AddWithValue("@pWeight", collateralRequest.Weight);
            cmd.Parameters.AddWithValue("@pNote", collateralRequest.Note);
            cmd.Parameters.AddWithValue("@pModifiedID", collateralRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                collateralResponse.isSuccess = true;
                collateralResponse.errorCode = "";
            }
            else
            {
                collateralResponse.isSuccess = false;
                collateralResponse.errorCode = pResult;
            }

            return collateralResponse;
        }
        public Model.ResponseDTO.DeleteCollateralResponse DeleteCollateral(Model.RequestDTO.DeleteCollateralRequest collateralRequest)
        {
            Model.ResponseDTO.DeleteCollateralResponse collateralResponse = new Model.ResponseDTO.DeleteCollateralResponse();
            collateralResponse = dtDeleteCollateral(collateralRequest);
            return collateralResponse;
        }
        wsMCS.Model.ResponseDTO.DeleteCollateralResponse dtDeleteCollateral(Model.RequestDTO.DeleteCollateralRequest collateralRequest)
        {
            Model.ResponseDTO.DeleteCollateralResponse collateralResponse = new Model.ResponseDTO.DeleteCollateralResponse();
            SqlCommand cmd = new SqlCommand("spDeleteCollateral", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pCollateralID", collateralRequest.CollateralID);
            cmd.Parameters.AddWithValue("@pModifiedID", collateralRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                collateralResponse.isSuccess = true;
                collateralResponse.errorCode = "";
            }
            else
            {
                collateralResponse.isSuccess = false;
                collateralResponse.errorCode = pResult;
            }

            return collateralResponse;
        }
        public Model.ResponseDTO.SetLoanLenderResponse SetLender(Model.RequestDTO.setLoanLenderRequest lenderRequest)
        {
            Model.ResponseDTO.SetLoanLenderResponse lenderResponse = new Model.ResponseDTO.SetLoanLenderResponse();
            lenderResponse = dtSetLender(lenderRequest);
            return lenderResponse;
        }
        wsMCS.Model.ResponseDTO.SetLoanLenderResponse dtSetLender(Model.RequestDTO.setLoanLenderRequest lenderRequest)
        {
            Model.ResponseDTO.SetLoanLenderResponse lenderResponse = new Model.ResponseDTO.SetLoanLenderResponse();
            SqlCommand cmd = new SqlCommand("spSetLender", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pLoanID", lenderRequest.LoanID);
            cmd.Parameters.AddWithValue("@pLenderID", lenderRequest.LenderID);
            cmd.Parameters.AddWithValue("@pModifiedID", lenderRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                lenderResponse.isSuccess = true;
                lenderResponse.errorCode = "";
            }
            else
            {
                lenderResponse.isSuccess = false;
                lenderResponse.errorCode = pResult;
            }

            return lenderResponse;
        }
        public DataTable GetLender(int LoanID)
        {
            DataTable dt = new DataTable("LenderByLoanID");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT LenderID FROM Loan 
                                                     WHERE LoanID = @LoanID", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@LoanID", LoanID);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public Model.ResponseDTO.SetLoanVerifierResponse SetVerifier(Model.RequestDTO.setLoanVerifierRequest verifierRequest)
        {
            Model.ResponseDTO.SetLoanVerifierResponse verifierResponse = new Model.ResponseDTO.SetLoanVerifierResponse();
            verifierResponse = dtSetVerifier(verifierRequest);
            return verifierResponse;
        }
        wsMCS.Model.ResponseDTO.SetLoanVerifierResponse dtSetVerifier(Model.RequestDTO.setLoanVerifierRequest verifierRequest)
        {
            Model.ResponseDTO.SetLoanVerifierResponse verifierResponse = new Model.ResponseDTO.SetLoanVerifierResponse();
            SqlCommand cmd = new SqlCommand("spSetVerifier", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pLoanID", verifierRequest.LoanID);
            cmd.Parameters.AddWithValue("@pVerifierID", verifierRequest.VerifierID);
            cmd.Parameters.AddWithValue("@pModifiedID", verifierRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                verifierResponse.isSuccess = true;
                verifierResponse.errorCode = "";
            }
            else
            {
                verifierResponse.isSuccess = false;
                verifierResponse.errorCode = pResult;
            }

            return verifierResponse;
        }
        public DataTable GetVerifier(int LoanID)
        {
            DataTable dt = new DataTable("VerifierByLoanID");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT VerifierID FROM Loan 
                                                     WHERE LoanID = @LoanID", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@LoanID", LoanID);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }

        public bool LoanIsApproved(int LoanId)
        {
            SqlCommand cmd = new SqlCommand("select IsApprove from Loan where LoanID = @LoanID", Utils.sqlConn);
            cmd.Parameters.Add("@LoanID", SqlDbType.Int).Value = LoanId;
            bool isApprove = false;
            try
            {
                cmd.Connection.Open();
                // object o = cmd.ExecuteScalar();
                isApprove = Convert.ToBoolean(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                isApprove = false;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return isApprove;
        }
        public DataTable GoldCarat()
        {
            DataTable dt = new DataTable("VerifierByLoanID");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM GoldCarat ORDER BY GoldCarat", Utils.sqlConn);

            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable DocumentList()
        {
            DataTable dt = new DataTable("DocumentList");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM Document ORDER BY DocumentName", Utils.sqlConn);

            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable CollateralList(int LoanID)
        {
            DataTable dt = new DataTable("CollateralByLoanID");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM V_CollateralList WHERE LoanID = @LoanID 
                                                              ORDER BY [Transaction], Cdate  ", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@LoanID", LoanID);
            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                dt = null;
            }
            return dt;
        }
        public DataTable DtCollateralByID(int CollateralID)
        {
            DataTable dt = new DataTable("CollateralByID");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM V_CollateralList WHERE CollateralID = @CollateralID 
                                                              ORDER BY CollateralID  ", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@CollateralID ", CollateralID);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable DtPaymentSchedule(int LoanID)
        {
            DataTable dt = new DataTable("PaymentScheduleByID");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM PaymentScedule WHERE LoanID = @LoanID
                                                              ORDER BY Period ", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@LoanID", LoanID);

            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable DtLoanFullData(int LoanID)
        {
            DataTable dt = new DataTable("LoanFullDataByID");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT dbo.TotalPrinsipalByLoanID(@LoanID) AS PrincipalDebtAmount, 
                                                            dbo.TotalRateByLoanID(@LoanID) AS RateDebtAmount,
                                                            dbo.TotalPenaltyByLoanID(@LoanID) AS PenaltyDebtAmount,
                                                            dbo.TotalDebtByLoanID(@LoanID) AS TotalDebtAmount,
                                                            dbo.MonthlyRateByLoanID(@LoanID) AS MonthlyInterestAmount,
                                                            dbo.MonthlyPaymentyLoanID(@LoanID) AS MonthlyPayment,
                                                            dbo.TotalCurrentDebtByLoanID(@LoanID) AS CurrentDebtAmount,
                                                            dbo.CurrentBalanceByLoanID(@LoanID) AS CurrentBalance,
                                                            dbo.DaysOfRespite(@LoanID) AS RespiteCount,
                                                            dbo.MonthCount(@LoanID) AS MonthCount , 
                                                            dbo.DayCount(@LoanID) AS DayCount ,V1.* 
                                                     FROM V_LoanFullData V1 
                                                     WHERE V1.LoanID = @LoanID  ", Utils.sqlConn);

            da.SelectCommand.Parameters.AddWithValue("@LoanID", LoanID);

            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public string loanStatus(int LoanId)
        {
            SqlCommand cmd = new SqlCommand(@"SELECT Status FROM Loan WHERE LoanID =  @LoanID", Utils.sqlConn);
            cmd.Parameters.Add("@LoanID", SqlDbType.Int).Value = LoanId;
            string LoanStatus = "";
            try
            {
                cmd.Connection.Open();
                // object o = cmd.ExecuteScalar();
                LoanStatus = Convert.ToString(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                LoanStatus = "";
            }
            finally
            {
                cmd.Connection.Close();
            }
            return LoanStatus;
        }
        public Model.ResponseDTO.acceptLoanResponse acceptLoan(Model.RequestDTO.acceptLoanRequest acceptRequest)
        {
            Model.ResponseDTO.acceptLoanResponse acceptResponse = new Model.ResponseDTO.acceptLoanResponse();
            acceptResponse = dtAcceptLoan(acceptRequest);
            return acceptResponse;
        }
        wsMCS.Model.ResponseDTO.acceptLoanResponse dtAcceptLoan(Model.RequestDTO.acceptLoanRequest acceptRequest)
        {
            Model.ResponseDTO.acceptLoanResponse acceptResponse = new Model.ResponseDTO.acceptLoanResponse();
            SqlCommand cmd = new SqlCommand("spAcceptLoanContract", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pLoanID", acceptRequest.LoanID);
            cmd.Parameters.AddWithValue("@pTrandate", Config.HostingTime);
            cmd.Parameters.AddWithValue("@pDescription", acceptRequest.Description);
            cmd.Parameters.AddWithValue("@pCreatedID", acceptRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                acceptResponse.isSuccess = true;
                acceptResponse.errorCode = "";
            }
            else
            {
                acceptResponse.isSuccess = false;
                acceptResponse.errorCode = pResult;
            }

            return acceptResponse;
        }

        public Model.ResponseDTO.SetResponse InsertCollateralOut(Model.RequestDTO.insertCollateralOutRequest Request)
        {
            Model.ResponseDTO.SetResponse InsertCollateralOutResponse = new Model.ResponseDTO.SetResponse();
            InsertCollateralOutResponse = spInsertCollateralOut(Request);
            return InsertCollateralOutResponse;
        }
        wsMCS.Model.ResponseDTO.SetResponse spInsertCollateralOut(Model.RequestDTO.insertCollateralOutRequest Request)
        {
            Model.ResponseDTO.SetResponse InsertCollateralOutResponse = new Model.ResponseDTO.SetResponse();
            SqlCommand cmd = new SqlCommand("spInsertCollateralOut", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pLoanID", Request.LoanID);
            cmd.Parameters.AddWithValue("@pCreatedID", Request.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                InsertCollateralOutResponse.isSuccess = true;
                InsertCollateralOutResponse.errorCode = "";
            }
            else
            {
                InsertCollateralOutResponse.isSuccess = false;
                InsertCollateralOutResponse.errorCode = pResult;
            }

            return InsertCollateralOutResponse;
        }
        public Model.ResponseDTO.SetResponse CollateralReturn(Model.RequestDTO.CollateralReturnRequest Request)
        {
            Model.ResponseDTO.SetResponse ReturnResponse = new Model.ResponseDTO.SetResponse();
            ReturnResponse = CollateralReturnDB(Request);
            return ReturnResponse;
        }
        wsMCS.Model.ResponseDTO.SetResponse CollateralReturnDB(Model.RequestDTO.CollateralReturnRequest Request)
        {
            Model.ResponseDTO.SetResponse CollateralResponse = new Model.ResponseDTO.SetResponse();
            SqlCommand cmd = new SqlCommand("spCollateralRetrun", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pLoanID", Request.LoanID);
            cmd.Parameters.AddWithValue("@pModifiedID", Request.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                CollateralResponse.isSuccess = true;
                CollateralResponse.errorCode = "";
            }
            else
            {
                CollateralResponse.isSuccess = false;
                CollateralResponse.errorCode = pResult;
            }

            return CollateralResponse;
        }


        public DataTable dtBarCodeCollateralListByLoanID(int loanID)
        {
            DataTable dt = new DataTable("CollateralList");
            SqlDataAdapter da = new SqlDataAdapter(@"select * from V_LoanCollateralList where LoanID = @pLoanID", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@pLoanID ", loanID);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public Model.ResponseDTO.SetResponse DeleteLoan(Model.RequestDTO.DeleteLoanByIDRequest request)
        {
            Model.ResponseDTO.SetResponse deleteResponse = new Model.ResponseDTO.SetResponse();
            deleteResponse = dbDeleteResponse(request);
            return deleteResponse;
        }
        wsMCS.Model.ResponseDTO.SetResponse dbDeleteResponse(Model.RequestDTO.DeleteLoanByIDRequest request)
        {
            Model.ResponseDTO.SetResponse deleteResponse = new Model.ResponseDTO.SetResponse();
            SqlCommand cmd = new SqlCommand("spDeleteLoanByID", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pLoanID", request.LoanID);
            cmd.Parameters.AddWithValue("@pDescription", request.Description);
            cmd.Parameters.AddWithValue("@pModifiedID", request.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;
            cmd.Parameters.Add(ParamResult);
            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                deleteResponse.isSuccess = true;
                deleteResponse.errorCode = "";
            }
            else
            {
                deleteResponse.isSuccess = false;
                deleteResponse.errorCode = pResult;
            }
            return deleteResponse;
        }
        public Model.ResponseDTO.GetCurAcntResponse GetCurAcntByLoanID(Model.RequestDTO.GetCurAcntRequest CurAcntRequest)
        {
            Model.ResponseDTO.GetCurAcntResponse CurAcntResponse = new Model.ResponseDTO.GetCurAcntResponse();
            CurAcntResponse = dbCurAcntData(CurAcntRequest);
            return CurAcntResponse;
        }
        wsMCS.Model.ResponseDTO.GetCurAcntResponse dbCurAcntData(Model.RequestDTO.GetCurAcntRequest CurAcntRequest)
        {
            int pAcntID = 0;
            string pAcntName = "";
            string pAcntCode = "";
            int pCurrencyID = 0;
            decimal pCurBalance = 0;

            wsMCS.Model.ResponseDTO.GetCurAcntResponse CurAcntResponse = new Model.ResponseDTO.GetCurAcntResponse();

            SqlCommand cmd = new SqlCommand("spGetCurAcntByLoanID", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT

            cmd.Parameters.AddWithValue("@pLoanID", CurAcntRequest.LoanID);

            SqlParameter acntID = new SqlParameter("@pAcntID", SqlDbType.Int);
            acntID.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(acntID);

            SqlParameter acntName = new SqlParameter("@pAcntName", SqlDbType.NVarChar);
            acntName.Direction = ParameterDirection.Output;
            acntName.Size = 300;
            cmd.Parameters.Add(acntName);

            SqlParameter acntCode = new SqlParameter("@pAcntCode", SqlDbType.NVarChar);
            acntCode.Direction = ParameterDirection.Output;
            acntCode.Size = 100;
            cmd.Parameters.Add(acntCode);

            SqlParameter acntCurrency = new SqlParameter("@pCurrencyID", SqlDbType.Int);
            acntCurrency.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(acntCurrency);

            SqlParameter acntCurrencyName = new SqlParameter("@pCurrencyName", SqlDbType.VarChar);
            acntCurrencyName.Direction = ParameterDirection.Output;
            acntCurrencyName.Size = 3;
            cmd.Parameters.Add(acntCurrencyName);

            SqlParameter acntBalance = new SqlParameter("@pCurBalance", SqlDbType.Decimal);
            acntBalance.Precision = 18;
            acntBalance.Scale = 2;
            acntBalance.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(acntBalance);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                if (pResult == "OK")
                {
                    pAcntID = Convert.ToInt32(cmd.Parameters["@pAcntID"].Value);
                    pAcntName = Convert.ToString(cmd.Parameters["@pAcntName"].Value);
                    pAcntCode = Convert.ToString(cmd.Parameters["@pAcntCode"].Value);
                    pCurrencyID = Convert.ToInt32(cmd.Parameters["@pCurrencyID"].Value);
                    pCurBalance = Convert.ToDecimal(cmd.Parameters["@pCurBalance"].Value);
                    CurAcntResponse.isSuccess = true;
                    CurAcntResponse.errorCode = "";
                    CurAcntResponse.AcntID = pAcntID;
                    CurAcntResponse.AcntName = pAcntName;
                    CurAcntResponse.AcntCode = pAcntCode;
                    CurAcntResponse.CurrencyID = pCurrencyID;
                    CurAcntResponse.CurBalance = pCurBalance;
                    CurAcntResponse.CurrencyName = Convert.ToString(cmd.Parameters["@pCurrencyName"].Value);
                }
                else
                {
                    CurAcntResponse.isSuccess = false;
                    CurAcntResponse.errorCode = pResult;
                    CurAcntResponse.AcntID = 0;
                    CurAcntResponse.AcntName = "";
                    CurAcntResponse.AcntCode = "";
                    CurAcntResponse.CurrencyID = 0;
                    CurAcntResponse.CurBalance = 0;
                    CurAcntResponse.CurrencyName = "";
                }
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return CurAcntResponse;
        }
        public DataTable dtPaymentType()
        {
            DataTable dt = new DataTable("PaymentType");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT * FROM PaymentType ORDER BY PaymentTypeID", Utils.sqlConn);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public Model.ResponseDTO.GetPaymentDebtResponse dbGetPaymentDebtByLoanID(Model.RequestDTO.getPaymentDebtByLoanIDRequest debtRequest)
        {
            Model.ResponseDTO.GetPaymentDebtResponse debtResponse = new Model.ResponseDTO.GetPaymentDebtResponse();
            debtResponse = dbGetPaymentDebt(debtRequest);
            return debtResponse;
        }
        wsMCS.Model.ResponseDTO.GetPaymentDebtResponse dbGetPaymentDebt(Model.RequestDTO.getPaymentDebtByLoanIDRequest debtRequest)
        {
            decimal pPayedPrincipalAmount = 0;
            decimal pPayedExpPrincipalAmount = 0;
            decimal pPayedRateAmount = 0;
            decimal pPayedExpRateAmount = 0;
            decimal pPayedPenaltyAmount = 0;
            decimal pPayedTotalAmount = 0;

            wsMCS.Model.ResponseDTO.GetPaymentDebtResponse debtResponse = new Model.ResponseDTO.GetPaymentDebtResponse();

            SqlCommand cmd = new SqlCommand("spGetPaymentDebtByLoanID", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pLoanID", debtRequest.LoanID);
            cmd.Parameters.AddWithValue("@pMonthCount", debtRequest.MonthCount);
            cmd.Parameters.AddWithValue("@pPaymentType", debtRequest.PaymentType);
            cmd.Parameters.AddWithValue("@pAmount", debtRequest.Amount);

            SqlParameter payedPrincipalAmount = new SqlParameter("@pPayedPrincipalAmount", SqlDbType.Decimal);
            payedPrincipalAmount.Direction = ParameterDirection.Output;
            payedPrincipalAmount.Scale = 2;
            cmd.Parameters.Add(payedPrincipalAmount);
            SqlParameter payedExpPrincipalAmount = new SqlParameter("@pPayedExpPrincipalAmount", SqlDbType.Decimal);
            payedExpPrincipalAmount.Direction = ParameterDirection.Output;
            payedExpPrincipalAmount.Scale = 2;
            cmd.Parameters.Add(payedExpPrincipalAmount);
            SqlParameter payedRateAmount = new SqlParameter("@pPayedRateAmount", SqlDbType.Decimal);
            payedRateAmount.Direction = ParameterDirection.Output;
            payedRateAmount.Scale = 2;
            cmd.Parameters.Add(payedRateAmount);
            SqlParameter payedExpRateAmount = new SqlParameter("@pPayedExpRateAmount", SqlDbType.Decimal);
            payedExpRateAmount.Direction = ParameterDirection.Output;
            payedExpRateAmount.Scale = 2;
            cmd.Parameters.Add(payedExpRateAmount);
            SqlParameter payedPenaltyAmount = new SqlParameter("@pPayedPenaltyAmount", SqlDbType.Decimal);
            payedPenaltyAmount.Direction = ParameterDirection.Output;
            payedPenaltyAmount.Scale = 2;
            cmd.Parameters.Add(payedPenaltyAmount);
            SqlParameter payedTotalAmount = new SqlParameter("@pPayedTotalAmount", SqlDbType.Decimal);
            payedTotalAmount.Direction = ParameterDirection.Output;
            payedTotalAmount.Scale = 2;
            cmd.Parameters.Add(payedTotalAmount);
            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                if (pResult == "OK")
                {
                    pPayedPrincipalAmount = Convert.ToDecimal(cmd.Parameters["@pPayedPrincipalAmount"].Value);
                    pPayedExpPrincipalAmount = Convert.ToDecimal(cmd.Parameters["@pPayedExpPrincipalAmount"].Value);
                    pPayedRateAmount = Convert.ToDecimal(cmd.Parameters["@pPayedRateAmount"].Value);
                    pPayedExpRateAmount = Convert.ToDecimal(cmd.Parameters["@pPayedExpRateAmount"].Value);
                    pPayedPenaltyAmount = Convert.ToDecimal(cmd.Parameters["@pPayedPenaltyAmount"].Value);
                    pPayedTotalAmount = Convert.ToDecimal(cmd.Parameters["@pPayedTotalAmount"].Value);
                    debtResponse.isSuccess = true;
                    debtResponse.errorCode = "";
                    debtResponse.payedPrincipalAmount = pPayedPrincipalAmount;
                    debtResponse.payedExpPrincipalAmount = pPayedExpPrincipalAmount;
                    debtResponse.payedRateAmount = pPayedRateAmount;
                    debtResponse.payedExpRateAmount = pPayedExpRateAmount;
                    debtResponse.payedPenaltyAmount = pPayedPenaltyAmount;
                    debtResponse.payedTotalAmount = pPayedTotalAmount;
                }
                else
                {
                    debtResponse.isSuccess = false;
                    debtResponse.errorCode = pResult;
                    debtResponse.payedPrincipalAmount = 0;
                    debtResponse.payedExpPrincipalAmount = 0;
                    debtResponse.payedRateAmount = 0;
                    debtResponse.payedExpRateAmount = 0;
                    debtResponse.payedPenaltyAmount = 0;
                    debtResponse.payedTotalAmount = 0;
                }
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return debtResponse;
        }
        public DataTable dtLoanPayment(int LoanID)
        {
            DataTable dt = new DataTable("LoanPayment");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT P.MsgID_Part, P.PaymentID, P.LoanID, FORMAT(P.PaymentDate,'dd.MM.yyyy') AS PaymentDate, ISNULL(P.DateOfRespite,0) AS DateOfRespite, 
                                                            P.MonthCount, P.PayedPrincipalAmount, P.PayedRateAmount, P.PayedPenalytAmount, 
	                                                        (ISNULL(P.PayedPrincipalAmount,0) + ISNULL(P.PayedRateAmount,0) + ISNULL(P.PayedPenalytAmount,0)) AS TotalAmount , 
	                                                        ISNULL(P.Note, '') AS Note, P.CreatedID, P.CreatedDate
                                                     FROM LoanPayment P
	                                                 WHERE P.LoanID = @pLoanID and Status = 'ACTIVE' ORDER BY P.PaymentID DESC", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@pLoanID", LoanID);
            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                dt = null;
            }
            return dt;
        }
        public DataTable dtCollateralOutList(Model.RequestDTO.GetCollateralOutListRequest dataRequest)
        {
            DataTable dt = new DataTable("CollateralOutList");

            string sql_query = @"SELECT C.OutID, B.BranchName, Format(C.OutDate, 'dd.MM.yyyy') as OutDate, C.FullName,
                                      C.DepositBox, C.CollateralNumber, C.Note,  L.LoanID,  L.BranchID  FROM CollateralOut C
		                              INNER JOIN Loan L ON C.LoanID = L.LoanID
		                              INNER JOIN Branch B ON L.BranchID = B.BranchID
                                      WHERE Format(C.OutDate, 'dd.MM.yyyy') BETWEEN @pStartDate AND @pEndDate AND C.Status = 'ACTIVE' AND
	                                  L.BranchID IN (SELECT vb.BranchID FROM v_UserBranch vb WHERE vb.UserID = @pUserID) {0}
                                      ORDER BY C.OutID , B.BranchName, C.DepositBox DESC ";

            string sql_sub_query = " ";
            string pResult = "";
            if (dataRequest.BranchID != 0)
            {
                sql_sub_query += " AND L.BranchID =  @pBranchID";
            }
            if (dataRequest.DepositBox != "")
            {
                sql_sub_query += " AND C.DepositBox Like  @pDepositBox";
            }
            if (dataRequest.CollateralNumber != "")
            {
                sql_sub_query += " AND C.CollateralNumber Like  @pCollateralNumber";
            }

            string _sql = string.Format(sql_query, sql_sub_query).Trim();

            SqlDataAdapter da = new SqlDataAdapter(_sql, Utils.sqlConn);

            da.SelectCommand.Parameters.AddWithValue("@pUserID", dataRequest.UserID);
            da.SelectCommand.Parameters.AddWithValue("@pStartDate", dataRequest.StartDate);
            da.SelectCommand.Parameters.AddWithValue("@pEndDate", dataRequest.EndDate);

            if (dataRequest.BranchID != 0)
            {
                da.SelectCommand.Parameters.Add("@pBranchID", SqlDbType.Int).Value = dataRequest.BranchID;
            }
            if (dataRequest.DepositBox != "")
            {
                da.SelectCommand.Parameters.Add("@pDepositBox", SqlDbType.NVarChar, 32767).Value = "%" + dataRequest.DepositBox + "%";
            }
            if (dataRequest.CollateralNumber != "")
            {
                da.SelectCommand.Parameters.Add("@pCollateralNumber", SqlDbType.NVarChar, 32767).Value = "%" + dataRequest.CollateralNumber + "%";
            }

            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
                dt = null;
            }
            return dt;
        }
        public Model.ResponseDTO.SetResponse DeleteCollateralOut(Model.RequestDTO.DeleteCollateralByOutIDRequest request)
        {
            Model.ResponseDTO.SetResponse deleteResponse = new Model.ResponseDTO.SetResponse();
            deleteResponse = dbDeleteResponse(request);
            return deleteResponse;
        }
        wsMCS.Model.ResponseDTO.SetResponse dbDeleteResponse(Model.RequestDTO.DeleteCollateralByOutIDRequest request)
        {
            Model.ResponseDTO.SetResponse deleteResponse = new Model.ResponseDTO.SetResponse();
            SqlCommand cmd = new SqlCommand("spDeleteCollateralOutByOutID", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pOutID", request.OutID);
            cmd.Parameters.AddWithValue("@pDescription", request.Description);
            cmd.Parameters.AddWithValue("@pModifiedID", request.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;
            cmd.Parameters.Add(ParamResult);
            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                deleteResponse.isSuccess = true;
                deleteResponse.errorCode = "";
            }
            else
            {
                deleteResponse.isSuccess = false;
                deleteResponse.errorCode = pResult;
            }
            return deleteResponse;
        }
        public DataTable dtCollateralList(Model.RequestDTO.GetCollateralListRequest dataRequest)
        {
            DataTable dtCollateralList = new DataTable("CollateralList");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetCollateralList", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pUserID", dataRequest.UserID);
                    cmd.Parameters.AddWithValue("@pCurrencyID", dataRequest.CurrencyID);
                    cmd.Parameters.AddWithValue("@pStartDate", dataRequest.StartDate);
                    cmd.Parameters.AddWithValue("@pEndDate", dataRequest.EndDate);
                    cmd.Parameters.AddWithValue("@pBranchID", dataRequest.BranchID);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtCollateralList.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        pResult = error;
                    }
                    return dtCollateralList;
                }
            }
        }
        public DataTable dtGetTempCollateralList(Model.RequestDTO.GetTempCollateralListRequest dataRequest)
        {
            DataTable dtTempCollateralList = new DataTable("TempCollateralList");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetTempCollateralList", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pDepositBox", dataRequest.DepositBox);
                    cmd.Parameters.AddWithValue("@pCurrencyID", dataRequest.CurrencyID);
                    cmd.Parameters.AddWithValue("@pBranchID", dataRequest.BranchID);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtTempCollateralList.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        pResult = error;
                    }
                    return dtTempCollateralList;
                }
            }
        }
        public DataTable dtGetDelayedLoansList(Model.RequestDTO.GetDelayedLoansListRequest dataRequest)
        {
            string error = "";
            DataTable dtDelayedLoanList = new DataTable("GetDelayedLoan");
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetDelayedLoanList", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pUserID", dataRequest.UserID);
                    cmd.Parameters.AddWithValue("@pBranchID", dataRequest.BranchID);
                    cmd.Parameters.AddWithValue("@pDayOfLoan", dataRequest.DayOfLoan);
                    cmd.Parameters.AddWithValue("@pCurrencyID", dataRequest.CurrencyID);
                    cmd.Parameters.AddWithValue("@pCollateralNumber", dataRequest.CollateralNumber);
                    cmd.Parameters.AddWithValue("@pFullName", dataRequest.Fullname);
                    cmd.Parameters.AddWithValue("@pColorType", dataRequest.ColorType);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtDelayedLoanList.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        pResult = error;
                    }
                }
            }
            return dtDelayedLoanList;
        }
        public Model.ResponseDTO.SetResponse ChangeLoanStatus(Model.RequestDTO.ChangeLoanStatusRequest statusRequest)
        {
            Model.ResponseDTO.SetResponse statusResponse = new Model.ResponseDTO.SetResponse();
            statusResponse = dtChangeLoanStatus(statusRequest);
            return statusResponse;
        }
        wsMCS.Model.ResponseDTO.SetResponse dtChangeLoanStatus(Model.RequestDTO.ChangeLoanStatusRequest statusRequest)
        {
            string sql_Query = "";
            string tranDate = Config.HostingTime.ToString("dd.MM.yyyy");
            Model.ResponseDTO.SetResponse statusResponse = new Model.ResponseDTO.SetResponse();
            if (statusRequest.LoanStatusID == 3)
            {
                sql_Query = "UPDATE Loan SET LoanStatusID = '" + statusRequest.LoanStatusID + "', ActualExpiryDate = '" + tranDate + "' WHERE LoanID = @pLoanID";
            }
            if (statusRequest.LoanStatusID == 1)
            {
                sql_Query = "UPDATE Loan SET LoanStatusID = '" + statusRequest.LoanStatusID + "', ActualExpiryDate = '' WHERE LoanID = @pLoanID";
            }
            SqlCommand cmd = new SqlCommand(sql_Query, Utils.sqlConn);
        
        //INPUT
            cmd.Parameters.AddWithValue("@pLoanID", statusRequest.LoanID);
            cmd.Parameters.AddWithValue("@pLoanStatusID", statusRequest.LoanStatusID);
            cmd.Parameters.AddWithValue("@pModifiedID", statusRequest.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            string pResult = "OK";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                statusResponse.isSuccess = true;
                statusResponse.errorCode = "";
            }
            else
            {
                statusResponse.isSuccess = false;
                statusResponse.errorCode = pResult;
            }

            return statusResponse;
        }
        public DataTable dtGetLoansByDateList(Model.RequestDTO.GetLoansByDateListRequest dataRequest)
        {
            string sql_query = @"SELECT (SELECT Count(CustomerID)  From Loan WHERE CustomerID = L.CustomerID ) AS LoanCount, 
	                 (SELECT BranchName FROM Branch WHERE BranchID = L.BranchID) AS BranchName,
	                C.FullName + ' - ' + L.StartDate + ' - ' + cast( L.LoanAmount as varchar) + ' ' + Cur.CurrencyName AS Description, 
                     L.CollateralNumber,  
					 L.LoanAmount - 
                     (select Isnull(sum(LP.PayedPrincipalAmount),0) FROM LoanPayment LP WHERE LP.LoanID = L.LoanID and LP.Status = 'ACTIVE') AS TotalBalance,				 
                     L.PaymentDate, C.MobilePhone1, C.MobilePhone2,  L.LoanNote
               from Loan L
   	               	    INNER JOIN Customer C ON C.CustomerID = L.CustomerID 
   	               	    INNER JOIN Currency Cur ON Cur.CurrencyID = L.CurrencyID
                     where L.LoanStatusID = 1 and L.Status = 'ACTIVE' and L.IsApprove =1
                           AND L.BranchID IN (SELECT BranchID FROM v_UserBranch WHERE UserID = @pUserID)
                           AND L.CurrencyID = @pCurrencyID 
						   AND TRY_PARSE(L.PaymentDate as date using 'en-gb') BETWEEN TRY_PARSE(@pDateStart as date using 'en-gb') AND 
															   TRY_PARSE(@pDateEnd as date using 'en-gb') {0}

                     ORDER BY TRY_PARSE(L.PaymentDate as date using 'en-gb') DESC";



            string sql_sub_query = " ";
            if (dataRequest.BranchID != 0)
            {
                sql_sub_query += " AND L.BranchID = @pBranchID";
            }
            if (dataRequest.CollateralNumber != "")
            {
                sql_sub_query += " AND L.CollateralNumber LIKE  @pCollateralNumber";
            }
            if (dataRequest.FullName != "")
            {
                sql_sub_query += " AND C.FullName LIKE  @pFullName";
            }


            string _sql = string.Format(sql_query, sql_sub_query).Trim();
            SqlDataAdapter da = new SqlDataAdapter(_sql, Utils.sqlConn);

            da.SelectCommand.Parameters.AddWithValue("@pUserID", dataRequest.UserID);
            da.SelectCommand.Parameters.AddWithValue("@pCurrencyID", dataRequest.CurrencyID);
            da.SelectCommand.Parameters.AddWithValue("@pDateStart", dataRequest.DateStart);
            da.SelectCommand.Parameters.AddWithValue("@pDateEnd", dataRequest.DateEnd);
            if (dataRequest.BranchID != 0)
            {
                da.SelectCommand.Parameters.Add("@pBranchID", SqlDbType.Int).Value = dataRequest.BranchID;
            }
            if (dataRequest.CollateralNumber != "")
            {
                da.SelectCommand.Parameters.Add("@pCollateralNumber", SqlDbType.NVarChar, 32767).Value = "%" + dataRequest.CollateralNumber + "%";
            }
            if (dataRequest.FullName != "")
            {
                da.SelectCommand.Parameters.Add("@pFullName", SqlDbType.NVarChar, 32767).Value = "%" + dataRequest.FullName + "%";
            }


            DataTable dtGetLoansByDateList = new DataTable("GetLoansByDateList");
            try
            {
                da.Fill(dtGetLoansByDateList);
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return null;
            }
            return dtGetLoansByDateList;
        }
        public DataTable dtGetControlLoansList(Model.RequestDTO.GetControlLoansListRequest dataRequest)
        {

            string pCurrentDate = Config.HostingTime.ToString("dd.MM.yyyy");

            string sql_query =
            @" SELECT L.LoanID, (SELECT Count(CustomerID)  From Loan WHERE CustomerID = L.CustomerID ) AS LoanCount, 
	            (SELECT BranchName FROM Branch WHERE BranchID = L.BranchID) AS BranchName,
	            C.FullName + ' - ' + L.StartDate + ' - ' + cast( L.LoanAmount as varchar) + ' ' + Cur.CurrencyName AS Description, L.CollateralNumber,  
                ISNULL(dbo.DebtPrincipalByLoanID(L.LoanID, 'CUST_LOAN_SSUDA_ACNT' ),0) +
                ISNULL(dbo.ExpiredPrincipalDebtByLoanID(L.LoanID, 'CUST_LOAN_EXPERIED_SSUDA_ACNT'),0) AS TotalPrincipalDebt,
                ISNULL(dbo.ExpiredInterestDebtByLoanID(L.LoanID, 'CUST_LOAN_EXPERIED_RATE_ACNT'),0) +
	  	        ISNULL(dbo.DebtInterestByLoanID(L.LoanID, 'CUST_LOAN_RATE_ACNT'),0) AS TotalInterestDebt,
                dbo.PenalytDebtByLoanID(L.LoanID, 'CUST_LOAN_PENALTY_ACNT') AS PenaltyDebt, 
                ISNULL(dbo.DebtPrincipalByLoanID(L.LoanID, 'CUST_LOAN_SSUDA_ACNT' ),0) + ISNULL(dbo.ExpiredPrincipalDebtByLoanID(L.LoanID, 'CUST_LOAN_EXPERIED_SSUDA_ACNT'),0)+
		        ISNULL(dbo.ExpiredInterestDebtByLoanID(L.LoanID, 'CUST_LOAN_EXPERIED_RATE_ACNT'),0) + ISNULL(dbo.DebtInterestByLoanID(L.LoanID, 'CUST_LOAN_RATE_ACNT'),0) + 
                ISNULL(dbo.PenalytDebtByLoanID(L.LoanID, 'CUST_LOAN_PENALTY_ACNT'),0)AS TotalDebt,
                L.PaymentDate, 
	  	        DATEDIFF(DAY, TRY_PARSE('" + pCurrentDate + @"' as date using 'en-gb'), TRY_PARSE(L.PaymentDate as date using 'en-gb')) as DaysOfRespite ,
                C.MobilePhone1, C.MobilePhone2, L.LoanNote
             FROM Loan L
   	            INNER JOIN Customer C ON C.CustomerID = L.CustomerID 
   	            INNER JOIN Currency Cur ON Cur.CurrencyID = L.CurrencyID
             WHERE L.LoanStatusID = 3 and L.Status = 'ACTIVE' and L.IsApprove =1
                AND L.BranchID IN (SELECT BranchID FROM v_UserBranch WHERE UserID = @pUserID) AND L.CurrencyID = @pCurrencyID 
	            AND TRY_PARSE (L.ActualExpiryDate as date using 'en-gb') BETWEEN TRY_PARSE(@pDateStart as date using 'en-gb') AND TRY_PARSE(@pDateEnd as date using 'en-gb')
                    {0}";

            string sql_sub_query = " ";
            if (dataRequest.BranchID != 0)
            {
                sql_sub_query += " AND L.BranchID = @pBranchID";
            }
            if (dataRequest.CollateralNumber != "")
            {
                sql_sub_query += " AND L.CollateralNumber LIKE  @pCollateralNumber";
            }
            if (dataRequest.Fullname != "")
            {
                sql_sub_query += " AND C.FullName LIKE  @pFullName";
            }

            string _sql = string.Format(sql_query, sql_sub_query).Trim();
            SqlDataAdapter da = new SqlDataAdapter(_sql, Utils.sqlConn);

            da.SelectCommand.Parameters.AddWithValue("@pUserID", dataRequest.UserID);
            da.SelectCommand.Parameters.AddWithValue("@pCurrencyID", dataRequest.CurrencyID);
            da.SelectCommand.Parameters.AddWithValue("@pDateStart", dataRequest.DateStart);
            da.SelectCommand.Parameters.AddWithValue("@pDateEnd", dataRequest.DateEnd);

            if (dataRequest.BranchID != 0)
            {
                da.SelectCommand.Parameters.Add("@pBranchID", SqlDbType.Int).Value = dataRequest.BranchID;
            }
            if (dataRequest.CollateralNumber != "")
            {
                da.SelectCommand.Parameters.Add("@pCollateralNumber", SqlDbType.NVarChar, 32767).Value = "%" + dataRequest.CollateralNumber + "%";
            }
            if (dataRequest.Fullname != "")
            {
                da.SelectCommand.Parameters.Add("@pFullName", SqlDbType.NVarChar, 32767).Value = "%" + dataRequest.Fullname + "%";
            }

            DataTable dtControlLoansList = new DataTable("GetControlLoansList");
            try
            {
                da.Fill(dtControlLoansList);
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return null;
            }
            return dtControlLoansList;
        }
        public DataTable dtGetEndedLoansList(Model.RequestDTO.GetEndedLoansListRequest dataRequest)
        {

            string pCurrentDate = Config.HostingTime.ToString("dd.MM.yyyy");

            string sql_query =
            @"SELECT L.LoanID,  
            	    (SELECT BranchName FROM Branch WHERE BranchID = L.BranchID) AS BranchName,
            	    C.FullName + ' - ' + L.StartDate + ' - ' + cast( L.LoanAmount as varchar) + ' ' + Cur.CurrencyName AS Description, 
            		 L.CollateralNumber,  
                     L.LoanAmount,  L.ActualExpiryDate AS EndDate,  Ls.LoanStatus, Format(L.CollateralReturnDate, 'dd.MM.yyyy HH:MM') AS HandOverDate
             FROM Loan L
                	  INNER JOIN Customer C ON C.CustomerID = L.CustomerID 
                	  INNER JOIN Currency Cur ON Cur.CurrencyID = L.CurrencyID
             	  INNER JOIN LoanStatus LS ON L.LoanStatusID = LS.LoanStatusID
             WHERE L.LoanStatusID IN(2,4) and L.Status = 'ACTIVE' and L.IsApprove = 1
                   AND L.BranchID IN (SELECT BranchID FROM v_UserBranch WHERE UserID = @pUserID) AND L.CurrencyID = @pCurrencyID 
             	  AND TRY_PARSE (L.ActualExpiryDate as date using 'en-gb') BETWEEN TRY_PARSE(@pDateStart as date using 'en-gb') AND TRY_PARSE(@pDateEnd as date using 'en-gb') {0}
             ORDER BY TRY_PARSE (L.ActualExpiryDate as date using 'en-gb') DESC";
            
            string sql_sub_query = " ";
            if (dataRequest.BranchID != 0)
            {
                sql_sub_query += " AND L.BranchID = @pBranchID";
            }
            if (dataRequest.CollateralNumber != "")
            {
                sql_sub_query += " AND L.CollateralNumber LIKE  @pCollateralNumber";
            }
            if (dataRequest.Fullname != "")
            {
                sql_sub_query += " AND C.FullName LIKE  @pFullName";
            }
            if (dataRequest.LoanStatusID != 0)
            {
                sql_sub_query += " AND L.LoanStatusID = @pLoanStatusID";
            }
            string _sql = string.Format(sql_query, sql_sub_query).Trim();
            SqlDataAdapter da = new SqlDataAdapter(_sql, Utils.sqlConn);

            da.SelectCommand.Parameters.AddWithValue("@pUserID", dataRequest.UserID);
            da.SelectCommand.Parameters.AddWithValue("@pCurrencyID", dataRequest.CurrencyID);
            da.SelectCommand.Parameters.AddWithValue("@pDateStart", dataRequest.DateStart);
            da.SelectCommand.Parameters.AddWithValue("@pDateEnd", dataRequest.DateEnd);

            if (dataRequest.BranchID != 0)
            {
                da.SelectCommand.Parameters.Add("@pBranchID", SqlDbType.Int).Value = dataRequest.BranchID;
            }
            if (dataRequest.CollateralNumber != "")
            {
                da.SelectCommand.Parameters.Add("@pCollateralNumber", SqlDbType.NVarChar, 32767).Value = "%" + dataRequest.CollateralNumber + "%";
            }
            if (dataRequest.Fullname != "")
            {
                da.SelectCommand.Parameters.Add("@pFullName", SqlDbType.NVarChar, 32767).Value = "%" + dataRequest.Fullname + "%";
            }
            if (dataRequest.LoanStatusID != 0)
            {
                da.SelectCommand.Parameters.Add("@pLoanStatusID", SqlDbType.Int).Value = dataRequest.LoanStatusID;
            }
            DataTable dtEndedLoansList = new DataTable("GetEndedLoansList");
            try
            {
                da.Fill(dtEndedLoansList);
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return null;
            }
            return dtEndedLoansList;
        }
        public DataTable dtGetLoanPaymentList(Model.RequestDTO.GetLoanPaymentListRequest dataRequest)
        {          
            string sql_query =
            @"SELECT B.BranchName, C.FullName + ' - ' + L.StartDate + ' - ' + Cast(L.LoanAmount as varchar) + ' - ' + Cur.CurrencyName AS Description,
            L.CollateralNumber, LP.PaymentDate,  LP.PayedPrincipalAmount, LP.PayedRateAmount, LP.PayedPenalytAmount, LP.TotalPayment
            FROM LoanPayment LP
	            INNER JOIN Loan L ON LP.LoanID = L.LoanID
	            INNER JOIN Customer C ON L.CustomerID = C.CustomerID 
	            INNER JOIN Branch B ON B.BranchID = L.BranchID
	            INNER JOIN Currency Cur ON Cur.CurrencyID = L.CurrencyID
             WHERE  L.BranchID IN (SELECT BranchID FROM v_UserBranch WHERE UserID = @pUserID) AND L.CurrencyID = @pCurrencyID
	         AND LP.PaymentDate  BETWEEN TRY_PARSE(@pDateStart as date using 'en-gb') AND TRY_PARSE(@pDateEnd as date using 'en-gb') and LP.Status = 'ACTIVE' {0}
             ORDER BY LP.PaymentDate DESC";

            string sql_sub_query = " ";
            if (dataRequest.BranchID != 0)
            {
                sql_sub_query += " AND L.BranchID = @pBranchID";
            }
            if (dataRequest.CollateralNumber != "")
            {
                sql_sub_query += " AND L.CollateralNumber LIKE  @pCollateralNumber";
            }
            if (dataRequest.Fullname != "")
            {
                sql_sub_query += " AND C.FullName LIKE  @pFullName";
            }
            
            string _sql = string.Format(sql_query, sql_sub_query).Trim();
            SqlDataAdapter da = new SqlDataAdapter(_sql, Utils.sqlConn);

            da.SelectCommand.Parameters.AddWithValue("@pUserID", dataRequest.UserID);
            da.SelectCommand.Parameters.AddWithValue("@pCurrencyID", dataRequest.CurrencyID);
            da.SelectCommand.Parameters.AddWithValue("@pDateStart", dataRequest.DateStart);
            da.SelectCommand.Parameters.AddWithValue("@pDateEnd", dataRequest.DateEnd);

            if (dataRequest.BranchID != 0)
            {
                da.SelectCommand.Parameters.Add("@pBranchID", SqlDbType.Int).Value = dataRequest.BranchID;
            }
            if (dataRequest.CollateralNumber != "")
            {
                da.SelectCommand.Parameters.Add("@pCollateralNumber", SqlDbType.NVarChar, 32767).Value = "%" + dataRequest.CollateralNumber + "%";
            }
            if (dataRequest.Fullname != "")
            {
                da.SelectCommand.Parameters.Add("@pFullName", SqlDbType.NVarChar, 32767).Value = "%" + dataRequest.Fullname + "%";
            }
           
            DataTable dtGetLoanPaymentList = new DataTable("GetLoanPaymentList");
            try
            {
                da.Fill(dtGetLoanPaymentList);
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return null;
            }
            return dtGetLoanPaymentList;
        }
        public Model.ResponseDTO.SetResponse DeleteLoanPaymnet(Model.RequestDTO.DeleteLoanPaymentRequest datatRequest)
        {
            Model.ResponseDTO.SetResponse deleteResponse = new Model.ResponseDTO.SetResponse();
            deleteResponse = dtDeleteLoanPaymnet(datatRequest);
            return deleteResponse;
        }
        wsMCS.Model.ResponseDTO.SetResponse dtDeleteLoanPaymnet(Model.RequestDTO.DeleteLoanPaymentRequest request)
        {
            Model.ResponseDTO.SetResponse deleteResponse = new Model.ResponseDTO.SetResponse();
            SqlCommand cmd = new SqlCommand("spDeleteLoanPaymnentByID", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pPaymentID", request.PaymentID);
            cmd.Parameters.AddWithValue("@pDescription", request.Description);
            cmd.Parameters.AddWithValue("@pModifiedID", request.ModifiedID);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;
            cmd.Parameters.Add(ParamResult);
            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                deleteResponse.isSuccess = true;
                deleteResponse.errorCode = "";
            }
            else
            {
                deleteResponse.isSuccess = false;
                deleteResponse.errorCode = pResult;
            }
            return deleteResponse;
        }
    }
}
