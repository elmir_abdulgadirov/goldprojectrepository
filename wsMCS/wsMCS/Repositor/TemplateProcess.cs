﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using wsMCS.App_Code;
using apiMCS.App_Code;

namespace wsMCS.Repositor
{
    public class TemplateProcess
    {
        public DataTable DtPaymentTempData(Model.RequestDTO.TempLoanPaymentRequest tempRequest)
        {
            DataTable dt = new DataTable("PaymentScheduleByID");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT T.ID AS TranID, T.BranchName, T.BranchTaxID ,  Format(T.TranDate, 'dd.MM.yyyy hh:mm') AS TranDate,  T.Addl_Text AS CustomerName, T.T_DESCRIPTION ,
                                                     T.DT_Amount AS Amount,
                                                     T.Description AS PayedPerson, T.CreatedUser FROM vTransactions T
                                                      WHERE T.ID = @TranID ", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@TranID", tempRequest.TransactionID);

            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                dt = null;
            }
            return dt;
        }
        public DataTable DtCashJournaltTempData(Model.RequestDTO.TempCashJournalRequest cashRequest)
        {
            DataTable dtCashJournal = new DataTable("TempCashJournalList");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("spGetCashJournalList", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pACNT_ID", cashRequest.ACNT_ID);
                    cmd.Parameters.AddWithValue("@pUserID", cashRequest.UserID);
                    cmd.Parameters.AddWithValue("@pBranchID", cashRequest.BranchID);
                    /*cmd.Parameters.AddWithValue("@pACNT_TYPE", cashRequest.ACNT_TYPE);*/
                    cmd.Parameters.AddWithValue("@pCurrencyID", cashRequest.CurrencyID);
                    cmd.Parameters.AddWithValue("@pDateFrom", cashRequest.DateFrom);
                    cmd.Parameters.AddWithValue("@pDateTo", cashRequest.DateTo);
                    cmd.Parameters.AddWithValue("@pSource", cashRequest.Source);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtCashJournal.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        pResult = ex.Message;
                    }
                    return dtCashJournal;
                }
            }
        }
        public DataTable DtGetCostTempDataByID(int TransactionID)
        {
            DataTable dt = new DataTable("CostTempDataByID");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT  B.BranchName, B.BranchTaxID, Format(T.ID,'00000000') AS TranID ,  Format(T.TranDate, 'dd.MM.yyyy') AS TranDate, 
                                                     cbt.ResponsibilityPerson, T.Amount, T.Description, U.FullName AS CreatedUser, T.MsgID, T.CreatedID, cbt.Corresp_AcntID, 
                                                     A.ACNT_ID, A.Branch_ID,  cbt.Assignment from Transactions T
	                                                 INNER JOIN Accounts A ON A.ACNT_ID = T.ACNT_ID
	                                                 INNER JOIN Branch B ON B.BranchID = A.Branch_ID
	                                                 INNER JOIN Users U ON U.ID  = T.CreatedID
	                                                 INNER JOIN CashBankTransactions cbt ON cbt.MsgID = T.MsgID
                                                     WHERE T.Status = 'O' and cbt.Corresp_AcntID <> A.ACNT_ID and
                                                     T.Auth_Stat ='A' and T.ID = @TransactionID", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@TransactionID", TransactionID);

            try
            {
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                dt = null;
            }
            return dt;
        }
    }
}