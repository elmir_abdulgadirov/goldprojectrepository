﻿using wsMCS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using wsMCS.App_Code;
using apiMCS.App_Code;

namespace wsMCS.Repositor
{
    public class UserProcess
    {
        public Model.ResponseDTO.UserLoginResponse UserLogin(Model.RequestDTO.UserLoginRequest Request)
        {
            Model.ResponseDTO.UserLoginResponse response = new Model.ResponseDTO.UserLoginResponse();
            response = dbLogin(Request);
            return response;
        }

         wsMCS.Model.ResponseDTO.UserLoginResponse dbLogin(Model.RequestDTO.UserLoginRequest Request)
        {
            SqlCommand cmd = new SqlCommand("spLogin", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;

            //input
            cmd.Parameters.AddWithValue("@pLogin", Request.UserLogin);
            cmd.Parameters.AddWithValue("@pPassword", Config.Sha1(Request.UserPassword));
            cmd.Parameters.AddWithValue("@pLastLoginDate", Config.HostingTime);

            //output
            SqlParameter pIsLogin = new SqlParameter("@pIsLogin", SqlDbType.Bit);
            pIsLogin.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(pIsLogin);

            SqlParameter pErrorCode = new SqlParameter("@pErrorCode", SqlDbType.VarChar);
            pErrorCode.Direction = ParameterDirection.Output;
            pErrorCode.Size = 100;
            cmd.Parameters.Add(pErrorCode);

            SqlParameter pFullName = new SqlParameter("@pFullName", SqlDbType.NVarChar);
            pFullName.Direction = ParameterDirection.Output;
            pFullName.Size = 100;
            cmd.Parameters.Add(pFullName);

            SqlParameter pContactNumber = new SqlParameter("@pContactNumber", SqlDbType.VarChar);
            pContactNumber.Direction = ParameterDirection.Output;
            pContactNumber.Size = 50;
            cmd.Parameters.Add(pContactNumber);

            SqlParameter pSex = new SqlParameter("@pSex", SqlDbType.Char);
            pSex.Direction = ParameterDirection.Output;
            pSex.Size = 1;
            cmd.Parameters.Add(pSex);

            SqlParameter pPosition = new SqlParameter("@pPosition", SqlDbType.NVarChar);
            pPosition.Direction = ParameterDirection.Output;
            pPosition.Size = 100;
            cmd.Parameters.Add(pPosition);

            SqlParameter pPicture = new SqlParameter("@pPicture", SqlDbType.VarChar);
            pPicture.Direction = ParameterDirection.Output;
            pPicture.Size = 100;
            cmd.Parameters.Add(pPicture);

            SqlParameter pUserId = new SqlParameter("@pUserId", SqlDbType.Int);
            pUserId.Direction = ParameterDirection.Output;
            pUserId.Size = 10;
            cmd.Parameters.Add(pUserId);

            SqlParameter pSessionKey = new SqlParameter("@pSessionKey", SqlDbType.VarChar);
            pSessionKey.Direction = ParameterDirection.Output;
            pSessionKey.Size = 100;
            cmd.Parameters.Add(pSessionKey);

            SqlParameter pUserPermission = new SqlParameter("@pUserPermission", SqlDbType.VarChar);
            pUserPermission.Direction = ParameterDirection.Output;
            pUserPermission.Size = 50;
            cmd.Parameters.Add(pUserPermission);

            wsMCS.Model.ResponseDTO.UserLoginResponse _dbResponse = new Model.ResponseDTO.UserLoginResponse();
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                _dbResponse.IsLogin = Convert.ToBoolean(cmd.Parameters["@pIsLogin"].Value);
                _dbResponse.ErrorCode = Convert.ToString(cmd.Parameters["@pErrorCode"].Value);
                _dbResponse.FullName = Convert.ToString(cmd.Parameters["@pFullName"].Value);
                _dbResponse.ContactNumber = Convert.ToString(cmd.Parameters["@pContactNumber"].Value);
                _dbResponse.Sex = Convert.ToString(cmd.Parameters["@pSex"].Value);
                _dbResponse.Position = Convert.ToString(cmd.Parameters["@pPosition"].Value);
                _dbResponse.Picture = Convert.ToString(cmd.Parameters["@pPicture"].Value);
                _dbResponse.Picture = Convert.ToString(cmd.Parameters["@pPicture"].Value);
                _dbResponse.UserId = Convert.ToInt32(cmd.Parameters["@pUserId"].Value);
                _dbResponse.SessionKey = Convert.ToString(cmd.Parameters["@pSessionKey"].Value);
                _dbResponse.UserPermission = Convert.ToString(cmd.Parameters["@pUserPermission"].Value);

            }
            catch (Exception ex)
            {
                _dbResponse.IsLogin = false;
                _dbResponse.ErrorCode = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return _dbResponse;
        }

        public DataTable dtMenu(int userId)
        {
            DataTable dt = new DataTable("Menu");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT M.id, 
                   M.parent_id, 
                   M.NAME, 
                   M.url, 
                   M.position, 
                   M.icon 
            FROM   dbo.menu M 
            WHERE  M.id IN (SELECT F.menu_id 
                            FROM   userfunctions U 
                                   INNER JOIN functions F 
                                           ON U.function_id = F.function_id 
                                              AND U.menu_id = F.menu_id 
                            WHERE  F.fieldname = '_MAIN_PAGE' 
                                   AND U.user_id = @UserID) 
                    OR M.id IN (SELECT M.parent_id 
                                FROM   dbo.menu M 
                                WHERE  M.id IN (SELECT F.menu_id 
                                                FROM   userfunctions U 
                                                       INNER JOIN functions F 
                                                               ON U.function_id = 
                                                                  F.function_id 
                                                                  AND U.menu_id = F.menu_id 
                                                WHERE  F.fieldname = '_MAIN_PAGE' 
                                                       AND U.user_id = @UserID)) 
            ORDER  BY M.position ", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@UserID", userId);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }

        public Model.ResponseDTO.AddUserResponse AddUser(Model.RequestDTO.AddUserRequest addUserRequest)
        {
            Model.ResponseDTO.AddUserResponse userAddResponse = new Model.ResponseDTO.AddUserResponse();
            userAddResponse = dbAddUser(addUserRequest);
            return userAddResponse;
        }

        wsMCS.Model.ResponseDTO.AddUserResponse dbAddUser(Model.RequestDTO.AddUserRequest AddUserRequest)
        {
            wsMCS.Model.ResponseDTO.AddUserResponse response = new Model.ResponseDTO.AddUserResponse();

           SqlCommand cmd = new SqlCommand("spAddUser", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pFullName", AddUserRequest.FullName );
            cmd.Parameters.AddWithValue("@pContactNumber", AddUserRequest.ContactNumber);
            cmd.Parameters.AddWithValue("@pSex", AddUserRequest.Sex);
            cmd.Parameters.AddWithValue("@pPosition", AddUserRequest.Position);
            cmd.Parameters.AddWithValue("@pLogin", AddUserRequest.Login);
            cmd.Parameters.AddWithValue("@pPassword", Config.Sha1(AddUserRequest.Password));
            cmd.Parameters.AddWithValue("@pPicture", AddUserRequest.Picture);
            cmd.Parameters.AddWithValue("@pStatus", AddUserRequest.Status);
            cmd.Parameters.AddWithValue("@pCreatedId", AddUserRequest.CreatedId);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);


            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                response.isSuccess = true;
                response.errorCode = "";
            }
            else
            {
                response.isSuccess = false;
                response.errorCode = pResult;
            }

            return response;
        }

        public DataTable dtGetUser()
        {
            DataTable dtUser = new DataTable("Users");
            SqlDataAdapter daUser = new SqlDataAdapter(@"SELECT U.*
            FROM   dbo.Users U 
            WHERE  U.status = 'ACTIVE'  or U.Status = 'DEACTIVE'
            ORDER  BY U.CreatedDate DESC   ", Utils.sqlConn);

            try
            {
                daUser.Fill(dtUser);
            }
            catch
            {
                dtUser = null;
            }
            return dtUser;

        }

        public Model.ResponseDTO.ChangePasswordResponse ChangePassword(Model.RequestDTO.ChangePasswordRequest Request)
        {
            Model.ResponseDTO.ChangePasswordResponse Response = new Model.ResponseDTO.ChangePasswordResponse();
            Response = dbChangePassword(Request);
            return Response;
        }

        wsMCS.Model.ResponseDTO.ChangePasswordResponse dbChangePassword(Model.RequestDTO.ChangePasswordRequest Request)
        {
            wsMCS.Model.ResponseDTO.ChangePasswordResponse response = new Model.ResponseDTO.ChangePasswordResponse();
            SqlCommand cmd = new SqlCommand("spChangePassword", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pUserId", Request.UserID);
            cmd.Parameters.AddWithValue("@pNewPassword", Config.Sha1(Request.NewPassword));
            cmd.Parameters.AddWithValue("@pModifyId", Request.ModifyId);
            cmd.Parameters.AddWithValue("@pModifyDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);


            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                response.isSuccess = true;
                response.errorCode = "";
            }
            else
            {
                response.isSuccess = false;
                response.errorCode = pResult;
            }

            return response;
        }

        public Model.ResponseDTO.ModifyUserResponse ModifyUser(Model.RequestDTO.ModifyUserRequest ModifyUserRequset)
        {
            Model.ResponseDTO.ModifyUserResponse Response = new Model.ResponseDTO.ModifyUserResponse();
            Response = dbModifyUser(ModifyUserRequset);
            return Response;
        }

        wsMCS.Model.ResponseDTO.ModifyUserResponse dbModifyUser(Model.RequestDTO.ModifyUserRequest ModifyUserRequest)
        {
            Model.ResponseDTO.ModifyUserResponse ModifyUserResponse = new Model.ResponseDTO.ModifyUserResponse();
            SqlCommand cmd = new SqlCommand("spModifyUser", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;

            //INPUT
            cmd.Parameters.AddWithValue("@pUserId",ModifyUserRequest.UserId);
            cmd.Parameters.AddWithValue("@pFullName", ModifyUserRequest.FullName);
            cmd.Parameters.AddWithValue("@pContactNumber", ModifyUserRequest.ContactNumber);
            cmd.Parameters.AddWithValue("@pSex", ModifyUserRequest.Sex);
            cmd.Parameters.AddWithValue("@pPosition", ModifyUserRequest.Position);
            cmd.Parameters.AddWithValue("@pPicture", ModifyUserRequest.Picture);
            cmd.Parameters.AddWithValue("@pModifiedId", ModifyUserRequest.ModifydId);
            cmd.Parameters.AddWithValue("@pModifiedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;
            cmd.Parameters.Add(ParamResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception Ex)
            {
                pResult = Ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                ModifyUserResponse.isSuccess = true;
                ModifyUserResponse.errorCode = "";
            }
            else
            {
                ModifyUserResponse.isSuccess = false;
                ModifyUserResponse.errorCode = pResult;
            }
            return ModifyUserResponse; 
        }
        public Model.ResponseDTO.ChangeUserStatusResponse ChangeStatus(Model.RequestDTO.ChangeUserStatusRequest StatusRequest)
        {
            Model.ResponseDTO.ChangeUserStatusResponse DeleteResponse = new Model.ResponseDTO.ChangeUserStatusResponse();
            DeleteResponse = dbChangeUserStatus(StatusRequest);
            return DeleteResponse;
        }

        wsMCS.Model.ResponseDTO.ChangeUserStatusResponse dbChangeUserStatus(Model.RequestDTO.ChangeUserStatusRequest StatusRequest)
        {
            Model.ResponseDTO.ChangeUserStatusResponse StatusResponse = new Model.ResponseDTO.ChangeUserStatusResponse();
            SqlCommand cmd = new SqlCommand("spChangeUserStatus", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;

            //INPUT
            cmd.Parameters.AddWithValue("@pUserId", StatusRequest.UserId);
            cmd.Parameters.AddWithValue("@pUserStatus", StatusRequest.UserStatus);
            cmd.Parameters.AddWithValue("@pModifyId", StatusRequest.ModifyId);
            cmd.Parameters.AddWithValue("@pModifyDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;
            cmd.Parameters.Add(ParamResult);

            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                StatusResponse.isSuccess = true;
                StatusResponse.errorCode = "";
            }
            else
            {
                StatusResponse.isSuccess = false;
                StatusResponse.errorCode = pResult;
            }
            return StatusResponse;
        }
        public DataTable dtGetUserByID(int userId)
        {
            DataTable dtUser = new DataTable("Users");
            SqlDataAdapter daUser = new SqlDataAdapter(@"SELECT U.ID, U.FullName, U.ContactNumber, U.Sex, U.Position, 
                                            U.Login, LastLoginDate, U.Picture, U.Status,
                                            U.CreatedId, U.CreatedDate, U.ModifiedId, U.ModifiedDate 
                                            FROM Users U 
                                            WHERE  U.ID = @userId ", Utils.sqlConn);
           
            daUser.SelectCommand.Parameters.AddWithValue("@UserID", userId);
            try
            {
                daUser.Fill(dtUser);
            }
            catch
            {
                dtUser = null;
            }
            return dtUser;
        }
        public Model.ResponseDTO.AddUserBranchResponse AddUserBranch(Model.RequestDTO.AddUserBranchRequest BranchRequest)
        {
            Model.ResponseDTO.AddUserBranchResponse BranchResponse = new Model.ResponseDTO.AddUserBranchResponse();
            BranchResponse = dtAddUserBranch(BranchRequest);
            return BranchResponse;
        }

        wsMCS.Model.ResponseDTO.AddUserBranchResponse dtAddUserBranch(Model.RequestDTO.AddUserBranchRequest BranchRequest)
        {
            Model.ResponseDTO.AddUserBranchResponse BranchResponse = new Model.ResponseDTO.AddUserBranchResponse();
            SqlCommand cmd = new SqlCommand("spAddUserBranch", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pUserID", BranchRequest.UserID);
            cmd.Parameters.AddWithValue("@pBranchCode", BranchRequest.BranchCode);
            cmd.Parameters.AddWithValue("@pCreatedID", BranchRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                BranchResponse.isSuccess = true;
                BranchResponse.errorCode = "";
            }
            else
            {
                BranchResponse.isSuccess = false;
                BranchResponse.errorCode = pResult;
            }

            return BranchResponse;
        }

        public Model.ResponseDTO.DeleteUserBranchResponse DeleteUserBranch(Model.RequestDTO.DeleteUserBranchRequest BranchRequest)
        {
            Model.ResponseDTO.DeleteUserBranchResponse BranchResponse = new Model.ResponseDTO.DeleteUserBranchResponse();
            BranchResponse = dtUserBranch(BranchRequest);
            return BranchResponse;
        }

        wsMCS.Model.ResponseDTO.DeleteUserBranchResponse dtUserBranch(Model.RequestDTO.DeleteUserBranchRequest BranchRequest)
        {
            Model.ResponseDTO.DeleteUserBranchResponse BranchResponse = new Model.ResponseDTO.DeleteUserBranchResponse();
            SqlCommand cmd = new SqlCommand("spDeleteUserBranch", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pUserID", BranchRequest.UserID);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;

            cmd.Parameters.Add(ParamResult);
            string pResult = "";

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception EX)
            {
                pResult = EX.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                BranchResponse.isSuccess = true;
                BranchResponse.errorCode = "";
            }
            else
            {
                BranchResponse.isSuccess = false;
                BranchResponse.errorCode = pResult;
            }

            return BranchResponse;
        }
        public DataTable dtUserBranch(int UserID)
        {
            DataTable dt = new DataTable("UserBranch");
            SqlDataAdapter da = new SqlDataAdapter(@"SELECT B.*, ISNULL(UB.UserID,0) UserID FROM Branch B LEFT JOIN UserBranch UB 
							ON B.BranchCode = UB.BranchCode and UB.UserID = @UserID 
                            ORDER BY B.BranchCode ", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@UserID", UserID);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }
        public DataTable GetUserModulesList()
        {
            DataTable dt = new DataTable("UserModules");
            SqlDataAdapter da = new SqlDataAdapter("select * from V_MenuForRole order by Position", Utils.sqlConn);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }

        public DataTable GetUserModuleFunctionsList(int userID, int menuID)
        {
            DataTable dt = new DataTable("UserModuleFunctions");
            SqlDataAdapter da = new SqlDataAdapter(@"select f.* , case when isnull(u.User_ID,0) = 0 then 0 else 1 end as isRole
                                                 from Functions f left join UserFunctions u on f.Menu_ID = u.Menu_ID and f.Function_ID = u.Function_ID and u.User_ID = @User_ID
                                                where f.Menu_ID = @Menu_ID order by f.FieldName", Utils.sqlConn);
            da.SelectCommand.Parameters.AddWithValue("@User_ID", userID);
            da.SelectCommand.Parameters.AddWithValue("@Menu_ID", menuID);
            try
            {
                da.Fill(dt);
            }
            catch
            {
                dt = null;
            }
            return dt;
        }

        public Model.ResponseDTO.AddUserFunctionRespnse AddUserFunction(Model.RequestDTO.AddUserFunctionRequest FunctionRequest)
        {
            Model.ResponseDTO.AddUserFunctionRespnse FunctionResponse = new Model.ResponseDTO.AddUserFunctionRespnse();
            FunctionResponse = dtUserFunction(FunctionRequest);
            return FunctionResponse;
        }

        wsMCS.Model.ResponseDTO.AddUserFunctionRespnse dtUserFunction(Model.RequestDTO.AddUserFunctionRequest FunctionRequest)
        {
            wsMCS.Model.ResponseDTO.AddUserFunctionRespnse response = new Model.ResponseDTO.AddUserFunctionRespnse();
            SqlCommand cmd = new SqlCommand("spAddUserFunctions", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pUserID", FunctionRequest.UserID);
            cmd.Parameters.AddWithValue("@pMenuID", FunctionRequest.MenuID);
            cmd.Parameters.AddWithValue("@pFunctionID", FunctionRequest.FunctonID);
            cmd.Parameters.AddWithValue("@pCreatedID", FunctionRequest.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                response.isSuccess = true;
                response.errorCode = "";
            }
            else
            {
                response.isSuccess = false;
                response.errorCode = pResult;
            }

            return response;
        }
        public Model.ResponseDTO.DeleteUserFunctionRespnse DeleteFunction(Model.RequestDTO.DeleteUserFunctionRequest FunctionRequest)
        {
            Model.ResponseDTO.DeleteUserFunctionRespnse FunctionResponse = new Model.ResponseDTO.DeleteUserFunctionRespnse();
            FunctionResponse = dtDeltetFunction(FunctionRequest);
            return FunctionResponse;
        }
        wsMCS.Model.ResponseDTO.DeleteUserFunctionRespnse dtDeltetFunction(Model.RequestDTO.DeleteUserFunctionRequest FunctionRequest)
        {
            wsMCS.Model.ResponseDTO.DeleteUserFunctionRespnse FunctionResponse = new Model.ResponseDTO.DeleteUserFunctionRespnse();
            SqlCommand cmd = new SqlCommand("spDeleteUserFunctions", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            //INPUT
            cmd.Parameters.AddWithValue("@pUserID", FunctionRequest.UserID);
            cmd.Parameters.AddWithValue("@pMenuID", FunctionRequest.MenuID);

            SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            ParamResult.Direction = ParameterDirection.Output;
            ParamResult.Size = 100;
            cmd.Parameters.Add(ParamResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                FunctionResponse.isSuccess = true;
                FunctionResponse.errorCode = "";
            }
            else
            {
                FunctionResponse.isSuccess = false;
                FunctionResponse.errorCode = pResult;
            }
            return FunctionResponse;
        }
        public Model.ResponseDTO.CheckFunctionPermissionResponse FunctionPermission(Model.RequestDTO.CheckFunctionPermissionRequest FunctionRequest)
        {
            wsMCS.Model.ResponseDTO.CheckFunctionPermissionResponse FunctionResponse = new Model.ResponseDTO.CheckFunctionPermissionResponse();
            SqlCommand cmd = new SqlCommand("spCheckFunctionPermission", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@pUserID", FunctionRequest.UserID);
            cmd.Parameters.AddWithValue("@pFunction_ID", FunctionRequest.FunctionID);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);


            SqlParameter paramPermisssion = new SqlParameter("@pPermission", SqlDbType.VarChar);
            paramPermisssion.Direction = ParameterDirection.Output;
            paramPermisssion.Size = 3;
            cmd.Parameters.Add(paramPermisssion);


            string pResult = "", pPermission = "NO"; ;
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                pPermission = Convert.ToString(cmd.Parameters["@pPermission"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            if (pResult == "OK")
            {
                FunctionResponse.isSuccess = true;
                FunctionResponse.Permisssion = pPermission;
                FunctionResponse.errorCode = "";
            }
            else
            {
                FunctionResponse.isSuccess = false;
                FunctionResponse.Permisssion = pPermission;
                FunctionResponse.errorCode = pResult;
            }

            return FunctionResponse;
        }
        public Model.ResponseDTO.GetBranchFullDataByIDResponse GetBranchFullData(Model.RequestDTO.GetBranchFullDataByIDRequest dataRequest)
        {
            Model.ResponseDTO.GetBranchFullDataByIDResponse BranchDataResponse = new Model.ResponseDTO.GetBranchFullDataByIDResponse();
            BranchDataResponse = dbGetBranchFullData(dataRequest);
            return BranchDataResponse;
        }

        wsMCS.Model.ResponseDTO.GetBranchFullDataByIDResponse dbGetBranchFullData(Model.RequestDTO.GetBranchFullDataByIDRequest dataRequest)
        {
            SqlCommand cmd = new SqlCommand("spGetBranchFullDataByID", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;

            //input
            cmd.Parameters.AddWithValue("@pBranchID", dataRequest.BranchID);
          

            //output
            SqlParameter pBranchCode = new SqlParameter("@pBranchCode", SqlDbType.Char);
            pBranchCode.Direction = ParameterDirection.Output;
            pBranchCode.Size = 3;
            cmd.Parameters.Add(pBranchCode);

            SqlParameter pBranchName = new SqlParameter("@pBranchName", SqlDbType.NVarChar);
            pBranchName.Direction = ParameterDirection.Output;
            pBranchName.Size = 100;
            cmd.Parameters.Add(pBranchName);

            SqlParameter pBranchTaxID = new SqlParameter("@pBranchTaxID", SqlDbType.Char);
            pBranchTaxID.Direction = ParameterDirection.Output;
            pBranchTaxID.Size = 10;
            cmd.Parameters.Add(pBranchTaxID);

            SqlParameter pAdres = new SqlParameter("@pAdres", SqlDbType.NVarChar);
            pAdres.Direction = ParameterDirection.Output;
            pAdres.Size = 50;
            cmd.Parameters.Add(pAdres);

            SqlParameter pMobil = new SqlParameter("@pMobil", SqlDbType.VarChar);
            pMobil.Direction = ParameterDirection.Output;
            pMobil.Size = 20;
            cmd.Parameters.Add(pMobil);

            SqlParameter pOfficePhone = new SqlParameter("@pOfficePhone", SqlDbType.VarChar);
            pOfficePhone.Direction = ParameterDirection.Output;
            pOfficePhone.Size = 20;
            cmd.Parameters.Add(pOfficePhone);

            SqlParameter pBranch_Mail = new SqlParameter("@pBranch_Mail", SqlDbType.VarChar);
            pBranch_Mail.Direction = ParameterDirection.Output;
            pBranch_Mail.Size = 50;
            cmd.Parameters.Add(pBranch_Mail);

            SqlParameter pBranch_Mail_Password = new SqlParameter("@pBranch_Mail_Password", SqlDbType.VarChar);
            pBranch_Mail_Password.Direction = ParameterDirection.Output;
            pBranch_Mail_Password.Size = 50;
            cmd.Parameters.Add(pBranch_Mail_Password);

            SqlParameter pCompanyCode = new SqlParameter("@pCompanyCode", SqlDbType.Char);
            pCompanyCode.Direction = ParameterDirection.Output;
            pCompanyCode.Size = 3;
            cmd.Parameters.Add(pCompanyCode);

            SqlParameter pDirectorName = new SqlParameter("@pDirectorName", SqlDbType.NVarChar);
            pDirectorName.Direction = ParameterDirection.Output;
            pDirectorName.Size = 300;
            cmd.Parameters.Add(pDirectorName);

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            wsMCS.Model.ResponseDTO.GetBranchFullDataByIDResponse _dbResponse = new Model.ResponseDTO.GetBranchFullDataByIDResponse();
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                _dbResponse.BranchCode = Convert.ToString(cmd.Parameters["@pBranchCode"].Value);
                _dbResponse.BranchName = Convert.ToString(cmd.Parameters["@pBranchName"].Value);
                _dbResponse.BranchTaxID = Convert.ToString(cmd.Parameters["@pBranchTaxID"].Value);
                _dbResponse.Adres = Convert.ToString(cmd.Parameters["@pAdres"].Value);
                _dbResponse.Mobil = Convert.ToString(cmd.Parameters["@pMobil"].Value);
                _dbResponse.OfficePhone = Convert.ToString(cmd.Parameters["@pOfficePhone"].Value);
                _dbResponse.Branch_Mail = Convert.ToString(cmd.Parameters["@pBranch_Mail"].Value);
                _dbResponse.Branch_Mail_Password = Convert.ToString(cmd.Parameters["@pBranch_Mail_Password"].Value);
                _dbResponse.CompanyCode = Convert.ToString(cmd.Parameters["@pCompanyCode"].Value);
                _dbResponse.DirectorName = Convert.ToString(cmd.Parameters["@pDirectorName"].Value);

                _dbResponse.isSuccess = true;
                _dbResponse.errorCode = "";

            }
            catch (Exception ex)
            {
                _dbResponse.isSuccess = false;
                _dbResponse.errorCode = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return _dbResponse;
        }
       
        public string OpenDay(Model.RequestDTO.OpenDayRequest Request)
        {
            SqlCommand cmd = new SqlCommand("spRunDailyDebt", Utils.sqlConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 1800;
            //INPUT
            cmd.Parameters.AddWithValue("@pBranchID", Request.BranchID);
            cmd.Parameters.AddWithValue("@pTranDate", Request.TranDate);
            cmd.Parameters.AddWithValue("@pCreatedID", Request.CreatedID);
            cmd.Parameters.AddWithValue("@pCreatedDate", Config.HostingTime);

         

            SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.VarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            string pResult = "";
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return pResult;
        }
        public DataTable dtTrandDateStatus(Model.RequestDTO.TranDateStatusRequest dataRequest)
        {
            DataTable dtCollateralList = new DataTable("TrandDateStatus");
            string error = "";
            using (SqlConnection con = Utils.sqlConn)
            {
                using (SqlCommand cmd = new SqlCommand("[spGetTranDateSatus]", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@pTranDate", dataRequest.TranDate);
                    cmd.Parameters.AddWithValue("@pBranchID", dataRequest.BranchID);

                    SqlParameter ParamResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                    ParamResult.Direction = ParameterDirection.Output;
                    ParamResult.Size = 100;
                    cmd.Parameters.Add(ParamResult);
                    string pResult = "";
                    try
                    {
                        cmd.Connection = con;
                        con.Open();
                        IDataReader idr = cmd.ExecuteReader();
                        dtCollateralList.Load(idr);
                        pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
                        idr.Close();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        error = ex.Message;
                        pResult = error;
                    }
                    return dtCollateralList;
                }
            }
        }

    }
}
