﻿using wsMCS.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace apiMCS.App_Code
{
    public class Utils
    {
        public static SqlConnection sqlConn
        {
            get
            {
                return new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
            }
        }
    }
}
