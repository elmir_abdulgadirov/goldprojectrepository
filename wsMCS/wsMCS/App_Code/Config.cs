﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsMCS.App_Code
{
    public class Config
    {

        public static string Sha1(string Password)
        {
            byte[] result;
            System.Security.Cryptography.SHA1 ShaEncrp = new System.Security.Cryptography.SHA1CryptoServiceProvider();
           // Password = String.Format("{0}{1}{0}", "CSAASADM", Password);
            Password = $"CSAASADM{Password}CSAASADM";
            byte[] buffer = new byte[Password.Length];
            buffer = System.Text.Encoding.UTF8.GetBytes(Password);
            result = ShaEncrp.ComputeHash(buffer);
            return Convert.ToBase64String(result);
        }

        // for hosting datetime - deyishir asili olaraq
        public static DateTime HostingTime
        {
            get
            {
                return DateTime.Now.AddHours(0);
            }
        }
    }
}