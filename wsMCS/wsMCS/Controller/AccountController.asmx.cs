﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using wsMCS.Repositor;

namespace wsMCS.Controller
{
    /// <summary>
    /// Summary description for AccountController
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class AccountController : System.Web.Services.WebService
    {
        AccountProcess _accountProcess = new AccountProcess();
        [WebMethod]
        public DataTable GetAccountTypeList()
        {
            return _accountProcess.dtAccountList();
        }
        [WebMethod]
        public Model.ResponseDTO.AddAccountResponse AddCustomerCurrentAccount(Model.RequestDTO.AddAccountRequest AccountRequest)
        {
            return _accountProcess.AddAccount(AccountRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.DeleteAccountResponse DeletCurrentAccount(Model.RequestDTO.DeleteAccountRequest AccountRequest)
        {
            return _accountProcess.DeleteAccount(AccountRequest);
        }

        [WebMethod]
        public DataTable GetCustomerCurrAcnts(Model.RequestDTO.GetCustAcntsRequest getCustAcntsRequest,out int RecordCount)
        {
            return _accountProcess.GetCustomerCurrAcnts(getCustAcntsRequest,out RecordCount);
        }

        [WebMethod]
        public DataTable SearchCustCurrAcnts(Model.RequestDTO.SearchCustCurrAcntsRequest searchCustCurrAcntsRequest)
        {
            return _accountProcess.SearchCustCurrAcnts(searchCustCurrAcntsRequest);
        }
        [WebMethod]
        public DataTable GetDebetAcntsByCurrAcnt(Model.RequestDTO.GetDebetAcntsByCurrAcntRequest acntRequest)
        {
            return _accountProcess.getDebetAcnt(acntRequest);
        }
        [WebMethod]
        public DataTable GetCashTransactionList(Model.RequestDTO.GetCashTransactiontRequest cashRequest, out DataTable accountBalance)
        {
            return _accountProcess.GetCashList(cashRequest, out accountBalance);
        }
        [WebMethod]
        public DataTable GetAccountGroupList()
        {
            return _accountProcess.dtAccountGroupList();
        }
        [WebMethod]
        public DataTable GetAccountSubGroupList(string AcntGroupCode)
        {
            return _accountProcess.dtAccountSubGroupList( AcntGroupCode);
        }
        [WebMethod]
        public DataTable GetAccountSubSubGroupList(string AcntSubGroupCode)
        {
            return _accountProcess.dtAccountSubSubGroupList(AcntSubGroupCode);
        }
        [WebMethod]
        public DataTable GetAcntDataByAcntCode(string AcntCode , int UserID, int CurrencyID)
        {
            return _accountProcess.dtAcntDataList(AcntCode, UserID, CurrencyID);
        }
        [WebMethod]
        public DataTable GetCashBankDataList(Model.RequestDTO.GetCashBankRequest dataRequest)
        {
            return _accountProcess.CashBankDataList(dataRequest);
        }
        [WebMethod]
        public DataTable GetCashBankAccountList(int UserID, string AcntType)
        {
            return _accountProcess.dtCashBankAcntList(UserID, AcntType);
        }
        [WebMethod]
        public DataTable GetPortfolioList(Model.RequestDTO.GetPortfolioListRequest portfolioRequest, out DataTable portfolioBalance)
        {
            return _accountProcess.PortfolioList(portfolioRequest, out portfolioBalance);
        }
        [WebMethod]
        public DataTable GetAcntGroupedBalance(Model.RequestDTO.GetAcntGroupedBalanceRequest dataRequest)
        {
            return _accountProcess.GetAcntGroupedBalanceList(dataRequest);
        }
        [WebMethod]
        public DataTable GetAcntBalanceBySubGroup(Model.RequestDTO.GetAcntBalanceBySubGroupRequest dataRequest)
        {
            return _accountProcess.GetAcntBalanceBySubGroupList(dataRequest);
        }
        [WebMethod]
        public DataTable GetAcntTotalBalanceByBranch(Model.RequestDTO.GetTotalBalanceRequest dataRequest)
        {
            return _accountProcess.GetAcntTotalBalanceList(dataRequest);
        }
        [WebMethod]
        public DataTable GetAcntBalanceBySubSubCode(Model.RequestDTO.GetAcntBalanceBySubSubCodeRequest dataRequest)
        {
            return _accountProcess.AcntBalanceBySubSubCodeList(dataRequest);
        }
        [WebMethod]
        public DataTable GetCurAcntTranLIst(Model.RequestDTO.GetCurAcntTranLIstRequest dataRequest)
        {
            return _accountProcess.CurAcntTranList(dataRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.GetAllAcntBalanceByLoanIDResponse GetAllAcntBalanceByLoanID(Model.RequestDTO.GetAllAcntBalanceByLoanIDRequest dataRequest)
        {
            return _accountProcess.GetBalanceByLoanID(dataRequest);
        }
        [WebMethod]
        public DataTable GetSubAcntList(Model.RequestDTO.GetSubAcntListRequest dataRequest)
        {
            return _accountProcess.dbGetSubAcntListByMainAcntID(dataRequest);
        }
    }
}
