﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using wsMCS.Repositor;

namespace wsMCS.Controller
{
    /// <summary>
    /// Summary description for CustomerController
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CustomerController : System.Web.Services.WebService
    {
        CustomerProcess _custProcess = new CustomerProcess();
        [WebMethod]
        public Model.ResponseDTO.AddCustomerResponse AddCustomer(Model.RequestDTO.AddCustomerRequest CustomerRequest)
        {
            Model.ResponseDTO.AddCustomerResponse CustomerResponse = new Model.ResponseDTO.AddCustomerResponse();
            CustomerResponse = _custProcess.AddCustomer(CustomerRequest);
            return CustomerResponse;
        }
        [WebMethod]
        public Model.ResponseDTO.ModifyCustomerResponse ModifyCustomer(Model.RequestDTO.ModifyCustomerRequest CustomerRequest)
        {
            return _custProcess.ModifyCustomer(CustomerRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.DeleteCustomerResponse DeleteCustomer(Model.RequestDTO.DeleteCustomerRequest CustomerRequest)
        {
            return _custProcess.DeleteCustomer(CustomerRequest);
        }
        [WebMethod]
        public DataTable GetCustomers(Model.RequestDTO.GetCustomerRequest CustomerRequest , out int recordCount)
        {
            return _custProcess.GetCustomers(CustomerRequest, out recordCount);
        }
        [WebMethod]
        public DataTable GetCustomerByID(int customerId)
        {
            return _custProcess.dtCustomerByID(customerId);
        }
    }
}
