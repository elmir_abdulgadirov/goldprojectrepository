﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using wsMCS.Repositor;

namespace wsMCS.Controller
{
    /// <summary>
    /// Summary description for GeneralController
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class GeneralController : System.Web.Services.WebService
    {
        GeneralProcess _generalController = new GeneralProcess();
        [WebMethod]
        public DataTable CountryList()
        {
            return _generalController.dtCountry();
        }
        [WebMethod]
        public DataTable CityList()
        {
            return _generalController.dtCity();
        }
        [WebMethod]
        public DataTable GetUserBranchs(int userId)
        {
            return _generalController.dtBranch(userId);
        }
        [WebMethod]
        public DataTable CurrencyList()
        {
            return _generalController.dtCurrency();
        }

        [WebMethod]
        public decimal getDailyCurrency(string currency)
        {
            return _generalController.getDailyCurrency(currency);
        }
        [WebMethod]
        public int GetSequenceNextVal()
        {
            return _generalController.SequenceNextVal();
        }
        [WebMethod]
        public DataTable LoanAmortization(Model.RequestDTO.loanAmortizaiotnRequest dataRequest)
        {
            return _generalController.dtLoanAmortization(dataRequest);
        }
    }
}
