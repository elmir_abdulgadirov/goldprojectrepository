﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using wsMCS.Repositor;

namespace wsMCS.Controller
{
    /// <summary>
    /// Summary description for UserController
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class UserController : System.Web.Services.WebService
    {
        UserProcess _uProcess = new UserProcess();

        [WebMethod]
        public Model.ResponseDTO.UserLoginResponse UserLogin(Model.RequestDTO.UserLoginRequest Request)
        {
            return _uProcess.UserLogin(Request);
        }

        [WebMethod]
        public DataTable dtMenu(int userId)
        {
            return _uProcess.dtMenu(userId);
        }

        [WebMethod]
        public Model.ResponseDTO.AddUserResponse AddUser(Model.RequestDTO.AddUserRequest addUserRequest)
        {
            return _uProcess.AddUser(addUserRequest);
        }

        [WebMethod]
        public DataTable dtUser()
        {
            return _uProcess.dtGetUser();
        }
        [WebMethod]
        public Model.ResponseDTO.ChangePasswordResponse ChangePassword(Model.RequestDTO.ChangePasswordRequest Request)
        {
            return _uProcess.ChangePassword(Request);
        }

        [WebMethod]
        public Model.ResponseDTO.ModifyUserResponse ModifyUser(Model.RequestDTO.ModifyUserRequest ModifyUserRequest)
        {
            return _uProcess.ModifyUser(ModifyUserRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.ChangeUserStatusResponse ChangeUserStatus(Model.RequestDTO.ChangeUserStatusRequest StatusRequest)
        {
            return _uProcess.ChangeStatus(StatusRequest);
        }
        [WebMethod]
        public DataTable dtGetUserByID(int userID)
        {
            return _uProcess.dtGetUserByID(userID);
        }
        [WebMethod]
        public Model.ResponseDTO.AddUserBranchResponse AddUserBranch(Model.RequestDTO.AddUserBranchRequest BranchRequest)
        {
            return _uProcess.AddUserBranch(BranchRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.DeleteUserBranchResponse DeleteUserBranch(Model.RequestDTO.DeleteUserBranchRequest UserBranchRequest)
        {
            return _uProcess.DeleteUserBranch(UserBranchRequest);
        }
        [WebMethod]
        public DataTable dtUserBranchList(int UserID)
        {
            return _uProcess.dtUserBranch(UserID);
        }

        [WebMethod]
        public DataTable dtUserModulesList()
        {
            return _uProcess.GetUserModulesList();
        }
        [WebMethod]
        public DataTable dtUserModuleFunctionsList(int UserID, int MenuID)
        {
            return _uProcess.GetUserModuleFunctionsList(UserID, MenuID);
        }
        [WebMethod]
        public Model.ResponseDTO.AddUserFunctionRespnse AddUserFunction(Model.RequestDTO.AddUserFunctionRequest FunctionRequest)
        {
            return _uProcess.AddUserFunction(FunctionRequest);
        }

        [WebMethod]
        public Model.ResponseDTO.DeleteUserFunctionRespnse DeleteUserFunction(Model.RequestDTO.DeleteUserFunctionRequest FunctionRequest)
        {
            return _uProcess.DeleteFunction(FunctionRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.CheckFunctionPermissionResponse CheckUserPermission(Model.RequestDTO.CheckFunctionPermissionRequest checkRequest)
        {
            return _uProcess.FunctionPermission(checkRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.GetBranchFullDataByIDResponse GetBranchFullDataByID(Model.RequestDTO.GetBranchFullDataByIDRequest dataRequest)
        {
            return _uProcess.GetBranchFullData(dataRequest);
        }
        [WebMethod]
        public string openDay(Model.RequestDTO.OpenDayRequest dataRequest)
        {
            //return _uProcess.OpenDay(dataRequest);
            Task openDayTask = new Task(() => {
                _uProcess.OpenDay(dataRequest);
            });
            openDayTask.Start();
            return "PENDING";
        }
        [WebMethod]
        public DataTable GetTrandDateStatus(Model.RequestDTO.TranDateStatusRequest dataRequest)
        {
            return _uProcess.dtTrandDateStatus(dataRequest);
        }
    }
}
