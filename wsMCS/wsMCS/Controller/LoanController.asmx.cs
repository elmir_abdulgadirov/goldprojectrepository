﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using wsMCS.Repositor;

namespace wsMCS.Controller
{
    /// <summary>
    /// Summary description for LoanController
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class LoanController : System.Web.Services.WebService
    {
        LoanProcess _loanProcess = new LoanProcess();
        [WebMethod]
        public wsMCS.Model.ResponseDTO.AddLoanResponse AddLoan(wsMCS.Model.RequestDTO.AddLoanRequestDTO LoanRequest)
        {
            Model.ResponseDTO.AddLoanResponse LoanResponse = new Model.ResponseDTO.AddLoanResponse();
            LoanResponse = _loanProcess.AddLoan(LoanRequest);
            return LoanResponse;
        }
        [WebMethod]
        public wsMCS.Model.ResponseDTO.ModifyLoanResponse ModifyLoan(wsMCS.Model.RequestDTO.ModifyLoanRequestDTO LoanRequest)
        {
            Model.ResponseDTO.ModifyLoanResponse LoanResponse = new Model.ResponseDTO.ModifyLoanResponse();
            LoanResponse = _loanProcess.ModifyLoan(LoanRequest);
            return LoanResponse;
        }
        [WebMethod]
        public wsMCS.Model.ResponseDTO.ModifyLoanCollateralResponse ModifyLoanCollateral(wsMCS.Model.RequestDTO.ModifyLoanCollateralRequest LoanRequest)
        {
            Model.ResponseDTO.ModifyLoanCollateralResponse LoanResponse = new Model.ResponseDTO.ModifyLoanCollateralResponse();
            LoanResponse = _loanProcess.ModifyLoanCollateral(LoanRequest);
            return LoanResponse;
        }
        /*[WebMethod]
        public Model.ResponseDTO.DeleteLoanResponse DeleteLoan(Model.RequestDTO.DeleteLoanRequest LoanRequest)
        {
            return _loanProcess.DeleteLoan(LoanRequest);
        }*/
        [WebMethod]
        public DataTable GetLoanList(Model.RequestDTO.GetLoanListRequest getLoanRequest, out int RecordCount)
        {
            return _loanProcess.GetLoanList(getLoanRequest, out RecordCount);
        }
        [WebMethod]
        public DataTable LoanTypeList()
        {
            return _loanProcess.dtLoanType();
        }
        [WebMethod]
        public DataTable LoanStatusList()
        {
            return _loanProcess.dtLoanStatus();
        }
        [WebMethod]
        public DataTable LoanMethodList()
        {
            return _loanProcess.dtLoanMethod();
        }
        [WebMethod]
        public DataTable LoanCollateralTypeList()
        {
            return _loanProcess.dtLoanCollateralType();
        }
        [WebMethod]
        public wsMCS.Model.ResponseDTO.AddLoanNoteResponse AddLoanNote(wsMCS.Model.RequestDTO.AddLoanNoteRequest NoteRequest)
        {
            return _loanProcess.AddLoanNote(NoteRequest);
        }
        [WebMethod]
        public wsMCS.Model.ResponseDTO.ModifyLoanNoteResponse ModifyLoanNote(wsMCS.Model.RequestDTO.ModifyLoanNoteRequest NoteRequest)
        {
            return _loanProcess.ModifyLoanNote(NoteRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.DeleteLoanNoteResponse DeleteLoanNote(Model.RequestDTO.DeleteLoanNoteRequest NoteRequest)
        {
            return _loanProcess.DeleteLoanNote(NoteRequest);
        }
        [WebMethod]
        public DataTable GetNotesByLoanID(int LoandID)
        {
            return _loanProcess.dtNoteByLoanID(LoandID);
        }
        [WebMethod]
        public Model.ResponseDTO.AddCardResponse AddCard(Model.RequestDTO.AddCardRequest CardRequest)
        {
            return _loanProcess.AddCard(CardRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.ModifyCardResponse ModifyCard(Model.RequestDTO.ModifyCardRequest CardRequest)
        {
            return _loanProcess.ModifyCard(CardRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.DeleteCardResponse DeleteCard(Model.RequestDTO.DeleteCardRequest CardRequest)
        {
            return _loanProcess.DeleteCard(CardRequest);
        }
        [WebMethod]
        public DataTable GetCardrs(Model.RequestDTO.GetCardRequest CardRequest)
        {
            return _loanProcess.GetCards(CardRequest);
        }
        [WebMethod]
        public DataTable GetCardByID(int customerId)
        {
            return _loanProcess.dtCardrByID(customerId);
        }
        [WebMethod]
        public Model.ResponseDTO.AddGuarantorResponse AddGuarantor(Model.RequestDTO.AddGuarantorRequest gurantorRequest)
        {
            return _loanProcess.AddGuarantor(gurantorRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.DeleteGuarantorResponse DeleteGuarantor(Model.RequestDTO.DeleteGuarantorRequest gurantorRequest)
        {
            return _loanProcess.DeleteGuarantor(gurantorRequest);
        }
        [WebMethod]
        public DataTable GetGuarantorsByLoanID(int LoandID)
        {
            return _loanProcess.GetGuarantor(LoandID);
        }
        [WebMethod]
        public Model.ResponseDTO.AddLoanDocumentResponse AddLoanDocument(Model.RequestDTO.AddLoanDocumentRequest documentRequest)
        {
            return _loanProcess.AddLoanDocument(documentRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.DeleteLoanDocumentResponse DeleteLoanDocument(Model.RequestDTO.DeleteLoanDocumentRequest documentRequest)
        {
            return _loanProcess.DeleteDocument(documentRequest);
        }
        [WebMethod]
        public DataTable GetDocumentsByLoanID(int LoandID)
        {
            return _loanProcess.GetDocuments(LoandID);
        }
        [WebMethod]
        public Model.ResponseDTO.AddCollateralResponse AddCollateral(Model.RequestDTO.AddCollateralRequest collateralRequest)
        {
            return _loanProcess.AddCollateral(collateralRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.ModifyCollateralResponse ModifyCollateral(Model.RequestDTO.ModifyCollateralRequest collateralRequest)
        {
            return _loanProcess.ModifyCollateral(collateralRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.DeleteCollateralResponse DeleteCollateral(Model.RequestDTO.DeleteCollateralRequest collateralRequest)
        {
            return _loanProcess.DeleteCollateral(collateralRequest);
        }
        [WebMethod]
        public DataTable GetCollateralByLoanIDList(int LoanID)
        {
            return _loanProcess.CollateralList(LoanID);
        }
        [WebMethod]
        public Model.ResponseDTO.SetLoanLenderResponse SetLoanLender(Model.RequestDTO.setLoanLenderRequest lenderRequest)
        {
            return _loanProcess.SetLender(lenderRequest);
        }
        [WebMethod]
        public DataTable GetLenderByLoanID(int LoandID)
        {
            return _loanProcess.GetLender(LoandID);
        }
        [WebMethod]
        public Model.ResponseDTO.SetLoanVerifierResponse SetLoanVerifier(Model.RequestDTO.setLoanVerifierRequest verifierRequest)
        {
            return _loanProcess.SetVerifier(verifierRequest);
        }
        [WebMethod]
        public DataTable GetVerifierByLoanID(int LoandID)
        {
            return _loanProcess.GetVerifier(LoandID);
        }

        [WebMethod]
        public bool LoanIsApproved(int LoandID)
        {
            return _loanProcess.LoanIsApproved(LoandID);
        }
        [WebMethod]
        public DataTable GetGoldCaratList()
        {
            return _loanProcess.GoldCarat();
        }
        [WebMethod]
        public DataTable GetDocumentList()
        {
            return _loanProcess.DocumentList();
        }
        [WebMethod]
        public DataTable GetCollateralByID(int CollateralID)
        {
            return _loanProcess.DtCollateralByID(CollateralID);
        }
        [WebMethod]
        public DataTable GetPaymentScheduleByLoanID(int LoanID)
        {
            return _loanProcess.DtPaymentSchedule(LoanID);
        }
        [WebMethod]
        public DataTable GetLoanFullDatatByLoanID(int LoanID)
        {
            return _loanProcess.DtLoanFullData(LoanID);
        }
        [WebMethod]
        public string GetLoanStatus(int LoandID)
        {
            return _loanProcess.loanStatus(LoandID);
        }
        [WebMethod]
        public Model.ResponseDTO.acceptLoanResponse AcceptLoanContract(Model.RequestDTO.acceptLoanRequest acceptRequest)
        {
            return _loanProcess.acceptLoan(acceptRequest);
        }

        [WebMethod]
        public Model.ResponseDTO.SetResponse InsertCollateralOut(Model.RequestDTO.insertCollateralOutRequest Request)
        {
            return _loanProcess.InsertCollateralOut(Request);
        }
        [WebMethod]
        public Model.ResponseDTO.SetResponse CollateralRetrunProcess(Model.RequestDTO.CollateralReturnRequest Request)
        {
            return _loanProcess.CollateralReturn(Request);
        }

        [WebMethod]
        public DataTable BarCodeCollateralListByLoanID(int loanID)
        {
            return _loanProcess.dtBarCodeCollateralListByLoanID(loanID);
        }
        [WebMethod]
        public Model.ResponseDTO.SetResponse DeleteLoanByID(Model.RequestDTO.DeleteLoanByIDRequest request)
        {
            return _loanProcess.DeleteLoan(request);
        }
        [WebMethod]
        public Model.ResponseDTO.GetCurAcntResponse GetCurAcntDataByLoanID(Model.RequestDTO.GetCurAcntRequest request)
        {
            return _loanProcess.GetCurAcntByLoanID(request);
        }
        [WebMethod]
        public DataTable GetPaymentTypeList()
        {
            return _loanProcess.dtPaymentType();
        }
        [WebMethod]
        public Model.ResponseDTO.GetPaymentDebtResponse GetPaymentDebtByLoanID(Model.RequestDTO.getPaymentDebtByLoanIDRequest debtRequest)
        {
            return _loanProcess.dbGetPaymentDebtByLoanID(debtRequest);
        }

        [WebMethod]
        public DataTable GetLoanPaymentList(int LoanID)
        {
            return _loanProcess.dtLoanPayment(LoanID);
        }
        [WebMethod]
        public DataTable GetCollateralOutList(Model.RequestDTO.GetCollateralOutListRequest dataRequest)
        {
            return _loanProcess.dtCollateralOutList(dataRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.SetResponse DeleteCollateralOutByID(Model.RequestDTO.DeleteCollateralByOutIDRequest dataRequest)
        {
            return _loanProcess.DeleteCollateralOut(dataRequest);
        }
        [WebMethod]
        public DataTable GetCollateralList(Model.RequestDTO.GetCollateralListRequest dataRequest)
        {
            return _loanProcess.dtCollateralList(dataRequest);
        }
        [WebMethod]
        public DataTable GetTempCollateralList(Model.RequestDTO.GetTempCollateralListRequest dataRequest)
        {
            return _loanProcess.dtGetTempCollateralList(dataRequest);
        }
        [WebMethod]
        public DataTable GetDelayedLoansList(Model.RequestDTO.GetDelayedLoansListRequest dataRequest)
        {
            return _loanProcess.dtGetDelayedLoansList(dataRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.SetResponse ChangeLoanStatusByID(Model.RequestDTO.ChangeLoanStatusRequest dataRequest)
        {
            return _loanProcess.ChangeLoanStatus(dataRequest);
        }
        [WebMethod]
        public DataTable GetLoansByDateList(Model.RequestDTO.GetLoansByDateListRequest dataRequest)
        {
            return _loanProcess.dtGetLoansByDateList(dataRequest);
        }
        [WebMethod]
        public DataTable GetControlLoansList(Model.RequestDTO.GetControlLoansListRequest dataRequest)
        {
            return _loanProcess.dtGetControlLoansList(dataRequest);
        }
        [WebMethod]
        public DataTable GetEndedLoansList(Model.RequestDTO.GetEndedLoansListRequest dataRequest)
        {
            return _loanProcess.dtGetEndedLoansList(dataRequest);
        }
        [WebMethod]
        public DataTable GetPaymentList(Model.RequestDTO.GetLoanPaymentListRequest dataRequest)
        {
            return _loanProcess.dtGetLoanPaymentList(dataRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.SetResponse DeleteLoanPayment(Model.RequestDTO.DeleteLoanPaymentRequest dataRequest)
        {
            return _loanProcess.DeleteLoanPaymnet(dataRequest);
        }
    }
}
