﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using wsMCS.Repositor;

namespace wsMCS.Controller
{
    /// <summary>
    /// Summary description for TemplateController
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class TemplateController : System.Web.Services.WebService
    {
        TemplateProcess _templateProcess = new TemplateProcess();
        [WebMethod]
        public DataTable GetPaymentTempDataByID(Model.RequestDTO.TempLoanPaymentRequest tempRequest)
        {
            return _templateProcess.DtPaymentTempData(tempRequest);
        }
        [WebMethod]
        public DataTable GetCashJournalTempData(Model.RequestDTO.TempCashJournalRequest tempRequest)
        {
            return _templateProcess.DtCashJournaltTempData(tempRequest);
        }
        [WebMethod]
        public DataTable GetCostTempDataByID(int TransactionID)
        {
            return _templateProcess.DtGetCostTempDataByID(TransactionID);
        }
    }
}
