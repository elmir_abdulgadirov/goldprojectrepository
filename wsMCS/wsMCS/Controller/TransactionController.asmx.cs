﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using wsMCS.Repositor;

namespace wsMCS.Controller
{
    /// <summary>
    /// Summary description for TransactionController
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class TransactionController : System.Web.Services.WebService
    {
        TransactionProcess  _tranProcess = new TransactionProcess();
        [WebMethod]
        public Model.ResponseDTO.createTranResponse CreateTransaction(Model.RequestDTO.createTranRequest tranRequest)
        {
            return _tranProcess.createTran(tranRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.reverseTranResponse ReverseTransaction(Model.RequestDTO.reverseTranRequest reverseRequest)
        {
            return _tranProcess.reverseTran(reverseRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.SetResponse CreatePaymentByLoanID(Model.RequestDTO.CreatePaymentByLoanIDRequest paymentRequest)
        {
            return _tranProcess.dbCreatePaymentByLoanID(paymentRequest);
        }
        [WebMethod]
        public DataTable GetTransactionSourceMasterList()
        {
            return _tranProcess.dtTranSourceMaster();
        }
        [WebMethod]
        public Model.ResponseDTO.SetResponse CreateCashBankTransaction(Model.RequestDTO.CreateCashBankTransactionRequest tranRequest)
        {
            return _tranProcess.CreatCashBankTran(tranRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.SetResponse DeleteCashBankTransactionByID(Model.RequestDTO.DeleteCashBankTransactionRequest dataRequest)
        {
            return _tranProcess.DeleteCashBankTranByID(dataRequest);
        }
        [WebMethod]
        public Model.ResponseDTO.SetResponse CreateSpecialTransaction(Model.RequestDTO.CreateSpecialTransactionRequest dataRequest)
        {
            return _tranProcess.CreateSpecialTran(dataRequest);
        }
        [WebMethod]
        public DataTable GetSpecialTranList(Model.RequestDTO.GetSpecialTranListRequest dataRequest)
        {
            return _tranProcess.dtGetSpecialTranList(dataRequest);
        }
        [WebMethod]
        public DataTable GetGeneralTranList(Model.RequestDTO.GetGeneralTranListRequest dataRequest)
        {
            return _tranProcess.dtGetGeneralTranList(dataRequest);
        }
    }
}
