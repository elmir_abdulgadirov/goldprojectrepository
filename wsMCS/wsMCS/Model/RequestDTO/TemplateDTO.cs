﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsMCS.Model.RequestDTO
{
    public class TempLoanPaymentRequest
    {
        public int TransactionID { get; set; }
    }
    public class TempCashJournalRequest
    {
        public int ACNT_ID { get; set; }
        /*public string ACNT_TYPE { get; set; }*/
        public int CurrencyID { get; set; }
        public int UserID { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public int BranchID { get; set; }
        public string Source { get; set; }
    }
}