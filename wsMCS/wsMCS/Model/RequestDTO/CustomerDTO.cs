﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace wsMCS.Model.RequestDTO
{
    public class AddCustomerRequest
    {
        public int BranchID { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public string Pin { get; set; }
        public string IdCardSeries { get; set; }
        public string IdCardNumber { get; set; }
        public string DateOfIssue { get; set; }
        public string DateOfExpiry { get; set; }
        public string DateOfBirth { get; set; }
        public string Authority { get; set; }
        public string Gender { get; set; }
        public int CountryID { get; set; }
        public int CityID { get; set; }
        public string LegalAddress { get; set; }
        public string RegistrationAddress { get; set; }
        public string MobilePhone1 { get; set; }
        public string MobilePhone2 { get; set; }
        public string MobilePhone3 { get; set; }
        public string HomePhone { get; set; }
        public string Note { get; set; }
        public string Email { get; set; }
        public int CreatedID { get; set; }
    }
    public class ModifyCustomerRequest
    {
        public int CustomerID { get; set; }
        public int BranchID { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public string Pin { get; set; }
        public string IdCardSeries { get; set; }
        public string IdCardNumber { get; set; }
        public string DateOfIssue { get; set; }
        public string DateOfExpiry { get; set; }
        public string DateOfBirth { get; set; }
        public string Authority { get; set; }
        public string Gender { get; set; }
        public int CountryID { get; set; }
        public int CityID { get; set; }
        public string LegalAddress { get; set; }
        public string RegistrationAddress { get; set; }
        public string MobilePhone1 { get; set; }
        public string MobilePhone2 { get; set; }
        public string MobilePhone3 { get; set; }
        public string HomePhone { get; set; }
        public string Note { get; set; }
        public string Email { get; set; }
        public int ModifiedID { get; set; }
    }
    public class DeleteCustomerRequest
    {
        public int CustomerID;
    }
    public class GetCustomerRequest
    {
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public int userID { get; set; }
        public string Pin { get; set; }
        public string IdCardNumber { get; set; }
        public string Fullname { get; set; }


    }  
}