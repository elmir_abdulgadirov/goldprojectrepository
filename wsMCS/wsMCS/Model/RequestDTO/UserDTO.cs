﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace wsMCS.Model.RequestDTO
{
    public class UserLoginRequest
    {
        public string UserLogin { get; set; }
        public string UserPassword { get; set; }
    }

    public class AddUserRequest
    { 
        public string FullName { get; set; }
        public string ContactNumber { get; set; }
        public string Sex { get; set; }
        public string Position { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Picture { get; set; }
        public string Status { get; set; }
        public int CreatedId { get; set; }
    }

    public class ChangePasswordRequest
    {
        public int UserID { get; set; }
        public string NewPassword { get; set; }
        public int ModifyId { get; set; }
    }

    public class ModifyUserRequest
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string ContactNumber { get; set; }
        public string Sex { get; set; }
        public string Position { get; set; }
        public string Picture { get; set; }
        public int ModifydId { get; set; }
    }
    public class ChangeUserStatusRequest
    {
        public int UserId { get; set; }
        public int ModifyId { get; set; }
        public string UserStatus { get; set; }
    }
    public class AddUserBranchRequest
    {
        public string BranchCode { get; set; }
        public int UserID { get; set; }
        public int CreatedID { get; set; }
    }
    public class DeleteUserBranchRequest
    {
        public int UserID;
    }
    public class AddUserFunctionRequest
    {
        public int UserID { get; set; }
        public int MenuID { get; set; }
        public string FunctonID { get; set; }
        public int CreatedID { get; set; }
    }
    public class DeleteUserFunctionRequest
    {
        public int UserID { get; set; }
        public int MenuID { get; set; }
    }
    public class CheckFunctionPermissionRequest
    {
        public int UserID { get; set; }
        public string FunctionID { get; set; }
    }
    public class GetBranchFullDataByIDRequest
    {
        public int BranchID { get; set; }
    }
    public class OpenDayRequest
    {
        public DateTime TranDate { get; set; }
        public int BranchID { get; set; }
        public int CreatedID { get; set; }
    }
    public class TranDateStatusRequest
    {
        public DateTime TranDate { get; set; }
        public int BranchID { get; set; }
    }
}
