﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsMCS.Model.RequestDTO
{
    public class AddAccountRequest
    {
        public int CustomerID { get; set; }
        public int AcntCurrencyID { get; set; }
        public string AcntName { get; set; }
        public int CreatedID { get; set; }
    }
    public class DeleteAccountRequest
    {
        public int AcntID;
        public int ModifiedID;
    }

    public class GetCustAcntsRequest
    {
        public int UserId { get; set; }
        public string AcntName { get; set; }
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
    }

    public class SearchCustCurrAcntsRequest
    {
        public int UserId { get; set; }
        public int AcntID { get; set; }
        public string AcntName { get; set; }
        public string CollaeralNumber { get; set; }
    }
    public class GetDebetAcntsByCurrAcntRequest
    {
        public int BranchID { get; set; }
        public int CurrencyID { get; set; }
    }
    public class GetCashTransactiontRequest
    {
        public string ACNT_TYPE { get; set; }
        public int CurrencyID { get; set; }
        public int UserID { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public int BranchID { get; set; }
        public string Source { get; set; }
    }
    public class GetCashBankRequest
    {
        public string OPER_TYPE { get; set; }
        public int UserID { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
    public class GetPortfolioListRequest
    {
        public string DateFrom { get; set; }
        public string DateTo { get; set; }     
        public int CurrencyID { get; set; }
        public int UserID { get; set; }    
        public int BranchID { get; set; }
    }
    public class GetAcntGroupedBalanceRequest
    {
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string AcntGroupCode { get; set; }
        public string AcntSubGroupCode { get; set; }
        public int CurrencyID { get; set; }
        public int BranchID { get; set; }
        public int UserID { get; set; }
    }
    public class GetAcntBalanceBySubGroupRequest
    {
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string AcntSubGroupCode { get; set; }
        public int CurrencyID { get; set; }
        public int BranchID { get; set; }
        public int UserID { get; set; }
    }
    public class GetTotalBalanceRequest
    {
        public int BranchID { get; set; }
        public int UserID { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
    public class GetAcntBalanceBySubSubCodeRequest
    {
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string AcntSubSubGroupCode { get; set; }
        public int BranchID { get; set; }
        public int UserID { get; set; }
    }
    public class GetCurAcntTranLIstRequest
    {
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string AcntName { get; set; }
        public int CurrencyID { get; set; }
        public int UserID { get; set; }
        public int BranchID { get; set; }
    }
    public class GetAllAcntBalanceByLoanIDRequest
    {
        public int LOAN_ID { get; set; }
    }
    public class GetSubAcntListRequest
    {
        public int MainAcntID { get; set; }
    }
}