﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsMCS.Model.RequestDTO
{
    public class createTranRequest
    {
        public string MsgID { get; set; }
        public int Debet_Acnt_ID { get; set; }
        public float Debet_Amount { get; set; }
        public int Debet_Acnt_CurrencyID { get; set; }
        public int Credit_Acnt_ID { get; set; }
        public float Credit_Amount { get; set; }
        public int Credit_Acnt_CurrencyID { get; set; }
        public string Source { get; set; }
        public string Description { get; set; }
        public string AddlText { get; set; }
        public int CreatedID { get; set; }
    }
    public class reverseTranRequest
    {
        public string MsgID { get; set; }
        public string Description { get; set; }
        public int ModifiedID { get; set; }
    }
    
    public class CreatePaymentByLoanIDRequest
    {
        public int LoanID { get; set; }
        public int MonthCount { get; set; }
        public int PaymentType { get; set; }
        public decimal PayedPrincipalAmount { get; set; }
        public decimal PayedExpPrincipalAmount { get; set; }
        public decimal PayedRateAmount { get; set; }
        public decimal PayedExpRateAmount { get; set; }
        public decimal PayedPenaltyAmount { get; set; }
        public string Note { get; set; }
        public int CreatedID { get; set; }
    }
    public class CreateCashBankTransactionRequest
    {
        public string ValueDate { get; set; }
        public string ResponsibilityPerson { get; set; }
        public string Assignment { get; set; }
        public decimal Amount { get; set; }
        public string Note { get; set; }
        public string TranType { get; set; }
        public string OperationType { get; set; }
        public int Corresp_AcntID { get; set; }
        public int CashBankAcntID { get; set; }
        public int CreatedID { get; set; }
    }
    public class DeleteCashBankTransactionRequest
    {
        public string MsgID { get; set; }
	    public string Description { get; set; }
	    public int ModifiedID { get; set; }	
    }
    public class CreateSpecialTransactionRequest
    {
        public string ValueDate { get; set; }
        public int DTAccountID { get; set; }
        public int CTAccountID { get; set; }
        public string SourceKey { get; set; }
        public decimal Amount { get; set; }
        public string Note { get; set; }
        public int CreatedID { get; set; }
    }
    public class GetSpecialTranListRequest
    { 
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public int UserID { get; set; }
        public int BranchID { get; set; }
    }
    public class GetGeneralTranListRequest
    {
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public int UserID { get; set; }
        public int BranchID { get; set; }
    }

}