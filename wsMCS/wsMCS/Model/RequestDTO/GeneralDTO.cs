﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsMCS.Model.RequestDTO
{
    public class loanAmortizaiotnRequest
    {
        public string startDate { get; set; }
        public double loanAmount { get; set; }
        public double monthlyRate { get; set; }
        public int monthPeriod { get; set; }
        public int discountPeriod { get; set; }
    }
}