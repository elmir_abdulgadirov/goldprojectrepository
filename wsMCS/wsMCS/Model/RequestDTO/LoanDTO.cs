﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsMCS.Model.RequestDTO
{
    public class AddLoanRequestDTO
    {
        public int CreatedID { get; set; }
    }
    public class ModifyLoanRequestDTO
    {
        public int LoanID { get; set; }
        public int BranchID { get; set; }
        public int CustomerID { get; set; }
        public int LoanTypeID { get; set; }
        public string StartDate { get; set; }
        public string PaymentDate { get; set; }
        public string DateOfExpiry { get; set; }
        public string CalculationDate { get; set; }
        public string CollateralNumber { get; set; }
        public string AgreementNumber { get; set; }
        public string AcNumber { get; set; }
        public float LoanAmount { get; set; }
        public float LoanInterest { get; set; }
        public int LoanPeriod { get; set; }
        public int DiscountPeriod { get; set; }
        public float CommissionFee { get; set; }
        public int LoanStatusID { get; set; }
        public int CurrencyID { get; set; }
        public int LoanMethodID { get; set; }
        public string DepositBox { get; set; }
        public string CollateralReturnDate { get; set; }
        public float JewelerCost { get; set; }
        public int AccountTypeID { get; set; }
        public string CollateralType { get; set; }
        public int LenderID { get; set; }
        public int VerifierID { get; set; }
        public int ModifiedID { get; set; }
    }
    public class ModifyLoanCollateralRequest
    {
        public int LoanID { get; set; }
        public string DepositBox { get; set; }
        public string JewelerName { get; set; }
        public float JewelerCost { get; set; }
        public int ModifiedID { get; set; }
    }
    public class DeleteLoanRequest
    {
        public int LoanID;
    }
    public class AddLoanNoteRequest
    {
        public string Note { get; set; }
        public int LoanID { get; set; }
        public int CreatedID { get; set; }
    }
    public class ModifyLoanNoteRequest
    {
        public int NoteID { get; set; }
        public string Note { get; set; }
        public int CreatedID { get; set; }
    }
    public class DeleteLoanNoteRequest
    {
        public int NoteID { get; set; }
        public int ModifiedId { get; set; }
    }
    public class GetLoanNoteRequest
    {
        public int LoanID { get; set; }
    }
    public class AddCardRequest
    {
        public string Pan { get; set; }
        public float CardAmount { get; set; }
        public string DateOfExpiry { get; set; }
        public string SecretWord { get; set; }
        public int LoanID { get; set; }
        public int CreatedID { get; set; }
    }
    public class ModifyCardRequest
    {
        public int CardID { get; set; }
        public string Pan { get; set; }
        public float CardAmount { get; set; }
        public string DateOfExpiry { get; set; }
        public string SecretWord { get; set; }
        public int LoanID { get; set; }
        public int ModifiedID { get; set; }
    }
    public class DeleteCardRequest
    {
        public int CardID { get; set; }
    }
    public class GetCardRequest
    {
        public int LoanId { get; set; }
    }
    public class AddGuarantorRequest
    {
        public int CusomterID { get; set; }
        public int LoanID { get; set; }
        public int CreatedID { get; set; }
    }
    public class DeleteGuarantorRequest
    {
        public int GuarantorID { get; set; }
        public int ModifiedID { get; set; }
    }
    public class AddLoanDocumentRequest
    {
        public int LoanID { get; set; }
        public string FileName {get; set;}
        public string Note { get; set; }
        public int CreatedID { get; set; }
    }
    public class DeleteLoanDocumentRequest
    {
        public int LoanDocumentID { get; set; }
        public int ModifiedID { get; set; }
    }
    public class AddCollateralRequest
    {
        public int CollateralTypeID { get; set; }
        public string CollateralName { get; set; }
        public string UnitOfMeasurement { get; set; }
        public int CaratID { get; set; }
        public float Price { get; set; }
        public int Count { get; set; }
        public float BruttoWeight { get; set; }
        public float Weight { get; set; }     
        public string Transaction { get; set; }
        public string Note { get; set; }
        public int LoanID { get; set; }
        public int CreatedID { get; set; }
    }
    public class ModifyCollateralRequest
    {   
        public int CollateralID { get; set; }
        public int CollateralTypeID { get; set; }
        public string CollateralName { get; set; }
        public string UnitOfMeasurement { get; set; }
        public int CaratID { get; set; }
        public float Price { get; set; }
        public int Count { get; set; }
        public float BruttoWeight { get; set; }
        public float Weight { get; set; }
        public string Note { get; set; }
        public int ModifiedID { get; set; }
    }
    public class DeleteCollateralRequest
    {
        public int CollateralID { get; set; }
        public int ModifiedID { get; set; }
    }
    public class setLoanLenderRequest
    {
        public int LoanID { get; set; }
        public int LenderID { get; set; }
        public int ModifiedID { get; set; }
    }
    public class getLoanLenderByLoanIDRequest
    {
        public int LoanID { get; set; }
    }
    public class setLoanVerifierRequest
    {
        public int LoanID { get; set; }
        public int VerifierID { get; set; }
        public int ModifiedID { get; set; }
    }
    public class getLoanVerifierByLoanIDRequest
    {
        public int LoanID { get; set; }
    }
    public class acceptLoanRequest
    {
        public int LoanID { get; set; }
        public string Description { get; set; }
        public int CreatedID { get; set; }
    }

    public class insertCollateralOutRequest
    {
        public int LoanID { get; set; }
        public int CreatedID { get; set; }
    }

    public class GetLoanListRequest
    {
        public int UserId { get; set; }
        public string CustomerName { get; set; }
        public string CollateralNumber { get; set; }
        public int LoanMethodID { get; set; }
        public string MobilePhone1 { get; set; }
        public string MobilePhone2 { get; set; }
        public string StartDate { get; set; }
        public string CollateralType { get; set; }
        public int CurrencyID { get; set; }
        public float LoanAmount { get; set; }
        public int LoanSTatusID { get; set; }
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
    }
    public class CollateralReturnRequest
    {
        public int LoanID { get; set; }
        public int ModifiedID { get; set; }
    }
    public class DeleteLoanByIDRequest
    {
        public int LoanID { get; set; }
        public string Description { get; set; }
        public int ModifiedID { get; set; }
    }
    public class GetCurAcntRequest
    {
        public int LoanID { get; set; }
    }
    public class getPaymentDebtByLoanIDRequest
    {
        public int LoanID { get; set; }
        public int MonthCount { get; set; }
        public int PaymentType { get; set; }
        public decimal Amount { get; set; }
    }
    public class GetCollateralOutListRequest
    { 
        public int UserID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int BranchID { get; set; }
        public string DepositBox { get; set; }
        public  string CollateralNumber { get; set; }
    }
    public class DeleteCollateralByOutIDRequest
    {
        public int OutID { get; set; }
        public string Description { get; set; }
        public int ModifiedID { get; set; }
    }
    public class GetCollateralListRequest
    {
        public int UserID { get; set; }
        public int CurrencyID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int BranchID { get; set; }
    }
    public class GetTempCollateralListRequest
    {
        public string DepositBox { get; set; }
        public int CurrencyID { get; set; }
        public int BranchID { get; set; }
    }
    public class GetDelayedLoansListRequest
    {
        public string CollateralNumber { get; set; }
        public string Fullname { get; set; }
        public int CurrencyID { get; set; }
        public int BranchID { get; set; }
        public int UserID { get; set; }
        public int DayOfLoan { get; set; }
        public string ColorType { get; set; }
    }
    public class ChangeLoanStatusRequest
    {
        public int LoanID { get; set; }
        public int LoanStatusID { get; set; }
        public int ModifiedID { get; set; }
    }
    public class GetLoansByDateListRequest
    {
        public int UserID { get; set; }
        public int CurrencyID { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public int BranchID { get; set; }
        public string FullName { get; set; }
        public string CollateralNumber { get; set; }
    }
    public class GetControlLoansListRequest
    {
        public int CurrencyID { get; set; }
        public int UserID { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public int BranchID { get; set; }
        public string CollateralNumber { get; set; }
        public string Fullname { get; set; }  
    }
    public class GetEndedLoansListRequest
    {
        public int CurrencyID { get; set; }
        public int UserID { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public int BranchID { get; set; }
        public string CollateralNumber { get; set; }
        public string Fullname { get; set; }
        public int LoanStatusID { get; set; }
    }
    public class GetLoanPaymentListRequest
    {
        public int CurrencyID { get; set; }
        public int UserID { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public int BranchID { get; set; }
        public string CollateralNumber { get; set; }
        public string Fullname { get; set; }
    }
    public class DeleteLoanPaymentRequest
    {
        public int PaymentID { get; set; }
        public string Description { get; set; }
        public int ModifiedID { get; set; }
    }
}