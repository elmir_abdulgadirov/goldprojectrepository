﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace wsMCS.Model.ResponseDTO
{
    public class AddCustomerResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class ModifyCustomerResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class DeleteCustomerResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
}