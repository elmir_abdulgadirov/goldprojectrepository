﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsMCS.Model.ResponseDTO
{
    public class createTranResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class reverseTranResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
}