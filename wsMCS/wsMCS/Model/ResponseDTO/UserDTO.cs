﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace wsMCS.Model.ResponseDTO
{
    public class UserLoginResponse
    {
        public bool IsLogin { get; set; }
        public string ErrorCode { get; set; }
        public string FullName { get; set; }
        public string ContactNumber { get; set; }
        public string Sex { get; set; }
        public string Position { get; set; }
        public string Login { get; set; }
        public string Picture { get; set; }
        public int UserId { get; set; }
        public string SessionKey { get; set; }
        public string UserPermission { get; set; }
    }

    public class AddUserResponse
    { 
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }

    public class ChangePasswordResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }

    public class ModifyUserResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class ChangeUserStatusResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class AddUserBranchResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class DeleteUserBranchResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class AddUserFunctionRespnse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class DeleteUserFunctionRespnse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class CheckFunctionPermissionResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
        public string Permisssion { get; set; }
    }
    public class GetBranchFullDataByIDResponse
    {
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string BranchTaxID { get; set; }
        public string Adres { get; set; }
        public string Mobil { get; set; }
        public string OfficePhone { get; set; }
        public string Branch_Mail { get; set; }
        public string Branch_Mail_Password { get; set; }
        public string CompanyCode { get; set; }
        public string DirectorName { get; set; }
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
}
