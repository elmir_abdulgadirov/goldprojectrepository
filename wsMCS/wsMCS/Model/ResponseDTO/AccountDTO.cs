﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsMCS.Model.ResponseDTO
{
    public class AddAccountResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class DeleteAccountResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class SearchAccountResponse
    {
        public string CurrencyName { get; set; }
    }
    public class GetAllAcntBalanceByLoanIDResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
        public string custCurrent_Balance { get; set; }
        public string sSuda_Balance { get; set; }
        public string expSsuda_Balance { get; set; }
        public string rate_Balance { get; set; }
        public string expRate_Balance { get; set; }
        public string penalty_Balance { get; set; }
        public string expDebt_Balance { get; set; }
        public string closeDebt_Balance { get; set; }
    }
}