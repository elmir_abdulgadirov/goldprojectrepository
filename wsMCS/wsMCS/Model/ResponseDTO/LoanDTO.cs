﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wsMCS.Model.ResponseDTO
{
    public class AddLoanResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
        public int loanId { get; set; }
    }
    public class ModifyLoanResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class ModifyLoanCollateralResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class DeleteLoanResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class AddLoanNoteResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class ModifyLoanNoteResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class DeleteLoanNoteResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class AddCardResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class ModifyCardResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class DeleteCardResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class AddGuarantorResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class DeleteGuarantorResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class AddLoanDocumentResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class DeleteLoanDocumentResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class AddCollateralResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class ModifyCollateralResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class DeleteCollateralResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class SetLoanLenderResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class SetLoanVerifierResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class acceptLoanResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }

    public class SetResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class CollateralReturnResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class GetCurAcntResponse
    {
        public int AcntID { get; set; }
        public string AcntName { get; set; }
        public string AcntCode { get; set; }
        public int CurrencyID { get; set; }
        public string CurrencyName { get; set; }
        public decimal CurBalance { get; set; }
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
    }
    public class GetPaymentDebtResponse
    {
        public bool isSuccess { get; set; }
        public string errorCode { get; set; }
        public decimal payedPrincipalAmount { get; set; }
        public decimal payedExpPrincipalAmount { get; set; }
        public decimal payedRateAmount { get; set; }
        public decimal payedExpRateAmount { get; set; }
        public decimal payedPenaltyAmount { get; set; }
        public decimal payedTotalAmount { get; set; }
    }
}