﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="TestPrint_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/StyleSheet.css" rel="stylesheet" />
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <table class="body-wrap" id ="payment_print">
        <tbody>
            <tr>
                <td></td>
                <td class="container" width="100%">
                    <div class="content">
                        <table class="main" width="100%" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td class="content-wrap aligncenter">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td class="content-block">
                                                        <h2>Thanks for using our app</h2>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="content-block">
                                                        <table class="invoice">
                                                            <tbody>
                                                                <tr>
                                                                    <td>Anna Smith<br>
                                                                        Invoice #12345<br>
                                                                        June 01 2015</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <table class="invoice-items" cellpadding="0" cellspacing="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>Service 1</td>
                                                                                    <td class="alignright">$ 20.00</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Service 2</td>
                                                                                    <td class="alignright">$ 10.00</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Service 3</td>
                                                                                    <td class="alignright">$ 6.00</td>
                                                                                </tr>
                                                                                <tr class="total">
                                                                                    <td class="alignright" width="80%">Total</td>
                                                                                    <td class="alignright">$ 36.00</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                               
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                       
                    </div>
                </td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <br />
    <asp:Button ID="Button1" Style ="width:100%" OnClick ="Button1_Click" runat="server" Text="Button" />
    



    <script>

        function PrintPayment() {
            var mywindow = window.open('', 'PRINT');

           
           
            mywindow.document.write('<html><head>');
            mywindow.document.write('<link href="css/StyleSheet.css" rel="stylesheet" />');        
            mywindow.document.write('</head><body >');
            mywindow.document.write(document.getElementById('payment_print').innerHTML);
            mywindow.document.write('</body></html>');
            

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/

            mywindow.print();
            mywindow.close();
           // document.getElementById('payment_print').style.display = 'none';
            return true;
        }

    </script>

</asp:Content>

