﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Payments_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">Ödəmələr</span>
        </nav>

        <div class="sl-pagebody">
            <div class="card pd-20 pd-sm-40">



                <!-- SMALL MODAL -->
                <div id="alertmodal" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                            <div class="modal-footer justify-content-right" style="padding: 5px">
                                <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->



                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>



                        <!-- LARGE MODAL -->
                        <div id="modalObjectDebt" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" style="max-width:1000px" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">&nbsp&nbspÖDƏNİŞ</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrObjPayMessage" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">
                                            <div class="row mg-t-20">
                                                <div class="col-lg">
                                                    Ödəmə Tarix
                                                     <asp:TextBox ID="txtPaymentDate" runat="server" MaxLength="10" class="form-control fc-datepicker" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row mg-t-20">
                                                <div class="col-lg">
                                                    Qalıq borc:
                                                    <asp:TextBox ID="txtRemainDebt" Enabled="false" runat="server" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    Ödəniləcək məbləğ:
                                                    <asp:TextBox ID="txtPayAmount" runat="server" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                                 <div class="col-lg">
                                                    Boş məbləğ:
                                                    <asp:TextBox ID="txtRevAmount" runat="server" Text ="0" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    Güzəşt məbləğ:
                                                    <asp:TextBox ID="txtDiscountAmount" runat="server" Text ="0" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    Köçürmə məbləğ:
                                                    <asp:TextBox ID="txtTransferAmount" runat="server" Text ="0" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row mg-t-20">
                                                <div class="col-lg">
                                                    ƏDV borc:
                                                    <asp:TextBox ID="txtRemainVATDebt" Enabled="false" runat="server" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>

                                                <div class="col-lg">
                                                    Ödəniləcək ƏDV:
                                                    <asp:TextBox ID="txtPayVatAmount" runat="server" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    Boş ƏDV:
                                                    <asp:TextBox ID="txtRevVat" runat="server" Text="0" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    Güşəşt ƏDV:
                                                    <asp:TextBox ID="txtDiscountVAT" runat="server" Text="0" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    Köçürmə ƏDV:
                                                    <asp:TextBox ID="txtTransferVatAmount" runat="server" Text="0" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="row mg-t-20">
                                                <div class="col-lg">
                                                    <asp:Button ID="btnPay" OnClick="btnPay_Click" CssClass="btn btn-success" runat="server" Text="ÖDƏMƏ ET" />
                                                    <button id="btnPayCancel" style="margin-left: 10px" type="button" class="btn btn-danger" data-dismiss="modal">İMTİNA ET</button>
                                                    <asp:Button ID="btnRevAmount" OnClick="btnRevAmount_Click" CssClass="btn btn-secondary" runat="server" Text="BOŞ ET" />
                                                    <asp:Button ID="btnDiscount" OnClick="btnDiscount_Click" CssClass="btn btn-primary" runat="server" Text="GÜZƏŞT ET" />
                                                    <asp:Button ID="btnTransferAmount" OnClick="btnTransferAmount_Click" CssClass="btn btn-primary" runat="server" Text="KÖÇÜRMƏ ET" />
                                                </div>
                                                <div style="clear: both"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->


                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnPay" />
                        <asp:PostBackTrigger ControlID ="btnRevAmount" />
                        <asp:PostBackTrigger ControlID ="btnDiscount" />
                        <asp:PostBackTrigger ControlID ="btnTransferAmount" />
                    </Triggers>
                </asp:UpdatePanel>



                <!-- LARGE MODAL -->
                <div id="modalPaymentDetails" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content tx-size-sm">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">&nbsp;&nbsp;ÖDƏNİŞ (ÇAP / SİLİNMƏ)</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body pd-20">
                                <asp:Literal ID="ltrModalPaymentDetails" runat="server"></asp:Literal>
                                <div class="card pd-20 pd-sm-40">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="table-responsive">
                                                <asp:GridView ID="GrdPaymentDetails" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
                                                    <Columns>
                                                        <asp:BoundField DataField="PaymentDate" HeaderText="ƏMƏLİYYAT TARİXİ" />
                                                        <asp:BoundField DataField="PaymentType" HeaderText="ÖDƏMƏ TİPİ" />
                                                        <asp:BoundField DataField="TranType" HeaderText="ƏMƏLİYYAT TİPİ" />
                                                        <asp:BoundField DataField="PaymentAmount" HeaderText="MƏBLƏĞ" />
                                                        <asp:BoundField DataField="MakerName" HeaderText="QƏBUL EDƏN" />
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkPaymentPrint" runat="server" OnClick="lnkPaymentPrint_Click"
                                                                    CommandArgument='<%#Convert.ToString(Eval("ID")).Trim() %>'>
                                                                            <img src ="/img/print.png" title ="Çap et"/>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkPaymentDelete" runat="server"
                                                                    OnClick="lnkPaymentDelete_Click"
                                                                    OnClientClick="return confirm('Ödənişi silmək istədiyinizdən əminsiniz?');"
                                                                    CommandArgument='<%#Convert.ToString(Eval("ID")).Trim() %>'>
                                                                            <img src ="/img/delete_icon.png" title ="Sil"/>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        ÖDƏNİŞ YOXDUR...
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-body -->
                            <div class="modal-footer">
                                <button id="btnClose" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Bağla</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->





                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <asp:LinkButton ID="lnkSearchObjectCode" OnClick="lnkSearchObjectCode_Click" runat="server"><b>Obyekt kodu üzrə</b></asp:LinkButton>
                    </li>
                    <li class="nav-item">
                        <asp:LinkButton ID="lnkSearchZoneCode" OnClick="lnkSearchZoneCode_Click" runat="server"><b>Zona / Sıra / Korpus üzrə</b></asp:LinkButton>
                    </li>
                </ul>

                <asp:MultiView ID="MViewSearch" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <br />
                        <div class="row">
                            <div class="col-lg">
                                Obyekt nömrəsi
                                <asp:TextBox ID="txtSearchObjectFullNumber" runat="server" MaxLength="25" class="form-control" placeholder="Obyekt kodu" type="text"></asp:TextBox>
                            </div>
                            <div class="col-lg">
                                Müştəri İD
                                <asp:TextBox ID="txtSearchCustomerID" runat="server" MaxLength="25" class="form-control" placeholder="Müştəri İD" type="text"></asp:TextBox>
                            </div>
                            <div class="col-lg">
                                VÖEN
                                <asp:TextBox ID="txtSearchTaxID" runat="server" MaxLength="10" class="form-control" placeholder="VÖEN" type="text"></asp:TextBox>
                            </div>
                             <div class="col-lg">
                                 Borc Tarixi:
                                 <asp:TextBox ID="txtDebtDate"  runat="server" AutoCompleteType="Disabled" MaxLength="10" placeholder="dd.mm.yyyy" class="form-control fc-datepicker" type="text"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-lg-auto">
                                <img id="loading1" style="display: none" src="../img/loader.gif" />
                                <asp:Button ID="btnSearchByObjectCode" class="btn btn-info pd-x-20" runat="server" Text="Axtar"
                                    OnClientClick="this.style.display = 'none';
                                    document.getElementById('loading1').style.display = '';"
                                    OnClick="btnSearchByObjectCode_Click" />
                            </div>
                            <div class="col-lg-auto" style="min-width:50px">
                                <asp:LinkButton ID="btnObjectDetail"
                                        Cssclass="btn btn-info pd-x-20" runat="server" Text="Məlumat"
                                        CommandArgument='<%#Eval("RegNumber")%>'
                                        OnClick="btnObjectDetail_Click">                                    
                                    </asp:LinkButton>
                            </div>
                            <div class="col-lg-auto">
                                <asp:Literal ID="ltrTenantData" Text="" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <br />
                        <div class="row">
                            <div class="col-lg">
                                <asp:DropDownList ID="drlZone" OnSelectedIndexChanged="drlZone_SelectedIndexChanged" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-lg">
                                <asp:DropDownList ID="drlLine" OnSelectedIndexChanged="drlLine_SelectedIndexChanged" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-lg">
                                <asp:DropDownList ID="drlCorpus" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-lg">
                                <img id="loading2" style="display: none" src="../img/loader.gif" />
                                <asp:Button ID="btnSearchByZoneCode" class="btn btn-info pd-x-20" runat="server" Text="Axtar"
                                    OnClientClick="this.style.display = 'none';
                                    document.getElementById('loading2').style.display = '';"
                                    OnClick="btnSearchByZoneCode_Click" />
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
                <br />
                <asp:Literal ID="ltrTotalDebt" runat="server"></asp:Literal>
                <br />
                <br />
                <div class="table-responsive">
                     <asp:GridView ID="GrdObjectDebt" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" DataKeyNames="Debt_ID" GridLines="None">
                        <Columns>
                            <asp:BoundField DataField="Value_date_desc" HeaderText="BORC TARİXİ" />
                             <asp:BoundField DataField="Debt_TypeDesc" HeaderText="BORC TİPİ">
                                <ItemStyle Font-Bold="True" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton Enabled="false" ID="lnkObjectDetailsShow" runat="server"
                                        CommandArgument='<%#Eval("ObjectFullNumber")%>'>
						                <%#Eval("ObjectFullNumber")%>
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    OBYEKT KODU
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>
                           
                             <asp:TemplateField>
                                <ItemTemplate>
                                   <%#  Convert.ToString(Eval("PaymentMethod")) == "Nəğd" ? "<font style = \"color: Green; font-weight: bold;\">" + Eval("PaymentMethod") +  "</font>" : "<font style = \"color: blue; font-weight: bold;\">" + Eval("PaymentMethod") %>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    ÖDƏNİŞ TİPİ
                                </HeaderTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Config.ToDecimal(Convert.ToString(Eval("Debt_Amount"))) > 0 ? "<font style = \"color:red; font-weight: bold;\">" + Eval("Debt_Amount") +  "</font>" : Eval("Debt_Amount")%>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    PLAN
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Paid_Amount" HeaderText="ÖDƏMƏ" />
                            <asp:BoundField DataField="Reducton_Amount" HeaderText="BOŞ" />
                            <asp:BoundField DataField="Discount_Amount" HeaderText="GÜZƏŞT" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Config.ToDecimal(Convert.ToString(Eval("Remain_Amount"))) == 0 ?
                                          "<font style = \"color:green;font-weight: bold;\">" + Eval("Remain_Amount") +  "</font>"  :
                                          "<font style = \"color:red;font-weight: bold;\">" + Eval("Remain_Amount") +  "</font>" %>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    QALIQ
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Vat_Amount" HeaderText="ƏDV" />
                            <asp:BoundField DataField="Remain_VatAmount" HeaderText="ƏDV Qalıq" />

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkPay"
                                        Visible='<%#Config.ToDecimal(Convert.ToString(Eval("Remain_Amount"))) > 0 ||
                                            Config.ToDecimal(Convert.ToString(Eval("Remain_VatAmount"))) > 0 ? true : false %>'
                                        CssClass="btn btn-danger" runat="server"
                                        CommandArgument='<%#Eval("Debt_ID")%>'
                                        OnClick="lnkPay_Click">
                                        ÖDƏ
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="60px" HorizontalAlign="Right" />
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkPaymentDetails" title="Ödənişin çapı və silinməsi" OnClick="lnkPaymentDetails_Click" CommandArgument='<%#Eval("Debt_ID")%>' runat="server">
                                       <img src ="../img/payment_details.png" />
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="50px" HorizontalAlign="Right" />
                            </asp:TemplateField>


                        </Columns>
                    </asp:GridView>

                    <asp:Literal ID="ltrPrintData" runat="server"></asp:Literal>

                </div>

            </div>
        </div>
        <!-- LARGE MODAL -->
                <div id="modalObjectDetails" class="modal fade" data-backdrop="static" style="height:auto">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content tx-size-sm">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">&nbsp;&nbsp;OBYEKT MƏLUMATLARI</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body pd-20" >
                                <asp:Literal ID="ltrModalObjectDetails" runat="server"></asp:Literal>
                                <div class="card pd-20 pd-sm-40">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="table-responsive">
                                                <asp:GridView ID="GrdObjectDetails" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True" ShowFooter="True">
                                                    <Columns>
                                                        <asp:BoundField DataField="ObjectFullNumber" HeaderText="OBYEK KODU" />
                                                        <asp:TemplateField HeaderText="M2">
                                                            <ItemTemplate>   
                                                                <asp:Label ID="lblGrdObjectAreaValue" runat="server" Text='<%#Eval("ObjectArea") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <b><asp:Label ID="lblGrdObjectAreaSum" Text ="0" runat="server"></asp:Label></b>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                        
                                                        <asp:BoundField DataField="ObjectPrice" HeaderText="QİYMƏT" />

                                                        <asp:TemplateField HeaderText="AYLIQ PLAN">
                                                            <ItemTemplate>   
                                                                <asp:Label ID="lblGrdMonthlyPaymentValue" runat="server" Text='<%#Eval("MonthlyPayment") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <b><asp:Label ID="lblGrdMonthlyPaymentSum" Text ="0" runat="server"></asp:Label></b>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="GÜZƏŞT">
                                                            <ItemTemplate>   
                                                                <asp:Label ID="lblGrdObjectDiscountValue" runat="server" Text='<%#Eval("ObjectDiscount") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <b><asp:Label ID="lblGrdObjectDiscountSum" Text ="0" runat="server"></asp:Label></b>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="YEKUN PLAN">
                                                            <ItemTemplate>   
                                                                <asp:Label ID="lblGrdNetPaymentValue" runat="server" Text='<%#Eval("NetPayment") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                <b><asp:Label ID="lblGrdNetPaymentSum" Text ="0" runat="server"></asp:Label></b>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>
                                                                                                      
                                                    </Columns>                                                                                             
                                                    <EmptyDataTemplate>
                                                        OBYEKT YOXDUR...
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-body -->
                            <div class="modal-footer">
                                <button id="btnObjectClose" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Bağla</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->

        <style>
            .payment_print_tab tbody td  {
             font-family: Tahoma;
             font-size: 10px;
            }
             .table-bordered, tbody tr th, table td{
                border-bottom: 1px solid #0b2254 !important;
                background:#fff !important;
                color:#000000 !important;
            }
        </style>

        <script type="text/javascript">
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }

            function openObjectDebtModal() {
                $('#modalObjectDebt').modal({ show: true });
            }

            function openPaymentDetailsModal() {
                $('#modalPaymentDetails').modal({ show: true });
            }

            function openObjectDetailsModal() {
                $('#modalObjectDetails').modal({ show: true });
            }

            function PrintPayment() {
                var mywindow = window.open('', 'PRINT');

                mywindow.document.write('<html><head>');
                mywindow.document.write('<link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet" />');
                mywindow.document.write('<link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet" />');
                mywindow.document.write('<link href="../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />');
                mywindow.document.write('<link rel="stylesheet" href="../css/starlight.css" />');
                mywindow.document.write('<style>.payment_print_tab tbody td  {font-family: "Roboto", "Helvetica Neue", Arial, sans-serif; font-size: 12px;padding: 0.3rem;}</style>');
                mywindow.document.write('</head><body >');
                mywindow.document.write(document.getElementById('payment_print').innerHTML);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10*/

                mywindow.print();
                mywindow.close();
                document.getElementById('payment_print').style.display = 'none';
                return true;
            }
        </script>
         <script src="../lib/jquery/jquery.js"></script>

        <style>
            .ui-datepicker {
                z-index: 1151 !important; /* has to be larger than 1050 */
            }
        </style>
          <script type="text/javascript">
              $(function () {
                  'use strict';
                  // Datepicker
                  $('.fc-datepicker').datepicker({
                      dateFormat: 'dd.mm.yy',
                      showOtherMonths: true,
                      selectOtherMonths: true
                  });
              });
          </script>

    </div>
</asp:Content>

