﻿<%@ Page Title="HESABAT-PLAN" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="ReportPayments_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet" />
    <link href="../lib/spectrum/spectrum.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">Hesabat - Plan üzrə</span>
        </nav>

        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>Axtarış filterləri</h5>
            </div>
            <div class="card pd-20 pd-sm-40">

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

                        <!-- SMALL MODAL -->
                        <div id="alertmodal" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content bd-0 tx-14">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                                    <div class="modal-footer justify-content-right" style="padding: 5px">
                                        <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->


                        <div class="row">
                            <div class="col-lg">
                                <asp:DropDownList ID="drlZone" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" OnSelectedIndexChanged="drlZone_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div><br />
                            <div class="col-lg">
                                <asp:DropDownList ID="drlLine" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" OnSelectedIndexChanged="drlLine_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div><br />
                            <div class="col-lg">
                                <asp:DropDownList ID="drlCorpus" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server"></asp:DropDownList>
                            </div><br />
                        </div><br />

                        <div class="row">
                            <div class="col-lg-6">
                                 Tarixdən:
                                      <asp:TextBox ID="txtFromDate" runat="server" MaxLength="10" placeholder="dd.mm.yyyy" class="form-control fc-datepicker" type="text"></asp:TextBox>
                                 </div>
                                  <div class="col-lg-6">
                                  Tarixə:
                                       <asp:TextBox ID="txtToDate" runat="server" MaxLength="10" placeholder="dd.mm.yyyy" class="form-control fc-datepicker" type="text"></asp:TextBox>
                                   </div>
                               </div><br />
                        <div class="row">
                            <div class="col-lg-6">
                                 Ödəmə Tipi:
                                 <asp:DropDownList ID="drlPaymentType" class="form-control" runat="server">
                                     <asp:ListItem Value="" Selected="True">--seçin--</asp:ListItem>
                                     <asp:ListItem Value="0">NƏĞD</asp:ListItem>
                                     <asp:ListItem Value="1">KÖÇÜRMƏ</asp:ListItem>
                                     <asp:ListItem Value="2">NƏĞD/KÖÇÜRMƏ</asp:ListItem>
                                 </asp:DropDownList>
                           </div>                       
                                 <div class="col-lg-6">
                                     Borc Tipi:
                                    <asp:DropDownList ID="drlDeptType" class="form-control" runat="server">
                                    <asp:ListItem Value="" Selected="True">--seçin--</asp:ListItem>
                                    <asp:ListItem Value="PLACE">YER PULU</asp:ListItem>
                                    <asp:ListItem Value="OWNER">SAHİBKAR PULU</asp:ListItem>
                                    <asp:ListItem Value="ELECTRIC">ELEKTRİK BORCU</asp:ListItem>
                                    <asp:ListItem Value="WATER">SU BORCU</asp:ListItem>
                                    <asp:ListItem Value="TAX">VERGİ XİDMƏTİ</asp:ListItem>
                                    <asp:ListItem Value="INTERNET">İNTERNET XİDMƏTİ</asp:ListItem>
                                    <asp:ListItem Value="TEND">TEND PULU</asp:ListItem>
                                    <asp:ListItem Value="VAT">ƏDV</asp:ListItem>
                                    </asp:DropDownList>
                                 </div>
                           </d>
                        </div>
                        <br />
                        <asp:Literal ID="ltrDataCount" runat="server"></asp:Literal><br />
                        <div class="row">
                            <div style="text-align: right">
                            <asp:LinkButton ID="LinkButton1" OnClick ="lnkDownloadExcel_Click" Visible="false"  runat="server" CssClass="btn btn-outline-info">
								   <img style ="width:16px;height:16px" src ="/img/excel_icon.png" />&nbsp&nbspExcel-ə yüklə
                            </asp:LinkButton>
                            </div>
                            <div class="col-lg" style="text-align: right">
                                <img id="loading_image" style="display: none" src="../img/loader.gif" />
                                <asp:Button ID="btnSearch" CssClass="btn btn-primary" runat="server" Text="Axtar" OnClick="btnSearch_Click"
                                    OnClientClick="this.style.display = 'none';
													document.getElementById('loading_image').style.display = '';" />
                            </div>
                        </div>
                        <br />
                        
                        <div style="text-align: right">
                            <asp:LinkButton ID="lnkDownloadExcel" OnClick ="lnkDownloadExcel_Click" Visible="false"  runat="server" CssClass="btn btn-outline-info">
								   <img style ="width:16px;height:16px" src ="/img/excel_icon.png" />&nbsp&nbspExcel-ə yüklə
                            </asp:LinkButton>
                        </div>
                        <br />
                        <div class="table-responsive">
                            <asp:GridView ID="GrdReports" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            ZONA : <%#Eval("ZONA")%>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            ZONA
                                        </HeaderTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                             <%#Eval("DEBT_TYPE")%>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            BORC TİPİ
                                        </HeaderTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                             <%#Eval("ODEME_TIPI")%>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            ÖDƏNİŞ TİPİ
                                        </HeaderTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            PLAN : <%#Eval("HESABLAMA")%>
                                            <br />
                                            ÖDƏMƏ : <%#Eval("ODEME")%>
                                            <br />
                                            BOŞ : <%#Eval("BOSH")%>
                                            <br />
                                            GÜZƏŞT : <%#Eval("GUZESHT")%>
                                            <br />
                                            QALIQ : <%#Eval("QALIQ_ESAS")%>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            ƏSAS BORC
                                        </HeaderTemplate>
                                    </asp:TemplateField>

                                     <asp:TemplateField>
                                        <ItemTemplate>
                                            PLAN : <%#Eval("EDV")%>
                                            <br />
                                            ÖDƏMƏ : <%#Eval("EDV_ODEME")%>
                                            <br />
                                            BOŞ : <%#Eval("EDV_BOSH")%>
                                            <br />
                                            GÜZƏŞT : <%#Eval("EDV_GUZESHT")%>
                                            <br />
                                            QALIQ : <%#Eval("QALIQ_EDV")%>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            ƏDV BORC
                                        </HeaderTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                             <ItemTemplate>
                                            PLAN : 
                                            <br />
                                            ÖDƏMƏ : 
                                            <br />
                                            BOŞ : 
                                            <br />
                                            GÜZƏŞT : 
                                            <br />
                                            QALIQ : 
                                        </ItemTemplate>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            CƏM BORC
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>

                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="lnkDownloadExcel" />
                        <asp:PostBackTrigger ControlID="btnSearch" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>

        <script src="../lib/jquery/jquery.js"></script>

        <style>
            .ui-datepicker {
                z-index: 1151 !important; /* has to be larger than 1050 */
            }
        </style>

        <script type="text/javascript">
            $(function () {
                'use strict';
                // Datepicker
                $('.fc-datepicker').datepicker({
                    dateFormat: 'dd.mm.yy',
                    showOtherMonths: true,
                    selectOtherMonths: true
                });
            });
        </script>

        <script>
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }
        </script>
    </div>
</asp:Content>


