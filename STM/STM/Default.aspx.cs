﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["LogonUserID"] != null) Config.Rd("/index");
            ImgSecurity.ImageUrl = "~/Captcha/?" + new Random().Next().ToString();
        }
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        DbProcess db = new DbProcess();
        if (txtLogin.Text.Length < 3)
        {
            ltrAlertMsg.Text = Alert.DangerMessage("İstifadəçi adını daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            txtLogin.Focus();
            return;
        }

        if (txtPassword.Text.Length < 5)
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Şifrəni daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            txtPassword.Focus();
            return;
        }
        string security_code = txtSecurity.Text;
        txtSecurity.Text = "";
        ImgSecurity.ImageUrl = "~/Captcha/?" + new Random().Next().ToString();

        string Er = "";
        if (security_code.Length < 1) Er = "*";
        if (Config.Cs(Session["SecurityKod"]).Length < 1) Er = "*";
        if (Config.Cs(Session["SecurityKod"]) != security_code) Er = "*";

        if (Er == "*")
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Təhlükəsizlik kodu düzgün deyil.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        string pUserInfo = "";
        string login_result = db.dbLogin(txtLogin.Text, txtPassword.Text, ref pUserInfo);

        if (login_result != "OK")
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Xəta: " + login_result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        string[] uInfo = pUserInfo.Split('#');
        try
        {
            if (uInfo.Length != 3)
            {
                ltrAlertMsg.Text = Alert.DangerMessage("Sistemdə xəta baş verdi..");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                return;
            }
            Session["LogonUserID"] = Convert.ToInt32(uInfo[0].Trim());
            Session["LoginUserName"] = Convert.ToString(uInfo[1].Trim());
            Session["LoginUserImage"] = Convert.ToString(uInfo[2].Trim());
        }
        catch
        {
            Session.Clear();
            Session.Abandon();
            ltrAlertMsg.Text = Alert.DangerMessage("Sistemdə xəta baş verdi.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        Config.Rd("/index");
        
       
      
    }
}