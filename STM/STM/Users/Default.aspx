﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Users_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .custom-file-control:before {
            content: "Şəkil yüklə";
        }

        .custom-file-control:after {
            content: "İstifadəçinin şəkili..";
        }

        .ckbox span:after {
        }

        .userimage {
            width: 100px;
            border-radius: 100%;
            display: flex;
            margin-right: 15px;
        }
    </style>


    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">İstifadəçilər</span>
        </nav>

        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>İstifadəçi tənzimləməsi</h5>
            </div>
            <div class="card pd-20 pd-sm-40">
                <!-- SMALL MODAL -->
                <div id="alertmodal" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>

                            <div class="modal-footer justify-content-right" style="padding: 5px">
                                <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->


                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <asp:LinkButton ID="btnAddNew" runat="server"
                            CssClass="btn btn-outline-info btn-block"
                            Style="width: 20% !important"
                            data-toggle="modal" data-target="#modaldemo3">
                    <i class="icon ion-person-add"></i> Yeni İstifadəçi
                        </asp:LinkButton>
                        <br />
                        <div class="table-responsive">
                            <asp:GridView ID="grdUsers" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" GridLines="None" EnableModelValidation="True">
                                <Columns>
                                    <asp:BoundField DataField="Login" HeaderText="İSTİFADƏÇİ ADI" />
                                    <asp:BoundField DataField="FullName" HeaderText="S.A.A" />
                                    <asp:BoundField DataField="ContactNumber" HeaderText="ƏLAQƏ NÖMRƏSİ" />
                                    <asp:BoundField DataField="Position" HeaderText="VƏZİFƏSİ" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkViewUserRole"
                                                title="İstifadəçi məlumatları və hüquqları" runat="server"
                                                class="btn btn-outline-dark btn-icon mg-r-5" CommandArgument='<%# Eval("ID") %>' OnClick="lnkViewUserRole_Click">
                                                <div><i class="fa fa-cog"></i></div></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDeleteUser"
                                                class="btn btn-outline-danger btn-block"
                                                Width="38px" Height="38px" runat="server" Text="X" Font-Bold="True"
                                                title="Sil"
                                                CommandArgument='<%# Eval("ID") %>' OnClick="btnDeleteUser_Click"
                                                OnClientClick="return confirm('İstifadəçini silmək istədiyinizdən əminsiniz?');" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>


                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <!-- LARGE MODAL -->
                                <div id="modaldemo3" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-person-add"></i>&nbsp&nbspYENİ İSTİFADƏÇİ</h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <asp:Literal ID="ltrMessage" runat="server"></asp:Literal>
                                                <div class="card pd-20 pd-sm-40">
                                                    <div class="row">
                                                        <div class="col-lg-8">
                                                            <asp:TextBox ID="txtUserFullName" MaxLength="60" runat="server" class="form-control" placeholder="Soyad, ad, ata adı" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:TextBox ID="txtUserContactNumber" MaxLength="12" runat="server" class="form-control" placeholder="Əlaqə nömrəsi" type="text"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            <asp:TextBox ID="txtUserLogin" runat="server" MaxLength="20" class="form-control" placeholder="İstifadəçi adı" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:TextBox ID="txtUserPassword" runat="server" MaxLength="50" class="form-control" placeholder="Şifrə" TextMode="Password"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:TextBox ID="txtUserPasswordRepeat" runat="server" MaxLength="50" class="form-control" placeholder="Şifrə təkrar" TextMode="Password"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg-8">
                                                            <asp:TextBox ID="txtUserPosition" runat="server" MaxLength="50" class="form-control" placeholder="Vəzifə" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlUserSex" class="form-control" runat="server">
                                                                <asp:ListItem Value="" Selected="True">Cinsi</asp:ListItem>
                                                                <asp:ListItem Value="K">Kişi</asp:ListItem>
                                                                <asp:ListItem Value="Q">Qadın</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg-12">
                                                            <label class="custom-file">
                                                                <asp:FileUpload ID="FUlpUserPic" runat="server" class="custom-file-input" />
                                                                <span class="custom-file-control"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- modal-body -->
                                            <div class="modal-footer">
                                                <img id="add_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnUserAdd" class="btn btn-info pd-x-20" runat="server" Text="Əlavə et" OnClick="btnUserAdd_Click"
                                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_loading').style.display = '';
                                                           document.getElementById('btnUserAddCancel').style.display = 'none';
                                                           document.getElementById('alert_msg').style.display = 'none';" />
                                                <button id="btnUserAddCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- modal-dialog -->
                                </div>
                                <!-- modal -->
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnUserAdd" />
                            </Triggers>
                        </asp:UpdatePanel> 



                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <asp:LinkButton ID="lnkUserDetailBack" runat="server" OnClick="lnkUserDetailBack_Click"><i class="icon ion-arrow-left-a"> GERİ</i></asp:LinkButton>
                        <p class="mg-b-20 mg-sm-b-20"></p>
                        <h6 class="card-body-title">İSTİFADƏÇİNİN HÜQUQLARI</h6>
                        <p class="mg-b-20 mg-sm-b-10"></p>
                        <div class="pd-10 bd" style="background-color: #d8dce3;">
                            <ul class="nav nav-pills flex-column flex-md-row" role="tablist">
                                <li class="nav-item">
                                    <asp:LinkButton ID="lnkUserInfo" runat="server" class="nav-link active" role="tab" OnClick="lnkUserInfo_Click">Ümumi Məlumat</asp:LinkButton>
                                </li>
                                <li class="nav-item">
                                    <asp:LinkButton ID="lnkUserZones" runat="server" class="nav-link" role="tab" OnClick="lnkUserZones_Click">Zonalar Hüququ</asp:LinkButton>
                                </li>
                                <li class="nav-item">
                                    <asp:LinkButton ID="lnkUserModules" runat="server" class="nav-link" role="tab" OnClick="lnkUserModules_Click">Modullar Hüququ</asp:LinkButton>
                                </li>
                            </ul>
                        </div>
                        <!-- pd-10 -->
                        <div class="card pd-20 pd-sm-15">
                            <asp:MultiView ID="MultiView2" runat="server" ActiveViewIndex="0">
                                <asp:View ID="viewUserInfo" runat="server">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <asp:Literal ID="ltrUserImage" runat="server"></asp:Literal>
                                                </div>
                                                <div class="col-lg-6">

                                                    <div class="alert alert-info pd-y-20" role="alert">
                                                        <div class="d-sm-flex align-items-center justify-content-start">
                                                            <div class="mg-t-20 mg-sm-t-0">
                                                                <h6 class="card-title tx-uppercase tx-12 mg-b-4">SON YENİLƏNMƏ TARİXİ : 
                                                                    <asp:Literal ID="ltrUserLastUpdateDate" runat="server"></asp:Literal>
                                                                </h6>

                                                                <h6 class="card-title tx-uppercase tx-12 mg-b-4">YENİLƏMƏNİ EDƏN İSTİFADƏÇİ : 
                                                                    <asp:Literal ID="ltrUserLastUpdateMaker" runat="server"></asp:Literal>
                                                                </h6>

                                                                <h6 class="card-title tx-uppercase tx-12 mg-b-4">SİSTEMƏ SON GİRİŞ TARİXİ : 
                                                                    <asp:Literal ID="ltrUserLastLogin" runat="server"></asp:Literal>
                                                                </h6>

                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>



                                            <p class="mg-b-20 mg-sm-b-10"></p>
                                            <div class="col-lg-12">
                                                <asp:TextBox ID="txtUserEditFullName" MaxLength="60" runat="server" class="form-control" placeholder="Soyad, ad, ata adı" type="text"></asp:TextBox>
                                            </div>
                                            <p class="mg-b-20 mg-sm-b-10"></p>
                                            <div class="col-lg-12">
                                                <asp:TextBox ID="txtUserEditContactNumber" MaxLength="50" runat="server" class="form-control" placeholder="Əlaqə nömrəsi" type="text"></asp:TextBox>
                                            </div>
                                            <p class="mg-b-20 mg-sm-b-10"></p>
                                            <div class="col-lg-12">
                                                <asp:DropDownList ID="drlUserEditSex" class="form-control" runat="server">
                                                    <asp:ListItem Value="" Selected="True">Cinsi</asp:ListItem>
                                                    <asp:ListItem Value="K">Kişi</asp:ListItem>
                                                    <asp:ListItem Value="Q">Qadın</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <p class="mg-b-20 mg-sm-b-10"></p>
                                            <div class="col-lg-12">
                                                <asp:TextBox ID="txtUserEditPosition" runat="server" MaxLength="50" class="form-control" placeholder="Vəzifə" type="text"></asp:TextBox>
                                            </div>
                                            <p class="mg-b-20 mg-sm-b-10"></p>
                                            <div class="col-lg-12">
                                                <asp:TextBox ID="txtUserEditLogin" ReadOnly runat="server" MaxLength="20" class="form-control" placeholder="İstifadəçi adı" type="text" Enabled="False"></asp:TextBox>
                                            </div>

                                            <p class="mg-b-20 mg-sm-b-20"></p>
                                            <div class="col-lg-12" id ="divEditPass" runat ="server">
                                                <label class="ckbox">
                                                    <asp:CheckBox ID="chkUserEditPass" runat="server" AutoPostBack="True" OnCheckedChanged="chkUserEditPass_CheckedChanged"></asp:CheckBox>
                                                    <span>Şifrəni dəyiş</span>
                                                </label>
                                            </div>
                                            <div class="col-lg-12">
                                                <asp:TextBox ID="txtUserEditPassword" runat="server" MaxLength="50" class="form-control" placeholder="Şifrə" TextMode="Password" Enabled="False"></asp:TextBox>
                                            </div>
                                            <p class="mg-b-20 mg-sm-b-10"></p>
                                            <div class="col-lg-12">
                                                <asp:TextBox ID="txtUserEditPasswordRepeat" runat="server" MaxLength="50" class="form-control" placeholder="Şifrə təkrar" TextMode="Password" Enabled="False"></asp:TextBox>
                                            </div>
                                            <p class="mg-b-20 mg-sm-b-20"></p>
                                            <div class="col-lg-12" id ="divEditPicture" runat ="server">
                                                <label class="ckbox">
                                                    <asp:CheckBox ID="chkUserEditPicture" runat="server" AutoPostBack="True" OnCheckedChanged="chkUserEditPicture_CheckedChanged"></asp:CheckBox>
                                                    <span>Şəkili dəyiş</span>
                                                </label>
                                            </div>

                                            <p class="mg-b-20 mg-sm-b-10"></p>
                                            <div class="col-lg-12" id ="divEditPicture1" runat ="server">
                                                <label class="custom-file" style="width: 100%">
                                                    <asp:FileUpload ID="FUlpUserEditPic" runat="server" class="custom-file-input" Enabled="False" />
                                                    <span class="custom-file-control"></span>
                                                </label>
                                            </div>
                                            <p class="mg-b-20 mg-sm-b-20"></p>
                                            <div class="col-lg-12" style="text-align: right">
                                                <img id="edituser_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnUserEditDetail" class="btn btn-info pd-x-20" runat="server" Text="Yenilə"
                                                    OnClientClick="this.style.display = 'none';
                                                           document.getElementById('edituser_loading').style.display = '';"
                                                    OnClick="btnUserEditDetail_Click" />
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnUserEditDetail" />
                                            <asp:PostBackTrigger ControlID="btnUserEditZones" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </asp:View>
                                <asp:View ID="viewUserZones" runat="server">

                                    <div class="table-responsive">
                                        <asp:GridView ID="GrdUserZones" class="table mg-b-0" runat="server" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
                                            <Columns>
                                                <asp:BoundField DataField="Code" HeaderText="ZONA KODU" />
                                                <asp:BoundField DataField="Description" HeaderText="ZONA ADI" />
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <label class="ckbox">
                                                            <input type="checkbox" id="chkUserZoneHeader1" runat="server" onchange="checkAllZone(this)" />
                                                            <span></span>
                                                        </label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <label class="ckbox">
                                                            <asp:CheckBox ID="chkUserZone" Checked='<%#Convert.ToInt32(Eval("UserID")) == 0 ? false : true %>' runat="server"></asp:CheckBox>
                                                            <span></span>
                                                        </label>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <p class="mg-b-20 mg-sm-b-20"></p>
                                    <div class="col-lg-12" style="text-align: right; padding-right: 0px !important">
                                        <img id="edituserzone_loading" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnUserEditZones" class="btn btn-info pd-x-20" runat="server" Text="Təsdiqlə"
                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('edituserzone_loading').style.display = '';"
                                            OnClick="btnUserEditZones_Click" />
                                    </div>
                                </asp:View>
                                <asp:View ID="viewUserModules" runat="server">

                                    <div class="table-responsive">
                                        <asp:GridView ID="GrdUsersModuleRole" class="table mg-b-0" runat="server" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
                                            <Columns>
                                                <asp:BoundField DataField="FullName" HeaderText="Modul adı" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnUsersModuleDetails"
                                                            CssClass="btn btn-outline-primary btn-block mg-b-10"
                                                            runat="server" Text="Funksiyalar"
                                                            CommandArgument='<%#Eval("ID")%>'
                                                            OnClick="btnUsersModuleDetails_Click" />
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Right" Width="150px" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>


                                    <!-- LARGE MODAL -->
                                    <div id="usersmenurole_modal" class="modal fade" data-backdrop="static">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content tx-size-sm">
                                                <div class="modal-header pd-x-20">
                                                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="fa fa-gear"></i>&nbsp&nbspİSTİFADƏÇİN FUNKSİYA HÜQUQLARI </h6>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body pd-20">
                                                    <asp:Literal ID="ltrUserRoleFunctionAlert" runat="server"></asp:Literal>
                                                    <div class="card pd-10 pd-sm-20">
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="GrdUsersFunctions" class="table mg-b-0" runat="server" DataKeyNames="Function_ID,FieldName,Menu_ID" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
                                                                <Columns>
                                                                    <asp:BoundField DataField="Description" HeaderText="FUNKSİYA ADI" HtmlEncode="False" />
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>
                                                                            <label class="ckbox">
                                                                                <input type="checkbox" id="chkUserRoleHeader" runat="server" onchange="checkAllFunctions(this)" />
                                                                                <span></span>
                                                                            </label>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <label class="ckbox">
                                                                                <asp:CheckBox ID="chkUserRoleFunctions" Checked='<%#Convert.ToInt32(Eval("isRole")) == 0 ? false : true %>' runat="server"></asp:CheckBox>
                                                                                <span></span>
                                                                            </label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Right" Width="50px" />
                                                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <EmptyDataTemplate>
                                                                  SEÇDİYİNİZ MODUL DAXİLİNDƏ FUNKSİYA TAPILMADI...
                                                                </EmptyDataTemplate>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- modal-body -->
                                                <div class="modal-footer">
                                                    <img id="menurole_loading" style="display: none" src="../img/loader.gif" />
                                                    <asp:Button ID="btnUserMenuRoleAccept" class="btn btn-info pd-x-20" runat="server" Text="Təsdiqlə" OnClick="btnUserMenuRoleAccept_Click"
                                                        OnClientClick="this.style.display = 'none';
                                                           document.getElementById('menurole_loading').style.display = '';
                                                           document.getElementById('btnUserMenuRoleCancel').style.display = 'none';
                                                           document.getElementById('alert_msg').style.display = 'none';" />
                                                    <button id="btnUserMenuRoleCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- modal-dialog -->
                                    </div>
                                    <!-- modal -->
                                </asp:View>
                            </asp:MultiView>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>

        <script type="text/javascript">
            function openAddUserModal() {
                $('#modaldemo3').modal({ show: true });
            }

            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }

            function openUsermenuRoleModal() {
                $('#usersmenurole_modal').modal({ show: true });
            }

            function checkAllZone(ele) {
                var checkboxes = document.getElementsByTagName('input');
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            if (checkboxes[i].id.indexOf('chkUserZone') != -1) {
                                checkboxes[i].checked = true;
                            }
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            if (checkboxes[i].id.indexOf('chkUserZone') != -1) {
                                checkboxes[i].checked = false;
                            }
                        }
                    }
                }
            }



            function checkAllFunctions(ele) {
                var checkboxes = document.getElementsByTagName('input');
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            if (checkboxes[i].id.indexOf('chkUserRoleFunctions') != -1) {
                                checkboxes[i].checked = true;
                            }
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type == 'checkbox') {
                            if (checkboxes[i].id.indexOf('chkUserRoleFunctions') != -1) {
                                checkboxes[i].checked = false;
                            }
                        }
                    }
                }
            }

        </script>
    </div>
</asp:Content>

