﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Users_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();

    void FillUsers()
    {
        grdUsers.DataSource = db.GetUserList();
        grdUsers.DataBind();
        grdUsers.UseAccessibleHeader = true;
       if(grdUsers.Rows.Count > 0) grdUsers.HeaderRow.TableSection = TableRowSection.TableHeader;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");

        if (!IsPostBack)
        {
            FillUsers();
        }

        string FUNCTION_ID = "USERS_VIEW_MAIN_PAGE";

        /*Begin Permission*/
        db.CheckFunctionPermission(Convert.ToInt32(Session["LogonUserID"]), FUNCTION_ID);

        btnAddNew.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "NEWUSER");
        if (grdUsers.Rows.Count > 0)
        {
            grdUsers.Columns[4].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "VIEWUSERROLE");
            grdUsers.Columns[5].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "DELETEUSER");           
        }
        btnUserEditDetail.Visible = divEditPass.Visible = txtUserEditPassword.Visible =
            txtUserEditPasswordRepeat.Visible = divEditPicture.Visible = FUlpUserEditPic.Visible =
            divEditPicture1.Visible =
            db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "EDITUSERINFO");
        btnUserEditZones.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "EDITUSERZONESROLE");
        btnUserMenuRoleAccept.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "ACCEPTUSERFUNCTIONS");

        
        /*End Permission*/
       
    }
    protected void btnUserAdd_Click(object sender, EventArgs e)
    {
        ltrMessage.Text = "";
        if (txtUserFullName.Text.Trim().Length < 5)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Ad , soyad , ata adını daxil edin!");
            return;
        }
        if (txtUserContactNumber.Text.Trim().Length != 0)
        {
            if (!Config.IsNumeric(txtUserContactNumber.Text.Trim()))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
                ltrMessage.Text = Alert.DangerMessage("Əlaqə nömrəsini rəqəm tipi olaraq daxil edin! (Nümunə: 0555555555)");
                return;
            }
        }

        if (txtUserLogin.Text.Trim().Length < 3 || !Config.isEnglish(txtUserLogin.Text.Trim()))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage("İstifadəçi adını 3 simvoldan az olmayaraq , ingilis böyük,kiçik hərflərindən , rəqəm simvollarından istifadə edərək daxil edin!");
            return;
        }

        if (txtUserPassword.Text.Trim().Length < 6 || !Config.isEnglish(txtUserPassword.Text.Trim()))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Şifrəni  6 simvoldan az olmayaraq , ingilis böyük,kiçik hərflərindən , rəqəm simvollarından istifadə edərək daxil edin!");
            return;
        }

        if (txtUserPassword.Text != txtUserPasswordRepeat.Text)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Daxil etdiyiniz şifrələr eyni deyil.");
            return;
        }

        if (drlUserSex.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage("İstifadəçinin cinsini seçin!");
            return;
        }

        string imageName = "";

        if (FUlpUserPic.HasFile)
        {
            string AllowTypesImage = "-jpg-jpeg-png-";
            string ImageType = System.IO.Path.GetExtension(FUlpUserPic.PostedFile.FileName).Trim('.').ToLower();
            if (AllowTypesImage.IndexOf("-" + ImageType + "-") < 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
                ltrMessage.Text = Alert.DangerMessage(".jpg,.jpeg,.png  tiplərindən birini seçin!");
                return;
            }
            //image
            if (FUlpUserPic.PostedFile.ContentLength / 1024 > 1024)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
                ltrMessage.Text = Alert.DangerMessage("Maksimum 1 mb şəkil yükləməyə icazə verilir.");
                return;
            }

            Guid g_id = Guid.NewGuid();
            string ID = g_id.ToString();
            imageName = ID.ToString().Trim() + System.IO.Path.GetFileName(FUlpUserPic.FileName.Trim());

            Bitmap img = new Bitmap(FUlpUserPic.PostedFile.InputStream, false);

            try
            {
                ImageResize.ImgResize("/UserImages/" + imageName, 256, 256, FUlpUserPic.PostedFile.InputStream, 90L);
            }
            catch (Exception ex)
            {

            }

        }

        if (imageName != "")
        {
            imageName = "/UserImages/" + imageName;
        }
        if (imageName == "")
        {
            if (drlUserSex.SelectedValue == "K") imageName = "/img/man.png";
            if (drlUserSex.SelectedValue == "Q") imageName = "/img/woman.png";
        }


        string result = db.AddUser(txtUserFullName.Text.Trim(), txtUserContactNumber.Text.Trim(), drlUserSex.SelectedValue, txtUserPosition.Text.Trim(),
            txtUserLogin.Text.Trim(), txtUserPassword.Text.Trim(), imageName, Convert.ToInt32(Session["LogonUserID"]));

        if (result != "OK")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddUserModal();", true);
            ltrMessage.Text = Alert.DangerMessage(result);
            return;
        }

        Config.Rd("/users");

    }

    protected void btnDeleteUser_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        int UserID = Convert.ToInt32(btn.CommandArgument);
        string result = db.DeleteUser(UserID, Convert.ToInt32(Session["LogonUserID"]));
        if (result != "OK")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage("Error: (" + result + ")");
            return;
        }

        FillUsers();

    }

    protected void lnkViewUserRole_Click(object sender, EventArgs e)
    {
        LinkButton lnkView = (LinkButton)sender;
        int viewUserID = Convert.ToInt32(lnkView.CommandArgument);
        Session["viewUserID"] = viewUserID;
        FillUsersDetails(viewUserID);
        MultiView1.ActiveViewIndex = 1;

        MultiView2.ActiveViewIndex = 0;

        lnkUserInfo.CssClass = "nav-link active";
        lnkUserZones.CssClass = "nav-link";
        lnkUserModules.CssClass = "nav-link";
    }

    protected void lnkUserInfo_Click(object sender, EventArgs e)
    {
        if (Session["viewUserID"] == null)
        {
            MultiView1.ActiveViewIndex = 0;
            return;
        }
        MultiView2.ActiveViewIndex = 0;

        lnkUserInfo.CssClass = "nav-link active";
        lnkUserZones.CssClass = "nav-link";
        lnkUserModules.CssClass = "nav-link";
    }
    protected void lnkUserZones_Click(object sender, EventArgs e)
    {
        if (Session["viewUserID"] == null)
        {
            MultiView1.ActiveViewIndex = 0;
            return;
        }
        MultiView2.ActiveViewIndex = 1;

        lnkUserInfo.CssClass = "nav-link";
        lnkUserZones.CssClass = "nav-link active";
        lnkUserModules.CssClass = "nav-link";
        int userID = Convert.ToInt32(Session["viewUserID"]);
        FillUserZone(userID);
    }
    protected void lnkUserModules_Click(object sender, EventArgs e)
    {
        if (Session["viewUserID"] == null)
        {
            MultiView1.ActiveViewIndex = 0;
            return;
        }
        MultiView2.ActiveViewIndex = 2;

        lnkUserInfo.CssClass = "nav-link";
        lnkUserZones.CssClass = "nav-link";
        lnkUserModules.CssClass = "nav-link active";
        int userID = Convert.ToInt32(Session["viewUserID"]);
        FillUserModules();
    }
    protected void lnkUserDetailBack_Click(object sender, EventArgs e)
    {
        Session["viewUserID"] = null;
        MultiView1.ActiveViewIndex = 0;
    }
    protected void chkUserEditPass_CheckedChanged(object sender, EventArgs e)
    {
        txtUserEditPassword.Enabled = txtUserEditPasswordRepeat.Enabled = chkUserEditPass.Checked;
    }

    void FillUsersDetails(int userId)
    {
        DataRow dr = db.GetUserByUserId(userId);
        if (dr != null)
        {
            txtUserEditPassword.Enabled = txtUserEditPasswordRepeat.Enabled = chkUserEditPass.Checked = false;
            txtUserEditFullName.Text = Convert.ToString(dr["Fullname"]);
            txtUserEditContactNumber.Text = Convert.ToString(dr["ContactNumber"]);
            drlUserEditSex.SelectedValue = Convert.ToString(dr["Sex"]);
            txtUserEditPosition.Text = Convert.ToString(dr["Position"]);
            txtUserEditLogin.Text = Convert.ToString(dr["Login"]);
            ltrUserImage.Text = "<img class =\"userimage\" src=\"" + Convert.ToString(dr["Picture"]).Trim() + "\" alt=\"\" />";
            ltrUserLastLogin.Text = Convert.ToString(dr["LastLoginDateFormat"]);
            ltrUserLastUpdateDate.Text = Convert.ToString(dr["MakerDateFormat"]);
            ltrUserLastUpdateMaker.Text = Convert.ToString(dr["MakerUser"]);
            chkUserEditPass.Checked = chkUserEditPicture.Checked = FUlpUserEditPic.Enabled = txtUserEditPassword.Enabled = txtUserEditPasswordRepeat.Enabled = false;
        }
    }



    void FillUserZone(int userId)
    {
        DataTable dt = db.GetUserZone(userId);
        if (dt != null)
        {
            GrdUserZones.DataSource = dt;
            GrdUserZones.DataBind();
            GrdUserZones.UseAccessibleHeader = true;
           if(GrdUserZones.Rows.Count > 0) GrdUserZones.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }

    void FillUserModules()
    {
        DataTable dt = db.GetUserModulesList();
        if (dt != null)
        {
            GrdUsersModuleRole.DataSource = dt;
            GrdUsersModuleRole.DataBind();
            GrdUsersModuleRole.UseAccessibleHeader = true;
           if(GrdUsersModuleRole.Rows.Count > 0) GrdUsersModuleRole.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }

    protected void btnUserEditDetail_Click(object sender, EventArgs e)
    {
        if(Session["viewUserID"] == null)
        {
            MultiView1.ActiveViewIndex = 0;
            return;
        }

        int UserID = Convert.ToInt32(Session["viewUserID"]);

        ltrAlertMsg.Text = "";
        if (txtUserEditFullName.Text.Trim().Length < 5)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage("Ad , soyad , ata adını daxil edin!");
            return;
        }

        if (txtUserEditContactNumber.Text.Trim().Length != 0)
        {
            if (!Config.IsNumeric(txtUserEditContactNumber.Text.Trim()))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                ltrAlertMsg.Text = Alert.DangerMessage("Əlaqə nömrəsini rəqəm tipi olaraq daxil edin! (Nümunə: 0555555555)");
                return;
            }
        }

        if (drlUserEditSex.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage("İstifadəçinin cinsini seçin!");
            return;
        }

        if (chkUserEditPass.Checked)
        {
            if (txtUserEditPassword.Text.Trim().Length < 6 || !Config.isEnglish(txtUserEditPassword.Text.Trim()))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                ltrAlertMsg.Text = Alert.DangerMessage("Şifrəni  6 simvoldan az olmayaraq , ingilis böyük,kiçik hərflərindən , rəqəm simvollarından istifadə edərək daxil edin!");
                return;
            }

            if (txtUserEditPassword.Text != txtUserEditPasswordRepeat.Text)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                ltrAlertMsg.Text = Alert.DangerMessage("Daxil etdiyiniz şifrələr eyni deyil.");
                return;
            }
        }


        string imageName = "";

        if (chkUserEditPicture.Checked)
        {
            if (!FUlpUserEditPic.HasFile)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                ltrAlertMsg.Text = Alert.DangerMessage("Şəkil seçin!");
                return;
            }
            if (FUlpUserEditPic.HasFile)
            {
                string AllowTypesImage = "-jpg-jpeg-png-";
                string ImageType = System.IO.Path.GetExtension(FUlpUserEditPic.PostedFile.FileName).Trim('.').ToLower();
                if (AllowTypesImage.IndexOf("-" + ImageType + "-") < 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                    ltrAlertMsg.Text = Alert.DangerMessage(".jpg,.jpeg,.png  tiplərindən birini seçin!");
                    return;
                }
            
                if (FUlpUserEditPic.PostedFile.ContentLength / 1024 > 1024)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                    ltrAlertMsg.Text = Alert.DangerMessage("Maksimum 1 mb şəkil yükləməyə icazə verilir.");
                    return;
                }

                Guid g_id = Guid.NewGuid();
                string ID = g_id.ToString();
                imageName = ID.ToString().Trim() + System.IO.Path.GetFileName(FUlpUserEditPic.FileName.Trim());

                Bitmap img = new Bitmap(FUlpUserEditPic.PostedFile.InputStream, false);

                try
                {
                    ImageResize.ImgResize("/UserImages/" + imageName, 256, 256, FUlpUserEditPic.PostedFile.InputStream, 90L);
                }
                catch (Exception ex)
                {
                    throw new ExecutionEngineException();
                }

            }
        }

        if (imageName != "")
        {
            imageName = "/UserImages/" + imageName;
        }
        string password = "";
        
        if (chkUserEditPass.Checked) password = txtUserEditPassword.Text.Trim();


        string result = db.UpdateUser(UserID, txtUserEditFullName.Text, txtUserEditContactNumber.Text,
                        drlUserEditSex.SelectedValue, txtUserEditPosition.Text, password, imageName, Convert.ToInt32(Session["LogonUserID"]));

         if (result != "OK")
         {
             ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
             ltrAlertMsg.Text = Alert.DangerMessage(result);
             return;
         }
         ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
         ltrAlertMsg.Text = Alert.SuccessMessage("Məlumat yeniləndi!");
         FillUsersDetails(UserID);


    }
    protected void chkUserEditPicture_CheckedChanged(object sender, EventArgs e)
    {
        FUlpUserEditPic.Enabled = chkUserEditPicture.Checked;
    }
    
    protected void btnUserEditZones_Click(object sender, EventArgs e)
    {
        if (Session["viewUserID"] == null)
        {
            MultiView1.ActiveViewIndex = 0;
            return;
        }

        int UserID = Convert.ToInt32(Session["viewUserID"]);

        DataTable dt = new DataTable("ZoneParams");
        dt.Columns.Add("pZoneCode",typeof(string));
        dt.Columns.Add("pUserId", typeof(int));
        dt.Columns.Add("pCMakerId", typeof(int));
        for (int i = 0; i < GrdUserZones.Rows.Count; i++)
        {
            CheckBox chk = (CheckBox)GrdUserZones.Rows[i].Cells[2].FindControl("chkUserZone");
            if (chk.Checked)
            {
                string zoneCode = Convert.ToString(GrdUserZones.Rows[i].Cells[0].Text);
                dt.Rows.Add(zoneCode, UserID, Convert.ToInt32(Session["LogonUserID"]));
            }
        }

        string result = db.AddUserZone(dt, UserID);
        if (result == "OK") ltrAlertMsg.Text = Alert.SuccessMessage("Məlumat təsdiqləndi");
        else ltrAlertMsg.Text = Alert.DangerMessage("Xəta: " + result);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        FillUserZone(UserID);
       
    }
    protected void btnUsersModuleDetails_Click(object sender, EventArgs e)
    {
        if (Session["viewUserID"] == null)
        {
            MultiView1.ActiveViewIndex = 0;
            return;
        }

        Button btnThis = (Button)sender;
        int menuID = Convert.ToInt32(btnThis.CommandArgument);
        int selectedUserID = Convert.ToInt32(Session["viewUserID"]);
        
        
        FillUserRoleFunctions(selectedUserID, menuID);

        //Dashboard
        if (menuID == 1)
        {
           btnUserMenuRoleAccept.Enabled = false;
        }
        else
        {
            btnUserMenuRoleAccept.Enabled = GrdUsersFunctions.Rows.Count > 0 ? true : false;
        }

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openUsermenuRoleModal();", true);
       
    }

    void FillUserRoleFunctions(int userID,int menuID)
    {
        DataTable dt = db.GetUserModuleFunctionsList(userID, menuID);
        if (dt != null)
        {
            GrdUsersFunctions.DataSource = dt;
            GrdUsersFunctions.DataBind();
            GrdUsersFunctions.UseAccessibleHeader = true;
            if(dt.Rows.Count > 0) GrdUsersFunctions.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }

    protected void btnUserMenuRoleAccept_Click(object sender, EventArgs e)
    {
        if (Session["viewUserID"] == null)
        {
            MultiView1.ActiveViewIndex = 0;
            return;
        }

        DataTable dt = new DataTable("UserFunctionParams");
        dt.Columns.Add("pFunctionId", typeof(string));

        int UserID = Convert.ToInt32(Session["viewUserID"]);

        string Function_ID = "", FieldName = ""; 
        int Menu_ID = 0 ;
        bool  chkFunction , selectedIdEmpty = false;
        string main_page_function_id = "";

        for (int i = 0; i < GrdUsersFunctions.Rows.Count; i++)
        {
             Function_ID = (string)this.GrdUsersFunctions.DataKeys[i]["Function_ID"];
             FieldName = (string)this.GrdUsersFunctions.DataKeys[i]["FieldName"];
             Menu_ID = (int)this.GrdUsersFunctions.DataKeys[i]["Menu_ID"];

             if (FieldName == "_MAIN_PAGE") main_page_function_id = Function_ID;

             chkFunction = ((CheckBox)GrdUsersFunctions.Rows[i].Cells[1].FindControl("chkUserRoleFunctions")).Checked;

            if (chkFunction)
            {
                selectedIdEmpty = true;
                if (FieldName != "_MAIN_PAGE") dt.Rows.Add(Function_ID);     
            }
        }
        
        if (selectedIdEmpty)
        {
            if (main_page_function_id != "") dt.Rows.Add(main_page_function_id);
        }

        string result = db.AddUserFunctions(dt, UserID, Menu_ID, Convert.ToInt32(Session["LogonUserID"]));
       if (result == "OK") ltrAlertMsg.Text = Alert.SuccessMessage("Məlumat təsdiqləndi.");
       else ltrAlertMsg.Text = Alert.DangerMessage("Xəta: " + result);

       ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
       FillUserRoleFunctions(UserID, Menu_ID);

       
    }
}