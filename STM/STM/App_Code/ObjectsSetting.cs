﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;


public partial class DbProcess : DALC
{
    public string AddObject(string pZoneCode, string pLineCode, string pCorpusCode, string pObjectNumber, 
                             string pObjectFullNumber, string pObjectType, decimal pObjectArea, 
                             decimal pObjectAreaContract,string pObjectPosition, string pObjectStatus,
                             string pOwnerType,  string pObjectNote, string pStatus, int pCMakerId,string pObjectContractNumber,
                             int pPaymentType,int pIsVat)
    {
        SqlCommand cmd = new SqlCommand("AddObject", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pZoneCode", pZoneCode);
        cmd.Parameters.AddWithValue("@pLineCode", pLineCode);
        cmd.Parameters.AddWithValue("@pCorpusCode", pCorpusCode);
        cmd.Parameters.AddWithValue("@pObjectNumber", pObjectNumber);
        cmd.Parameters.AddWithValue("@pObjectFullNumber", pObjectFullNumber);
        cmd.Parameters.AddWithValue("@pObjectType", pObjectType);
        cmd.Parameters.AddWithValue("@pObjectArea", pObjectArea);
        cmd.Parameters.AddWithValue("@pObjectAreaContract", pObjectAreaContract);
        cmd.Parameters.AddWithValue("@pObjectPosition", pObjectPosition);
        cmd.Parameters.AddWithValue("@pObjectStatus", pObjectStatus);
        cmd.Parameters.AddWithValue("@pOwnerType", pOwnerType);
        cmd.Parameters.AddWithValue("@pObjectNote", pObjectNote);
        cmd.Parameters.AddWithValue("@pStatus", pStatus);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);
        cmd.Parameters.AddWithValue("@pObjectContractNumber", pObjectContractNumber);
        cmd.Parameters.AddWithValue("@pPaymentType", pPaymentType);
        cmd.Parameters.AddWithValue("@pIsVat", pIsVat);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

 

    public DataTable GetObjectList(ObjectsSearchParams Ops, bool isTop, int UserID, bool ExportExcel, out int dataCount)
    {
        dataCount = 0;
        string sql_query = String.Format(@"select {0} * 
        from Objects O where O.Status = 'ACTIVE' and
        O.ZoneCode in (select ZoneCode from UsersZone where UserID = @UserID)", isTop ? "top 100" : "");

        if (ExportExcel)
        {
            sql_query = String.Format(@"select {0}  
            (select max(Z.Description) from Zone Z where Z.Code = O.ZoneCode) as [Zona],
            (select max(L.Description) from Line L where L.Code = O.LineCode and L.ZoneCode = O.ZoneCode) as [Sıra],
            (select max(C.Description) from Corpus C where C.Code = O.CorpusCode and C.LineCode = O.LineCode and C.ZoneCode = O.ZoneCode) as [Korpus],
            O.ObjectNumber as [Obyekt nömrəsi],
            O.ObjectFullNumber as [Obyekt nömrəsi tam],
            replace(replace(replace(replace(O.ObjectType,'MAGAZA',N'Mağaza'),'SKLAT',N'Anbar'),'TUALET',N'Tualet'),'METBEX',N'Mətbəx') as [Obyekt tipi],
            O.ObjectArea as [Obyekt sahəsi],
            O.ObjectAreaContract as [Müqavilədəki sahəsi],
            replace(replace(replace(O.ObjectPosition,'KUNC',N'Künc'),'NORMAL',N'Normal'),'KECHID',N'Keçid') as [Obyektin vəziyyəti],
            replace(replace(replace(replace(O.ObjectStatus,'ACTIV',N'Aktiv'),'BOSH',N'Boş'),'TEMIR',N'Təmir'),'BAGLI',N'Bağlı') as [Obyektin statusu],
            replace(replace(O.OwnerType,'BAZAR',N'Bazar'),'SHEXSI',N'Şəxsi') as [Mülkiyyətin tipi],
            (select isnull(max(Op.ObjectPrice),0) from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'PLACE') as [Yer pulu],
            (select isnull(max(Op.ObjectDiscount),0) from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'PLACE') as [Endirim məbləği],
            (select isnull(max(U.Fullname),'') from Users U where U.ID =
            (select isnull(max(Op.ObjectDiscountMakerID),0) from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'PLACE')) as [Endirim edən],
            (select isnull(max(Op.ObjectPrice),0) from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'OWNER') as [Sahibkar pulu],
            (select isnull(max(Op.ObjectPrice),0) from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'TAX') as [Vergi pulu],
            (select isnull(max(Op.ObjectPrice),0) from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'INTERNET') as [İnternet pulu]
            from Objects O where O.Status = 'ACTIVE' and
            O.ZoneCode in (select ZoneCode from UsersZone where UserID = @UserID)", isTop ? "top 100" : "");
        }

        if (Ops.zoneCode.Length > 0) sql_query += " and O.ZoneCode = @ZoneCode";
        if (Ops.lineCode.Length > 0) sql_query += " and O.LineCode = @LineCode";
        if (Ops.corpusCode.Length > 0) sql_query += " and O.CorpusCode = @CorpusCode";
        if (Ops.objectNumber != "") sql_query += " and O.ObjectNumber = @ObjectNumber";
        if (Ops.objectType != "0") sql_query += " and O.ObjectType = @ObjectType";
        if (Ops.objectArea > 0) sql_query += " and O.ObjectArea = @ObjectArea";
        if (Ops.objectAreaContract > 0) sql_query += " and O.ObjectAreaContract = @ObjectAreaContract";
        if (Ops.objectPosition != "0") sql_query += " and O.ObjectPosition = @ObjectPosition";
        if (Ops.ownerType != "0") sql_query += " and O.OwnerType = @OwnerType";
        if (Ops.objectStatus != "0") sql_query += " and O.ObjectStatus = @ObjectStatus";

        sql_query += " order by O.ZoneCode,O.LineCode,O.CorpusCode";



        DataTable dt = new DataTable("GetObjectList");
        SqlDataAdapter da = new SqlDataAdapter(sql_query, SqlConn);
     
        da.SelectCommand.Parameters.AddWithValue("@UserID", UserID);

        if (Ops.zoneCode.Length > 0) da.SelectCommand.Parameters.AddWithValue("@ZoneCode", Ops.zoneCode);
        if (Ops.lineCode.Length > 0) da.SelectCommand.Parameters.AddWithValue("@LineCode", Ops.lineCode);
        if (Ops.corpusCode.Length > 0) da.SelectCommand.Parameters.AddWithValue("@CorpusCode", Ops.corpusCode);
        if (Ops.objectNumber != "") da.SelectCommand.Parameters.AddWithValue("@ObjectNumber", Ops.objectNumber);
        if (Ops.objectType != "0") da.SelectCommand.Parameters.AddWithValue("@ObjectType", Ops.objectType);
        if (Ops.objectArea > 0) da.SelectCommand.Parameters.AddWithValue("@ObjectArea", Ops.objectArea);
        if (Ops.objectAreaContract > 0) da.SelectCommand.Parameters.AddWithValue("@ObjectAreaContract", Ops.objectAreaContract);
        if (Ops.objectPosition != "0") da.SelectCommand.Parameters.AddWithValue("@ObjectPosition", Ops.objectPosition);
        if (Ops.ownerType != "0") da.SelectCommand.Parameters.AddWithValue("@OwnerType", Ops.ownerType);
        if (Ops.objectStatus != "0") da.SelectCommand.Parameters.AddWithValue("@ObjectStatus", Ops.objectStatus);

       

       
        try
        {
            da.Fill(dt);
            dataCount = dt.Rows.Count;
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetObjectPricesList(int ObjectId)
    {
        DataTable dt = new DataTable("GetObjectPricesList");
        SqlDataAdapter da = new SqlDataAdapter(@"select P.*, 
            isnull(O.ObjectPrice,0) ObjectPrice,
            isnull(O.ObjectDiscount,0) ObjectDiscount,
            isnull(O.ObjectDiscountMakerID,0) ObjectDiscountMakerID from 
            PaymentsType P left join  ObjectPrices O on P.CODE = O.ObjectPriceType and O.ObjectID = @ObjectID
            where P.CODE in ('PLACE','OWNER','TAX','INTERNET','TEND', 'TRADE_UNIONS', 'POLKA')", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ObjectID", ObjectId);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetObjectDiscountMakerList()
    {
        DataTable dt = new DataTable("GetObjectDiscountMakerList");
        SqlDataAdapter da = new SqlDataAdapter(@"select * from HR order by ID", SqlConn);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public DataTable GetObjectListForBulkPrice(string pZoneCode, string pLineCode, string pCorpusCode, 
                                               string pObjectPosition, decimal pPrice , decimal pDiscountPrice,
                                               int pDiscountMakerID, int pCMakerId,out int pResult)
    {
        DataTable dt = new DataTable("GetObjectListForBulkPrice");
        SqlCommand cmd = new SqlCommand("GetObjectListForBulkPrice", SqlConn);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pZoneCode", pZoneCode);
        cmd.Parameters.AddWithValue("@pLineCode", pLineCode);
        cmd.Parameters.AddWithValue("@pCorpusCode", pCorpusCode);
        cmd.Parameters.AddWithValue("@pObjectPosition", pObjectPosition);
        cmd.Parameters.AddWithValue("@pObjectPrice", pPrice);
        cmd.Parameters.AddWithValue("@pObjectDiscount", pDiscountPrice);
        cmd.Parameters.AddWithValue("@pObjectDiscountMakerID", pDiscountMakerID);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
       
        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToInt32(cmd.Parameters["@pResult"].Value);
            da.Fill(dt);
        }
        catch
        {
            pResult = -1;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return dt;
    }



    public string ChangeObjectPrices(DataTable dtParams)
    {
        SqlConnection sql_conn = SqlConn;
        SqlTransaction transaction;

        SqlCommand cmd;
        SqlParameter paramResult;
        sql_conn.Open();
        transaction = sql_conn.BeginTransaction();
        string pResult = "";
        try
        {
            for (int i = 0; i < dtParams.Rows.Count; i++)
            {
                cmd = new SqlCommand("AddObjectPrices", sql_conn, transaction);
                cmd.CommandType = CommandType.StoredProcedure;
                
                cmd.Parameters.AddWithValue("@pObjectID", Convert.ToInt32(dtParams.Rows[i]["ObjectID"]));
                cmd.Parameters.AddWithValue("@pObjectPrice", Config.ToDecimal(dtParams.Rows[i]["ObjectPrice"].ToString()));
                cmd.Parameters.AddWithValue("@pObjectDiscount", Config.ToDecimal(dtParams.Rows[i]["ObjectDiscount"].ToString()));
                cmd.Parameters.AddWithValue("@pObjectDiscountMakerID", Convert.ToInt32(dtParams.Rows[i]["ObjectDiscountMakerID"]));
                cmd.Parameters.AddWithValue("@pObjectPriceType", Convert.ToString(dtParams.Rows[i]["ObjectPriceType"]));
                cmd.Parameters.AddWithValue("@pCMakerId", Convert.ToInt32(dtParams.Rows[i]["CMakerId"]));
                cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

                paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                paramResult.Direction = ParameterDirection.Output;
                paramResult.Size = 100;
                cmd.Parameters.Add(paramResult);

                try
                {
                    cmd.ExecuteNonQuery();
                    pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);

                    if (pResult != "OK")
                    {
                        transaction.Rollback();
                        sql_conn.Close();
                        return pResult;
                    }
                }
                catch (Exception ex)
                {
                    pResult = ex.Message;
                    transaction.Rollback();
                    sql_conn.Close();
                    return pResult;
                }
            }
            transaction.Commit();

        }
        catch (Exception sqlError)
        {
            pResult = sqlError.Message;
            transaction.Rollback();
            sql_conn.Close();
            return pResult;
        }
        sql_conn.Close();
        return pResult;
    }


    public DataRow GetObjectByID(int ObjectId)
    {
        DataTable dt = new DataTable("GetObjectByID");
        SqlDataAdapter da = new SqlDataAdapter(@"select 
        (select isnull(Z.Description,'') from Zone Z where Z.Code = O.ZoneCode) ZoneDesc,
        (select isnull(L.Description,'') from Line L where L.Code = O.LineCode and L.ZoneCode = O.ZoneCode) LineDesc,
        (select isnull(C.Description,'') from Corpus C where C.Code = O.CorpusCode and C.ZoneCode = O.ZoneCode and C.LineCode = O.LineCode) CorpusDesc,
        (select isnull(U.FullName,'') from Users U where U.ID = O.CMakerId) CMakerName,
        CONVERT(varchar, O.CMakerDate, 120) CMakerDateFormat,
        O.*
        from Objects O 
        where O.Status = 'ACTIVE' and O.ID = @ObjectID", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ObjectID", ObjectId);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt.Rows[0];
    }



    public string UpdateObject(int pObjectID, string pZoneCode, string pLineCode, string pCorpusCode, string pObjectNumber,
                            string pObjectFullNumber, string pObjectType, decimal pObjectArea,
                            decimal pObjectAreaContract, string pObjectPosition, string pObjectStatus,
                             string pOwnerType, string pObjectContractNumber, int pPaymentType, int pIsVAT, 
                             string pObjectNote, string pStatus, int pCMakerId)

    {
        SqlCommand cmd = new SqlCommand("UpdateObject", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pObjectID", pObjectID);
        cmd.Parameters.AddWithValue("@pZoneCode", pZoneCode);
        cmd.Parameters.AddWithValue("@pLineCode", pLineCode);
        cmd.Parameters.AddWithValue("@pCorpusCode", pCorpusCode);
        cmd.Parameters.AddWithValue("@pObjectNumber", pObjectNumber);
        cmd.Parameters.AddWithValue("@pObjectFullNumber", pObjectFullNumber);
        cmd.Parameters.AddWithValue("@pObjectType", pObjectType);
        cmd.Parameters.AddWithValue("@pObjectArea", pObjectArea);
        cmd.Parameters.AddWithValue("@pObjectAreaContract", pObjectAreaContract);
        cmd.Parameters.AddWithValue("@pObjectPosition", pObjectPosition);
        cmd.Parameters.AddWithValue("@pObjectStatus", pObjectStatus);
        cmd.Parameters.AddWithValue("@pOwnerType", pOwnerType);
        cmd.Parameters.AddWithValue("@pObjectContractNumber", pObjectContractNumber);
        cmd.Parameters.AddWithValue("@pPaymentType", pPaymentType);
        cmd.Parameters.AddWithValue("@pIsVat", pIsVAT);
        cmd.Parameters.AddWithValue("@pObjectNote",pObjectNote);
        cmd.Parameters.AddWithValue("@pStatus", pStatus);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }



    public string DeleteObject(int pObjectID  , int pCMakerID)
    {
        SqlCommand cmd = new SqlCommand("DeleteObject", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pObjectID", pObjectID);
        cmd.Parameters.AddWithValue("@pCMakerID", pCMakerID);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);
        
        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public DataTable GetCounterForSetNumber(int ObjectId)
    {
        DataTable dt = new DataTable("GetCounterForSetNumber");
        SqlDataAdapter da =
            new SqlDataAdapter(@"select (select replace(Description,N' Pulu', '') from PaymentsType where Code = C.CounterType) CounterTypeDesc, 
                                C.CounterType,
                                isnull(Ct.CounterNumber,'') CounterNumber, 
                                C.Unite, C.UnitePrice from 
                                CountersUnite C left join Counters Ct on 
                                C.CounterType = Ct.CounterType and Ct.ObjectID = @ObjectID and Ct.Status = 'ACTIVE'", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ObjectID", ObjectId);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }



    public string ChangeObjectCounters(DataTable dtParams)
    {
        SqlConnection sql_conn = SqlConn;
        SqlTransaction transaction;

        SqlCommand cmd;
        SqlParameter paramResult;
        sql_conn.Open();
        transaction = sql_conn.BeginTransaction();
        string pResult = "";
        try
        {
            for (int i = 0; i < dtParams.Rows.Count; i++)
            {
                cmd = new SqlCommand("AddObjectCounters", sql_conn, transaction);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@pObjectID", Convert.ToInt32(dtParams.Rows[i]["ObjectID"]));
                cmd.Parameters.AddWithValue("@pCounterNumber", Convert.ToString(dtParams.Rows[i]["CounterNumber"]));
                cmd.Parameters.AddWithValue("@pCounterType", Convert.ToString(dtParams.Rows[i]["CounterType"]));
                cmd.Parameters.AddWithValue("@pStatus", "ACTIVE");
                cmd.Parameters.AddWithValue("@pCMakerId", Convert.ToInt32(dtParams.Rows[i]["CMakerId"]));
                cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

                paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                paramResult.Direction = ParameterDirection.Output;
                paramResult.Size = 100;
                cmd.Parameters.Add(paramResult);

                try
                {
                    cmd.ExecuteNonQuery();
                    pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);

                    if (pResult != "OK")
                    {
                        transaction.Rollback();
                        sql_conn.Close();
                        return pResult;
                    }
                }
                catch (Exception ex)
                {
                    pResult = ex.Message;
                    transaction.Rollback();
                    sql_conn.Close();
                    return pResult;
                }
            }
            transaction.Commit();

        }
        catch (Exception sqlError)
        {
            pResult = sqlError.Message;
            transaction.Rollback();
            sql_conn.Close();
            return pResult;
        }
        sql_conn.Close();
        return pResult;
    }



    public DataTable GetObjectHistoryByID(int ObjectId)
    {
        DataTable dt = new DataTable("GetObjectByID");
        SqlDataAdapter da = new SqlDataAdapter(@"select 
        (select isnull(Z.Description,'') from Zone Z where Z.Code = O.ZoneCode) ZoneDesc,
        (select isnull(L.Description,'') from Line L where L.Code = O.LineCode and L.ZoneCode = O.ZoneCode) LineDesc,
        (select isnull(C.Description,'') from Corpus C where C.Code = O.CorpusCode and C.ZoneCode = O.ZoneCode and C.LineCode = O.LineCode) CorpusDesc,
        (select isnull(U.FullName,'') from Users U where U.ID = O.CMakerId) CMakerName,
        CONVERT(varchar, O.CMakerDate, 120) CMakerDateFormat,
        O.*
        from  ObjectsHistory O  
        where O.ObjectID = @ObjectID order by O.CMakerDate desc", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ObjectID", ObjectId);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetObjectPricesVewByID(int ObjectId)
    {
        DataTable dt = new DataTable("GetObjectPricesVewByID");
        SqlDataAdapter da = new SqlDataAdapter(@"select Op.* , Pt.Description as PriceTypeDesc ,
        (select U.FullName from Users U where U.ID =Op.ObjectDiscountMakerID) DiscountMakerName,
        (select U.FullName from Users U where U.ID =Op.CMakerId) MakerName,
         CONVERT(varchar, Op.CMakerDate, 120) CMakerDateFormat
         from ObjectPrices Op inner join PaymentsType Pt on Op.ObjectPriceType = Pt.CODE   where ObjectID = @ObjectID order by Op.ObjectPriceType desc", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ObjectID", ObjectId);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetObjectPricesHistoryVewByID(int ObjectId)
    {
        DataTable dt = new DataTable("GetObjectPricesHistoryVewByID");
        SqlDataAdapter da = new SqlDataAdapter(@"select Op.* , Pt.Description as PriceTypeDesc ,
        (select U.FullName from Users U where U.ID =Op.ObjectDiscountMakerID) DiscountMakerName,
        (select U.FullName from Users U where U.ID =Op.CMakerId) MakerName,
         CONVERT(varchar, Op.CMakerDate, 120) CMakerDateFormat
         from ObjectPricesHistory Op inner join PaymentsType Pt on Op.ObjectPriceType = Pt.CODE   where Op.ObjectID = @ObjectID
         order by Op.CMakerDate  desc", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ObjectID", ObjectId);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetCountersByObjectID(int ObjectId)
    {
        DataTable dt = new DataTable("GetCountersByObjectID");
        SqlDataAdapter da = new SqlDataAdapter(@"select C.*,
        (select U.Fullname from Users U where U.ID = C.CMakerID) MakerName,
        replace(P.Description,' Pulu','') as CounterTypeDesc,
        C.CounterType,
        CONVERT(varchar, C.CMakerDate, 120) CMakerDateFormat
        from Counters C inner join PaymentsType P on P.CODE = C.CounterType
        where C.Status = 'ACTIVE' and C.ObjectID = @ObjectID order by P.Description , C.CMakerDate desc", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ObjectID", ObjectId);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetCountersHistoryByObjectID(int ObjectId)
    {
        DataTable dt = new DataTable("GetCountersHistoryByObjectID");
        SqlDataAdapter da = new SqlDataAdapter(@"select C.*,
        (select U.Fullname from Users U where U.ID = C.CMakerID) MakerName,
        replace(P.Description,' Pulu','') as CounterTypeDesc,
        C.CounterType,
        CONVERT(varchar, C.CMakerDate, 120) CMakerDateFormat
        from CountersHistory C inner join PaymentsType P on P.CODE = C.CounterType
        where  C.ObjectID = @ObjectID order by P.Description , C.CMakerDate desc", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ObjectID", ObjectId);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public DataTable GetOwnersObjectID(int ObjectId)
    {
        DataTable dt = new DataTable("GetOwnersObjectID");
        SqlDataAdapter da = new SqlDataAdapter(@"select O.*,
        (select U.FullName from Users U where U.ID = O.CMakerId) MakerName,
        CONVERT(varchar, O.CMakerDate, 120) CMakerDateFormat
        from Owners O 
        where O.ObjectID = @ObjectID and O.Status = 'ACTIVE' order by  O.CMakerDate desc", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ObjectID", ObjectId);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public DataTable GetOwnersHistoryObjectID(int ObjectId)
    {
        DataTable dt = new DataTable("GetOwnersHistoryObjectID");
        SqlDataAdapter da = new SqlDataAdapter(@"select t.* from (select  O.OwnerID, O.Fullname, 
        max(O.RegDate) RegDate,
        max(U.FullName) MakerName,
        max(CONVERT(varchar, O.CMakerDate, 120)) CMakerDateFormat
        from OwnersHistory O left join Users U on O.CMakerId = U.ID
        where O.ObjectID = @ObjectID
        group by O.OwnerID, O.Fullname) t order by t.CMakerDateFormat desc", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ObjectID", ObjectId);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


}