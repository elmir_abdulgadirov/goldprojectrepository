﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public partial class DbProcess : DALC
{
    public DataTable GetDebtsByParams(int userID, string pObjectCode, string pRegNumber, string pTaxID, string pDebtDate,
                                      string pZoneCode, string pLineCode, string pCorpusCode, out string pTenantData)
    {
        pTenantData = "";
        DataTable dt = new DataTable("GetDebtsByParams");
        string sql_string = @"SELECT N'ABONENT: '+ T.Fullname + ' ID: ' + T.RegNumber + ',  VÖEN: ' + ISNULL(T.TaxID, '') AS TenantData,
           Ob.ID Debt_ID, O.ObjectFullNumber, T.RegNumber, t.TaxID,  Ob.Debt_Owner, Ob.ObjectStatus,
		   case when ob.PaymentType = 0 then N'Nəğd' Else N'Köçürmə' END PaymentMethod,
           Ob.Debt_Type, 
           FORMAT( convert(date,Ob.Value_date,104),'MMMM') Value_date_desc,
           (select Pt.Description from PaymentsType Pt where Pt.CODE = Ob.Debt_Type) Debt_TypeDesc,
	       Ob.ObjectID , Ob.Value_date,
	       Ob.Debt_Amount, Ob.Paid_Amount, Ob.Reducton_Amount, ob.Discount_Amount,
           (isnull(Ob.Debt_Amount,0) - isnull(Ob.Paid_Amount,0) - isnull(Ob.Reducton_Amount,0) - isnull(Ob.Discount_Amount,0)) Remain_Amount, Ob.Vat_Amount,
		   (isnull(Ob.Vat_Amount,0) - isnull(Ob.Paid_VAT_Amount,0) - isnull(Ob.Reducton_VatAmount,0) - isnull(Ob.Discount_VatAmount,0) ) Remain_VatAmount from 
           Object_Debts Ob inner join Objects O on Ob.ObjectID = O.ID
           inner join TenantRegs tr ON tr.ObjectID = o.ID
		   inner join Tenants t ON t.RegNumber = tr.RegNumber
        --------------------------------------------------------------------------------------
            where 
           ((Ob.Debt_Amount - Ob.Paid_Amount) > 0 or (Ob.Vat_Amount - Ob.Paid_VAT_Amount) > 0  or cast(Ob.CMakerDate as Date) >= cast(getdate() - 360 as Date)) and

            Ob.Debt_Type in (select replace(Function_ID,'DEBT_PAYMENT_','') DebtType from UserFunctions 
            where user_id = @UserID and Menu_ID = 10 and charindex('DEBT_PAYMENT_',Function_ID) > 0) and
            Ob.PaymentType in(SELECT replace(Function_ID,'PAYMENT_TYPE_','') PaymentType FROM UserFunctions 
			where user_id = @UserID and Menu_ID = 10 and charindex('PAYMENT_TYPE_',Function_ID) > 0) and
            O.ZoneCode in  (select ZoneCode from UsersZone where UserID = @UserID) and 
            O.Status = 'ACTIVE' and
            Ob.Status = 'ACTIVE'";


        if (pObjectCode.Trim().Length > 0)
        {
         sql_string += @" and ( O.ID in 
		    (select ObjectID from TenantRegs where RegNumber in (
             select RegNumber from TenantRegs where ObjectID in 
            (select ID from Objects where ObjectFullNumber = @ObjectFullNumber))) or O.ObjectFullNumber = @ObjectFullNumber )";
        }
        if (pRegNumber.Length > 0)
        {
            sql_string += " and tr.RegNumber = @RegNumber";
        }
        if (pTaxID.Length > 0)
        {
            sql_string += " and t.Taxid = @TaxID";
        }
        if (pDebtDate.Length > 0)
        {
            sql_string += " and Ob.Value_date = Convert(date, @DebtDate,104)";
        }
        if (pZoneCode.Length > 0)
        {
            sql_string += " and O.ZoneCode  = @ZoneCode";
        }
        if (pLineCode.Length > 0)
        {
            sql_string += " and O.LineCode  = @LineCode";
        }
        if (pCorpusCode.Length > 0)
        {
            sql_string += " and O.CorpusCode  = @CorpusCode";
        }
        if (pObjectCode.Trim().Length > 0 || pTaxID.Trim().Length > 0 || pRegNumber.Trim().Length > 0)
        {
            sql_string += @"  order by Ob.Value_date desc";
        }


        SqlDataAdapter da = new SqlDataAdapter(sql_string, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", userID);

        if (pObjectCode.Trim().Length > 0)
        {
            da.SelectCommand.Parameters.AddWithValue("@ObjectFullNumber", pObjectCode);
        }
        if (pRegNumber.Trim().Length > 0)
        {
            da.SelectCommand.Parameters.AddWithValue("@RegNumber", pRegNumber);
        }
        if (pTaxID.Trim().Length > 0)
        {
            da.SelectCommand.Parameters.AddWithValue("@TaxID", pTaxID);
        }
        if (pDebtDate.Trim().Length > 0)
        {
            da.SelectCommand.Parameters.AddWithValue("@DebtDate", pDebtDate);
        }
        if (pZoneCode.Length > 0)
        {
            da.SelectCommand.Parameters.AddWithValue("@ZoneCode", pZoneCode);
        }

        if (pLineCode.Length > 0)
        {
            da.SelectCommand.Parameters.AddWithValue("@LineCode", pLineCode);
        }
        if (pCorpusCode.Length > 0)
        {
            da.SelectCommand.Parameters.AddWithValue("@CorpusCode", pCorpusCode);
        }
       
        try
        {
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                pTenantData = dt.Rows[0][0].ToString();
            }
        }
        catch (Exception ex)
        {
            dt = null;
        }
        return dt;
    }


    public DataTable GetRemainDebtByDebtID(int debt_id)
    {
        DataTable dt = new DataTable("DebtBalance");
        SqlDataAdapter da = new SqlDataAdapter(@"select (isnull(Debt_Amount,0) - isnull(Paid_Amount,0) - isnull(Reducton_Amount,0) - isnull(Discount_Amount,0)) as RemainAmount,
	                                      (isnull(Vat_Amount,0) - isnull(Paid_VAT_Amount,0) - isnull(Reducton_VatAmount,0) - isnull(Discount_VatAmount,0)) as RemainVATAmount
                                          from Object_Debts where ID = @debt_id", SqlConn);
        da.SelectCommand.Parameters.Add("@debt_id", debt_id);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }
    public string CreatePayment(DateTime pPaymentDate, int pDebtID, decimal pPaid_Amount, decimal pPaid_VAT_Amount, int pCmakerID)
    {
        SqlCommand cmd = new SqlCommand("CreatePayment", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pDebtID", pDebtID);
        cmd.Parameters.AddWithValue("@pPaid_Amount", pPaid_Amount);
        cmd.Parameters.AddWithValue("@pPaid_VAT_Amount", pPaid_VAT_Amount);
        cmd.Parameters.AddWithValue("@pPaymentDate", pPaymentDate);
        cmd.Parameters.AddWithValue("@pCmakerID", pCmakerID);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }
    public string CreateReductionPayment(int pDebtID , decimal pRevAmount, decimal pRevVatAmount, int pCmakerID)
    {
        SqlCommand cmd = new SqlCommand("CreateReverseAmount", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pDebtID", pDebtID);
        cmd.Parameters.AddWithValue("@pRevAmount", pRevAmount);
        cmd.Parameters.AddWithValue("@pRevVatAmount", pRevVatAmount);
        cmd.Parameters.AddWithValue("@pRevDate", Config.HostingTime);
        cmd.Parameters.AddWithValue("@pCmakerID", pCmakerID);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);
        
        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }
    public string CreateDiscountPayment(int pDebtID, decimal pDiscountAmount, decimal pDiscountVatAmount, int pCmakerID)
    {
        SqlCommand cmd = new SqlCommand("CreateDiscountAmount", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pDebtID", pDebtID);
        cmd.Parameters.AddWithValue("@pDiscountAmount", pDiscountAmount);
        cmd.Parameters.AddWithValue("@pDiscountVatAmount", pDiscountVatAmount);
        cmd.Parameters.AddWithValue("@pDiscountDate", Config.HostingTime);
        cmd.Parameters.AddWithValue("@pCmakerID", pCmakerID);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }
    public string CreateTransferPayment(int pDebtID, decimal pTransferAmount, decimal pTransferVatAmount, int pCmakerID)
    {
        SqlCommand cmd = new SqlCommand("CreateTransferAmount", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pDebtID", pDebtID);
        cmd.Parameters.AddWithValue("@pTransferAmount", pTransferAmount);
        cmd.Parameters.AddWithValue("@pTransferVatAmount", pTransferVatAmount);
        cmd.Parameters.AddWithValue("@pTransferDate", Config.HostingTime);
        cmd.Parameters.AddWithValue("@pCmakerID", pCmakerID);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }
    public DataTable GetPaymentDetailsByDebtId(int debt_id)
    {
        DataTable dt = new DataTable("GetPaymentDetailsByDebtId");
        SqlDataAdapter da = new SqlDataAdapter(@"select p.ID ,  convert(varchar(10),p.PaymentDate,104) PaymentDate, 
        (SELECT PT.Description FROM PaymentsType PT WHERE PT.CODE = P.PaymentType) AS PaymentType, TranType,
        p.PaymentAmount, (select u.FullName from Users u where u.ID = p.CMakerId) MakerName, p.PaymentType
        FROM Payments p  WHERE p.PaymentStatus = 'A' and p.Object_Debt_ID = @debtID 
        ORDER BY CMakerDate desc", SqlConn);
        
        da.SelectCommand.Parameters.Add("@debtID", debt_id);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public string DeletePayment(int pPaymentID, int pCMakerID)
    {
        SqlCommand cmd = new SqlCommand("DeletePayment", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pPaymentID", pPaymentID);
        cmd.Parameters.AddWithValue("@pCMakerID", pCMakerID);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }



    public DataTable GetPaymentDetailsById(int payment_id)
    {
        DataTable dt = new DataTable("GetPaymentDetailsById");
        SqlDataAdapter da = new SqlDataAdapter(@"select 
         Pt.Description as PType,
		 TR.RegNumber,
         O.ObjectFullNumber,
         p.PaymentAmount, 
         (select sum(Ods.Debt_Amount) - sum(Ods.Paid_Amount) from Object_Debts Ods where Ods.Debt_Type = p.PaymentType
          and Ods.Status = 'ACTIVE' and  Ods.ObjectID in (select Od.ObjectID from Object_Debts Od where Od.ID = p.Object_Debt_ID)) as Remain_Amount,
         U.Fullname,
         (select u.FullName from Users u where u.ID = p.CMakerId) MakerName,
          convert(varchar,p.PaymentDate,120) PaymentDate,
		  convert(varchar,convert(date,Od.Value_date,120),120) Debt_Date
         from Payments p inner join PaymentsType Pt on p.PaymentType = Pt.CODE  
                         inner join Objects O on p.ObjectID = O.ID
				         inner join Users U on p.CMakerId = U.ID
						 inner join Object_Debts Od on P.Object_Debt_ID = Od.ID
						 inner join TenantRegs TR ON TR.ObjectID = O.ID
         where p.PaymentStatus = 'A' and p.ID = @ID", SqlConn);

        da.SelectCommand.Parameters.Add("@ID", payment_id);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetObjectDetailsByRegNumber(string RegNumber)
    {
        DataTable dt = new DataTable("GetObjecttDetailsByDebtId");
        SqlDataAdapter da = new SqlDataAdapter(@"SELECT T.RegNumber, O.ZoneCode, O.LineCode, O.CorpusCode, O.ObjectNumber, O.ObjectFullNumber, O.ObjectArea, OP.ObjectPrice,
                FORMAT(O.ObjectArea * OP.ObjectPrice ,'0.00') AS MonthlyPayment, OP.ObjectDiscount,
                FORMAT(O.ObjectArea * OP.ObjectPrice - ISNULL(OP.ObjectDiscount,0),'0.00') AS NetPayment
                FROM Objects O, ObjectPrices OP, TenantRegs TR, Tenants T
                WHERE O.ID = OP.ObjectID AND TR.ObjectID = O.ID AND T.RegNumber = TR.RegNumber	AND OP.ObjectPriceType = 'PLACE'
                AND T.RegNumber = @pRegNumber", SqlConn);

        da.SelectCommand.Parameters.Add("@pRegNumber", RegNumber);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }
}