﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public partial class DbProcess : DALC
{
    

    public string UpdateCounterUnite(string pCounterType, decimal pUnitePrice, int pCMakerId)
    {
        SqlCommand cmd = new SqlCommand("UpdateCounterUnite", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pCounterType", pCounterType);
        cmd.Parameters.AddWithValue("@pUnitePrice", pUnitePrice);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public DataTable GetCounterUnite()
    {
        DataTable dt = new DataTable("GetCounterUnite");
        SqlDataAdapter da = new SqlDataAdapter(@"select 
        (select P.Description from PaymentsType P where P.CODE = C.CounterType) CounterTypeDesc,
        C.* ,
        CONVERT(varchar, C.CMakerDate, 120) CMakerDateFormat,
        U.FullName MakerName
        from CountersUnite C  left Join Users U on C.CMakerID = U.ID
        order by C.CMakerDate desc", SqlConn);

        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public DataTable GetCounterUniteHistory()
    {
        DataTable dt = new DataTable("GetCounterUniteHistory");
        SqlDataAdapter da = new SqlDataAdapter(@"select 
        (select P.Description from PaymentsType P where P.CODE = C.CounterType) CounterTypeDesc,
        C.* ,
        CONVERT(varchar, C.CMakerDate, 120) CMakerDateFormat,
        U.FullName MakerName
        from CountersUniteHistory C  left Join Users U on C.CMakerID = U.ID
        order by C.CMakerDate desc", SqlConn);

        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

   
}