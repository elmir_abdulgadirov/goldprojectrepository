﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;


public class Alert
{
	public static string DangerMessage(string message)
    {
        return String.Format("<div id = \"alert_msg\" class=\"alert alert-danger\" role=\"alert\">" +
             "<div class=\"d-flex align-items-center justify-content-start\">" +
                "<i class=\"icon ion-ios-close alert-icon tx-24\"></i>" +
                "<span>{0}</span>" +
             "</div>" +
           "</div>",message);
    }

    public static string WarningMessage(string message)
    {
        return String.Format("<div id = \"alert_msg_warning\" class=\"alert alert-warning\" role=\"alert\">" +
             "<div class=\"d-flex align-items-center justify-content-start\">" +
                "<i class=\"icon ion-alert-circled alert-icon tx-24 mg-t-5 mg-xs-t-0\"></i>" +
                "<span>{0}</span>" +
             "</div>" +
           "</div>", message);
    }

    public static string SuccessMessage(string message)
    {
        return String.Format("<div class=\"alert alert-success\" role=\"alert\">" +
              "{0}" +
            "</div>",message);
    }

    public static string AlertModal(string alertMessage)
    {
        string messagestr = String.Format("<div id=\"alertmodal\" class=\"modal fade\">" +
              "<div class=\"modal-dialog modal-sm\" role=\"document\">" +
                "<div class=\"modal-content bd-0 tx-14\">" +
                  "<div class=\"modal-header pd-x-20\">" +
                    "<h6 class=\"tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold\">Message</h6>" +
                  "</div>" +
                  "<div class=\"modal-body pd-20\">" +
                    "<p class=\"mg-b-5\">{0}</p>" +
                  "</div>" +
                  "<div class=\"modal-footer justify-content-center\">" +
                    "<button type=\"button\" class=\"btn btn-secondary pd-x-20\" data-dismiss=\"modal\">OK</button>" +
                  "</div>" +
                "</div>" +
              "</div>" +
            "</div>",alertMessage);
        return messagestr;
    }

   

    
}