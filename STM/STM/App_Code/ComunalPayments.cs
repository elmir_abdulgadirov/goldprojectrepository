﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public partial class DbProcess : DALC
{
    public DataTable GetComunalPointers(int userID, string pObjectCode, string pZoneCode, string pLineCode, string pCorpusCode)
    {
        DataTable dt = new DataTable("GetCashList");
        string sql_string = @"select 
                O.ID ObjectID,
                O.ObjectFullNumber, 
                C.CounterNumber , 
                C.CounterType ,
                case C.CounterType when 'ELECTRIC' then 'Elektrik'
                       when 'WATER' then 'Su' end as CounterTypeDesc,
                isnull(E.Counter_Value1,0) Counter_Value1,
                isnull(E.Counter_Value2,0) Counter_Value2,
                isnull(E.Calculated_amount,0) Calculated_amount,
                isnull((select t.Fullname from TenantRegs tr inner join Tenants t on tr.RegNumber = t.RegNumber and tr.Status = 'ACTIVE'
                and t.Status = tr.Status where tr.ObjectID = C.ObjectID),
                (select Os.Fullname from Owners Os where Status = 'ACTIVE' and ObjectID = C.ObjectID)) Debt_Owner,
                convert(varchar(10),E.CMakerDate,104)  as Value_date_desc
            from Counters C
            inner join Objects O on C.ObjectID = O.ID
            left join  ElectricWater_CounterValues E on C.CounterNumber = E.CounterNumber and C.CounterType = E.Counter_Type and E.Status = 'ACTIVE'

            where 
            C.CounterType in (SELECT replace(Function_ID,'COMUNAL_PAYMENT_','') from UserFunctions  where User_ID = @UserID and Menu_ID = 22 and CHARINDEX('COMUNAL_PAYMENT_',Function_ID) > 0) and
          
            C.Status = O.Status and C.Status = 'ACTIVE' and
            O.ZoneCode in  (select ZoneCode from UsersZone where UserID = @UserID) ";


        if (pObjectCode.Trim().Length > 0)
        {
            sql_string += @" and ( O.ID in 
		    (select ObjectID from TenantRegs where RegNumber in (
             select RegNumber from TenantRegs where ObjectID in 
            (select ID from Objects where ObjectFullNumber = @ObjectFullNumber))) or O.ObjectFullNumber = @ObjectFullNumber) order by O.ID";
        }

        if (pZoneCode.Length > 0)
        {
            sql_string += " and O.ZoneCode  = @ZoneCode";
        }
        if (pLineCode.Length > 0)
        {
            sql_string += " and O.LineCode  = @LineCode";
        }
        if (pCorpusCode.Length > 0)
        {
            sql_string += " and O.CorpusCode  = @CorpusCode";
        }

        if (pObjectCode.Trim().Length == 0)
        {
            sql_string += @"  order by O.ObjectFullNumber";
        }


        SqlDataAdapter da = new SqlDataAdapter(sql_string, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", userID);

        if (pObjectCode.Trim().Length > 0)
        {
            da.SelectCommand.Parameters.AddWithValue("@ObjectFullNumber", pObjectCode);
        }

        if (pZoneCode.Length > 0)
        {
            da.SelectCommand.Parameters.AddWithValue("@ZoneCode", pZoneCode);
        }

        if (pLineCode.Length > 0)
        {
            da.SelectCommand.Parameters.AddWithValue("@LineCode", pLineCode);
        }
        if (pCorpusCode.Length > 0)
        {
            da.SelectCommand.Parameters.AddWithValue("@CorpusCode", pCorpusCode);
        }

        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public string CreateComunalDebt(string pCounterNumber, string pCounterType, decimal pCurrentPointer,
                                    int pCMakerId)
    {
        SqlCommand cmd = new SqlCommand("CalculateComunalDebts", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pCounterNumber", pCounterNumber);
        cmd.Parameters.AddWithValue("@pCounterType", pCounterType);
        cmd.Parameters.AddWithValue("@pCurrentPointer", pCurrentPointer);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

}