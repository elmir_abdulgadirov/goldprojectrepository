﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class OwnersSearchParams
{
    public string fullname = "";
    public string passport_number = "";
    public string contact_number = "";
    public string object_number = "";
    public string reg_date = "";
    public string contract_number = "";
    public string address = "";
}