﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class ObjectsSearchParams
{
    public string zoneCode = "";
    public string lineCode = "";
    public string corpusCode = "";
    public string objectNumber = "";
    public string objectType = "0";
    public decimal objectArea = 0.0m;
    public decimal objectAreaContract = 0.0m;
    public string objectPosition = "0";
    public string ownerType = "0";
    public string objectStatus = "0";
}