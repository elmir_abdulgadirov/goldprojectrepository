﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
 


public partial class DbProcess : DALC
{
    
    public DataTable GetMenu(int userID)  
    {
        DataTable dt = new DataTable("Menu");
        SqlDataAdapter da = new SqlDataAdapter(@"SELECT M.id, 
                   M.parent_id, 
                   M.NAME, 
                   M.url, 
                   M.position, 
                   M.icon 
            FROM   dbo.menu M 
            WHERE  M.id IN (SELECT F.menu_id 
                            FROM   userfunctions U 
                                   INNER JOIN functions F 
                                           ON U.function_id = F.function_id 
                                              AND U.menu_id = F.menu_id 
                            WHERE  F.fieldname = '_MAIN_PAGE' 
                                   AND U.user_id = @UserID) 
                    OR M.id IN (SELECT M.parent_id 
                                FROM   dbo.menu M 
                                WHERE  M.id IN (SELECT F.menu_id 
                                                FROM   userfunctions U 
                                                       INNER JOIN functions F 
                                                               ON U.function_id = 
                                                                  F.function_id 
                                                                  AND U.menu_id = F.menu_id 
                                                WHERE  F.fieldname = '_MAIN_PAGE' 
                                                       AND U.user_id = @UserID)) 
            ORDER  BY M.position ", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", userID);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public void CheckFunctionPermission(int UserID, string FunctionID)
    {
        SqlCommand cmd = new SqlCommand("CheckFunctionPermission", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pUserID", UserID);
        cmd.Parameters.AddWithValue("@pFunction_ID", FunctionID);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }

        if (pResult != "OK")
        {
            Config.Rd("/notaccess");
        }
    }



    public bool CheckFieldView(int UserID, string FunctionID)
    {
        SqlCommand cmd = new SqlCommand("CheckFunctionPermission", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pUserID", UserID);
        cmd.Parameters.AddWithValue("@pFunction_ID", FunctionID);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch 
        {
            return false;
        }
        finally
        {
            cmd.Connection.Close();
        }

        if (pResult != "OK")
        {
            return false;
        }
        return true;
    }

    public string AddUser(string pFullName,string pContactNumber, string pSex , string pPosition , string pLogin,
          string pPassword,string pPicture, int pCMakerID)
    {
        SqlCommand cmd = new SqlCommand("AddUser", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pFullName", pFullName);

        cmd.Parameters.AddWithValue("@pContactNumber", pContactNumber);
        cmd.Parameters.AddWithValue("@pSex", pSex);
        cmd.Parameters.AddWithValue("@pPosition", pPosition);
        cmd.Parameters.AddWithValue("@pLogin", pLogin);
        cmd.Parameters.AddWithValue("@pPassword",Config.Sha1(pPassword));
        cmd.Parameters.AddWithValue("@pPicture", pPicture);
        cmd.Parameters.AddWithValue("@pCMakerID", pCMakerID);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

      
        string pResult = "";
        try 
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally 
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public DataTable GetUserList()
    {
        DataTable dt = new DataTable("Users");
        SqlDataAdapter da = new SqlDataAdapter("select * from V_GetActiveUsers order by CMakerDate desc", SqlConn);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public string DeleteUser(int pUserID, int pCMakerID)
    {
        SqlCommand cmd = new SqlCommand("DeleteUser", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pUserID", pUserID);
        cmd.Parameters.AddWithValue("@pCMakerID", pCMakerID);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }



    public DataRow GetUserByUserId(int UserID)
    {
        DataTable dt = new DataTable("User");
        DataRow dr = null;
        SqlDataAdapter da = new SqlDataAdapter("select * from V_GetActiveUsers where ID = @ID", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ID",UserID);
        try
        {
            da.Fill(dt);
            if (dt == null) return null;
            if (dt.Rows.Count == 0) return null;
            dr = dt.Rows[0];
        }
        catch
        {
            dt = null;
        }
        return dr;
    }

    public string UpdateUser(int pUserID, string pFullname, string pContactNumber, string pSex, string pPosition,
          string pPassword, string pPicture, int pCMakerID)
    {
        SqlCommand cmd = new SqlCommand("UpdateUser", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pUserID", pUserID);
        cmd.Parameters.AddWithValue("@pFullname", pFullname);
        cmd.Parameters.AddWithValue("@pContactNumber", pContactNumber);
        cmd.Parameters.AddWithValue("@pSex", pSex);
        cmd.Parameters.AddWithValue("@pPosition", pPosition);
        cmd.Parameters.AddWithValue("@pPassword", pPassword != "" ? Config.Sha1(pPassword) : "");
        cmd.Parameters.AddWithValue("@pPicture", pPicture);
        cmd.Parameters.AddWithValue("@pCMakerID", pCMakerID);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public DataTable GetUserZone(int UserID)
    {
        DataTable dt = new DataTable("UserZone");
        SqlDataAdapter da = new SqlDataAdapter("select z.*, isnull(u.UserID,0) UserID  from Zone z left join UsersZone u on z.Code = u.ZoneCode and u.UserID = @ID order by z.Code", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ID", UserID);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public string AddUserZone(DataTable dtParams,int UserID)
    {
        SqlConnection sql_conn = SqlConn;
        SqlTransaction transaction;

        SqlCommand cmd;
        SqlParameter paramResult;
        sql_conn.Open();
        transaction = sql_conn.BeginTransaction();
        string pResult = "";
        try
        {
            // DELETE BEGIN ----------------------------------------------------------------------
            cmd = new SqlCommand("DeleteUserZone", sql_conn, transaction);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@pUserId", UserID);

            paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            try
            {
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);

                if (pResult != "OK")
                {
                    transaction.Rollback();
                    sql_conn.Close();
                    return pResult;
                }
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
                transaction.Rollback();
                sql_conn.Close();
                return pResult;
            }     

            // DELETE END ----------------------------------------------------------------------------

            for (int i = 0; i < dtParams.Rows.Count; i++)
            {
                cmd = new SqlCommand("InserUserZone", sql_conn, transaction);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@pZoneCode", Convert.ToString(dtParams.Rows[i]["pZoneCode"]));
                cmd.Parameters.AddWithValue("@pUserId", Convert.ToInt32(dtParams.Rows[i]["pUserId"]));
                cmd.Parameters.AddWithValue("@pCMakerId", Convert.ToInt32(dtParams.Rows[i]["pCMakerId"]));
                cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

                paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                paramResult.Direction = ParameterDirection.Output;
                paramResult.Size = 100;
                cmd.Parameters.Add(paramResult);

                try
                {
                    cmd.ExecuteNonQuery();
                    pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);

                    if (pResult != "OK")
                    {
                        transaction.Rollback();
                        sql_conn.Close();
                        return pResult;
                    }
                }
                catch (Exception ex)
                {
                    pResult = ex.Message;
                    transaction.Rollback();
                    sql_conn.Close();
                    return pResult;
                }            
            }
            transaction.Commit();

        }
        catch (Exception sqlError)
        {
            pResult = sqlError.Message;
            transaction.Rollback();
            sql_conn.Close();
            return pResult;
        }
        sql_conn.Close();
        return pResult;
    }


    public DataTable GetUserModulesList()
    {
        DataTable dt = new DataTable("UserModules");
        SqlDataAdapter da = new SqlDataAdapter("select * from V_MenuForRole order by Position", SqlConn);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetUserModuleFunctionsList(int userID,int menuID)
    {
        DataTable dt = new DataTable("UserModuleFunctions");
        SqlDataAdapter da = new SqlDataAdapter(@"select f.* , case when isnull(u.User_ID,0) = 0 then 0 else 1 end as isRole
                                                 from Functions f left join UserFunctions u on f.Menu_ID = u.Menu_ID and f.Function_ID = u.Function_ID and u.User_ID = @User_ID
                                                where f.Menu_ID = @Menu_ID order by f.FieldName", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@User_ID", userID);
        da.SelectCommand.Parameters.AddWithValue("@Menu_ID", menuID);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }




    public string AddUserFunctions(DataTable dtParams, int UserID,int MenuID , int CMakerID)
    {
        SqlConnection sql_conn = SqlConn;
        SqlTransaction transaction;

        SqlCommand cmd;
        SqlParameter paramResult;
        sql_conn.Open();
        transaction = sql_conn.BeginTransaction();
        string pResult = "";
        try
        {
            // DELETE BEGIN ----------------------------------------------------------------------
            cmd = new SqlCommand("DeleteUserFunctions", sql_conn, transaction);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@pUserId", UserID);
            cmd.Parameters.AddWithValue("@pMenuId", MenuID);

            paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
            paramResult.Direction = ParameterDirection.Output;
            paramResult.Size = 100;
            cmd.Parameters.Add(paramResult);

            try
            {
                cmd.ExecuteNonQuery();
                pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);

                if (pResult != "OK")
                {
                    transaction.Rollback();
                    sql_conn.Close();
                    return pResult;
                }
            }
            catch (Exception ex)
            {
                pResult = ex.Message;
                transaction.Rollback();
                sql_conn.Close();
                return pResult;
            }

            // DELETE END ----------------------------------------------------------------------------


            for (int i = 0; i < dtParams.Rows.Count; i++)
            {
                cmd = new SqlCommand("InserUserFunctions", sql_conn, transaction);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@pMenuId", MenuID);
                cmd.Parameters.AddWithValue("@pUserId", UserID);
                cmd.Parameters.AddWithValue("@pFunctionId", Convert.ToString(dtParams.Rows[i]["pFunctionId"]));
                cmd.Parameters.AddWithValue("@pCMakerID", CMakerID);

                paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
                paramResult.Direction = ParameterDirection.Output;
                paramResult.Size = 100;
                cmd.Parameters.Add(paramResult);

                try
                {
                    cmd.ExecuteNonQuery();
                    pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);

                    if (pResult != "OK")
                    {
                        transaction.Rollback();
                        sql_conn.Close();
                        return pResult;
                    }
                }
                catch (Exception ex)
                {
                    pResult = ex.Message;
                    transaction.Rollback();
                    sql_conn.Close();
                    return pResult;
                }
            }

            transaction.Commit();

        }
        catch (Exception sqlError)
        {
            pResult = sqlError.Message;
            transaction.Rollback();
            sql_conn.Close();
            return pResult;
        }
        sql_conn.Close();
        return pResult;
    }




    public string dbLogin(string pLogin, string pPassword,ref string pUserInfo)
    {
        SqlCommand cmd = new SqlCommand("ULogin", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pLogin", pLogin);
        cmd.Parameters.AddWithValue("@pPassword", Config.Sha1(pPassword));
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        SqlParameter paramUserInfo = new SqlParameter("@pUserInfo", SqlDbType.NVarChar);
        paramUserInfo.Direction = ParameterDirection.Output;
        paramUserInfo.Size = 200;
        cmd.Parameters.Add(paramUserInfo);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
            pUserInfo = Convert.ToString(cmd.Parameters["@pUserInfo"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public DataTable GetUserZoneI(int userID)
    {
        DataTable dt = new DataTable("UserZone");
        SqlDataAdapter da = new SqlDataAdapter(@"select Zn.Code , Zn.Description from Users U inner join 
                                                 UsersZone Z on U.ID = Z.UserID 
                                                 inner join Zone Zn on Z.ZoneCode = Zn.Code
                                                 where U.ID = @ID", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ID", userID);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetZoneByAllowedUser(int UserID)
    {
        DataTable dt = new DataTable("AllZones");
        SqlDataAdapter da = new SqlDataAdapter(@"select Z.* , U.FullName from Zone Z 
          left join Users U on Z.CMakerId = U.ID where Z.Code in 
            (select ZoneCode from UsersZone where UserID = @UserID)
            ORDER BY IsNumeric(Z.Code), CASE WHEN IsNumeric(Z.Code) = 1  THEN CAST(Z.Code AS INT) ELSE NULL END", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", UserID);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetAllLines(string zoneCode)
    {
        DataTable dt = new DataTable("AllLines");
        SqlDataAdapter da = new SqlDataAdapter(@"select L.* , U.FullName from Line L left join Users U on L.CMakerId = U.ID 
        where L.ZoneCode = @ZoneCode
        ORDER BY IsNumeric(L.Code), CASE WHEN Isnumeric(L.code) = 1 THEN CAST(L.code AS INT) ELSE NULL END", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ZoneCode", zoneCode);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetAllCorpus(string zoneCode, string lineCode)
    {
        DataTable dt = new DataTable("AllLines");
        SqlDataAdapter da = new SqlDataAdapter(@"select C.* , U.FullName from Corpus 
        C left join Users U on C.CMakerId = U.ID 
        where C.ZoneCode = @ZoneCode and C.LineCode = @LineCode
        ORDER BY IsNumeric(C.Code), CASE WHEN IsNumeric(C.Code) = 1 THEN CAST(C.Code AS INT) ELSE NULL END", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ZoneCode", zoneCode);
        da.SelectCommand.Parameters.AddWithValue("@LineCode", lineCode);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }




    public string ChangePassword(int pUserID, string pPass)
    {
        SqlCommand cmd = new SqlCommand("ChangePassword", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pUserID", pUserID);
        cmd.Parameters.AddWithValue("@pPassword", Config.Sha1(pPass));


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public string ChangeUserPicture(int pUserID, string pPicturePath)
    {
        SqlCommand cmd = new SqlCommand("ChangeUserImage", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pUserID", pUserID);
        cmd.Parameters.AddWithValue("@pPicturePath", pPicturePath);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


}