﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class ReportForObjectsParams
{
    public string pFromDate = "";
    public string pToDate = "";
    public string pZoneCode = "";
    public string pLineCode = "";
    public string pCorpusCode = "";
    public int pPaymentType = -1;
    public string pDebtType = "";
    public string pObjectNumber = "";
    public string pObjectPosition = "0";
    public string pObjectType = "0";
    public string pOwnerType = "0";
    public string pObjectStatus = "0";
    public decimal pObjectAreaMin = 0;
    public decimal pObjectAreaMax = 0;
    public string pTenantContractNo = "";
    public string pTenantName = "";
    public string pOwnerContractNo = "";
    public string pOwnerName = "";
}