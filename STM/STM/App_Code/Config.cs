﻿using System;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public class Config
{

    public static Decimal ToDecimal(string Value)
    {
        try
        {
            return Convert.ToDecimal(Value);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static bool CheckObjectCode(string ObjectFullCode)
    {
        int count = 0;
        for (int i = 0; i < ObjectFullCode.Length; i++)
        {
            if (ObjectFullCode[i] == '-')
            {
                count++;
            }
        }
        if (ObjectFullCode.Length < 7) return false;
        if (count == 3) return true;
        return false;
    }

    // for hosting datetime - deyishir asili olaraq
    public static DateTime HostingTime
    {
        get
        {
            return DateTime.Now.AddHours(0);
        }
    }
    //Bunun lazım olan bütün səhifələrin ilk Page_Load -na qoyuruq. 
    //Lazım olan bütün setting əmməliyyatlarını bura əlavə et.
    public static void PageSettings()
    {
        HttpContext.Current.Response.Cache.SetNoStore(); //Templəri bağlayırıq...
        HttpContext.Current.Session.Timeout = 10080;
        HttpContext.Current.Server.ScriptTimeout = 9999; //Əgər yüklənmə gecikərsə maksimum gözləmə saniyəsi.
        System.Threading.Thread.Sleep(0);
    } 

    //Get WebConfig.config App Key
    public static string GetAppSetting(string KeyName)
    {
        return Cs(ConfigurationManager.AppSettings[KeyName]);
    }

    //Fileupload-da gələn şəkilin ölçüsünü kəsir (100x??px).
    public static Unit PicturesSizeSplit(string s)
    {
        try
        {
            int i = Convert.ToInt16(s.Substring(0, 3).Trim().Trim('x'));
            if (i < 220)
                return Unit.Pixel(i);
            else return Unit.Pixel(220);
        }
        catch { return Unit.Pixel(220); }
    }

    //Açar yaradaq.
    public static string Key(int say)
    {
        Random ran = new Random();
        string Bind = "aqwertyuipasdfghjkzxcvbnmQAZWSXEDCRFVTGBYHNUJMKP23456789";
        string Key = "";
        for (int i = 1; i <= say; i++)
        {
            Key += Bind.Substring(ran.Next(Bind.Length - 1), 1);
        }
        return Key.Trim();
    }

    //Set title to url (clear latin and special simvols)
    public static string ClrTitle(string Title)
    {
        Title = Title.ToLower().Trim().Trim('-').Trim('.').Trim();
        Title = Title.Replace("   ", " ");
        Title = Title.Replace("  ", " ");
        Title = Title.Replace(" ", "-");
        Title = Title.Replace(",", "-");
        Title = Title.Replace("\"", "");
        Title = Title.Replace("“", "");
        Title = Title.Replace("”", "");

        Title = Title.Replace("---", "-");
        Title = Title.Replace("--", "-");
        Title = Title.Replace("#", "");
        Title = Title.Replace("&", "");

        //No Latin
        Title = Title.Replace("ü", "u");
        Title = Title.Replace("ı", "i");
        Title = Title.Replace("ö", "o");
        Title = Title.Replace("ğ", "g");
        Title = Title.Replace("ə", "e");
        Title = Title.Replace("ç", "ch");
        Title = Title.Replace("ş", "sh");

        return Title;
    }


    public static string MonthToText(string Mont)
    {
        if (Mont == "01") return "Yanvar";
        if (Mont == "02") return "Fevral";
        if (Mont == "03") return "Mart";
        if (Mont == "04") return "Aprel";
        if (Mont == "05") return "May";
        if (Mont == "06") return "İyun";
        if (Mont == "07") return "İyul";
        if (Mont == "08") return "Avqust";
        if (Mont == "09") return "Sentyabr";
        if (Mont == "10") return "Oktyabr";
        if (Mont == "11") return "Noyabr";
        if (Mont == "12") return "Dekabr";

        return "--";
    }

    public static string DateTimeFormat(DateTime Dt)
    {
        return Dt.ToString("dd") + " " + MonthToText(Dt.ToString("MM")) + " " + Dt.ToString("yyyy");
    }

    public static string ClearInjection(string x)
    {
        x = x.Replace("<", "&lt;");
        x = x.Replace(">", "&gt;");
        x = x.Replace("`", "");
        x = x.Replace("'", "");
        x = x.Replace("\"", "");
        x = x.Replace("?", "");
        x = x.Replace("!", "");
        x = x.Replace("/", "");
        x = x.Replace("\\", "");
        x = x.Replace("&", "");
        x = x.Replace("#", "");
        x = x.Replace("~", "");
        x = x.Replace("%", "");
        x = x.Trim('.');
        x = x.Trim();
        return x;
    }

    //Cümlə çox uzun olanda üç nöqtə qoyaq.
    public static string SizeLimit(string Text, int Length)
    {
        if (Text.Length > Length) Text = Text.Substring(0, Length) + " ...";
        return Text;
    }

    //ConvertString.
    public static string Cs(object s)
    {
        if (s == null) s = "";
        return Convert.ToString(s);
    }

    //Numaric testi.
    public static bool IsNumeric(string s)
    {
        if (s == null) return false;
        if (s.Length < 1) return false;
        for (int i = 0; i < s.Length; i++)
        {
            if ("0123456789".IndexOf(s.Substring(i, 1)) < 0) { return false; }
        }
        return true;
    }

    //Messages Box.
    public static void MsgBox(string MsgTxt, Page P)
    {
        P.ClientScript.RegisterClientScriptBlock(P.GetType(), "PopupScript", "window.focus(); alert('" + MsgTxt + "');", true);
    }

    //Sha1  - özəl
    public static string Sha1(string Password)
    {
        byte[] result;
        System.Security.Cryptography.SHA1 ShaEncrp = new System.Security.Cryptography.SHA1CryptoServiceProvider();
        Password = String.Format("{0}{1}{0}", "CSAASADM", Password);
        byte[] buffer = new byte[Password.Length];
        buffer = System.Text.Encoding.UTF8.GetBytes(Password);
        result = ShaEncrp.ComputeHash(buffer);
        return Convert.ToBase64String(result);
    }

    //Səhifəni yönləndirək:
    public static void Rd(string GetUrl)
    {
        HttpContext.Current.Response.Redirect(GetUrl, true);
        HttpContext.Current.Response.End();
    }

    //Tarix yaradaq istifadəçilər üçün:
    public static string Dt(string Format)
    {
        //Əgər boş gələrsə hər ikisidə return edilir.
        string DateStr = "dd.MM.yyyy HH:mm";
        if (Format.Length > 1) DateStr = Format;
        if (Format == "t") DateStr = "dd.MM.yyyy";
        if (Format == "z") DateStr = "HH:mm:ss";
        DateStr = DateTime.Now.AddHours(0).ToString(DateStr);
        return DateStr;
    }

   
    public static bool IsEmail(string inputEmail)
    {
        const string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
        @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
        @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

        return (new Regex(strRegex)).IsMatch(inputEmail);
    }

    public static bool SendAdminMail(string to, string messTxt)
    {
        //Mail gondermek lazimd olduqda gonderilsin:
        System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage(GetAppSetting("MailLogin"), to, "LearnInformatics", messTxt);
        System.Net.Mail.SmtpClient SmtpMail = new System.Net.Mail.SmtpClient(GetAppSetting("MailServer"));
        mm.IsBodyHtml = true;
        SmtpMail.Credentials = new System.Net.NetworkCredential(GetAppSetting("MailLogin"), GetAppSetting("MailPassword"));
        mm.BodyEncoding = System.Text.Encoding.UTF8;
        mm.Priority = System.Net.Mail.MailPriority.Normal;
        mm.IsBodyHtml = true;
        try
        {
            SmtpMail.Send(mm);
            return true;
        }
        catch { return false; }
    }

    // alternativ sendMail
    public static bool SendEmail(string to, string messTxt)
    {
        using (MailMessage mm = new MailMessage(GetAppSetting("MailLogin"),to))
        {
            mm.Subject = "LearnInformatics";
            mm.Body =  messTxt;
            
            mm.IsBodyHtml = false;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = GetAppSetting("MailServer");
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential(GetAppSetting("MailLogin"), GetAppSetting("MailPassword"));
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
           
            try
            {
                smtp.Send(mm);
                return true;
            }
            catch
            {
               return false;
            }
           
        }
    }

    public static bool isDate(string dateFormat)
    {
        string tarih = dateFormat;
        DateTime date;
        string pattern = "dd.MM.yyyy";
        bool rihtDate = DateTime.TryParseExact(tarih, pattern, CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
        return rihtDate;
    }

    
    public static bool isEnglish(String key)
    {
        string bind = "0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        for (int i = 0; i < key.Length; i++)
        {
            if (bind.IndexOf(key[i]) == -1) return false;
        }
        return true;
    }

    public static string getKeyFromSplit(string str, char delimiter, int keyIndex)
    {
        try
        {
            return str.Split(delimiter)[keyIndex];
        }
        catch 
        {
            return "";
        }
    }

    public static DateTime ToDate(string date)
    {
        DateTime dt;
        string pattern = "dd.MM.yyyy";
        DateTime.TryParseExact(date, pattern, CultureInfo.InvariantCulture, DateTimeStyles.None, out dt);
        return dt;
    }
}
