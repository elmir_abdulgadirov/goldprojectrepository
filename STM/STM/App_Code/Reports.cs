﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public partial class DbProcess : DALC
{


    public DataTable GetReportForObjects(ReportForObjectsParams Rop, int UserID, bool ExportExcel, out int dataCount)
    {
        dataCount = 0;
        string sql_query = string.Format(@"select 
            O.ID as ObjectID,
            (select Z.Description from Zone Z where Z.Code = O.ZoneCode) as Zone,
            (select L.Description from Line L where L.Code = O.LineCode and L.ZoneCode = O.ZoneCode) as Line,
            (select C.Description from Corpus C where C.Code = O.CorpusCode and C.LineCode = O.LineCode and C.ZoneCode = O.ZoneCode) as Corpus,
            O.ObjectNumber,
            O.ObjectFullNumber,
            case O.ObjectType when 'MAGAZA' then N'Mağaza'
                              when 'SKLAT'  then N'Anbar'
				              when 'TUALET' then N'Tualet'
				              when 'METBEX' then N'Mətbəx'
				              when 'MARKET' then N'Market' end as ObjectType,
            O.ObjectArea,
            O.ObjectAreaContract,
            case O.ObjectPosition when 'KUNC' then N'Künc'
					              when 'KECHID'  then N'Keçid'
					              when 'NORMAL' then N'Normal' end as ObjectPosition,

            case O.ObjectStatus   when 'ACTIV' then N'Aktiv'
                                  when 'TEMIR'  then N'Təmir'
					              when 'BAGLI'  then N'Bağlı'
					              when 'BOSH' then N'Boş' end as ObjectStatus,

            case O.OwnerType      when 'BAZAR' then N'Bazar'
					              when 'SHEXSI' then N'Şəxsi' end as OwnerType,

            isnull((select Op.ObjectPrice from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'PLACE'),0) as PLACE_PRICE,
            isnull((select Op.ObjectDiscount from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'PLACE'),0) as PLACE_PRICE_DISCOUNT,
            isnull((select U.FullName from Users U where U.ID = (select isnull(Op.ObjectDiscountMakerID,0) from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'PLACE')),'') as PLACE_PRICE_DISCOUNT_MAKERNAME,
            isnull((select Op.ObjectPrice from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'OWNER'),0) as OWNER_PRICE,
            isnull((select Op.ObjectPrice from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'TAX'),0) as TAX_PRICE,
            isnull((select Op.ObjectPrice from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'INTERNET'),0) as INTERNET_PRICE,
            isnull(Tenant.TenantName,'') as TenantName,
            isnull(Tenant.TenantContract,'') as TenantContract,
            isnull(Own.Fullname,'') as OwnerName,
            isnull(Own.ContractNumber,'') as OwnerContract,
            isnull((select DebtAmount from  V_ObjectDebts Vo where Vo.Debt_Type = 'PLACE' and Vo.ObjectID = O.ID),0) as PRICE_DEBT,
            isnull((select DebtAmount from  V_ObjectDebts Vo where Vo.Debt_Type = 'OWNER' and Vo.ObjectID = O.ID),0) as OWNER_DEBT,
            isnull((select DebtAmount from  V_ObjectDebts Vo where Vo.Debt_Type = 'TAX' and Vo.ObjectID = O.ID),0) as TAX_DEBT,
            isnull((select DebtAmount from  V_ObjectDebts Vo where Vo.Debt_Type = 'INTERNET' and Vo.ObjectID = O.ID),0) as INTERNET_DEBT,
            isnull((select DebtAmount from  V_ObjectDebts Vo where Vo.Debt_Type = 'ELECTRIC' and Vo.ObjectID = O.ID),0) as ELECTRIC_DEBT,
            isnull((select DebtAmount from  V_ObjectDebts Vo where Vo.Debt_Type = 'WATER' and Vo.ObjectID = O.ID),0) as WATER_DEBT

            from 
            Objects O 

            left join

            (select 
            Tr.ObjectID as TenantObjectID, 
            T.ContractNumber as TenantContract,
            T.Fullname + (case Tr.Type when 'ICARECHI' then N' (İcarəçi)' when 'SAHIBKAR' then N' (Sahibkar)' end) as TenantName
            from Tenants T inner join TenantRegs Tr on T.RegNumber = Tr.RegNumber
            where T.Status = Tr.Status and T.Status = 'ACTIVE') Tenant on O.ID = Tenant.TenantObjectID

            left join 

            Owners Own on O.ID = Own.ObjectID and Own.Status = 'ACTIVE'

            where O.Status = 'ACTIVE' and 
            O.ZoneCode in (select ZoneCode from UsersZone where UserID = @UserID)");

        if (ExportExcel)
        {
            sql_query = string.Format(@"select 
            (select Z.Description from Zone Z where Z.Code = O.ZoneCode) as [Zona],
            (select L.Description from Line L where L.Code = O.LineCode and L.ZoneCode = O.ZoneCode) as [Sıra],
            (select C.Description from Corpus C where C.Code = O.CorpusCode and C.LineCode = O.LineCode and C.ZoneCode = O.ZoneCode) as [Korpus],
            O.ObjectNumber as [Obyekt №],
            O.ObjectFullNumber [Obyekt tam №],
            case O.ObjectType when 'MAGAZA' then N'Mağaza'
                              when 'SKLAT'  then N'Anbar'
				              when 'TUALET' then N'Tualet'
				              when 'METBEX' then N'Mətbəx'
				              when 'MARKET' then N'Market' end as  [Tipi],
            O.ObjectArea [Sahə],
            O.ObjectAreaContract [Müqavilə üzrə sahə],
            case O.ObjectPosition when 'KUNC' then N'Künc'
					              when 'KECHID'  then N'Keçid'
					              when 'NORMAL' then N'Normal' end as [Yerləşmə tipi],

            case O.ObjectStatus   when 'ACTIV' then N'Aktiv'
                                  when 'TEMIR'  then N'Təmir'
					              when 'BAGLI'  then N'Bağlı'
					              when 'BOSH' then N'Boş' end as [Statusu],

            case O.OwnerType      when 'BAZAR' then N'Bazar'
					              when 'SHEXSI' then N'Şəxsi' end as [Mülkiyyət],

            isnull((select Op.ObjectPrice from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'PLACE'),0) as [Yer pulu],
            isnull((select Op.ObjectDiscount from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'PLACE'),0) as [Yer pulu üzrə endirim],
            isnull((select U.FullName from Users U where U.ID = (select isnull(Op.ObjectDiscountMakerID,0) from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'PLACE')),'') as [Yer pulu üzrə endirim edən],
            isnull((select Op.ObjectPrice from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'OWNER'),0) as [Sahibkar pulu],
            isnull((select Op.ObjectPrice from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'TAX'),0) as [Vergi pulu],
            isnull((select Op.ObjectPrice from ObjectPrices Op where Op.ObjectID = O.ID and Op.ObjectPriceType = 'INTERNET'),0) as [İnternet pulu],
            isnull(Tenant.TenantName,'') as [İcarəçi adı],
            isnull(Tenant.TenantContract,'') as [Müqavilə (İcarəçi)],
            isnull(Own.Fullname,'') as [Sahibkar adı],
            isnull(Own.ContractNumber,'') as [Müqavilə (Sahibkar)],
            isnull((select DebtAmount from  V_ObjectDebts Vo where Vo.Debt_Type = 'PLACE' and Vo.ObjectID = O.ID),0) as [Yer pulu borcu],
            isnull((select DebtAmount from  V_ObjectDebts Vo where Vo.Debt_Type = 'OWNER' and Vo.ObjectID = O.ID),0) as [Sahibkar pulu borcu],
            isnull((select DebtAmount from  V_ObjectDebts Vo where Vo.Debt_Type = 'TAX' and Vo.ObjectID = O.ID),0) as [Vergi pulu borcu],
            isnull((select DebtAmount from  V_ObjectDebts Vo where Vo.Debt_Type = 'INTERNET' and Vo.ObjectID = O.ID),0) as [İnternet pulu borcu],
            isnull((select DebtAmount from  V_ObjectDebts Vo where Vo.Debt_Type = 'ELECTRIC' and Vo.ObjectID = O.ID),0) as [Elektrik pulu borcu],
            isnull((select DebtAmount from  V_ObjectDebts Vo where Vo.Debt_Type = 'WATER' and Vo.ObjectID = O.ID),0) as [Su pulu borcu]

            from 
            Objects O 

            left join

            (select 
            Tr.ObjectID as TenantObjectID, 
            T.ContractNumber as TenantContract,
            T.Fullname + (case Tr.Type when 'ICARECHI' then N' (İcarəçi)' when 'SAHIBKAR' then N' (Sahibkar)' end) as TenantName
            from Tenants T inner join TenantRegs Tr on T.RegNumber = Tr.RegNumber
            where T.Status = Tr.Status and T.Status = 'ACTIVE') Tenant on O.ID = Tenant.TenantObjectID

            left join 

            Owners Own on O.ID = Own.ObjectID and Own.Status = 'ACTIVE'

            where O.Status = 'ACTIVE' and 
            O.ZoneCode in (select ZoneCode from UsersZone where UserID = @UserID)");
        }

        if (Rop.pZoneCode.Length != 0) sql_query += " and O.ZoneCode = @pZoneCode";
        if (Rop.pLineCode.Length != 0) sql_query += " and O.LineCode = @pLineCode";
        if (Rop.pCorpusCode.Length != 0) sql_query += " and O.CorpusCode = @pCorpusCode";

        if (Rop.pObjectNumber != "") sql_query += " and O.ObjectNumber = @pObjectNumber";
        if (Rop.pObjectPosition != "0") sql_query += " and O.ObjectPosition = @pObjectPosition";
        if (Rop.pObjectType != "0") sql_query += " and O.ObjectType = @pObjectType";
        if (Rop.pOwnerType != "0") sql_query += " and O.OwnerType = @pOwnerType";
        if (Rop.pObjectStatus != "0") sql_query += " and O.ObjectStatus = @pObjectStatus";
        if (Rop.pObjectAreaMin != 0) sql_query += " and O.ObjectArea >= @pObjectAreaMin";
        if (Rop.pObjectAreaMax != 0) sql_query += " and O.ObjectArea <= @pObjectAreaMax";

        if (Rop.pTenantContractNo != "") sql_query += " and Tenant.TenantContract = @pTenantContract";
        if (Rop.pTenantName != "") sql_query += "  and lower(replace(Tenant.TenantName,N'İ','i')) like lower(N'%' + @pTenantName + '%')";

        if (Rop.pOwnerContractNo != "") sql_query += " and Own.ContractNumber = @pContractNumber";
        if (Rop.pOwnerName != "") sql_query += "  and lower(replace(Own.Fullname,N'İ','i')) like lower(N'%' + @pOwnerName + '%')";


        sql_query += " order by O.ZoneCode, O.LineCode, O.CorpusCode";

        DataTable dt = new DataTable("GetReportForObjects");
        SqlDataAdapter da = new SqlDataAdapter(sql_query, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", UserID);

        if (Rop.pZoneCode.Length != 0) da.SelectCommand.Parameters.AddWithValue("@pZoneCode", Rop.pZoneCode);
        if (Rop.pLineCode.Length != 0) da.SelectCommand.Parameters.AddWithValue("@pLineCode", Rop.pLineCode);
        if (Rop.pCorpusCode.Length != 0) da.SelectCommand.Parameters.AddWithValue("@pCorpusCode", Rop.pCorpusCode);

        if (Rop.pObjectNumber != "") da.SelectCommand.Parameters.AddWithValue("@pObjectNumber", Rop.pObjectNumber);
        if (Rop.pObjectPosition != "0") da.SelectCommand.Parameters.AddWithValue("@pObjectPosition", Rop.pObjectPosition);
        if (Rop.pObjectType != "0") da.SelectCommand.Parameters.AddWithValue("@pObjectType", Rop.pObjectType);
        if (Rop.pOwnerType != "0") da.SelectCommand.Parameters.AddWithValue("@pOwnerType", Rop.pOwnerType);
        if (Rop.pObjectStatus != "0") da.SelectCommand.Parameters.AddWithValue("@pObjectStatus", Rop.pObjectStatus);
        if (Rop.pObjectAreaMin != 0) da.SelectCommand.Parameters.AddWithValue("@pObjectAreaMin", Rop.pObjectAreaMin);
        if (Rop.pObjectAreaMax != 0) da.SelectCommand.Parameters.AddWithValue("@pObjectAreaMax", Rop.pObjectAreaMax);

        if (Rop.pTenantContractNo != "") da.SelectCommand.Parameters.AddWithValue("@pTenantContract", Rop.pTenantContractNo);
        if (Rop.pTenantName != "") da.SelectCommand.Parameters.AddWithValue("@pTenantName", Rop.pTenantName);

        if (Rop.pOwnerContractNo != "") da.SelectCommand.Parameters.AddWithValue("@pContractNumber", Rop.pOwnerContractNo);
        if (Rop.pOwnerName != "") da.SelectCommand.Parameters.AddWithValue("@pOwnerName", Rop.pOwnerName);

        try
        {
            da.Fill(dt);
            dataCount = dt.Rows.Count;
        }
        catch
        {
            dt = null;
        }
        return dt;
    }




    public DataTable GetReportForPayment(ReportForObjectsParams Rop, int UserID, bool ExportExcel, out int dataCount)
    {
        dataCount = 0;
        string sql_query = string.Format(@"select 
        (select Z.Description from Zone Z where Z.Code = O.ZoneCode) as Zone,
        (select L.Description from Line L where L.Code = O.LineCode and L.ZoneCode = O.ZoneCode) as Line,
        (select C.Description from Corpus C where C.Code = O.CorpusCode and C.LineCode = O.LineCode and C.ZoneCode = O.ZoneCode) as Corpus, 
        O.ObjectFullNumber,
        Tenant.TenantName,
        Tenant.TenantContract,
        isnull((select Pt.Description from PaymentsType Pt where Pt.CODE = P.PaymentType),'') as PaymentType,
        P.PaymentAmount,
        convert(varchar(10),P.PaymentDate,104) as PaymentDate,
        (select U.FullName from Users U where U.ID = P.CMakerId) as PaymentMakerName
        from Payments P
        left join
        Objects O on P.ObjectID = O.ID
        left join
       (select 
       Tr.ObjectID as TenantObjectID,
       T.ContractNumber as TenantContract, 
       T.Fullname + (case Tr.Type when 'ICARECHI' then N' (İcarəçi)' when 'SAHIBKAR' then N' (Sahibkar)' end) as TenantName
       from Tenants T inner join TenantRegs Tr on T.RegNumber = Tr.RegNumber
       where T.Status = Tr.Status and T.Status = 'ACTIVE') Tenant on O.ID = Tenant.TenantObjectID
       where P.PaymentStatus = 'A'
       and O.ZoneCode in (select ZoneCode from UsersZone where UserID = @UserID)");

        if (ExportExcel)
        {
            sql_query = string.Format(@"select 
        (select Z.Description from Zone Z where Z.Code = O.ZoneCode) as [Zona],
        (select L.Description from Line L where L.Code = O.LineCode and L.ZoneCode = O.ZoneCode) as [Sıra],
        (select C.Description from Corpus C where C.Code = O.CorpusCode and C.LineCode = O.LineCode and C.ZoneCode = O.ZoneCode) as [Korpus], 
        O.ObjectFullNumber as [Obyekt Kodu], ISNULL( Tenant.TaxID, '') as VÖEN,
        Tenant.TenantName as [İcarəçi], 
        isnull(Tenant.TenantContract,'') [Müqavilə №],
        isnull((select Pt.Description from PaymentsType Pt where Pt.CODE = P.PaymentType),'') as [Ödəmə Növü],
		P.TranType AS [Əməliyyat Tipi],
		CASE WHEN OD.PaymentType = 0 THEN N'Nəğd'  ELSE N'Köçürmə' END [Ödəmə Tipi],
        P.PaymentAmount as [Məbləğ],
        convert(varchar(10),P.PaymentDate,104) as [Ödəmə Tarixi],
        (select U.FullName from Users U where U.ID = P.CMakerId) as [Ödənişi qəbul edən]
        from Payments P
        left join Objects O on P.ObjectID = O.ID
		inner join Object_Debts OD ON OD.ID = P.Object_Debt_ID  
        left join
       (select 
       Tr.ObjectID as TenantObjectID, T.TaxID,
       T.ContractNumber as TenantContract, 
       T.Fullname + (case Tr.Type when 'ICARECHI' then N' (İcarəçi)' when 'SAHIBKAR' then N' (Sahibkar)' end) as TenantName
       from Tenants T inner join TenantRegs Tr on T.RegNumber = Tr.RegNumber
       where T.Status = Tr.Status and T.Status = 'ACTIVE') Tenant on O.ID = Tenant.TenantObjectID
       where P.PaymentStatus = 'A'
       and O.ZoneCode in (select ZoneCode from UsersZone where UserID = @UserID)");
        }

        if (Rop.pFromDate != "") sql_query += " and convert(date, P.PaymentDate, 104) >= convert(date, @pFromDate, 104)";
        if (Rop.pToDate != "") sql_query += " and convert(date, P.PaymentDate, 104)  <= convert(date, @pToDate, 104)";

        if (Rop.pZoneCode.Length != 0) sql_query += " and O.ZoneCode = @pZoneCode";
        if (Rop.pLineCode.Length != 0) sql_query += " and O.LineCode = @pLineCode";
        if (Rop.pCorpusCode.Length != 0) sql_query += " and O.CorpusCode = @pCorpusCode";

        if (Rop.pObjectNumber != "") sql_query += " and O.ObjectNumber = @pObjectNumber";
        if (Rop.pObjectPosition != "0") sql_query += " and O.ObjectPosition = @pObjectPosition";
        if (Rop.pObjectType != "0") sql_query += " and O.ObjectType = @pObjectType";
        if (Rop.pOwnerType != "0") sql_query += " and O.OwnerType = @pOwnerType";
        if (Rop.pObjectStatus != "0") sql_query += " and O.ObjectStatus = @pObjectStatus";
        if (Rop.pObjectAreaMin != 0) sql_query += " and O.ObjectArea >= @pObjectAreaMin";
        if (Rop.pObjectAreaMax != 0) sql_query += " and O.ObjectArea <= @pObjectAreaMax";

        if (Rop.pTenantContractNo != "") sql_query += " and Tenant.TenantContract = @pTenantContract";
        if (Rop.pTenantName != "") sql_query += "  and lower(replace(Tenant.TenantName,N'İ','i')) like lower(N'%' + @pTenantName + '%')";

        


        sql_query += " order by O.ZoneCode, O.LineCode, O.CorpusCode";

        DataTable dt = new DataTable("GetReportForPayment");
        SqlDataAdapter da = new SqlDataAdapter(sql_query, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", UserID);

        if (Rop.pFromDate != "") da.SelectCommand.Parameters.AddWithValue("@pFromDate", Rop.pFromDate);
        if (Rop.pToDate != "") da.SelectCommand.Parameters.AddWithValue("@pToDate", Rop.pToDate);

        if (Rop.pZoneCode.Length != 0) da.SelectCommand.Parameters.AddWithValue("@pZoneCode", Rop.pZoneCode);
        if (Rop.pLineCode.Length != 0) da.SelectCommand.Parameters.AddWithValue("@pLineCode", Rop.pLineCode);
        if (Rop.pCorpusCode.Length != 0) da.SelectCommand.Parameters.AddWithValue("@pCorpusCode", Rop.pCorpusCode);

        if (Rop.pObjectNumber != "") da.SelectCommand.Parameters.AddWithValue("@pObjectNumber", Rop.pObjectNumber);
        if (Rop.pObjectPosition != "0") da.SelectCommand.Parameters.AddWithValue("@pObjectPosition", Rop.pObjectPosition);
        if (Rop.pObjectType != "0") da.SelectCommand.Parameters.AddWithValue("@pObjectType", Rop.pObjectType);
        if (Rop.pOwnerType != "0") da.SelectCommand.Parameters.AddWithValue("@pOwnerType", Rop.pOwnerType);
        if (Rop.pObjectStatus != "0") da.SelectCommand.Parameters.AddWithValue("@pObjectStatus", Rop.pObjectStatus);
        if (Rop.pObjectAreaMin != 0) da.SelectCommand.Parameters.AddWithValue("@pObjectAreaMin", Rop.pObjectAreaMin);
        if (Rop.pObjectAreaMax != 0) da.SelectCommand.Parameters.AddWithValue("@pObjectAreaMax", Rop.pObjectAreaMax);

        if (Rop.pTenantContractNo != "") da.SelectCommand.Parameters.AddWithValue("@pTenantContract", Rop.pTenantContractNo);
        if (Rop.pTenantName != "") da.SelectCommand.Parameters.AddWithValue("@pTenantName", Rop.pTenantName);

       

        try
        {
            da.Fill(dt);
            dataCount = dt.Rows.Count;
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataTable GetReportForPlan(ReportForObjectsParams Rop, int UserID, bool ExportExcel, out int dataCount)
    {
        dataCount = 0;
        string sql_query = "";
        if (!ExportExcel)
        {
            sql_query = string.Format(@"SELECT O.ZoneCode AS ZONA,
            (SELECT Description FROM PaymentsType WHERE CODE = OD.Debt_Type) AS DEBT_TYPE,
           CASE WHEN OD.PaymentType = 0 THEN N'NƏĞD'
                WHEN OD.PaymentType = 1 THEN N'KOÇÜRMƏ'
                ELSE N'NƏĞD/KÖÇÜRMƏ' END AS ODEME_TIPI,
				SUM(OD.Debt_Amount) AS HESABLAMA, SUM(OD.Paid_Amount) AS ODEME, SUM(OD.Reducton_Amount) AS BOSH, SUM(OD.Discount_Amount) AS GUZESHT,  
	            SUM(isnull(OD.Debt_Amount,0) - isnull(OD.Paid_Amount,0)-isnull(OD.Reducton_Amount,0) - isnull(OD.Discount_Amount,0)) AS QALIQ_ESAS,
	            SUM(OD.Vat_Amount) AS EDV, SUM(OD.Paid_VAT_Amount) AS EDV_ODEME, SUM(OD.Reducton_VatAmount) AS EDV_BOSH, SUM(OD.Discount_VatAmount) AS EDV_GUZESHT,	            
	            SUM(isnull(OD.Vat_Amount,0) - isnull(OD.Paid_VAT_Amount,0)-isnull(OD.Reducton_VatAmount,0) - isnull(OD.Discount_VatAmount,0)) AS QALIQ_EDV
         FROM Object_Debts OD, Objects O, TenantRegs TR, PaymentsType PT 
         WHERE OD.Debt_Type = PT.CODE AND O.ID = OD.ObjectID AND TR.ObjectID = O.ID AND OD.Status= 'ACTIVE'
	     and O.ZoneCode in (select ZoneCode from UsersZone where UserID = @UserID)");

        }
        else
        {
            sql_query = string.Format(@"SELECT FORMAT(CAST(OD.Value_Date as date), 'dd.MM.yyyy') AS BORC_TARIXI, O.ZoneCode AS ZONA, O.LineCode AS SIRA, O.CorpusCode AS KORPUS,  O.ObjectNumber AS MAGAZA, 
         O.ObjectFullNumber AS MAGAZA_KODU, TR.RegNumber AS MUSHTERI_ID, O.ObjectArea AS KVM,
           CASE WHEN O.PaymentType = 0 THEN N'NƏĞD'
                WHEN O.PaymentType = 1 THEN N'KOÇÜRMƏ'
                ELSE N'NƏĞD/KÖÇÜRMƏ' END AS ODEME_TIPI,
                UPPER(PT.Description) AS BORC_TIPI, OD.Debt_Amount AS HESABLAMA, OD.Paid_Amount AS ODEME, OD.Reducton_Amount AS BOSH, OD.Discount_Amount AS GUZESHT,  
	            isnull(OD.Debt_Amount,0) - isnull(OD.Paid_Amount,0)-isnull(OD.Reducton_Amount,0) - isnull(OD.Discount_Amount,0) AS QALIQ_ESAS,
	            OD.Vat_Amount AS EDV, OD.Paid_VAT_Amount AS EDV_ODEME, OD.Reducton_VatAmount AS EDV_BOSH, OD.Discount_VatAmount AS EDV_GUZESHT,
	            isnull(OD.Vat_Amount,0) - isnull(OD.Paid_VAT_Amount,0)-isnull(OD.Reducton_VatAmount,0) - isnull(OD.Discount_VatAmount,0) AS QALIQ_EDV,
	            isnull(OD.Vat_Amount,0) - isnull(OD.Paid_VAT_Amount,0)-isnull(OD.Reducton_VatAmount,0) - isnull(OD.Discount_VatAmount,0) AS QALIQ_EDV
         FROM Object_Debts OD, Objects O, TenantRegs TR, PaymentsType PT 
         WHERE OD.Debt_Type = PT.CODE AND O.ID = OD.ObjectID AND TR.ObjectID = O.ID AND OD.Status= 'ACTIVE'
	     and O.ZoneCode in (select ZoneCode from UsersZone where UserID = @UserID)");
        }

        if (ExportExcel)
        {
            if (Rop.pZoneCode.Length != 0) sql_query += " and O.ZoneCode = @pZoneCode";
            if (Rop.pFromDate != "") sql_query += " and convert(date, OD.Value_Date, 104) >= convert(date, @pFromDate, 104)";
            if (Rop.pToDate != "") sql_query += " and convert(date, OD.Value_Date, 104)  <= convert(date, @pToDate, 104)";
            if (Rop.pPaymentType > -1) sql_query += " and O.PaymentType = @pPaymentType";
            if (Rop.pDebtType != "") sql_query += " and OD.Dept_Type = @pDebt_Type";
            sql_query += " order by O.ZoneCode, O.LineCode, O.CorpusCode";
        }
        else
        {
            if (Rop.pFromDate != "") sql_query += " and convert(date, OD.Value_Date, 104) >= convert(date, @pFromDate, 104)";
            if (Rop.pToDate != "") sql_query += " and convert(date, OD.Value_Date, 104)  <= convert(date, @pToDate, 104)";

            if (Rop.pZoneCode.Length != 0) sql_query += " and O.ZoneCode = @pZoneCode";
            if (Rop.pLineCode.Length != 0) sql_query += " and O.LineCode = @pLineCode";
            if (Rop.pCorpusCode.Length != 0) sql_query += " and O.CorpusCode = @pCorpusCode";
            if (Rop.pPaymentType > -1) sql_query += " and O.PaymentType = @pPaymentType";
            if (Rop.pDebtType != "") sql_query += " and OD.Dept_Type = @pDebt_Type";
            if (Rop.pObjectNumber != "") sql_query += " and O.ObjectNumber = @pObjectNumber";
            if (Rop.pObjectPosition != "0") sql_query += " and O.ObjectPosition = @pObjectPosition";
            if (Rop.pObjectType != "0") sql_query += " and O.ObjectType = @pObjectType";
            if (Rop.pOwnerType != "0") sql_query += " and O.OwnerType = @pOwnerType";
            if (Rop.pObjectStatus != "0") sql_query += " and O.ObjectStatus = @pObjectStatus";
            if (Rop.pObjectAreaMin != 0) sql_query += " and O.ObjectArea >= @pObjectAreaMin";
            if (Rop.pObjectAreaMax != 0) sql_query += " and O.ObjectArea <= @pObjectAreaMax";
            if (Rop.pTenantContractNo != "") sql_query += " and Tenant.TenantContract = @pTenantContract";
            if (Rop.pTenantName != "") sql_query += "  and lower(replace(Tenant.TenantName,N'İ','i')) like lower(N'%' + @pTenantName + '%')";
            sql_query += " GROUP BY O.ZoneCode, OD.Debt_Type,  OD.PaymentType";
        }


        DataTable dt = new DataTable("GetReportForPayment");
        SqlDataAdapter da = new SqlDataAdapter(sql_query, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", UserID);

        if (Rop.pFromDate != "") da.SelectCommand.Parameters.AddWithValue("@pFromDate", Rop.pFromDate);
        if (Rop.pToDate != "") da.SelectCommand.Parameters.AddWithValue("@pToDate", Rop.pToDate);
        if (Rop.pZoneCode.Length != 0) da.SelectCommand.Parameters.AddWithValue("@pZoneCode", Rop.pZoneCode);
        if (Rop.pLineCode.Length != 0) da.SelectCommand.Parameters.AddWithValue("@pLineCode", Rop.pLineCode);
        if (Rop.pCorpusCode.Length != 0) da.SelectCommand.Parameters.AddWithValue("@pCorpusCode", Rop.pCorpusCode);
        if (Rop.pPaymentType >-1) da.SelectCommand.Parameters.AddWithValue("@pPaymentType", Rop.pPaymentType);
        if (Rop.pDebtType != "") da.SelectCommand.Parameters.AddWithValue("@pDebt_Type", Rop.pDebtType);
        if (Rop.pObjectNumber != "") da.SelectCommand.Parameters.AddWithValue("@pObjectNumber", Rop.pObjectNumber);
        if (Rop.pObjectPosition != "0") da.SelectCommand.Parameters.AddWithValue("@pObjectPosition", Rop.pObjectPosition);
        if (Rop.pObjectType != "0") da.SelectCommand.Parameters.AddWithValue("@pObjectType", Rop.pObjectType);
        if (Rop.pOwnerType != "0") da.SelectCommand.Parameters.AddWithValue("@pOwnerType", Rop.pOwnerType);
        if (Rop.pObjectStatus != "0") da.SelectCommand.Parameters.AddWithValue("@pObjectStatus", Rop.pObjectStatus);
        if (Rop.pObjectAreaMin != 0) da.SelectCommand.Parameters.AddWithValue("@pObjectAreaMin", Rop.pObjectAreaMin);
        if (Rop.pObjectAreaMax != 0) da.SelectCommand.Parameters.AddWithValue("@pObjectAreaMax", Rop.pObjectAreaMax);
        if (Rop.pTenantContractNo != "") da.SelectCommand.Parameters.AddWithValue("@pTenantContract", Rop.pTenantContractNo);
        if (Rop.pTenantName != "") da.SelectCommand.Parameters.AddWithValue("@pTenantName", Rop.pTenantName);

        try
        {
            da.Fill(dt);
            dataCount = dt.Rows.Count;
        }
        catch (Exception ex)
        {
            dt = null;
        }
        return dt;
    }

}