﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public partial class DbProcess : DALC
{
    public DataTable GetMyCash(int userID, DateTime date)
    {
        DataTable dt = new DataTable("GetMyCash");
        SqlDataAdapter da = new SqlDataAdapter(@"select 
	        pt.Description PaymentTypeDesc,
	        p.PaymentType, 
	        convert(varchar(10),p.PaymentDate,104) PaymentDate,
	        sum(p.PaymentAmount) PaymentAmount
        from Payments p inner join PaymentsType pt on p.PaymentType = pt.CODE 
        where p.PaymentStatus = 'A' and p.CMakerId = @UserID and p.PaymentDate = cast(@PaymetDate as Date)
        group by p.PaymentType,p.PaymentDate, pt.Description order by pt.Description", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", userID);
        da.SelectCommand.Parameters.AddWithValue("@PaymetDate", date);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public DataTable GetUserPaymentsByZone(int userID, DateTime dateFrom, DateTime dateTo, string zoneCode, string paymentTypeCode)
    {
        string sqlCirteria = " ";
        DataTable dt = new DataTable("GetUserPaymentsByZone");
        string sql = @"select 
             U.FullName CMakerName,
             pt.Description PaymentTypeDesc, p.TranType,
             p.PaymentType, 
             convert(varchar(10),p.PaymentDate,104) PaymentDate,
             sum(p.PaymentAmount) PaymentAmount
            from Payments p inner join PaymentsType pt on p.PaymentType = pt.CODE 
                            inner join Users U on P.CMakerId = U.ID
				            inner join Objects O on P.ObjectID = O.ID
            where p.PaymentStatus = 'A' and 
            p.PaymentDate between cast(@DateFrom as Date) and cast(@DateTo as Date)
            and O.ZoneCode in (select ZoneCode from UsersZone where UserID = @UserID) {0}
            group by U.FullName, p.PaymentType, p.PaymentDate, pt.Description, p.TranType 
            order by U.FullName";

        if (zoneCode != "")
        {
            sqlCirteria += " and o.ZoneCode = @vZoneCode ";
        }

        if (paymentTypeCode != "")
        {
            sqlCirteria += " and p.PaymentType = @PaymentType ";
        }

        string _sql = string.Format(sql, sqlCirteria).Trim();
        SqlDataAdapter da = new SqlDataAdapter(_sql, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", userID);
        da.SelectCommand.Parameters.AddWithValue("@DateFrom", dateFrom);
        da.SelectCommand.Parameters.AddWithValue("@DateTo", dateTo);
        da.SelectCommand.Parameters.AddWithValue("@PaymentType", paymentTypeCode);
        da.SelectCommand.Parameters.AddWithValue("@vZoneCode", zoneCode);

        try
        {
            da.Fill(dt);
        }
        catch (Exception e)
        {
            dt = null;
            string s = e.Message;
        }
        return dt;
    }



    public DataTable GetCashAmountByUserID(int userID)
    {
        DataTable dt = new DataTable("GetCashAmountByUserID");
        SqlDataAdapter da = new SqlDataAdapter(@"select Z.Description as ZoneName, C.* from Cashs C , Zone Z 
        where  C.ZoneCode = Z.Code and
        C.ZoneCode in (select ZoneCode from UsersZone where UserID = @UserID)", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", userID);       
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public string SendCashTransaction(int pCashID, decimal pAmount, string pSenderNote,
                                      int pCMakerId)
    {
        SqlCommand cmd = new SqlCommand("SendCashTransaction", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pCashID", pCashID);
        cmd.Parameters.AddWithValue("@pAmount", pAmount);
        cmd.Parameters.AddWithValue("@pSenderNote", pSenderNote);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public DataTable GetTransferStatus(int userID)
    {
        DataTable dt = new DataTable("GetTransferStatus");
        SqlDataAdapter da = new SqlDataAdapter(@"select 
            (select Code from Cashs where ID = C.CashID) CashCode,
            (select CashName from Cashs where ID = C.CashID) CashName,

            (select FullName from Users where ID = C.SenderMakerID) SenderMakerName,
            CONVERT(varchar, C.SenderDate, 120) SenderDateFormat,

            (select FullName from Users where ID = C.AcceptorMakerID) AcceptorMakerName,
            CONVERT(varchar, C.AcceptorDate, 120) AcceptorDateFormat,
     
           case C.TransactionStatus when 'PENDING' then N'Gözləmədə'
                         when 'ACCEPT' then N'Qəbul edilib'
						 when 'CANCEL' then N'Ləğv edildi' end as  TransactionStatusDesc,
 
            C.*   
            from CashTransactions C where C.CashID in
        (select ID from Cashs where ZoneCode in (select ZoneCode from UsersZone where USerID = @UserID))
        and (C.TransactionStatus = 'PENDING' or cast(C.SenderDate as Date) >= cast(@SenderDate as Date)) order by C.SenderDate desc", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", userID);
        da.SelectCommand.Parameters.AddWithValue("@SenderDate", Config.HostingTime.AddDays(-7));
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public DataTable GetCashHeadOffice()
    {
        DataTable dt = new DataTable("GetCashHeadOffice");
        SqlDataAdapter da = new SqlDataAdapter(@"select N'Ümumi kassa (Baş ofis)' as CashName, C.* from CashHeadOffice C", SqlConn);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public DataTable GetTransferStatusForAcceptor()
    {
        DataTable dt = new DataTable("GetTransferStatusForAcceptor");
        SqlDataAdapter da = new SqlDataAdapter(@"select 
        (select Code from Cashs where ID = C.CashID) CashCode,
        (select CashName from Cashs where ID = C.CashID) CashName,

        (select FullName from Users where ID = C.SenderMakerID) SenderMakerName,
        (select Picture from Users where ID = C.SenderMakerID) SenderMakerImage,
        CONVERT(varchar, C.SenderDate, 120) SenderDateFormat,

        (select FullName from Users where ID = C.AcceptorMakerID) AcceptorMakerName,
        CONVERT(varchar, C.AcceptorDate, 120) AcceptorDateFormat,
 
        case C.TransactionStatus when 'PENDING' then N'Gözləmədə'
                   when 'ACCEPT' then N'Qəbul edilib'
                   when 'CANCEL' then N'Ləğv edildi' end as  TransactionStatusDesc,
        C.*   
       from CashTransactions C where 
       C.TransactionStatus = 'PENDING' order by C.SenderDate desc", SqlConn);
      
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public decimal GetTransferAmountByCashTranID(int CashTranID)
    {
        SqlCommand cmd = new SqlCommand("select Amount from CashTransactions where ID = @ID", SqlConn);
        cmd.Parameters.AddWithValue("@ID", CashTranID);
        cmd.Connection.Open();
        try { return Config.ToDecimal(Convert.ToString(cmd.ExecuteScalar())); }
        catch { return -1; }
        finally { cmd.Connection.Close(); }
    }



    public string AcceptCashTransfer(int pCashTransferID, string pAcceptorNote, string pTransferStatus, int pCMakerId)
    {
        SqlCommand cmd = new SqlCommand("AcceptCashTransfer", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pCashTransferID", pCashTransferID);
        cmd.Parameters.AddWithValue("@pAcceptorNote", pAcceptorNote);
        cmd.Parameters.AddWithValue("@pTransferStatus", pTransferStatus);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public string SendFromCashTransfer(int pCashHeadOficeID, decimal pTransferAmount, int pCMakerId, string pSenderNote)
    {
        SqlCommand cmd = new SqlCommand("SendFromCashTransfer", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pCashHeadOficeID", pCashHeadOficeID);
        cmd.Parameters.AddWithValue("@pTransferAmount", pTransferAmount);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pSenderNote", pSenderNote);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }
    public DataTable GetPaymentTypeList(int userID)
    {
        DataTable dt = new DataTable("PaymentList");
        SqlDataAdapter da = new SqlDataAdapter(@"SELECT pst.CODE, pst.Description
                FROM UserFunctions	uf
                inner join PaymentsType pst on pst.CODE = substring(function_ID,14,10 )
                WHERE uf.Function_ID  like 'DEBT_PAYMENT%' and uf.User_ID = @UserID ", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", userID);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }
}