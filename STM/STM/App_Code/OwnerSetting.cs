﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public partial class DbProcess : DALC
{
    public string AddOwner(string pFullName, string pPassportNumber, int pContractTypeID, string pContactNumber, string pAddress,
                             string pObjectFullNumber, string pRegDate , string pContractNumber,
                             string pStatus, int pCMakerId)
    {
        SqlCommand cmd = new SqlCommand("AddOwner", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pFullName", pFullName);
        cmd.Parameters.AddWithValue("@pPassportNumber", pPassportNumber);
        cmd.Parameters.AddWithValue("@pContractTypeID", pContractTypeID);
        cmd.Parameters.AddWithValue("@pContactNumber", pContactNumber);
        cmd.Parameters.AddWithValue("@pAddress", pAddress);
        cmd.Parameters.AddWithValue("@pObjectFullNumber", pObjectFullNumber);
        cmd.Parameters.AddWithValue("@pRegDate", pRegDate);
        cmd.Parameters.AddWithValue("@pContractNumber", pContractNumber);
        cmd.Parameters.AddWithValue("@pONotes", "");
        cmd.Parameters.AddWithValue("@pStatus", pStatus);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);



        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public DataTable GetOwnersList(OwnersSearchParams Osp, int UserID, bool isTop, bool ExportExcel, out int dataCount)
    {
        dataCount = 0;
        string sql_query = string.Format(@"select {0} O.* , Ob.ObjectFullNumber      
                                from Owners O left join Objects Ob on O.ObjectID = Ob.ID where
                                Ob.ZoneCode in (select ZoneCode from UsersZone where UserID = @UserID) and
                                O.Status = 'ACTIVE'", isTop ? "top 100" : "");

        if (ExportExcel)
        {
            sql_query = string.Format(@"select {0}
                            Ob.ZoneCode as [Zona kodu],
                            Ob.LineCode as [Sıra kodu],
                            Ob.CorpusCode as [Korpus kodu],
                            O.Fullname as [Soyad Ad],
                            O.PassportNumber as [Ş/v nömrəsi],
                            O.ContactNumber as [Əlaqə nömrəsi],
                            O.Address as [Ünvanı],
                            Ob.ObjectFullNumber as [Obyekt kodu tam],
                            O.RegDate as [Qeydiyyat tarixi],
                            O.ContractNumber as [Müqavilə nömrəsi]
                            from Owners O left join Objects Ob on O.ObjectID = Ob.ID where
                            Ob.ZoneCode in (select ZoneCode from UsersZone where UserID = 1) and
                            O.Status = 'ACTIVE'", isTop ? "top 100" : ""); 
        }


        if (Osp.fullname != "") sql_query += " and lower(O.Fullname) = @pFullname";
        if (Osp.passport_number != "") sql_query += " and lower(O.PassportNumber) = @pPassportNumber";
        if (Osp.contact_number != "") sql_query += " and lower(O.ContactNumber) = @pContactNumber";
        if (Osp.object_number != "") sql_query += " and lower(Ob.ObjectFullNumber) = @pObjectFullNumber";
        if (Osp.reg_date != "") sql_query += " and lower(O.RegDate) = @pRegDate";
        if (Osp.contract_number != "") sql_query += " and lower(O.ContractNumber) = @pContractNumber";
        if (Osp.address != "") sql_query += " and lower(O.Address) = @pAddress";

        sql_query += " order by Ob.ZoneCode,Ob.LineCode,Ob.CorpusCode";

        DataTable dt = new DataTable("GetOwnersList");
        SqlDataAdapter da = new SqlDataAdapter(sql_query, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", UserID);

        if (Osp.fullname != "")        da.SelectCommand.Parameters.AddWithValue("@pFullname", Osp.fullname);
        if (Osp.passport_number != "") da.SelectCommand.Parameters.AddWithValue("@pPassportNumber", Osp.passport_number);
        if (Osp.contact_number != "")  da.SelectCommand.Parameters.AddWithValue("@pContactNumber", Osp.contact_number);
        if (Osp.object_number != "")   da.SelectCommand.Parameters.AddWithValue("@pObjectFullNumber", Osp.object_number);
        if (Osp.reg_date != "")        da.SelectCommand.Parameters.AddWithValue("@pRegDate", Osp.reg_date);
        if (Osp.contract_number != "") da.SelectCommand.Parameters.AddWithValue("@pContractNumber", Osp.contract_number);
        if (Osp.address != "")         da.SelectCommand.Parameters.AddWithValue("@pAddress", Osp.address); 

        try
        {
            da.Fill(dt);
            dataCount = dt.Rows.Count;
        }
        catch
        {
            dt = null;
        }
        return dt;
    }




    public string UpdateOwner(int pOwnerID , string pFullName, string pPassportNumber, int pContractTypeID,
                              string pContactNumber, string pAddress,string pObjectFullNumber, 
                              string pRegDate, string pContractNumber, string pStatus, int pCMakerId)
    {
        SqlCommand cmd = new SqlCommand("UpdateOwner", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pOwnerID", pOwnerID);
        cmd.Parameters.AddWithValue("@pFullName", pFullName);
        cmd.Parameters.AddWithValue("@pPassportNumber", pPassportNumber);
        cmd.Parameters.AddWithValue("@pContractTypeID", pContractTypeID);
        cmd.Parameters.AddWithValue("@pContactNumber", pContactNumber);
        cmd.Parameters.AddWithValue("@pAddress", pAddress);
        cmd.Parameters.AddWithValue("@pObjectFullNumber", pObjectFullNumber);
        cmd.Parameters.AddWithValue("@pRegDate", pRegDate);
        cmd.Parameters.AddWithValue("@pContractNumber", pContractNumber);
        cmd.Parameters.AddWithValue("@pONotes", "");
        cmd.Parameters.AddWithValue("@pStatus", pStatus);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);



        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public DataRow GetOwnerByID(int ownerID)
    {
        DataTable dt = new DataTable("GetOwnerByID");
        SqlDataAdapter da = new SqlDataAdapter(@"select Ow.* , Ob.ObjectFullNumber from Owners Ow left join 
        Objects Ob on Ow.ObjectID = Ob.ID
        where Ow.Status = 'ACTIVE' and Ow.ID = @ID", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ID", ownerID);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt.Rows[0];
    }

    public string DeleteOwner(int pOwnerID, int pCMakerId)
    {
        SqlCommand cmd = new SqlCommand("DeleteOwner", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pOwnerID", pOwnerID);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }
    public DataTable GetContractType()
    {
        DataTable dt = new DataTable("ContrTypes");
        SqlDataAdapter da = new SqlDataAdapter(@"SELECT ID, ContractTypeName FROM ContractTypes ORDER BY ContractTypeName", SqlConn);
        //da.SelectCommand.Parameters.AddWithValue("@ID",ID);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

}