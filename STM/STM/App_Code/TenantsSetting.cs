﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public partial class DbProcess : DALC
{
    public string AddTenant(string pFullname, string pTaxID, string pPassportNumber , string pContactNumber,
                             string pContractNumber, string pAddress, string pRegNumber,
                             string pRegDate, string pStatus, string pTNotes, int pCMakerId)
                            
    {
        SqlCommand cmd = new SqlCommand("AddTenant", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@pFullName", pFullname);
        cmd.Parameters.AddWithValue("@pTaxID", pTaxID);
        cmd.Parameters.AddWithValue("@pPassportNumber", pPassportNumber);
        cmd.Parameters.AddWithValue("@pContactNumber", pContactNumber);
        cmd.Parameters.AddWithValue("@pContractNumber", pContractNumber);
        cmd.Parameters.AddWithValue("@pAddress", pAddress);
        cmd.Parameters.AddWithValue("@pRegNumber", pRegNumber);
        cmd.Parameters.AddWithValue("@pRegDate", pRegDate);
        cmd.Parameters.AddWithValue("@pStatus", pStatus);
        cmd.Parameters.AddWithValue("@pTNotes", pTNotes);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public string UpdateTenant(int pID, string pFullname, string pTaxID, string pPassportNumber, string pContactNumber,
                             string pContractNumber, string pAddress, string pRegNumber,
                             string pRegDate, string pStatus, string pTNotes, int pCMakerId)
    {
        SqlCommand cmd = new SqlCommand("UpdateTenant", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pTenantID", pID);
        cmd.Parameters.AddWithValue("@pFullName", pFullname);
        cmd.Parameters.AddWithValue("@pTaxID", pTaxID);
        cmd.Parameters.AddWithValue("@pPassportNumber", pPassportNumber);
        cmd.Parameters.AddWithValue("@pContactNumber", pContactNumber);
        cmd.Parameters.AddWithValue("@pContractNumber", pContractNumber);
        cmd.Parameters.AddWithValue("@pAddress", pAddress);
        cmd.Parameters.AddWithValue("@pRegNumber", pRegNumber);
        cmd.Parameters.AddWithValue("@pRegDate", pRegDate);
        cmd.Parameters.AddWithValue("@pStatus", pStatus);
        cmd.Parameters.AddWithValue("@pTNotes", pTNotes);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

   

    public DataTable GetTenantsList(TenantsSearchParams Tsp , int UserID ,bool isTop , bool ExportExcel,out int datacount)
    {
        datacount = 0;
        string sql_query = string.Format(@"select  {0}  '' as TenantObjects ,  
                                           t.* from Tenants t left join 
                                             (select RegNumber, max(ObjectID) ObjectID from 
                                               TenantRegs group by RegNumber) t1 on 
                                               t.RegNumber = t1.RegNumber
                                               left join Objects O on t1.ObjectID = O.ID
                                               where 
                                               t.Status = 'ACTIVE' and
                                              (O.ZoneCode in (select ZoneCode from UsersZone where UserID = @UserID) or
                                              t.RegNumber in
                                              (select RegNumber from Tenants where RegNumber not in 
                                                (select RegNumber from TenantRegs)))", isTop ? "top 100" : "");


        if (ExportExcel)
        {
            sql_query = string.Format(@"select {0} 
                                        t.Fullname as [S.A.A],
                                        t.RegNumber as [Qeydiyyat nömrəsi],
                                        t.RegDate as [Qeydiyyat tarixi],
                                        substring(dbo.GetObjectListByRegNumber(t.RegNumber,' , '),3,len(dbo.GetObjectListByRegNumber(t.RegNumber,' , '))) as [Obyekt(lər)],
                                        t.PassportNumber as [Ş/v №],
                                        t.ContactNumber as [Əlaqə №],
                                        t.ContractNumber as [Müqavilə №],
                                        t.Address as [Ünvan]
                                         from Tenants t left join 
                                       (select RegNumber, max(ObjectID) ObjectID from 
                                        TenantRegs group by RegNumber) t1 on t.RegNumber = t1.RegNumber
                                        left join Objects O on t1.ObjectID = O.ID
                                        where 
                                        t.Status = 'ACTIVE' and
                                        (O.ZoneCode in (select ZoneCode from UsersZone where UserID = @UserID) or
                                              t.RegNumber in
                                              (select RegNumber from Tenants where RegNumber not in 
                                                (select RegNumber from TenantRegs)))", isTop ? "top 100" : "");
        }

        if (Tsp.fullname != "") sql_query += " and lower(t.Fullname) like '%' + @pFullname + '%'";
        if (Tsp.passport_number != "") sql_query += " and lower(t.PassportNumber) = @pPassportNumber";
        if (Tsp.contact_number != "") sql_query += " and lower(t.ContactNumber) = @pContactNumber";
        if (Tsp.reg_number != "") sql_query += " and lower(t.RegNumber) like '%' + @pRegNumber + '%'";
        if (Tsp.reg_date != "") sql_query += " and t.RegDate = @pRegDate";
        if (Tsp.contract_number != "") sql_query += " and lower(t.ContractNumber) = @pContractNumber";
        if (Tsp.address != "") sql_query += " and lower(t.Address) like '%' + @pAddress + '%'";

        sql_query += " order by 1 desc";

        DataTable dt = new DataTable("GetTenantsList");
        SqlDataAdapter da = new SqlDataAdapter(sql_query, SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", UserID);

        if (Tsp.fullname != "")         da.SelectCommand.Parameters.AddWithValue("@pFullname", Tsp.fullname);
        if (Tsp.passport_number != "")  da.SelectCommand.Parameters.AddWithValue("@pPassportNumber", Tsp.passport_number);
        if (Tsp.contact_number != "")   da.SelectCommand.Parameters.AddWithValue("@pContactNumber", Tsp.contact_number);
        if (Tsp.reg_number != "")       da.SelectCommand.Parameters.AddWithValue("@pRegNumber", Tsp.reg_number);
        if (Tsp.reg_date != "")         da.SelectCommand.Parameters.AddWithValue("@pRegDate", Tsp.reg_date);
        if (Tsp.contract_number != "")  da.SelectCommand.Parameters.AddWithValue("@pContractNumber", Tsp.contract_number);
        if (Tsp.address != "")          da.SelectCommand.Parameters.AddWithValue("@pAddress", Tsp.address);

        try
        {
            da.Fill(dt);
            datacount = dt.Rows.Count;
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

    public DataRow GetTenantByID(int tenantID)
    {
        DataTable dt = new DataTable("GetTenantByID");
        SqlDataAdapter da = new SqlDataAdapter(@"select dbo.GetObjectListByRegNumber(t.RegNumber, '<br/>') TenantObjects,  t.* from Tenants t where ID = @tenantID", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@tenantID", tenantID);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt.Rows[0];
    }


    public string DeleteTenant(int pTenantID, int pCMakerID)
    {
        SqlCommand cmd = new SqlCommand("DeleteTenant", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@pTenantID", pTenantID);
        cmd.Parameters.AddWithValue("@pCMakerID", pCMakerID);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

    public DataTable GetTenantObjects(string regNumber)
    {
        DataTable dt = new DataTable("GetTenantObjects");
        SqlDataAdapter da = new SqlDataAdapter(@"select t2.ObjectFullNumber, t1.* from 
        TenantRegs t1, Objects t2 where t1.ObjectID = t2.ID
        and t1.RegNumber = @RegNumber", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@RegNumber", regNumber);

        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }



    public string AddTenantReg(string pObjectFullNumber, string pRegNumber, string pType, int pCMakerId)
    {
        SqlCommand cmd = new SqlCommand("AddTenantRegs", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@pObjectFullNumber", pObjectFullNumber);
        cmd.Parameters.AddWithValue("@pRegNumber", pRegNumber);
        cmd.Parameters.AddWithValue("@pType", pType);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }



    public string DeleteTenantReg(string pObjectFullNumber,  int pCMakerId)
    {
        SqlCommand cmd = new SqlCommand("DeleteTenantRegs", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@pObjectFullNumber", pObjectFullNumber);     
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public DataTable GetTenantByObjectId(int objectID)
    {
        DataTable dt = new DataTable("GetTenantByObjectId");
        SqlDataAdapter da = new SqlDataAdapter(@"select t2.ObjectID, 
        (select U.Fullname from Users U where U.ID = t1.CMakerId) MakerName,
        CONVERT(varchar, t1.CMakerDate, 120) CMakerDateFormat,
        t1.* from Tenants t1 , TenantRegs t2 where 
        t1.RegNumber = t2.RegNumber and  t1.Status = 'ACTIVE' and t2.ObjectID = @ObjectID", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ObjectID", objectID);

        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public DataTable GetTenantHistoryByObjectId(int objectID)
    {
        DataTable dt = new DataTable("GetTenantHistoryByObjectId");
        SqlDataAdapter da = new SqlDataAdapter(@"select t.* from (select   t1.Fullname, t1.RegNumber,
        max(t1.RegDate) RegDate,
        max(U.FullName) MakerName,
        max(CONVERT(varchar, t2.CMakerDate, 120)) CMakerDateFormat
        from TenantsHistory t1 inner join TenantRegsHistory t2 on t1.RegNumber = t2.RegNumber
                         left join Users U on t2.CMakerId = U.ID
        where t2.ObjectID = @ObjectID
        group by   t1.Fullname,t1.RegNumber) t order by t.CMakerDateFormat desc", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@ObjectID", objectID);

        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }

}