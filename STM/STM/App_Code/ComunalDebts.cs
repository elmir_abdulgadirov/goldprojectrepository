﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ComunalDebts
/// </summary>
public partial class DbProcess : DALC
{
    public DataTable GetDebtDetailsByCounterNumber(string counterNumber)
    {
        DataTable dt = new DataTable("GetPaymentDetailsById");
        SqlDataAdapter da = new SqlDataAdapter(@"SELECT FORMAT( convert(date,Od.Value_date,120), 'dd-MM-yyyy') AS DebtDate, TR.RegNumber, EC.DebtID, U.FullName, EC.ID, PT.Description  as PType,
		  O.ObjectFullNumber, EC.CounterNumber, EC.ObjectID, EC.Counter_Type, EC.Counter_Value1, EC.Counter_Value2, EC.Calculated_amount
		  FROM ElectricWater_CounterValues EC
		  inner join Objects O ON O.ID = EC.ObjectID
		  inner join TenantRegs TR on TR.ObjectID = O.ID
		  Inner join Object_Debts OD on OD.ID = EC.DebtID
		  inner join Users U on OD.CMakerId = U.ID
		  inner join PaymentsType Pt on OD.Debt_Type = Pt.CODE  
		   WHERE EC.Status = 'ACTIVE' AND EC.CounterNumber = @pCounterNumber", SqlConn);

        da.SelectCommand.Parameters.Add("@pCounterNumber", counterNumber);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }
}