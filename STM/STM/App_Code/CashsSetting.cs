﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public partial class DbProcess : DALC
{
    

    public string AddNewCash(string pCode, string pCashName, string pZoneCode,int pCMakerId)
    {
        SqlCommand cmd = new SqlCommand("AddCash", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pCode", pCode);
        cmd.Parameters.AddWithValue("@pCashName", pCashName);
        cmd.Parameters.AddWithValue("@pZoneCode", pZoneCode);
        cmd.Parameters.AddWithValue("@pCMakerId", pCMakerId);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);


        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);


        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }


    public DataTable GetCashList(int userID)
    {
        DataTable dt = new DataTable("GetCashList");
        SqlDataAdapter da = new SqlDataAdapter(@"select C.*, 
        (select U.Fullname from Users U where U.ID = C.CMakerID) MakerName,
        CONVERT(varchar, C.CMakerDate, 120) CMakerDateFormat,
        (select Z.Description from Zone Z where Z.Code = C.ZoneCode) ZoneName
        from Cashs C  where C.Status = 'ACTIVE' and 
        C.ZoneCode in 
        (select ZoneCode from UsersZone where UserID = @UserID) order by C.CMakerDate desc", SqlConn);
        da.SelectCommand.Parameters.AddWithValue("@UserID", userID);
        try
        {
            da.Fill(dt);
        }
        catch
        {
            dt = null;
        }
        return dt;
    }


    public string DeleteCash(int pCashID, int pCMakerID)
    {
        SqlCommand cmd = new SqlCommand("DeleteCash", SqlConn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@pCashID", pCashID);
        cmd.Parameters.AddWithValue("@pCMakerID", pCMakerID);
        cmd.Parameters.AddWithValue("@pCMakerDate", Config.HostingTime);

        SqlParameter paramResult = new SqlParameter("@pResult", SqlDbType.NVarChar);
        paramResult.Direction = ParameterDirection.Output;
        paramResult.Size = 100;
        cmd.Parameters.Add(paramResult);

        string pResult = "";
        try
        {
            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            pResult = Convert.ToString(cmd.Parameters["@pResult"].Value);
        }
        catch (Exception ex)
        {
            pResult = ex.Message;
        }
        finally
        {
            cmd.Connection.Close();
        }
        return pResult;
    }

   
}