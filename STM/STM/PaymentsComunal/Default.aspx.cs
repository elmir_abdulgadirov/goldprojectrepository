﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

public partial class Payments_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");
        ltrPrintData.Text = "";

        if (!IsPostBack)
        {
            lnkSearchObjectCode.CssClass = "nav-link active";
            lnkSearchZoneCode.CssClass = "nav-link";
            FillZone(Convert.ToInt32(Session["LogonUserID"]));

            if (Request.QueryString["objectcode"] != null)
            {
                string object_code = Convert.ToString(Request.QueryString["objectcode"]);
                string taxID = Convert.ToString(Request.QueryString["TaxID"]);
                txtSearchObjectFullNumber.Text = object_code;
                btnSearchByObjectCode_Click(null, null);
            }
        }


        string FUNCTION_ID = "DEBTS_MAIN_PAGE";

        /*Begin Permission*/
        db.CheckFunctionPermission(Convert.ToInt32(Session["LogonUserID"]), FUNCTION_ID);
        //btnRevAmount.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "REDUCTION_PAYMENT");
        //btnDiscount.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "DISCOUNT_PAYMENT");
        //btnTransferAmount.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "CASH_TO_TRANSFER");
        txtPaymentDate.Enabled = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "PAYMENT_DATE_ENABLE_DISABLE");
        GrdPaymentDetails.Columns[5].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "PRINT_PAYMENT");
        //GrdPaymentDetails.Columns[6].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "DELETE_PAYMENT");


        /*End Permission*/
    }
    protected void lnkSearchObjectCode_Click(object sender, EventArgs e)
    {
        MViewSearch.ActiveViewIndex = 0;
        lnkSearchObjectCode.CssClass = "nav-link active";
        lnkSearchZoneCode.CssClass = "nav-link";
    }
    protected void lnkSearchZoneCode_Click(object sender, EventArgs e)
    {
        txtSearchObjectFullNumber.Text = "";
        MViewSearch.ActiveViewIndex = 1;
        lnkSearchObjectCode.CssClass = "nav-link";
        lnkSearchZoneCode.CssClass = "nav-link active";
    }

    void FillZone(int userID)
    {
        DataTable dtZone = db.GetZoneByAllowedUser(userID);
        drlZone.DataSource = dtZone;
        drlZone.DataBind();
        drlZone.Items.Insert(0, new ListItem("--Zona seçin", ""));
        drlLine.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpus.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }
    void FillLine(string ZoneID)
    {
        DataTable dtLine = db.GetAllLines(ZoneID);
        drlLine.DataSource = dtLine;
        drlLine.DataBind();
        drlLine.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpus.Items.Clear();
        drlCorpus.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }

    void FillCorpus(string ZoneID, string LineID)
    {
        DataTable dtCorpus = db.GetAllCorpus(ZoneID, LineID);
        drlCorpus.DataSource = dtCorpus;
        drlCorpus.DataBind();
        drlCorpus.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }
    protected void drlZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillLine(Convert.ToString(drlZone.SelectedValue));
    }
    protected void drlLine_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCorpus(Convert.ToString(drlZone.SelectedValue), Convert.ToString(drlLine.SelectedValue));
    }



    void FillDebtByParam(int UserID, string pObjectFullNumber, string pRegNumber, string pTaxID, string pDebtDate,
                         string pZoneCode, string pLineCode, string pCorpusCode)
    {
        ltrTotalDebt.Text = "";
        ltrTenantData.Text = "";
        string pTenantData = "";
        DataTable dt = new DataTable("FillDebtByParam");
        dt = db.GetDebtsByParams(UserID, pObjectFullNumber, pRegNumber, pTaxID, pDebtDate, pZoneCode, pLineCode,
             pCorpusCode, out pTenantData);
        GrdObjectDebt.DataSource = dt;
        GrdObjectDebt.DataBind();
        GrdObjectDebt.UseAccessibleHeader = true;
        if (GrdObjectDebt.Rows.Count > 0) GrdObjectDebt.HeaderRow.TableSection = TableRowSection.TableHeader;

        decimal debt_total = 0.0m, paid_total = 0.0m, remain_total = 0.0m, remain_VAT_total = 0.0m, discount_total = 0.0m, reduction_total = 0.0m;
        for (int i = 0; i < GrdObjectDebt.Rows.Count; i++)
        {
            debt_total += Config.ToDecimal(Convert.ToString(dt.Rows[i]["Debt_Amount"]));
            paid_total += Config.ToDecimal(Convert.ToString(dt.Rows[i]["Paid_Amount"]));
            //discount_total += Config.ToDecimal(Convert.ToString(dt.Rows[i]["Discount_Amount"]));
            //reduction_total += Config.ToDecimal(Convert.ToString(dt.Rows[i]["Reducton_Amount"]));
            remain_total += Config.ToDecimal(Convert.ToString(dt.Rows[i]["Remain_Amount"]));
            //remain_VAT_total += Config.ToDecimal(Convert.ToString(dt.Rows[i]["Remain_VatAmount"]));*/
        }

        if (dt.Rows.Count == 0)
        {
            ltrAlertMsg.Text = Alert.WarningMessage("Məlumat tapılmadı.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }
        ltrTenantData.Text = pTenantData.ToString();
        ltrTotalDebt.Text = string.Format("<span class=\"badge badge-danger\"> </span>ÜMUMİ BORC: {0}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                                         "<span class=\"badge badge-success\" style = \"margin-top:5px\"> </span>ÜMUMİ ÖDƏMƏ: {1}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                                         "<span class=\"badge badge-warning\" style = \"margin-top:5px\"> </span>ÜMUMİ QALIQ: {2}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" 
                                         , debt_total, paid_total, remain_total);

        /*Begin permission*/
        if (GrdObjectDebt.Rows.Count > 0)
        {
            GrdObjectDebt.Columns[4].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "ADD_NEW_PAYMENT");
        }
        /*End permission*/

    }

    protected void btnSearchByObjectCode_Click(object sender, EventArgs e)
    {

        ltrTotalDebt.Text = "";
        ltrTenantData.Text = "";
        GrdObjectDebt.DataSource = null;
        GrdObjectDebt.DataBind();
        if (txtSearchObjectFullNumber.Text.Trim().Replace(" ", "").Length > 0)
        {
            if (!Config.CheckObjectCode(txtSearchObjectFullNumber.Text.Trim().Replace(" ", "")))
            {
                ltrAlertMsg.Text = Alert.DangerMessage("Obyekt kodunu düzgün daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                return;
            }
        }
        if (txtSearchObjectFullNumber.Text.Trim().Length == 0 && txtSearchCustomerID.Text.Trim().Length == 0 )
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Axtaış parametrləri daxil edilməyib!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }
        Session["SEARCH_DEBT_CMEKER_ID"] = Convert.ToInt32(Session["LogonUserID"]);
        Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"] = txtSearchObjectFullNumber.Text.Trim().Replace(" ", "");
        Session["SEARCH_DEBT_CUSTOMER_ID"] = txtSearchCustomerID.Text.Trim().Replace(" ", "");
        Session["SEARCH_DEBT_DATE"] = txtDebtDate.Text.Trim().Replace(" ", "");
        Session["SEARCH_DEBT_ZONE_CODE"] = "";
        Session["SEARCH_DEBT_LINE_CODE"] = "";
        Session["SEARCH_DEBT_CORPUS_CODE"] = "";

        FillDebtByParam(Convert.ToInt32(Session["SEARCH_DEBT_CMEKER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"]),
                        Convert.ToString(Session["SEARCH_DEBT_CUSTOMER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_TAXID"]),
                        Convert.ToString(Session["SEARCH_DEBT_DATE"]),
                        Convert.ToString(Session["SEARCH_DEBT_ZONE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_LINE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_CORPUS_CODE"])); ;
    }

    protected void lnkPay_Click(object sender, EventArgs e)
    {
        ltrObjPayMessage.Text = "";
        txtPayAmount.Text = "0";
        txtPaymentDate.Text = DateTime.Now.ToString("dd.MM.yyyy");

        LinkButton lnk = (LinkButton)sender;
        int debt_id = Convert.ToInt32(lnk.CommandArgument);
        Session["pay_debt_id"] = debt_id;

        txtRemainDebt.Text = db.GetRemainDebtByDebtID(debt_id).Rows[0][0].ToString();

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectDebtModal();", true);
    }

    protected void btnPay_Click(object sender, EventArgs e)
    {
        ltrObjPayMessage.Text = "";
        if (Session["pay_debt_id"] == null) Config.Rd("/exit");


        if (Session["SEARCH_DEBT_CMEKER_ID"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_ZONE_CODE"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_LINE_CODE"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_CORPUS_CODE"] == null) Config.Rd("/exit");

        int debt_id = Convert.ToInt32(Session["pay_debt_id"]);
        decimal pay_amount = 0, remain_amount = 0, pay_vat_amount = 0, remain_vat_amount = 0;
        DateTime paymentDate;
        if (!Config.isDate(txtPaymentDate.Text))
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Tarix formatını düzgün daxil edin ! (məs : 22.10.2015)");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        paymentDate = DateTime.ParseExact(txtPaymentDate.Text, "dd.MM.yyyy", CultureInfo.InvariantCulture);
        try { pay_amount = Config.ToDecimal(txtPayAmount.Text.Trim()); }
        catch
        {
            ltrObjPayMessage.Text = Alert.DangerMessage("Ödəniş məbləğini düzgün daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectDebtModal();", true);
            return;
        }

        try { remain_amount = Config.ToDecimal(txtRemainDebt.Text.Trim()); }
        catch
        {
            ltrObjPayMessage.Text = Alert.DangerMessage("Qalıq borc düzgün deyil");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectDebtModal();", true);
            return;
        }

        /*if (pay_amount > remain_amount)
        {
            ltrObjPayMessage.Text = Alert.WarningMessage("Ödəniləcək məbləğ qalıq borc məbləğindən böyük olmaz.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectDebtModal();", true);
            return;
        }*/

        
        /*if (pay_vat_amount > remain_vat_amount)
        {
            ltrObjPayMessage.Text = Alert.WarningMessage("Ödəniləcək ƏDV məbləğ qalıq ƏDV məbləğindən böyük ola bilmiəz!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectDebtModal();", true);
            return;
        }*/

        string result = db.CreatePayment(paymentDate, debt_id, pay_amount, pay_vat_amount, Convert.ToInt32(Session["LogonUserID"]));
        if (!result.Equals("OK"))
        {
            ltrObjPayMessage.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectDebtModal();", true);
            return;
        }

        if (result.Equals("OK"))
        {
            ltrAlertMsg.Text = Alert.SuccessMessage("Ödəniş qəbul edildi");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }

        FillDebtByParam(Convert.ToInt32(Session["SEARCH_DEBT_CMEKER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"]),
                        Convert.ToString(Session["SEARCH_DEBT_CUSTOMER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_TAXID"]),
                        Convert.ToString(Session["SEARCH_DEBT_DATE"]),
                        Convert.ToString(Session["SEARCH_DEBT_ZONE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_LINE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_CORPUS_CODE"]));

    }
    /*protected void btnRevAmount_Click(object sender, EventArgs e)
    {
        ltrObjPayMessage.Text = "";
        if (Session["pay_debt_id"] == null) Config.Rd("/exit");


        if (Session["SEARCH_DEBT_CMEKER_ID"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_ZONE_CODE"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_LINE_CODE"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_CORPUS_CODE"] == null) Config.Rd("/exit");

        int debt_id = Convert.ToInt32(Session["pay_debt_id"]);
        decimal rev_amount = 0, rev_vat_amount = 0;
        

        string result = db.CreateReductionPayment(debt_id, rev_amount, rev_vat_amount, Convert.ToInt32(Session["LogonUserID"]));
        if (!result.Equals("OK"))
        {
            ltrObjPayMessage.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectDebtModal();", true);
            return;
        }

        if (result.Equals("OK"))
        {
            ltrAlertMsg.Text = Alert.SuccessMessage("Silinmə icra edildi");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }

        FillDebtByParam(Convert.ToInt32(Session["SEARCH_DEBT_CMEKER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"]),
                        Convert.ToString(Session["SEARCH_DEBT_CUSTOMER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_TAXID"]),
                        Convert.ToString(Session["SEARCH_DEBT_DATE"]),
                        Convert.ToString(Session["SEARCH_DEBT_ZONE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_LINE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_CORPUS_CODE"]));

    }
    protected void btnDiscount_Click(object sender, EventArgs e)
    {
        ltrObjPayMessage.Text = "";
        if (Session["pay_debt_id"] == null) Config.Rd("/exit");


        if (Session["SEARCH_DEBT_CMEKER_ID"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_ZONE_CODE"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_LINE_CODE"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_CORPUS_CODE"] == null) Config.Rd("/exit");

        int debt_id = Convert.ToInt32(Session["pay_debt_id"]);
        decimal discount_amount = 0, discount_vat_amount = 0;


        string result = db.CreateDiscountPayment(debt_id, discount_amount, discount_vat_amount, Convert.ToInt32(Session["LogonUserID"]));
        if (!result.Equals("OK"))
        {
            ltrObjPayMessage.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectDebtModal();", true);
            return;
        }

        if (result.Equals("OK"))
        {
            ltrAlertMsg.Text = Alert.SuccessMessage("Güzəşt icra edildi");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }

        FillDebtByParam(Convert.ToInt32(Session["SEARCH_DEBT_CMEKER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"]),
                        Convert.ToString(Session["SEARCH_DEBT_CUSTOMER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_TAXID"]),
                        Convert.ToString(Session["SEARCH_DEBT_DATE"]),
                        Convert.ToString(Session["SEARCH_DEBT_ZONE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_LINE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_CORPUS_CODE"]));


        if (result.Equals("OK"))
        {
            ltrAlertMsg.Text = Alert.SuccessMessage("Güzəşt icra edildi");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }

        FillDebtByParam(Convert.ToInt32(Session["SEARCH_DEBT_CMEKER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"]),
                        Convert.ToString(Session["SEARCH_DEBT_CUSTOMER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_TAXID"]),
                        Convert.ToString(Session["SEARCH_DEBT_DATE"]),
                        Convert.ToString(Session["SEARCH_DEBT_ZONE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_LINE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_CORPUS_CODE"]));

    }
    protected void btnTransferAmount_Click(object sender, EventArgs e)
    {
        ltrObjPayMessage.Text = "";
        if (Session["pay_debt_id"] == null) Config.Rd("/exit");


        if (Session["SEARCH_DEBT_CMEKER_ID"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_ZONE_CODE"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_LINE_CODE"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_CORPUS_CODE"] == null) Config.Rd("/exit");

        int debt_id = Convert.ToInt32(Session["pay_debt_id"]);
        decimal transfer_Amount = 0, transfer_Vat_Amount = 0;


        string result = db.CreateTransferPayment(debt_id, transfer_Amount, transfer_Vat_Amount, Convert.ToInt32(Session["LogonUserID"]));
        if (!result.Equals("OK"))
        {
            ltrObjPayMessage.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectDebtModal();", true);
            return;
        }

        if (result.Equals("OK"))
        {
            ltrAlertMsg.Text = Alert.SuccessMessage("Transfer icra edildi");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }

        FillDebtByParam(Convert.ToInt32(Session["SEARCH_DEBT_CMEKER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"]),
                        Convert.ToString(Session["SEARCH_DEBT_CUSTOMER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_TAXID"]),
                        Convert.ToString(Session["SEARCH_DEBT_DATE"]),
                        Convert.ToString(Session["SEARCH_DEBT_ZONE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_LINE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_CORPUS_CODE"]));


        if (result.Equals("OK"))
        {
            ltrAlertMsg.Text = Alert.SuccessMessage("Transfer icra edildi");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }

        FillDebtByParam(Convert.ToInt32(Session["SEARCH_DEBT_CMEKER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"]),
                        Convert.ToString(Session["SEARCH_DEBT_CUSTOMER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_TAXID"]),
                        Convert.ToString(Session["SEARCH_DEBT_DATE"]),
                        Convert.ToString(Session["SEARCH_DEBT_ZONE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_LINE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_CORPUS_CODE"]));

    }*/
    protected void btnSearchByZoneCode_Click(object sender, EventArgs e)
    {
        ltrTotalDebt.Text = "";
        GrdObjectDebt.DataSource = null;
        GrdObjectDebt.DataBind();
        if (drlZone.SelectedValue == "")
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Zona seçin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }


        Session["SEARCH_DEBT_CMEKER_ID"] = Convert.ToInt32(Session["LogonUserID"]);
        Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"] = "";
        Session["SEARCH_DEBT_CUSTOMER_ID"] = "";
        Session["SEARCH_DEBT_TAXID"] = "";
        Session["SEARCH_DEBT_ZONE_CODE"] = Convert.ToString(drlZone.SelectedValue);
        Session["SEARCH_DEBT_LINE_CODE"] = Convert.ToString(drlLine.SelectedValue);
        Session["SEARCH_DEBT_CORPUS_CODE"] = Convert.ToString(drlCorpus.SelectedValue);

        FillDebtByParam(Convert.ToInt32(Session["SEARCH_DEBT_CMEKER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"]),
                        Convert.ToString(Session["SEARCH_DEBT_CUSTOMER_ID"]),
                        Convert.ToString(Session["SEARCH_DEBT_TAXID"]),
                        Convert.ToString(Session["SEARCH_DEBT_DATE"]),
                        Convert.ToString(Session["SEARCH_DEBT_ZONE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_LINE_CODE"]),
                        Convert.ToString(Session["SEARCH_DEBT_CORPUS_CODE"]));
    }

    protected void lnkPaymentDetails_Click(object sender, EventArgs e)
    {
        ltrModalPaymentDetails.Text = "";
        LinkButton lnk = (LinkButton)sender;
        int debt_id = Convert.ToInt32(lnk.CommandArgument);
        FillPaymentDetails(debt_id);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openPaymentDetailsModal();", true);
    }

    void FillPaymentDetails(int debt_id)
    {
        ltrModalPaymentDetails.Text = "";
        DataTable dt = new DataTable("FillPaymentDetails");
        dt = db.GetPaymentDetailsByDebtId(debt_id);
        GrdPaymentDetails.DataSource = dt;
        GrdPaymentDetails.DataBind();
        GrdPaymentDetails.UseAccessibleHeader = true;
        if (GrdPaymentDetails.Rows.Count > 0) GrdPaymentDetails.HeaderRow.TableSection = TableRowSection.TableHeader;

        /*Begin permission*/
        if (GrdPaymentDetails.Rows.Count > 0)
        {
            GrdPaymentDetails.Columns[3].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "PRINT_PAYMENT");
           // GrdPaymentDetails.Columns[4].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "DELETE_PAYMENT");
        }
        /*End permission*/
    }

    protected void lnkPaymentDelete_Click(object sender, EventArgs e)
    {
        if (Session["SEARCH_DEBT_CMEKER_ID"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_ZONE_CODE"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_LINE_CODE"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_CORPUS_CODE"] == null) Config.Rd("/exit");

        LinkButton lnk = (LinkButton)sender;
        int payment_id = Convert.ToInt32(lnk.CommandArgument);

        string result = db.DeletePayment(payment_id, Convert.ToInt32(Session["LogonUserID"]));

        if (!result.Equals("OK"))
        {
            ltrModalPaymentDetails.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openPaymentDetailsModal();", true);
            return;
        }

        FillDebtByParam(Convert.ToInt32(Session["SEARCH_DEBT_CMEKER_ID"]),
                       Convert.ToString(Session["SEARCH_DEBT_OBJECT_FULL_NUMBER"]),
                       Convert.ToString(Session["SEARCH_DEBT_CUSTOMER_ID"]),
                       Convert.ToString(Session["SEARCH_DEBT_TAXID"]),
                       Convert.ToString(Session["SEARCH_DEBT_DATE"]),
                       Convert.ToString(Session["SEARCH_DEBT_ZONE_CODE"]),
                       Convert.ToString(Session["SEARCH_DEBT_LINE_CODE"]),
                       Convert.ToString(Session["SEARCH_DEBT_CORPUS_CODE"]));


        if (result.Equals("OK"))
        {
            ltrAlertMsg.Text = Alert.SuccessMessage("Ödəniş silindi.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }
    }
    protected void lnkPaymentPrint_Click(object sender, EventArgs e)
    {
        ltrPrintData.Text = "";
        LinkButton lnk = (LinkButton)sender;
        int payment_id = Convert.ToInt32(lnk.CommandArgument);

        DataTable dt = db.GetPaymentDetailsById(payment_id);
        if (dt == null || dt.Rows.Count == 0)
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Çap prosesində xəta baş verdi.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }
        DataRow dr = dt.Rows[0];

        string print_format = "<div class=\"card\" id=\"payment_print\">" +
                                                "<h6 class=\"card-header\" style =\"text-align:center\">" + Convert.ToString(dr["PType"]) + " ödənişi</h5>" +
                                                "<div class=\"card-body\">" +
                                                    "<h6 class=\"card-body-title\">Müştəri İD: " + Convert.ToString(dr["RegNumber"]) + "</h6>" +
                                                    "<h6 class=\"card-body-title\">Obyekt kodu: " + Convert.ToString(dr["ObjectFullNumber"]) + "</h6>" +
                                                    "<h6 class=\"mg-b-0\">Tarix/Saat: " + Convert.ToString(Config.HostingTime.ToString("yyyy-MM-dd hh:mm:ss")) + "</h6>" +
                                                    "<hr />" +
                                                    "<table class=\"table payment_print_tab\">" +
                                                        "<tbody>" +
                                                            "<tr>" +
                                                                "<td>" +
                                                                    "Ödənilən məbləğ:" +
                                                                "</td>" +
                                                                "<td>" +
                                                                    "" + Convert.ToString(dr["PaymentAmount"]) + " AZN" +
                                                                "</td>" +
                                                            "</tr>" +
                                                            /*  "<tr>" +
                                                                  "<td>" +
                                                                      "<h6 class=\"tx-inverse mg-b-0\">Qalıq borc:</h6>" +
                                                                  "</td>" +
                                                                  "<td>" +
                                                                      "<h6 class=\"tx-inverse mg-b-0\">" + Convert.ToString(dr["Remain_Amount"]) + " AZN</h6>" +
                                                                  "</td>" +
                                                              "</tr>" +*/
                                                            "<tr>" +
                                                                "<td>" +
                                                                    "Ödənişi qəbul edən:" +
                                                                "</td>" +
                                                                "<td>" +
                                                                     Convert.ToString(dr["Fullname"]) +
                                                                "</td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td>" +
                                                                    "Ödəniş tarixi:" +
                                                                "</td>" +
                                                                "<td>" +
                                                                    "" + Convert.ToString(dr["PaymentDate"]) + "" +
                                                                "</td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td>" +
                                                                    "Borc tarixi:" +
                                                                "</td>" +
                                                                "<td>" +
                                                                    "" + Convert.ToString(dr["Debt_Date"]) + "" +
                                                                "</td>" +
                                                            "</tr>" +
                                                        "</tbody>" +
                                                    "</table>" +
                                                "</div>" +
                                                "<style>" +
                                                ".card {border: 1px solid #dadada;}" +
                                                "</style>" +
                                               "</div>";
        ltrPrintData.Text = print_format;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "PrintPayment();", true);
    }
}