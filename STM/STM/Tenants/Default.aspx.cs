﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Tenants_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");
       
        if (!IsPostBack)
        {
            if (Session["TenantsSearchParams"] != null)
            {
                Session["TenantsSearchParams"] = null;
                Session.Remove("TenantsSearchParams");
            }
            TenantsSearchParams Tsp = new TenantsSearchParams();
            FillTenants(Tsp, Convert.ToInt32(Session["LogonUserID"]),true,false);
        }

        
        string FUNCTION_ID = "OBJECT_TENANTS_MAIN_PAGE";

        /*Begin Permission*/
        db.CheckFunctionPermission(Convert.ToInt32(Session["LogonUserID"]), FUNCTION_ID);

        lnkAddNewTenant.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "TENANT_ADD_NEW");
        lnkDownloadExcel.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "TENANT_DOWNLOAD_EXCEL");
        lnkTenantSearch.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "TENANT_SEARCH");

  
        
    
        if (GrdTenants.Rows.Count > 0)
        {
            GrdTenants.Columns[6].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "TENANT_ADD_TO_OBJECTS");
            GrdTenants.Columns[7].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "TENANT_CHANGE_DATA");
            GrdTenants.Columns[8].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "TENANT_DELETE");
        }


        /*End Permission*/
       
    }

    void FillTenants(TenantsSearchParams Tsp , int UserID , bool isTop , bool ExportExcel)
    {
        int dataCount;
        DataTable dtTenants = db.GetTenantsList(Tsp,UserID,isTop,ExportExcel,out dataCount);
        if (!ExportExcel)
        {
            GrdTenants.DataSource = dtTenants;
            GrdTenants.DataBind();
            GrdTenants.UseAccessibleHeader = true;
            if (GrdTenants.Rows.Count > 0) GrdTenants.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        if (ExportExcel)
        {
            ExportExcel export = new ExportExcel();
            export.ExportToExcel(dtTenants, "İcarəçilər", "Icarechiler");
        }
        ltrDataCount.Text = string.Format("<span>Məlumat sayı: <b>{0}</b></span>", dataCount);
       
    }

    

    protected void btnNewTenantAccept_Click(object sender, EventArgs e)
    {
        ltrModalAddTenant.Text = "";
        if (txtNewTenantFullName.Text.Trim().Length < 5)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddTenantModal();", true);
            ltrModalAddTenant.Text = Alert.DangerMessage("İcarəçinin soyad,ad,ata adını daxil edin!");
            return;
        }

        if (txtNewTenantRegNumber.Text.Trim().Length == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddTenantModal();", true);
            ltrModalAddTenant.Text = Alert.DangerMessage("İcarəçinin qeydiyyat nömrəsini daxil edin!");
            return;
        }


        if (!Config.isDate(txtNewTenantRegDate.Text))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddTenantModal();", true);
            ltrModalAddTenant.Text = Alert.DangerMessage("Qeydiyyat tarixini düzgün daxil edin! (Misal: 22.10.2015)");
            return;
        }

        string result = db.AddTenant(txtNewTenantFullName.Text.Trim(),
                                     txtNewTaxID.Text.Trim(),
                                     txtNewTenantPassportNumber.Text.Trim(),
                                     txtNewTenantContactNumber.Text.Trim(),
                                     txtNewTenantContractNumber.Text.Trim(),
                                     txtNewTenantAddress.Text.Trim(),
                                     txtNewTenantRegNumber.Text.Trim(),
                                     txtNewTenantRegDate.Text.Trim(),
                                     "ACTIVE",
                                     "",
                                     Convert.ToInt32(Session["LogonUserID"]));

        if (!result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddTenantModal();", true);
            ltrModalAddTenant.Text = Alert.DangerMessage("Xəta : " + result);
            return;
        }
        Config.Rd("/tenants");
    }
    protected void lnkTenantEdit_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        int tenantID = Convert.ToInt32(lnk.CommandArgument);
        Session["selected_tenantId"] = tenantID;
        ltrModalEditTenant.Text = "";
        DataRow drOwner = db.GetTenantByID(tenantID);

        txtEditTenantFullName.Text = Convert.ToString(drOwner["Fullname"]);
        txtEditTaxID.Text = Convert.ToString(drOwner["TaxID"]);
        txtEditTenantPassportNumber.Text = Convert.ToString(drOwner["PassportNumber"]);
        txtEditTenantContactNumber.Text = Convert.ToString(drOwner["ContactNumber"]);
        txtEditTenantRegNumber.Text = Convert.ToString(drOwner["RegNumber"]);
        txtEditTenantRegDate.Text = Convert.ToString(drOwner["RegDate"]);
        txtEditTenantContractNumber.Text = Convert.ToString(drOwner["ContractNumber"]);
        txtEditTenantAddress.Text = Convert.ToString(drOwner["Address"]);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditTenantModal();", true); 
    }
   
    protected void btnEditTenantAccept_Click(object sender, EventArgs e)
    {
        if (Session["selected_tenantId"] == null) Config.Rd("/exit");
        int tenantID = Convert.ToInt32(Session["selected_tenantId"]);

        ltrModalEditTenant.Text = "";
        if (txtEditTenantFullName.Text.Trim().Length < 5)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditTenantModal();", true);
            ltrModalEditTenant.Text = Alert.DangerMessage("İcarəçinin soyad,ad,ata adını daxil edin!");
            return;
        }

        if (txtEditTenantRegNumber.Text.Trim().Length == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditTenantModal();", true);
            ltrModalEditTenant.Text = Alert.DangerMessage("İcarəçinin qeydiyyat nömrəsini daxil edin!");
            return;
        }


        if (!Config.isDate(txtEditTenantRegDate.Text))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditTenantModal();", true);
            ltrModalEditTenant.Text = Alert.DangerMessage("Qeydiyyat tarixini düzgün daxil edin! (Misal: 22.10.2015)");
            return;
        }

        string result = db.UpdateTenant(tenantID,
                                     txtEditTenantFullName.Text.Trim(),
                                     txtEditTaxID.Text.Trim(),
                                     txtEditTenantPassportNumber.Text.Trim(),
                                     txtEditTenantContactNumber.Text.Trim(),
                                     txtEditTenantContractNumber.Text.Trim(),
                                     txtEditTenantAddress.Text.Trim(),
                                     txtEditTenantRegNumber.Text.Trim(),
                                     txtEditTenantRegDate.Text.Trim(),
                                     "ACTIVE",
                                     "",
                                     Convert.ToInt32(Session["LogonUserID"]));
        if (!result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditTenantModal();", true);
            ltrModalEditTenant.Text = Alert.DangerMessage("Xəta : " + result);
            return;
        }

        if (result.Equals("OK"))
        {
            TenantsSearchParams Tsp = new TenantsSearchParams();
            if (Session["TenantsSearchParams"] != null) Tsp = (TenantsSearchParams)Session["TenantsSearchParams"];

            FillTenants(Tsp, Convert.ToInt32(Session["LogonUserID"]), Session["TenantsSearchParams"] != null ? false : true ,false);

            ltrAlertMsg.Text = Alert.SuccessMessage("Məlumat yeniləndi");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }
    }
    protected void btnDeleteTenant_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        int tenantID = Convert.ToInt32(btn.CommandArgument);
        string result = db.DeleteTenant(tenantID, Convert.ToInt32(Session["LogonUserID"]));
        if (result.Equals("OK"))
        {
            TenantsSearchParams Tsp = new TenantsSearchParams();
            if (Session["TenantsSearchParams"] != null) Tsp = (TenantsSearchParams)Session["TenantsSearchParams"];

            FillTenants(Tsp, Convert.ToInt32(Session["LogonUserID"]), Session["TenantsSearchParams"] != null ? false : true, false);
           
            ltrAlertMsg.Text = Alert.SuccessMessage("Silindi");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }
        else
        {
            TenantsSearchParams Tsp = new TenantsSearchParams();
            if (Session["TenantsSearchParams"] != null) Tsp = (TenantsSearchParams)Session["TenantsSearchParams"];

            FillTenants(Tsp, Convert.ToInt32(Session["LogonUserID"]), Session["TenantsSearchParams"] != null ? false : true, false);

            ltrAlertMsg.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }
    }
    protected void lnkTenantObjectSetting_Click(object sender, EventArgs e)
    {
        txtTenantObjectCode.Text = "";
        drlTenantType.SelectedValue = "0";
        ltrModalTenantObjectSetting.Text = "";
        LinkButton lnk = (LinkButton)sender;
        string regNumber = Convert.ToString(lnk.CommandArgument);
        Session["selected_regNumber"] = regNumber; 
        FillTenantObjects(regNumber);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openTenantObjectModal();", true);
    }

    void FillTenantObjects(string regNumber)
    {
        DataTable dtTenantObjets = db.GetTenantObjects(regNumber);
        GrdTenantObjects.DataSource = dtTenantObjets;
        GrdTenantObjects.DataBind();
        GrdTenantObjects.UseAccessibleHeader = true;
        if (GrdTenantObjects.Rows.Count > 0) GrdTenantObjects.HeaderRow.TableSection = TableRowSection.TableHeader;
    }
    
    protected void lnkTenantObjectAdd_Click(object sender, EventArgs e)
    {
        if (Session["selected_regNumber"] == null) Config.Rd("/exit");
        string regNumber = Convert.ToString(Session["selected_regNumber"]);

        ltrModalTenantObjectSetting.Text = "";
        if (txtTenantObjectCode.Text.Trim().Length < 5)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openTenantObjectModal();", true);
            ltrModalTenantObjectSetting.Text = Alert.DangerMessage("Obyekt kodunu düzgün daxil edin!");
            return;
        }

        if (drlTenantType.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openTenantObjectModal();", true);
            ltrModalTenantObjectSetting.Text = Alert.DangerMessage("Tipi seçin!");
            return;
        }

        string result = db.AddTenantReg(txtTenantObjectCode.Text.Trim(),
                                         regNumber,
                                         drlTenantType.SelectedValue,
                                         Convert.ToInt32(Session["LogonUserID"]));

        if (!result.Equals("OK"))
        {
            FillTenantObjects(regNumber);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openTenantObjectModal();", true);
            ltrModalTenantObjectSetting.Text = Alert.DangerMessage("Xəta : " + result);
            return;
        }

        if (result.Equals("OK"))
        {
            FillTenantObjects(regNumber);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openTenantObjectModal();", true);
        }
        txtTenantObjectCode.Text = "";
        drlTenantType.SelectedValue = "0";

        TenantsSearchParams Tsp = new TenantsSearchParams();
        if (Session["TenantsSearchParams"] != null) Tsp = (TenantsSearchParams)Session["TenantsSearchParams"];

        FillTenants(Tsp, Convert.ToInt32(Session["LogonUserID"]), Session["TenantsSearchParams"] != null ? false : true, false);
    }
    protected void lnkTenantObjectDelete_Click(object sender, EventArgs e)
    {
        if (Session["selected_regNumber"] == null) Config.Rd("/exit");
        txtTenantObjectCode.Text = "";
        drlTenantType.SelectedValue = "0";
        ltrModalTenantObjectSetting.Text = "";
        LinkButton lnk = (LinkButton)sender;
        string ObjectFullNumbe = Convert.ToString(lnk.CommandArgument);
        string regNumber = Convert.ToString(Session["selected_regNumber"]);

        string result = db.DeleteTenantReg(ObjectFullNumbe, Convert.ToInt32(Session["LogonUserID"]));

        if (!result.Equals("OK"))
        {
            FillTenantObjects(regNumber);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openTenantObjectModal();", true);
            ltrModalTenantObjectSetting.Text = Alert.DangerMessage("Xəta : " + result);
            return;
        }

        if (result.Equals("OK"))
        {
            FillTenantObjects(regNumber);

            TenantsSearchParams Tsp = new TenantsSearchParams();
            if (Session["TenantsSearchParams"] != null) Tsp = (TenantsSearchParams)Session["TenantsSearchParams"];

            FillTenants(Tsp, Convert.ToInt32(Session["LogonUserID"]), Session["TenantsSearchParams"] != null ? false : true, false);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openTenantObjectModal();", true);
        }      
        
    }
    protected void GrdTenantObjects_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lb = e.Row.FindControl("lnkTenantObjectDelete") as LinkButton;
            ScriptManager.GetCurrent(this).RegisterPostBackControl(lb);
        }
    }
    protected void lnkDownloadExcel_Click(object sender, EventArgs e)
    {
        TenantsSearchParams Tsp = new TenantsSearchParams();
        if (Session["TenantsSearchParams"] != null) Tsp = (TenantsSearchParams)Session["TenantsSearchParams"];
        FillTenants(Tsp, Convert.ToInt32(Session["LogonUserID"]), Session["TenantsSearchParams"] != null ? false : true, true);
    }
    
    protected void btnSearchTenant_Click(object sender, EventArgs e)
    {
        if (txtSearchTenantRegDate.Text.Trim() != "")
        {
            if (!Config.isDate(txtSearchTenantRegDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openTenantSearchModal();", true);
                ltrModalSearchTenant.Text = Alert.DangerMessage("Qeydiyyat tarixini düzgün daxil edin! (Misal: 22.10.2015)");
                return;
            }
        }

        TenantsSearchParams Tsp = new TenantsSearchParams();
        Tsp.fullname = txtSearchTenantFullName.Text.Trim().Replace("İ","i"). ToLower();
        Tsp.passport_number = txtSearchTenantPassportNumber.Text.Trim().ToLower();
        Tsp.contact_number = txtSearchTenantContactNumber.Text.Trim();
        Tsp.reg_number = txtSearchTenantRegNumber.Text.Trim();
        Tsp.reg_date = txtSearchTenantRegDate.Text.Trim();
        Tsp.contract_number = txtSearchTenantContractNumber.Text.Trim().ToLower();
        Tsp.address = txtSearchTenantAddress.Text.Trim().Replace("İ", "i").ToLower();

        Session["TenantsSearchParams"] = Tsp;

        FillTenants(Tsp, Convert.ToInt32(Session["LogonUserID"]), false, false);
    }

   
}