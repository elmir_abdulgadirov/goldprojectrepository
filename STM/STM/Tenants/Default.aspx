﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Tenants_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link href="../lib/select2/css/select2.min.css" rel="stylesheet" />
    <link href="../lib/spectrum/spectrum.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">İcarəçilər</span>
        </nav>

        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>İcarəçilər</h5>
            </div>
            <div class="card pd-20 pd-sm-40">

                <!-- SMALL MODAL -->
                <div id="alertmodal" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                            <div class="modal-footer justify-content-right" style="padding: 5px">
                                <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->


                <div class="row">
                    <div class="col-lg">
                        <asp:LinkButton ID="lnkAddNewTenant" runat="server"
                            CssClass="btn btn-outline-info btn-block"
                            data-toggle="modal" data-target="#modalAddTenant">
                                   <i class="icon ion-plus-circled"></i>&nbsp;&nbsp;Yeni İcarəçi
                        </asp:LinkButton>
                    </div>

                    <div class="col-lg">
                        <asp:LinkButton ID="lnkTenantSearch" runat="server"
                            CssClass="btn btn-outline-info btn-block"
                             data-toggle="modal" data-target="#modalSearchTenant">
                                   <i class="icon ion-search"></i> Axtarış
                        </asp:LinkButton>
                    </div>

                    <div class="col-lg">
                        <img id="loading_excel" style="display: none" src="../img/loader.gif" />
                        <asp:LinkButton ID="lnkDownloadExcel" runat="server"
                            CssClass="btn btn-outline-info btn-block" OnClick="lnkDownloadExcel_Click">
                                   <img style ="width:16px;height:16px" src ="/img/excel_icon.png" />&nbsp&nbspExcel-ə yüklə
                        </asp:LinkButton>
                    </div>

                </div>
                 <br />
                <asp:Literal ID="ltrDataCount" runat="server"></asp:Literal>
                <br />

                <div class="table-responsive">
                    <asp:GridView ID="GrdTenants" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" GridLines="None" EnableModelValidation="True">
                        <Columns>
                            <asp:BoundField DataField="Fullname" HeaderText="S.A.A" />

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%#Convert.ToString(Eval("TenantObjects")).Length > 5 ? Convert.ToString(Eval("TenantObjects")).Substring(5) : "" %>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Obyekt(lər)
                                </HeaderTemplate>
                            </asp:TemplateField>

                            <asp:BoundField DataField="RegNumber" HeaderText="QEYDİYYAT №" />
                            <asp:BoundField DataField="RegDate" HeaderText="QEYDİYYAT TARİXİ" />


                            <asp:BoundField DataField="ContractNumber" HeaderText="MÜQAVİLƏ №" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <b>Əlaqə nömrəsi :</b> <%#Eval("ContactNumber") %>
                                    <br />
                                    <b>Ünvan:</b> <%#Eval("Address") %>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    ƏLAQƏ 
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkTenantObjectSetting" runat="server" OnClick="lnkTenantObjectSetting_Click"
                                        CommandArgument='<%#Eval("RegNumber")%>'>
                                                <img src ="/img/object_tenant_setting.png" title ="Obyekt Əlavə edilməsi / silinməsi"/>
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="50px" />
                            </asp:TemplateField>


                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkTenantEdit" runat="server" OnClick="lnkTenantEdit_Click"
                                        CommandArgument='<%#Eval("ID")%>'>
                                                <img src ="/img/edit_icon.png" title ="İcarəçinin yenilənməsi"/>
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="50px" />
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="btnDeleteTenant"
                                        class="btn btn-outline-danger btn-block"
                                        Width="38px" Height="38px" runat="server" Text="X" Font-Bold="True"
                                        title="Sil"
                                        CommandArgument='<%# Eval("ID") %>' OnClick="btnDeleteTenant_Click"
                                        OnClientClick="return confirm('İcarəçini silmək istədiyinizdən əminsiniz?');" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="50px" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            MƏLUMAT TAPILMADI...
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>

                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <!-- LARGE MODAL -->
                        <div id="modalAddTenant" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>&nbsp&nbspYENİ İCARƏÇİ</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrModalAddTenant" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">
                                            <div class="row" >
                                                <div  class="col-8">
                                                    Soyad , Ad , Ata adı:
                                                    <asp:TextBox ID="txtNewTenantFullName" runat="server" MaxLength="60" class="form-control" placeholder="Soyad , ad , ata adı" type="text"></asp:TextBox>
                                                </div>                                                                                      
                                                <div  class="col-4">
                                                    VÖEN:
                                                    <asp:TextBox ID="txtNewTaxID" runat="server" MaxLength="10" class="form-control" placeholder="VÖEN" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row" >                                 
                                                <div class="col-4">
                                                    Şəxsiyyət V/N:
                                                    <asp:TextBox ID="txtNewTenantPassportNumber" runat="server" MaxLength="15" class="form-control" placeholder="Şəxsiyyət V/N" type="text"></asp:TextBox>
                                                </div>  
                                                 <div class="col-4">
                                                    Qeydiyyat nömrəsi:
                                                    <asp:TextBox ID="txtNewTenantRegNumber" runat="server" MaxLength="30" class="form-control" placeholder="Qeydiyyat nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-4">
                                                    Qeydiyyat tarixi:
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon ion-calendar tx-16 lh-0 op-6"></i></span>
                                                        <asp:TextBox ID="txtNewTenantRegDate" runat="server" MaxLength="30" placeholder="dd.mm.yyyy" class="form-control fc-datepicker" type="text"></asp:TextBox>
                                                    </div>
                                            </div>
                                            <br />
                                            <div class="row">                                  
                                               
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">                                          
                                                <div  class="col-lg-6">
                                                    Müqavilə nömrəsi:
                                                    <asp:TextBox ID="txtNewTenantContractNumber" runat="server" MaxLength="20" class="form-control" placeholder="Müqavilə nömrəsi" type="text"></asp:TextBox>
                                                </div>                                          
                                                 <div class="col-lg-6">
                                                    Əlaqə nömrəsi:
                                                    <asp:TextBox ID="txtNewTenantContactNumber" runat="server" MaxLength="20" class="form-control" placeholder="Əlaqə nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg">
                                                    Ünvanı:
                                                   <asp:TextBox ID="txtNewTenantAddress" runat="server" MaxLength="100" class="form-control" placeholder="Ünvanı" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- modal-body -->
                                    <div class="modal-footer">
                                        <img id="add_loading_tenant" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnNewTenantAccept" class="btn btn-info pd-x-20" OnClick="btnNewTenantAccept_Click" runat="server" Text="Əlavə et"
                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_loading_tenant').style.display = '';
                                                           document.getElementById('btnNewTenantAcceptCancel').style.display = 'none';
                                                           document.getElementById('alert_msg').style.display = 'none';" />
                                        <button id="btnNewTenantAcceptCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                       </div>
                        <!-- modal -->




                        <!-- LARGE MODAL -->
                        <div id="modalEditTenant" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>&nbsp&nbspMƏLUMATLARI YENİLƏ</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrModalEditTenant" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    Soyad , ad , ata adı:
                                                    <asp:TextBox ID="txtEditTenantFullName" runat="server" MaxLength="60" class="form-control" placeholder="Soyad , ad , ata adı" type="text"></asp:TextBox>
                                                </div>
                                                <div  class="col-lg-4">
                                                    VÖEN:
                                                    <asp:TextBox ID="txtEditTaxID" runat="server" MaxLength="10" class="form-control" placeholder="VÖEN" type="text"></asp:TextBox>
                                                </div>                                               
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    Şəxsiyyət V/N:
                                                   <asp:TextBox ID="txtEditTenantPassportNumber" runat="server" MaxLength="20" class="form-control" placeholder="Şəxsiyyət V/N" type="text"></asp:TextBox>
                                                </div>                                             
                                                <div class="col-lg-4">
                                                    Qeydiyyat nömrəsi:
                                                    <asp:TextBox ID="txtEditTenantRegNumber" Enabled="false" runat="server" MaxLength="30" class="form-control" placeholder="Qeydiyyat nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-4">
                                                    Qeydiyyat tarixi:
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon ion-calendar tx-16 lh-0 op-6"></i></span>
                                                        <asp:TextBox ID="txtEditTenantRegDate" runat="server" MaxLength="10" placeholder="dd.mm.yyyy" class="form-control fc-datepicker" type="text"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg">
                                                    Müqavilə nömrəsi
                                                    <asp:TextBox ID="txtEditTenantContractNumber" runat="server" MaxLength="20" class="form-control" placeholder="Müqavilə nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    Əlaqə nömrəsi:
                                                    <asp:TextBox ID="txtEditTenantContactNumber" runat="server" MaxLength="20" class="form-control" placeholder="Əlaqə nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                            </div>

                                            <br />
                                            <div class="row">
                                                <div class="col-lg">
                                                    Ünvanı:
                                                    <asp:TextBox ID="txtEditTenantAddress" runat="server" MaxLength="100" class="form-control" placeholder="Ünvanı" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- modal-body -->
                                    <div class="modal-footer">
                                        <img id="edit_loading_tenant" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnEditTenantAccept" class="btn btn-info pd-x-20" runat="server" Text="Yenilə" OnClick="btnEditTenantAccept_Click"
                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('edit_loading_tenant').style.display = '';
                                                           document.getElementById('btnEditTenantAcceptCancel').style.display = 'none';
                                                           document.getElementById('alert_msg').style.display = 'none';" />
                                        <button id="btnEditTenantAcceptCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->






                        <!-- LARGE MODAL -->
                        <div id="modalTenantObjectSetting" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>&nbsp&nbspOBYEKT TƏNZİMLƏNMƏSİ</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrModalTenantObjectSetting" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <asp:TextBox ID="txtTenantObjectCode" runat="server" MaxLength="30" class="form-control" placeholder="Obyekt kodu" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-4">
                                                    <asp:DropDownList ID="drlTenantType" class="form-control" runat="server">
                                                        <asp:ListItem Value="0" Selected="True">Seçin</asp:ListItem>
                                                        <asp:ListItem Value="ICARECHI">İcarəçi</asp:ListItem>
                                                        <asp:ListItem Value="SAHIBKAR">Sahibkar</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-lg-4">
                                                    <asp:LinkButton ID="lnkTenantObjectAdd" runat="server" CssClass="btn" OnClick="lnkTenantObjectAdd_Click">
                                                      <i class="icon ion-plus-circled"></i>&nbsp;Əlavə et
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="GrdTenantObjects" class="table table-hover table-bordered mg-b-0" runat="server" OnRowDataBound="GrdTenantObjects_RowDataBound" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
                                                            <Columns>
                                                                <asp:BoundField DataField="ObjectFullNumber" HeaderText="Obyekt(lər)" />
                                                                <asp:BoundField DataField="Type" HeaderText="İstifadəçi tipi" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkTenantObjectDelete" runat="server"
                                                                            OnClientClick="return confirm('Obyekti icarəçidən silmək istədiyinizdən əminsiniz?');"
                                                                            CommandArgument='<%#Convert.ToString(Eval("ObjectFullNumber")).Trim() %>' OnClick="lnkTenantObjectDelete_Click">
                                                                            <img src ="/img/delete_icon.png" title ="Sil"/>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Right" Width="50px" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                MƏLUMAT TAPILMADI...
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- modal-body -->
                                    <div class="modal-footer">
                                        <button id="btnClose" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Bağla</button>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->






                        <!-- LARGE MODAL -->
                        <div id="modalSearchTenant" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-search"></i>&nbsp&nbspAXTARIŞ</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrModalSearchTenant" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">
                                            <code class="code-base"><i>Qeyd: </i>İcazəniz olan zona(lar) üzrə SİSTEMdə mövcud olan bütün icarəçilərin siyahısını almaq üçün axtarış sahələrini təmizləyin və 'Axtar' düyməsinə sıxın</code>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg">
                                                    Soyad , ad , ata adı:
                                                    <asp:TextBox ID="txtSearchTenantFullName" runat="server" MaxLength="60" class="form-control" placeholder="Soyad , ad , ata adı" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    Şəxsiyyət V/N:
                                                   <asp:TextBox ID="txtSearchTenantPassportNumber" runat="server" MaxLength="20" class="form-control" placeholder="Şəxsiyyət V/N" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    Əlaqə nömrəsi:
                                                    <asp:TextBox ID="txtSearchTenantContactNumber" runat="server" MaxLength="20" class="form-control" placeholder="Əlaqə nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg">
                                                    Qeydiyyat nömrəsi:
                                                    <asp:TextBox ID="txtSearchTenantRegNumber" runat="server" MaxLength="30" class="form-control" placeholder="Qeydiyyat nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    Qeydiyyat tarixi:
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon ion-calendar tx-16 lh-0 op-6"></i></span>
                                                        <asp:TextBox ID="txtSearchTenantRegDate" runat="server" MaxLength="30" placeholder="dd.mm.yyyy" class="form-control fc-datepicker" type="text"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-lg">
                                                    Müqavilə nömrəsi
                                                   <asp:TextBox ID="txtSearchTenantContractNumber" runat="server" MaxLength="20" class="form-control" placeholder="Müqavilə nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg">
                                                    Ünvanı:
                                                   <asp:TextBox ID="txtSearchTenantAddress" runat="server" MaxLength="100" class="form-control" placeholder="Ünvanı" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- modal-body -->
                                    <div class="modal-footer">
                                        <img id="search_loading_tenant" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnSearchTenant" OnClick ="btnSearchTenant_Click" class="btn btn-info pd-x-20" runat="server" Text="Axtarış"
                                            OnClientClick="this.style.display = 'none';
                                               document.getElementById('search_loading_tenant').style.display = '';
                                               document.getElementById('btnSearchTenantCancel').style.display = 'none';
                                               document.getElementById('alert_msg').style.display = 'none';" />
                                        <button id="btnSearchTenantCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->





                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnNewTenantAccept" />
                        <asp:PostBackTrigger ControlID="btnEditTenantAccept" />
                        <asp:PostBackTrigger ControlID="lnkTenantObjectAdd" />
                        <asp:PostBackTrigger ControlID="btnSearchTenant" />
                        <asp:PostBackTrigger ControlID="btnSearchTenant" />
                        
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <script type="text/javascript">
            function openAddTenantModal() {
                $('#modalAddTenant').modal({ show: true });
            }

            function openEditTenantModal() {
                $('#modalEditTenant').modal({ show: true });
            }

            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }

            function openTenantObjectModal() {
                $('#modalTenantObjectSetting').modal({ show: true });
            }

            function openTenantSearchModal() {
                $('#modalSearchTenant').modal({ show: true });
            }
        </script>

        <script src="../lib/jquery/jquery.js"></script>

        <style>
            .ui-datepicker {
                z-index: 9999 !important; /* has to be larger than 1050 */
            }
        </style>

        <script type="text/javascript">
            $(function () {
                'use strict';
                // Datepicker
                $('#modalAddTenant').on('shown.bs.modal', function () {
                    $('.fc-datepicker').datepicker({
                        dateFormat: 'dd.mm.yy',
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        container: '#modalAddTenant modal-body'
                    });
                });

                $('#modalEditTenant').on('shown.bs.modal', function () {
                    $('.fc-datepicker').datepicker({
                        dateFormat: 'dd.mm.yy',
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        container: '#modalEditTenant modal-body'
                    });
                });

                $('#modalSearchTenant').on('shown.bs.modal', function () {
                    $('.fc-datepicker').datepicker({
                        dateFormat: 'dd.mm.yy',
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        container: '#modalSearchTenant modal-body'
                    });
                });

            });
        </script>
    </div>
</asp:Content>

