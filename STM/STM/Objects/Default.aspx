﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Objects_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style>
        #ctl00_ContentPlaceHolder1_GrdObjectPricesList td {
            vertical-align: middle;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">Obyektlər</span>
        </nav>
        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>Obyektlərə baxış və tənzimləməsi</h5>
            </div>
            <div class="card pd-20 pd-sm-40">
                <!-- SMALL MODAL -->
                <div id="alertmodal" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                            <div class="modal-footer justify-content-right" style="padding: 5px">
                                <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->

                <asp:MultiView ID="MView1" runat="server" ActiveViewIndex="0">
                    <asp:View runat="server">
                        <div class="row">
                            <div class="col-lg-1">
                                <a href="/objects" title="Səhifəni yenilə" class="btn btn-outline-info btn-block"><i class="fa fa-refresh"></i></a>
                            </div>
                            <div class="col-lg">
                                <asp:LinkButton ID="lnkObjectSearch" runat="server"
                                    CssClass="btn btn-outline-info btn-block"
                                    data-toggle="modal" data-target="#ModalObjectSearch">
								   <i class="icon ion-search"></i> Axtarış
                                </asp:LinkButton>
                            </div>

                            <div class="col-lg">
                                <asp:LinkButton ID="btnAddNewObjects" runat="server"
                                    CssClass="btn btn-outline-info btn-block"
                                    data-toggle="modal" data-target="#modalNewObject">
								   <i class="icon ion-plus-circled"></i> Yeni Obyekt
                                </asp:LinkButton>
                            </div>

                            <div class="col-lg">
                                <asp:LinkButton ID="lnkBulkPrices" runat="server"
                                     data-toggle="modal" data-target="#ModalObjectBulkPrices"
                                     CssClass="btn btn-outline-info btn-block">
								   <img style ="width:16px;height:16px" src ="/img/money_icon.png"/>&nbspToplu qiymətləndirmə
                                </asp:LinkButton>
                            </div>
                            &nbsp
							<div class="col-lg">
                                <asp:LinkButton ID="lnkDownloadExcel" OnClick ="lnkDownloadExcel_Click" runat="server" CssClass="btn btn-outline-info btn-block">
								   <img style ="width:16px;height:16px" src ="/img/excel_icon.png" />&nbsp&nbspExcel-ə yüklə
                                </asp:LinkButton>
                            </div>

                        </div>
                        <br/>
                        <asp:Literal ID="ltrDataCount" runat="server"></asp:Literal>
                        <br />
                        <div class="table-responsive">
                            <asp:GridView ID="GrdObjects" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" GridLines="None" EnableModelValidation="True">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkObjectDetailsShow" title="Obyektə baxış" runat="server"
                                                CommandArgument='<%#Eval("ID")%>' OnClick="lnkObjectDetailsShow_Click">
												<%#Eval("ObjectFullNumber")%>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            KODU
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="ObjectType" HeaderText="TİPİ" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <%#Eval("ObjectArea") +" m²  <b>/</b> "+ Eval("ObjectAreaContract")  + " m²"%>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            SAHƏSİ - REAL / MÜQAVİLƏ (m²)
                                        </HeaderTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ObjectPosition" HeaderText="YERLƏŞMƏSİ" />
                                    <asp:BoundField DataField="OwnerType" HeaderText="MÜLKİYYƏT" />
                                    <asp:BoundField DataField="ObjectStatus" HeaderText="STATUS" />

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkObjectCounterShow" runat="server" title="Sayğac" OnClick="lnkObjectCounterShow_Click"
                                                CommandArgument='<%#Eval("ID")%>'>
												<img src ="/img/counter.png" />
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                    </asp:TemplateField>


                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkObjectPricesShow" runat="server"
                                                CommandArgument='<%#Eval("ID")%>'
                                                OnClick="lnkObjectPricesShow_Click">
												<img src ="/img/money_icon.png" title ="Obyektin qiymətləndirilməsi"/>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkObjectEdit" runat="server"
                                                CommandArgument='<%#Eval("ID")%>' OnClick="lnkObjectEdit_Click">
												<img src ="/img/edit_icon.png" title ="Obyektin yenilənməsi"/>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDeleteObject" runat="server"
                                                class="btn btn-outline-danger btn-block"
                                                Width="38px" Height="38px" Text="X" Font-Bold="True"
                                                title="Sil" OnClick="btnDeleteObject_Click"
                                                CommandArgument='<%# Eval("ID") %>'
                                                OnClientClick="return confirm('Obyekti silmək istədiyinizdən əminsiniz?');" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>



                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>




                                <!-- LARGE MODAL -->
                                <div id="modalObjectPrices" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-compose"></i>&nbsp&nbspOBYEKTİN QİYMƏTLƏNDİRİLMƏSİ</h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <asp:Literal ID="ltrObjPriceMessage" runat="server"></asp:Literal>
                                                <div class="card pd-20 pd-sm-40">
                                                    <code class="code-base"><i>Qeyd: </i>Seçilmiş borc tipinə uyğun məbləği 0 (sıfır) daxil etdiyiniz halda, SİSTEM həmin borc tipinə uyğun heç bir borc hesablamayacaq.</code>
                                                    <p class="mg-b-20 mg-sm-b-10"></p>
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="GrdObjectPricesList" class="table mg-b-0" runat="server"
                                                            DataKeyNames="CODE" AutoGenerateColumns="False" GridLines="None"
                                                            EnableModelValidation="True" OnRowDataBound="GrdObjectPricesList_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkObjectPriceSelectModal" onclick="changeInputsDisable(this);" runat="server" />
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>#</HeaderTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <%#"<b>" + Eval("Description") + "</b>" %>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>BORC TİPİ</HeaderTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtObjectPriceModal" MaxLength="6" runat="server" class="form-control" Enabled="false" Text='<%#Eval("ObjectPrice")%>' type="text"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>MƏBLƏĞ</HeaderTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtObjectDiscountModal" MaxLength="6" runat="server" class="form-control" Enabled="false" Text='<%#Eval("ObjectDiscount")%>' type="text"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>ENDİRİM</HeaderTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblObjectDiscountMakerModal" runat="server" Text='<%# Eval("ObjectDiscountMakerID") %>' Visible="false" />
                                                                        <asp:DropDownList ID="drlObjectDiscountMakerModal" runat="server" class="form-control" Enabled="false"></asp:DropDownList>                                                                     
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>ENDİRİM EDƏN</HeaderTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                MƏLUMAT TAPILMADI...
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </div>


                                                </div>
                                            </div>
                                            <!-- modal-body -->
                                            <div class="modal-footer">
                                                <img id="add_loadingObjectPriceAccept" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnObjectPriceAccept" class="btn btn-info pd-x-20" runat="server" Text="Təsdiqlə"
                                                    OnClientClick="this.style.display = 'none';
														   document.getElementById('add_loadingObjectPriceAccept').style.display = '';
														   document.getElementById('btnObjectPriceAcceptCancel').style.display = 'none';
														   document.getElementById('alert_msg').style.display = 'none';"
                                                    OnClick="btnObjectPriceAccept_Click" />
                                                <button id="btnObjectPriceAcceptCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- modal-dialog -->
                                </div>
                                <!-- modal -->





                                <!-- LARGE MODAL -->
                                <div id="modalNewObject" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>&nbsp&nbspYENİ OBYEKT</h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <asp:Literal ID="ltrMessage" runat="server"></asp:Literal>
                                                <div class="card pd-20 pd-sm-40">
                                                    <div class="row">
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlZone" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" OnSelectedIndexChanged="drlZone_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlLine" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" OnSelectedIndexChanged="drlLine_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlCorpus" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            <asp:TextBox ID="txtObjectNumber" runat="server" MaxLength="20" class="form-control" placeholder="Obyekt nömrəsi" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlObjectType" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Selected="True">Obyektin tipi</asp:ListItem>
                                                                <asp:ListItem Value="MAGAZA">Mağaza</asp:ListItem>
                                                                <asp:ListItem Value="SKLAT">Anbar</asp:ListItem>
                                                                <asp:ListItem Value="TUALET">Tualet</asp:ListItem>
                                                                <asp:ListItem Value="METBEX">Kafe</asp:ListItem>
                                                                <asp:ListItem Value="MARKET">Market</asp:ListItem>
                                                                <asp:ListItem Value="OFIS">Ofis</asp:ListItem>
                                                                <asp:ListItem Value="KOSHK">Köşk</asp:ListItem>
                                                                <asp:ListItem Value="POLKA">Polka</asp:ListItem>
                                                                <asp:ListItem Value="MOYKA">Moyka</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:TextBox ID="txtObjectArea" runat="server" MaxLength="50" class="form-control" placeholder="Obyektin sahəsi (m²)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            <asp:TextBox ID="txtObjectContractArea" runat="server" MaxLength="50" class="form-control" placeholder="Müqavilə sahəsi (m²)" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlObjectPosition" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Selected="True">Yerləşmə tipi</asp:ListItem>
                                                                <asp:ListItem Value="KUNC">Künc</asp:ListItem>
                                                                <asp:ListItem Value="KECHID">Keçid</asp:ListItem>
                                                                <asp:ListItem Value="NORMAL">Normal</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlOwnerType" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Selected="True">Mülkiyyət</asp:ListItem>
                                                                <asp:ListItem Value="BAZAR">Bazar</asp:ListItem>
                                                                <asp:ListItem Value="SHEXSI">Şəxsi</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>




                                                     <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            <asp:TextBox ID="txtObjectContacrtNumber" runat="server" MaxLength="20" class="form-control" placeholder="Müqavilə nömrəsi" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlPaymentType" class="form-control" runat="server">
                                                                <asp:ListItem Value="" Selected="True">Ödəmə tipi</asp:ListItem>
                                                                <asp:ListItem Value="0">Nəğd</asp:ListItem>
                                                                <asp:ListItem Value="1">Köçürmə</asp:ListItem>
                                                                <asp:ListItem Value="2">Nəğd & Köçürmə</asp:ListItem>
                                                                <asp:ListItem Value="3">Heçbir</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlIsVat" class="form-control" runat="server">
                                                                <asp:ListItem Value="" Selected="True">Vergi</asp:ListItem>
                                                                <asp:ListItem Value="0">ƏDV-siz</asp:ListItem>
                                                                <asp:ListItem Value="1">ƏDV-(18%)</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>






                                                    <div class="row mg-t-20">
                                                        <div class="col-lg-8">
                                                             <asp:TextBox ID="TxtNote" runat="server" MaxLength="255" class="form-control" placeholder="Qeyd" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <asp:DropDownList ID="drlObjectStatus" class="form-control" runat="server">
                                                                <asp:ListItem Value="ACTIV" Selected="True">Aktiv</asp:ListItem>
                                                                <asp:ListItem Value="TEMIR">Təmir</asp:ListItem>
                                                                <asp:ListItem Value="BOSH">Boş</asp:ListItem>
                                                                <asp:ListItem Value="BAGLI">Bağlı</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                        </div>
                                                        <div class="col-lg">
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <!-- modal-body -->
                                            <div class="modal-footer">
                                                <img id="add_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnAddNewObject" class="btn btn-info pd-x-20" runat="server" Text="Əlavə et"
                                                    OnClick="btnAddNewObject_Click"
                                                    OnClientClick="this.style.display = 'none';
														   document.getElementById('add_loading').style.display = '';
														   document.getElementById('btnAddNewObjectCancel').style.display = 'none';
														   document.getElementById('alert_msg').style.display = 'none';" />
                                                <button id="btnAddNewObjectCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- modal-dialog -->
                                </div>
                                <!-- modal -->




                                <!-- LARGE MODAL -->
                                <div id="modalEditObjects" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>&nbsp&nbspOBYEKTİN YENİLƏNMƏSİ</h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <asp:Literal ID="ltrModalEditObject" runat="server"></asp:Literal>
                                                <div class="card pd-20 pd-sm-40">
                                                    <div class="row">
                                                        <div class="col-lg">
                                                            Zona:
															<asp:DropDownList ID="drlZoneEdit" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" OnSelectedIndexChanged="drlZoneEdit_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            Sıra:
															<asp:DropDownList ID="drlLineEdit" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" OnSelectedIndexChanged="drlLineEdit_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            Korpus:
															<asp:DropDownList ID="drlCorpusEdit" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            Obyekt nömrəsi:
															<asp:TextBox ID="txtObjectNumberEdit" runat="server" MaxLength="20" class="form-control" placeholder="Obyekt nömrəsi" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg">
                                                            Obyektin tipi:
															<asp:DropDownList ID="drlObjectTypeEdit" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Selected="True">Obyektin tipi</asp:ListItem>                
                                                                <asp:ListItem Value="MAGAZA">Mağaza</asp:ListItem>
                                                                <asp:ListItem Value="SKLAT">Anbar</asp:ListItem>
                                                                <asp:ListItem Value="TUALET">Tualet</asp:ListItem>
                                                                <asp:ListItem Value="METBEX">Kafe</asp:ListItem>
                                                                <asp:ListItem Value="MARKET">Market</asp:ListItem>
                                                                <asp:ListItem Value="OFIS">Ofis</asp:ListItem>
                                                                <asp:ListItem Value="KOSHK">Köşk</asp:ListItem>
                                                                <asp:ListItem Value="POLKA">Polka</asp:ListItem>
                                                                <asp:ListItem Value="MOYKA">Moyka</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            Obyektin sahəsi (m²):
															<asp:TextBox ID="txtObjectAreaEdit" runat="server" MaxLength="50" class="form-control" placeholder="Obyektin sahəsi (m²)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            Müqavilə sahəsi (m²):
															<asp:TextBox ID="txtObjectContractAreaEdit" runat="server" MaxLength="50" class="form-control" placeholder="Müqavilə sahəsi (m²)" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg">
                                                            Yerləşmə tipi:
															<asp:DropDownList ID="drlObjectPositionEdit" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Selected="True">Yerləşmə tipi</asp:ListItem>
                                                                <asp:ListItem Value="KUNC">Künc</asp:ListItem>
                                                                <asp:ListItem Value="KECHID">Keçid</asp:ListItem>
                                                                <asp:ListItem Value="NORMAL">Normal</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            Mülkiyyət tipi:
															<asp:DropDownList ID="drlOwnerTypeEdit" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Selected="True">Mülkiyyət</asp:ListItem>
                                                                <asp:ListItem Value="BAZAR">Bazar</asp:ListItem>
                                                                <asp:ListItem Value="SHEXSI">Şəxsi</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            Müqavilə nömrəsi:
                                                            <asp:TextBox ID="txtObjectContractNumberEdit" runat="server" MaxLength="20" class="form-control" placeholder="Müqavilə nömrəsi" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg">
                                                            Ödəniş tipi:
                                                            <asp:DropDownList ID="drlPaymnetTypeEdit" class="form-control" runat="server">
                                                                <asp:ListItem Value="" Selected="True">Ödəmə tipi</asp:ListItem>
                                                                <asp:ListItem Value="0">Nəğd</asp:ListItem>
                                                                <asp:ListItem Value="1">Köçürmə</asp:ListItem>
                                                                <asp:ListItem Value="2">Nəğd & Köçürmə</asp:ListItem>
                                                                <asp:ListItem Value="3">Heçbir</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            Vergi hesablama:
                                                            <asp:DropDownList ID="drlIsVATEdit" class="form-control" runat="server">
                                                                <asp:ListItem Value="" Selected="True">Vergi</asp:ListItem>
                                                                <asp:ListItem Value="0">ƏDV-siz</asp:ListItem>
                                                                <asp:ListItem Value="1">ƏDV-(18%)</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg-8">
                                                            Qeyd:
                                                             <asp:TextBox ID="TxtNoteEdit" runat="server" MaxLength="255" class="form-control" placeholder="Qeyd" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            Obyektin statusu:
															<asp:DropDownList ID="drlObjectStatusEdit" class="form-control" runat="server">
                                                                <asp:ListItem Value="ACTIV" Selected="True">Aktiv</asp:ListItem>
                                                                <asp:ListItem Value="TEMIR">Təmir</asp:ListItem>
                                                                <asp:ListItem Value="BOSH">Boş</asp:ListItem>
                                                                <asp:ListItem Value="BAGLI">Bağlı</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                        </div>
                                                        <div class="col-lg">
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <!-- modal-body -->
                                            <div class="modal-footer">
                                                <img id="edit_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnObjectEdit" OnClick="btnObjectEdit_Click" class="btn btn-info pd-x-20" runat="server" Text="Yenilə"
                                                    OnClientClick="this.style.display = 'none';
														   document.getElementById('edit_loading').style.display = '';
														   document.getElementById('btnObjectCancelEdit').style.display = 'none';
														   document.getElementById('alert_msg').style.display = 'none';" />
                                                <button id="btnObjectCancelEdit" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- modal-dialog -->
                                </div>
                                <!-- modal -->





                                <!-- LARGE MODAL -->
                                <div id="ModalObjectCounters" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-compose"></i>&nbsp&nbspOBYEKTİN QİYMƏTLƏNDİRİLMƏSİ</h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <asp:Literal ID="ltrMsgObjectCounters" runat="server"></asp:Literal>
                                                <div class="card pd-20 pd-sm-40">
                                                    <code class="code-base"><i>Qeyd: </i>Seçilmiş sayğac tipinə uyğun sayğac nömrəsini boş daxil etdiyiniz halda,  həmin sayğac SİSTEM tərəfindən silinəcək</code>
                                                    <p class="mg-b-20 mg-sm-b-10"></p>
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="GrdObjectCountersList" class="table mg-b-0" runat="server"
                                                            DataKeyNames="CounterType" AutoGenerateColumns="False" GridLines="None"
                                                            EnableModelValidation="True">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkObjectCounterSelectModal" onclick="changeInputsDisableCounter(this);" runat="server" />
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>#</HeaderTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <%#"<b>" + Eval("CounterTypeDesc") + "</b>" %>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>SAYĞAC TİPİ</HeaderTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtObjectCounterNumberModal" MaxLength="25" runat="server" class="form-control" Enabled="false" Text='<%#Eval("CounterNumber")%>' type="text"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>NÖMRƏSİ</HeaderTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblObjectCounterUniteModal" runat="server" class="form-control" Text='<%#Eval("Unite")%>'></asp:Label>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>VAHİDİ</HeaderTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtObjectCounterUnitePriceModal" MaxLength="10" runat="server" class="form-control" Enabled="false" Text='<%#Eval("UnitePrice")%>' type="text"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                    <HeaderTemplate>QİYMƏT</HeaderTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                MƏLUMAT TAPILMADI...
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                    </div>


                                                </div>
                                            </div>
                                            <!-- modal-body -->
                                            <div class="modal-footer">
                                                <img id="loading_for_counter" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnObjectCounterAccept" OnClick="btnObjectCounterAccept_Click"
                                                    class="btn btn-info pd-x-20" runat="server" Text="Təsdiqlə"
                                                    OnClientClick="this.style.display = 'none';
														   document.getElementById('loading_for_counter').style.display = '';
														   document.getElementById('btnObjectCounterAcceptCancel').style.display = 'none';
														   document.getElementById('alert_msg').style.display = 'none';" />
                                                <button id="btnObjectCounterAcceptCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- modal-dialog -->
                                </div>
                                <!-- modal -->








                                <!-- LARGE MODAL -->
                                <div id="ModalObjectBulkPrices" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">
                                                    <img style="width:16px;height:16px" src="/img/money_icon.png">  TOPLU QİYMƏTLƏNDİRMƏ</h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <asp:Literal ID="ltrBulkPriceMessage" runat="server"></asp:Literal>
                                                <div class="card pd-20 pd-sm-40">
                                                    <div class="row">
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlZoneBulkPrice" OnSelectedIndexChanged ="drlZoneBulkPrice_SelectedIndexChanged" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlLineBulkPrice" OnSelectedIndexChanged ="drlLineBulkPrice_SelectedIndexChanged" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            <asp:DropDownList ID="drlCorpusBulkPrice" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            Yerləşmə tipi:
															<asp:DropDownList ID="drlObjectPositionBulkPrice" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Selected="True">Yerləşmə tipi</asp:ListItem>
                                                                <asp:ListItem Value="KUNC">Künc</asp:ListItem>
                                                                <asp:ListItem Value="KECHID">Keçid</asp:ListItem>
                                                                <asp:ListItem Value="NORMAL">Normal</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            Məbləğ:
															<asp:TextBox ID="txtBulkPrice" runat="server" MaxLength="10" class="form-control" Text="0" placeholder="Məbləğ"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg">
                                                            Endirim məbləği:
															<asp:TextBox ID="txtBulkDiscountPrice" runat="server" MaxLength="10" class="form-control" Text="0" placeholder="Qiyməti"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            Endirim edən:
                                                        <asp:DropDownList ID="drlBulkDiscountMaker" runat="server" class="form-control"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                        </div>
                                                        <div class="col-lg">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        <!-- modal-body -->
                                        <div class="modal-footer">
                                            <img id="img_loading_bulk" style="display: none" src="../img/loader.gif" />
                                            <asp:Button ID="btnSetBulkPrice" OnClick ="btnSetBulkPrice_Click" class="btn btn-info pd-x-20" runat="server" Text="Təsdiqlə"
                                                OnClientClick="this.style.display = 'none';
														   document.getElementById('img_loading_bulk').style.display = '';
														   document.getElementById('btnSetBulkPriceCancel').style.display = 'none';
														   document.getElementById('alert_msg').style.display = 'none';" />
                                            <button id="btnSetBulkPriceCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- modal-dialog -->
                                </div>
								<!-- modal -->







                                 <!-- LARGE MODAL -->
                                <div id="ModalObjectSearch" class="modal fade" data-backdrop="static">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content tx-size-sm">
                                            <div class="modal-header pd-x-20">
                                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-search"></i>&nbsp&nbspAXTARIŞ</h6>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body pd-20">
                                                <asp:Literal ID="ltrObjectSearchMessage" runat="server"></asp:Literal>
                                                <div class="card pd-20 pd-sm-40">
                                                    <code class="code-base"><i>Qeyd: </i>İcazəniz olan zona(lar) üzrə SİSTEMdə mövcud olan bütün obyeklərin siyahısını almaq üçün axtarış sahələrini təmizləyin və 'Axtar' düyməsinə sıxın</code>
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-lg">
                                                            Zona:
															<asp:DropDownList ID="drlZoneSearch" DataTextField="Description" OnSelectedIndexChanged ="drlZoneSearch_SelectedIndexChanged" DataValueField="Code" CssClass="form-control" runat="server"  AutoPostBack="true"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            Sıra:
															<asp:DropDownList ID="drlLineSearch" DataTextField="Description" OnSelectedIndexChanged ="drlLineSearch_SelectedIndexChanged" DataValueField="Code" CssClass="form-control" runat="server"  AutoPostBack="true"></asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            Korpus:
															<asp:DropDownList ID="drlCorpusSearch" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            Obyekt nömrəsi:
															<asp:TextBox ID="txtObjectNumberSearch" runat="server" MaxLength="20" class="form-control" placeholder="Obyekt nömrəsi" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg">
                                                            Obyektin tipi:
															<asp:DropDownList ID="drlObjectTypeSearch" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Selected="True">Obyektin tipi</asp:ListItem>
                                                                <asp:ListItem Value="MAGAZA">Mağaza</asp:ListItem>
                                                                <asp:ListItem Value="SKLAT">Anbar</asp:ListItem>
                                                                <asp:ListItem Value="TUALET">Tualet</asp:ListItem>
                                                                <asp:ListItem Value="METBEX">Kafe</asp:ListItem>
                                                                <asp:ListItem Value="MARKET">Market</asp:ListItem>
                                                                <asp:ListItem Value="OFIS">Ofis</asp:ListItem>
                                                                <asp:ListItem Value="KOSHK">Köşk</asp:ListItem>
                                                                <asp:ListItem Value="POLKA">Polka</asp:ListItem>
                                                                <asp:ListItem Value="MOYKA">Moyka</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            Obyektin sahəsi (m²):
															<asp:TextBox ID="txtObjectAreaSearch" runat="server" MaxLength="50" class="form-control" placeholder="Obyektin sahəsi (m²)"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            Müqavilə sahəsi (m²):
															<asp:TextBox ID="txtObjectContractAreaSearch" runat="server" MaxLength="50" class="form-control" placeholder="Müqavilə sahəsi (m²)" type="text"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg">
                                                            Yerləşmə tipi:
															<asp:DropDownList ID="drlObjectPositionSearch" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Selected="True">Yerləşmə tipi</asp:ListItem>
                                                                <asp:ListItem Value="KUNC">Künc</asp:ListItem>
                                                                <asp:ListItem Value="KECHID">Keçid</asp:ListItem>
                                                                <asp:ListItem Value="NORMAL">Normal</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                            Mülkiyyət tipi:
															<asp:DropDownList ID="drlOwnerTypeSearch" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Selected="True">Mülkiyyət</asp:ListItem>
                                                                <asp:ListItem Value="BAZAR">Bazar</asp:ListItem>
                                                                <asp:ListItem Value="SHEXSI">Şəxsi</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="row mg-t-20">
                                                        <div class="col-lg">
                                                            Obyektin statusu:
															<asp:DropDownList ID="drlObjectStatusSearch" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Selected="True">Obyektin statusu</asp:ListItem>
                                                                <asp:ListItem Value="ACTIV">Aktiv</asp:ListItem>
                                                                <asp:ListItem Value="TEMIR">Təmir</asp:ListItem>
                                                                <asp:ListItem Value="BOSH">Boş</asp:ListItem>
                                                                <asp:ListItem Value="BAGLI">Bağlı</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg">
                                                        </div>
                                                        <div class="col-lg">
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <!-- modal-body -->
                                            <div class="modal-footer">
                                                <img id="search_loading" style="display: none" src="../img/loader.gif" />
                                                <asp:Button ID="btnObject_Search" OnClick ="btnObject_Search_Click" class="btn btn-info pd-x-20" runat="server" Text="Axtar"
                                                    OnClientClick="this.style.display = 'none';
														   document.getElementById('search_loading').style.display = '';
														   document.getElementById('btnObject_SearchCancel').style.display = 'none';
														   document.getElementById('alert_msg').style.display = 'none';" />
                                                <button id="btnObject_SearchCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- modal-dialog -->
                                </div>
                                <!-- modal -->





                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnAddNewObject" />
                                <asp:PostBackTrigger ControlID="drlZone" />
                                <asp:PostBackTrigger ControlID="drlLine" />
                                <asp:PostBackTrigger ControlID="drlZoneEdit" />
                                <asp:PostBackTrigger ControlID="drlLineEdit" />
                                <asp:PostBackTrigger ControlID="btnObjectPriceAccept" />
                                <asp:PostBackTrigger ControlID="btnObjectEdit" />
                                <asp:PostBackTrigger ControlID="btnObjectCounterAccept" />
                                <asp:PostBackTrigger ControlID="drlZoneBulkPrice" />
                                <asp:PostBackTrigger ControlID="drlLineBulkPrice" />
                                <asp:PostBackTrigger ControlID="btnSetBulkPrice" />
                                <asp:PostBackTrigger ControlID="drlZoneSearch" />
                                <asp:PostBackTrigger ControlID="drlLineSearch" />
                                <asp:PostBackTrigger ControlID="btnObject_Search" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </asp:View>
                    <asp:View ID="vv2" runat="server">
                        <asp:LinkButton ID="lnkObjectBack" OnClick="lnkObjectBack_Click" runat="server"><i class="icon ion-arrow-left-a"> GERİ</i></asp:LinkButton>
                        <p class="mg-b-20 mg-sm-b-20"></p>
                        <div class="container">
                            <h4>
                                <asp:Literal ID="ltrObjectFullNumber" runat="server"></asp:Literal></h4>
                            <hr />
                            <!-- Nav pills -->
                            <ul class="nav nav-pills" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="pill" href="#omenu1">Ümumi məlumat</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#omenu2">Öhdəlikləri</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#omenu3">Sahibi(lər)</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#omenu4">İcarəçi(lər)</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#omenu5">Sayğac(lar)</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div id="omenu1" class="container tab-pane active">
                                    <br />
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <dl>
                                                        <dt>Zona:</dt>
                                                        <dd>
                                                            <kbd style="background-color: #1576d6 !important">
                                                                <asp:Literal ID="litZone" runat="server"></asp:Literal>
                                                            </kbd>
                                                        </dd>
                                                        <dt>Sira:</dt>
                                                        <dd>
                                                            <kbd style="background-color: #1576d6 !important">
                                                                <asp:Literal ID="litLine" runat="server"></asp:Literal>
                                                            </kbd>
                                                        </dd>
                                                        <dt>Korpus:</dt>
                                                        <dd>
                                                            <kbd style="background-color: #1576d6 !important">
                                                                <asp:Literal ID="litCorpus" runat="server"></asp:Literal>
                                                            </kbd>
                                                        </dd>
                                                    </dl>
                                                </td>
                                                <td>
                                                    <dl>
                                                        <dt>Obyekt Nömrəsi:</dt>
                                                        <dd>
                                                            <kbd style="background-color: #1576d6 !important">
                                                                <asp:Literal ID="litObjectNumber" runat="server"></asp:Literal>
                                                            </kbd>
                                                        </dd>
                                                        <dt>Obyekt Kodu:</dt>
                                                        <dd>
                                                            <kbd style="background-color: #1576d6 !important">
                                                                <asp:Literal ID="litObjectFullNumber" runat="server"></asp:Literal>
                                                            </kbd>
                                                        </dd>
                                                        <dt>Obyekt tipi:</dt>
                                                        <dd>
                                                            <kbd style="background-color: #1576d6 !important">
                                                                <asp:Literal ID="litObjectType" runat="server"></asp:Literal>
                                                            </kbd>
                                                        </dd>
                                                    </dl>
                                                </td>
                                                <td>
                                                    <dl>
                                                        <dt>Sahəsi:</dt>
                                                        <dd><kbd style="background-color: #1576d6 !important">
                                                            <asp:Literal ID="litObjectArea" runat="server"></asp:Literal>
                                                            m<sup>2</sup></kbd></dd>
                                                        <dt>Müqavilə sahəsi:</dt>
                                                        <dd><kbd style="background-color: #1576d6 !important">
                                                            <asp:Literal ID="litObjectAreaContract" runat="server"></asp:Literal>
                                                            m<sup>2</sup></kbd></dd>
                                                        <dt>Yerləşmə tipi:</dt>
                                                        <dd>
                                                            <kbd style="background-color: #1576d6 !important">
                                                                <asp:Literal ID="litObjectPositionType" runat="server"></asp:Literal>
                                                            </kbd>
                                                        </dd>
                                                    </dl>
                                                </td>
                                                <td>
                                                    <dl>
                                                        <dt>Statusu / Mülkiyyət tipi:</dt>
                                                        <dd>
                                                            <kbd style="background-color: #1576d6 !important">
                                                                <asp:Literal ID="litObjectStatus" runat="server"></asp:Literal>
                                                                <b>/</b>
                                                                <asp:Literal ID="litOwnerType" runat="server"></asp:Literal>
                                                            </kbd>
                                                        </dd>




                                                        <dt>Son dəyişiklik edən:</dt>
                                                        <dd>
                                                            <kbd style="background-color: #1576d6 !important">
                                                                <asp:Literal ID="litObjectCMakerName" runat="server"></asp:Literal>
                                                            </kbd>
                                                        </dd>
                                                        <dt>Son dəyişiklik tarixi:</dt>
                                                        <dd>
                                                            <kbd style="background-color: #1576d6 !important">
                                                                <asp:Literal ID="litObjectCMakerDate" runat="server"></asp:Literal>
                                                            </kbd>
                                                        </dd>
                                                    </dl>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <br />
                                    <h4>Dəyişikliklərin tarixçəsi</h4>
                                    <hr />
                                    <div class="table-responsive">
                                        <asp:GridView ID="GrdObjectHistory" class="table" GridLines="None" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("ZoneDesc") + "<br/>" + Eval("LineDesc") + "<br/>" + Eval("CorpusDesc") %>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>ZONA/SIRA/KOR.</HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ObjectNumber" HeaderText="№" />
                                                <asp:BoundField DataField="ObjectType" HeaderText="TİPİ" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("ObjectArea") + "<b> / </b>" + Eval("ObjectAreaContract") %>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>SAHƏ(M²)</HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ObjectPosition" HeaderText="YERLƏŞMƏ" />
                                                <asp:BoundField DataField="ObjectStatus" HeaderText="STATUS" />
                                                <asp:BoundField DataField="OwnerType" HeaderText="MÜLKİYYƏT" />
                                                <asp:BoundField DataField="CMakerName" HeaderText="AVTOR" />
                                                <asp:BoundField DataField="CMakerDateFormat" HeaderText="TARİX" />
                                            </Columns>
                                            <EmptyDataTemplate>MƏLUMAT TAPILMADI...</EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div id="omenu2" class="container tab-pane fade">
                                    <br />
                                    <div class="table-responsive">
                                        <asp:GridView ID="GrdObjectViewPrices" class="table" GridLines="None" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("PriceTypeDesc") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>BORC TİPİ</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("ObjectPrice") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>MƏBLƏĞ</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("ObjectDiscount") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>ENDİRİM</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%# Convert.ToString(Eval("DiscountMakerName")).Trim() != "" ?  "<kbd style=\"background-color: #1576d6 !important\">" + Eval("DiscountMakerName") + "</kbd>" :  ""%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>ENDİRİM EDƏN</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("MakerName") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>AVTOR</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("CMakerDateFormat") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>TARİX</HeaderTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>MƏLUMAT TAPILMADI...</EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <br />
                                    <h4>Dəyişikliklərin tarixçəsi</h4>
                                    <hr />
                                    <div class="table-responsive">
                                        <asp:GridView ID="GrdObjectViewPricesHistory" class="table" GridLines="None" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("PriceTypeDesc")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>BORC TİPİ</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("ObjectPrice")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>MƏBLƏĞ</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("ObjectDiscount")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>ENDİRİM</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%# Eval("DiscountMakerName")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>ENDİRİM EDƏN</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("MakerName")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>AVTOR</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("CMakerDateFormat")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>TARİX</HeaderTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>MƏLUMAT TAPILMADI...</EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div id="omenu3" class="container tab-pane fade">
                                    <br />
                                    <div class="table-responsive">
                                        <asp:GridView ID="GrdObjectViewOwners" class="table" GridLines="None" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("Fullname") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>SAHİBKAR ADI</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("RegDate") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>QEYDİYYAT TARİXİ</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("MakerName") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>AVTOR</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("CMakerDateFormat") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>TARİX</HeaderTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>MƏLUMAT TAPILMADI...</EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <br />
                                    <h4>Dəyişikliklərin tarixçəsi</h4>
                                    <hr />
                                    <div class="table-responsive">
                                        <asp:GridView ID="GrdObjectViewOwnersHistory" class="table" GridLines="None" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("Fullname")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>SAHİBKAR ADI</HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("RegDate")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>QEYDİYYAT TARİXİ</HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("MakerName")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>AVTOR</HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%# Eval("CMakerDateFormat")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>TARİX</HeaderTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>MƏLUMAT TAPILMADI...</EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>

                                </div>
                                <div id="omenu4" class="container tab-pane fade">
                                    <br />
                                    <div class="table-responsive">
                                        <asp:GridView ID="GrdObjectViewTenant" class="table" GridLines="None" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("Fullname") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>İCARƏÇİ ADI</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("RegNumber") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>QEYDİYYAT №</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("RegDate") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>QEYDİYYAT TARİXİ</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("MakerName") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>AVTOR</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("CMakerDateFormat") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>TARİX</HeaderTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>MƏLUMAT TAPILMADI...</EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <br />
                                    <h4>Dəyişikliklərin tarixçəsi</h4>
                                    <hr />
                                    <div class="table-responsive">
                                        <asp:GridView ID="GrdObjectViewTenantHistory" class="table" GridLines="None" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("Fullname")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>İCARƏÇİ ADI</HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("RegNumber")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>QEYDİYYAT №</HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("RegDate")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>QEYDİYYAT TARİXİ</HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("MakerName")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>AVTOR</HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%# Eval("CMakerDateFormat")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>TARİX</HeaderTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>MƏLUMAT TAPILMADI...</EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div id="omenu5" class="container tab-pane fade">
                                    <br />
                                    <div class="table-responsive">
                                        <asp:GridView ID="GrdObjectViewCounters" class="table" GridLines="None" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("CounterTypeDesc") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>SAYĞAC TİPİ</HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("CounterNumber") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>SAYĞAC NÖMRƏSİ</HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("MakerName") + "</kbd>"%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>AVTOR</HeaderTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%# "<kbd style=\"background-color: #1576d6 !important\">" + Eval("CMakerDateFormat") + "</kbd>" %>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>TARİX</HeaderTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>MƏLUMAT TAPILMADI...</EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <br />
                                    <h4>Dəyişikliklərin tarixçəsi</h4>
                                    <hr />
                                    <div class="table-responsive">
                                        <asp:GridView ID="GrdObjectViewCountersHistory" class="table" GridLines="None" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("CounterTypeDesc")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>SAYĞAC TİPİ</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("CounterNumber")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>SAYĞAC NÖMRƏSİ</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%#Eval("MakerName")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>AVTOR</HeaderTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <%# Eval("CMakerDateFormat")%>
                                                    </ItemTemplate>
                                                    <HeaderTemplate>TARİX</HeaderTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>MƏLUMAT TAPILMADI...</EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>



        <script type="text/javascript">
            function openAddObjectModal() {
                $('#modalNewObject').modal({ show: true });
            }
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }
            function openObjectPricesModal() {
                $('#modalObjectPrices').modal({ show: true });
            }
            function openObjectEditModal() {
                $('#modalEditObjects').modal({ show: true }); 
            }
            function openObjectCounterModal() {
                $('#ModalObjectCounters').modal({ show: true }); 
            }

            function openObjectBulkPricesModal() {
                $('#ModalObjectBulkPrices').modal({ show: true });
            }

            function openObjectSearchModal() {
                $('#ModalObjectSearch').modal({ show: true });
            }

        </script>

        <script>
            function changeInputsDisable(chk) {
                var txtObjectPriceModal_1 = $(chk).parents('tr').find('input[type="text"][id$="txtObjectPriceModal"]');
                var txtObjectDiscountModal_1 = $(chk).parents('tr').find('input[type="text"][id$="txtObjectDiscountModal"]');
                var drlObjectDiscountMakerModal_1 = $(chk).parents('tr').find('select');

                if (chk.checked == true) {
                    txtObjectPriceModal_1.removeAttr("disabled");
                    txtObjectDiscountModal_1.removeAttr("disabled");
                    drlObjectDiscountMakerModal_1.prop('disabled', false);
                }
                else {
                    txtObjectPriceModal_1.attr("disabled", "disabled");
                    txtObjectDiscountModal_1.attr("disabled", "disabled");
                    drlObjectDiscountMakerModal_1.prop('disabled', true);
                }
            }

            function changeInputsDisableCounter(chk) {
                var txtObjectCounterNumberModal_1 = $(chk).parents('tr').find('input[type="text"][id$="txtObjectCounterNumberModal"]');

                if (chk.checked == true) {
                    txtObjectCounterNumberModal_1.removeAttr("disabled");
                }
                else {
                    txtObjectCounterNumberModal_1.attr("disabled", "disabled");
                }
            }
        </script>
    </div>
</asp:Content>

