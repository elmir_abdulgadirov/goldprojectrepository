﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Objects_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();
    void FillZone(int userID)
    {
        DataTable dtZone = db.GetZoneByAllowedUser(userID);
        drlZone.DataSource = dtZone;
        drlZone.DataBind();
        drlZone.Items.Insert(0, new ListItem("--Zona seçin", ""));
        drlLine.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpus.Items.Insert(0, new ListItem("--Korpus seçin", ""));      
    }


    void FillZoneBulkPrice(int userID)
    {
        DataTable dtZone = db.GetZoneByAllowedUser(userID);
        drlZoneBulkPrice.DataSource = dtZone;
        drlZoneBulkPrice.DataBind();
        drlZoneBulkPrice.Items.Insert(0, new ListItem("--Zona seçin", ""));
        drlLineBulkPrice.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpusBulkPrice.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }

    void FillZoneSearch(int userID)
    {
        DataTable dtZone = db.GetZoneByAllowedUser(userID);
        drlZoneSearch.DataSource = dtZone;
        drlZoneSearch.DataBind();
        drlZoneSearch.Items.Insert(0, new ListItem("--Zona seçin", ""));
        drlLineSearch.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpusSearch.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }


    void FillLine(string ZoneID)
    {
        DataTable dtLine = db.GetAllLines(ZoneID);
        drlLine.DataSource = dtLine;
        drlLine.DataBind();
        drlLine.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpus.Items.Clear();
        drlCorpus.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }

    void FillLineBulkPrice(string ZoneID)
    {
        DataTable dtLine = db.GetAllLines(ZoneID);
        drlLineBulkPrice.DataSource = dtLine;
        drlLineBulkPrice.DataBind();
        drlLineBulkPrice.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpusBulkPrice.Items.Clear();
        drlCorpusBulkPrice.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }

    void FillLineSearch(string ZoneID)
    {
        DataTable dtLine = db.GetAllLines(ZoneID);
        drlLineSearch.DataSource = dtLine;
        drlLineSearch.DataBind();
        drlLineSearch.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpusSearch.Items.Clear();
        drlCorpusSearch.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }


    void FillCorpus(string ZoneID, string LineID)
    {
        DataTable dtCorpus = db.GetAllCorpus(ZoneID, LineID);
        drlCorpus.DataSource = dtCorpus;
        drlCorpus.DataBind();
        drlCorpus.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }

    void FillCorpusBulkPrice(string ZoneID, string LineID)
    {
        DataTable dtCorpus = db.GetAllCorpus(ZoneID, LineID);
        drlCorpusBulkPrice.DataSource = dtCorpus;
        drlCorpusBulkPrice.DataBind();
        drlCorpusBulkPrice.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }

    void FillCorpusSearch(string ZoneID, string LineID)
    {
        DataTable dtCorpus = db.GetAllCorpus(ZoneID, LineID);
        drlCorpusSearch.DataSource = dtCorpus;
        drlCorpusSearch.DataBind();
        drlCorpusSearch.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }

    void FillDiscountMakerBulkPrice()
    {
        drlBulkDiscountMaker.DataSource = db.GetObjectDiscountMakerList();
        drlBulkDiscountMaker.DataTextField = "FullName";
        drlBulkDiscountMaker.DataValueField = "ID";
        drlBulkDiscountMaker.DataBind();
        drlBulkDiscountMaker.Items.Insert(0, new ListItem("Endirim edən", "0"));
    }


    void FillObjectList(ObjectsSearchParams Ops, bool isTop , int userID,bool exportExcel)
    {
        int datacount;
        DataTable dtObjects = db.GetObjectList(Ops, isTop, userID, exportExcel,out datacount);
        if (!exportExcel)
        {
            GrdObjects.DataSource = dtObjects;
            GrdObjects.DataBind();
            GrdObjects.UseAccessibleHeader = true;
            if (GrdObjects.Rows.Count > 0) GrdObjects.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        ltrDataCount.Text = string.Format("<span>Məlumat sayı: <b>{0}</b></span>", datacount);
        if (exportExcel)
        {
            ExportExcel exExcel = new ExportExcel();
            exExcel.ExportToExcel(dtObjects, "İcarəçilər", "Icarechiler");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");
        if (!IsPostBack)
        {
            FillZone(Convert.ToInt32(Session["LogonUserID"]));
            FillZoneForEdit(Convert.ToInt32(Session["LogonUserID"]));
           

            FillZoneBulkPrice(Convert.ToInt32(Session["LogonUserID"]));
            FillDiscountMakerBulkPrice();

            FillZoneSearch(Convert.ToInt32(Session["LogonUserID"]));

            if (Session["ObjectsSearchParams"] != null)
            {
                Session["ObjectsSearchParams"] = null;
                Session.Remove("ObjectsSearchParams");
            }
            ObjectsSearchParams Ops = new ObjectsSearchParams();
            FillObjectList(Ops,true,Convert.ToInt32(Session["LogonUserID"]),false);
        }

        string FUNCTION_ID = "OBJECTS_MAIN_PAGE";

        /*Begin Permission*/
        db.CheckFunctionPermission(Convert.ToInt32(Session["LogonUserID"]), FUNCTION_ID);

        lnkObjectSearch.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "SEARCH_OBJECTS");
        btnAddNewObjects.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "ADD_NEW_OBJECT");
        lnkBulkPrices.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "OBJECTS_BULK_PRICE");
        lnkDownloadExcel.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "OBJECTS_DOWNLOAD_EXCEL");
        

        if (GrdObjects.Rows.Count > 0)
        {
            GrdObjects.Columns[6].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "OBJECT_CHANGE_COUNTERS");
            GrdObjects.Columns[7].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "OBJECT_CHANGE_PRICE");
            GrdObjects.Columns[8].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "OBJECT_CHANGE_DATA");
            GrdObjects.Columns[9].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "OBJECT_DELETE");
        }       
        /*End Permission*/
    }
    protected void drlZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        ltrMessage.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
        FillLine(Convert.ToString(drlZone.SelectedValue));     
    }
    protected void drlLine_SelectedIndexChanged(object sender, EventArgs e)
    {
        ltrMessage.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
        FillCorpus(Convert.ToString(drlZone.SelectedValue), Convert.ToString(drlLine.SelectedValue));    
    }
  
    protected void btnAddNewObject_Click(object sender, EventArgs e)
    {
        if (drlZone.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Zonanı seçin!");
            return;
        }
        if (drlLine.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Sıranı seçin!");
            return;
        }
        if (drlCorpus.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Korpusu seçin!");
            return;
        }
        if (txtObjectNumber.Text.Trim().Length == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Obyekt nömrəsini daxil edin!");
            return;
        }
        if (drlObjectType.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Obyektin tipini seçin!");
            return;
        }
        decimal objectArea = 0;
        try 
        {
            objectArea = Convert.ToDecimal(txtObjectArea.Text.Trim().Replace(",", "."));
        }
        catch 
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Obyektin sahəsini daxil edin!");
            return;
        }

        decimal objectAreaContract = 0;
        try
        {
            objectAreaContract = Convert.ToDecimal(txtObjectContractArea.Text.Trim().Replace(",","."));
        }
        catch
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Obyektin müqavilədəki sahəsini daxil edin!");
            return;
        }

        if (drlObjectPosition.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Obyektin yerləşmə tipini seçin!");
            return;
        }


        if (drlOwnerType.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Mülkiyyət tipini seçin!");
            return;
        }


        if (drlPaymentType.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Ödəmə tipini seçin!");
            return;
        }

        if (drlIsVat.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Vergini seçin!");
            return;
        }



        if (drlObjectStatus.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Obyektin statusunu seçin!");
            return;
        }
        string object_full_number = drlZone.SelectedValue + "-" + drlLine.SelectedValue + "-" + drlCorpus.SelectedValue + "-" + txtObjectNumber.Text.Trim();

        string objectNote = Convert.ToString(TxtNote.Text.Trim());
        string result = db.AddObject(Convert.ToString(drlZone.SelectedValue),
                                     Convert.ToString(drlLine.SelectedValue),
                                     Convert.ToString(drlCorpus.SelectedValue),
                                     txtObjectNumber.Text.Trim(),
                                     object_full_number,
                                     drlObjectType.SelectedValue,
                                     objectArea,
                                     objectAreaContract,
                                     drlObjectPosition.SelectedValue,
                                     drlObjectStatus.SelectedValue,
                                     drlOwnerType.SelectedValue,
                                     objectNote,
                                     "ACTIVE",
                                     Convert.ToInt32(Session["LogonUserID"]),
                                     txtObjectContacrtNumber.Text,
                                     Convert.ToInt32(drlPaymentType.SelectedValue),
                                     Convert.ToInt32(drlIsVat.SelectedValue));
        if (!result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage(result);
            return;
        }
        ObjectsSearchParams Osp = new ObjectsSearchParams(); 
        if (Session["ObjectsSearchParams"] != null) Osp = (ObjectsSearchParams)Session["ObjectsSearchParams"];

        FillObjectList(Osp, Session["ObjectsSearchParams"] != null ? false : true, Convert.ToInt32(Session["LogonUserID"]), false);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        ltrAlertMsg.Text = Alert.SuccessMessage("Obyekt yaradıldı");
       
    }
  

    void FillPriceModal(int ObjectId)
    {      
        GrdObjectPricesList.DataSource = db.GetObjectPricesList(ObjectId);
        GrdObjectPricesList.DataBind();
        GrdObjectPricesList.UseAccessibleHeader = true;
        if (GrdObjectPricesList.Rows.Count > 0) GrdObjectPricesList.HeaderRow.TableSection = TableRowSection.TableHeader;

    }
    protected void GrdObjectPricesList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Find the DropDownList in the Row
            DropDownList ddlDiscountMaker = (e.Row.FindControl("drlObjectDiscountMakerModal") as DropDownList);
            ddlDiscountMaker.DataSource = db.GetObjectDiscountMakerList();
            ddlDiscountMaker.DataTextField = "FullName";
            ddlDiscountMaker.DataValueField = "ID";
            ddlDiscountMaker.DataBind();

            //Add Default Item in the DropDownList
            ddlDiscountMaker.Items.Insert(0, new ListItem("Seçin", "0"));

            string ObjectDiscountMakerModal = (e.Row.FindControl("lblObjectDiscountMakerModal") as Label).Text;
            ddlDiscountMaker.Items.FindByValue(ObjectDiscountMakerModal).Selected = true;
        }
    }
    protected void btnObjectPriceAccept_Click(object sender, EventArgs e)
    {
        if(Session["ObjectIDSelected_Prices"] == null) Config.Rd("/exit");
        int ObjectID = Convert.ToInt32(Session["ObjectIDSelected_Prices"]);
        ltrObjPriceMessage.Text = "";
        DataTable dt = new DataTable("ObjectPrice");
        dt.Columns.Add("ObjectID", typeof(int));
        dt.Columns.Add("ObjectPrice", typeof(decimal));
        dt.Columns.Add("ObjectDiscount", typeof(decimal));
        dt.Columns.Add("ObjectDiscountMakerID", typeof(int));
        dt.Columns.Add("ObjectPriceType", typeof(string));
        dt.Columns.Add("CMakerId", typeof(int));

        for (int i = 0; i < GrdObjectPricesList.Rows.Count; i++)
        {
            CheckBox Chk = (GrdObjectPricesList.Rows[i].Cells[0].FindControl("chkObjectPriceSelectModal") as CheckBox);
            if (Chk.Checked)
            {
                string priceType = (string)GrdObjectPricesList.DataKeys[i]["CODE"];
                TextBox txtObjectPriceModal = (GrdObjectPricesList.Rows[i].Cells[2].FindControl("txtObjectPriceModal") as TextBox);
                TextBox txtObjectDiscountModal = (GrdObjectPricesList.Rows[i].Cells[3].FindControl("txtObjectDiscountModal") as TextBox);
                DropDownList drlObjectDiscountMakerModal = (GrdObjectPricesList.Rows[i].Cells[4].FindControl("drlObjectDiscountMakerModal") as DropDownList);

                decimal objectPrice, ObjectDiscount = 0;
                try { objectPrice = Config.ToDecimal(txtObjectPriceModal.Text); }
                catch 
                {
                    ltrObjPriceMessage.Text = Alert.DangerMessage("Məbləği düzgün daxil edin!");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectPricesModal();", true);
                    drlObjectDiscountMakerModal.Enabled = txtObjectPriceModal.Enabled = txtObjectDiscountModal.Enabled = true;
                    return;
                }
               
                try { ObjectDiscount = Config.ToDecimal(txtObjectDiscountModal.Text); }
                catch
                {
                    ltrObjPriceMessage.Text = Alert.DangerMessage("Endirim məbləğini düzgün daxil edin!");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectPricesModal();", true);
                    drlObjectDiscountMakerModal.Enabled = txtObjectPriceModal.Enabled = txtObjectDiscountModal.Enabled = true;
                    return;
                }
                
                if (ObjectDiscount > 0)
                {
                    if (drlObjectDiscountMakerModal.SelectedValue == "0")
                    {
                        ltrObjPriceMessage.Text = Alert.DangerMessage("Endirim edəni seçin!");
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectPricesModal();", true);
                        drlObjectDiscountMakerModal.Enabled = txtObjectPriceModal.Enabled = txtObjectDiscountModal.Enabled = true;
                        return;
                    }
                }

                /*if (ObjectDiscount > objectPrice)
                {
                    ltrObjPriceMessage.Text = Alert.DangerMessage("Məbləğ endirim məbləğindən kiçik olmaz");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectPricesModal();", true);
                    drlObjectDiscountMakerModal.Enabled = txtObjectPriceModal.Enabled = txtObjectDiscountModal.Enabled = true;
                    return;
                }*/

                int ObjectDiscountMaker = Convert.ToInt32(drlObjectDiscountMakerModal.SelectedValue);
                if (ObjectDiscount == 0) ObjectDiscountMaker = 0;

                dt.Rows.Add(ObjectID, objectPrice, ObjectDiscount, ObjectDiscountMaker, priceType, Convert.ToInt32(Session["LogonUserID"])); 
            }
        }

        if (dt.Rows.Count > 0)
        {
            string result = db.ChangeObjectPrices(dt);
            if (result.Equals("OK"))
            {
                ltrAlertMsg.Text = Alert.SuccessMessage("Məlumat yeniləndi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            }
            else
            {
                ltrAlertMsg.Text = Alert.DangerMessage("Xəta: " + result);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            }
        }
       
    }
    protected void lnkObjectPricesShow_Click(object sender, EventArgs e)
    {
        ltrObjPriceMessage.Text = "";
        LinkButton btn = (LinkButton)sender;
        int ObjectId = Convert.ToInt32(btn.CommandArgument);
        Session["ObjectIDSelected_Prices"] = ObjectId;
        FillPriceModal(ObjectId);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectPricesModal();", true);
    }
    
    protected void lnkObjectEdit_Click(object sender, EventArgs e)
    {
        ltrModalEditObject.Text = "";
        LinkButton lnk = (LinkButton)sender;
        int ObjectId = Convert.ToInt32(lnk.CommandArgument);
        Session["ObjectIDSelected_Edit"] = ObjectId;
        
        DataRow drObject = db.GetObjectByID(ObjectId);
        
        drlZoneEdit.SelectedValue = Convert.ToString(drObject["ZoneCode"]);

        FillLineForEdit(Convert.ToString(drObject["ZoneCode"]));

        drlLineEdit.SelectedValue = Convert.ToString(drObject["LineCode"]);

        FillCorpusForEdit(Convert.ToString(drObject["ZoneCode"]), Convert.ToString(drObject["LineCode"]));
        drlCorpusEdit.SelectedValue = Convert.ToString(drObject["CorpusCode"]);

        txtObjectNumberEdit.Text = Convert.ToString(drObject["ObjectNumber"]);
        drlObjectTypeEdit.SelectedValue = Convert.ToString(drObject["ObjectType"]);
        txtObjectAreaEdit.Text = Convert.ToString(drObject["ObjectArea"]);
        txtObjectContractAreaEdit.Text = Convert.ToString(drObject["ObjectAreaContract"]);
        drlObjectPositionEdit.SelectedValue = Convert.ToString(drObject["ObjectPosition"]);
        drlOwnerTypeEdit.SelectedValue = Convert.ToString(drObject["OwnerType"]).Trim() == "" ? "0" : Convert.ToString(drObject["OwnerType"]).Trim();
        txtObjectContractNumberEdit.Text = Convert.ToString(drObject["ObjectContractNumber"]);
        drlPaymnetTypeEdit.SelectedValue = Convert.ToString(drObject["PaymentType"]).Trim() == "" ? "" : Convert.ToString(drObject["PaymentType"]);
        drlIsVATEdit.SelectedValue = Convert.ToString(drObject["IsVAT"]).Trim() == "" ? "" : Convert.ToString(drObject["IsVAT"]);
        drlObjectStatusEdit.SelectedValue = Convert.ToString(drObject["ObjectStatus"]);
        TxtNoteEdit.Text = Convert.ToString(drObject["ObjectNote"]);



        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);

    }

    void FillZoneForEdit(int userID)
    {
        DataTable dtZoneEdit = db.GetZoneByAllowedUser(userID);
        drlZoneEdit.DataSource = dtZoneEdit;
        drlZoneEdit.DataBind();
        drlZoneEdit.Items.Insert(0, new ListItem("--Zona seçin", ""));
        drlLineEdit.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpusEdit.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }
    void FillLineForEdit(string ZoneID)
    {
        DataTable dtLineEdit = db.GetAllLines(ZoneID);
        drlLineEdit.DataSource = dtLineEdit;
        drlLineEdit.DataBind();
        drlLineEdit.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpusEdit.Items.Clear();
        drlCorpusEdit.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }

    void FillCorpusForEdit(string ZoneID, string LineID)
    {
        DataTable dtCorpusEdit = db.GetAllCorpus(ZoneID, LineID);
        drlCorpusEdit.DataSource = dtCorpusEdit;
        drlCorpusEdit.DataBind();
        drlCorpusEdit.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }


    protected void drlZoneEdit_SelectedIndexChanged(object sender, EventArgs e)
    {
        ltrModalEditObject.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);
        FillLineForEdit(Convert.ToString(drlZoneEdit.SelectedValue));     
    }
    protected void drlLineEdit_SelectedIndexChanged(object sender, EventArgs e)
    {
        ltrModalEditObject.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);
        FillCorpusForEdit(Convert.ToString(drlZoneEdit.SelectedValue), Convert.ToString(drlLineEdit.SelectedValue));   
    }
   
    protected void btnObjectEdit_Click(object sender, EventArgs e)
    {
        int ObjectID = Convert.ToInt32(Session["ObjectIDSelected_Edit"]);

        if (Session["ObjectIDSelected_Edit"] == null) Config.Rd("/exit");

        if (drlZoneEdit.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);
            ltrModalEditObject.Text = Alert.DangerMessage("Zonanı seçin!");
            return;
        }
        if (drlLineEdit.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);
            ltrModalEditObject.Text = Alert.DangerMessage("Sıranı seçin!");
            return;
        }
        if (drlCorpusEdit.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);
            ltrModalEditObject.Text = Alert.DangerMessage("Korpusu seçin!");
            return;
        }
        if (txtObjectNumberEdit.Text.Trim().Length == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);
            ltrModalEditObject.Text = Alert.DangerMessage("Obyekt nömrəsini daxil edin!");
            return;
        }
        if (drlObjectTypeEdit.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);
            ltrModalEditObject.Text = Alert.DangerMessage("Obyektin tipini seçin!");
            return;
        }
        decimal objectArea = 0;
        try
        {
            objectArea = Convert.ToDecimal(txtObjectAreaEdit.Text.Trim().Replace(",", "."));
        }
        catch
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);
            ltrModalEditObject.Text = Alert.DangerMessage("Obyektin sahəsini daxil edin!");
            return;
        }

        decimal objectAreaContract = 0;
        try
        {
            objectAreaContract = Convert.ToDecimal(txtObjectContractAreaEdit.Text.Trim().Replace(",", "."));
        }
        catch
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);
            ltrModalEditObject.Text = Alert.DangerMessage("Obyektin müqavilədəki sahəsini daxil edin!");
            return;
        }

        if (drlObjectPositionEdit.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);
            ltrModalEditObject.Text = Alert.DangerMessage("Obyektin yerləşmə tipini seçin!");
            return;
        }

        string objectNote = Convert.ToString(TxtNoteEdit.Text.Trim());

        if (drlObjectStatusEdit.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);
            ltrModalEditObject.Text = Alert.DangerMessage("Obyektin statusunu seçin!");
            return;
        }

        if (drlOwnerTypeEdit.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);
            ltrModalEditObject.Text = Alert.DangerMessage("Mülkiyyət tipini seçin!");
            return;
        }

        if (drlPaymnetTypeEdit.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Ödəmə tipini seçin!");
            return;
        }

        if (drlIsVATEdit.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddObjectModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Vergini seçin!");
            return;
        }

        string object_full_number = drlZoneEdit.SelectedValue + "-" + drlLineEdit.SelectedValue + "-" + drlCorpusEdit.SelectedValue + "-" + txtObjectNumberEdit.Text.Trim();

        string result = db.UpdateObject(ObjectID,
                                     Convert.ToString(drlZoneEdit.SelectedValue),
                                     Convert.ToString(drlLineEdit.SelectedValue),
                                     Convert.ToString(drlCorpusEdit.SelectedValue),
                                     txtObjectNumberEdit.Text.Trim(),
                                     object_full_number,
                                     drlObjectTypeEdit.SelectedValue,
                                     objectArea,
                                     objectAreaContract,
                                     drlObjectPositionEdit.SelectedValue,
                                     drlObjectStatusEdit.SelectedValue,
                                     drlOwnerTypeEdit.SelectedValue,
                                     txtObjectContractNumberEdit.Text,
                                     Convert.ToInt32(drlPaymnetTypeEdit.SelectedValue),
                                     Convert.ToInt32(drlIsVATEdit.SelectedValue),
                                     objectNote,
                                     "ACTIVE",
                                     Convert.ToInt32(Session["LogonUserID"]));
        if (!result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectEditModal();", true);
            ltrModalEditObject.Text = Alert.DangerMessage(result);
            return;
        }
        ObjectsSearchParams Osp = new ObjectsSearchParams(); 
        if (Session["ObjectsSearchParams"] != null) Osp = (ObjectsSearchParams)Session["ObjectsSearchParams"];
        FillObjectList(Osp, Session["ObjectsSearchParams"] != null ? false : true, Convert.ToInt32(Session["LogonUserID"]), false);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        ltrAlertMsg.Text = Alert.SuccessMessage("Məlumat yeniləndi");     
    }
    
    protected void btnDeleteObject_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        int Object = Convert.ToInt32(btn.CommandArgument);
        string result = db.DeleteObject(Object, Convert.ToInt32(Session["LogonUserID"]));
        if (result != "OK")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage("Error: (" + result + ")");
            return;
        }

        ObjectsSearchParams Osp = new ObjectsSearchParams();
        if (Session["ObjectsSearchParams"] != null) Osp = (ObjectsSearchParams)Session["ObjectsSearchParams"];
        FillObjectList(Osp, Session["ObjectsSearchParams"] != null ? false : true, Convert.ToInt32(Session["LogonUserID"]), false);
    }


    void FillCounterModal(int ObjectId)
    {
        GrdObjectCountersList.DataSource = db.GetCounterForSetNumber(ObjectId);
        GrdObjectCountersList.DataBind();
        GrdObjectCountersList.UseAccessibleHeader = true;
        if (GrdObjectCountersList.Rows.Count > 0) GrdObjectCountersList.HeaderRow.TableSection = TableRowSection.TableHeader;

    }


    protected void lnkObjectCounterShow_Click(object sender, EventArgs e)
    {
        ltrMsgObjectCounters.Text = "";
        LinkButton btn = (LinkButton)sender;
        int ObjectId = Convert.ToInt32(btn.CommandArgument);
        Session["ObjectIDSelected_Counter"] = ObjectId;
        FillCounterModal(ObjectId);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectCounterModal();", true);
    }
    protected void btnObjectCounterAccept_Click(object sender, EventArgs e)
    {
        if (Session["ObjectIDSelected_Counter"] == null) Config.Rd("/exit");
        int ObjectID = Convert.ToInt32(Session["ObjectIDSelected_Counter"]);
        ltrMsgObjectCounters.Text = "";
        DataTable dt = new DataTable("ObjectCounter");
        dt.Columns.Add("CounterNumber", typeof(string));
        dt.Columns.Add("ObjectID", typeof(int));
        dt.Columns.Add("CounterType", typeof(string));
        dt.Columns.Add("CMakerId", typeof(int));

        for (int i = 0; i < GrdObjectCountersList.Rows.Count; i++)
        {
            CheckBox Chk = (GrdObjectCountersList.Rows[i].Cells[0].FindControl("chkObjectCounterSelectModal") as CheckBox);
            if (Chk.Checked)
            {
                string CounterType = (string)GrdObjectCountersList.DataKeys[i]["CounterType"];
                TextBox txtObjectCounterNumberModal = (GrdObjectCountersList.Rows[i].Cells[2].FindControl("txtObjectCounterNumberModal") as TextBox);
                dt.Rows.Add(txtObjectCounterNumberModal.Text, ObjectID, CounterType, Convert.ToInt32(Session["LogonUserID"]));
            }
        }

        if (dt.Rows.Count > 0)
        {
            string result = db.ChangeObjectCounters(dt);
            if (result.Equals("OK"))
            {
                ltrAlertMsg.Text = Alert.SuccessMessage("Məlumat yeniləndi.");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            }
            else
            {
                ltrAlertMsg.Text = Alert.DangerMessage("Xəta: " + result);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            }
        }
    }
    protected void lnkObjectDetailsShow_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        int ObjectId = Convert.ToInt32(btn.CommandArgument);
        
        MView1.ActiveViewIndex = 1;
        DataRow drObject = db.GetObjectByID(ObjectId);
        ltrObjectFullNumber.Text = Convert.ToString(drObject["ObjectFullNumber"]);

        /*Begin Umumi melumat*/
        litZone.Text = Convert.ToString(drObject["ZoneDesc"]);
        litLine.Text = Convert.ToString(drObject["LineDesc"]);
        litCorpus.Text = Convert.ToString(drObject["CorpusDesc"]);

        litObjectNumber.Text = Convert.ToString(drObject["ObjectNumber"]);
        litObjectFullNumber.Text = Convert.ToString(drObject["ObjectFullNumber"]);
        litObjectType.Text = Convert.ToString(drObject["ObjectType"]);

        litObjectArea.Text = Convert.ToString(drObject["ObjectArea"]);
        litObjectAreaContract.Text = Convert.ToString(drObject["ObjectAreaContract"]);
        litObjectPositionType.Text = Convert.ToString(drObject["ObjectPosition"]);

        litObjectStatus.Text = Convert.ToString(drObject["ObjectStatus"]);
        litOwnerType.Text = Convert.ToString(drObject["OwnerType"]);


        litObjectCMakerName.Text = Convert.ToString(drObject["CMakerName"]);
        litObjectCMakerDate.Text = Convert.ToString(drObject["CMakerDateFormat"]);


        GrdObjectHistory.DataSource = db.GetObjectHistoryByID(ObjectId);
        GrdObjectHistory.DataBind();
        GrdObjectHistory.UseAccessibleHeader = true;
        if (GrdObjectHistory.Rows.Count > 0) GrdObjectHistory.HeaderRow.TableSection = TableRowSection.TableHeader;

        /*End Umumi melumat*/


        /*Begin Ohdelikler*/
       
        GrdObjectViewPrices.DataSource = db.GetObjectPricesVewByID(ObjectId);
        GrdObjectViewPrices.DataBind();
        GrdObjectViewPrices.UseAccessibleHeader = true;
        if (GrdObjectViewPrices.Rows.Count > 0) GrdObjectViewPrices.HeaderRow.TableSection = TableRowSection.TableHeader;

        GrdObjectViewPricesHistory.DataSource = db.GetObjectPricesHistoryVewByID(ObjectId);
        GrdObjectViewPricesHistory.DataBind();
        GrdObjectViewPricesHistory.UseAccessibleHeader = true;
        if (GrdObjectViewPricesHistory.Rows.Count > 0) GrdObjectViewPricesHistory.HeaderRow.TableSection = TableRowSection.TableHeader;

        /*End Ohdelikler*/



        /*Begin Counter*/

        GrdObjectViewCounters.DataSource = db.GetCountersByObjectID(ObjectId);
        GrdObjectViewCounters.DataBind();
        GrdObjectViewCounters.UseAccessibleHeader = true;
        if (GrdObjectViewCounters.Rows.Count > 0) GrdObjectViewCounters.HeaderRow.TableSection = TableRowSection.TableHeader;


        GrdObjectViewCountersHistory.DataSource = db.GetCountersHistoryByObjectID(ObjectId);
        GrdObjectViewCountersHistory.DataBind();
        GrdObjectViewCountersHistory.UseAccessibleHeader = true;
        if (GrdObjectViewCountersHistory.Rows.Count > 0) GrdObjectViewCountersHistory.HeaderRow.TableSection = TableRowSection.TableHeader;

        /*End Counter*/


        /*Begin Owners*/

        GrdObjectViewOwners.DataSource = db.GetOwnersObjectID(ObjectId);
        GrdObjectViewOwners.DataBind();
        GrdObjectViewOwners.UseAccessibleHeader = true;
        if (GrdObjectViewOwners.Rows.Count > 0) GrdObjectViewOwners.HeaderRow.TableSection = TableRowSection.TableHeader;

        GrdObjectViewOwnersHistory.DataSource = db.GetOwnersHistoryObjectID(ObjectId);
        GrdObjectViewOwnersHistory.DataBind();
        GrdObjectViewOwnersHistory.UseAccessibleHeader = true;
        if (GrdObjectViewOwnersHistory.Rows.Count > 0) GrdObjectViewOwnersHistory.HeaderRow.TableSection = TableRowSection.TableHeader;

        /*End Owners*/



        /*Begin Tenants*/

        /*Begin Owners*/

        GrdObjectViewTenant.DataSource = db.GetTenantByObjectId(ObjectId);
        GrdObjectViewTenant.DataBind();
        GrdObjectViewTenant.UseAccessibleHeader = true;
        if (GrdObjectViewTenant.Rows.Count > 0) GrdObjectViewTenant.HeaderRow.TableSection = TableRowSection.TableHeader;


        GrdObjectViewTenantHistory.DataSource = db.GetTenantHistoryByObjectId(ObjectId);
        GrdObjectViewTenantHistory.DataBind();
        GrdObjectViewTenantHistory.UseAccessibleHeader = true;
        if (GrdObjectViewTenantHistory.Rows.Count > 0) GrdObjectViewTenantHistory.HeaderRow.TableSection = TableRowSection.TableHeader;

        /*End Tenants*/




    }
    protected void lnkObjectBack_Click(object sender, EventArgs e)
    {
        MView1.ActiveViewIndex = 0;
    }
    protected void drlZoneBulkPrice_SelectedIndexChanged(object sender, EventArgs e)
    {
        ltrBulkPriceMessage.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectBulkPricesModal();", true);
        FillLineBulkPrice(Convert.ToString(drlZoneBulkPrice.SelectedValue));  
    }
    protected void drlLineBulkPrice_SelectedIndexChanged(object sender, EventArgs e)
    {
        ltrBulkPriceMessage.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectBulkPricesModal();", true);
        FillCorpusBulkPrice(Convert.ToString(drlZoneBulkPrice.SelectedValue), Convert.ToString(drlLineBulkPrice.SelectedValue)); 
    }
    protected void btnSetBulkPrice_Click(object sender, EventArgs e)
    {
        if (drlZoneBulkPrice.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectBulkPricesModal();", true);
            ltrBulkPriceMessage.Text = Alert.DangerMessage("Zonanı seçin!");
            return;
        }
        if (drlLineBulkPrice.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectBulkPricesModal();", true);
            ltrBulkPriceMessage.Text = Alert.DangerMessage("Sıranı seçin!");
            return;
        }

        if (drlCorpusBulkPrice.SelectedValue == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectBulkPricesModal();", true);
            ltrBulkPriceMessage.Text = Alert.DangerMessage("Korpusu seçin!");
            return;
        }

        if (drlObjectPositionBulkPrice.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectBulkPricesModal();", true);
            ltrBulkPriceMessage.Text = Alert.DangerMessage("Yerləşmə tipini seçin!");
            return;
        }

        decimal objectPrice;
        try { objectPrice = Config.ToDecimal(txtBulkPrice.Text); }
        catch
        {
            ltrBulkPriceMessage.Text = Alert.DangerMessage("Məbləği düzgün daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectBulkPricesModal();", true);
            return;
        }

        if (objectPrice <= 0)
        {
            ltrBulkPriceMessage.Text = Alert.DangerMessage("Məbləği daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectBulkPricesModal();", true);
            return;
        }


        decimal objectPriceDiscount;
        try { objectPriceDiscount = Config.ToDecimal(txtBulkDiscountPrice.Text); }
        catch
        {
            ltrBulkPriceMessage.Text = Alert.DangerMessage("Endirim məbləğini düzgün daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectBulkPricesModal();", true);
            return;
        }

        if (objectPriceDiscount < 0)
        {
            ltrBulkPriceMessage.Text = Alert.DangerMessage("Endirim məbləğini düzgün daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectBulkPricesModal();", true);
            return;
        }

        if (objectPriceDiscount > 0)
        {
            if (drlBulkDiscountMaker.SelectedValue == "0")
            {
                ltrBulkPriceMessage.Text = Alert.DangerMessage("Endirim edəni seçin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectBulkPricesModal();", true);
                return;
            }
        }

        DataTable dt = new DataTable("ObjectPriceBulk");

        int resultcount;

        dt = db.GetObjectListForBulkPrice(Convert.ToString(drlZoneBulkPrice.SelectedValue),
                                          Convert.ToString(drlLineBulkPrice.SelectedValue),
                                          Convert.ToString(drlCorpusBulkPrice.SelectedValue),
                                          drlObjectPositionBulkPrice.SelectedValue,
                                          objectPrice,
                                          objectPriceDiscount,
                                          Convert.ToInt32(drlBulkDiscountMaker.SelectedValue),
                                          Convert.ToInt32(Session["LogonUserID"]),
                                          out resultcount);

        if (resultcount == -1)
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Xəta baş verdi . Error Code : 19880");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        if (resultcount == 0)
        {
            ltrAlertMsg.Text = Alert.WarningMessage("Məlumat tapılmadı");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        if (dt.Rows.Count > 0)
        {
            string result = db.ChangeObjectPrices(dt);
            if (result.Equals("OK"))
            {
                ltrAlertMsg.Text = Alert.SuccessMessage("Məlumat yeniləndi. Obyekt sayı: " + resultcount.ToString());
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            }
            else
            {
                ltrAlertMsg.Text = Alert.DangerMessage("Xəta: " + result);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            }
        }

      
    }
   
    
   
    protected void drlZoneSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        ltrObjectSearchMessage.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectSearchModal();", true);
        FillLineSearch(Convert.ToString(drlZoneSearch.SelectedValue));  
    }
    protected void drlLineSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        ltrObjectSearchMessage.Text = "";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectSearchModal();", true);
        FillCorpusSearch(Convert.ToString(drlZoneSearch.SelectedValue), Convert.ToString(drlLineSearch.SelectedValue)); 
    }

    //Search Objects
    protected void btnObject_Search_Click(object sender, EventArgs e)
    {
        ObjectsSearchParams Osp = new ObjectsSearchParams();
        Osp.zoneCode = Convert.ToString(drlZoneSearch.SelectedValue);
        Osp.lineCode = Convert.ToString(drlLineSearch.SelectedValue);
        Osp.corpusCode = Convert.ToString(drlCorpusSearch.SelectedValue);
        Osp.objectNumber = txtObjectNumberSearch.Text.Trim();
        Osp.objectType = drlObjectTypeSearch.SelectedValue;


        decimal objectArea = 0.0m;
        if (txtObjectAreaSearch.Text.Trim() != "")
        {
            try { objectArea = Config.ToDecimal(txtObjectAreaSearch.Text.Trim()); }
            catch
            {
                ltrObjectSearchMessage.Text = Alert.DangerMessage("Obyekt sahəsini düzgün daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectSearchModal();", true);
                return;
            }
        }
        Osp.objectArea = objectArea;

        decimal objectAreaContract = 0.0m;
        if (txtObjectContractAreaSearch.Text.Trim() != "")
        {
            try { objectAreaContract = Config.ToDecimal(txtObjectContractAreaSearch.Text.Trim()); }
            catch
            {
                ltrObjectSearchMessage.Text = Alert.DangerMessage("Obyektin müqavilə sahəsini düzgün daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openObjectSearchModal();", true);
                return;
            }
        }
        Osp.objectAreaContract = objectAreaContract;

        Osp.objectPosition = drlObjectPositionSearch.SelectedValue;
        Osp.ownerType = drlOwnerTypeSearch.SelectedValue;
        Osp.objectStatus = drlObjectStatusSearch.SelectedValue;

        Session["ObjectsSearchParams"] = Osp;

        FillObjectList(Osp, false, Convert.ToInt32(Session["LogonUserID"]),false);
        
    }
    protected void lnkDownloadExcel_Click(object sender, EventArgs e)
    {
        ObjectsSearchParams Osp = new ObjectsSearchParams();
        if (Session["ObjectsSearchParams"] != null) Osp = (ObjectsSearchParams)Session["ObjectsSearchParams"];
        FillObjectList(Osp, Session["ObjectsSearchParams"] != null ? false : true, Convert.ToInt32(Session["LogonUserID"]), true);
    }
}