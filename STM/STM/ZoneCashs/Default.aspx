﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="ZoneCashs_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet" />
    <link href="../lib/spectrum/spectrum.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">Zona üzrə kassa</span>
        </nav>

        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>KASSA</h5>
            </div>
            <div class="card pd-20 pd-sm-40">


                <!-- SMALL MODAL -->
                <div id="alertmodal" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                            <div class="modal-footer justify-content-right" style="padding: 5px">
                                <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->


                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

                        <!-- LARGE MODAL -->
                        <div id="modalTransferCash" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">&nbsp&nbspKASSA TRANSFERİ</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrCashTransferMessage" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">

                                            <div class="row mg-t-20">
                                                <div class="col-lg">
                                                    Kassadakı məbləğ:
                                                    <asp:TextBox ID="txtCashAmount" Enabled="false" runat="server" MaxLength="10" class="form-control" type="text" placeholder="Kassadakı məbləğ"></asp:TextBox>
                                                </div>

                                                <div class="col-lg">
                                                    Göndəriləcək məbləğ:
                                                    <asp:TextBox ID="txtTransferAmount" runat="server" MaxLength="10" class="form-control" type="text" placeholder="Göndəriləcək məbləğ"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row mg-t-20">
                                                <div class="col-lg">
                                                    Qeyd:  
                                                    <asp:TextBox runat="server" ID="txtNote" MaxLength="100" Rows="3" class="form-control" placeholder="Qeyd" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                                <!-- col -->
                                            </div>
                                            <!-- row -->
                                        </div>
                                        <div class="modal-footer">
                                            <img id="transfer_loading" style="display: none" src="../img/loader.gif" />
                                            <asp:Button ID="btnTransferCash" class="btn btn-info pd-x-20" runat="server" Text="Göndər"
                                                OnClick="btnTransferCash_Click"
                                                OnClientClick="this.style.display = 'none';
														   document.getElementById('transfer_loading').style.display = '';
														   document.getElementById('btnTransferCashCancel').style.display = 'none';
														   document.getElementById('alert_msg').style.display = 'none';" />
                                            <button id="btnTransferCashCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- modal-dialog -->
                            </div>
                            <!-- modal -->
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnTransferCash" />
                    </Triggers>
                </asp:UpdatePanel>


                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <asp:LinkButton ID="lnkCash" OnClick="lnkCash_Click" runat="server"><b>Zona üzrə kassa</b></asp:LinkButton>
                    </li>
                    <li class="nav-item">
                        <asp:LinkButton ID="lnkZonePayments" OnClick="lnkZonePayments_Click" runat="server"><b>Zona üzrə ödənişlər</b></asp:LinkButton>
                    </li>
                </ul>
                <asp:MultiView ID="MView" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <br />
                        <asp:Literal ID="ltrCashTotal" runat="server"></asp:Literal>
                        <br />
                        <br />
                        <div class="table-responsive">
                            <asp:GridView ID="GrdCashs" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" GridLines="None">
                                <Columns>
                                    <asp:BoundField DataField="ZoneName" HeaderText="Zona" />
                                    <asp:BoundField DataField="Code" HeaderText="Kassa kodu" />
                                    <asp:BoundField DataField="CashName" HeaderText="Kassa adı" />
                                    <asp:BoundField DataField="Amount" HeaderText="MƏBLƏĞ" />

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkTransfer"
                                                OnClick="lnkTransfer_Click"
                                                CssClass="btn btn-primary" runat="server"
                                                CommandArgument='<%#Convert.ToString(Eval("ID")) + "#" +  Convert.ToString(Eval("Amount")) %>'>
                                                Transfer
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" HorizontalAlign="Right" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>


                        <br />
                        <br />
                        <br />
                        <br />
                        <div class="card">
                            <h5 class="card-header">
                                <asp:Literal ID="ltrTransferStatus" runat="server"></asp:Literal></h5>
                            <div class="card-body">

                                <div class="table-responsive">
                                    <asp:GridView ID="GrdTransferStatus" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" GridLines="None">
                                        <Columns>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    Kassa kodu: <%#Eval("CashCode") %>
                                                    <br />
                                                    Kassa adı: <%#Eval("CashName") %>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    KASSA
                                                </HeaderTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="Amount" HeaderText="MƏBLƏĞ" />

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    S.A.A : <%#Eval("SenderMakerName") %>
                                                    <br />
                                                    TARİX : <%#Eval("SenderDateFormat") %>
                                                    <br />
                                                    QEYD : <%#Eval("SenderNote") %>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    GÖNDƏRƏN
                                                </HeaderTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    S.A.A : <%#Eval("AcceptorMakerName") %>
                                                    <br />
                                                    TARİX : <%#Eval("AcceptorDateFormat") %>
                                                    <br />
                                                    QEYD : <%#Eval("AcceptorNote") %>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    QƏBUL EDƏN
                                                </HeaderTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <img src="../img/<%#Eval("TransactionStatus")%>.png" />
                                                    <%#Eval("TransactionStatusDesc") %>
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    STATUS
                                                </HeaderTemplate>
                                                <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>

                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <br />
                          <div class="row">
                            <div class="input-group">                
                                    <span class="input-group-addon" style="margin-left:12px" ><i class="icon ion-calendar tx-16 lh-0 op-6"></i></span>                            
                                    <asp:TextBox ID="txtFromdate" type="text" class="form-control fc-datepicker" placeholder="dd.mm.yyyy" runat="server" MaxLength="10"></asp:TextBox>                                 
                                    <span class="input-group-addon" style="margin-left:12px"><i class="icon ion-calendar tx-16 lh-0 op-6"></i></span>
                                    <asp:TextBox ID="txtToDate" type="text" class="form-control fc-datepicker" placeholder="dd.mm.yyyy" runat="server" MaxLength="10"></asp:TextBox>                     
                                    <asp:DropDownList ID="drlZone" style="margin-left:12px"  DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server"  AutoPostBack="true"></asp:DropDownList>             
                                    <asp:DropDownList ID="drlPaymentType" style="margin-left:12px" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server"  AutoPostBack="true"></asp:DropDownList>                                                    
                            </div>
                            <asp:LinkButton ID="lnkSearch" runat="server" style="margin:12px"
                                CssClass="btn btn-outline-info" OnClick="lnkSearch_Click">
                                   <i class="icon ion-search"></i> Axtar
                                  </asp:LinkButton>
                        </div>

                        <br />
                        <asp:Literal ID="ltrTotalPayment" runat="server"></asp:Literal>
                        <br />
                        <br />
                        <div class="table-responsive">
                            <asp:GridView ID="GrdZonePayment" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" GridLines="None">
                                <Columns>
                                   <asp:BoundField DataField="PaymentDate" HeaderText="TARİX" />
                                    <asp:BoundField DataField="CMakerName" HeaderText="İSTİFADƏÇİ" />
                                    <asp:BoundField DataField="PaymentTypeDesc" HeaderText="ÖDƏNİŞİN TİPİ">
                                        <ItemStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TranType" HeaderText="ƏMƏLİYYAT TİPİ" />
                                    <asp:BoundField DataField="PaymentAmount" HeaderText="MƏBLƏĞ" />
                                </Columns>
                            </asp:GridView>
                        </div>

                    </asp:View>
                </asp:MultiView>


            </div>
        </div>

        <script type="text/javascript">
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }

            function openTransferCash() {
                $('#modalTransferCash').modal({ show: true });
            }
        </script>



        <script src="../lib/jquery/jquery.js"></script>

        <style>
            .ui-datepicker {
                z-index: 1151 !important; /* has to be larger than 1050 */
            }
            .payment_print_tab tbody td  {
             font-family: Tahoma;
             font-size: 10px;
            }
             .table-bordered, tbody tr th, table td{
                border-bottom: 1px solid #0b2254 !important;
                background:#fff !important;
                color:#000000 !important;
            }
        </style>

        <script type="text/javascript">
            $(function () {
                'use strict';
                // Datepicker
                $('.fc-datepicker').datepicker({
                    dateFormat: 'dd.mm.yy',
                    showOtherMonths: true,
                    selectOtherMonths: true
                });
            });
        </script>

        <style>
            .card {
                border:1px solid #dadada;
            }
        </style>

    </div>
</asp:Content>

