﻿using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ZoneCashs_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");

        if (!IsPostBack)
        {
            lnkCash.CssClass = "nav-link active";
            lnkZonePayments.CssClass = "nav-link";
            FillCashsByUser(Convert.ToInt32(Session["LogonUserID"]));
            FillCashsTransaction(Convert.ToInt32(Session["LogonUserID"]));
           
        }

        string FUNCTION_ID = "ZONE_CASHS_MAIN_PAGE";

        /*Begin Permission*/
          db.CheckFunctionPermission(Convert.ToInt32(Session["LogonUserID"]), FUNCTION_ID);
        /*End Permission*/

    }
    protected void lnkCash_Click(object sender, EventArgs e)
    {
        MView.ActiveViewIndex = 0;
        lnkCash.CssClass = "nav-link active";
        lnkZonePayments.CssClass = "nav-link";
        FillCashsByUser(Convert.ToInt32(Session["LogonUserID"]));
    }
    protected void lnkZonePayments_Click(object sender, EventArgs e)
    {
        MView.ActiveViewIndex = 1;
        lnkCash.CssClass = "nav-link";
        lnkZonePayments.CssClass = "nav-link active";
        txtFromdate.Text = Config.HostingTime.ToString("dd.MM.yyyy");
        txtToDate.Text = Config.HostingTime.ToString("dd.MM.yyyy");
        FillPaymentByZone(Convert.ToInt32(Session["LogonUserID"]), Config.ToDate(txtFromdate.Text),
         Config.ToDate(txtToDate.Text), drlZone.SelectedValue, drlPaymentType.SelectedValue);
        FillZone(Convert.ToInt32(Session["LogonUserID"]));
        FillPaymentList(Convert.ToInt32(Session["LogonUserID"]));
    }
    void FillZone(int userID)
    {
        DataTable dtZone = db.GetZoneByAllowedUser(userID);
        drlZone.DataSource = dtZone;
        drlZone.DataBind();
        drlZone.Items.Insert(0, new ListItem("--Zona seçin", ""));
    }
    void FillPaymentList(int userID)
    {
        DataTable dtPaymentTypeList = db.GetPaymentTypeList(userID);
        drlPaymentType.DataSource = dtPaymentTypeList;
        drlPaymentType.DataBind();
        drlPaymentType.Items.Insert(0, new ListItem("--seçin", ""));
    }
    void FillPaymentByZone(int UserID, DateTime dateFrom, DateTime dateTo, string zoneCode, string paymentTypeCode)
    {
        ltrTotalPayment.Text = "";
        DataTable dtt = new DataTable("FillPaymentByZone");
        dtt = db.GetUserPaymentsByZone(UserID, dateFrom, dateTo, zoneCode, paymentTypeCode);
        GrdZonePayment.DataSource = dtt;
        GrdZonePayment.DataBind();
        GrdZonePayment.UseAccessibleHeader = true;
        if (GrdZonePayment.Rows.Count > 0)
        {
            GrdZonePayment.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        string tranType = "";
        decimal paid_total = 0.0m;
        decimal discount_total = 0.0m;
        decimal reduction_total = 0.0m;
        for (int i = 0; i < GrdZonePayment.Rows.Count; i++)
        {
            tranType = dtt.Rows[i][2].ToString();
            if (tranType == "ODEME")
            {
                paid_total += Config.ToDecimal(Convert.ToString(dtt.Rows[i]["PaymentAmount"]));
            }
            if (tranType == "GUZESHT")
            {
                discount_total += Config.ToDecimal(Convert.ToString(dtt.Rows[i]["PaymentAmount"]));
            }
            if (tranType == "SILINME")
            {
                reduction_total += Config.ToDecimal(Convert.ToString(dtt.Rows[i]["PaymentAmount"]));
            }
        }

        if (dtt.Rows.Count == 0)
        {
            ltrAlertMsg.Text = Alert.WarningMessage("Məlumat tapılmadı.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }
        ltrTotalPayment.Text = string.Format("<span class=\"badge badge-success\" style = \"margin-top:5px\"> </span><b>CƏM ÖDƏMƏ: {0}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>" +
                                             "<span class=\"badge badge-info\" style = \"margin-top:5px\"> </span><b>CƏM GÜZƏŞT: {1}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>" +
                                             "<span class=\"badge badge-warning\" style = \"margin-top:5px\"> </span><b>CƏM SİLİNMƏ: {2} </b>"
                                            , paid_total, discount_total, reduction_total);
    }

    void FillCashsByUser(int UserID)
    {
        ltrCashTotal.Text = "";
        DataTable dtt = new DataTable("FillCashsByUser");
        dtt = db.GetCashAmountByUserID(UserID);
        GrdCashs.DataSource = dtt;
        GrdCashs.DataBind();
        GrdCashs.UseAccessibleHeader = true;
        if (GrdCashs.Rows.Count > 0)
        {
            GrdCashs.HeaderRow.TableSection = TableRowSection.TableHeader;
            /*Begin permission*/
            GrdCashs.Columns[4].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "TRANSFER_FROM_ZONE_CASHS");
            /*End permission*/
        }

        decimal paid_total = 0.0m;
        for (int i = 0; i < GrdCashs.Rows.Count; i++)
        {
            paid_total += Config.ToDecimal(Convert.ToString(dtt.Rows[i]["Amount"]));
        }

        if (dtt.Rows.Count == 0)
        {
            ltrAlertMsg.Text = Alert.WarningMessage("Məlumat tapılmadı.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        ltrCashTotal.Text = string.Format("<span class=\"badge badge-success\" style = \"margin-top:5px\"> </span><b>Ümumi məbləğ: {0}</b>", paid_total);
    }


    protected void lnkSearch_Click(object sender, EventArgs e)
    {
        if (!Config.isDate(txtFromdate.Text))
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Tarix formatını düzgün daxil edin ! (məs : 22.10.2015)");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }
        if (!Config.isDate(txtToDate.Text))
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Tarix formatını düzgün daxil edin ! (məs : 22.10.2015)");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        FillPaymentByZone(Convert.ToInt32(Session["LogonUserID"]), Config.ToDate(txtFromdate.Text),
         Config.ToDate(txtToDate.Text), drlZone.SelectedValue, drlPaymentType.SelectedValue);
    }
    protected void lnkTransfer_Click(object sender, EventArgs e)
    {
        ltrCashTransferMessage.Text = "";
        txtTransferAmount.Text = "";
        txtNote.Text = "";
        LinkButton lnk = (LinkButton)sender;
        string[] data = Convert.ToString(lnk.CommandArgument).Split('#');

        if (data.Length != 2)
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Xəta baş verdi");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        int CashID = Convert.ToInt32(data[0]);
        decimal Amount = Config.ToDecimal(data[1]);

        Session["C_CashID"] = CashID;
        Session["C_Amount"] = Amount;
        txtCashAmount.Text = Amount.ToString();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openTransferCash();", true);
    }
    protected void btnTransferCash_Click(object sender, EventArgs e)
    {
        if (Session["C_CashID"] == null) Config.Rd("/exit");
        int CashID = Convert.ToInt32(Session["C_CashID"]);

        decimal transfer_amount = 0,cash_amount = 0;
        try { transfer_amount = Config.ToDecimal(txtTransferAmount.Text.Trim()); }
        catch
        {
            ltrCashTransferMessage.Text = Alert.DangerMessage("Transfer məbləğini düzgün daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openTransferCash();", true);
            return;
        }

        try { cash_amount = Config.ToDecimal(Convert.ToString(Session["C_Amount"])); }
        catch
        {
            ltrCashTransferMessage.Text = Alert.DangerMessage("Xəta baş verdi. Xəta kodu 1988");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openTransferCash();", true);
            return;
        }

        if (transfer_amount > cash_amount)
        {
            ltrCashTransferMessage.Text = Alert.WarningMessage("Transfer olunacaq məbləğ kassadakı məbləğdən böyük olmaz.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openTransferCash();", true);
            return;
        }


        string result = db.SendCashTransaction(CashID, transfer_amount,txtNote.Text.Trim(), Convert.ToInt32(Session["LogonUserID"]));
        if (!result.Equals("OK"))
        {
            ltrCashTransferMessage.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openTransferCash();", true);
            return;
        }

        if (result.Equals("OK"))
        {
            ltrAlertMsg.Text = Alert.SuccessMessage("Transfer olundu");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }

        FillCashsTransaction(Convert.ToInt32(Session["LogonUserID"]));
    }


    void FillCashsTransaction(int UserID)
    {
        ltrTransferStatus.Text = "";
        DataTable dtt = new DataTable("FillCashsTransaction");
        dtt = db.GetTransferStatus(UserID);
        GrdTransferStatus.DataSource = dtt;
        GrdTransferStatus.DataBind();
        GrdTransferStatus.UseAccessibleHeader = true;
        if (GrdTransferStatus.Rows.Count > 0)
        {
            GrdTransferStatus.HeaderRow.TableSection = TableRowSection.TableHeader;
            ltrTransferStatus.Text = "Transferlərin statusu";
        }
    }
}