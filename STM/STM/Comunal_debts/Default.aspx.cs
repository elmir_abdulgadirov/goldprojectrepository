﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Comunal_payments_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");

        if (!IsPostBack)
        {
            lnkSearchObjectCode.CssClass = "nav-link active";
            lnkSearchZoneCode.CssClass = "nav-link";
            FillZone(Convert.ToInt32(Session["LogonUserID"]));
        }

        string FUNCTION_ID = "ADD_COUNTER_VALUE";

        /*Begin Permission*/
        db.CheckFunctionPermission(Convert.ToInt32(Session["LogonUserID"]), FUNCTION_ID);

        /*End Permission*/
    }

    void FillZone(int userID)
    {
        DataTable dtZone = db.GetZoneByAllowedUser(userID);
        drlZone.DataSource = dtZone;
        drlZone.DataBind();
        drlZone.Items.Insert(0, new ListItem("--Zona seçin", ""));
        drlLine.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpus.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }
    void FillLine(string ZoneID)
    {
        DataTable dtLine = db.GetAllLines(ZoneID);
        drlLine.DataSource = dtLine;
        drlLine.DataBind();
        drlLine.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpus.Items.Clear();
        drlCorpus.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }

    void FillCorpus(string ZoneID, string LineID)
    {
        DataTable dtCorpus = db.GetAllCorpus(ZoneID, LineID);
        drlCorpus.DataSource = dtCorpus;
        drlCorpus.DataBind();
        drlCorpus.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }
    protected void lnkSearchObjectCode_Click(object sender, EventArgs e)
    {
        MViewSearch.ActiveViewIndex = 0;
        lnkSearchObjectCode.CssClass = "nav-link active";
        lnkSearchZoneCode.CssClass = "nav-link";
    }
    protected void lnkSearchZoneCode_Click(object sender, EventArgs e)
    {
        txtSearchObjectFullNumber.Text = "";
        MViewSearch.ActiveViewIndex = 1;
        lnkSearchObjectCode.CssClass = "nav-link";
        lnkSearchZoneCode.CssClass = "nav-link active";
    }
    protected void drlZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillLine(Convert.ToString(drlZone.SelectedValue));
    }
    protected void drlLine_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCorpus(Convert.ToString(drlZone.SelectedValue), Convert.ToString(drlLine.SelectedValue));
    }


    void FillPointer(int UserID, string pObjectFullNumber, string pZoneCode, string pLineCode, string pCorpusCode)
    {
        DataTable dt = new DataTable("FillPointer");
        dt = db.GetComunalPointers(UserID, pObjectFullNumber, pZoneCode, pLineCode, pCorpusCode);
        GrdComunalPointer.DataSource = dt;
        GrdComunalPointer.DataBind();
        GrdComunalPointer.UseAccessibleHeader = true;
        if (GrdComunalPointer.Rows.Count > 0) GrdComunalPointer.HeaderRow.TableSection = TableRowSection.TableHeader;



        if (dt.Rows.Count == 0)
        {
            ltrAlertMsg.Text = Alert.WarningMessage("Məlumat tapılmadı.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        /*Begin permission*/
        if (GrdComunalPointer.Rows.Count > 0)
        {
            GrdComunalPointer.Columns[2].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "CHANGE_COMUNAL_POINTER");
        }
        /*End permission*/
    }
    protected void btnSearchByObjectCode_Click(object sender, EventArgs e)
    {
        GrdComunalPointer.DataSource = null;
        GrdComunalPointer.DataBind();
        if (!Config.CheckObjectCode(txtSearchObjectFullNumber.Text.Trim().Replace(" ", "")))
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Obyekt kodunu düzgün daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        Session["SEARCH_DEBT_CMEKER_ID_COUNTER"] = Convert.ToInt32(Session["LogonUserID"]);
        Session["SEARCH_DEBT_OBJECT_FULL_NUMBER_COUNTER"] = txtSearchObjectFullNumber.Text.Trim().Replace(" ", "");
        Session["SEARCH_DEBT_ZONE_CODE_COUNTER"] = "";
        Session["SEARCH_DEBT_LINE_CODE_COUNTER"] = "";
        Session["SEARCH_DEBT_CORPUS_CODE_COUNTER"] = "";

        FillPointer(Convert.ToInt32(Session["SEARCH_DEBT_CMEKER_ID_COUNTER"]),
                        Convert.ToString(Session["SEARCH_DEBT_OBJECT_FULL_NUMBER_COUNTER"]),
                        Convert.ToString(Session["SEARCH_DEBT_ZONE_CODE_COUNTER"]),
                        Convert.ToString(Session["SEARCH_DEBT_LINE_CODE_COUNTER"]),
                        Convert.ToString(Session["SEARCH_DEBT_CORPUS_CODE_COUNTER"]));
    }
    protected void btnSearchByZoneCode_Click(object sender, EventArgs e)
    {
        GrdComunalPointer.DataSource = null;
        GrdComunalPointer.DataBind();
        if (drlZone.SelectedValue == "0")
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Zona seçin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }


        Session["SEARCH_DEBT_CMEKER_ID_COUNTER"] = Convert.ToInt32(Session["LogonUserID"]);
        Session["SEARCH_DEBT_OBJECT_FULL_NUMBER_COUNTER"] = "";
        Session["SEARCH_DEBT_ZONE_CODE_COUNTER"] = Convert.ToString(drlZone.SelectedValue);
        Session["SEARCH_DEBT_LINE_CODE_COUNTER"] = Convert.ToString(drlLine.SelectedValue);
        Session["SEARCH_DEBT_CORPUS_CODE_COUNTER"] = Convert.ToString(drlCorpus.SelectedValue);

        FillPointer(Convert.ToInt32(Session["SEARCH_DEBT_CMEKER_ID_COUNTER"]),
                        Convert.ToString(Session["SEARCH_DEBT_OBJECT_FULL_NUMBER_COUNTER"]),
                        Convert.ToString(Session["SEARCH_DEBT_ZONE_CODE_COUNTER"]),
                        Convert.ToString(Session["SEARCH_DEBT_LINE_CODE_COUNTER"]),
                        Convert.ToString(Session["SEARCH_DEBT_CORPUS_CODE_COUNTER"]));
    }
    protected void lnkChangePointer_Click(object sender, EventArgs e)
    {
        ltrCounterPointerMessage.Text = "";
        txtCounterCurrentPointer.Text = "";
        LinkButton lnk = (LinkButton)sender;
        string[] data = Convert.ToString(lnk.CommandArgument).Split('#');

        if (data.Length != 4)
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Xəta baş verdi");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        string CounterNumber = data[0];
        string CounterType = data[1];
        decimal Counter_Value1 = Config.ToDecimal(data[2]);
        decimal Counter_Value2 = Config.ToDecimal(data[3]);

        Session["E_CounterNumber"] = CounterNumber;
        Session["E_CounterType"] = CounterType;


        txtCounterLastPointer.Text = Counter_Value2.ToString();
        litUnite1.Text = CounterType == "ELECTRIC" ? "<b>KW</b>" : "<b>m<sup>3</sup></b>";
        litUnite2.Text = CounterType == "ELECTRIC" ? "<b>KW</b>" : "<b>m<sup>3</sup></b>";

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCounterPointerModal();", true);
    }
    protected void btnChangeCPointer_Click(object sender, EventArgs e)
    {
        ltrCounterPointerMessage.Text = "";
        if (Session["E_CounterNumber"] == null) Config.Rd("/exit");
        if (Session["E_CounterType"] == null) Config.Rd("/exit");

        string CounterNumber = Convert.ToString(Session["E_CounterNumber"]);
        string CounterType = Convert.ToString(Session["E_CounterType"]);


        if (Session["SEARCH_DEBT_CMEKER_ID_COUNTER"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_OBJECT_FULL_NUMBER_COUNTER"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_ZONE_CODE_COUNTER"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_LINE_CODE_COUNTER"] == null) Config.Rd("/exit");
        if (Session["SEARCH_DEBT_CORPUS_CODE_COUNTER"] == null) Config.Rd("/exit");


        decimal LastPointer = 0;
        try { LastPointer = Config.ToDecimal(txtCounterLastPointer.Text.Trim()); }
        catch
        {
            ltrCounterPointerMessage.Text = Alert.DangerMessage("Son göstərici düzgün deyil.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCounterPointerModal();", true);
            return;
        }

        decimal currentPointer = 0;
        try { currentPointer = Config.ToDecimal(txtCounterCurrentPointer.Text.Trim()); }
        catch
        {
            ltrCounterPointerMessage.Text = Alert.DangerMessage("Cari göstəricini düzgün daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCounterPointerModal();", true);
            return;
        }



        if (LastPointer >= currentPointer)
        {
            ltrCounterPointerMessage.Text = Alert.WarningMessage("Cari göstərici son göstəricidən böyük deyil.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCounterPointerModal();", true);
            return;
        }

        string result = db.CreateComunalDebt(CounterNumber, CounterType, currentPointer, Convert.ToInt32(Session["LogonUserID"]));
        if (!result.Equals("OK"))
        {
            ltrCounterPointerMessage.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openCounterPointerModal();", true);
            return;
        }

        if (result.Equals("OK"))
        {
            ltrAlertMsg.Text = Alert.SuccessMessage("Göstərici yeniləndi.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }

        FillPointer(Convert.ToInt32(Session["SEARCH_DEBT_CMEKER_ID_COUNTER"]),
                        Convert.ToString(Session["SEARCH_DEBT_OBJECT_FULL_NUMBER_COUNTER"]),
                        Convert.ToString(Session["SEARCH_DEBT_ZONE_CODE_COUNTER"]),
                        Convert.ToString(Session["SEARCH_DEBT_LINE_CODE_COUNTER"]),
                        Convert.ToString(Session["SEARCH_DEBT_CORPUS_CODE_COUNTER"]));
    }
    protected void lnkPay_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        string object_code = lnk.CommandArgument.Trim();
        Config.Rd("/paymentsComunal?objectcode=" + object_code);
    }
    protected void lnkDebtPrint_Click(object sender, EventArgs e)
    {
        ltrPrintData.Text = "";
        LinkButton lnk = (LinkButton)sender;
        string counter_number = Convert.ToString(lnk.CommandArgument);

        DataTable dt = db.GetDebtDetailsByCounterNumber(counter_number);
        if (dt == null || dt.Rows.Count == 0)
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Çap prosesində xəta baş verdi.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }
        DataRow dr = dt.Rows[0];

        string print_format = "<div class=\"card\" id=\"debt_print\">" +
                                                "<h6 class=\"card-header\" style =\"text-align:center\">" + Convert.ToString(dr["PType"]) + " BİLDİRİŞİ</h5>" +
                                                "<div class=\"card-body\">" +
                                                    "<h6 class=\"card-body-title\">Müştəri İD: " + Convert.ToString(dr["RegNumber"]) + "</h6>" +
                                                    "<h6 class=\"card-body-title\">Obyekt kodu: " + Convert.ToString(dr["ObjectFullNumber"]) + "</h6>" +
                                                    "<h6 class=\"mg-b-0\">Tarix/Saat: " + Convert.ToString(Config.HostingTime.ToString("yyyy-MM-dd hh:mm:ss")) + "</h6>" +
                                                    "<hr />" +
                                                    "<table class=\"table payment_print_tab\">" +
                                                        "<tbody>" +
                                                            "<tr>" +
                                                                "<td>" +
                                                                    "Ilk Göstərici:" +
                                                                "</td>" +
                                                                "<td>" +
                                                                    "" + Convert.ToString(dr["Counter_Value1"]) + " KW" +
                                                                "</td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td>" +
                                                                    "Son Göstərici:" +
                                                                "</td>" +
                                                                "<td>" +
                                                                    "" + Convert.ToString(dr["Counter_Value2"]) + " KW" +
                                                                "</td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td>" +
                                                                    "Borc məbləğ:" +
                                                                "</td>" +
                                                                "<td>" +
                                                                    "" + Convert.ToString(dr["Calculated_amount"]) + " AZN" +
                                                                "</td>" +
                                                            "</tr>" +
                                                            /*  "<tr>" +
                                                                  "<td>" +
                                                                      "<h6 class=\"tx-inverse mg-b-0\">Qalıq borc:</h6>" +
                                                                  "</td>" +
                                                                  "<td>" +
                                                                      "<h6 class=\"tx-inverse mg-b-0\">" + Convert.ToString(dr["Remain_Amount"]) + " AZN</h6>" +
                                                                  "</td>" +
                                                              "</tr>" +*/
                                                            /*"<tr>" +
                                                                "<td>" +
                                                                    "Ödəniş:" +
                                                                "</td>" +
                                                                "<td>" +
                                                                     Convert.ToString(dr["Fullname"]) +
                                                                "</td>" +
                                                            "</tr>" +
                                                            "<tr>" +
                                                                "<td>" +
                                                                    "Borc tarixi:" +
                                                                "</td>" +
                                                                "<td>" +
                                                                    "" + Convert.ToString(dr["DebtDate"]) + "" +
                                                                "</td>" +
                                                            "</tr>" +*/
                                                            "<tr>" +
                                                                "<td>" +
                                                                    "Borc tarixi:" +
                                                                "</td>" +
                                                                "<td>" +
                                                                    "" + Convert.ToString(dr["DebtDate"]) + "" +
                                                                "</td>" +
                                                            "</tr>" +
                                                        "</tbody>" +
                                                    "</table>" +
                                                "</div>" +
                                                "<style>" +
                                                ".card {border: 1px solid #dadada;}" +
                                                "</style>" +
                                               "</div>";
        ltrPrintData.Text = print_format;
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "PrintDebt();", true);
    }
}