﻿<%@ Page Title="Komunal" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Comunal_payments_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">Komunal Borclar</span>
        </nav>

        <div class="sl-pagebody">
            <div class="card pd-20 pd-sm-40">

                <!-- SMALL MODAL -->
                <div id="alertmodal" class="modal fade">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                            <div class="modal-footer justify-content-right" style="padding: 5px">
                                <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->

                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

                        <!-- LARGE MODAL -->
                        <div id="modalCounterPointer" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">&nbsp&nbspSAYĞAC GÖSTƏRİCİLƏRİ</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrCounterPointerMessage" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">
                                            <div class="row mg-t-20">
                                                <div class="col-lg">
                                                    Son göstərici (<asp:Literal ID="litUnite1" runat="server"></asp:Literal>):
                                                    <asp:TextBox ID="txtCounterLastPointer" Enabled="false" runat="server" MaxLength="50" class="form-control" type="number"></asp:TextBox>
                                                </div>

        
                                            </div>
                                            <div class="row mg-t-20">
                                                <div class="col-lg">
                                                    Cari göstərici(<asp:Literal ID="litUnite2" runat="server"></asp:Literal>):
                                                    <asp:TextBox ID="txtCounterCurrentPointer" runat="server" MaxLength="50" class="form-control" type="number"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row mg-t-20">
                                                <div class="col-lg" style="float: left">
                                                    <asp:Button ID="btnChangeCPointer" OnClick ="btnChangeCPointer_Click" CssClass="btn btn-primary" runat="server" Text="Yenilə" />
                                                    <button id="btnChangeCPointerCancel" style="margin-left: 10px" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                                </div>
                                                <div style="clear: both"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->


                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnChangeCPointer" />
                    </Triggers>
                </asp:UpdatePanel>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <asp:LinkButton ID="lnkSearchObjectCode" OnClick="lnkSearchObjectCode_Click" runat="server"><b>Obyekt kodu üzrə</b></asp:LinkButton>
                    </li>
                    <li class="nav-item">
                        <asp:LinkButton ID="lnkSearchZoneCode" OnClick="lnkSearchZoneCode_Click" runat="server"><b>Zona / Sıra / Korpus üzrə</b></asp:LinkButton>
                    </li>
                </ul>

                <asp:MultiView ID="MViewSearch" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <br />
                        <div class="row">
                            <div class="col-lg">
                                <asp:TextBox ID="txtSearchObjectFullNumber" runat="server" MaxLength="25" class="form-control" placeholder="Obyekt kodu" type="text"></asp:TextBox>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-lg">
                                <img id="loading1" style="display: none" src="../img/loader.gif" />
                                <asp:Button ID="btnSearchByObjectCode" class="btn btn-info pd-x-20" runat="server" Text="Axtar"
                                    OnClientClick="this.style.display = 'none';
                                    document.getElementById('loading1').style.display = '';"
                                    OnClick="btnSearchByObjectCode_Click" />
                            </div>
                        </div>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <br />
                        <div class="row">
                            <div class="col-lg">
                                <asp:DropDownList ID="drlZone" OnSelectedIndexChanged="drlZone_SelectedIndexChanged" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-lg">
                                <asp:DropDownList ID="drlLine" OnSelectedIndexChanged="drlLine_SelectedIndexChanged" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-lg">
                                <asp:DropDownList ID="drlCorpus" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-lg">
                                <img id="loading2" style="display: none" src="../img/loader.gif" />
                                <asp:Button ID="btnSearchByZoneCode" class="btn btn-info pd-x-20" runat="server" Text="Axtar"
                                    OnClientClick="this.style.display = 'none';
                                    document.getElementById('loading2').style.display = '';"
                                    OnClick="btnSearchByZoneCode_Click" />
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>

                <div class="table-responsive" >
                    <asp:GridView ID="GrdComunalPointer"  class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" GridLines="None">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton Enabled="false" ID="lnkObjectDetailsShow" runat="server"
                                        CommandArgument='<%#Eval("ObjectFullNumber")%>'>
						                <%#Eval("ObjectFullNumber")%>
                                         <br />
                                        SAYĞAC: <b><%# Convert.ToString(Eval("CounterNumber")) %></b>
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    OBYEKT KODU
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    Ilk: <b><%# Config.ToDecimal(Convert.ToString(Eval("Counter_Value1"))) %> </b>
                                    <br />
                                    Son: <b><%# Config.ToDecimal(Convert.ToString(Eval("Counter_Value2"))) %></b>
                                    <br />
                                    Məbləğ: <b><%# Config.ToDecimal(Convert.ToString(Eval("Calculated_amount"))) %></b>
                                    <br />
                                    <b><%# Convert.ToString(Eval("Value_date_desc")) %></b>
                                        <asp:LinkButton ID="lnkDebtPrint" runat="server" OnClick="lnkDebtPrint_Click"
                                        CommandArgument='<%#Convert.ToString(Eval("CounterNumber")).Trim() %>'>
                                        <img src ="/img/print.png" title ="Çap et"/>
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    GÖSTƏRİCİLƏR
                                </HeaderTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkChangePointer"
                                        OnClick ="lnkChangePointer_Click"
                                        CssClass="btn btn-primary btn-block mg-b-10" runat="server"
                                        CommandArgument='<%#Convert.ToString(Eval("CounterNumber")) + "#" +  Convert.ToString(Eval("CounterType")) +  "#" +  Convert.ToString(Eval("Counter_Value1")) +  "#" +  Convert.ToString(Eval("Counter_Value2"))   %>'>
                                        Yenilə
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="lnkPay" OnClick ="lnkPay_Click"
                                        Visible='<%#Config.ToDecimal(Convert.ToString(Eval("Calculated_amount"))) > 0 ? true : false %>'
                                        CommandArgument ='<%#Eval("ObjectFullNumber")%>'
                                        CssClass="btn btn-danger" runat="server">
                                        Ödəniş et
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="60px" HorizontalAlign="Right" />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                    <asp:Literal ID="ltrPrintData" runat="server"></asp:Literal>
                </div>
            </div>
        </div>

        <script type="text/javascript">

            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }

            function openObjectDebtModal() {
                $('#modalObjectDebt').modal({ show: true });
            }

            function openPaymentDetailsModal() {
                $('#modalPaymentDetails').modal({ show: true });
            }
            function openCounterPointerModal() {
                $('#modalCounterPointer').modal({ show: true });
            }
            function PrintDebt() {
                var mywindow = window.open('', 'PRINT');

                mywindow.document.write('<html><head>');
                mywindow.document.write('<link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet" />');
                mywindow.document.write('<link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet" />');
                mywindow.document.write('<link href="../lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />');
                mywindow.document.write('<link rel="stylesheet" href="../css/starlight.css" />');
                mywindow.document.write('<style>.payment_print_tab tbody td  {font-family: "Roboto", "Helvetica Neue", Arial, sans-serif; font-size: 12px;padding: 0.3rem;}</style>');
                mywindow.document.write('</head><body >');
                mywindow.document.write(document.getElementById('debt_print').innerHTML);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10*/

                mywindow.print();
                mywindow.close();
                document.getElementById('debt_print').style.display = 'none';
                return true;
            }
        </script>

        

    </div>
</asp:Content>

