﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Index_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        DbProcess db = new DbProcess();
        if (Session["LogonUserID"] == null) Config.Rd("/exit");
        if (!IsPostBack)
        {
            int userid = Convert.ToInt32(Session["LogonUserID"]);
            GrdUserZones.DataSource = db.GetUserZoneI(userid);
            GrdUserZones.DataBind();
        }
    }
}