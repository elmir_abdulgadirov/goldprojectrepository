﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Index_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="">STM</a>
            <span class="breadcrumb-item active">Dashboard</span>
        </nav>
        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>STM - Obyektlərə Nəzarət Sistemi</h5>
                <p><%=DateTime.Now.ToString("dd.MM.yyyy HH:mm") %></p>
            </div>
            <div class="card pd-20 pd-sm-40">
                <h6 class="card-body-title">TƏHKİM OLDUĞUNUZ ZONA</h6>
                <p class="mg-b-20 mg-sm-b-30"></p>
                <div class="table-responsive">
                    <asp:GridView ID="GrdUserZones" class="table mg-b-0" runat="server" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
                        <Columns>
                            <asp:BoundField DataField="Description" HeaderText="ZONA ADI" />                           
                        </Columns>
                        <EmptyDataTemplate>
                            Təhkim olduğunuz zona yoxdur...
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

