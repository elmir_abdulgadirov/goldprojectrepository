﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="MyCash_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet" />
    <link href="../lib/spectrum/spectrum.css" rel="stylesheet" />

    <link rel="stylesheet" href="../css/starlight.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">Mənim kassam</span>
        </nav>

        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>Mənim kassam</h5>
            </div>
            <div class="card pd-20 pd-sm-40">


                <!-- SMALL MODAL -->
                <div id="alertmodal" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                            <div class="modal-footer justify-content-right" style="padding: 5px">
                                <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->


                <div class="input-group">
                    <span class="input-group-addon"><i class="icon ion-calendar tx-16 lh-0 op-6"></i></span>
                    <asp:TextBox ID="txtfromdate" type="text" class="form-control fc-datepicker" placeholder="dd.mm.yyyy" runat="server" MaxLength="10"></asp:TextBox>
                    <asp:LinkButton ID="lnkSearch" runat="server"
                        CssClass="btn btn-outline-info" OnClick="lnkSearch_Click">
                                   <i class="icon ion-search"></i> Axtar
                    </asp:LinkButton>
                </div>

                <br />
                <asp:Literal ID="ltrTotalDebt" runat="server"></asp:Literal>
                <br />
                <br />
                <div class="table-responsive">
                    <asp:GridView ID="GrdMyCasht" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False"  GridLines="None">
                        <Columns> 
                            <asp:BoundField  DataField="PaymentTypeDesc" HeaderText="ÖDƏNİŞİN TİPİ" >
                            <ItemStyle Font-Bold="True" />
                            </asp:BoundField>                       
                            <asp:BoundField DataField="PaymentDate" HeaderText="TARİX" />
                            <asp:BoundField DataField="PaymentAmount" HeaderText="MƏBLƏĞ" />
                        </Columns>
                    </asp:GridView>
                </div>

            </div>
        </div>

         <script type="text/javascript">

             function openAlertModal() {
                 $('#alertmodal').modal({ show: true });
             }

        </script>



        <script src="../lib/jquery/jquery.js"></script>

        <style>
            .ui-datepicker {
                z-index: 1151 !important; /* has to be larger than 1050 */
            }
        </style>

        <script type="text/javascript">
            $(function () {
                'use strict';
                // Datepicker
                $('.fc-datepicker').datepicker({
                    dateFormat: 'dd.mm.yy',
                    showOtherMonths: true,
                    selectOtherMonths: true
                });
            });
        </script>

    </div>
</asp:Content>

