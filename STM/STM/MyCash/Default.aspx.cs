﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MyCash_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");
        if (!IsPostBack)
        {
            txtfromdate.Text = Config.HostingTime.ToString("dd.MM.yyyy");
            FillCash(Convert.ToInt32(Session["LogonUserID"]), Config.ToDate(txtfromdate.Text));
        }

        string FUNCTION_ID = "MY_CASHS_MAIN_PAGE";

        /*Begin Permission*/
        db.CheckFunctionPermission(Convert.ToInt32(Session["LogonUserID"]), FUNCTION_ID);
        /*End Permission*/
      
    }
    protected void lnkSearch_Click(object sender, EventArgs e)
    {
        if (!Config.isDate(txtfromdate.Text))
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Tarix formatını düzgün daxil edin ! (məs : 22.10.2015)");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }
        FillCash(Convert.ToInt32(Session["LogonUserID"]), Config.ToDate(txtfromdate.Text));

       
    }

    void FillCash(int UserID,DateTime date)
    {
        ltrTotalDebt.Text = "";
        DataTable dt = new DataTable("FillCash");
        dt = db.GetMyCash(UserID, date);
        GrdMyCasht.DataSource = dt;
        GrdMyCasht.DataBind();
        GrdMyCasht.UseAccessibleHeader = true;
        if (GrdMyCasht.Rows.Count > 0) GrdMyCasht.HeaderRow.TableSection = TableRowSection.TableHeader;

        decimal  paid_total = 0.0m;
        for (int i = 0; i < GrdMyCasht.Rows.Count; i++)
        {
            paid_total += Config.ToDecimal(Convert.ToString(dt.Rows[i]["PaymentAmount"]));
        }

        if (dt.Rows.Count == 0)
        {
            ltrAlertMsg.Text = Alert.WarningMessage("Məlumat tapılmadı.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        ltrTotalDebt.Text = string.Format("<span class=\"badge badge-success\" style = \"margin-top:5px\"> </span><b>Ümumi məbləğ: {0}</b>", paid_total);
    }
}