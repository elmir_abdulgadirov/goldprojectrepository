﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CashSetting_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();
    void FillZone(int userID)
    {
        DataTable dtZone = db.GetZoneByAllowedUser(userID);
        drlZone.DataSource = dtZone;
        drlZone.DataBind();
        drlZone.Items.Insert(0, new ListItem("--Zona seçin", "0"));
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");
        if (!IsPostBack)
        {
            FillZone(Convert.ToInt32(Session["LogonUserID"]));
            FillCashList(Convert.ToInt32(Session["LogonUserID"]));          
        }

        string FUNCTION_ID = "CASHS_SETTING_MAIN_PAGE";

        /*Begin Permission*/
        db.CheckFunctionPermission(Convert.ToInt32(Session["LogonUserID"]), FUNCTION_ID);

        lnkAddNewCash.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "CASH_ADD_NEW");

        if (GrdCashs.Rows.Count > 0)
        {
            GrdCashs.Columns[5].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "CASH_DELETE");
        }

        /*End Permission*/
    }


    void FillCashList(int userId)
    {
        DataTable dtCashList = db.GetCashList(userId);
        GrdCashs.DataSource = dtCashList;
        GrdCashs.DataBind();
        GrdCashs.UseAccessibleHeader = true;
        if (GrdCashs.Rows.Count > 0) GrdCashs.HeaderRow.TableSection = TableRowSection.TableHeader;
    }


    protected void btnAddNewCash_Click(object sender, EventArgs e)
    {
        ltrNewCashModal.Text = "";
        if (drlZone.SelectedValue == "0")
        {
            ltrNewCashModal.Text = Alert.DangerMessage("Kassanın aid olduğu zonanı seçin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddCashModal();", true);
            return;
        }

        if (txtCashCode.Text.Trim().Length < 1)
        {
            ltrNewCashModal.Text = Alert.DangerMessage("Kassa kodunu daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddCashModal();", true);
            return;
        }
        if (txtCashName.Text.Trim().Length < 3)
        {
            ltrNewCashModal.Text = Alert.DangerMessage("Kassa adını daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddCashModal();", true);
            return;
        }

        string result = db.AddNewCash(txtCashCode.Text.Trim(), txtCashName.Text.Trim(), Convert.ToString(drlZone.SelectedValue), Convert.ToInt32(Session["LogonUserID"]));
      
        if (!result.Equals("OK"))
        {
            ltrNewCashModal.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddCashModal();", true);
            return;
        }

        if (result.Equals("OK"))
        {
            ltrAlertMsg.Text = Alert.SuccessMessage("Əlavə olundu");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }
        FillCashList(Convert.ToInt32(Session["LogonUserID"]));
    }
    protected void btnDeleteChash_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        int CashID = Convert.ToInt32(btn.CommandArgument);
        string result = db.DeleteCash(CashID, Convert.ToInt32(Session["LogonUserID"]));
        if (!result.Equals("OK"))
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }
        
        if (result.Equals("OK"))
        {
            ltrAlertMsg.Text = Alert.SuccessMessage("Silindi");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }
        FillCashList(Convert.ToInt32(Session["LogonUserID"]));

    }
}