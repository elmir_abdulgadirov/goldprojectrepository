﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="CashSetting_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">Kassalar</span>
        </nav>

        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>Kassa tənzimlənməsi</h5>
            </div>
            <div class="card pd-20 pd-sm-40">
                <div class="row">
                    <div class="col-lg">
                        <asp:linkbutton id="lnkAddNewCash" runat="server"
                            cssclass="btn btn-outline-info btn-block"
                            data-toggle="modal" data-target="#modalNewCash">
								   <i class="icon ion-plus-circled"></i> Yeni Kassa
						</asp:linkbutton>
                    </div>
                </div>
                <br />

               <div class="table-responsive">
                <asp:GridView ID="GrdCashs" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False"  GridLines="None" EnableModelValidation="True">
                    <Columns>
                        <asp:BoundField DataField="Code" HeaderText="Kodu" />
			            <asp:BoundField DataField="CashName" HeaderText="Adı" />
			            <asp:BoundField DataField="ZoneName" HeaderText="Zona" />
			            <asp:BoundField DataField="MakerName" HeaderText="Avtor" />
			            <asp:BoundField DataField="CMakerDateFormat" HeaderText="TARİX" />
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnDeleteCash"
                                    class="btn btn-outline-danger btn-block"
                                    Width="38px" Height="38px" runat="server" Text="X" Font-Bold="True"
                                    title="Sil"
                                    CommandArgument='<%# Eval("ID") %>' OnClick ="btnDeleteChash_Click"
                                    OnClientClick="return confirm('Kassanı silmək istədiyinizdən əminsiniz?');" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" Width="50px" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        MƏLUMAT TAPILMADI...
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>




            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
				<ContentTemplate>


                <!-- SMALL MODAL -->
                <div id="alertmodal" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                            <div class="modal-footer justify-content-right" style="padding: 5px">
                                <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->


                    <!-- LARGE MODAL -->
								<div id="modalNewCash" class="modal fade" data-backdrop="static">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content tx-size-sm">
											<div class="modal-header pd-x-20">
												<h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>&nbsp&nbspYENİ KASSA</h6>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body pd-20">
												<asp:Literal ID="ltrNewCashModal" runat="server"></asp:Literal>
												<div class="card pd-20 pd-sm-40">
													<div class="row">
														<div class="col-lg">
															<asp:DropDownList ID="drlZone" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server"></asp:DropDownList>
														</div>
														<div class="col-lg">
															<asp:TextBox ID="txtCashCode" runat="server" MaxLength="10" class="form-control" placeholder="Kassa kodu"></asp:TextBox>
														</div>
														<div class="col-lg">
															<asp:TextBox ID="txtCashName" runat="server" MaxLength="30" class="form-control" placeholder="Kassa adı"></asp:TextBox>
														</div>
													</div>
												</div>
											</div>
											<!-- modal-body -->
											<div class="modal-footer">
												<img id="add_loading_cash" style="display: none" src="../img/loader.gif" />
												<asp:Button ID="btnAddNewCash" class="btn btn-info pd-x-20" runat="server" Text="Əlavə et"
                                                    OnClick ="btnAddNewCash_Click"
													OnClientClick="this.style.display = 'none';
														   document.getElementById('add_loading_cash').style.display = '';
														   document.getElementById('btnAddNewCashCancel').style.display = 'none';
														   document.getElementById('alert_msg').style.display = 'none';" />
												<button id="btnAddNewCashCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
											</div>
										</div>
									</div>
									<!-- modal-dialog -->
								</div>
								<!-- modal -->
                 </ContentTemplate>
				<Triggers>
					<asp:PostBackTrigger ControlID="btnAddNewCash" />
				</Triggers>
			</asp:UpdatePanel>

            </div>
        </div>

        <script type="text/javascript">
            function openAddCashModal() {
                $('#modalNewCash').modal({ show: true });
            }
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }
        </script>
    </div>
</asp:Content>

