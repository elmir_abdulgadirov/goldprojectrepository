﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Counters_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">Komunal</span>
        </nav>

        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>Komunal qiymətlərinin tənzimlənməsi</h5>
            </div>


            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>


               <!-- SMALL MODAL -->
                <div id="alertmodal" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                            <div class="modal-footer justify-content-right" style="padding: 5px">
                                <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->



                        <!-- LARGE MODAL -->
                        <div id="modalEditCounterPrice" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>&nbspQİYMƏTİN YENİLƏNMƏSİ</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrModalEditCounterPrice" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    Komunal tipi:
                                                    <asp:TextBox ID="txtEditCounterType" runat="server" MaxLength="60" Enabled ="false" class="form-control" placeholder="Komunal tipi" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-6">
                                                    Qiyməti:
                                                    <asp:TextBox ID="txtEditCounterPrice" runat="server" MaxLength="4" class="form-control" placeholder="Qiyməti" type="text"></asp:TextBox>
                                                </div>
                                            </div>                            
                                        </div>
                                    </div>
                                    <!-- modal-body -->
                                    <div class="modal-footer">
                                        <img id="add_loading_counter" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnEditCounterPriceAccept" class="btn btn-info pd-x-20" OnClick ="btnEditCounterPriceAccept_Click" runat="server" Text="Yenilə"
                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_loading_counter').style.display = '';
                                                           document.getElementById('btnEditCounterPriceAcceptCancel').style.display = 'none';
                                                           document.getElementById('alert_msg').style.display = 'none';" />
                                        <button id="btnEditCounterPriceAcceptCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->

                     </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnEditCounterPriceAccept" />
                    </Triggers>
                </asp:UpdatePanel>

       


            <div class="card pd-20 pd-sm-40">
                <div class="table-responsive">
                    <asp:GridView ID="GrdCoutersUnite" class="table" GridLines="None" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("CounterTypeDesc") + "</kbd>"%>
                                </ItemTemplate>
                                <HeaderTemplate>KOMUNAL TİPİ</HeaderTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("Unite") + "</kbd>"%>
                                </ItemTemplate>
                                <HeaderTemplate>VAHİDİ</HeaderTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("UnitePrice") + "</kbd>"%>
                                </ItemTemplate>
                                <HeaderTemplate>QİYMƏTİ</HeaderTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("MakerName") + "</kbd>"%>
                                </ItemTemplate>
                                <HeaderTemplate>AVTOR</HeaderTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%#"<kbd style=\"background-color: #1576d6 !important\">" + Eval("CMakerDateFormat") + "</kbd>"%>
                                </ItemTemplate>
                                <HeaderTemplate>TARİX</HeaderTemplate>
                            </asp:TemplateField>

                            
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkCounterEdit" runat="server" 
                                        CommandArgument='<%#Convert.ToString(Eval("CounterType")).Trim() + "#" + Convert.ToString(Eval("CounterTypeDesc")).Trim() + "#" + Convert.ToString(Eval("UnitePrice")).Trim()  %>' OnClick ="lnkCounterEdit_Click">
                                                <img src ="/img/edit_icon.png" title ="Qiymətin yenilənməsi" />
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="50px" />
                            </asp:TemplateField>


                        </Columns>
                        <EmptyDataTemplate>MƏLUMAT TAPILMADI...</EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <br />
                <h4>Dəyişikliklərin tarixçəsi</h4>
                <hr />
                <div class="table-responsive">
                    <asp:GridView ID="GrdCoutersUniteHistory" class="table" GridLines="None" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%#Eval("CounterTypeDesc")%>
                                </ItemTemplate>
                                <HeaderTemplate>KOMUNAL TİPİ</HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%#Eval("Unite")%>
                                </ItemTemplate>
                                <HeaderTemplate>VAHİDİ</HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%#Eval("UnitePrice")%>
                                </ItemTemplate>
                                <HeaderTemplate>QİYMƏTİ</HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Eval("MakerName")%>
                                </ItemTemplate>
                                <HeaderTemplate>AVTOR</HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <%# Eval("CMakerDateFormat")%>
                                </ItemTemplate>
                                <HeaderTemplate>TARİX</HeaderTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>MƏLUMAT TAPILMADI...</EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </div>

         <script type="text/javascript">
             function openEditCounterPriceModal() {
                 $('#modalEditCounterPrice').modal({ show: true });
             }

             function openAlertModal() {
                 $('#alertmodal').modal({ show: true });
             }
        </script>

    </div>

</asp:Content>

