﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Counters_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");
        if (!IsPostBack)
        {
            FillCounterUnite();
            FillCounterUniteHistory();
        }

        string FUNCTION_ID = "COUNTERS_MAIN_PAGE";

        /*Begin Permission*/
        db.CheckFunctionPermission(Convert.ToInt32(Session["LogonUserID"]), FUNCTION_ID);

        if (GrdCoutersUnite.Rows.Count > 0)
        {
            GrdCoutersUnite.Columns[5].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "COUNTERS_CHANGE_PRICE");
        }

        /*End Permission*/
    }


    void FillCounterUnite()
    {
        DataTable dtCounterUnite = db.GetCounterUnite();
        GrdCoutersUnite.DataSource = dtCounterUnite;
        GrdCoutersUnite.DataBind();
        GrdCoutersUnite.UseAccessibleHeader = true;
        if (GrdCoutersUnite.Rows.Count > 0) GrdCoutersUnite.HeaderRow.TableSection = TableRowSection.TableHeader;
    }

    void FillCounterUniteHistory()
    {
        DataTable dtCounterUniteHistory = db.GetCounterUniteHistory();
        GrdCoutersUniteHistory.DataSource = dtCounterUniteHistory;
        GrdCoutersUniteHistory.DataBind();
        GrdCoutersUniteHistory.UseAccessibleHeader = true;
        if (GrdCoutersUniteHistory.Rows.Count > 0) GrdCoutersUniteHistory.HeaderRow.TableSection = TableRowSection.TableHeader;
    }
    protected void lnkCounterEdit_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;

        string data = lnk.CommandArgument;

        string counterType = data.Split('#')[0];
        string counterTypeDesc = data.Split('#')[1];
        decimal counterPrice = Config.ToDecimal(data.Split('#')[2]);


        txtEditCounterType.Text = counterTypeDesc;
        txtEditCounterPrice.Text = counterPrice.ToString();
        ltrModalEditCounterPrice.Text = "";

        Session["selected_counterType"] = counterType;
        Session["selected_counterPrice"] = counterPrice;


        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditCounterPriceModal();", true);

    }
    protected void btnEditCounterPriceAccept_Click(object sender, EventArgs e)
    {
        if (Session["selected_counterType"] == null || Session["selected_counterPrice"] == null)
        {
            Config.Rd("/exit");
        }
        decimal oldCounterPrice = Config.ToDecimal(Convert.ToString(Session["selected_counterPrice"]));
        decimal newCounterPrice = 0;
        string counterType = Convert.ToString(Session["selected_counterType"]);
        
        try { newCounterPrice = Config.ToDecimal(txtEditCounterPrice.Text.Trim()); }
        catch 
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditCounterPriceModal();", true);
            ltrModalEditCounterPrice.Text = Alert.DangerMessage("Qiyməti düzgün daxil edin!");
            return;
        }

        if (oldCounterPrice == newCounterPrice)
        {
            return;
        }

        string result = db.UpdateCounterUnite(counterType, newCounterPrice, Convert.ToInt32(Session["LogonUserID"]));
        if (!result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditCounterPriceModal();", true);
            ltrModalEditCounterPrice.Text = Alert.DangerMessage("Xəta : " + result);
            return;
        }

        if (result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage("Məlumat yeniləndi");
            FillCounterUnite();
            FillCounterUniteHistory();
        }

       
    }
}