﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Owners_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link href="../lib/select2/css/select2.min.css" rel="stylesheet" />
    <link href="../lib/spectrum/spectrum.css" rel="stylesheet" />



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">Sahibkarlar</span>
        </nav>

        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>Sahibkarlar</h5>
            </div>
            <div class="card pd-20 pd-sm-40">

                <!-- SMALL MODAL -->
                <div id="alertmodal" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                            <div class="modal-footer justify-content-right" style="padding: 5px">
                                <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->


                <div class="row">
                    <div class="col-lg">
                        <asp:LinkButton ID="lnkAddNewOwners" runat="server"
                            CssClass="btn btn-outline-info btn-block"
                            data-toggle="modal" data-target="#modalAddOwners">
                                   <i class="icon ion-plus-circled"></i> Yeni Sahibkar
                        </asp:LinkButton>
                    </div>

                    <div class="col-lg">
                        <asp:LinkButton ID="lnkOwnersSearch" runat="server"
                            CssClass="btn btn-outline-info btn-block"
                            data-toggle="modal" data-target="#modalSearchOwners">
                                   <i class="icon ion-search"></i> Axtarış
                        </asp:LinkButton>
                    </div>

                    <div class="col-lg">
                        <asp:LinkButton ID="lnkDownloadExcel" OnClick ="lnkDownloadExcel_Click" runat="server" CssClass="btn btn-outline-info btn-block">
                                   <img style ="width:16px;height:16px" src ="/img/excel_icon.png" />&nbsp&nbspExcel-ə yüklə
                        </asp:LinkButton>
                    </div>

                </div>
                <br />
                <asp:Literal ID="ltrDataCount" runat="server"></asp:Literal>
                <br />
                <div class="table-responsive">
                    <asp:GridView ID="GrdOwners" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" DataKeyNames="ID" GridLines="None" EnableModelValidation="True">
                        <Columns>
                            <asp:BoundField DataField="Fullname" HeaderText="S.A.A" />
                            <asp:BoundField DataField="ObjectFullNumber" HeaderText="Obyekt KODU" />
                            <asp:BoundField DataField="RegDate" HeaderText="QEYDİYYAT TARİXİ" />
                            <asp:BoundField DataField="ContractNumber" HeaderText="MÜQAVİLƏ №" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <b>Əlaqə nömrəsi :</b> <%#Eval("ContactNumber") %>
                                    <br />
                                    <b>Ünvan:</b> <%#Eval("Address") %>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    ƏLAQƏ 
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkOwnerEdit" OnClick="lnkOwnerEdit_Click" runat="server"
                                        CommandArgument='<%#Eval("ID")%>'>
                                                <img src ="/img/edit_icon.png" title ="Sahibkarın yenilənməsi"/>
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="50px" />
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Button ID="btnDeleteOwner"
                                        class="btn btn-outline-danger btn-block"
                                        Width="38px" Height="38px" runat="server" Text="X" Font-Bold="True"
                                        title="Sil"
                                        CommandArgument='<%# Eval("ID") %>'
                                        OnClientClick="return confirm('Sahibkarı silmək istədiyinizdən əminsiniz?');" OnClick="btnDeleteOwner_Click" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="50px" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            MƏLUMAT TAPILMADI...
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>

                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <!-- LARGE MODAL -->
                        <div id="modalAddOwners" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>&nbsp&nbspYENİ SAHİBKAR</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrModalAddOwner" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">
                                            <div class="row">
                                                <div class="col-lg-7">
                                                    Soyad , ad , ata adı:
                                                    <asp:TextBox ID="txtNewOwnersFullName" runat="server" MaxLength="60" class="form-control" placeholder="Soyad , ad , ata adı" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-5">
                                                    Şəxsiyyət V/N:
                                                    <asp:TextBox ID="txtNewOwnersPassportNumber" runat="server" MaxLength="20" class="form-control" placeholder="Şəxsiyyət V/N" type="text"></asp:TextBox>
                                                </div>
                                                
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-7">
                                                    Obyekt kodu:
                                                    <asp:TextBox ID="txtNewOwnersObjectFullNumber" runat="server" MaxLength="60" class="form-control" placeholder="Obyekt kodu" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-5">
                                                    Qeydiyyat tarixi:
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon ion-calendar tx-16 lh-0 op-6"></i></span>
                                                        <asp:TextBox ID="txtNewOwnersRegDate" runat="server" MaxLength="10" placeholder="dd.mm.yyyy" class="form-control fc-datepicker" type="text"></asp:TextBox>
                                                    </div>
                                                </div>                        
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div  class="col-lg-7">
                                                     Müqavilə Tipi:
                                                    <asp:DropDownList ID="drlContractType" DataTextField="ContractTypeName" DataValueField="ID" CssClass="form-control"  runat="server"  ></asp:DropDownList>
                                                </div>
                                                <div class="col-lg-5">
                                                    Müqavilə nömrəsi
                                                    <asp:TextBox ID="txtNewOwnersContractNumber" runat="server" MaxLength="20" class="form-control" placeholder="Müqavilə nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-7">
                                                    Ünvanı:
                                                   <asp:TextBox ID="txtNewOwnersAddress" runat="server" MaxLength="100" class="form-control" placeholder="Ünvanı" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-5">
                                                    Əlaqə nömrəsi:
                                                    <asp:TextBox ID="txtNewOwnersContactNumber" runat="server" MaxLength="20" class="form-control" placeholder="Əlaqə nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- modal-body -->
                                    <div class="modal-footer">
                                        <img id="add_loading" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnNewOwnerAccept" class="btn btn-info pd-x-20" OnClick="btnNewOwnerAccept_Click" runat="server" Text="Əlavə et"
                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_loading').style.display = '';
                                                           document.getElementById('btnNewOwnerAcceptCancel').style.display = 'none';
                                                           document.getElementById('alert_msg').style.display = 'none';" />
                                        <button id="btnNewOwnerAcceptCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->




                        <!-- LARGE MODAL -->
                        <div id="modalSearchOwners" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-search"></i>&nbsp&nbspAXTARIŞ</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrModalSearchOwner" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">
                                            <code class="code-base"><i>Qeyd: </i>İcazəniz olan zona(lar) üzrə SİSTEMdə mövcud olan bütün obyekt sahiblərinin siyahısını almaq üçün axtarış sahələrini təmizləyin və 'Axtar' düyməsinə sıxın</code>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg">
                                                    Soyad , ad , ata adı:
                                                    <asp:TextBox ID="txtSearchOwnersFullName" runat="server" MaxLength="60" class="form-control" placeholder="Soyad , ad , ata adı" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    Şəxsiyyət V/N:
                                                    <asp:TextBox ID="txtSearchOwnersPassportNumber" runat="server" MaxLength="20" class="form-control" placeholder="Şəxsiyyət V/N" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    Əlaqə nömrəsi:
                                                    <asp:TextBox ID="txtSearchOwnersContactNumber" runat="server" MaxLength="20" class="form-control" placeholder="Əlaqə nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg">
                                                    Obyekt kodu:
                                                    <asp:TextBox ID="txtSearchOwnersObjectFullNumber" runat="server" MaxLength="60" class="form-control" placeholder="Obyekt kodu" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg">
                                                    Qeydiyyat tarixi:
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon ion-calendar tx-16 lh-0 op-6"></i></span>
                                                        <asp:TextBox ID="txtSearchOwnersRegDate" runat="server" MaxLength="10" placeholder="dd.mm.yyyy" class="form-control fc-datepicker" type="text"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-lg">
                                                    Müqavilə nömrəsi
                                                    <asp:TextBox ID="txtSearchOwnersContractNumber" runat="server" MaxLength="20" class="form-control" placeholder="Müqavilə nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg">
                                                    Ünvanı:
                                                    <asp:TextBox ID="txtSearchOwnersAddress" runat="server" MaxLength="100" class="form-control" placeholder="Ünvanı" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- modal-body -->
                                    <div class="modal-footer">
                                        <img id="search_loading" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnSearchOwnerAccept" OnClick="btnSearchOwnerAccept_Click" class="btn btn-info pd-x-20" runat="server" Text="Axtar"
                                            OnClientClick="this.style.display = 'none';
                                               document.getElementById('search_loading').style.display = '';
                                               document.getElementById('btnSearchOwnerAcceptCancel').style.display = 'none';
                                               document.getElementById('alert_msg').style.display = 'none';" />
                                        <button id="btnSearchOwnerAcceptCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->




                        <!-- LARGE MODAL -->
                        <div id="modalEditOwners" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-edit"></i>&nbsp&nbspMƏLUMATLARI YENİLƏ</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrModalEditOwner" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">
                                            <div class="row">
                                                <div class="col-lg-7">
                                                    Soyad , ad , ata adı:
                                                    <asp:TextBox ID="txtEditOwnersFullName" runat="server" MaxLength="60" class="form-control" placeholder="Soyad , ad , ata adı" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-5">
                                                    Şəxsiyyət V/N:
                                                   <asp:TextBox ID="txtEditOwnersPassportNumber" runat="server" MaxLength="20" class="form-control" placeholder="Şəxsiyyət V/N" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-7">
                                                    Obyekt kodu:
                                                    <asp:TextBox ID="txtEditOwnersObjectFullNumber" runat="server" MaxLength="60" class="form-control" placeholder="Obyekt kodu" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-5">
                                                    Qeydiyyat tarixi:
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon ion-calendar tx-16 lh-0 op-6"></i></span>
                                                        <asp:TextBox ID="txtEditOwnersRegDate" runat="server" MaxLength="10" placeholder="dd.mm.yyyy" class="form-control fc-datepicker" type="text"></asp:TextBox>
                                                    </div>
                                                </div>
                                             </div>  
                                            <br />
                                            <div class="row">
                                                <div  class="col-lg-7">
                                                     Müqavilə Tipi:
                                                    <asp:DropDownList ID="drlEditContractTypeName" DataTextField="ContractTypeName" DataValueField="ID" CssClass="form-control"  runat="server"  ></asp:DropDownList>
                                                </div>
                                                <div class="col-lg-5">
                                                    Müqavilə nömrəsi
                                                    <asp:TextBox ID="txtEditOwnersContractNumber" runat="server" MaxLength="20" class="form-control" placeholder="Müqavilə nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-7">
                                                    Ünvanı:
                                                    <asp:TextBox ID="txtEditOwnersAddress" runat="server" MaxLength="100" class="form-control" placeholder="Ünvanı" type="text"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-5">
                                                    Əlaqə nömrəsi:
                                                    <asp:TextBox ID="txtEditOwnersContactNumber" runat="server" MaxLength="20" class="form-control" placeholder="Əlaqə nömrəsi" type="text"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- modal-body -->
                                    <div class="modal-footer">
                                        <img id="edit_loading" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnEditOwnerAccept" class="btn btn-info pd-x-20" OnClick="btnEditOwnerAccept_Click" runat="server" Text="Yenilə"
                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('edit_loading').style.display = '';
                                                           document.getElementById('btnEditOwnerAcceptCancel').style.display = 'none';
                                                           document.getElementById('alert_msg').style.display = 'none';" />
                                        <button id="btnEditOwnerAcceptCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->


                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnNewOwnerAccept" />
                        <asp:PostBackTrigger ControlID="btnEditOwnerAccept" />
                        <asp:PostBackTrigger ControlID="btnSearchOwnerAccept" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
        <script type="text/javascript">
            function openAddOwnerModal() {
                $('#modalAddOwners').modal({ show: true });
            }

            function openEditOwnerModal() {
                $('#modalEditOwners').modal({ show: true });
            }

            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }

            function openSearchModal() {
                $('#modalSearchOwners').modal({ show: true });
            }
        </script>

        <script src="../lib/jquery/jquery.js"></script>

        <style>
            .ui-datepicker {
                z-index: 1151 !important; /* has to be larger than 1050 */
            }
        </style>

        <script type="text/javascript">
            $(function () {
                'use strict';
                // Datepicker
                $('#modalAddOwners').on('shown.bs.modal', function () {
                    $('.fc-datepicker').datepicker({
                        dateFormat: 'dd.mm.yy',
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        container: '#modalAddOwners modal-body'
                    });
                });

                $('#modalEditOwners').on('shown.bs.modal', function () {
                    $('.fc-datepicker').datepicker({
                        dateFormat: 'dd.mm.yy',
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        container: '#modalEditOwners modal-body'
                    });
                });

                $('#modalSearchOwners').on('shown.bs.modal', function () {
                    $('.fc-datepicker').datepicker({
                        dateFormat: 'dd.mm.yy',
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        container: '#modalSearchOwners modal-body'
                    });
                });

            });
        </script>
    </div>
</asp:Content>

