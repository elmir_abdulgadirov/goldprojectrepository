﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Owners_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();

    void FillContractType()
    {
        DataTable dtCont = db.GetContractType();
        drlContractType.DataSource = dtCont;
        drlContractType.DataTextField = "ContractTypeName";
        drlContractType.DataValueField = "ID";
        drlContractType.DataBind();
        drlContractType.Items.Insert(0, new ListItem("--Tipi seçin", "0"));
        
    }
    void FillContractTypeForEdit()
    {
        DataTable dtContEdit = db.GetContractType();
        drlEditContractTypeName.DataSource = dtContEdit;
        drlEditContractTypeName.DataTextField = "ContractTypeName";
        drlEditContractTypeName.DataValueField = "ID";
        drlEditContractTypeName.DataBind();
        drlEditContractTypeName.Items.Insert(0, new ListItem("--Tipi seçin", "0"));
       

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");
        if (!IsPostBack)
        {
            FillContractType();
            FillContractTypeForEdit();

            if (Session["OwnersSearchParams"] != null)
            {
                Session["OwnersSearchParams"] = null;
                Session.Remove("OwnersSearchParams");
            }
            OwnersSearchParams Osp = new OwnersSearchParams();
            FillOwners(Osp, Convert.ToInt32(Session["LogonUserID"]), true, false);

           
        }
       
        
        string FUNCTION_ID = "OBJECT_OWNERS_MAIN_PAGE";

        /*Begin Permission*/
        db.CheckFunctionPermission(Convert.ToInt32(Session["LogonUserID"]), FUNCTION_ID);

        lnkAddNewOwners.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "OWNERS_ADD_NEW");
        lnkDownloadExcel.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "OWNERS_DOWNLOAD_EXCEL");
        lnkOwnersSearch.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "OWNERS_SEARCH");
        

        if (GrdOwners.Rows.Count > 0)
        {
            GrdOwners.Columns[5].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "OWNERS_CHANGE_DATA");
            GrdOwners.Columns[6].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "OWNERS_DELETE");
        }


        /*End Permission*/

    }
    protected void btnNewOwnerAccept_Click(object sender, EventArgs e)
    {
        ltrModalAddOwner.Text = "";
        if (txtNewOwnersFullName.Text.Trim().Length < 5)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddOwnerModal();", true);
            ltrModalAddOwner.Text = Alert.DangerMessage("Sahibkarın soyad,ad,ata adını daxil edin!");
            return;
        }

        if (txtNewOwnersObjectFullNumber.Text.Trim().Length < 7)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddOwnerModal();", true);
            ltrModalAddOwner.Text = Alert.DangerMessage("Obyektin kodunu daxil edin!");
            return;
        }

        if (!Config.isDate(txtNewOwnersRegDate.Text))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddOwnerModal();", true);
            ltrModalAddOwner.Text = Alert.DangerMessage("Qeydiyyat tarixini düzgün daxil edin! (Misal: 22.10.2015)");
            return;
        }

        if (drlContractType.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddOwnerModal();", true);
            ltrModalAddOwner.Text = Alert.DangerMessage("Müqavilə Tipini seçin!");
            return;
        }

       string result =  db.AddOwner(txtNewOwnersFullName.Text.Trim(), txtNewOwnersPassportNumber.Text,
            Convert.ToInt32(drlContractType.SelectedValue), txtNewOwnersContactNumber.Text,
            txtNewOwnersAddress.Text.Trim(), txtNewOwnersObjectFullNumber.Text.Trim().Replace(" ",""), txtNewOwnersRegDate.Text.Trim(), 
            txtNewOwnersContractNumber.Text.Trim(), "ACTIVE", Convert.ToInt32(Session["LogonUserID"]));
        
       if (!result.Equals("OK"))
       {
           ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddOwnerModal();", true);
           ltrModalAddOwner.Text = Alert.DangerMessage("Xəta : " + result);
           return;
       }
       Config.Rd("/owners");
    }

    void FillOwners(OwnersSearchParams Osp, int UserID, bool isTop, bool ExportExcel)
    {
        int dataCount;
        DataTable dtOwners = db.GetOwnersList(Osp,UserID,isTop,ExportExcel,out dataCount);
        if (!ExportExcel)
        {
            GrdOwners.DataSource = dtOwners;
            GrdOwners.DataBind();
            GrdOwners.UseAccessibleHeader = true;
            if (GrdOwners.Rows.Count > 0) GrdOwners.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        if (ExportExcel)
        {
            ExportExcel exExcel = new ExportExcel();
            exExcel.ExportToExcel(dtOwners, "Obyekt sahibləri", "Obyekt_Sahibleri");
        }

        ltrDataCount.Text = string.Format("<span>Məlumat sayı: <b>{0}</b></span>", dataCount);
       
      
    }
    protected void lnkOwnerEdit_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        int ownerID = Convert.ToInt32(lnk.CommandArgument);
        Session["selected_ownerId"] = ownerID;
        ltrModalEditOwner.Text = "";
        DataRow drOwner = db.GetOwnerByID(ownerID);
        
        txtEditOwnersFullName.Text = Convert.ToString(drOwner["Fullname"]);
        txtEditOwnersPassportNumber.Text = Convert.ToString(drOwner["PassportNumber"]);
        drlEditContractTypeName.SelectedValue = Convert.ToString(drOwner["ContractTypeID"]);
        txtEditOwnersContactNumber.Text = Convert.ToString(drOwner["ContactNumber"]);
        txtEditOwnersObjectFullNumber.Text = Convert.ToString(drOwner["ObjectFullNumber"]);
        txtEditOwnersRegDate.Text = Convert.ToString(drOwner["RegDate"]);
        txtEditOwnersContractNumber.Text = Convert.ToString(drOwner["ContractNumber"]);
        txtEditOwnersAddress.Text = Convert.ToString(drOwner["Address"]);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditOwnerModal();", true);       
    }
    protected void btnEditOwnerAccept_Click(object sender, EventArgs e)
    {
        if (Session["selected_ownerId"] == null) Config.Rd("/exit");
        int ownerID = Convert.ToInt32(Session["selected_ownerId"]);
       
        if (!Config.isDate(txtEditOwnersRegDate.Text.Trim()))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditOwnerModal();", true);
            ltrModalEditOwner.Text = Alert.DangerMessage("Qeydiyyat tarixini düzgün daxil edin! (Misal: 22.10.2015)");
            return;
        }
       string result =  db.UpdateOwner(ownerID, txtEditOwnersFullName.Text.Trim(),
                       txtEditOwnersPassportNumber.Text.Trim(),
                       Convert.ToInt32(drlEditContractTypeName.SelectedValue),
                       txtEditOwnersContactNumber.Text.Trim(),
                       txtEditOwnersAddress.Text.Trim(),
                       txtEditOwnersObjectFullNumber.Text.Trim().Replace(" ", ""),
                       txtEditOwnersRegDate.Text.Trim(),
                       txtEditOwnersContractNumber.Text.Trim(),
                       "ACTIVE",
                       Convert.ToInt32(Session["LogonUserID"]));
      
       if (!result.Equals("OK"))
       {
           ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openEditOwnerModal();", true);
           ltrModalEditOwner.Text = Alert.DangerMessage("Xəta : " + result);
           return;
       }

       if (result.Equals("OK"))
       {
           OwnersSearchParams Osp = new OwnersSearchParams();
           if (Session["OwnersSearchParams"] != null) Osp = (OwnersSearchParams)Session["OwnersSearchParams"];

           FillOwners(Osp, Convert.ToInt32(Session["LogonUserID"]), Session["OwnersSearchParams"] != null ? false : true,false);

           ltrAlertMsg.Text = Alert.SuccessMessage("Məlumat yeniləndi");
           ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
       }
       

    }
    protected void btnDeleteOwner_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        int ownerID = Convert.ToInt32(btn.CommandArgument);
        string result = db.DeleteOwner(ownerID, Convert.ToInt32(Session["LogonUserID"]));
        if (result.Equals("OK"))
        {
            OwnersSearchParams Osp = new OwnersSearchParams();
            if (Session["OwnersSearchParams"] != null) Osp = (OwnersSearchParams)Session["OwnersSearchParams"];

            FillOwners(Osp, Convert.ToInt32(Session["LogonUserID"]), Session["OwnersSearchParams"] != null ? false : true, false);


            ltrAlertMsg.Text = Alert.SuccessMessage("Silindi");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }
        else
        {
            OwnersSearchParams Osp = new OwnersSearchParams();
            if (Session["OwnersSearchParams"] != null) Osp = (OwnersSearchParams)Session["OwnersSearchParams"];

            FillOwners(Osp, Convert.ToInt32(Session["LogonUserID"]), Session["OwnersSearchParams"] != null ? false : true, false);

            ltrAlertMsg.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }
    }
    protected void btnSearchOwnerAccept_Click(object sender, EventArgs e)
    {
        if (txtSearchOwnersRegDate.Text.Trim() != "")
        {
            if (!Config.isDate(txtSearchOwnersRegDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openSearchModal();", true);
                ltrModalSearchOwner.Text = Alert.DangerMessage("Qeydiyyat tarixini düzgün daxil edin! (Misal: 22.10.2015)");
                return;
            }
        }

        OwnersSearchParams Osp = new OwnersSearchParams();
        Osp.fullname = txtSearchOwnersFullName.Text.Trim().ToLower();
        Osp.passport_number = txtSearchOwnersPassportNumber.Text.Trim().ToLower();
        Osp.contact_number = txtSearchOwnersContactNumber.Text.Trim().ToLower();
        Osp.object_number = txtSearchOwnersObjectFullNumber.Text.Trim().ToLower();
        Osp.reg_date = txtSearchOwnersRegDate.Text.Trim().ToLower();
        Osp.contract_number = txtSearchOwnersContractNumber.Text.Trim().ToLower();
        Osp.address = txtSearchOwnersAddress.Text.Trim().ToLower();

        Session["OwnersSearchParams"] = Osp;

        FillOwners(Osp, Convert.ToInt32(Session["LogonUserID"]), false , false);
    }
    protected void lnkDownloadExcel_Click(object sender, EventArgs e)
    {
        OwnersSearchParams Osp = new OwnersSearchParams();
        if (Session["OwnersSearchParams"] != null) Osp = (OwnersSearchParams)Session["OwnersSearchParams"];
        FillOwners(Osp, Convert.ToInt32(Session["LogonUserID"]), Session["OwnersSearchParams"] != null ? false : true, true);
    }
}