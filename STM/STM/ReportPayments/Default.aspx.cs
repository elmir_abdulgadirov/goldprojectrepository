﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ReportPayments_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();
    void FillZone(int userID)
    {
        DataTable dtZone = db.GetZoneByAllowedUser(userID);
        drlZone.DataSource = dtZone;
        drlZone.DataBind();
        drlZone.Items.Insert(0, new ListItem("--Zona seçin", ""));
        drlLine.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpus.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }


    void FillLine(string ZoneID)
    {
        DataTable dtLine = db.GetAllLines(ZoneID);
        drlLine.DataSource = dtLine;
        drlLine.DataBind();
        drlLine.Items.Insert(0, new ListItem("--Sıra seçin", ""));
        drlCorpus.Items.Clear();
        drlCorpus.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }

    void FillCorpus(string ZoneID, string LineID)
    {
        DataTable dtCorpus = db.GetAllCorpus(ZoneID, LineID);
        drlCorpus.DataSource = dtCorpus;
        drlCorpus.DataBind();
        drlCorpus.Items.Insert(0, new ListItem("--Korpus seçin", ""));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");
        if (!IsPostBack)
        {
            FillZone(Convert.ToInt32(Session["LogonUserID"]));


            string FUNCTION_ID = "REPORT_FOR_PAYMENT";

            /*Begin Permission*/
            db.CheckFunctionPermission(Convert.ToInt32(Session["LogonUserID"]), FUNCTION_ID);

            /*End Permission*/
        }
    }


    protected void drlZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillLine(Convert.ToString(drlZone.SelectedValue));
    }
    protected void drlLine_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillCorpus(Convert.ToString(drlZone.SelectedValue), Convert.ToString(drlLine.SelectedValue));
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        ReportForObjectsParams Rop = new ReportForObjectsParams();
        ltrAlertMsg.Text = "";

        if (txtFromDate.Text.Trim().Length != 0)
        {
            if (!Config.isDate(txtFromDate.Text))
            {
                ltrAlertMsg.Text = Alert.DangerMessage("Tarix formatını düzgün daxil edin ! (məs : 22.10.2015)");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                return;
            }
            Rop.pFromDate = txtFromDate.Text.Trim();
        }

        if (txtToDate.Text.Trim().Length != 0)
        {
            if (!Config.isDate(txtToDate.Text))
            {
                ltrAlertMsg.Text = Alert.DangerMessage("Tarix formatını düzgün daxil edin ! (məs : 22.10.2015)");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                return;
            }
            Rop.pToDate = txtToDate.Text.Trim();
        }
        
        if (drlZone.SelectedValue != "")
        {
            Rop.pZoneCode = Convert.ToString(drlZone.SelectedValue);
        }

        if (drlLine.SelectedValue != "")
        {
            Rop.pLineCode = Convert.ToString(drlLine.SelectedValue);
        }

        if (drlCorpus.SelectedValue != "")
        {
            Rop.pCorpusCode = Convert.ToString(drlCorpus.SelectedValue);
        }

        if (txtObjectNumber.Text.Trim().Length != 0)
        {
            Rop.pObjectNumber = txtObjectNumber.Text.Trim();
        }

        if (drlObjectPosition.SelectedValue != "0")
        {
            Rop.pObjectPosition = drlObjectPosition.SelectedValue;
        }

        if (drlObjectType.SelectedValue != "0")
        {
            Rop.pObjectType = drlObjectType.SelectedValue;
        }

        if (drlOwnerType.SelectedValue != "0")
        {
            Rop.pOwnerType = drlOwnerType.SelectedValue;
        }

        if (drlObjectStatus.SelectedValue != "0")
        {
            Rop.pObjectStatus = drlObjectStatus.SelectedValue;
        }

        if (txtObjectAreaMin.Text.Trim().Length != 0)
        {
            try
            {
                Rop.pObjectAreaMin = Config.ToDecimal(txtObjectAreaMin.Text.Trim());
            }
            catch
            {
                ltrAlertMsg.Text = Alert.DangerMessage("Obyekt sahəsini düzgün daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                return;
            }
        }

        if (txtObjectAreaMax.Text.Trim().Length != 0)
        {
            try
            {
                Rop.pObjectAreaMax = Config.ToDecimal(txtObjectAreaMax.Text.Trim());
            }
            catch
            {
                ltrAlertMsg.Text = Alert.DangerMessage("Obyekt sahəsini düzgün daxil edin!");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                return;
            }
        }

        if (txtTenantContract.Text.Trim().Length != 0)
        {
            Rop.pTenantName = txtTenantContract.Text.Trim();
        }

        if (txtTenantName.Text.Trim().Length != 0)
        {
            Rop.pTenantName = txtTenantName.Text.Trim();
        }

        Session["ForPayment_ReportForObjectsParams"] = Rop;

        FillReports(Rop, Convert.ToInt32(Session["LogonUserID"]), false);
    }


    void FillReports(ReportForObjectsParams Rop, int UserID, bool ExportExcel)
    {
        int datacount;
        DataTable dtReports = db.GetReportForPayment(Rop, UserID, ExportExcel, out datacount);
        if (!ExportExcel)
        {
            GrdReports.DataSource = dtReports;
            GrdReports.DataBind();
            GrdReports.UseAccessibleHeader = true;
            if (GrdReports.Rows.Count > 0) GrdReports.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        ltrDataCount.Text = string.Format("<span>Məlumat sayı: <b>{0}</b></span>", datacount);
        if (ExportExcel)
        {
            ExportExcel exExcel = new ExportExcel();
            exExcel.ExportToExcel(dtReports, "Odənishlər uzre hesabatliq", "Odənishlər uzre hesabatliq");
        }

        lnkDownloadExcel.Visible = GrdReports.Rows.Count > 0;
    }
    protected void lnkDownloadExcel_Click(object sender, EventArgs e)
    {
        ReportForObjectsParams Rop = new ReportForObjectsParams();
        if (Session["ForPayment_ReportForObjectsParams"] != null) Rop = (ReportForObjectsParams)Session["ForPayment_ReportForObjectsParams"];
        FillReports(Rop, Convert.ToInt32(Session["LogonUserID"]), true);
    }
   
}