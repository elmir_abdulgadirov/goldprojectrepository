﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="ReportPayments_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../lib/select2/css/select2.min.css" rel="stylesheet" />
    <link href="../lib/spectrum/spectrum.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">Hesabat - Ödənişlər üzrə</span>
        </nav>

        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>Axtarış filterləri</h5>
            </div>
            <div class="card pd-20 pd-sm-40">





                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

                        <!-- SMALL MODAL -->
                        <div id="alertmodal" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-sm" role="document">
                                <div class="modal-content bd-0 tx-14">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                                    <div class="modal-footer justify-content-right" style="padding: 5px">
                                        <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->


                        <div class="row">
                            <div class="col-lg">
                                <asp:DropDownList ID="drlZone" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" OnSelectedIndexChanged="drlZone_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-lg">
                                <asp:DropDownList ID="drlLine" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server" OnSelectedIndexChanged="drlLine_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="col-lg">
                                <asp:DropDownList ID="drlCorpus" DataTextField="Description" DataValueField="Code" CssClass="form-control" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <br />


                        <div id="accordion3" class="accordion" role="tablist" aria-multiselectable="true">
                            <div class="card">
                                <div class="card-header" role="tab" id="heading3">
                                    <h6 class="mg-b-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"
                                            aria-expanded="true" aria-controls="collapse3" class="tx-gray-800 transition">Tarix intervalı
                                        </a>
                                    </h6>
                                </div>

                                <div id="collapse3" class="collapse show" role="tabpanel" aria-labelledby="heading3">
                                    <div class="card-block pd-20">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                Tarixdən:
                                                <asp:TextBox ID="txtFromDate" runat="server" MaxLength="10" placeholder="dd.mm.yyyy" class="form-control fc-datepicker" type="text"></asp:TextBox>
                                            </div>
                                            <div class="col-lg-6">
                                                Tarixə:
                                                <asp:TextBox ID="txtToDate" runat="server" MaxLength="10" placeholder="dd.mm.yyyy" class="form-control fc-datepicker" type="text"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />



                        <div id="accordion" class="accordion" role="tablist" aria-multiselectable="true">
                            <div class="card">
                                <div class="card-header" role="tab" id="headingOne">
                                    <h6 class="mg-b-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                            aria-expanded="true" aria-controls="collapseOne" class="tx-gray-800 transition">Obyekt məlumatları
                                        </a>
                                    </h6>
                                </div>

                                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="card-block pd-20">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                Obyekt nömrəsi:
                                                <asp:TextBox ID="txtObjectNumber" runat="server" MaxLength="20" class="form-control" type="text"></asp:TextBox>
                                            </div>
                                            <div class="col-lg-6">
                                                Yerləşmə tipi:
                                                <asp:DropDownList ID="drlObjectPosition" class="form-control" runat="server">
                                                    <asp:ListItem Value="0" Selected="True">Yerləşmə tipi</asp:ListItem>
                                                    <asp:ListItem Value="KUNC">Künc</asp:ListItem>
                                                    <asp:ListItem Value="KECHID">Keçid</asp:ListItem>
                                                    <asp:ListItem Value="NORMAL">Normal</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-lg-4">
                                                Obyektin tipi:
                                                 <asp:DropDownList ID="drlObjectType" class="form-control" runat="server">
                                                     <asp:ListItem Value="0" Selected="True">Obyektin tipi</asp:ListItem>
                                                     <asp:ListItem Value="MAGAZA">Mağaza</asp:ListItem>
                                                     <asp:ListItem Value="SKLAT">Sklat</asp:ListItem>
                                                     <asp:ListItem Value="TUALET">Tualet</asp:ListItem>
                                                     <asp:ListItem Value="METBEX">Mətbəx</asp:ListItem>
                                                     <asp:ListItem Value="MARKET">Market</asp:ListItem>
                                                 </asp:DropDownList>
                                            </div>
                                            <div class="col-lg-4">
                                                Mülkiyyət:
                                                 <asp:DropDownList ID="drlOwnerType" class="form-control" runat="server">
                                                     <asp:ListItem Value="0" Selected="True">Mülkiyyət</asp:ListItem>
                                                     <asp:ListItem Value="BAZAR">Bazar</asp:ListItem>
                                                     <asp:ListItem Value="SHEXSI">Şəxsi</asp:ListItem>
                                                 </asp:DropDownList>
                                            </div>
                                            <div class="col-lg-4">
                                                Obyektin statusu:
                                                      <asp:DropDownList ID="drlObjectStatus" class="form-control" runat="server">
                                                          <asp:ListItem Value="0" Selected="True">Obyektin statusu</asp:ListItem>
                                                          <asp:ListItem Value="ACTIV">Aktiv</asp:ListItem>
                                                          <asp:ListItem Value="TEMIR">Təmir</asp:ListItem>
                                                          <asp:ListItem Value="BOSH">Boş</asp:ListItem>
                                                          <asp:ListItem Value="BAGLI">Bağlı</asp:ListItem>
                                                      </asp:DropDownList>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-lg-6">
                                                Obyektin sahəsi min. (m²) 
                                                <asp:TextBox ID="txtObjectAreaMin" runat="server" MaxLength="15" class="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-lg-6">
                                                Obyektin sahəsi max. (m²) 
                                                <asp:TextBox ID="txtObjectAreaMax" runat="server" MaxLength="15" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br />

                        <div id="accordion2" class="accordion" role="tablist" aria-multiselectable="true">
                            <div class="card">
                                <div class="card-header" role="tab" id="heading2">
                                    <h6 class="mg-b-0">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"
                                            aria-expanded="true" aria-controls="collapse2" class="tx-gray-800 transition">İcarəçi məlumatları
                                        </a>
                                    </h6>
                                </div>

                                <div id="collapse2" class="collapse  show" role="tabpanel" aria-labelledby="heading2">
                                    <div class="card-block pd-20">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                Müqavilə №:
                                                <asp:TextBox ID="txtTenantContract" runat="server" MaxLength="50" class="form-control" type="text"></asp:TextBox>
                                            </div>
                                            <div class="col-lg-6">
                                                S.A.A:
                                                <asp:TextBox ID="txtTenantName" runat="server" MaxLength="100" class="form-control" type="text"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <br />

                        <div class="row">
                            <div class="col-lg" style="text-align: right">
                                <img id="loading_image" style="display: none" src="../img/loader.gif" />
                                <asp:Button ID="btnSearch" CssClass="btn btn-primary" runat="server" Text="Axtar" OnClick="btnSearch_Click"
                                    OnClientClick="this.style.display = 'none';
													document.getElementById('loading_image').style.display = '';" />
                            </div>
                        </div>

                        <asp:Literal ID="ltrDataCount" runat="server"></asp:Literal>
                        <div style="text-align: right">
                            <asp:LinkButton ID="lnkDownloadExcel" OnClick ="lnkDownloadExcel_Click" Visible="false"  runat="server" CssClass="btn btn-outline-info">
								   <img style ="width:16px;height:16px" src ="/img/excel_icon.png" />&nbsp&nbspExcel-ə yüklə
                            </asp:LinkButton>
                        </div>
                        <br />
                        <div class="table-responsive">
                            <asp:GridView ID="GrdReports" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" GridLines="None" EnableModelValidation="True">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            Zona : <%#Eval("Zone")%>
                                            <br />
                                            Sıra : <%#Eval("Line")%>
                                            <br />
                                            Korpus : <%#Eval("Corpus")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                             <%#Eval("ObjectFullNumber")%>
                                        </ItemTemplate>
                                         <HeaderTemplate>
                                            OBYEKT KODU
                                        </HeaderTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <%#Eval("TenantName")%>
                                            <br />
                                           <%#Eval("TenantContract")%>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            İCARƏÇİ / SAHİBKAR
                                        </HeaderTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                             <%#Eval("PaymentType")%>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            ÖDƏNİŞ TİPİ
                                        </HeaderTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                          <%#Eval("PaymentAmount")%>
                                        </ItemTemplate>
                                         <HeaderTemplate>
                                            MƏBLƏĞ
                                        </HeaderTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField>
                                        <ItemTemplate>
                                           Ödəniş tarixi : <%#Eval("PaymentDate")%>
                                           <br />
                                           Qəbul edən :  <%#Eval("PaymentMakerName")%>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            İCARƏÇİ / SAHİBKAR
                                        </HeaderTemplate>
                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>
                        </div>

                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="lnkDownloadExcel" />
                        <asp:PostBackTrigger ControlID="btnSearch" />
                    </Triggers>
                </asp:UpdatePanel>


            </div>
        </div>


        <script src="../lib/jquery/jquery.js"></script>

        <style>
            .ui-datepicker {
                z-index: 1151 !important; /* has to be larger than 1050 */
            }
        </style>

        <script type="text/javascript">
            $(function () {
                'use strict';
                // Datepicker
                $('.fc-datepicker').datepicker({
                    dateFormat: 'dd.mm.yy',
                    showOtherMonths: true,
                    selectOtherMonths: true
                });
            });
        </script>



        <script>
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }
        </script>

    </div>
</asp:Content>


