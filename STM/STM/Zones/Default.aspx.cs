﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Zones_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();

    void FillZones(int UserID)
    {
        grdZones.DataSource = db.GetZoneByAllowedUser(UserID);
        grdZones.DataBind();
        grdZones.UseAccessibleHeader = true;
        if (grdZones.Rows.Count > 0) grdZones.HeaderRow.TableSection = TableRowSection.TableHeader;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");

        if (!IsPostBack)
        {
            FillZones(Convert.ToInt32(Session["LogonUserID"]));
        }

        string FUNCTION_ID = "ZONE_LINE_CORPUS_MAIN_PAGE";

        /*Begin Permission*/
        db.CheckFunctionPermission(Convert.ToInt32(Session["LogonUserID"]), FUNCTION_ID);

        btnAddNewZone.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "ADD_NEW_ZONE");
        if (grdZones.Rows.Count > 0)
        {
            grdZones.Columns[3].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "VIEW_LINES_IN_ZONE");
            grdZones.Columns[4].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "DELETE_ZONE");
        }

        btnAddNewLine.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "ADD_NEW_LINE");

        try
        {
            GrdLines.Columns[4].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "VIEW_CORPUS_IN_LINE");
            GrdLines.Columns[5].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "DELETE_LINE");
        }
        catch { }

        btnAddNewCorpus.Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "ADD_NEW_CORPUS");
        try
        {
            GrdCorpus.Columns[5].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "DELETE_CORPUS");
        }
        catch { }
        /*End Permission*/
        
    }

    protected void btnZoneAdd_Click(object sender, EventArgs e)
    {
        ltrMessage.Text = "";
        if (txtZoneCode.Text.Trim().Length == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddZoneModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Zona kodunu  daxil edin!");
            return;
        }
       

        if (txtZoneName.Text.Trim().Length == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddZoneModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Zona adını daxil edin");
            return;
        }

        string reslt = db.AddZone(txtZoneCode.Text.Trim(), txtZoneName.Text.Trim(), Convert.ToInt32(Session["LogonUserID"]));
        if (reslt != "OK")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddZoneModal();", true);
            ltrMessage.Text = Alert.DangerMessage("Xəta : " + reslt);
            return;
        }
        FillZones(Convert.ToInt32(Session["LogonUserID"]));
    }
    protected void btnDeleteZone_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        string pCode = Convert.ToString(btn.CommandArgument);
        string result = db.DeletedZone(pCode, Convert.ToInt32(Session["LogonUserID"]));
        if (result == "OK")
        {
            ltrAlertMsg.Text = Alert.SuccessMessage(pCode.ToString() + " nömrəli zona silindi");
        }
        else
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Xəta : " + result);
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        FillZones(Convert.ToInt32(Session["LogonUserID"]));
    }

    void FillLineByZoneCode(string ZoneCode)
    {
        GrdLines.DataSource = db.GetAllLines(ZoneCode);
        GrdLines.DataBind();
        GrdLines.UseAccessibleHeader = true;
        if (GrdLines.Rows.Count > 0) GrdLines.HeaderRow.TableSection = TableRowSection.TableHeader;
    }
   
   
    protected void lnkViewZoneLines_Click(object sender, EventArgs e)
    {
        LinkButton lnkView = (LinkButton)sender;
        string SelectedZoneCode = Convert.ToString(lnkView.CommandArgument);
        Session["SelectedZoneCode"] = SelectedZoneCode;

        FillLineByZoneCode(SelectedZoneCode);
        MultiView1.ActiveViewIndex = 1;
        txtZoneCodeReadonly.Text = SelectedZoneCode.ToString();
     
    }
    protected void lnkLineToZoneBack_Click(object sender, EventArgs e)
    {
        Session["SelectedZoneCode"] = null;
        MultiView1.ActiveViewIndex = 0;
    }

    protected void btnLineAdd_Click(object sender, EventArgs e)
    {
        ltrMessage.Text = "";
        if (txtLineCode.Text.Trim().Length == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddLineModal();", true);
            ltrLineMsg.Text = Alert.DangerMessage("Sıra kodunu  daxil edin!");
            return;
        }
        

        if (txtLineDescription.Text.Trim().Length == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddLineModal();", true);
            ltrLineMsg.Text = Alert.DangerMessage("Sıra adını daxil edin");
            return;
        }

        string reslt = db.AddLine(txtLineCode.Text.Trim(), txtZoneCodeReadonly.Text.Trim(), txtLineDescription.Text, Convert.ToInt32(Session["LogonUserID"]));
       
        if (reslt != "OK")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage("Xəta : " + reslt);
            txtLineCode.Text = txtLineDescription.Text = "";
            return;
        }

        FillLineByZoneCode(Convert.ToString(Session["SelectedZoneCode"]));
        txtLineCode.Text = txtLineDescription.Text = "";
    }
   
    protected void btnDeleteLine_Click(object sender, EventArgs e)
    {
        if (Session["SelectedZoneCode"] == null) MultiView1.ActiveViewIndex = 0;
        string ZoneCode = Convert.ToString(Session["SelectedZoneCode"]);
        Button btn = (Button)sender;
        string pCode = Convert.ToString(btn.CommandArgument);
        string result = db.DeletedLine(pCode, ZoneCode, Convert.ToInt32(Session["LogonUserID"]));
        if (result == "OK")
        {
            ltrAlertMsg.Text = Alert.SuccessMessage(ZoneCode + " nömrəli zona daxilindəki " + pCode.ToString() + " nömrəli Sıra silindi");
        }
        else
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Xəta : " + result);
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        FillLineByZoneCode(ZoneCode);
    }

    void FillCorpusByZoneLineCode(string ZoneCode,string LineCode)
    {
        GrdCorpus.DataSource = db.GetAllCorpus(ZoneCode, LineCode);
        GrdCorpus.DataBind();
        GrdCorpus.UseAccessibleHeader = true;
        if (GrdCorpus.Rows.Count > 0) GrdCorpus.HeaderRow.TableSection = TableRowSection.TableHeader;
    }
    
    protected void lnkViewLineCorpuses_Click(object sender, EventArgs e)
    {
        if (Session["SelectedZoneCode"] == null) MultiView1.ActiveViewIndex = 0;
        string ZoneCode = Convert.ToString(Session["SelectedZoneCode"]);

        LinkButton lnkView = (LinkButton)sender;
        string SelectedLineCode = Convert.ToString(lnkView.CommandArgument);
        Session["SelectedLineCode"] = SelectedLineCode;

        FillCorpusByZoneLineCode(ZoneCode, SelectedLineCode);
        MultiView1.ActiveViewIndex = 2;
        txtCOZoneCode.Text = ZoneCode.ToString();
        txtCOLineCode.Text = SelectedLineCode.ToString();
    }
    protected void lnkCorpusToLineBack_Click(object sender, EventArgs e)
    {
        Session["SelectedLineCode"] = null;
        MultiView1.ActiveViewIndex = 1;
    }
    
    protected void btnCorpusAdd_Click(object sender, EventArgs e)
    {
        if (Session["SelectedZoneCode"] == null) MultiView1.ActiveViewIndex = 0;

        ltrMsgCorpus.Text = "";
        if (txtCorpusCode.Text.Trim().Length == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddCorpusModal();", true);
            ltrMsgCorpus.Text = Alert.DangerMessage("Korpus kodunu  daxil edin!");
            return;
        }
       

        if (txtCorpusDescription.Text.Trim().Length == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAddCorpusModal();", true);
            ltrMsgCorpus.Text = Alert.DangerMessage("Korpus adını daxil edin");
            return;
        }

        string reslt = db.AddCorpus(txtCorpusCode.Text.Trim(),
                                    txtCOZoneCode.Text.Trim(),
                                    txtCOLineCode.Text.Trim(),
                                    txtCorpusDescription.Text,
                                    Convert.ToInt32(Session["LogonUserID"]));
          

        if (reslt != "OK")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage("Xəta : " + reslt);
            txtCorpusCode.Text = txtCorpusDescription.Text = "";
            return;
        }

        FillCorpusByZoneLineCode(txtCOZoneCode.Text.Trim(),txtCOLineCode.Text.Trim());
        txtCorpusCode.Text = txtCorpusDescription.Text = "";
    }
   
    protected void btnDeleteCorpus_Click(object sender, EventArgs e)
    {
        if (Session["SelectedZoneCode"] == null) MultiView1.ActiveViewIndex = 0;
        if (Session["SelectedLineCode"] == null) MultiView1.ActiveViewIndex = 1;

        string ZoneCode = Convert.ToString(Session["SelectedZoneCode"]);
        string LineCode = Convert.ToString(Session["SelectedLineCode"]);

        Button btn = (Button)sender;
        string pCode = Convert.ToString(btn.CommandArgument);


        string result = db.DeletedCorpus(pCode, ZoneCode, LineCode, Convert.ToInt32(Session["LogonUserID"]));
            
        if (result == "OK")
        {
            ltrAlertMsg.Text = Alert.SuccessMessage(ZoneCode + " nömrəli zona daxilindəki,  "
                + LineCode.ToString() + " nömrəli sıra daxilindəki " + pCode.ToString() + " nömrəli Korpus silindi");
        }
        else
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Xəta : " + result);
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        FillCorpusByZoneLineCode(ZoneCode, LineCode);
    }
}