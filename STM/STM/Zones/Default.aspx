﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Zones_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="">STM</a>
            <span class="breadcrumb-item active">Zone&Sıra&Korpus</span>
        </nav>
        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>STM - Zona & Sıra & Korpus</h5>
            </div>
            <div class="card pd-20 pd-sm-40">


                <!-- SMALL MODAL -->
                <div id="alertmodal" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>

                            <div class="modal-footer justify-content-right" style="padding: 5px">
                                <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->


                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <h6 class="card-body-title">ZONALAR</h6>
                        <p class="mg-b-20 mg-sm-b-10"></p>
                        <asp:LinkButton ID="btnAddNewZone" runat="server"
                            CssClass="btn btn-outline-info btn-block"
                            Style="width: 20% !important"
                            data-toggle="modal" data-target="#modalAddZone">
                           <i class="icon ion-plus-circled"></i> Yeni Zona
                        </asp:LinkButton>
                        <br />
                        <div class="table-responsive">
                            <asp:GridView ID="grdZones" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" DataKeyNames="Code" GridLines="None" EnableModelValidation="True">
                                <Columns>
                                    <asp:BoundField DataField="Code" HeaderText="ZONA KODU" />
                                    <asp:BoundField DataField="Description" HeaderText="ZONA ADI" />
                                    <asp:BoundField DataField="FullName" HeaderText="AVTOR" />

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkViewZoneLines" CommandArgument='<%# Eval("Code") %>'
                                                title="Sıralara baxış" runat="server"
                                                class="btn btn-outline-dark btn-icon mg-r-5" OnClick="lnkViewZoneLines_Click">
                                                <div><i class="fa fa-exchange"></i></div></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDeleteZone"
                                                class="btn btn-outline-danger btn-block"
                                                Width="38px" Height="38px" runat="server" Text="X" Font-Bold="True"
                                                title="Sil"
                                                CommandArgument='<%# Eval("Code") %>'
                                                OnClientClick="return confirm('Zonanı silmək istədiyinizdən əminsiniz?');" OnClick="btnDeleteZone_Click" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                    </asp:TemplateField>

                                </Columns>
                                <EmptyDataTemplate>
                                    ZONA TAPILMADI...
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>



                        <!-- LARGE MODAL -->
                        <div id="modalAddZone" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>&nbsp&nbspYENİ ZONA</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrMessage" runat="server"></asp:Literal>
                                        <div class="col-lg-12">
                                            <asp:TextBox ID="txtZoneCode" MaxLength="20" runat="server" class="form-control" placeholder="Zona kodu" type="text"></asp:TextBox>
                                        </div>
                                        <p class="mg-b-20 mg-sm-b-10"></p>
                                        <div class="col-lg-12">
                                            <asp:TextBox ID="txtZoneName" runat="server" MaxLength="20" class="form-control" placeholder="Zona adı" type="text"></asp:TextBox>
                                        </div>
                                    </div>
                                    <!-- modal-body -->
                                    <div class="modal-footer">
                                        <img id="add_loading" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnZoneAdd" class="btn btn-info pd-x-20" runat="server" Text="Əlavə et"
                                            OnClick="btnZoneAdd_Click"
                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_loading').style.display = '';
                                                           document.getElementById('btnZoneAddCancel').style.display = 'none';
                                                           document.getElementById('alert_msg').style.display = 'none';" />
                                        <button id="btnZoneAddCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->

                    </asp:View>

                    <asp:View ID="View2" runat="server">

                        <asp:LinkButton ID="lnkLineToZoneBack" runat="server" OnClick="lnkLineToZoneBack_Click"><i class="icon ion-arrow-left-a"> GERİ</i></asp:LinkButton>
                        <p class="mg-b-20 mg-sm-b-20"></p>
                        <h6 class="card-body-title">SIRALAR</h6>
                        <p class="mg-b-20 mg-sm-b-10"></p>

                        <asp:LinkButton ID="btnAddNewLine" runat="server"
                            CssClass="btn btn-outline-info btn-block"
                            Style="width: 20% !important"
                            data-toggle="modal" data-target="#modalAddLine">
                           <i class="icon ion-plus-circled"></i> Yeni Sıra
                        </asp:LinkButton>
                        <br />
                        <div class="table-responsive">
                            <asp:GridView ID="GrdLines" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" DataKeyNames="Code" GridLines="None" EnableModelValidation="True">
                                <Columns>
                                    <asp:BoundField DataField="ZoneCode" HeaderText="ZONA KODU" />
                                    <asp:BoundField DataField="Code" HeaderText="SIRA KODU" />
                                    <asp:BoundField DataField="Description" HeaderText="SIRA ADI" />
                                    <asp:BoundField DataField="FullName" HeaderText="AVTOR" />

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkViewLineCorpuses" CommandArgument='<%# Eval("Code") %>'
                                                title="Korpuslara baxış" runat="server"
                                                class="btn btn-outline-dark btn-icon mg-r-5" OnClick="lnkViewLineCorpuses_Click">
                                                <div><i class="fa fa-exchange"></i></div></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDeleteLine"
                                                class="btn btn-outline-danger btn-block"
                                                Width="38px" Height="38px" runat="server" Text="X" Font-Bold="True"
                                                title="Sil"
                                                CommandArgument='<%# Eval("Code") %>'
                                                OnClientClick="return confirm('Sıranı silmək istədiyinizdən əminsiniz?');" OnClick="btnDeleteLine_Click" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    SIRA TAPILMADI...
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>



                        <!-- LARGE MODAL -->
                        <div id="modalAddLine" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>&nbsp&nbspYENİ SIRA</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrLineMsg" runat="server"></asp:Literal>
                                        <div class="col-lg-12">
                                            <asp:TextBox ID="txtZoneCodeReadonly"
                                                Enabled="false" MaxLength="20" runat="server" class="form-control" placeholder="Sıra kodu" type="text"></asp:TextBox>
                                        </div>
                                        <p class="mg-b-20 mg-sm-b-10"></p>
                                        <div class="col-lg-12">
                                            <asp:TextBox ID="txtLineCode" MaxLength="20" runat="server" class="form-control" placeholder="Sıra kodu" type="text"></asp:TextBox>
                                        </div>
                                        <p class="mg-b-20 mg-sm-b-10"></p>
                                        <div class="col-lg-12">
                                            <asp:TextBox ID="txtLineDescription" runat="server" MaxLength="20" class="form-control" placeholder="Sıra adı" type="text"></asp:TextBox>
                                        </div>
                                    </div>
                                    <!-- modal-body -->
                                    <div class="modal-footer">
                                        <img id="add_loadingLine" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnLineAdd" class="btn btn-info pd-x-20" runat="server" Text="Əlavə et"
                                            OnClick="btnLineAdd_Click"
                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_loadingLine').style.display = '';
                                                           document.getElementById('btnLineAddCancel').style.display = 'none';
                                                           document.getElementById('alert_msg').style.display = 'none';" />
                                        <button id="btnLineAddCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->

                    </asp:View>



                    <asp:View ID="View3" runat="server">

                         <asp:LinkButton ID="lnkCorpusToLineBack" runat="server" OnClick="lnkCorpusToLineBack_Click" ><i class="icon ion-arrow-left-a"> GERİ</i></asp:LinkButton>
                        <p class="mg-b-20 mg-sm-b-20"></p>
                        <h6 class="card-body-title">KORPUSLAR</h6>
                        <p class="mg-b-20 mg-sm-b-10"></p>

                        <asp:LinkButton ID="btnAddNewCorpus" runat="server"
                            CssClass="btn btn-outline-info btn-block"
                            Style="width: 20% !important"
                            data-toggle="modal" data-target="#modalAddCorpus">
                           <i class="icon ion-plus-circled"></i> Yeni Korpus
                        </asp:LinkButton>
                        <br />
                        <div class="table-responsive">
                            <asp:GridView ID="GrdCorpus" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" DataKeyNames="Code" GridLines="None" EnableModelValidation="True">
                                <Columns>
                                    <asp:BoundField DataField="ZoneCode" HeaderText="ZONA KODU" />
                                    <asp:BoundField DataField="LineCode" HeaderText="SIRA KODU" />
                                    <asp:BoundField DataField="Code" HeaderText="KORPUS KODU" />
                                    <asp:BoundField DataField="Description" HeaderText="KORPUS ADI" />
                                    <asp:BoundField DataField="FullName" HeaderText="AVTOR" />

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDeleteCorpus"
                                                class="btn btn-outline-danger btn-block"
                                                Width="38px" Height="38px" runat="server" Text="X" Font-Bold="True"
                                                title="Sil"
                                                CommandArgument='<%# Eval("Code") %>'
                                                OnClientClick="return confirm('Korpusu silmək istədiyinizdən əminsiniz?');" OnClick="btnDeleteCorpus_Click"  />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" Width="50px" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    KORPUS TAPILMADI...
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>



                        <!-- LARGE MODAL -->
                        <div id="modalAddCorpus" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"><i class="icon ion-plus-circled"></i>&nbsp&nbspYENİ KORPUS</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrMsgCorpus" runat="server"></asp:Literal>
                                        <div class="col-lg-12">
                                            <asp:TextBox ID="txtCOZoneCode"
                                                Enabled="false" MaxLength="20" runat="server" class="form-control" placeholder="Zona kodu" type="text"></asp:TextBox>
                                        </div>
                                        <p class="mg-b-20 mg-sm-b-10"></p>

                                         <div class="col-lg-12">
                                            <asp:TextBox ID="txtCOLineCode"
                                                Enabled="false" MaxLength="20" runat="server" class="form-control" placeholder="Sıra kodu" type="text"></asp:TextBox>
                                        </div>
                                        <p class="mg-b-20 mg-sm-b-10"></p>

                                        <div class="col-lg-12">
                                            <asp:TextBox ID="txtCorpusCode" MaxLength="20" runat="server" class="form-control" placeholder="Korpus kodu" type="text"></asp:TextBox>
                                        </div>
                                        <p class="mg-b-20 mg-sm-b-10"></p>

                                        <div class="col-lg-12">
                                            <asp:TextBox ID="txtCorpusDescription" runat="server" MaxLength="20" class="form-control" placeholder="Korpus adı" type="text"></asp:TextBox>
                                        </div>
                                    </div>
                                    <!-- modal-body -->
                                    <div class="modal-footer">
                                        <img id="add_loadingCorpus" style="display: none" src="../img/loader.gif" />
                                        <asp:Button ID="btnCorpusAdd" class="btn btn-info pd-x-20" runat="server" Text="Əlavə et"
                                           OnClick ="btnCorpusAdd_Click"
                                            OnClientClick="this.style.display = 'none';
                                                           document.getElementById('add_loadingCorpus').style.display = '';
                                                           document.getElementById('btnCorpusAddCancel').style.display = 'none';
                                                           document.getElementById('alert_msg').style.display = 'none';" />
                                        <button id="btnCorpusAddCancel" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">İmtina et</button>
                                    </div>
                                </div>
                            </div>
                            <!-- modal-dialog -->
                        </div>
                        <!-- modal -->

                    </asp:View>
                </asp:MultiView>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function openAddZoneModal() {
            $('#modalAddZone').modal({ show: true });
        }

        function openAlertModal() {
            $('#alertmodal').modal({ show: true });
        }

        function openAddLineModal() {
            $('#modalAddLine').modal({ show: true });
        }

        function openAddCorpusModal() {
            $('#modalAddCorpus').modal({ show: true });
        }
    </script>
</asp:Content>

