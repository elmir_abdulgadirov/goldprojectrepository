﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class MasterPage : System.Web.UI.MasterPage
{
    DbProcess db = new DbProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int userID;
            if (Session["LogonUserID"] == null) Config.Rd("/exit");
            userID = Convert.ToInt32(Session["LogonUserID"]);

            ltrHeaderPicName.Text = 
                       String.Format("<a href=\"\" class=\"nav-link nav-link-profile\" data-toggle=\"dropdown\"/>" +
                        "<span class=\"logged-name\">{0}<span class=\"hidden-md-down\"> {1}</span></span>" +
                        "<img src=\"{2}\" class=\"wd-32 rounded-circle\" alt=\"\"/></a>",
                        Config.getKeyFromSplit(Convert.ToString(Session["LoginUserName"]),' ',1),
                        Config.getKeyFromSplit(Convert.ToString(Session["LoginUserName"]),' ',0),
                        Convert.ToString(Session["LoginUserImage"]));


            //Left Menu Begin
            DataTable dtMenu = db.GetMenu(userID);

            if (dtMenu != null)
            {
                IEnumerable<DataRow> queryHeadMenu =
                from headMenu in dtMenu.AsEnumerable()
                where headMenu.Field<int>("Parent_ID") == 0
                orderby headMenu.Field<int>("Position")
                select headMenu;

                DataTable dtHeadMenu = queryHeadMenu.CopyToDataTable();
                LtrMenu.Text = "";
                if (dtHeadMenu != null)
                {
                    foreach (DataRow drHeadMenu in dtHeadMenu.Rows)
                    {
                        DataTable dtSubMenu = new DataTable("dtSubMenu");
                        IEnumerable<DataRow> querySubMenu =
                           from subMenu in dtMenu.AsEnumerable()
                           where subMenu.Field<int>("Parent_ID") == Convert.ToInt32(drHeadMenu["ID"])
                           orderby subMenu.Field<int>("Position")
                           select subMenu;
                        if (querySubMenu.Any())
                        {
                            dtSubMenu = querySubMenu.CopyToDataTable<DataRow>();
                        }

                        string isSumMenuIcon = "";
                        if (dtSubMenu.Rows.Count > 0)
                        {
                            isSumMenuIcon = "<i class=\"menu-item-arrow fa fa-angle-down\"></i>";
                        }

                        LtrMenu.Text +=   "<a href=\"" + Convert.ToString(drHeadMenu["Url"]).Trim() + "\" class=\"sl-menu-link" + (Convert.ToString(drHeadMenu["Position"]).Trim() == "1" ? " active" : "") + "\">" +
                                "<div class=\"sl-menu-item\">" +
                                    "<i class=\"" + Convert.ToString(drHeadMenu["Icon"]).Trim() + "\"></i>" +
                                    "<span class=\"menu-item-label\">" + Convert.ToString(drHeadMenu["Name"]).Trim() + "</span>" + isSumMenuIcon +
                                "</div>" +
                            "</a>";

                        if (dtSubMenu.Rows.Count > 0)
                        {
                            LtrMenu.Text += "<ul class=\"sl-menu-sub nav flex-column\">";
                            foreach (DataRow drSubMenu in dtSubMenu.Rows)
                            {
                                LtrMenu.Text += "<li class=\"nav-item\"><a href=\"" + Convert.ToString(drSubMenu["Url"]).Trim() + "\" class=\"nav-link\">" + Convert.ToString(drSubMenu["Name"]).Trim() + "</a></li>";
                            }
                            LtrMenu.Text += "</ul>";
                        }
                    }
                }
            }

            // Left Menu End

        }



        /*Begin Right badge*/

        bool is_GENERAL_CASHS_IN_TRANSFER = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "GENERAL_CASHS_IN_TRANSFER");

        if (is_GENERAL_CASHS_IN_TRANSFER)
        {
            string badgeicon = "";
            DataTable dt = db.GetTransferStatusForAcceptor();
            if (dt.Rows.Count > 0)
            {
                badgeicon = "<span class=\"square-8 bg-danger\"></span>";
                ltrTransferFromCash.Text =" " + dt.Rows.Count.ToString() + " ";


                ltrTransferList.Text = "";
            }
            else
            {
                ltrTransferFromCash.Text = " 0 ";
            }

            ltrRigthBadge.Text = "<div class=\"navicon-right\">" +
                                    "<a id=\"btnRightMenu\" href=\"\" class=\"pos-relative\">" +
                                        "<i class=\"icon ion-ios-bell-outline\"></i>" +
                                       badgeicon +
                                    "</a>" +
                                 "</div>";

            ltrTransferList.Text = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ltrTransferList.Text += 
                 string.Format("<a href=\"/headcash/?type=transfer_from_cash\" class=\"media-list-link\">" +
                            "<div class=\"media\">" +
                                "<img src=\"..{0}\" class=\"wd-40 rounded-circle\" alt=\"\"/>" +
                                "<div class=\"media-body\">" +
                                    "<p class=\"mg-b-0 tx-medium tx-gray-800 tx-13\">{1}</p>" +
                                    "<span class=\"d-block tx-11 tx-gray-500\">{2} AZN</span>" +
                                    "<p class=\"tx-13 mg-t-10 mg-b-0\">{3}</p>" +
                                "</div>" +
                            "</div>" +
                        "</a>", dt.Rows[i]["SenderMakerImage"], dt.Rows[i]["SenderMakerName"], dt.Rows[i]["Amount"], dt.Rows[i]["SenderNote"]);
            }
        }

        /*End Right badge*/
    }
}
