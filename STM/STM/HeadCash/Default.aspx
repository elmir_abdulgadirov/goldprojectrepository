﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="HeadCash_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">Ümumi kassa</span>
        </nav>

        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>ÜMUMİ KASSA (Baş ofis)</h5>
            </div>
            <div class="card pd-20 pd-sm-40">


                <!-- SMALL MODAL -->
                <div id="alertmodal" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                            <div class="modal-footer justify-content-right" style="padding: 5px">
                                <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->


                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

                        <!-- LARGE MODAL -->
                        <div id="modalAcceptTransfer" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">&nbsp&nbspTRANSFER QƏBULU</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrTransferAcceptMessage" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">

                                            <div class="row mg-t-20">
                                                Transfer məbləği:
                                                    <asp:TextBox ID="txtTransferAmount" Enabled="false" runat="server" MaxLength="10" class="form-control" type="text" placeholder="Transfer məbləği"></asp:TextBox>
                                            </div>
                                            <div class="row mg-t-20">
                                                Qeyd:  
                                                    <asp:TextBox runat="server" ID="txtNote" MaxLength="100" Rows="3" class="form-control" placeholder="Qeyd" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <img id="transfer_loading1" style="display: none" src="../img/loader.gif" />
                                            <asp:Button ID="btnAcceptTransfer" OnClick="btnAcceptTransfer_Click" class="btn btn-success pd-x-20" runat="server" Text="Qəbul et"
                                                OnClientClick="this.style.display = 'none';
														document.getElementById('transfer_loading1').style.display = '';
														document.getElementById('alert_msg').style.display = 'none';" />

                                            <asp:Button ID="btnCancelTransfer" OnClick="btnCancelTransfer_Click" class="btn btn-danger pd-x-20" runat="server" Text="Ləğv et"
                                                OnClientClick="this.style.display = 'none';
                                                           document.getElementById('transfer_loading1').style.display = '';
														   document.getElementById('alert_msg').style.display = 'none';" />
                                            <button id="btnClose" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Bağla</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- modal-dialog -->
                            </div>
                            <!-- modal -->
                        </div>



                        <!-- LARGE MODAL -->
                        <div id="modalOutTransfer" class="modal fade" data-backdrop="static">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content tx-size-sm">
                                    <div class="modal-header pd-x-20">
                                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">&nbsp&nbspÜMUMİ KASSADAN TRANSFER</h6>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body pd-20">
                                        <asp:Literal ID="ltrOutTransferMessage" runat="server"></asp:Literal>
                                        <div class="card pd-20 pd-sm-40">

                                            <div class="row mg-t-20">
                                                Kassadakl məbləğ:
                                                <asp:TextBox ID="txtTotalCashAmount" Enabled ="false" runat="server" MaxLength="10" class="form-control" type="text" placeholder="Kassadakl məbləğ"></asp:TextBox>
                                            </div>

                                            <div class="row mg-t-20">
                                                Transfer məbləği:
                                                <asp:TextBox ID="txtOutTranfer" runat="server" MaxLength="10" class="form-control" type="text" placeholder="Transfer məbləği"></asp:TextBox>
                                            </div>
                                            <div class="row mg-t-20">
                                                Qeyd:  
                                            <asp:TextBox runat="server" ID="txtOutNote" MaxLength="100" Rows="3" class="form-control" placeholder="Qeyd" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <img id="transfer_loading2" style="display: none" src="../img/loader.gif" />
                                            <asp:Button ID="btnOutTransfer" OnClick ="btnOutTransfer_Click"  class="btn btn-success pd-x-20" runat="server" Text="Transfer et"
                                                OnClientClick="this.style.display = 'none';
                                                               document.getElementById('transfer_loading2').style.display = '';
                                                               document.getElementById('btnClose1').style.display = 'none';
                                                               document.getElementById('alert_msg').style.display = 'none';" />
                                            <button id="btnClose1" type="button" class="btn btn-secondary pd-x-20" data-dismiss="modal">Bağla</button>
                                        </div>
                                    </div>
                                </div>
                                <!-- modal-dialog -->
                            </div>
                            <!-- modal -->
                        </div>







                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnAcceptTransfer" />
                        <asp:PostBackTrigger ControlID="btnCancelTransfer" />
                        <asp:PostBackTrigger ControlID="btnOutTransfer" />
                    </Triggers>
                </asp:UpdatePanel>


                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <asp:LinkButton ID="lnkHeadCash" OnClick="lnkHeadCash_Click" runat="server"><b>Ümumi Kassa</b></asp:LinkButton>
                    </li>
                    <li class="nav-item">
                        <asp:LinkButton ID="lnkTransferFromCash" OnClick="lnkTransferFromCash_Click" runat="server"><b>Kassalardan transfer</b></asp:LinkButton>
                    </li>
                </ul>
                <asp:MultiView ID="MView" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <br />
                        <br />
                        <div class="table-responsive">
                            <asp:GridView ID="GrdHeadCashs" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" GridLines="None">
                                <Columns>
                                    <asp:BoundField DataField="CashName" HeaderText="KASSA" />
                                    <asp:BoundField DataField="Amount" HeaderText="Məbləğ" />

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkOutTransfer" 
                                                CssClass="btn btn-primary" runat="server" OnClick ="lnkOutTransfer_Click"
                                                CommandArgument='<%#Convert.ToString(Eval("ID")) + "#" +  Convert.ToString(Eval("Amount")) %>'>
                                                Transfer
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" HorizontalAlign="Right" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>



                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <br />
                        <asp:Literal ID="ltrTotalPending" runat="server"></asp:Literal>
                        <br />
                        <br />
                        <div class="table-responsive">
                            <asp:GridView ID="GrdTransferStatus" class="table table-hover table-bordered mg-b-0" runat="server" AutoGenerateColumns="False" GridLines="None">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            Kassa kodu: <%#Eval("CashCode") %>
                                            <br />
                                            Kassa adı: <%#Eval("CashName") %>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            KASSA
                                        </HeaderTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="Amount" HeaderText="MƏBLƏĞ" />

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            S.A.A : <%#Eval("SenderMakerName") %>
                                            <br />
                                            TARİX : <%#Eval("SenderDateFormat") %>
                                            <br />
                                            QEYD : <%#Eval("SenderNote") %>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            GÖNDƏRƏN
                                        </HeaderTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            S.A.A : <%#Eval("AcceptorMakerName") %>
                                            <br />
                                            TARİX : <%#Eval("AcceptorDateFormat") %>
                                            <br />
                                            QEYD : <%#Eval("AcceptorNote") %>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            QƏBUL EDƏN
                                        </HeaderTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <img src="../img/<%#Eval("TransactionStatus")%>.png" />
                                            <%#Eval("TransactionStatusDesc") %>
                                        </ItemTemplate>
                                        <HeaderTemplate>
                                            STATUS
                                        </HeaderTemplate>
                                        <ItemStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkViewAccept" OnClick="lnkViewAccept_Click" title="Təsdiq və ya ləğv et" CommandArgument='<%#Eval("ID")%>' runat="server">
                                       <img src ="../img/payment_details.png" />
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="50px" HorizontalAlign="Right" />
                                    </asp:TemplateField>

                                </Columns>
                                <EmptyDataTemplate>TRANSFER YOXDUR...</EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </asp:View>
                </asp:MultiView>


            </div>
        </div>

        <script type="text/javascript">
            function openAlertModal() {
                $('#alertmodal').modal({ show: true }); 
            }

            function openAcceptTransfer() {
                $('#modalAcceptTransfer').modal({ show: true });
            }

            function openOutTransfer() {
                $('#modalOutTransfer').modal({ show: true });
            }
        </script>

    </div>
</asp:Content>

