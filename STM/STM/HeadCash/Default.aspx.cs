﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HeadCash_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");

        if (!IsPostBack)
        {
            lnkHeadCash.CssClass = "nav-link active";
            lnkTransferFromCash.CssClass = "nav-link";
            FillHeadOfficeCash();
        }

        string FUNCTION_ID = "GENERAL_CASHS_MAIN_PAGE";

        /*Begin Permission*/
          db.CheckFunctionPermission(Convert.ToInt32(Session["LogonUserID"]), FUNCTION_ID);
        /*End Permission*/

          if (Request.QueryString["type"] != null)
          {
              string type = Request.QueryString["type"].ToString();
              if (type == "transfer_from_cash")
              {
                  lnkTransferFromCash_Click(null, null);
              }
          }
    }
    protected void lnkHeadCash_Click(object sender, EventArgs e)
    {
        Config.Rd("/headcash");
        MView.ActiveViewIndex = 0;
        lnkHeadCash.CssClass = "nav-link active";
        lnkTransferFromCash.CssClass = "nav-link";
        FillHeadOfficeCash();
    }
    protected void lnkTransferFromCash_Click(object sender, EventArgs e)
    {
        MView.ActiveViewIndex = 1;
        lnkHeadCash.CssClass = "nav-link";
        lnkTransferFromCash.CssClass = "nav-link active";
        FillTransferStatusForAcceptor();
    }


    void FillHeadOfficeCash()
    {
        DataTable dtt = new DataTable("FillHeadOfficeCash");
        dtt = db.GetCashHeadOffice();
        GrdHeadCashs.DataSource = dtt;
        GrdHeadCashs.DataBind();
        GrdHeadCashs.UseAccessibleHeader = true;
        if (GrdHeadCashs.Rows.Count > 0)
        {
              GrdHeadCashs.HeaderRow.TableSection = TableRowSection.TableHeader;
            /*Begin permission*/
              GrdHeadCashs.Columns[2].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "GENERAL_CASHS_OUT_TRANSFER");
            /*End permission*/
        }
      
        if (dtt.Rows.Count == 0)
        {
            ltrAlertMsg.Text = Alert.WarningMessage("Məlumat tapılmadı.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }
    }


    void FillTransferStatusForAcceptor()
    {
        DataTable dtt = new DataTable("FillTransferStatusForAcceptor");
        dtt = db.GetTransferStatusForAcceptor();
        GrdTransferStatus.DataSource = dtt;
        GrdTransferStatus.DataBind();
        GrdTransferStatus.UseAccessibleHeader = true;
        if (GrdTransferStatus.Rows.Count > 0)
        {
            GrdTransferStatus.HeaderRow.TableSection = TableRowSection.TableHeader;
            /*Begin permission*/
             GrdTransferStatus.Columns[5].Visible = db.CheckFieldView(Convert.ToInt32(Session["LogonUserID"]), "GENERAL_CASHS_IN_TRANSFER");
            /*End permission*/
        }

    }
    protected void lnkViewAccept_Click(object sender, EventArgs e)
    {
        ltrTransferAcceptMessage.Text = "";
        LinkButton lnk = (LinkButton)sender;
        int ID = Convert.ToInt32(lnk.CommandArgument);
        Session["T_CASH_TRAN_ID"] = ID;
        decimal transAmount = db.GetTransferAmountByCashTranID(ID);
        if (transAmount == -1)
        {
            ltrAlertMsg.Text = Alert.WarningMessage("Xəta baş verdi. Xəta kodu 19881.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }
        txtTransferAmount.Text = transAmount.ToString();

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAcceptTransfer();", true);
    }
    protected void btnAcceptTransfer_Click(object sender, EventArgs e)
    {
        if (Session["T_CASH_TRAN_ID"] == null) Config.Rd("/exit");
        int T_CASH_TRAN_ID = Convert.ToInt32(Session["T_CASH_TRAN_ID"]);

        string result = db.AcceptCashTransfer(T_CASH_TRAN_ID,txtNote.Text.Trim(),"ACCEPT", Convert.ToInt32(Session["LogonUserID"]));

        if (!result.Equals("OK"))
        {
            ltrTransferAcceptMessage.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAcceptTransfer();", true);
            return;
        }
        FillTransferStatusForAcceptor();
      

        if (result.Equals("OK"))
        {
            Config.Rd("/headcash/?type=transfer_from_cash");
        }      
    }
    protected void btnCancelTransfer_Click(object sender, EventArgs e)
    {
        if (Session["T_CASH_TRAN_ID"] == null) Config.Rd("/exit");
        int T_CASH_TRAN_ID = Convert.ToInt32(Session["T_CASH_TRAN_ID"]);

        if (txtNote.Text.Trim().Length < 3)
        {
            ltrTransferAcceptMessage.Text = Alert.WarningMessage("Transferi ləğv etməyinizə dair qeyd daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAcceptTransfer();", true);
            return;
        }


        string result = db.AcceptCashTransfer(T_CASH_TRAN_ID, txtNote.Text.Trim(), "CANCEL", Convert.ToInt32(Session["LogonUserID"]));

        if (!result.Equals("OK"))
        {
            ltrTransferAcceptMessage.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAcceptTransfer();", true);
            return;
        }
        FillTransferStatusForAcceptor();


        if (result.Equals("OK"))
        {
            Config.Rd("/headcash/?type=transfer_from_cash");
        }      
    }
    protected void lnkOutTransfer_Click(object sender, EventArgs e)
    {
        ltrOutTransferMessage.Text = "";
        txtOutTranfer.Text = txtOutNote.Text = "";
        LinkButton lnk = (LinkButton)sender;
        string [] data = Convert.ToString(lnk.CommandArgument).Split('#');
        if (data.Length != 2)
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Xəta baş verdi. Xəta kodu 19882.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }

        int CashHeadID = Convert.ToInt32(data[0]);
        decimal cashAmount = 0;
        try { cashAmount = Config.ToDecimal(data[1]); }
        catch 
        {
            ltrAlertMsg.Text = Alert.DangerMessage("Xəta baş verdi. Xəta kodu 19883.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            return;
        }
        Session["TO_CashHeadID"] = CashHeadID;
        txtTotalCashAmount.Text = cashAmount.ToString();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openOutTransfer();", true);
        
    }
    protected void btnOutTransfer_Click(object sender, EventArgs e)
    {
        if (Session["TO_CashHeadID"] == null) Config.Rd("/exit");
        int TO_CashHeadID = Convert.ToInt32(Session["TO_CashHeadID"]);

        decimal transfer_out_amount = 0, total_cash_amount = 0;
        try { transfer_out_amount = Config.ToDecimal(txtOutTranfer.Text.Trim()); }
        catch
        {
            ltrOutTransferMessage.Text = Alert.DangerMessage("Transfer məbləğini düzgün daxil edin!");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openOutTransfer();", true);
            return;
        }

        try { total_cash_amount = Config.ToDecimal(Convert.ToString(txtTotalCashAmount.Text)); }
        catch
        {
            ltrOutTransferMessage.Text = Alert.DangerMessage("Xəta baş verdi. Xəta kodu 1988");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openOutTransfer();", true);
            return;
        }

        if (transfer_out_amount > total_cash_amount)
        {
            ltrOutTransferMessage.Text = Alert.WarningMessage("Transfer olunacaq məbləğ kassadakı məbləğdən böyük olmaz.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openOutTransfer();", true);
            return;
        }


        string result = db.SendFromCashTransfer(TO_CashHeadID, transfer_out_amount,  Convert.ToInt32(Session["LogonUserID"]), txtOutNote.Text.Trim());

        if (!result.Equals("OK"))
        {
            ltrOutTransferMessage.Text = Alert.DangerMessage("Xəta : " + result);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openOutTransfer();", true);
            return;
        }
       


        if (result.Equals("OK"))
        {
            ltrAlertMsg.Text = Alert.SuccessMessage("Transfer edildi.");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
        }
        FillHeadOfficeCash();

    }
}