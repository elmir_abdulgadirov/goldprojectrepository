﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="UserProfile_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


       <style>
        .custom-file-control:before {
            content: "Şəkil yüklə";
        }

        .custom-file-control:after {
            content: "İstifadəçinin şəkili..";
        }

       

        .userimage {
            width: 100px;
            border-radius: 100%;
            display: flex;
            margin-right: 15px;
        }
    </style>

    <div class="sl-mainpanel">
        <nav class="breadcrumb sl-breadcrumb">
            <a class="breadcrumb-item" href="#">STM</a>
            <span class="breadcrumb-item active">İstifadəçi profili</span>
        </nav>

        <div class="sl-pagebody">
            <div class="sl-page-title">
                <h5>İstifadəçi profili</h5>
            </div>
            <div class="card pd-20 pd-sm-40">


                <!-- SMALL MODAL -->
                <div id="alertmodal" class="modal fade" data-backdrop="static">
                    <div class="modal-dialog modal-sm" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header pd-x-20">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Message</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <asp:Literal ID="ltrAlertMsg" runat="server"></asp:Literal>
                            <div class="modal-footer justify-content-right" style="padding: 5px">
                                <button type="button" class="btn btn-info pd-x-20" data-dismiss="modal">OK</button>
                            </div>
                        </div>
                    </div>
                    <!-- modal-dialog -->
                </div>
                <!-- modal -->



                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <div class="card">
                            <h5 class="card-header">Şifrənin dəyişdirilməsi</h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtUserPassword" runat="server" MaxLength="50" class="form-control" placeholder="Şifrə" TextMode="Password"></asp:TextBox>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-lg-6">
                                        <asp:TextBox ID="txtUserPasswordRepeat" runat="server" MaxLength="50" class="form-control" placeholder="Şifrə təkrar" TextMode="Password"></asp:TextBox>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-lg">
                                        <asp:Button ID="btnChangePassword" class="btn btn-secondary" runat="server" Text="Yenilə" OnClick="btnChangePassword_Click" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <div class="card">
                            <h5 class="card-header">Mənim profilim</h5>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-lg-6">
                                        <asp:Literal ID="ltrUserImage" runat="server"></asp:Literal>
                                    </div>
                                </div>
                                <p class="mg-b-20 mg-sm-b-10"></p>
                                <div class="col-lg-6">
                                    Soyad, ad, ata adı:
                                    <asp:TextBox ID="txtFullName" MaxLength="60" runat="server" class="form-control" Enabled="false" placeholder="Soyad, ad, ata adı" type="text"></asp:TextBox>
                                </div>
                                <p class="mg-b-20 mg-sm-b-10"></p>
                                <div class="col-lg-6">
                                    Əlaqə nömrəsi:
                                    <asp:TextBox ID="txtUserContactNumber" MaxLength="50" runat="server" class="form-control" Enabled="false" placeholder="Əlaqə nömrəsi" type="text"></asp:TextBox>
                                </div>
                                <p class="mg-b-20 mg-sm-b-10"></p>
                                <div class="col-lg-6">
                                    Vəzifəsi:
                                    <asp:TextBox ID="txtUserPosition" runat="server" MaxLength="50" class="form-control" Enabled="false" placeholder="Vəzifəsi" type="text"></asp:TextBox>
                                </div>
                                <p class="mg-b-20 mg-sm-b-10"></p>
                                <div class="col-lg-6">
                                    İstifadəçi adı:
                                    <asp:TextBox ID="txtUserLogin" runat="server" MaxLength="20" class="form-control" Enabled="false"  placeholder="İstifadəçi adı" type="text"></asp:TextBox>
                                </div>
                                <p class="mg-b-20 mg-sm-b-10"></p>
                              
                                <div class="col-lg-6" id="divPicture1" runat="server">
                                    <label class="custom-file" style="width: 100%">
                                        <asp:FileUpload ID="FUlpUserPic" runat="server" class="custom-file-input" />
                                        <span class="custom-file-control"></span>
                                    </label>
                                </div>
                                <p class="mg-b-20 mg-sm-b-20"></p>
                                <div class="col-lg-6">
                                    <img id="viewuser_loading" style="display: none" src="../img/loader.gif" />
                                    <asp:Button ID="btnUserEditImage" class="btn btn-info pd-x-20" runat="server" Text="Yenilə"
                                        OnClientClick="this.style.display = 'none';
                                                       document.getElementById('viewuser_loading').style.display = '';" OnClick="btnUserEditImage_Click" />
                                </div>

                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>
        <style>
            .card {
                border: 1px solid #dadada;
            }
        </style>

        <script>
            function openAlertModal() {
                $('#alertmodal').modal({ show: true });
            }
        </script>

    </div>
</asp:Content>

