﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserProfile_Default : System.Web.UI.Page
{
    DbProcess db = new DbProcess();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LogonUserID"] == null) Config.Rd("/exit");
        if (Request.QueryString["type"] != null)
        {
            string type = Request.QueryString["type"].ToString();
            if (type == "changepassword")
            {
                MultiView1.ActiveViewIndex = 0;
            }
            if (type == "myprofile")
            {
                MultiView1.ActiveViewIndex = 1;
                FillUsersDetails(Convert.ToInt32(Session["LogonUserID"]));
            }
        }
    }



    void FillUsersDetails(int userId)
    {
        DataRow dr = db.GetUserByUserId(userId);
        if (dr != null)
        {
            txtFullName.Text = Convert.ToString(dr["Fullname"]);
            txtUserContactNumber.Text = Convert.ToString(dr["ContactNumber"]);
            txtUserPosition.Text = Convert.ToString(dr["Position"]);
            txtUserLogin.Text = Convert.ToString(dr["Login"]);
            ltrUserImage.Text = "<img class =\"userimage\" src=\"" + Convert.ToString(dr["Picture"]).Trim() + "\" alt=\"\" />";
        }
    }




    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        if (txtUserPassword.Text.Trim().Length < 6 || !Config.isEnglish(txtUserPassword.Text.Trim()))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage("Şifrəni  6 simvoldan az olmayaraq , ingilis böyük,kiçik hərflərindən , rəqəm simvollarından istifadə edərək daxil edin!");
            return;
        }

        if (txtUserPassword.Text != txtUserPasswordRepeat.Text)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage("Daxil etdiyiniz şifrələr eyni deyil.");
            return;
        }

        string result = db.ChangePassword(Convert.ToInt32(Session["LogonUserID"]), txtUserPassword.Text.Trim());
        if (!result.Equals("OK"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage("Xəta baş verdi. " + result);
            return;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.SuccessMessage("Şifrəniz dəyişdirildi. ");
        }
    }
    protected void btnUserEditImage_Click(object sender, EventArgs e)
    {
        string imageName = "";
        if (!FUlpUserPic.HasFile)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
            ltrAlertMsg.Text = Alert.DangerMessage("Şəkil seçin!");
            return;
        }
        if (FUlpUserPic.HasFile)
        {
            string AllowTypesImage = "-jpg-jpeg-png-";
            string ImageType = System.IO.Path.GetExtension(FUlpUserPic.PostedFile.FileName).Trim('.').ToLower();
            if (AllowTypesImage.IndexOf("-" + ImageType + "-") < 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                ltrAlertMsg.Text = Alert.DangerMessage(".jpg,.jpeg,.png  tiplərindən birini seçin!");
                return;
            }

            if (FUlpUserPic.PostedFile.ContentLength / 1024 > 1024)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                ltrAlertMsg.Text = Alert.DangerMessage("Maksimum 1 mb şəkil yükləməyə icazə verilir.");
                return;
            }

            Guid g_id = Guid.NewGuid();
            string ID = g_id.ToString();
            imageName = ID.ToString().Trim() + System.IO.Path.GetFileName(FUlpUserPic.FileName.Trim());

            Bitmap img = new Bitmap(FUlpUserPic.PostedFile.InputStream, false);

            try
            {
                ImageResize.ImgResize("/UserImages/" + imageName, 256, 256, FUlpUserPic.PostedFile.InputStream, 90L);
            }
            catch (Exception ex)
            {
                throw new ExecutionEngineException();
            }

            imageName = "/UserImages/" + imageName;



            string result = db.ChangeUserPicture(Convert.ToInt32(Session["LogonUserID"]), imageName);

            if (result != "OK")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                ltrAlertMsg.Text = Alert.DangerMessage(result);
                return;
            }

            if (result == "OK")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openAlertModal();", true);
                ltrAlertMsg.Text = Alert.SuccessMessage("Şəkil yeniləndi");
            }
            FillUsersDetails(Convert.ToInt32(Session["LogonUserID"]));
            Config.Rd("/userprofile/?type=myprofile");


        }
    }
}