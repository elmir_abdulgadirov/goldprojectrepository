﻿using AppMKRCreditXML.Repository;
using AppMKRCreditXML.Repository.Impl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace AppMKRCreditXML
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            _mainService = new MainService();
        }


        IMainService _mainService;

        private void btnInvoke_Click(object sender, EventArgs e)
        {
            UnFillGrid();
            if (CDate1.Checked && !CDate2.Checked)
            {
                MessageBox.Show("Hər iki tarix sahəsini aktiv edin", "Səhv", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!CDate1.Checked && CDate2.Checked)
            {
                MessageBox.Show("Hər iki tarix sahəsini aktiv edin", "Səhv", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (CDate1.Value.Date > CDate2.Value.Date)
            {
                
                MessageBox.Show("İlk tarix son tarixdən böyük ola bilməz", "Səhv", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string date1 = "", date2 = "";
            if (CDate1.Checked && CDate2.Checked)
            {  
                date1 = CDate1.Value.Date.ToString();
                date2 = CDate2.Value.Date.ToString();
            }

            btnVisibleFalse();
            System.Threading.Thread.Sleep(2000); /*for test*/


            string result,path;
            DataTable dt;
            _mainService.DeployFullXML(date1,date2,out result,out path,out dt);

            btnVisibleTrue();

            if (result != "OK")
            {
                MessageBox.Show(result, "Səhv", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            MessageBox.Show($"Yaranmış fayl : {path}", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            fillGrid(dt);

        }

        void fillGrid(DataTable dt)
        {
            //  dataGridView1.DataSource = dt;
            var query = from p in dt.AsEnumerable()
                        select new
                        {
                            Tarix = p.Field<string>("DateOfGrant"),
                            Soyad_Ad = p.Field<string>("name"),
                            Kredit_No = p.Field<string>("AccountNo"),
                        };
            dataGridView1.DataSource = query.ToList();
            lblCount.Text =$"Kredit sayı : { dataGridView1.Rows.Count.ToString()}";
            if (dataGridView1.Rows.Count > 0)
            {
                dataGridView1.Columns[0].Width = 100;
                dataGridView1.Columns[1].Width = 450;
                dataGridView1.Columns[2].Width = 200;
            }
        }

        void UnFillGrid()
        {
            dataGridView1.DataSource = null;
            lblCount.Text = $"";
        }

        void btnVisibleTrue()
        {
            Invoke(new Action(() =>
            {
                btnInvoke.Enabled = true;
            }));
        }

        void btnVisibleFalse()
        {
            Invoke(new Action(() =>
            {
                btnInvoke.Enabled = false;
            }));
        }


    }
}
