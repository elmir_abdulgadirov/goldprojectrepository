﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppMKRCreditXML.Repository
{
    interface IMainService
    {
        string DeployCredit(string date1, string date2, out DataTable dt);
        void DeployFullXML(string date1, string date2, out string result, out string path,out DataTable dt);
        void WriteFile(string fullXML, out string result);
    }
}
