﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppMKRCreditXML.Repository
{
    class DALC
    {
        public string ReadMainXML()
        {
            try
            {
                return System.IO.File.ReadAllText($@"{Application.StartupPath}\Resource\Templates\MainFile.xml");
            }
            catch
            {
                return "ERROR";
            }
        }

        public string ReadCreditXML()
        {
            try
            {
                return System.IO.File.ReadAllText($@"{Application.StartupPath}\Resource\Templates\Credits.xml");
            }
            catch
            {
                return "ERROR";
            }
        }

        public  OleDbConnection conStr
        {
            get
            {
                try
                {
                    return new OleDbConnection(ConfigurationManager.AppSettings["AccessConnection"]);
                }
                catch
                {
                    return null;
                }
            }
        }



        public DataTable GetCredits(string date1, string date2)
        {
            string sql = @"
             SELECT 
                  CreditView.ID, 
                  CreditView.name, 
                  CreditView.CountryCode, 
                  CreditView.BankruptcyStatus, 
                  CreditView.DateOfBirth, 
                  CreditView.PlaceOfBirth, 
                  CreditView.PinCode, 
                  CreditView.AccountNo, 
                  CreditView.CurrencyOfCredit, 
                  CreditView.CreditType, 
                  CreditView.InitialAmountOfCredit, 
                  CreditView.CreditLineAmount, 
                  CreditView.DisoutAmountOfCredit, 
                  CreditView.AnnualInterestRate, 
                  CreditView.PurposeOfCredit, 
                  CreditView.CreditPeriodInMonths, 
                  CreditView.DateOfGrant, 
                  CreditView.DueTimeFirstContract, 
                  CreditView.DueTimeLastContract, 
                  CreditView.LastPaymentDate, 
                  CreditView.MonthlyPaymentAmount, 
                  CreditView.DaysMainSumIsOverdue, 
                  CreditView.DaysInterestIsOverdue, 
                  CreditView.OiaForRepperiod, 
                  CreditView.NumberOfProlongs, 
                  CreditView.CreditClassCode, 
                  CreditView.CreditStatusCode, 
                  CreditView.LOAN_ID 
                    FROM 
                      CreditView    WHERE 1 = 1";


            if (date1 != "" && date2 != "")
            {
                sql += " and  (((CreditView.START_DATE) between @date1 and @date2))";
            }


            OleDbDataAdapter da = new OleDbDataAdapter(sql, conStr);
            if (date1 != "" && date2 != "")
            {
                da.SelectCommand.Parameters.AddWithValue("@date1", date1);
                da.SelectCommand.Parameters.AddWithValue("@date2", date2);
            }

            DataTable dt = new DataTable("Borrower");
            try
            {
                da.Fill(dt);
            }
            catch
            {
                throw;
            }
            return dt;
        }


        public DataTable GetGuarantees(int loanID)
        {
            string sql = @"
             SELECT 
	            CreditGuaranteeView.ID, 
	            CreditGuaranteeView.name, 
	            CreditGuaranteeView.CountryCode, 
	            CreditGuaranteeView.DateOfBirth, 
	            CreditGuaranteeView.PlaceOfBirth, 
	            CreditGuaranteeView.PinCode
            FROM CreditGuaranteeView
            WHERE CreditGuaranteeView.LL_ID= @id";

            OleDbDataAdapter da = new OleDbDataAdapter(sql, conStr);
            da.SelectCommand.Parameters.AddWithValue("@id", loanID);

            DataTable dt = new DataTable("Guarantee");
            try
            {
                da.Fill(dt);
            }
            catch
            {
                throw;
            }
            return dt;
        }


        public DataTable GetCollateral(int loanID)
        {
            string sql = @"
             SELECT 
	                CreditCollateralView.CollateralTypeCode, 
	                CreditCollateralView.AnyInfoToDisting, 
	                CreditCollateralView.MarketValue, 
	                CreditCollateralView.RegistryNo, 
	                CreditCollateralView.RegistryDate, 
	                CreditCollateralView.RegistryAgency
                FROM CreditCollateralView
                WHERE CreditCollateralView.LoanID=@id";

            OleDbDataAdapter da = new OleDbDataAdapter(sql, conStr);
            da.SelectCommand.Parameters.AddWithValue("@id", loanID);

            DataTable dt = new DataTable("Collateral");
            try
            {
                da.Fill(dt);
            }
            catch
            {
                throw;
            }
            return dt;
        }


        public DataTable GetBankData()
        {
            string sql = @"
             SELECT Company.BankID, Company.BankName
           FROM Company";

            OleDbDataAdapter da = new OleDbDataAdapter(sql, conStr);

            DataTable dt = new DataTable("Company");
            try
            {
                da.Fill(dt);
            }
            catch
            {
                throw;
            }
            return dt;
        }

    }
}
