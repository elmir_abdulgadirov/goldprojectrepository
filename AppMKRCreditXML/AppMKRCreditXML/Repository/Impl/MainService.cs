﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data;
using System.Xml;
using System.IO;
using System.Xml.Linq;

namespace AppMKRCreditXML.Repository.Impl
{
    class MainService : IMainService
    {

        DALC _dalc = new DALC();

        public string DeployCredit(string date1, string date2,out DataTable dt)
        {
            string _credits = "";
            DataTable dtCredit = _dalc.GetCredits(date1, date2);
            dt = dtCredit;
            if (dtCredit.Rows.Count == 0) return "NO_DATA";
            string creditXML = _dalc.ReadCreditXML();
            if (creditXML == "ERROR") return "ERROR";

            string _tempCredit;
            foreach (DataRow drCredit in dtCredit.Rows)
            {
                _tempCredit = creditXML.Trim();

                _tempCredit = _tempCredit
                             .Replace("{Borrower_id}", Convert.ToString(drCredit["ID"]))
                             .Replace("{Borrower_name}", Convert.ToString(drCredit["name"]))
                             .Replace("{Borrower_CountryCode}", Convert.ToString(drCredit["CountryCode"]))
                             .Replace("{Borrower_BankruptcyStatus}", Convert.ToString(drCredit["BankruptcyStatus"]))
                             .Replace("{Borrower_DateOfBirth}", Convert.ToString(drCredit["DateOfBirth"]))
                             .Replace("{Borrower_PlaceOfBirth}", Convert.ToString(drCredit["PlaceOfBirth"]))
                             .Replace("{Borrower_PinCode}", Convert.ToString(drCredit["PinCode"]))
                             .Replace("{credit_AccountNo}", Convert.ToString(drCredit["AccountNo"]))
                             .Replace("{credit_CurrencyOfCredit}", Convert.ToString(drCredit["CurrencyOfCredit"]))
                             .Replace("{credit_CreditType}", Convert.ToString(drCredit["CreditType"]))
                             .Replace("{credit_InitialAmountOfCredit}", Convert.ToString(drCredit["InitialAmountOfCredit"]))
                             .Replace("{credit_CreditLineAmount}", Convert.ToString(drCredit["CreditLineAmount"]))
                             .Replace("{credit_DisoutAmountOfCredit}", Convert.ToString(drCredit["DisoutAmountOfCredit"]))
                             .Replace("{credit_AnnualInterestRate}", Convert.ToString(drCredit["AnnualInterestRate"]))
                             .Replace("{credit_PurposeOfCredit}", Convert.ToString(drCredit["PurposeOfCredit"]))
                             .Replace("{credit_CreditPeriodInMonths}", Convert.ToString(drCredit["CreditPeriodInMonths"]))
                             .Replace("{credit_DateOfGrant}", Convert.ToString(drCredit["DateOfGrant"]))
                             .Replace("{credit_DueTimeFirstContract}", Convert.ToString(drCredit["DueTimeFirstContract"]))
                             .Replace("{credit_DueTimeLastContract}", Convert.ToString(drCredit["DueTimeLastContract"]))
                             .Replace("{credit_LastPaymentDate}", Convert.ToString(drCredit["LastPaymentDate"]))
                             .Replace("{credit_MonthlyPaymentAmount}", Convert.ToString(drCredit["MonthlyPaymentAmount"]))
                             .Replace("{credit_DaysMainSumIsOverdue}", Convert.ToString(drCredit["DaysMainSumIsOverdue"]))
                             .Replace("{credit_DaysInterestIsOverdue}", Convert.ToString(drCredit["DaysInterestIsOverdue"]))
                             .Replace("{credit_OiaForRepperiod}", Convert.ToString(drCredit["OiaForRepperiod"]))
                             .Replace("{credit_NumberOfProlongs}", Convert.ToString(drCredit["NumberOfProlongs"]))
                             .Replace("{credit_CreditClassCode}", Convert.ToString(drCredit["CreditClassCode"]))
                             .Replace("{credit_CreditStatusCode}", Convert.ToString(drCredit["CreditStatusCode"]))
                             
                             .Replace("{Group_Credit}", "") /*Grup sheklinde kreditler verilmediyi uchun bu tag-i nezere almiriq*/
                             ;
                /*Guarantees begin*/
                DataTable dtGuarantees = _dalc.GetGuarantees(Convert.ToInt32(drCredit["LOAN_ID"]));

                if (dtGuarantees.Rows.Count > 0)
                {
                    string _guaranteesXML = "";
                    foreach (DataRow drGuarantee in dtGuarantees.Rows)
                    {
                        _guaranteesXML += $@"<Guarantee>
						                        <id>{Convert.ToString(drGuarantee["ID"])}</id>
						                        <name>{Convert.ToString(drGuarantee["name"])}</name>
						                        <CountryCode>{Convert.ToString(drGuarantee["CountryCode"])}</CountryCode>
						                        <DateOfBirth>{Convert.ToString(drGuarantee["DateOfBirth"])}</DateOfBirth>
						                        <PlaceOfBirth>{Convert.ToString(drGuarantee["PlaceOfBirth"])}</PlaceOfBirth>
						                        <PinCode>{Convert.ToString(drGuarantee["PinCode"])}</PinCode>
					                         </Guarantee>" + System.Environment.NewLine;
                    }
                    _guaranteesXML = System.Environment.NewLine + "<Guarantees>" + _guaranteesXML + "</Guarantees>" ;
                    _tempCredit = _tempCredit.Replace("{Guarantees}", _guaranteesXML);
                }
                else
                {
                    _tempCredit = _tempCredit.Replace("{Guarantees}", "");
                }
                /*Guarantees end*/



                /*Collateral begin*/
                DataTable dtCollaterals = _dalc.GetCollateral(Convert.ToInt32(drCredit["LOAN_ID"]));

                if (dtCollaterals.Rows.Count > 0)
                {
                    string _collateralsXML = "";
                    foreach (DataRow drCollaterals in dtCollaterals.Rows)
                    {
                        // texniki tapshiraga esasen bir neche girov olarsa ,kredite aid bazar deyeri en yuksek olan goturulur
                        _collateralsXML = System.Environment.NewLine + $@"<Collateral>
				                                <CollateralTypeCode>{Convert.ToString(drCollaterals["CollateralTypeCode"])}</CollateralTypeCode>
				                                <AnyInfoToDisting>{Convert.ToString(drCollaterals["AnyInfoToDisting"])}</AnyInfoToDisting>
				                                <MarketValue>{Convert.ToString(drCollaterals["MarketValue"])}</MarketValue>
				                                <RegistryNo>{Convert.ToString(drCollaterals["RegistryNo"])}</RegistryNo>
				                                <RegistryDate>{Convert.ToString(drCollaterals["RegistryDate"])}</RegistryDate>
				                                <RegistryAgency>{Convert.ToString(drCollaterals["RegistryAgency"])}</RegistryAgency>
			                                </Collateral>";
                    }
                    
                    _tempCredit = _tempCredit.Replace("{Collateral}", _collateralsXML);
                }
                else
                {
                    _tempCredit = _tempCredit.Replace("{Collateral}", @"<Collateral><CollateralTypeCode>007</CollateralTypeCode></Collateral>");
                }
                /*Collateral end*/


                _credits += _tempCredit + System.Environment.NewLine;
            }
            _credits = _credits.TrimEnd('\r', '\n'); 
            return _credits;
        }

        public void DeployFullXML(string date1, string date2,out string result,out string path, out DataTable dt)
        {
            result = "OK";
            string _credits = DeployCredit(date1, date2,out dt);
            if (_credits == "ERROR") result = "Xəta baş verdi: Xəta kodu 1001";
            if (_credits == "NO_DATA") result = "Axtarış parametrlərinə uyğun kredit tapılmadı";
            string fullXML = _dalc.ReadMainXML();
            if (fullXML == "ERROR") result = "Xəta baş verdi: Xəta kodu 1002";

            DataRow _bankData = _dalc.GetBankData().Rows[0];

            fullXML = fullXML
                        .Replace("{ReportingDate}", DateTime.Now.ToString("dd/MM/yyyy").Replace(".","/").Replace(",","/"))
                        .Replace("{Credits}", _credits)
                        .Replace("{BankID}", Convert.ToString(_bankData["BankID"]))
                        .Replace("{BankName}", Convert.ToString(_bankData["BankName"]));

            WriteFile(fullXML,out path);
        }

        public void WriteFile(string fullXML,out string result)
        {
            string rootFolder, yearFolder, monthFolder, dayFolder,_path;
            rootFolder = ConfigurationManager.AppSettings["MKR_FOLDER"];
            yearFolder = DateTime.Now.Year.ToString("0000");
            monthFolder = getMonth(DateTime.Now.Month.ToString("00"));
            dayFolder = DateTime.Now.Day.ToString("00");

            _path = $@"{rootFolder}{yearFolder}\{monthFolder}\{dayFolder}";

            if (!System.IO.Directory.Exists(_path))
            {
                System.IO.Directory.CreateDirectory(_path);
            }
            string pp = $@"{_path}\mkr_result_{DateTime.Now.ToString("ddMMyyyyhhmmss")}.xml";
            System.IO.File.WriteAllText(pp, fullXML);

 
            result = pp;
        }

        string getMonth(string m)
        {
            string _mm = "";
            switch (m)
            {
                case "01": _mm = "Yanvar";   break;
                case "02": _mm = "Fevral"; break;
                case "03": _mm = "Mart"; break;
                case "04": _mm = "Aprel"; break;
                case "05": _mm = "May"; break;
                case "06": _mm = "Iyun"; break;
                case "07": _mm = "Iyul"; break;
                case "08": _mm = "Avqust"; break;
                case "09": _mm = "Sentyabr"; break;
                case "10": _mm = "Oktyabr"; break;
                case "11": _mm = "Noyabr"; break;
                case "12": _mm = "Dekabr"; break;
            }
            return _mm;
        }



        

    }
}
