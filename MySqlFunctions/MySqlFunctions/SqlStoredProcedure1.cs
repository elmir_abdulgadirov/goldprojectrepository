using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Transactions;

public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void CLR_spInsertDailyProcDebt(SqlInt32 vCreatedID,SqlString vCreatedName, SqlDateTime vTrandate,out SqlInt32 vID)
    {
          using (SqlConnection connection = new SqlConnection("context connection=true"))
          {
              connection.Open();
              SqlCommand cmd = new SqlCommand("spInsertDailyProcDebt", connection);
              cmd.CommandType = CommandType.StoredProcedure;

              SqlParameter pCreatedID = new SqlParameter("@pCreatedID", SqlDbType.Int);
              pCreatedID.Direction = ParameterDirection.Input;
              pCreatedID.Value = vCreatedID;
              cmd.Parameters.Add(pCreatedID);

              SqlParameter pCreatedName = new SqlParameter("@pCreatedName", SqlDbType.VarChar);
              pCreatedName.Direction = ParameterDirection.Input;
              pCreatedName.Size = 50;
              pCreatedName.Value = vCreatedName;
              cmd.Parameters.Add(pCreatedName);

              SqlParameter pTrandate = new SqlParameter("@pTrandate", SqlDbType.DateTime);
              pTrandate.Direction = ParameterDirection.Input;
              pTrandate.Value = vTrandate;
              cmd.Parameters.Add(pTrandate);

              SqlParameter paramID = new SqlParameter("@pID", SqlDbType.Int);
              paramID.Direction = ParameterDirection.Output;
              cmd.Parameters.Add(paramID);

              try
              {
                 cmd.ExecuteNonQuery();
                 vID = Convert.ToInt32(cmd.Parameters["@pID"].Value);
              }
              catch (Exception ex)
              {
                  vID = -1;
                  //   throw new Exception();
              }
     
        }
    }



    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void CLR_spUpdateDailyProcDebt(SqlInt32 vID, SqlString vResult)
    {
        using (SqlConnection connection = new SqlConnection("context connection=true"))
        {
            connection.Open();
            SqlCommand cmd = new SqlCommand("spUpdateDailyProcDebt", connection);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter pID = new SqlParameter("@pID", SqlDbType.Int);
            pID.Direction = ParameterDirection.Input;
            pID.Value = vID;
            cmd.Parameters.Add(pID);

            SqlParameter pResult = new SqlParameter("@pResult", SqlDbType.VarChar);
            pResult.Direction = ParameterDirection.Input;
            pResult.Size = 300;
            pResult.Value = vResult;
            cmd.Parameters.Add(pResult);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                SqlContext.Pipe.Send(ex.Message);
            }

        }
    }
   

}
