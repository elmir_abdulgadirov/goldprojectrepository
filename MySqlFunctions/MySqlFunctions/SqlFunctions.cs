using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

public partial class UserDefinedFunctions
{
    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlString GetMsgIDFirstPart(string MsgID)
    {
        string[] arr = MsgID.Split('-');
        string newMsgID = "";
        for (int i = 0; i < arr.Length - 1; i++)
        {
            newMsgID += arr[i] + "-";
        }
        newMsgID = newMsgID.Trim('-');
        return newMsgID;
    }



    [Microsoft.SqlServer.Server.SqlFunction]
    public static SqlString GetSplitPart(string input, char separator, int index)
    {
        try
        {
            string[] arr = input.Split(separator);
            return arr[index];
        }
        catch
        {
            return "";
        }
    }
}
