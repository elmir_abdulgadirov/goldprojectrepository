﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Drop
       [MySqlFunctions] (Assembly)
     Create
       [MySqlFunctions] (Assembly)

** Supporting actions
     Drop
       [dbo].[SqlFunctions] (Function)
       [dbo].[CLR_spInsertDailyProcDebt] (Procedure)
       [dbo].[CLR_spUpdateDailyProcDebt] (Procedure)
     Create
       [dbo].[CLR_spInsertDailyProcDebt] (Procedure)
       [dbo].[CLR_spUpdateDailyProcDebt] (Procedure)

If this deployment is executed, [dbo].[SqlFunctions] will be dropped and not re-created.

