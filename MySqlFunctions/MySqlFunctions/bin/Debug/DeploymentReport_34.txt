﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Drop
       [MySqlFunctions] (Assembly)
     Create
       [MySqlFunctions] (Assembly)
       [dbo].[GetMsgIDFirstPart] (Function)
       [dbo].[CLR_spInsertDailyProcDebt] (Procedure)
       [dbo].[CLR_spUpdateDailyProcDebt] (Procedure)

** Supporting actions
